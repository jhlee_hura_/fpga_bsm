-makelib xcelium_lib/xil_defaultlib -sv \
  "C:/Xilinx/Vivado/2018.1/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \
  "C:/Xilinx/Vivado/2018.1/data/ip/xpm/xpm_memory/hdl/xpm_memory.sv" \
-endlib
-makelib xcelium_lib/xpm \
  "C:/Xilinx/Vivado/2018.1/data/ip/xpm/xpm_VCOMP.vhd" \
-endlib
-makelib xcelium_lib/fifo_generator_v13_2_2 \
  "../../Bell_charlie.ip_user_files/ipstatic/simulation/fifo_generator_vlog_beh.v" \
-endlib
-makelib xcelium_lib/fifo_generator_v13_2_2 \
  "../../Bell_charlie.ip_user_files/ipstatic/hdl/fifo_generator_v13_2_rfs.vhd" \
-endlib
-makelib xcelium_lib/fifo_generator_v13_2_2 \
  "../../Bell_charlie.ip_user_files/ipstatic/hdl/fifo_generator_v13_2_rfs.v" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../Bell_common/ip/fifo_32x64k/sim/fifo_32x64k.v" \
  "../../../Bell_common/ip/fifo_regin_47x16/sim/fifo_regin_47x16.v" \
-endlib
-makelib xcelium_lib/blk_mem_gen_v8_4_1 \
  "../../Bell_charlie.ip_user_files/ipstatic/simulation/blk_mem_gen_v8_4.v" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../Bell_common/ip/rom_cr/sim/rom_cr.v" \
  "../../../Bell_common/ip/selectio_8to1_dc/selectio_8to1_dc_selectio_wiz.v" \
  "../../../Bell_common/ip/selectio_8to1_dc/selectio_8to1_dc.v" \
-endlib
-makelib xcelium_lib/xbip_utils_v3_0_9 \
  "../../Bell_charlie.ip_user_files/ipstatic/hdl/xbip_utils_v3_0_vh_rfs.vhd" \
-endlib
-makelib xcelium_lib/c_reg_fd_v12_0_5 \
  "../../Bell_charlie.ip_user_files/ipstatic/hdl/c_reg_fd_v12_0_vh_rfs.vhd" \
-endlib
-makelib xcelium_lib/c_mux_bit_v12_0_5 \
  "../../Bell_charlie.ip_user_files/ipstatic/hdl/c_mux_bit_v12_0_vh_rfs.vhd" \
-endlib
-makelib xcelium_lib/c_shift_ram_v12_0_12 \
  "../../Bell_charlie.ip_user_files/ipstatic/hdl/c_shift_ram_v12_0_vh_rfs.vhd" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../Bell_common/ip/sr_8x128/sim/sr_8x128.vhd" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../Bell_common/ip/fifod_1x16/sim/fifod_1x16.v" \
  "../../../Bell_common/ip/fifod_32x16/sim/fifod_32x16.v" \
  "../../../Bell_common/ip/selectio_1to8_dc/selectio_1to8_dc_selectio_wiz.v" \
  "../../../Bell_common/ip/selectio_1to8_dc/selectio_1to8_dc.v" \
  "../../../Bell_common/ip/clk_wiz_ld1530/clk_wiz_ld1530_clk_wiz.v" \
  "../../../Bell_common/ip/clk_wiz_ld1530/clk_wiz_ld1530.v" \
  "../../../Bell_common/ip/clk_wiz_serdes/clk_wiz_serdes_clk_wiz.v" \
  "../../../Bell_common/ip/clk_wiz_serdes/clk_wiz_serdes.v" \
  "../../../Bell_common/ip/clk_wiz_0/clk_wiz_0_clk_wiz.v" \
  "../../../Bell_common/ip/clk_wiz_0/clk_wiz_0.v" \
  "../../../Bell_common/board_intf/board_perip_intf.v" \
  "../../Bell_charlie.srcs/sources_1/new/con_set/con10_intf_charlie.v" \
  "../../Bell_charlie.srcs/sources_1/new/con_set/con10b3_iobuf_charlie.v" \
  "../../Bell_charlie.srcs/sources_1/new/con_set/con1_intf_charlie.v" \
  "../../Bell_charlie.srcs/sources_1/new/con_set/con1b3_iobuf_charlie.v" \
  "../../Bell_charlie.srcs/sources_1/new/con_set/con2_intf_charlie.v" \
  "../../Bell_charlie.srcs/sources_1/new/con_set/con2b3_iobuf_charlie.v" \
  "../../Bell_charlie.srcs/sources_1/new/con_set/con3_intf_charlie.v" \
  "../../Bell_charlie.srcs/sources_1/new/con_set/con3b3_iobuf_charlie.v" \
  "../../Bell_charlie.srcs/sources_1/new/con_set/con4_intf_charlie.v" \
  "../../Bell_charlie.srcs/sources_1/new/con_set/con4b3_iobuf_charlie.v" \
  "../../Bell_charlie.srcs/sources_1/new/con_set/con5_intf_charlie.v" \
  "../../Bell_charlie.srcs/sources_1/new/con_set/con5b3_iobuf_charlie.v" \
  "../../Bell_charlie.srcs/sources_1/new/con_set/con6_intf_charlie.v" \
  "../../Bell_charlie.srcs/sources_1/new/con_set/con6b3_iobuf_charlie.v" \
  "../../Bell_charlie.srcs/sources_1/new/con_set/con7_intf_charlie.v" \
  "../../Bell_charlie.srcs/sources_1/new/con_set/con7b3_iobuf_charlie.v" \
  "../../Bell_charlie.srcs/sources_1/new/con_set/con8_intf_charlie.v" \
  "../../Bell_charlie.srcs/sources_1/new/con_set/con8b3_iobuf_charlie.v" \
  "../../Bell_charlie.srcs/sources_1/new/con_set/con9_intf_charlie.v" \
  "../../../Bell_common/_lib/common/data_merge2to1.v" \
  "../../../Bell_common/data_gen/data_serialize.v" \
  "../../../Bell_common/_lib/common/data_width_downconvert.v" \
  "../../../Bell_common/_lib/ext_intf/i2c_intf.v" \
  "../../../Bell_common/data_gen/laser_trig_gen.v" \
  "../../../Bell_common/data_gen/log_8bit.v" \
  "../../../Bell_common/_lib/common/pls_expander.v" \
  "../../../Bell_common/_lib/ext_intf/pwm.v" \
  "../../Bell_charlie.srcs/sources_1/new/regs/regs_sig_charlie.v" \
  "../../Bell_charlie.srcs/sources_1/new/regs/regs_sys_charlie.v" \
  "../../../Bell_common/_lib/ext_intf/shiftreg_intf.v" \
  "../../../Bell_common/data_gen/spad_detect.v" \
  "../../../Bell_common/_lib/ext_intf/spi_intf.v" \
  "../../Bell_charlie.srcs/sources_1/new/Bell_Charlie.v" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  glbl.v
-endlib

