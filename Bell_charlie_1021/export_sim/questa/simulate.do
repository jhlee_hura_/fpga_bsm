onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib Bell_Charlie_opt

do {wave.do}

view wave
view structure
view signals

do {Bell_Charlie.udo}

run -all

quit -force
