onbreak {quit -f}
onerror {quit -f}

vsim -voptargs="+acc" -t 1ps -L xil_defaultlib -L xpm -L fifo_generator_v13_2_2 -L blk_mem_gen_v8_4_1 -L xbip_utils_v3_0_9 -L c_reg_fd_v12_0_5 -L c_mux_bit_v12_0_5 -L c_shift_ram_v12_0_12 -L unisims_ver -L unimacro_ver -L secureip -lib xil_defaultlib xil_defaultlib.Bell_Charlie xil_defaultlib.glbl

do {wave.do}

view wave
view structure
view signals

do {Bell_Charlie.udo}

run -all

quit -force
