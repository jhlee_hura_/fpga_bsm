`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: HURA
// Engineer: LEE Seong Yun (sylee@hura.co.kr)
//
// Create Date: 2019/01/04 11:05:02
// Design Name: QKD
// Module Name: qkd_alice_test_bit_gen
// Project Name: QKD
// Target Devices: Kintex 7 160T
// Tool Versions: Vivado 2018.1
// Description:
//
// Dependencies:
//
// Revision:
// 		build. 1 (2019/01/04)
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////
module qkd_alice_test_bit_gen (
	input wire 			clk,
	input wire 			sclr,
	input wire			load,
	input wire [31:0]	sig0_test_data,
	input wire [31:0]	sig1_test_data,
	input wire [31:0]	pm_test_data,
	input wire [31:0]	d_test_data,
	input wire			rd_en,

	output wire [1:0] 	sig_bit,
	output wire			phase_bit,
	output wire			data_bit,
	output reg			dov_bit
);

assign sig_bit		= (sig1[31] && sig0[31]) ? 2'b10 : {sig1[31],sig0[31]};
assign phase_bit	= pm[31];
assign data_bit		= d[31];

reg [31:0] sig0;
reg [31:0] sig1;
reg [31:0] pm;
reg [31:0] d;

always @(posedge clk) begin
	if (sclr) begin
		sig0 <= sig0_test_data;
		sig1 <= sig1_test_data;
		pm <= pm_test_data;
		d <= d_test_data;
		dov_bit <= 1'b0;
	end
	else begin
		if (load) begin
			sig0 <= sig0_test_data;
			sig1 <= sig1_test_data;
			pm <= pm_test_data;
			d <= d_test_data;
			dov_bit <= 1'b0;
		end
		else if (rd_en) begin
			sig0 <= {sig0[30:0],sig0[31]};
			sig1 <= {sig1[30:0],sig1[31]};
			pm <= {pm[30:0],pm[31]};
			d <= {d[30:0],d[31]};
			dov_bit <= 1'b1;
		end
		else begin
			sig0 <= sig0;
			sig1 <= sig1;
			pm <= pm;
			d <= d;
			dov_bit <= 1'b0;
		end
	end
end


endmodule