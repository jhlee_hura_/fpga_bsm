`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: HURA
// Engineer: LEE Seong Yun (sylee@hura.co.kr)
//
// Create Date: 2019/01/04 11:05:02
// Design Name: 
// Module Name: con6b3_iobuf_charlie 
// Project Name: 
// Target Devices: Kintex 7 160T
// Tool Versions: Vivado 2018.1
// Description:
//
// Dependencies:
//
// Revision:
// 		build. 1 (2019/01/04)
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////
module con6b3_iobuf_charlie (
	inout 	wire [15:0]	CON,

    // LTC 1867
    input wire            ltc1867_sck,
    input wire            ltc1867_scs,
    input wire            ltc1867_sdo,
    output wire            ltc1867_sdi,
    // DAC MAX5144 for LD1530_TEC
    input wire             tec_dac_sck,
    input wire             tec_dac_scs,
    input wire             tec_dac_sdo,
    input wire            tec_clr,
    input wire            tec_shdn_b,
    // DAC AD5761 for LD1530
    input wire             ad5761_dac_sck,
    input wire             ad5761_dac_scs,
    input wire             ad5761_dac_sdo,
    output wire         ad5761_dac_sdi,
    // Enable
    input wire            hmc920_en,
    
    // [DP] AD5252  
    input wire            dp_sck,            // I
    output wire            dp_sdi,            // I
    input wire            dp_sdo,            // O
    inout wire            dp_T            // IO
    
    );
    
    // --------------------------------------------------------------------
    // SPI - LTC1867
    // --------------------------------------------------------------------
    IOBUF CON_0 (
    .O(),                // Buffer output
    .IO(CON[0]),        // Buffer inout port (connect directly to top-level port)
    .I(ltc1867_sck),    // Buffer input
    .T(1'b0)            // 3-state enable input, high=input, low=output
    );
    
    IOBUF CON_1 (
    .O(),                // Buffer output
    .IO(CON[1]),        // Buffer inout port (connect directly to top-level port)
    .I(ltc1867_scs),    // Buffer input
    .T(1'b0)            // 3-state enable input, high=input, low=output
    );
    
    IOBUF CON_2 (
    .O(ltc1867_sdi),    // Buffer output
    .IO(CON[2]),        // Buffer inout port (connect directly to top-level port)
    .I(1'bz),            // Buffer input
    .T(1'b1)            // 3-state enable input, high=input, low=output
    );
    
    IOBUF CON_3 (
    .O(),                // Buffer output
    .IO(CON[3]),        // Buffer inout port (connect directly to top-level port)
    .I(ltc1867_sdo),    // Buffer input
    .T(1'b0)            // 3-state enable input, high=input, low=output
    );
    
    
    // --------------------------------------------------------------------
    // TEC (MAX5144)
    // --------------------------------------------------------------------
    IOBUF CON_4 (
    .O(),                    // Buffer output
    .IO(CON[4]),            // Buffer inout port (connect directly to top-level port)
    .I(tec_dac_sdo),    // Buffer input
    .T(1'b0)                // 3-state enable input, high=input, low=output
    );
    
    IOBUF CON_5 (
    .O(),                // Buffer output
    .IO(CON[5]),        // Buffer inout port (connect directly to top-level port)
    .I(tec_clr),    // Buffer input
    .T(1'b0)            // 3-state enable input, high=input, low=output
    );
    
    IOBUF CON_6 (
    .O(),                    // Buffer output
    .IO(CON[6]),            // Buffer inout port (connect directly to top-level port)
    .I(tec_dac_scs),    // Buffer input
    .T(1'b0)                // 3-state enable input, high=input, low=output
    );
    
    IOBUF CON_7 (
    .O(),                    // Buffer output
    .IO(CON[7]),            // Buffer inout port (connect directly to top-level port)
    .I(tec_dac_sck),    // Buffer input
    .T(1'b0)                // 3-state enable input, high=input, low=output
    );
    
    IOBUF CON_8 (
    .O(),                    // Buffer output
    .IO(CON[8]),            // Buffer inout port (connect directly to top-level port)
    .I(tec_shdn_b),    // Buffer input
    .T(1'b0)                // 3-state enable input, high=input, low=output
    );
    
    // --------------------------------------------------------------------
    // LD1550(AD5761)
    // --------------------------------------------------------------------
    IOBUF CON_9 (
    .O(),                    // Buffer output
    .IO(CON[9]),            // Buffer inout port (connect directly to top-level port)
    .I(ad5761_dac_scs),    // Buffer input
    .T(1'b0)                // 3-state enable input, high=input, low=output
    );
    
    IOBUF CON_10 (
    .O(),                // Buffer output
    .IO(CON[10]),        // Buffer inout port (connect directly to top-level port)
    .I(ad5761_dac_sck),    // Buffer input
    .T(1'b0)            // 3-state enable input, high=input, low=output
    );
    
    IOBUF CON_11 (
    .O(),                    // Buffer output
    .IO(CON[11]),            // Buffer inout port (connect directly to top-level port)
    .I(ad5761_dac_sdo),    // Buffer input
    .T(1'b0)                // 3-state enable input, high=input, low=output
    );
    
    IOBUF CON_12 (
    .O(ad5761_dac_sdi),                    // Buffer output
    .IO(CON[12]),            // Buffer inout port (connect directly to top-level port)
    .I(1'bz),    // Buffer input
    .T(1'b1)                // 3-state enable input, high=input, low=output
    );
    
    
    IOBUF CON_13 (
    .O(),                // Buffer output
    .IO(CON[13]),    // Buffer inout port (connect directly to top-level port)
    .I(hmc920_en),        // Buffer input
    .T(1'b0)            // 3-state enable input, high=input, low=output
    );
    
    IOBUF CON_14 (
    .O(dp_sdi),                // Buffer output
    .IO(CON[14]),    // Buffer inout port (connect directly to top-level port)
    .I(dp_sdo),        // Buffer input
    .T(dp_T)            // 3-state enable input, high=input, low=output
    );
    
    IOBUF CON_15 (
    .O(),                // Buffer output
    .IO(CON[15]),    // Buffer inout port (connect directly to top-level port)
    .I(dp_sck),        // Buffer input
    .T(1'b0)            // 3-state enable input, high=input, low=output
    );
    // --------------------------------------------------------------------

endmodule