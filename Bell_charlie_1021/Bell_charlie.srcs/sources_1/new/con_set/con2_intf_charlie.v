`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: HURA
// Engineer: LEE Seong Yun (sylee@hura.co.kr)
//
// Create Date: 2019/01/04 11:05:02
// Design Name: 
// Module Name: con2_intf_charlie 
// Project Name: 
// Target Devices: Kintex 7 160T
// Tool Versions: Vivado 2018.1
// Description:
//
// Dependencies:
//
// Revision:
// 		build. 1 (2019/01/04)
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////
module con2_intf_charlie (
   
    input    wire   CON2B1_p0,
    input    wire   CON2B1_n0,
    output   wire   CON2B1_p1,
    output   wire   CON2B1_n1,
    input    wire   CON2B1_p2,
    input    wire   CON2B1_n2,  
    output   wire   CON2B2_p,
    output   wire   CON2B2_n,          
    inout    wire [21:0]    CON2B3,
    
      input     wire     sys_clk,
      input    wire    sig_clk,
      input    wire    clk_200,
      input    wire    clk_600,
      (* mark_debug = "true" *)input    wire    sig_10M,
      input    wire    rst,    
      //
      input   wire [1:0] trig_type,
      
      output  reg       spd_trig,
      output  wire      spd_trig_valid,
      output  wire      mask_10M,
      
      output    reg [31:0]    spad_cnt1,
      output    reg [31:0]    spad_cnt2,
      output    reg [31:0]    spad_cnt3,        
      output    reg [31:0]    spad_cnt4,         
      // ---- Serdes ---
      input    wire           test_mode,
      input    wire [31:0]    din_test,
      input    wire           shape_load,            // set false path
      input    wire [5:0]     pulse_width,        // set false path
      input    wire [6:0]     i_offset_quotient,        // set false path
      input    wire [2:0]     i_offset_remainder,
      // delay tap 78ps per tap (max 2.418ns)    
      input wire [4:0]    delay_tap_in,        // set false path
      input wire [4:0]    detect_delay_tap_in, //             
      // ---- Enable  -----
      input    wire        hmc920_en,
      
      // ---- TEC READ ADC (LTC1867) ---
      input    wire [15:0]    ltc1867_din,
      input    wire        ltc1867_din_valid,
      output    wire        ltc1867_din_ready,
      output    wire [15:0]    ltc1867_dout,
      
      // ---- TEC DAC (MAX5144) ---
      input    wire        tec_shdn_b,
      input    wire        tec_clr,
      input    wire [15:0]    tec_dac_din,
      input    wire        tec_dac_din_valid,
      output    wire        tec_dac_din_ready,
      
      // ---- SIP DAC (AD5060) ---
      input    wire        f_sip_disable,
      input    wire [23:0]    sip_dac_din,
      input    wire        sip_dac_din_valid,
      output    wire        sip_dac_din_ready,
      
      // ---- SCNT DCNT DAC (AD5752A) ---
      input    wire        spd_en,
      input    wire [23:0]    spd_dac_din,
      input    wire        spd_dac_din_valid,
      output    wire        spd_dac_din_ready,
      
      // ---- DP (AD5252) ---
      input    wire [23:0]    dp_din,
      input    wire        dp_din_valid,
      output    wire [7:0]    dp_dout
);
OBUFDS #(
	.IOSTANDARD("DEFAULT"), // Specify the output I/O standard
	.SLEW("FAST")			// Specify the output slew rate
) OBUFDS_inst (
	.O(CON2B2_p),		// Diff_p output (connect directly to top-level port)
	.OB(CON2B2_n),		// Diff_n output (connect directly to top-level port)
	.I(1'b1)				// Buffer input
);

// ---------------------------------------------------------------------------------
// SPD Trig MASK
// ---------------------------------------------------------------------------------
data_serialize_c trigmask_10M_gen (
    .clk            (sig_clk),        // I
    .rst            (rst),            // I
    .clk_200        (clk_200),    // I
    .clk_600        (clk_600),    // I
    
    .test_mode        (test_mode),    // I
    .din_test        (din_test),        // I [31:0]
    
    .polarity        (1'b1),                // I
    .din            (32'hffffffff),        // I [31:0]
    .din_valid        (1'b0),                // I
    .din_ready        (),                    // O
    
    .do_bit            (),                    // O
    .dov_bit        (mask_10M),                    // O
    
    .dout_p            (CON2B1_p1),            // O
    .dout_n            (CON2B1_n1),            // O
    
    .shape_load        (shape_load),        // I
    .pulse_width    (pulse_width),        // I [5:0]
    .i_offset_quotient    (i_offset_quotient),        // I [6:0]
    .i_offset_remainder   (i_offset_remainder),         // [2:0]
    .delay_tap_in    (delay_tap_in),        // I [4:0]
    .delay_tap_out    ()    // O [4:0]

);

// ---------------------------------------------------------------------------------
// SPD Trig in 1 (SCNT)
// ---------------------------------------------------------------------------------
wire do_trig1;
spad_detect_b spad_detect_1 (
    .sig_clk(sig_clk),                // I     // 100
    .rst(rst),                   // I
    .clk_200(clk_200),                // I     // 100    
    .clk_600(clk_600),        // I     // 700
    //to pad
    .din_p(CON2B1_p0),        // I
    .din_n(CON2B1_n0),        // I
    
    .dout_trig(do_trig1),        // O // clk domain
    .delay_tap_in(detect_delay_tap_in),    // I [4:0]
    .delay_tap_out()               // O [4:0]    
);
// ---------------------------------------------------------------------------------
// SPD Trig in 2 (DCNT or NCNT)
// ---------------------------------------------------------------------------------
(* mark_debug = "true" *)wire do_trig2;

spad_detect_b spad_detect_2 (
    .sig_clk(sig_clk),                // I     // 100
    .rst(rst),                   // I
    .clk_200(clk_200),                // I     // 100    
    .clk_600(clk_600),        // I     // 700
   
    //to pad
    .din_p(CON2B1_p2),        // I
    .din_n(CON2B1_n2),        // I
    .dout_trig(do_trig2),        // O // clk domain
    .delay_tap_in(detect_delay_tap_in),    // I [4:0]
    .delay_tap_out()               // O [4:0]    
);
// ---------------------------------------------------------------------------------
// SPD Valid Trig Signal GEN.
// ---------------------------------------------------------------------------------

reg trig1_r;
reg trig2_r;
always @(posedge sig_clk) begin
if (sig_10M) begin
    trig1_r <= 1'b0;
    trig2_r <= 1'b0;        
end
else begin
    trig1_r <= (do_trig1) ? 1'b1 : trig1_r;
    trig2_r <= (do_trig2) ? 1'b1 : trig2_r;        
end
end

wire trig1; 
wire trig2;

assign trig1 = sig_10M  & (trig1_r | do_trig1);
assign trig2 = sig_10M  & (trig2_r | do_trig2);
assign spd_trig_valid = sig_10M;
// ---------------------------------------------------------------------------------
// Trig. for Log. & CNT(Single, Differential, Negative, OR, SPD Signal GEN)
// ---------------------------------------------------------------------------------

wire sing_trig;
wire neg_trig;
wire dff_trig;
wire or_trig;
//wire b2_neg_or_trig;
assign sing_trig = trig1;
assign dff_trig =  trig2;
assign neg_trig = ((~trig2_r) & (~do_trig2))& sig_10M;
assign or_trig = (sing_trig || neg_trig);

always @ * begin
    case  (trig_type)
    2'b00 :  spd_trig = sing_trig ;
    2'b01 :  spd_trig = dff_trig; 
    2'b10 :  spd_trig = neg_trig;
    2'b11 :  spd_trig = or_trig;     
    default : spd_trig = sing_trig;
    endcase      
end

// ---------------------------------------------------------------------------------
// SPD Valid Trig Signal Count
// ---------------------------------------------------------------------------------
reg [31:0] trig1_cnt_tmp;
reg [31:0] trig2_cnt_tmp;
reg [31:0] trig3_cnt_tmp;
reg [31:0] trig4_cnt_tmp;
reg [31:0]    clk_cnt;

always @(posedge sig_clk) begin
if (rst) begin
        clk_cnt            <= 'b0;
        spad_cnt1        <= 'b0;
        spad_cnt2        <= 'b0;
        spad_cnt3        <= 'b0;
        spad_cnt4        <= 'b0;        
        trig1_cnt_tmp    <= 'b0;
        trig2_cnt_tmp    <= 'b0;
        trig3_cnt_tmp    <= 'b0;
        trig4_cnt_tmp    <= 'b0;                 
    end
    else begin
        clk_cnt          <=    (clk_cnt >= (`SIGCLKPERSEC-1'b1)) ? 'b0 : clk_cnt + 1'b1;
        spad_cnt1        <=    (clk_cnt == 'b0) ? trig1_cnt_tmp : spad_cnt1;
        trig1_cnt_tmp    <=    (clk_cnt == 'b0) ? 'b0 :
                            (sing_trig ) ? trig1_cnt_tmp + 1'b1 : trig1_cnt_tmp;
        spad_cnt2        <=    (clk_cnt == 'b0) ? trig2_cnt_tmp : spad_cnt2;
        trig2_cnt_tmp    <=    (clk_cnt == 'b0) ? 'b0 :
                            (dff_trig ) ? trig2_cnt_tmp + 1'b1 : trig2_cnt_tmp; 
        spad_cnt3        <=    (clk_cnt == 'b0) ? trig3_cnt_tmp : spad_cnt3;       
        trig3_cnt_tmp    <=    (clk_cnt == 'b0) ? 'b0 :
                            (neg_trig) ? trig3_cnt_tmp + 1'b1 : trig3_cnt_tmp;   
        spad_cnt4        <=    (clk_cnt == 'b0) ? trig4_cnt_tmp : spad_cnt4;       
        trig4_cnt_tmp    <=    (clk_cnt == 'b0) ? 'b0 :
                            (or_trig) ? trig4_cnt_tmp + 1'b1 : trig4_cnt_tmp;                                                           
    end
end


// ---------------------------------------------------------------------------------
// Assign CON1B3
// ---------------------------------------------------------------------------------
wire    ltc1867_sck;
wire    ltc1867_scs;
wire    ltc1867_sdo;
wire    ltc1867_sdi;
wire    tec_dac_sck;
wire    tec_dac_scs;
wire    tec_dac_sdo;
wire    sip_dac_sck;
wire    sip_dac_scs;
wire    sip_dac_sdo;
wire    spd_dac_sck;
wire    spd_dac_scs;
wire    spd_dac_sdo;
//  Digital Potential Meter(AD5252) I2C Port
wire    dp_sck;
wire    dp_sdo;
wire    dp_sdi;
wire     dp_T;

con2b3_iobuf_charlie con2b3_iobuf_charlie (
    .CON(CON2B3),        // IO [21:0]
    
    // LTC 1867
    .ltc1867_sck(ltc1867_sck),        // I
    .ltc1867_scs(ltc1867_scs),        // I
    .ltc1867_sdo(ltc1867_sdo),        // I
    .ltc1867_sdi(ltc1867_sdi),        // O
    // DAC MAX5144 for LD1550_TEC
    .tec_dac_sck(tec_dac_sck),        // I
    .tec_dac_scs(tec_dac_scs),        // I
    .tec_dac_sdo(tec_dac_sdo),        // I
    .tec_clr(tec_clr),                // I
    .tec_shdn_b(tec_shdn_b),        // I
    // DAC AD5060 for SIP
    .sip_dac_sck(sip_dac_sck),        // I
    .sip_dac_scs(sip_dac_scs),        // I
    .sip_dac_sdo(sip_dac_sdo),        // I
    // DAC AD5752 for SPD
    .spd_dac_sck(spd_dac_sck),        // I
    .spd_dac_scs(spd_dac_scs),        // I
    .spd_dac_sdo(spd_dac_sdo),        // I
    // Enable
    .hmc920_en(hmc920_en),            // I
    
    .spd_en(spd_en),                // I
    .f_sip_disable(f_sip_disable),    // I
    
    // [DP] AD5252  for
    .dp_sck(dp_sck),            // I
    .dp_sdi(dp_sdi),            // O
    .dp_sdo(dp_sdo),            // I
    .dp_T(dp_T)            // IO
);


// ---------------------------------------------------------------------------------
//  LTC1867 Converter SPI Port
// ---------------------------------------------------------------------------------
spi_intf #(
    .SCLK_PERIOD(16),
    .CLK_START_END(8),
    .HIGH_CLK(8),
    .CLK_OFFSET(4),
    .CONT_WIDTH(0),
    .DATA_WIDTH(16),
    .RW_FLAG_BIT(0),
    .SCS_N_MODE(0),
    .SCLK_IDLE_MODE(0)
)
spi_intf_ltc1867(
    .clk(sys_clk),                            // I
    .sclr(rst),                        // I
    
    .sclk        (ltc1867_sck),            // O
    .scs_n        (ltc1867_scs),            // O
    .sdo        (ltc1867_sdo),            // O
    .sdi        (ltc1867_sdi),            // I
    .iobuf_T    (),                        // O //not used for 4line SPI
    
    .din        (ltc1867_din),            // I [WORD_WIDTH-1:0]
    .din_valid    (ltc1867_din_valid),    // I
    .din_ready    (ltc1867_din_ready),    // O
    .dout        (ltc1867_dout),            // O [WORD_WIDTH-1:0]
    .dout_valid    ()                        // O
);

// ---------------------------------------------------------------------------------
//  LD1550_TEC MAX5144 DAC Shift Reg Port
// ---------------------------------------------------------------------------------
shiftreg_intf #(
    .SCLK_PERIOD(16),
    .CLK_START_END(8),
    .HIGH_CLK(8),
    .CLK_OFFSET(4),
    .DATA_WIDTH(16),
    .SCS_N_MODE(0),
    .SCLK_IDLE_MODE(1)
) spi_intf_tec(
    .clk(sys_clk),                                    // I
    .sclr(rst),                                // I
    
    .din        (tec_dac_din),                // I [DATA_WIDTH-1:0]
    .din_valid    (tec_dac_din_valid),        // I
    .din_ready    (tec_dac_din_ready),        // O
    
    .sclk        (tec_dac_sck),            // O
    .scs_n        (tec_dac_scs),            // O
    .latch_en    (),                                // O
    .sdo        (tec_dac_sdo)            // O
);


// ---------------------------------------------------------------------------------
//  DAC AD5060 for SIP
// ---------------------------------------------------------------------------------
shiftreg_intf #(
    .SCLK_PERIOD(16),
    .CLK_START_END(8),
    .HIGH_CLK(8),
    .CLK_OFFSET(2),
    .DATA_WIDTH(24),
    .SCS_N_MODE(0),
    .SCLK_IDLE_MODE(1)
) spi_intf_sip_dac(
    .clk(sys_clk),                                // I
    .sclr(rst),                            // I
    
    .din        (sip_dac_din),                // I [DATA_WIDTH-1:0]
    .din_valid    (sip_dac_din_valid),        // I
    .din_ready    (sip_dac_din_ready),        // O
    
    .sclk        (sip_dac_sck),                // O
    .scs_n        (sip_dac_scs),                // O
    .latch_en    (),                            // O
    .sdo        (sip_dac_sdo)                // O

);

// ---------------------------------------------------------------------------------
//  DAC AD5752 for SPD
// ---------------------------------------------------------------------------------
shiftreg_intf #(
    .SCLK_PERIOD(16),
    .CLK_START_END(8),
    .HIGH_CLK(8),
    .CLK_OFFSET(2),
    .DATA_WIDTH(24),
    .SCS_N_MODE(0),
    .SCLK_IDLE_MODE(1)
) spi_intf_spd_dac(
    .clk(sys_clk),                                // I
    .sclr(rst),                            // I
    
    .din        (spd_dac_din),                // I [DATA_WIDTH-1:0]
    .din_valid    (spd_dac_din_valid),        // I
    .din_ready    (spd_dac_din_ready),        // O
    
    .sclk        (spd_dac_sck),                // O
    .scs_n        (spd_dac_scs),                // O
    .latch_en    (),                            // O
    .sdo        (spd_dac_sdo)                // O

);

// ---------------------------------------------------------------------------------
//  Digital Potential Meter(AD5252) I2C Port
// --------------------------------------------------------------------------------


i2c_intf #(
    .SCLK_PERIOD(640),
    .CLK_START_END(320),
    .HIGH_CLK(160),
    .CLK_OFFSET(160),
    .CHIPSEL_WIDTH(8),
    .CONT_WIDTH(8),
    .RCHIPSEL_WIDTH(0),
    .DATA_WIDTH(8),
    .RW_FLAG_BIT(16),
    .DATA_NUM(1)
) i2c_intf_ad5252(
    .clk(sys_clk),                            // I
    .sclr(rst),                        // I
    
    .sclk        (dp_sck),        // O
    .sdo        (dp_sdo),        // O
    .sdi        (dp_sdi),        // I
    .iobuf_T    (dp_T),        // O //not used for 4line SPI
    
    .din        (dp_din),        // I [WORD_WIDTH-1:0]
    .din_valid    (dp_din_valid),    // I
    .dout        (dp_dout),        // O [WORD_WIDTH-1:0]
    .dout_valid    (),        // O
    .s_busy        ()
);
endmodule