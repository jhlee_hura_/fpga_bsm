`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: HURA
// Engineer: LEE Seong Yun (sylee@hura.co.kr)
//
// Create Date: 2019/01/04 11:05:02
// Design Name: 
// Module Name: con10_intf_charlie 
// Project Name: 
// Target Devices: Kintex 7 160T
// Tool Versions: Vivado 2018.1
// Description:
//
// Dependencies:
//
// Revision:
// 		build. 1 (2019/01/04)
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////
module con10_intf_charlie (

	output	wire 	    CON10B1_p,
	output	wire	    CON10B1_n,
	output	wire 	    CON10B2_p,
    output  wire        CON10B2_n,
	inout	wire [15:0]	CON10B3	
);

OBUFDS #(
	.IOSTANDARD("DEFAULT"), // Specify the output I/O standard
	.SLEW("FAST")			// Specify the output slew rate
) OBUFDS_inst0 (
	.O(CON10B1_p),		// Diff_p output (connect directly to top-level port)
	.OB(CON10B1_n),		// Diff_n output (connect directly to top-level port)
	.I(1'b1)				// Buffer input
);
OBUFDS #(
	.IOSTANDARD("DEFAULT"), // Specify the output I/O standard
	.SLEW("FAST")			// Specify the output slew rate
) OBUFDS_inst1 (
	.O(CON10B2_p),		// Diff_p output (connect directly to top-level port)
	.OB(CON10B2_n),		// Diff_n output (connect directly to top-level port)
	.I(1'b1)				// Buffer input
);

// ---------------------------------------------------------------------------------
// Assign CON1B3
// ---------------------------------------------------------------------------------

con10b3_iobuf_charlie con10b3_iobuf_charlie (
	.CON(CON10B3)		// IO [15:0]
	
);

endmodule