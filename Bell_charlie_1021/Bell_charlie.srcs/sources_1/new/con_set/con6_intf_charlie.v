`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: HURA
// Engineer: LEE Seong Yun (sylee@hura.co.kr)
//
// Create Date: 2019/01/04 11:05:02
// Design Name: 
// Module Name: con6_intf_charlie 
// Project Name: 
// Target Devices: Kintex 7 160T
// Tool Versions: Vivado 2018.1
// Description:
//
// Dependencies:
//
// Revision:
// 		build. 1 (2019/01/04)
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////
module con6_intf_charlie (
	input	wire	sys_clk,
    input    wire    sig_clk,
    input    wire    clk_200,
    input    wire    clk_600,
    input    wire    rst,
    
    
    output    wire [1:0]   CON6B1_p,
    output    wire [1:0]   CON6B1_n,    
    output    wire         CON6B2_p,
    output    wire         CON6B2_n,
    inout     wire [15:0]  CON6B3,
    
    
    // ---- Serdes ---
    input    wire         test_mode,
    input    wire [31:0]    din_test,
    
    input    wire        shape_load,            // set false path
    input wire [6:0]    i_offset_quotient,          // I [6:0]
    input wire [2:0]    i_offset_remainder,        // I [2:0]    
    input    wire [5:0]     pulse_width,        // set false path

    input    wire [7:0]     pulse_add_width,    
    
    // delay tap 78ps per tap (max 2.418ns)
    
    input wire [4:0]    delay_tap_in,        // set false path

    
    
    input    wire         din_pulse,
    input    wire         din_pulseadd,
    
    // ---- Enable  -----
    input    wire        hmc920_en,
    
    
    // ---- LD_ADC (LTC1867) ---
    input    wire [15:0]    ltc1867_din,
    input    wire        ltc1867_din_valid,
    output    wire        ltc1867_din_ready,
    output    wire [15:0]    ltc1867_dout,
    // ---- TEC DAC (MAX5144) ---
    input    wire        tec_shdn_b,
    input    wire        tec_clr,
    input    wire [15:0]    tec_dac_din,
    input    wire        tec_dac_din_valid,
    output    wire        tec_dac_din_ready,
    // ---- LD DAC (AD5761) ---
    input    wire [23:0]    ad5761_dac_din,
    input    wire        ad5761_dac_din_valid,
    output    wire        ad5761_dac_din_ready,
    output    wire [23:0]    ad5761_dac_dout,
    
    // ---- DP (AD5252) ---
    input    wire [23:0]    dp_din,
    input    wire        dp_din_valid,
    output    wire [7:0]    dp_dout
);

OBUFDS #(
.IOSTANDARD("DEFAULT"), // Specify the output I/O standard
.SLEW("FAST")            // Specify the output slew rate
) OBUFDS_inst1 (
.O(CON6B1_p[1]),        // Diff_p output (connect directly to top-level port)
.OB(CON6B1_n[1]),        // Diff_n output (connect directly to top-level port)
.I(1'b1)                // Buffer input
);

OBUFDS #(
.IOSTANDARD("DEFAULT"), // Specify the output I/O standard
.SLEW("FAST")            // Specify the output slew rate
) OBUFDS_inst2 (
.O(CON6B2_p),        // Diff_p output (connect directly to top-level port)
.OB(CON6B2_n),        // Diff_n output (connect directly to top-level port)
.I(1'b1)                // Buffer input
);

laser_trig_gen_1200_b laser_trig_gen_1200 (
    .sig_clk            (sig_clk),          // I
    .clk_200            (clk_200),        // I
    .clk_600        (clk_600),    // I
    .sclr            (rst),            // I
    
    
    .delay_tap_in    (delay_tap_in),        // I [4:0]
    
    .shape_load            (shape_load),    // I
    .i_offset_quotient      (i_offset_quotient),    // I [6:0]
    .i_offset_remainder    (i_offset_remainder),    // I [2:0]    
    .pulse_width        (pulse_width),    // I [5:0]
    .pulse_add_width    (pulse_add_width),    // I [7:0]
    
    .test_mode        (test_mode),    // I
    
    .din_test        (din_test),        // I [31:0]

    
    .din_pulseadd    (din_pulseadd),        // I
    .din_pulse        (din_pulse),        // I [31:0]
    
    .dout_p            (CON6B1_p[0]),        // O
    .dout_n            (CON6B1_n[0]),        // O
    
    
    .delay_tap_out    ()    // O [4:0]

);

// ---------------------------------------------------------------------------------
// Assign CON1B3
// ---------------------------------------------------------------------------------
wire    ltc1867_sck;
wire    ltc1867_scs;
wire    ltc1867_sdo;
wire    ltc1867_sdi;
wire    tec_dac_sck;
wire    tec_dac_scs;
wire    tec_dac_sdo;
wire    ad5761_dac_sck;
wire    ad5761_dac_scs;
wire    ad5761_dac_sdo;
wire    ad5761_dac_sdi;
wire    dp_sck;
wire    dp_sdo;
wire    dp_sdi;
wire     dp_T;
con6b3_iobuf_charlie con6b3_iobuf_charlie (
.CON(CON6B3),        // IO [15:0]

// LTC 1867
.ltc1867_sck(ltc1867_sck),            // I
.ltc1867_scs(ltc1867_scs),            // I
.ltc1867_sdo(ltc1867_sdo),            // I
.ltc1867_sdi(ltc1867_sdi),            // O
// DAC MAX5144 for LD1530_TEC
.tec_dac_sck(tec_dac_sck),        // I
.tec_dac_scs(tec_dac_scs),        // I
.tec_dac_sdo(tec_dac_sdo),        // I
.tec_clr(tec_clr),                // I
.tec_shdn_b(tec_shdn_b),            // I
// DAC AD5761 for LD1530
.ad5761_dac_sck(ad5761_dac_sck),        // I
.ad5761_dac_scs(ad5761_dac_scs),        // I
.ad5761_dac_sdo(ad5761_dac_sdo),        // I
.ad5761_dac_sdi(ad5761_dac_sdi),        // O
// Enable
.hmc920_en(hmc920_en),        // I

// [DP] AD5252  
.dp_sck(dp_sck),            // I
.dp_sdi(dp_sdi),            // O
.dp_sdo(dp_sdo),            // I
.dp_T(dp_T)            // IO
);


// ---------------------------------------------------------------------------------
//  LTC1867 Converter SPI Port
// ---------------------------------------------------------------------------------
spi_intf #(
.SCLK_PERIOD(16),
.CLK_START_END(8),
.HIGH_CLK(8),
.CLK_OFFSET(4),
.CONT_WIDTH(0),
.DATA_WIDTH(16),
.RW_FLAG_BIT(0),
.SCS_N_MODE(0),
.SCLK_IDLE_MODE(0)
)
spi_intf_ltc1867(
.clk(sys_clk),                            // I
.sclr(rst),                        // I

.sclk        (ltc1867_sck),            // O
.scs_n        (ltc1867_scs),            // O
.sdo        (ltc1867_sdo),            // O
.sdi        (ltc1867_sdi),            // I
.iobuf_T    (),                        // O //not used for 4line SPI

.din        (ltc1867_din),            // I [WORD_WIDTH-1:0]
.din_valid    (ltc1867_din_valid),    // I
.din_ready    (ltc1867_din_ready),    // O
.dout        (ltc1867_dout),            // O [WORD_WIDTH-1:0]
.dout_valid    ()                        // O
);

// ---------------------------------------------------------------------------------
//  LD1530_TEC MAX5144 DAC Shift Reg Port
// ---------------------------------------------------------------------------------
shiftreg_intf #(
.SCLK_PERIOD(16),
.CLK_START_END(8),
.HIGH_CLK(8),
.CLK_OFFSET(4),
.DATA_WIDTH(16),
.SCS_N_MODE(0),
.SCLK_IDLE_MODE(1)
) spi_intf_tec(
.clk(sys_clk),                                    // I
.sclr(rst),                                // I

.din        (tec_dac_din),            // I [DATA_WIDTH-1:0]
.din_valid    (tec_dac_din_valid),        // I
.din_ready    (tec_dac_din_ready),        // O

.sclk        (tec_dac_sck),            // O
.scs_n        (tec_dac_scs),            // O
.latch_en    (),                                // O
.sdo        (tec_dac_sdo)            // O
);

// ---------------------------------------------------------------------------------
//  LD1530 AD5761 SPI Port - LD1530 Bias Voltage Vari.
// ---------------------------------------------------------------------------------
spi_intf #(
.SCLK_PERIOD(16),
.CLK_START_END(8),
.HIGH_CLK(4),
.CLK_OFFSET(4),
.CONT_WIDTH(0),
.DATA_WIDTH(24),
.RW_FLAG_BIT(0),
.SCS_N_MODE(0),
.SCLK_IDLE_MODE(1)
) spi_intf_ad5761(
.clk(sys_clk),                            // I
.sclr(rst),                        // I

.sclk        (ad5761_dac_sck),        // O
.scs_n        (ad5761_dac_scs),        // O
.sdo        (ad5761_dac_sdo),        // O
.sdi        (ad5761_dac_sdi),        // I
.iobuf_T    (),                        // O //not used for 4line SPI

.din        (ad5761_dac_din),        // I [WORD_WIDTH-1:0]
.din_valid    (ad5761_dac_din_valid),    // I
.din_ready    (ad5761_dac_din_ready),    // O
.dout        (ad5761_dac_dout),        // O [WORD_WIDTH-1:0]
.dout_valid    ()                        // O
);
// ---------------------------------------------------------------------------------
//  Digital Potential Meter(AD5252) I2C Port
// --------------------------------------------------------------------------------


i2c_intf #(
.SCLK_PERIOD(640),
.CLK_START_END(320),
.HIGH_CLK(160),
.CLK_OFFSET(160),
.CHIPSEL_WIDTH(8),
.CONT_WIDTH(8),
.RCHIPSEL_WIDTH(0),
.DATA_WIDTH(8),
.RW_FLAG_BIT(16),
.DATA_NUM(1)
) i2c_intf_ad5252(
.clk(sys_clk),                            // I
.sclr(rst),                        // I

.sclk        (dp_sck),        // O
.sdo        (dp_sdo),        // O
.sdi        (dp_sdi),        // I
.iobuf_T    (dp_T),        // O //not used for 4line SPI

.din        (dp_din),        // I [WORD_WIDTH-1:0]
.din_valid    (dp_din_valid),    // I
.dout        (dp_dout),        // O [WORD_WIDTH-1:0]
.dout_valid    (),        // O
.s_busy        ()
);    

endmodule