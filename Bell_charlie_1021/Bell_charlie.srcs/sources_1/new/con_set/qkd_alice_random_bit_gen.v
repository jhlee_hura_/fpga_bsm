`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: HURA
// Engineer: LEE Seong Yun (sylee@hura.co.kr)
//
// Create Date: 2019/01/04 11:05:02
// Design Name: QKD
// Module Name: qkd_alice_random_bit_gen
// Project Name: QKD
// Target Devices: Kintex 7 160T
// Tool Versions: Vivado 2018.1
// Description:
//
// Dependencies:
//
// Revision:
// 		build. 1 (2019/01/04)
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////
module qkd_alice_random_bit_gen (
	input wire 			clk,
	input wire 			sclr,
	input wire [31:0]	seed,
	input wire			seed_valid,
	input wire [7:0]	thres1,		// lower thres
	input wire [7:0]	thres2,
	input wire			rd_en,

	output wire [1:0] 	sig_bit,
	output wire			phase_bit,
	output wire			data_bit,
	output wire			dov_bit
);

wire [63:0] do_lfsr;

assign sig_bit = 	(do_lfsr[9:2] < thres1) ? 2'b00 :
					(do_lfsr[9:2] < thres2) ? 2'b01 :
					2'b10;
assign phase_bit	= do_lfsr[50];
assign data_bit		= do_lfsr[0];

lfsr #(
	.NBIT(64)
) lfsr (
	.clk(clk),					// I
	.sclr(sclr),				// I
	.en(rd_en),					// I
	.seed({32'hffffffff,seed}),				// I [NBIT-1:0]
	.seed_valid(seed_valid),	// I
	.dout(do_lfsr),				// O [NBIT-1:0]
	.dout_valid(dov_bit)		// O
);

endmodule