`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: HURA
// Engineer: LEE Seong Yun (sylee@hura.co.kr)
//
// Create Date: 2019/01/04 11:05:02
// Design Name: QKD Alice
// Module Name: QKD_Alice
// Project Name: QKD
// Target Devices: Kintex 7 160T
// Tool Versions: Vivado 2018.1
// Description:
//
// Dependencies:
//
// Revision:
// 		build. 1 (2019/01/04)
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////
module qkd_operation (
	input wire		clk, 	// sig_clk,
	input wire		sclr,

	input wire		sig_sync,
	input wire		start,
	input wire		stop,
	input wire	[17:0]	sig_number,

	input wire [31:0]	seed,
	input wire			seed_valid,
	input wire	[7:0]	thres1,
	input wire	[7:0]	thres2,

	input wire			sig_test_mode,
	input wire [31:0]	sig0_test_data,
	input wire [31:0]	sig1_test_data,
	input wire			pm_test_mode,
	input wire [31:0]	pm_test_data,
	input wire			d_test_mode,
	input wire [31:0]	d_test_data,

	output reg [31:0]	sig1,
	output reg [31:0]	sig2,
(* mark_debug = "true" *)	output reg [31:0]	ph,
	output reg [31:0]	data,
(* mark_debug = "true" *)	output reg			op_ready,
(* mark_debug = "true" *)	output reg 			dout_last,
(* mark_debug = "true" *)	output reg			dout_start,
(* mark_debug = "true" *)	output reg			dout_valid,
(* mark_debug = "true" *)	output wire			busy,
(* mark_debug = "true" *)	input  wire			dout_ready
);

// MSB First
// random bit�? 10 Mbps�? ?��비되�?�? 100MHz�? ?��?��?���? ?���? 문제?��?��.
reg [31:0]	r_sig1;
reg [31:0]	r_sig2;
reg [31:0]	r_ph;
reg [31:0]	r_data;
reg [31:0]	p_sig1;
reg [31:0]	p_sig2;
(* mark_debug = "true" *)reg [31:0]	p_ph;
reg [31:0]	p_data;
(* mark_debug = "true" *)reg [13:0]	sig_cnt;

(* mark_debug = "true" *)reg			buffer_full;
(* mark_debug = "true" *)reg			word_valid;
(* mark_debug = "true" *)reg			start_flag;
(* mark_debug = "true" *)reg			stop_flag;
wire [13:0] sig_number_32b;

assign sig_number_32b = (sig_number[4:0] == 5'b0) ? {1'b0,sig_number[17:5]} : sig_number[17:5] + 1'b1;



localparam WAIT_CNT = 'd120;
localparam IDLE_STAT = 2'b00;
localparam READY_STAT = 2'b01;
localparam RUN_STAT = 2'b10;
(* mark_debug = "true" *)reg [1:0]	stat;
(* mark_debug = "true" *)reg 		first_flag;
(* mark_debug = "true" *)reg [7:0] wait_cnt;

assign		busy = (stat != IDLE_STAT) | dout_last;
always @(posedge clk) begin
	if (sclr) begin
		sig1	<= 'b0;
		sig2	<= 'b0;
		ph		<= 'b0;
		data	<= 'b0;
		buffer_full <= 1'b0;
		dout_valid <= 1'b0;
		sig_cnt  <= 'b0;
		start_flag	<= 1'b0;
		stop_flag	<= 1'b0;
		dout_start	<= 1'b0;
		dout_last	<= 1'b0;
		first_flag	<= 1'b0;
		wait_cnt	<= 8'b0;
		op_ready	<= 1'b0;
		stat		<= IDLE_STAT;
	end
	else begin
		case (stat)
			IDLE_STAT : begin
				dout_valid 	<= 1'b0;
				sig_cnt		<= 'b0;
				sig1		<= 32'b0;
				sig2		<= 32'b0;
				ph			<= 32'b0;
				data		<= 32'b0;
				buffer_full	<= 1'b0;
				start_flag	<= 1'b0;
				stop_flag	<= 1'b0;
				dout_start	<= 1'b0;
				dout_last	<= 1'b0;
				first_flag	<= 1'b0;
				wait_cnt	<= 8'b0;
				op_ready	<= start;
				stat		<= (start)	? READY_STAT : IDLE_STAT;
			end
			READY_STAT : begin
				op_ready	<= 1'b0;
				wait_cnt	<= (wait_cnt > WAIT_CNT) ? wait_cnt : wait_cnt + 1'b1;
				start_flag	<= 1'b1;
				first_flag	<= 1'b1;
				buffer_full <=	(word_valid) ? 1'b1 : buffer_full;
				stat		<= 	((wait_cnt > WAIT_CNT) && buffer_full && sig_sync) ? RUN_STAT :
								(stop) ? IDLE_STAT : READY_STAT;
			end
			RUN_STAT : begin
				wait_cnt	<= 8'b0;
				stop_flag <= (stop) ? 1'b1 : stop_flag;
				if (buffer_full && dout_ready) begin
					dout_start	<= (start_flag) ? 1'b1 : 1'b0;
					dout_last	<= (sig_cnt >= (sig_number_32b-1'b1) || stop_flag || stop) ? 1'b1 : 1'b0;
					start_flag	<= 1'b0;
					sig_cnt		<= sig_cnt + 1'b1;
					first_flag	<= 1'b0;
					sig1		<= (first_flag) ? 32'hffff_ffff : p_sig1;
					sig2		<= (first_flag) ? 32'h0 : p_sig2;
					ph			<= (first_flag) ? 32'hffff_ffcd : p_ph;
					data		<= (first_flag) ? 32'hffff_ffff : p_data;
					buffer_full <= 1'b0;
					dout_valid	<= 1'b1;
					stat		<= (sig_cnt >= (sig_number_32b-1'b1) || stop_flag || stop) ? IDLE_STAT : RUN_STAT;
				end
				else begin
					buffer_full <=	(word_valid) ? 1'b1 : buffer_full;
					dout_last	<= 1'b0;
					dout_start	<= 1'b0;
					dout_valid	<= 1'b0;
					stat		<= RUN_STAT;
				end

			end
		endcase
	end
end

(* mark_debug = "true" *)reg 		rd_en;
(* mark_debug = "true" *)wire [1:0] 	sig_bit;
(* mark_debug = "true" *)wire		phase_bit;
(* mark_debug = "true" *)wire		data_bit;
//wire		dov_bit;
(* mark_debug = "true" *)reg [4:0]	k = 5'h1f;
always @(posedge clk) begin
	if (sclr) begin
		k		<= 5'h1f;
		r_sig1	<= 'b0;
		r_sig2	<= 'b0;
		r_ph	<= 'b0;
		r_data	<= 'b0;
		p_sig1	<= 'b0;
		p_sig2	<= 'b0;
		p_ph	<= 'b0;
		p_data	<= 'b0;
		rd_en	<= 1'b0;
		word_valid	<= 1'b0;
	end
	else begin
		if (k == 'b0 || start) begin
			word_valid <= 1'b0;
			p_sig1	<= r_sig1;
			p_sig2	<= r_sig2;
			p_ph	<= r_ph;
			p_data	<= r_data;

			k 		<= 5'h1f;
			rd_en 	<= 1'b1;
			r_sig1 	<= sig_bit[0];
			r_sig2 	<= sig_bit[1];
			r_ph 	<= phase_bit;
			r_data 	<= data_bit;


		end
		else if (k == 'b1) begin
			word_valid	<= (buffer_full) ? 1'b0 : 1'b1;
			k 			<= (buffer_full) ? 'b1 : 'b0;
			rd_en 		<= (buffer_full) ? 1'b0 : 1'b1;
			r_sig1		<= (buffer_full) ? r_sig1 :  {r_sig1[30:0], sig_bit[0]};
			r_sig2		<= (buffer_full) ? r_sig2 :  {r_sig2[30:0], sig_bit[1]};
			r_ph		<= (buffer_full) ? r_ph	  :  {r_ph[30:0], phase_bit};
			r_data		<= (buffer_full) ? r_data :  {r_data[30:0], data_bit};


		end
		else begin
			word_valid <= 1'b0;
			rd_en 	<= 1'b1;
			k 		<= k - 1'b1 ;
			r_sig1	<= {r_sig1[30:0], sig_bit[0]};
			r_sig2	<= {r_sig2[30:0], sig_bit[1]};
			r_ph	<= {r_ph[30:0], phase_bit};
			r_data	<= {r_data[30:0], data_bit};
		end
	end
end


wire [1:0]	test_sig_bit;
wire [1:0]	random_sig_bit;
wire 		test_phase_bit;
wire 		random_phase_bit;
wire 		test_data_bit;
wire 		random_data_bit;

assign sig_bit = (sig_test_mode) ? test_sig_bit : random_sig_bit;
assign phase_bit = (pm_test_mode) ? test_phase_bit : random_phase_bit;
assign data_bit = (d_test_mode) ? test_data_bit : random_data_bit;


qkd_alice_random_bit_gen qkd_alice_random_bit_gen (
	.clk(clk),				// I
	.sclr(sclr),			// I
	.seed(seed),			// I [31:0]
	.seed_valid(seed_valid),		// I
	.thres1(thres1),			// I [7:0]	// lower thres
	.thres2(thres2),			// I [7:0]
	.rd_en(rd_en),			// I

	.sig_bit(random_sig_bit),			// O [1:0]
	.phase_bit(random_phase_bit),		// O
	.data_bit(random_data_bit),		// O
	.dov_bit()					// O
); // latency = 1


qkd_alice_test_bit_gen qkd_alice_test_bit_gen (
	.clk(clk),							// I
	.sclr(sclr),						// I
	.load(op_ready),						// I
	.sig0_test_data(sig0_test_data),	// I [31:0]
	.sig1_test_data(sig1_test_data),	// I [31:0]
	.pm_test_data(pm_test_data),		// I [31:0]
	.d_test_data(d_test_data),			// I [31:0]
	.rd_en(rd_en),						// I

	.sig_bit(test_sig_bit),				// O [1:0]
	.phase_bit(test_phase_bit),			// O
	.data_bit(test_data_bit),			// O
	.dov_bit()							// O
);



endmodule