`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: HURA
// Engineer: LEE Seong Yun (sylee@hura.co.kr)
//
// Create Date: 2019/01/04 11:05:02
// Design Name: 
// Module Name: con9_intf_charlie 
// Project Name: 
// Target Devices: Kintex 7 160T
// Tool Versions: Vivado 2018.1
// Description:
//
// Dependencies:
//
// Revision:
// 		build. 1 (2019/01/04)
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////
module con9_intf_charlie (
	// to Con 9
	input	wire     	CON9B1_p,
	input	wire     	CON9B1_n,
	output	wire		ADC_A_CLK_p,
	output	wire		ADC_A_CLK_n,
	input	wire		A_DCO_p,
	input	wire		A_DCO_n,
	inout	wire [15:0]	CON9B3
);

assign	CON9B3 = 'b0;

OBUFDS #(
	.IOSTANDARD("DEFAULT"), // Specify the output I/O standard
	.SLEW("FAST")			// Specify the output slew rate
) OBUFDS_inst (
	.O(ADC_A_CLK_p),		// Diff_p output (connect directly to top-level port)
	.OB(ADC_A_CLK_n),		// Diff_n output (connect directly to top-level port)
	.I(1'b1)				// Buffer input
);


endmodule