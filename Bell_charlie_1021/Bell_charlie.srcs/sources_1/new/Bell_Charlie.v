`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: HURA
// Engineer: LEE Seong Yun (sylee@hura.co.kr)
//
// Create Date: 2019/01/04 11:05:02
// Design Name: 
// Module Name: 
// Project Name: BSM
// Target Devices: Kintex 7 160T
// Tool Versions: Vivado 2018.1
// Description:
//
// Dependencies:
//
// Revision:
// 		build. 1 (2019/01/04)
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////
module Bell_Charlie(

	//------------- PCI Express ---------------
 	input	wire        pcie_ref_clk_p,
	input   wire        pcie_ref_clk_n,
	output  wire [3:0]  pci_exp_txp,
	output  wire [3:0]  pci_exp_txn,
	input   wire [3:0]  pci_exp_rxp,
	input   wire [3:0]  pci_exp_rxn,
	
 	input	wire        OCXO_p,
    input   wire        OCXO_n,
	// to CON1
	input	 wire    CON1B1_p0,
    input    wire    CON1B1_n0,
    output   wire    CON1B1_p1,
    output   wire    CON1B1_n1,
    input    wire    CON1B1_p2,
    input    wire    CON1B1_n2,     
    output   wire    CON1B2_p,
    output   wire    CON1B2_n,

	inout	wire [21:0]	CON1B3,

	// to CON2
	input	 wire    CON2B1_p0,
    input    wire    CON2B1_n0,
    output   wire    CON2B1_p1,
    output   wire    CON2B1_n1,
    input    wire    CON2B1_p2,
    input    wire    CON2B1_n2, 
    output   wire    CON2B2_p,
    output   wire    CON2B2_n,

	inout	wire [21:0]	CON2B3,

	// to CON3
	input	 wire    CON3B1_p0,
    input    wire    CON3B1_n0,
    output   wire    CON3B1_p1,
    output   wire    CON3B1_n1,
    input    wire    CON3B1_p2,
    input    wire    CON3B1_n2, 
    output   wire    CON3B2_p,
    output   wire    CON3B2_n,

	inout	wire [21:0]	CON3B3,

	// to CON4
	input	 wire    CON4B1_p0,
    input    wire    CON4B1_n0,
    output   wire    CON4B1_p1,
    output   wire    CON4B1_n1,
    input    wire    CON4B1_p2,
    input    wire    CON4B1_n2, 
    output   wire    CON4B2_p,
    output   wire    CON4B2_n,

	inout	wire [21:0]	CON4B3,

	// to CON5
	output	wire 	     CON5B1_p,
    output  wire        CON5B1_n,
    output  wire        CON5B2_p,
    output  wire        CON5B2_n,
    inout   wire [15:0] CON5B3,
    
	// to CON6
	output	wire [1:0]	CON6B1_p,
    output  wire [1:0] CON6B1_n,
	output	wire		CON6B2_p,
	output	wire		CON6B2_n,
	inout	wire [15:0]	CON6B3,
	// to CON7
	output	wire		CON7B1_p,
    output  wire       CON7B1_n,
	output	wire		CON7B2_p,
	output	wire		CON7B2_n,
	inout	wire [15:0]	CON7B3,
	// to CON8
	output	wire		CON8B1_p0,
    output  wire       CON8B1_n0,
 	output	wire		CON8B1_p2,
    output  wire       CON8B1_n2,	
	inout	wire [15:0]	CON8B3,
	// to CON9
	input	wire     	CON9B1_p,
	input	wire     	CON9B1_n,
	output	wire		ADC_A_CLK_p,
	output	wire		ADC_A_CLK_n,
	input	wire		A_DCO_p,
	input	wire		A_DCO_n,
	inout	wire [15:0]	CON9B3,

	// to CON10
    output  wire       CON10B1_p,
    output  wire       CON10B1_n,
    output  wire       CON10B2_p,
    output  wire       CON10B2_n,
	inout	wire [15:0]	CON10B3,

	// Fan control
	output	wire		FAN_CTRL,       // o
	output	wire		FAN_CTRL_MAIN,  // o

	// Temp. Sensor (LM 70)
	output	wire		TMP_MOSI,       // o
	input	wire		TMP_MISO,       // o
	output	wire		TMP_SCS,        // o
	output	wire		TMP_SCK,        // o
                                        // o
	// LED
	output 	wire		LED_FRONT1,		// ok
	output 	wire		LED_FRONT2,		// ok
	output 	wire		LED_FRONT3,		// ok

	// Array LED
	output	wire [7:0]	GPIO_LED,		// ok

	// 10 MHz Clk
	output	wire		F_CLK_3_3,	// alice	// ok
//	input	wire		F_CLK_3_3,	// bob

	output 	wire [7:0]	GPIO_3_3_PIN,		// ok
	output	wire [4:0]	GPIO_3_3_SMA,       // ok
	output 	wire [9:0]	GPIO_1_8_PIN,       // ok
	input   wire        GPIO_1_8_SMA_REF,
    output  wire [3:0]  GPIO_1_8_SMA,       // ok

	output	wire		PD_OSC_EN,					// ok
	input	wire		PD_OSC_p,		// 100MHz	// ok
	input	wire		PD_OSC_n,					// ok

	// to CPLD
	input	wire [7:0]	FGPIO
);

// (* mark_debug = "true" *)
// ---------------------------------------------------------------------------------
// do not assignd pins
assign GPIO_3_3_PIN[7:0] = 8'b0;

//assign GPIO_1_8_PIN[9:0] = 10'b0;


// ---------------------------------------------------------------------------------
// No change Pins
assign PD_OSC_EN = 1'b1;

// ---------------------------------------------------------------------------------
// Reset buffer
// ---------------------------------------------------------------------------------
 wire 	grst;
 wire  back_rst_n;
 wire  sys_clk;
    IBUF    hw_rst_n_ibuf (.O(back_rst_n), .I(FGPIO[0]));
    
    pls_expander #(
        .COUNT(10),
        .POLARITY(1)
    ) grst_exp(
        .sin(~back_rst_n),        // ~(fpga_rst_n & back_rst_n)
        .clk(sys_clk),
        .sout(grst)
    );

// ---------------------------------------------------------------------------------
// Clock
// ---------------------------------------------------------------------------------
   // --- External 1_8 EXT SMA 10 MHz -----
wire    _18SMA_REF_CLK_IBUF;
 IBUF #(
       .IBUF_LOW_PWR("TRUE"),  // Low power (TRUE) vs. performance (FALSE) setting for referenced I/O standards
       .IOSTANDARD("DEFAULT")  // Specify the input I/O standard 
     ) i_18SMA_REF_CLK_ibuf (
     .I (GPIO_1_8_SMA_REF),   
     .O (_18SMA_REF_CLK_IBUF) // 10MHz
     );
wire    input_clk_ext_sma_stopped;
wire    clk_ext_sma_locked;
wire    clk_ext_sma;
wire    locked;
 clk_wiz_a clk_wiz_ext_sma
 (
     // Clock out ports
     .clk_out1(clk_ext_sma),     // output clk_out1
     // Status and control signals
     .reset(grst), // input reset
     .input_clk_stopped(input_clk_ext_sma_stopped), // output input_clk_stopped
     .locked(locked),       // output locked
    // Clock in ports
     .clk_in1(_18SMA_REF_CLK_IBUF));    // input clk_in1_n    
 
 assign clk_ext_sma_locked = ~input_clk_ext_sma_stopped & locked;    


 // --- External OCXO 100MHz  -----
/*
wire    _EXT100MHz_BUF;
IBUFDS #(
   .DIFF_TERM("FALSE"),       // Differential Termination
   .IBUF_LOW_PWR("TRUE"),     // Low power="TRUE", Highest performance="FALSE" 
   .IOSTANDARD("DEFAULT")     // Specify the input I/O standard
) IBUFDS_inst (
   .O(_EXT100MHz_BUF),  // Buffer output
   .I(OCXO_p),  // Diff_p buffer input (connect directly to top-level port)
   .IB(OCXO_n) // Diff_n buffer input (connect directly to top-level port)
);
*/
wire		pcie_clk;
wire clk_100_pci;
wire clk_clk_100_pci_locked;
clk_wiz_f clk_int_pci (
    .clk_out1(clk_100_pci),            // O
    .reset(1'b0),                    // I
    .locked(clk_clk_100_pci_locked),        // O
    .clk_in1(pcie_clk)

    );  

 // ---  Sig clk MUX - PCIE clock or 1_8 EXT SMA 10 MHz

reg      clk_in_sel = 1'b1;
wire    clk_sel_rst;
wire    sig_clk;
wire     clk_sig_locked;
wire    clk_200; // 200 MHz
wire    clk_600;
 clk_wiz_e clk_sig_mux (
     .clk_in2(clk_ext_sma),            // I
     .clk_in_sel(clk_in_sel),        // I 1: clk_in1, 0: clk_in2
     .clk_out1(sig_clk),             // O
     .clk_out2(clk_200),            // O
     .clk_out3(clk_600),            // O                 
     .reset(grst | clk_sel_rst),     // I
     .locked(clk_sig_locked),                       // O
     .clk_in1(clk_100_pci)           // I
     );         
              // ---  Need delay time and reset for changing REF CLK   
             reg     clk_in_sel_old;
             reg        clk_sel_proc;
         
             always @(posedge sys_clk) begin
                 if (grst) begin
                     clk_in_sel <= 1'b1;
                     clk_in_sel_old <= 1'b1;
                 end
                 else begin
                     clk_in_sel <= (clk_ext_sma_locked) ? 1'b0 : 1'b1;
                     clk_in_sel_old <= clk_in_sel;
                 end
             end    
             
             pls_expander #(
                     .COUNT(12),
                     .POLARITY(1)
                 ) c2h_0_rst_ext(
                     .sin(clk_in_sel != clk_in_sel_old),
                     .clk(sys_clk),
                     .sout(clk_sel_rst)
                 );
                 
wire		pcie_link_up;
wire		clk_sys_locked;

assign      sys_clk=pcie_clk;
assign      clk_sys_locked = pcie_link_up;

// --------------------------------------------------------------------------------
// 10M Clock Generation
// ---------------------------------------------------------------------------------
reg [3:0]	sig_clk_cnt = 'b0;
reg 		sig_clk_10 = 1'b0;
reg			sig_clk_10M_pulse = 1'b0;


always @(posedge sig_clk) begin
	sig_clk_cnt <= (sig_clk_cnt == 4'd9) ? 4'b0 : sig_clk_cnt + 1'b1;

	if (sig_clk_cnt == 4'b0) begin
		sig_clk_10M_pulse <= 1'b1;
		sig_clk_10 <= 1'b1;
	end
	else if (sig_clk_cnt == 4'd5) begin
		sig_clk_10M_pulse <= 1'b0;
		sig_clk_10 <= 1'b0;
	end
	else begin
		sig_clk_10 <= sig_clk_10;
		sig_clk_10M_pulse <= 1'b0;
	end
end

// ---------------------------------------------------------------------------------
// sync for differentr clock domain
// ---------------------------------------------------------------------------------
reg			sig_sync = 1'b0;
reg [6:0]	sig_clk_sync_cnt = 'b0;
always @(posedge sig_clk) begin
	sig_clk_sync_cnt <= (sig_clk_sync_cnt == 7'd79) ? 7'b0 : sig_clk_sync_cnt + 1'b1;
	sig_sync <= (sig_clk_sync_cnt == 7'b0) ? 1'b1 : 1'b0;
end

(* mark_debug = "true" *)reg shape_ld;

wire c1_shape_load;
wire c2_shape_load;
wire c3_shape_load;
wire c4_shape_load;
wire c7_shape_load;
wire c6_shape_load;

(* mark_debug = "true" *)wire shape_load;
assign shape_load = c1_shape_load | c2_shape_load | c3_shape_load |c4_shape_load  ;

reg shape_load_r;
always @(posedge sig_clk) begin
	shape_load_r <=	(shape_load) ? 1'b1 :
					(sig_sync) ? 1'b0 :
					shape_load_r;

	shape_ld <= shape_load_r & sig_sync;
end

// sync for trigger module
(* mark_debug = "true" *)reg shape_ld_trig;
(* mark_debug = "true" *)wire shape_load_trig;
assign shape_load_trig = c6_shape_load | c7_shape_load ;

reg shape_load_trig_r;
always @(posedge sig_clk) begin
	shape_load_trig_r <=	(shape_load_trig) ? 1'b1 :
					(sig_sync) ? 1'b0 :
					shape_load_trig_r;

	shape_ld_trig <= shape_load_trig_r & sig_sync;
end


// ---------------------------------------------------------------------------------
// LED Buffers
// ---------------------------------------------------------------------------------
wire [7:0]	array_led;
wire		f_led1;
wire		f_led2;
wire		f_led3;

assign GPIO_LED = ~array_led[7:0];

OBUF   front_led_1_obuf (.O(LED_FRONT1), .I(~f_led1));		// FPGA & DDR init & DDR POWER
OBUF   front_led_2_obuf (.O(LED_FRONT2), .I(~f_led2));		// user design part
OBUF   front_led_3_obuf (.O(LED_FRONT3), .I(~f_led3));		// Fault user design part


assign array_led[4:3] = 'b0;


reg [25:0]	clock_count='b0;
always @(posedge sys_clk)
	clock_count <= clock_count + 26'b1;
assign array_led[0]	= clock_count[25];		// 125MHz

reg [25:0]	sig_clk_count='b0;
always @(posedge sig_clk)
	sig_clk_count <=sig_clk_count + 26'b1;
assign array_led[1]	= sig_clk_count[25];

reg [25:0]	clk_200_count='b0;
always @(posedge clk_200)
	clk_200_count <=clk_200_count + 26'b1;
assign array_led[2]	= clk_200_count[25];

assign array_led[5] = clk_sig_locked;
assign array_led[6] = clk_sys_locked;
assign array_led[7] =clk_ext_sma_locked;
wire led_test;
wire led1_on;
wire led2_on;
wire led3_on;
assign f_led1 = led1_on ; //(clk_sys_locked && clk_sig_locked);
assign f_led2 = (led_test) ? led2_on : clock_count[25];	//trig_led;
assign f_led3 = (led_test) ? led3_on : pcie_link_up;

// ---------------------------------------------------------------------------------
//	board peripheral (lm 70)
// ---------------------------------------------------------------------------------
wire [15:0] lm70_spi_rd;
wire		lm70_spi_valid;
wire		lm70_din_ready;
wire [15:0]	fan_high_dura;
wire [15:0]	fan_low_dura;
wire [15:0]	fan1_high_dura;
wire [15:0]	fan1_low_dura;

board_perip_intf board_perip_intf(
	.clk			(sys_clk),
	.sclr			(grst),

	// LM70 Temp Sensor
	.lm70_sck		(TMP_SCK),			// O
	.lm70_scs		(TMP_SCS),			// O
	.lm70_sdo		(TMP_MOSI),			// O
	.lm70_sdi		(TMP_MISO),			// I

	.lm70_din_valid	(lm70_spi_valid),	// I
	.lm70_din_ready	(lm70_din_ready),	// O
	.lm70_dout		(lm70_spi_rd),		// O [15:0]

	// FAN MAIN Control
	.fan_main_ctrl		(FAN_CTRL_MAIN),	// O

	.fan_main_high_dura	(fan1_high_dura),	// I [15:0]
	.fan_main_low_dura	(fan1_low_dura),	// I [15:0]

	// FAN Control
	.fan_ctrl		(FAN_CTRL),			// O

	.fan_high_dura	(fan_high_dura),	// I [15:0]
	.fan_low_dura	(fan_low_dura)		// I [15:0]
);

// ---------------------------------------------------------------------------------
// START
// ---------------------------------------------------------------------------------
(* mark_debug = "true" *)wire 		op_start;


(* mark_debug = "true" *)reg sig_start_flag;
always @(posedge sig_clk) begin
	sig_start_flag <= 	(op_start) ? 1'b1 :
						(sig_clk_10M_pulse) ? 1'b0 :
						sig_start_flag;

end

// START_FLAG is delayed for Trigger laser module
// ---------------------------------------------------------------------------------
wire [14:0] o_a6_delay_cnt;
wire    o_a6_delayed_flag;
pulse_delay pulse_delay_con6_trig (
    .clk(sig_clk), // I
    .sclr(grst), // I
    .i_delay_cnt(o_a6_delay_cnt), // I [14:0]
    .i_start_pulse(sig_start_flag&sig_clk_10M_pulse), // I
    .i_ref_pulse(sig_clk_10M_pulse), // I
    .o_delayed_pulse(o_a6_delayed_flag) // O

);
wire [14:0] o_a7_delay_cnt;
wire    o_a7_delayed_flag;
pulse_delay pulse_delay_con7_trig (
    .clk(sig_clk), // I
    .sclr(grst), // I
    .i_delay_cnt(o_a7_delay_cnt), // I [14:0]
    .i_start_pulse(sig_start_flag&sig_clk_10M_pulse), // I
    .i_ref_pulse(sig_clk_10M_pulse), // I
    .o_delayed_pulse(o_a7_delayed_flag) // O

);
// ---------------------------------------------------------------------------------
//	SPAD Trig signal for log
// ---------------------------------------------------------------------------------
(* mark_debug = "true" *)wire [1:0] c_trig_type ;
(* mark_debug = "true" *)wire		c1_spd_trig;
(* mark_debug = "true" *)wire		c1_spd_trig_valid;
(* mark_debug = "true" *)wire		c1_mask_10M;

(* mark_debug = "true" *)wire		c2_spd_trig;
(* mark_debug = "true" *)wire		c2_spd_trig_valid;
(* mark_debug = "true" *)wire		c2_mask_10M;

(* mark_debug = "true" *)wire		c3_spd_trig;
(* mark_debug = "true" *)wire		c3_spd_trig_valid;
(* mark_debug = "true" *)wire		c3_mask_10M;

(* mark_debug = "true" *)wire		c4_spd_trig;
(* mark_debug = "true" *)wire		c4_spd_trig_valid;
(* mark_debug = "true" *)wire		c4_mask_10M;




wire [31:0] log32;
wire        log32_valid;
wire [127:0]log128;
wire        log128_valid;


wire [7:0]	emp_log;


(* mark_debug = "true" *)reg sig_start_log;
always @(posedge sig_clk) begin
	if (op_start)
		sig_start_log <= 1'b1;
	else
		sig_start_log <= (sig_clk_10M_pulse) ? 1'b0 : sig_start_log;
end


// ---------------------------------------------------------------------------------
//	Charlie Interface
// ---------------------------------------------------------------------------------

wire [31:0]    c1_spad_cnt1;
wire [31:0]    c1_spad_cnt2;
wire [31:0]    c1_spad_cnt3;
wire [31:0]    c1_spad_cnt4;

wire 			c1_test_mode;
wire [31:0]		c1_test_data;

wire [5:0]		c1_width;
wire [6:0]		c1_i_offset_quotient;
wire [2:0]		c1_i_offset_remainder;
wire [4:0]		c1_delay_tap;
wire [4:0]     c1_detect_delay_tap;
 
wire			c1_hmc920_en;
wire [15:0]		c1_ltc1867_spi_wr;
wire			c1_ltc1867_spi_valid;
wire			c1_ltc1867_din_ready;
wire [15:0]		c1_ltc1867_spi_rd;
wire			c1_tec_en;
wire			c1_tec_dac_clr;
wire [15:0]		c1_tec_dac_wr;
wire			c1_tec_dac_valid;
wire 			c1_sip_en;
wire [23:0]		c1_sip_dac_wr;
wire 			c1_sip_dac_valid;
wire 			c1_spd_en;
wire [23:0]		c1_spd_dac_wr;
wire 			c1_spd_dac_valid;
wire [23:0]    c1_dp_wr;
wire           c1_dp_valid;
wire [7:0]     c1_dp_rd;
// CON1 -> reg sig 
wire [31:0]		c1_spad_scnt;
wire [31:0]		c1_spad_dcnt;
wire [31:0]		c1_spad_ncnt;
wire [31:0]		c1_spad_orcnt;
assign c1_spad_scnt = c1_spad_cnt1; 
assign c1_spad_dcnt = c1_spad_cnt2; 
assign c1_spad_ncnt = c1_spad_cnt3; 
assign c1_spad_orcnt = c1_spad_cnt4; 

con1_intf_charlie con1_intf_charlie (
	.sys_clk	(sys_clk),		// I
	.sig_clk	(sig_clk),		// I
	.clk_200   (clk_200),      // I
	.clk_600    (clk_600),      // I
	
	.sig_10M   (sig_clk_10M_pulse), // I
	.rst		(grst),			// I

	// to Con 1
	.CON1B1_p0	(CON1B1_p0),		// I 
    .CON1B1_n0    (CON1B1_n0),        // I
    .CON1B1_p1    (CON1B1_p1),        // O
    .CON1B1_n1  (CON1B1_n1),        // O
    .CON1B1_p2    (CON1B1_p2),        // I 
    .CON1B1_n2  (CON1B1_n2),        // I  
	.CON1B2_p	(CON1B2_p),		// O
	.CON1B2_n	(CON1B2_n),		// O
	.CON1B3		(CON1B3),		// IO [21:0]
	
	// C_TRIG_TYPE_REG
	.trig_type			(c_trig_type),			// I [1:0]
	.spd_trig			(c1_spd_trig),			// O
	.spd_trig_valid	    (c1_spd_trig_valid),			// O
	.mask_10M		    (c1_mask_10M),	// O

	.spad_cnt1	(c1_spad_cnt1),		// O [31:0]
	.spad_cnt2	(c1_spad_cnt2),		// O [31:0]
	.spad_cnt3	(c1_spad_cnt3),		// O [31:0]	
	.spad_cnt4  (c1_spad_cnt4),    // O [31:0]		

	// ---- Serdes ---
	.test_mode		(c1_test_mode),		// I 			// sig_clk
    .din_test        (c1_test_data),        // I [31:0]        // sig_clk
	.shape_load		(shape_ld),			// I
	.pulse_width	(c1_width),			// I [5:0]
    .i_offset_quotient(c1_i_offset_quotient),   // I [6:0]     
    .i_offset_remainder(c1_i_offset_remainder), // I [2:0]

	// delay tap 78ps per tap (max 2.418ns)

	.delay_tap_in	(c1_delay_tap),		// I [4:0]
    .detect_delay_tap_in (c1_detect_delay_tap), // I [4:0]
   
    // ---- Enable  -----
    .hmc920_en        (c1_hmc920_en),            // I

    // ---- READ TEMP _ADC (LTC1867) ---
    .ltc1867_din        (c1_ltc1867_spi_wr),    // I [15:0]
    .ltc1867_din_valid    (c1_ltc1867_spi_valid),    // I
    .ltc1867_din_ready    (c1_ltc1867_din_ready),    // O
    .ltc1867_dout        (c1_ltc1867_spi_rd),    // O [15:0]
    // ---- MAX1968 Enable ---  
    .tec_shdn_b            (c1_tec_en),                // I
    // ---- TEC DAC (MAX5144) ---    
    .tec_clr            (c1_tec_dac_clr),            // I
    .tec_dac_din        (c1_tec_dac_wr),            // I [15:0]
    .tec_dac_din_valid    (c1_tec_dac_valid),        // I
    .tec_dac_din_ready    (),                        // O
    // ---- SIP DAC (AD5060) ---
    .f_sip_disable        (c1_sip_en),            // I
    .sip_dac_din        (c1_sip_dac_wr),        // I [23:0]
    .sip_dac_din_valid    (c1_sip_dac_valid),        // I
    .sip_dac_din_ready(),                        // O
    // ---- SPD DAC (AD5752A) ---
    .spd_en                (c1_spd_en),            // I
    .spd_dac_din        (c1_spd_dac_wr),        // I [23:0]
    .spd_dac_din_valid    (c1_spd_dac_valid),        // I
    .spd_dac_din_ready(),                        // O
    
    // ---- DP (AD5252) ---

    .dp_din(c1_dp_wr),                 // I [23:0]
    .dp_din_valid(c1_dp_valid),        // I
    .dp_dout(c1_dp_rd)                 // O [7:0]    	
);


wire [31:0]    c2_spad_cnt1;
wire [31:0]    c2_spad_cnt2;
wire [31:0]    c2_spad_cnt3;
wire [31:0]    c2_spad_cnt4;

wire 			c2_test_mode;
wire [31:0]		c2_test_data;

wire [5:0]		c2_width;
wire [6:0]		c2_i_offset_quotient;
wire [2:0]		c2_i_offset_remainder;
wire [4:0]		c2_delay_tap;
wire [4:0]     c2_detect_delay_tap;
wire			c2_hmc920_en;
wire [15:0]		c2_ltc1867_spi_wr;
wire			c2_ltc1867_spi_valid;
wire			c2_ltc1867_din_ready;
wire [15:0]		c2_ltc1867_spi_rd;
wire			c2_tec_en;
wire			c2_tec_dac_clr;
wire [15:0]		c2_tec_dac_wr;
wire			c2_tec_dac_valid;
wire 			c2_sip_en;
wire [23:0]		c2_sip_dac_wr;
wire 			c2_sip_dac_valid;
wire 			c2_spd_en;
wire [23:0]		c2_spd_dac_wr;
wire 			c2_spd_dac_valid;
wire [23:0]    c2_dp_wr;
wire           c2_dp_valid;
wire [7:0]     c2_dp_rd;
// CON2 -> reg sig 
wire [31:0]		c2_spad_scnt;
wire [31:0]		c2_spad_dcnt;
wire [31:0]		c2_spad_ncnt;
wire [31:0]		c2_spad_orcnt;
assign c2_spad_scnt = c2_spad_cnt1; 
assign c2_spad_dcnt = c2_spad_cnt2; 
assign c2_spad_ncnt = c2_spad_cnt3; 
assign c2_spad_orcnt = c2_spad_cnt4;

con2_intf_charlie con2_intf_charlie (
	.sys_clk	(sys_clk),		// I
    .sig_clk    (sig_clk),        // I
    .clk_200   (clk_200),      // I
    .clk_600    (clk_600),      // I
	.sig_10M(sig_clk_10M_pulse), // I	
	.rst		(grst),			// I

	// to Con 2
	.CON2B1_p0	(CON2B1_p0),		// I 
    .CON2B1_n0  (CON2B1_n0),        // I 
    .CON2B1_p1  (CON2B1_p1),        // O
    .CON2B1_n1  (CON2B1_n1),        // O 
    .CON2B1_p2  (CON2B1_p2),        // I 
    .CON2B1_n2  (CON2B1_n2),        // I  
	.CON2B2_p	(CON2B2_p),		// O
	.CON2B2_n	(CON2B2_n),		// O
	.CON2B3		(CON2B3),		// IO [21:0]
	
	// C_TRIG_TYPE_REG
	.trig_type			(c_trig_type),			// I [1:0]
	.spd_trig			(c2_spd_trig),			// O
	.spd_trig_valid	    (c2_spd_trig_valid),			// O
	.mask_10M		    (c2_mask_10M),	// O

	.spad_cnt1	(c2_spad_cnt1),		// O [31:0]
	.spad_cnt2	(c2_spad_cnt2),		// O [31:0]
	.spad_cnt3	(c2_spad_cnt3),		// O [31:0]	
	.spad_cnt4  (c2_spad_cnt4),    // O [31:0]		

	// ---- Serdes ---
	.test_mode		(c2_test_mode),		// I 			// sig_clk
    .din_test        (c2_test_data),        // I [31:0]        // sig_clk
	.shape_load		(shape_ld),			// I
	.pulse_width	(c2_width),			// I [5:0]
    .i_offset_quotient(c2_i_offset_quotient),   // I [6:0]     /
    .i_offset_remainder(c2_i_offset_remainder), // I [2:0]

	// delay tap 78ps per tap (max 2.418ns)

	.delay_tap_in	(c2_delay_tap),		// I [4:0]
    .detect_delay_tap_in (c2_detect_delay_tap), // I [4:0]    
    .hmc920_en        (c2_hmc920_en),            // I

    // ---- READ TEMP _ADC (LTC1867) ---
    .ltc1867_din        (c2_ltc1867_spi_wr),    // I [15:0]
    .ltc1867_din_valid    (c2_ltc1867_spi_valid),    // I
    .ltc1867_din_ready    (c2_ltc1867_din_ready),    // O
    .ltc1867_dout        (c2_ltc1867_spi_rd),    // O [15:0]
    // ---- TEC DAC (MAX5144) ---
    .tec_shdn_b            (c2_tec_en),                // I
    .tec_clr            (c2_tec_dac_clr),            // I
    .tec_dac_din        (c2_tec_dac_wr),            // I [15:0]
    .tec_dac_din_valid    (c2_tec_dac_valid),        // I
    .tec_dac_din_ready    (),                        // O
    // ---- SIP DAC (AD5060) ---
    .f_sip_disable        (c2_sip_en),            // I
    .sip_dac_din        (c2_sip_dac_wr),        // I [23:0]
    .sip_dac_din_valid    (c2_sip_dac_valid),        // I
    .sip_dac_din_ready(),                        // O
    // ---- SPD DAC (AD5752A) ---
    .spd_en                (c2_spd_en),            // I
    .spd_dac_din        (c2_spd_dac_wr),        // I [23:0]
    .spd_dac_din_valid    (c2_spd_dac_valid),        // I
    .spd_dac_din_ready(),                        // O
    
    // ---- DP (AD5252) ---

    .dp_din(c2_dp_wr),                 // I [23:0]
    .dp_din_valid(c2_dp_valid),        // I
    .dp_dout(c2_dp_rd)                 // O [7:0]    	
);

wire [31:0]    c3_spad_cnt1;
wire [31:0]    c3_spad_cnt2;
wire [31:0]    c3_spad_cnt3;
wire [31:0]    c3_spad_cnt4;

wire 			c3_test_mode;
wire [31:0]		c3_test_data;

wire [5:0]		c3_width;
wire [6:0]		c3_i_offset_quotient;
wire [2:0]		c3_i_offset_remainder;
wire [4:0]		c3_delay_tap;
wire [4:0]     c3_detect_delay_tap;
wire			c3_hmc920_en;
wire [15:0]		c3_ltc1867_spi_wr;
wire			c3_ltc1867_spi_valid;
wire			c3_ltc1867_din_ready;
wire [15:0]		c3_ltc1867_spi_rd;
wire			c3_tec_en;
wire			c3_tec_dac_clr;
wire [15:0]		c3_tec_dac_wr;
wire			c3_tec_dac_valid;
wire 			c3_sip_en;
wire [23:0]		c3_sip_dac_wr;
wire 			c3_sip_dac_valid;
wire 			c3_spd_en;
wire [23:0]		c3_spd_dac_wr;
wire 			c3_spd_dac_valid;
wire [23:0]    c3_dp_wr;
wire           c3_dp_valid;
wire [7:0]     c3_dp_rd;
// CON1 -> reg sig 
wire [31:0]		c3_spad_scnt;
wire [31:0]		c3_spad_dcnt;
wire [31:0]		c3_spad_ncnt;
wire [31:0]		c3_spad_orcnt;
assign c3_spad_scnt = c3_spad_cnt1; 
assign c3_spad_dcnt = c3_spad_cnt2; 
assign c3_spad_ncnt = c3_spad_cnt3; 
assign c3_spad_orcnt = c3_spad_cnt4; 

con3_intf_charlie con3_intf_charlie (
	.sys_clk	(sys_clk),		// I
    .sig_clk    (sig_clk),        // I
    .clk_200   (clk_200),      // I
    .clk_600    (clk_600),      // I
    .sig_10M(sig_clk_10M_pulse), // I    
    .rst        (grst),            // I

	// to Con 3
	.CON3B1_p0	(CON3B1_p0),		// I 
    .CON3B1_n0    (CON3B1_n0),        // I 
    .CON3B1_p1    (CON3B1_p1),        // O
    .CON3B1_n1  (CON3B1_n1),        // O
    .CON3B1_p2    (CON3B1_p2),        // I 
    .CON3B1_n2  (CON3B1_n2),        // I  
	.CON3B2_p	(CON3B2_p),		// O
	.CON3B2_n	(CON3B2_n),		// O
	.CON3B3		(CON3B3),		// IO [21:0]

	// C_TRIG_TYPE_REG
	.trig_type			(c_trig_type),			// I [1:0]
	.spd_trig			(c3_spd_trig),			// O
	.spd_trig_valid	    (c3_spd_trig_valid),			// O
	.mask_10M		    (c3_mask_10M),	// O

	.spad_cnt1	(c3_spad_cnt1),		// O [31:0]
	.spad_cnt2	(c3_spad_cnt2),		// O [31:0]
	.spad_cnt3	(c3_spad_cnt3),		// O [31:0]	
	.spad_cnt4  (c3_spad_cnt4),    // O [31:0]		

	// ---- Serdes ---
	.test_mode		(c3_test_mode),		// I 			// sig_clk
    .din_test        (c3_test_data),        // I [31:0]        // sig_clk
	.shape_load		(shape_ld),			// I
	.pulse_width	(c3_width),			// I [5:0]
    .i_offset_quotient(c3_i_offset_quotient),   // I [6:0]    
    .i_offset_remainder(c3_i_offset_remainder), // I [2:0]

	// delay tap 78ps per tap (max 2.418ns)

	.delay_tap_in	(c3_delay_tap),		// I [4:0]
    .detect_delay_tap_in (c3_detect_delay_tap), // I [4:0]    
    // ---- Enable  -----
    .hmc920_en        (c3_hmc920_en),            // I

    // ---- READ TEMP _ADC (LTC1867) ---
    .ltc1867_din        (c3_ltc1867_spi_wr),    // I [15:0]
    .ltc1867_din_valid    (c3_ltc1867_spi_valid),    // I
    .ltc1867_din_ready    (c3_ltc1867_din_ready),    // O
    .ltc1867_dout        (c3_ltc1867_spi_rd),    // O [15:0]
    // ---- TEC DAC (MAX5144) ---
    .tec_shdn_b            (c3_tec_en),                // I
    .tec_clr            (c3_tec_dac_clr),            // I
    .tec_dac_din        (c3_tec_dac_wr),            // I [15:0]
    .tec_dac_din_valid    (c3_tec_dac_valid),        // I
    .tec_dac_din_ready    (),                        // O
    // ---- SIP DAC (AD5060) ---
    .f_sip_disable        (c3_sip_en),            // I
    .sip_dac_din        (c3_sip_dac_wr),        // I [23:0]
    .sip_dac_din_valid    (c3_sip_dac_valid),        // I
    .sip_dac_din_ready(),                        // O
    // ---- SPD DAC (AD5752A) ---
    .spd_en                (c3_spd_en),            // I
    .spd_dac_din        (c3_spd_dac_wr),        // I [23:0]
    .spd_dac_din_valid    (c3_spd_dac_valid),        // I
    .spd_dac_din_ready(),                        // O
    
    // ---- DP (AD5252) ---

    .dp_din(c3_dp_wr),                 // I [23:0]
    .dp_din_valid(c3_dp_valid),        // I
    .dp_dout(c3_dp_rd)                 // O [7:0]    	
);


wire [31:0]    c4_spad_cnt1;
wire [31:0]    c4_spad_cnt2;
wire [31:0]    c4_spad_cnt3;
wire [31:0]    c4_spad_cnt4;

wire 			c4_test_mode;
wire [31:0]		c4_test_data;

wire [5:0]		c4_width;
wire [6:0]		c4_i_offset_quotient;
wire [2:0]		c4_i_offset_remainder;
wire [4:0]		c4_delay_tap;
wire [4:0]     c4_detect_delay_tap;
wire			c4_hmc920_en;
wire [15:0]		c4_ltc1867_spi_wr;
wire			c4_ltc1867_spi_valid;
wire			c4_ltc1867_din_ready;
wire [15:0]		c4_ltc1867_spi_rd;
wire			c4_tec_en;
wire			c4_tec_dac_clr;
wire [15:0]		c4_tec_dac_wr;
wire			c4_tec_dac_valid;
wire 			c4_sip_en;
wire [23:0]		c4_sip_dac_wr;
wire 			c4_sip_dac_valid;
wire 			c4_spd_en;
wire [23:0]		c4_spd_dac_wr;
wire 			c4_spd_dac_valid;
wire [23:0]    c4_dp_wr;
wire           c4_dp_valid;
wire [7:0]     c4_dp_rd;
// CON1 -> reg sig 
wire [31:0]		c4_spad_scnt;
wire [31:0]		c4_spad_dcnt;
wire [31:0]		c4_spad_ncnt;
wire [31:0]		c4_spad_orcnt;
assign c4_spad_scnt = c4_spad_cnt1; 
assign c4_spad_dcnt = c4_spad_cnt2; 
assign c4_spad_ncnt = c4_spad_cnt3; 
assign c4_spad_orcnt = c4_spad_cnt4; 

con4_intf_charlie con4_intf_charlie (
	.sys_clk	(sys_clk),		// I
    .sig_clk    (sig_clk),        // I
    .clk_200   (clk_200),      // I
    .clk_600    (clk_600),      // I
    .sig_10M(sig_clk_10M_pulse), // I    
    .rst        (grst),            // I

	// to Con 4
	.CON4B1_p0	(CON4B1_p0),		// I 
    .CON4B1_n0    (CON4B1_n0),        // I 
    .CON4B1_p1    (CON4B1_p1),        // O
    .CON4B1_n1  (CON4B1_n1),        // O
    .CON4B1_p2    (CON4B1_p2),        // I 
    .CON4B1_n2  (CON4B1_n2),        // I 
	.CON4B2_p	(CON4B2_p),		// O
	.CON4B2_n	(CON4B2_n),		// O
	.CON4B3		(CON4B3),		// IO [21:0]

	// C_TRIG_TYPE_REG
	.trig_type			(c_trig_type),			// I [1:0]
	.spd_trig			(c4_spd_trig),			// O
	.spd_trig_valid	    (c4_spd_trig_valid),			// O
	.mask_10M		    (c4_mask_10M),	// O

	.spad_cnt1	(c4_spad_cnt1),		// O [31:0]
	.spad_cnt2	(c4_spad_cnt2),		// O [31:0]
	.spad_cnt3	(c4_spad_cnt3),		// O [31:0]	
	.spad_cnt4  (c4_spad_cnt4),    // O [31:0]		

	// ---- Serdes ---
	.test_mode		(c4_test_mode),		// I 			// sig_clk
    .din_test       (c4_test_data),        // I [31:0]        // sig_clk
	.shape_load		(shape_ld),			// I
	.pulse_width	(c4_width),			// I [5:0]
    .i_offset_quotient(c4_i_offset_quotient),   // I [6:0]    
    .i_offset_remainder(c4_i_offset_remainder), // I [2:0]

	// delay tap 78ps per tap (max 2.418ns)

	.delay_tap_in	(c4_delay_tap),		// I [4:0]
    .detect_delay_tap_in (c4_detect_delay_tap), // I [4:0]  	
	
    // ---- Enable  -----
    .hmc920_en        (c4_hmc920_en),            // I

    // ---- READ TEMP _ADC (LTC1867) ---
    .ltc1867_din        (c4_ltc1867_spi_wr),    // I [15:0]
    .ltc1867_din_valid    (c4_ltc1867_spi_valid),    // I
    .ltc1867_din_ready    (c4_ltc1867_din_ready),    // O
    .ltc1867_dout        (c4_ltc1867_spi_rd),    // O [15:0]
    // ---- TEC DAC (MAX5144) ---
    .tec_shdn_b            (c4_tec_en),                // I
    .tec_clr            (c4_tec_dac_clr),            // I
    .tec_dac_din        (c4_tec_dac_wr),            // I [15:0]
    .tec_dac_din_valid    (c4_tec_dac_valid),        // I
    .tec_dac_din_ready    (),                        // O
    // ---- SIP DAC (AD5060) ---
    .f_sip_disable        (c4_sip_en),            // I
    .sip_dac_din        (c4_sip_dac_wr),        // I [23:0]
    .sip_dac_din_valid    (c4_sip_dac_valid),        // I
    .sip_dac_din_ready(),                        // O
    // ---- SPD DAC (AD5752A) ---
    .spd_en                (c4_spd_en),            // I
    .spd_dac_din        (c4_spd_dac_wr),        // I [23:0]
    .spd_dac_din_valid    (c4_spd_dac_valid),        // I
    .spd_dac_din_ready(),                        // O
    
    // ---- DP (AD5252) ---

    .dp_din(c4_dp_wr),                 // I [23:0]
    .dp_din_valid(c4_dp_valid),        // I
    .dp_dout(c4_dp_rd)                 // O [7:0]    	
);

wire		c5_fpc_en;
wire		c5_fpc_dac_clr;
wire [31:0]	c5_fpc_dac_din;
wire		c5_fpc_dac_din_valid;
wire		c5_fpc10_dac_clr;
wire [31:0]	c5_fpc10_dac_din;
wire		c5_fpc10_dac_din_valid;
// FPC
con5_intf_charlie con5_intf_charlie (
	.sys_clk(sys_clk),			// I
	.rst(grst),					// I

	.CON5B1_p	(CON5B1_p),		// O 
	.CON5B1_n	(CON5B1_n),		// O 
	.CON5B2_p	(CON5B2_p),		// O
	.CON5B2_n	(CON5B2_n),		// O
	.CON5B3		(CON5B3),		// IO [15:0]

	// ---- Enable  -----
	.fpc_en					(c5_fpc_en),	// I
	// ---- FPC (AD5044) ---
	.fpc_dac_clr			(c5_fpc_dac_clr),		// I
	.fpc_dac_din			(c5_fpc_dac_din),		// I [31:0]
	.fpc_dac_din_valid		(c5_fpc_dac_din_valid),		// I
	.fpc_dac_din_ready		(),		// O
	// ---- FPC10 (AD5044) ---
	.fpc10_dac_clr			(c5_fpc10_dac_clr),		// I
	.fpc10_dac_din			(c5_fpc10_dac_din),		// I [31:0]
	.fpc10_dac_din_valid	(c5_fpc10_dac_din_valid),	// I
	.fpc10_dac_din_ready	()	// O

);
wire 			c6_test_mode;
wire [31:0]		c6_test_data;

wire [5:0]		c6_width;
wire [7:0]      c6_add_width;
wire [6:0]		c6_i_offset_quotient;
wire [2:0]		c6_i_offset_remainder;
wire [4:0]		c6_delay_tap;

wire			c6_hmc920_en;
wire [15:0]		c6_ltc1867_spi_wr;
wire			c6_ltc1867_spi_valid;
wire			c6_ltc1867_din_ready;
wire [15:0]		c6_ltc1867_spi_rd;
wire [23:0]		c6_ad5761_dac_wr;
wire			c6_ad5761_dac_valid;
wire [23:0]		c6_ad5761_dac_rd;
wire			c6_tec_en;
wire			c6_tec_dac_clr;
wire [15:0]		c6_tec_dac_wr;
wire			c6_tec_dac_valid;

wire [23:0]     c6_dp_wr;
wire            c6_dp_valid;
wire [7:0]      c6_dp_rd;
// LD SYNC
con6_intf_charlie con6_intf_charlie (

    .sys_clk	(sys_clk),		// I
    .sig_clk	(sig_clk),		// I       
	.clk_200	(clk_200),	// I
	.clk_600	(clk_600),	// I
	.rst(grst),					// I
	
	.CON6B1_p	(CON6B1_p),		// O [1:0]
	.CON6B1_n	(CON6B1_n),		// O [1:0] 
	.CON6B2_p	(CON6B2_p),		// O
	.CON6B2_n	(CON6B2_n),		// O
	.CON6B3		(CON6B3),		// IO [15:0]

	// ---- Serdes ---

        // ---- Serdes ---
    .test_mode        (c6_test_mode),        // I             // sig_clk
    .din_test        (c6_test_data),        // I [31:0]        // sig_clk

    .shape_load        (shape_ld_trig),            // I
    .pulse_width    (c6_width),            // I [5:0]
    .pulse_add_width(c6_add_width),        // I [7:0]
    .i_offset_quotient(c6_i_offset_quotient),   // I [7:0]    
    .i_offset_remainder(c6_i_offset_remainder), // I [2:0]

    // delay tap 78ps per tap (max 2.418ns)

    .delay_tap_in    (c6_delay_tap),        // I [4:0]

    
    .din_pulse            (sig_clk_10M_pulse),            // I
    .din_pulseadd        (o_a6_delayed_flag & sig_clk_10M_pulse),    // I

    // ---- Enable  -----
    .hmc920_en            (c6_hmc920_en),                // I
        
    // ---- LD_ADC (LTC1867) ---
    .ltc1867_din        (c6_ltc1867_spi_wr),        // I [15:0]
    .ltc1867_din_valid    (c6_ltc1867_spi_valid),        // I
    .ltc1867_din_ready    (c6_ltc1867_din_ready),        // O
    .ltc1867_dout        (c6_ltc1867_spi_rd),        // O [15:0]
    // ---- TEC DAC (MAX5144) ---
    .tec_shdn_b            (c6_tec_en),                // I
    .tec_clr            (c6_tec_dac_clr),            // I
    .tec_dac_din        (c6_tec_dac_wr),            // I [15:0]
    .tec_dac_din_valid    (c6_tec_dac_valid),            // I
    .tec_dac_din_ready    (),                            // O
    // ---- LD DAC (AD5761) ---
    .ad5761_dac_din            (c6_ad5761_dac_wr),        // I [23:0]
    .ad5761_dac_din_valid    (c6_ad5761_dac_valid),    // I
    .ad5761_dac_din_ready    (),                        // O
    .ad5761_dac_dout        (c6_ad5761_dac_rd),        // O [23:0]

    // ---- DP (AD5252) ---
    .dp_din(c6_dp_wr),                 // I [23:0]
    .dp_din_valid(c6_dp_valid),        // I
    .dp_dout(c6_dp_rd)                 // O [7:0

);
wire 			c7_test_mode;
wire [31:0]		c7_test_data;

wire [5:0]		c7_width;
wire [7:0]      c7_add_width;
wire [6:0]		c7_i_offset_quotient;
wire [2:0]		c7_i_offset_remainder;
wire [4:0]		c7_delay_tap;

wire			c7_hmc920_en;
wire [15:0]		c7_ltc1867_spi_wr;
wire			c7_ltc1867_spi_valid;
wire			c7_ltc1867_din_ready;
wire [15:0]		c7_ltc1867_spi_rd;
wire [23:0]		c7_ad5761_dac_wr;
wire			c7_ad5761_dac_valid;
wire [23:0]		c7_ad5761_dac_rd;
wire			c7_tec_en;
wire			c7_tec_dac_clr;
wire [15:0]		c7_tec_dac_wr;
wire			c7_tec_dac_valid;

wire [23:0]     c7_dp_wr;
wire            c7_dp_valid;
wire [7:0]      c7_dp_rd;
// LD SYNC
con7_intf_charlie con7_intf_charlie (
    .sys_clk	(sys_clk),		// I
    .sig_clk	(sig_clk),		// I    
    .clk_200    (clk_200),    // I
    .clk_600    (clk_600),    // I

	.rst		(grst),			// I	
	.CON7B1_p	(CON7B1_p),		// O 
	.CON7B1_n	(CON7B1_n),		// O 
	.CON7B2_p	(CON7B2_p),		// O
    .CON7B2_n    (CON7B2_n),        // O	
	.CON7B3		(CON7B3),		// IO [15:0]
	//MGT is not used

	// ---- Serdes ---
	.test_mode		(c7_test_mode),		// I 			// sig_clk
	.din_test		(c7_test_data),		// I [31:0]		// sig_clk

	.shape_load		(shape_ld_trig),			// I
	.pulse_width	(c7_width),			// I [5:0]
	.pulse_add_width(c7_add_width),		// I [7:0]
    .i_offset_quotient(c7_i_offset_quotient),   // I [6:0]    
    .i_offset_remainder(c7_i_offset_remainder), // I [2:0]

	// delay tap 78ps per tap (max 2.418ns)

	.delay_tap_in	(c7_delay_tap),		// I [4:0]
	
	.din_pulse			(sig_clk_10M_pulse),			// I
	.din_pulseadd		(o_a7_delayed_flag & sig_clk_10M_pulse),	// I

	// ---- Enable  -----
	.hmc920_en			(c7_hmc920_en),				// I
		
	// ---- LD_ADC (LTC1867) ---
	.ltc1867_din		(c7_ltc1867_spi_wr),		// I [15:0]
	.ltc1867_din_valid	(c7_ltc1867_spi_valid),		// I
	.ltc1867_din_ready	(c7_ltc1867_din_ready),		// O
	.ltc1867_dout		(c7_ltc1867_spi_rd),		// O [15:0]
	// ---- TEC DAC (MAX5144) ---
	.tec_shdn_b			(c7_tec_en),				// I
	.tec_clr			(c7_tec_dac_clr),			// I
	.tec_dac_din		(c7_tec_dac_wr),			// I [15:0]
	.tec_dac_din_valid	(c7_tec_dac_valid),			// I
	.tec_dac_din_ready	(),							// O
	// ---- LD DAC (AD5761) ---
	.ad5761_dac_din			(c7_ad5761_dac_wr),		// I [23:0]
	.ad5761_dac_din_valid	(c7_ad5761_dac_valid),	// I
	.ad5761_dac_din_ready	(),						// O
	.ad5761_dac_dout		(c7_ad5761_dac_rd),	    // O [23:0]

	// ---- DP (AD5252) ---
	.dp_din(c7_dp_wr),                 // I [23:0]
	.dp_din_valid(c7_dp_valid),        // I
	.dp_dout(c7_dp_rd)                 // O [7:0]
);
wire		c8_fpc_en;
wire		c8_fpc_dac_clr;
wire [31:0]	c8_fpc_dac_din;
wire		c8_fpc_dac_din_valid;
wire		c8_fpc10_dac_clr;
wire [31:0]	c8_fpc10_dac_din;
wire		c8_fpc10_dac_din_valid;

// FPC
con8_intf_charlie con8_intf_charlie (

	.sys_clk(sys_clk),			// I
	.rst		(grst),			// I	
	.CON8B1_p0	(CON8B1_p0),		// O 
	.CON8B1_n0	(CON8B1_n0),		// O 
	.CON8B1_p2	(CON8B1_p2),		// O
    .CON8B1_n2    (CON8B1_n2),        // O	
	.CON8B3		(CON8B3),		// IO [15:0]

		// ---- Enable  -----
    .fpc_en                    (c8_fpc_en),    // I
    // ---- FPC (AD5044) ---
    .fpc_dac_clr            (c8_fpc_dac_clr),        // I
    .fpc_dac_din            (c8_fpc_dac_din),        // I [31:0]
    .fpc_dac_din_valid        (c8_fpc_dac_din_valid),        // I
    .fpc_dac_din_ready        (),        // O
    // ---- FPC10 (AD5044) ---
    .fpc10_dac_clr            (c8_fpc10_dac_clr),        // I
    .fpc10_dac_din            (c8_fpc10_dac_din),        // I [31:0]
    .fpc10_dac_din_valid    (c8_fpc10_dac_din_valid),    // I
    .fpc10_dac_din_ready    ()    // O
);

con9_intf_charlie con9_intf_charlie (
	.CON9B1_p(CON9B1_p),			// I [11:0]
	.CON9B1_n(CON9B1_n),			// I [11:0]
	.ADC_A_CLK_p(ADC_A_CLK_p),		// O
	.ADC_A_CLK_n(ADC_A_CLK_n),		// O
	.A_DCO_p(A_DCO_p),				// I
	.A_DCO_n(A_DCO_n),				// I
	.CON9B3(CON9B3)					// I [15:0]
);

con10_intf_charlie con10_intf_charlie (
	.CON10B1_p	(CON10B1_p),		// O 
	.CON10B1_n	(CON10B1_n),		// O 
	.CON10B2_p	(CON10B2_p),		// O 
    .CON10B2_n  (CON10B2_n),        // O 
	.CON10B3	(CON10B3)		// IO [15:0]	
);

// Simultaneous Coefficient Output
(* mark_debug = "true" *)wire o_spd_trig12;
(* mark_debug = "true" *)wire o_spd_trig13;
(* mark_debug = "true" *)wire o_spd_trig24;
(* mark_debug = "true" *)wire o_spd_trig34;
wire o_spd_trig1234;

wire [31:0] o_spad12_cnt;
wire [31:0] o_spad13_cnt;
wire [31:0] o_spad24_cnt;
wire [31:0] o_spad34_cnt;
wire [31:0] o_spad1234_cnt;

simultaneous_coeff_mea simultaneous_coeff_mea (
        .rst(grst),
        .sig_clk(sig_clk),
        .spd_trig_1(c1_spd_trig),      // I
        .spd_trig_2(c2_spd_trig),      // I
        .spd_trig_3(c3_spd_trig),      // I
        .spd_trig_4(c4_spd_trig),      // I
        .spd_trig12(o_spd_trig12),      // O
        .spd_trig13(o_spd_trig13),      // O
        .spd_trig24(o_spd_trig24),      // O
        .spd_trig34(o_spd_trig34),      // O
        .spad_cnt12_cnt(o_spad12_cnt),  // O [31:0]
        .spad_cnt13_cnt(o_spad13_cnt),  // O [31:0]
        .spad_cnt24_cnt(o_spad24_cnt),  // O [31:0]
        .spad_cnt34_cnt(o_spad34_cnt),  // O [31:0]    
        .spad_cnt1234_cnt(o_spad1234_cnt)  // O [31:0]                                     
);

wire fifo_rst;


// LOG
log_8bit log_8bit (
	.clk		(sig_clk),		// I
	.rst		(grst || fifo_rst),			// I

	.din_log0	(c1_spd_trig),			// I
	.div_log0	(c1_spd_trig_valid),		// I
	.din_log1	(c2_spd_trig),			// I
	.div_log1	(c1_spd_trig_valid),		// I
	.din_log2	(c3_spd_trig),			// I
	.div_log2	(c1_spd_trig_valid),		// I
	.din_log3	(c4_spd_trig),			// I
	.div_log3	(c1_spd_trig_valid),		// I
/*	.din_log4	(o_spd_trig12),          // I
	.div_log4	(c1_spd_trig_valid),	    // I	
	.din_log5	(o_spd_trig13),				// I
	.div_log5	(c1_spd_trig_valid),		// I
	.din_log6	(o_spd_trig24),				// I
	.div_log6	(c1_spd_trig_valid),		// I
	.din_log7	(o_spd_trig34),	// I
	.div_log7	(c1_spd_trig_valid),		// I
*/
	.emp_log	(emp_log),			// O [7:0]
	.do_log_16to32	 (log32),			// O [31:0]
    .dov_log_16to32  (log32_valid),        // O
    .do_log_64to128  (log128),          // O [127:0]
    .dov_log_64to128 (log128_valid)     // O
	
	
);

// ---------------------------------------------------------------------------------
//Switch for DMA Test
wire			test_cnt_clr;
wire	[1:0]	data_sel;
wire	[127:0]	to_pcie_tdata;
wire	[15:0]	to_pcie_tkeep;
wire			to_pcie_tlast;
wire			to_pcie_tvalid;
wire			to_pcie_tready;

// Module for DMA
wire	[127:0]	o_dma_buf_data;
wire    [15:0]	o_dma_buf_keep;
wire            o_dma_buf_last;
wire            o_dma_buf_valid;
wire            o_dma_buf_ready;



pcie_dma_data_sel #(
	.DATA_WIDTH(128)
) pcie_dma_data_sel (
	.pcie_clk(pcie_clk),			// I
	.sclr(grst),					// I

	.test_cnt_clr(test_cnt_clr),	// I
	.data_sel(data_sel),			// I [1:0]
	
	
	.din_fifo_tdata(o_dma_buf_data),			// I [DATA_WIDTH-1:0]
    .din_fifo_tkeep(o_dma_buf_keep),            // I [KEEP_WIDTH-1:0]
    .din_fifo_tvalid(o_dma_buf_valid),        // I
    .din_fifo_tlast(o_dma_buf_last),            // I
    .din_fifo_tready(o_dma_buf_ready),        // O

	// to PCIE Axi streaming
	.to_pcie_tdata(to_pcie_tdata),			// O [DATA_WIDTH-1:0]
	.to_pcie_tkeep(to_pcie_tkeep),			// O [KEEP_WIDTH-1:0]
	.to_pcie_tlast(to_pcie_tlast),			// O
	.to_pcie_tvalid(to_pcie_tvalid),		// O
	.to_pcie_tready(to_pcie_tready)			// I
);

// ---------------------------------------------------------------------------------
// PCIE BLOCK // all pcie-clk
wire	[46:0]	regs_adda;
wire			regs_adda_valid;

wire			regs_bsc_sys_sel;
wire			regs_bsc_sig_sel;
wire			regs_bsc_pci_sel;
wire			regs_ud_sys_sel;
wire			regs_ud_sig_sel;
//wire			regs_ud_reg3_sel;
//wire			regs_ud_reg4_sel;

wire	[31:0]	regs_sys_data;
wire	[31:0]	regs_sig_data;
//wire	[31:0]	regs_ud_reg3_data;
//wire	[31:0]	regs_ud_reg4_data;
wire	[31:0]	regs_bsc_pci_data;
wire			regs_sys_data_valid;
wire			regs_sig_data_valid;
wire			regs_bsc_pci_data_valid;
//wire			regs_ud_reg3_data_valid;
//wire			regs_ud_reg4_data_valid;

wire	[7:0]	c2h_0_sts;
wire	[7:0]	h2c_0_sts;
wire			c2h_0_rst;			// to use fifo reset
wire			dma_fifo_empty;		// to use fifo reset
wire	[31:0]	c2h_0_src_addr;
wire	[27:0]	c2h_0_len;
wire	[31:0]	c2h_0_busy_clk_cnt;
wire	[31:0]	c2h_0_run_clk_cnt;
wire	[31:0]	c2h_0_packet_cnt;
wire	[31:0]	c2h_0_desc_cnt;
wire regs_data_valid;
assign dma_fifo_empty = 1'b0;
assign regs_data_valid = regs_sig_data_valid | regs_sys_data_valid | regs_bsc_pci_data_valid;// | regs_ud_reg3_data_valid; // | regs_ud_reg4_data_valid;

xdma_pcie_ep xdma_pcie_ep  (
	.pci_exp_txp(pci_exp_txp),						// O [3:0]
	.pci_exp_txn(pci_exp_txn),						// O [3:0]
	.pci_exp_rxp(pci_exp_rxp),						// I [3:0]
	.pci_exp_rxn(pci_exp_rxn),						// I [3:0]

	.sys_clk_p(pcie_ref_clk_p),						// I
	.sys_clk_n(pcie_ref_clk_n),						// I
	.sys_rst_n(~grst),								// I

	// -- to/from REGS
	.regs_adda			(regs_adda),				// O [46:0]
	.regs_adda_valid	(regs_adda_valid),			// O
	.regs_bsc_sys_sel	(regs_bsc_sys_sel),			// O
	.regs_bsc_sig_sel	(regs_bsc_sig_sel),			// O
	.regs_bsc_pci_sel	(regs_bsc_pci_sel),			// O
	.regs_ud_sys_sel	(regs_ud_sys_sel),			// O
	.regs_ud_sig_sel	(regs_ud_sig_sel),			// O
//	.regs_ud_reg3_sel	(regs_ud_reg3_sel),			// O
//	.regs_ud_reg4_sel	(regs_ud_reg4_sel),			// O

	.regs_bsc_sys_data	(regs_sys_data),			// I [31:0]
	.regs_bsc_sig_data	(regs_sig_data),			// I [31:0]
	.regs_bsc_pci_data	(regs_bsc_pci_data),		// I [31:0]
	.regs_ud_sys_data	(regs_sys_data),			// I [31:0]
	.regs_ud_sig_data	(regs_sig_data),				// I [31:0]
//	.regs_ud_reg3_data	(regs_ud_reg3_data),		// I [31:0]
//	.regs_ud_reg4_data	('b0),		// I [31:0]
	.regs_data_valid	(regs_data_valid),			// I

	// DMA Statusregs_sys_data
	.c2h_sts_0			(c2h_0_sts),				// O [7:0]
	.h2c_sts_0			(h2c_0_sts),				// O [7:0]

	.c2h_busy_clk_cnt	(c2h_0_busy_clk_cnt),		// O [31:0]
	.c2h_run_clk_cnt	(c2h_0_run_clk_cnt),		// O [31:0]
	.c2h_packet_cnt		(c2h_0_packet_cnt),			// O [31:0]
	.c2h_desc_cnt		(c2h_0_desc_cnt),			// O [31:0]

	// --  AXI ST interface to user
	// AXI streaming ports
	.s_axis_c2h_tdata_0		(to_pcie_tdata),		// I [127:0]
	.s_axis_c2h_tlast_0		(to_pcie_tlast),		// I
	.s_axis_c2h_tvalid_0	(to_pcie_tvalid),		// I
	.s_axis_c2h_tready_0	(to_pcie_tready),		// O
	.s_axis_c2h_tkeep_0		(to_pcie_tkeep),		// I [15:0]
	.m_axis_h2c_tdata_0		(),						// O [127:0]	//not used
	.m_axis_h2c_tlast_0		(),						// O			//not used
	.m_axis_h2c_tvalid_0	(),						// O			//not used
	.m_axis_h2c_tready_0	(1'b0),					// I			//not used
	.m_axis_h2c_tkeep_0		(),						// O [15:0]		//not used

	// user clk
	.user_clk				(pcie_clk),				// O 125MHz
	.user_resetn			(),						// O
	.user_lnk_up			(pcie_link_up)
);



// ---------------------------------------------------------------------------------
// Reg for sys_clk
regs_sys_charlie regs_sys_charlie(
	.clk				(sys_clk),								// I
	.rst				(grst),									// I
	.pcie_clk			(pcie_clk),								// I

	.regs_sel			(regs_bsc_sys_sel | regs_ud_sys_sel),		// I
	.regs_adda			(regs_adda),							// I	[46:0]
	.regs_adda_valid	(regs_adda_valid),						// I
	.regs_dout			(regs_sys_data),					// O	[31:0]
	.regs_dout_valid	(regs_sys_data_valid),				// O

	// -------------------------------------------------------
	// -- register ?���?
	// BSC_TEMP_REG
	.led_test(led_test),							// O
	.led1_on(led1_on),								// O
	.led2_on(led2_on),								// O
	.led3_on(led3_on),								// O

	// LM70_SPI_WR_REG
	.lm70_spi_valid(lm70_spi_valid),				// O
	// LM70_SPI_RD_REG
	.lm70_spi_rd(lm70_spi_rd),						// I [15:0]
	.lm70_spi_busy(~lm70_din_ready),				// I

	// FAN_HIGH_DURA_REG
	.fan_high_dura(fan_high_dura),					// O [15:0]
	// FAN_LOW_DURA_REG
	.fan_low_dura(fan_low_dura),					// O [15:0]

	// FAN1_HIGH_DURA_REG
	.fan1_high_dura(fan1_high_dura),				// O [15:0]
	// FAN1_LOW_DURA_REG
	.fan1_low_dura(fan1_low_dura),					// O [15:0]

	// --------------------------------------------------------
	// CON 1
    // C1_HMC920_EN_REG
    .c1_hmc920_en(c1_hmc920_en),                    // O
    // C1_SPD_EN_REG
    .c1_spd_en(c1_spd_en),                    // O
    // C1_SIP_EN_REG
    .c1_sip_en(c1_sip_en),                    // O

    // C1_LTC1867_SPI_WR_REG
    .c1_ltc1867_spi_wr        (c1_ltc1867_spi_wr),        // O [15:0]
    .c1_ltc1867_spi_valid    (c1_ltc1867_spi_valid),        // O
    // C1_LTC1867_SPI_RD_REG
    .c1_ltc1867_spi_rd        (c1_ltc1867_spi_rd),        // I [15:0]
    .c1_ltc1867_spi_busy    (~c1_ltc1867_din_ready),    // I

    // C1_SPD_DAC_WR_REG
    .c1_spd_dac_wr            (c1_spd_dac_wr),            // O [15:0]
    .c1_spd_dac_valid        (c1_spd_dac_valid),            // O

    // C1_SIP_DAC_WR_REG
    .c1_sip_dac_wr            (c1_sip_dac_wr),            // O [15:0]
    .c1_sip_dac_valid        (c1_sip_dac_valid),            // O

    // C1_TEC_EN_REG
    .c1_tec_en            (c1_tec_en),            // O
    // C1_TEC_DAC_WR_REG
    .c1_tec_dac_wr        (c1_tec_dac_wr),        // O [15:0]
    .c1_tec_dac_valid    (c1_tec_dac_valid),        // O
    // C1_TEC_DAC_CLR_REG
    .c1_tec_dac_clr        (c1_tec_dac_clr),        // O
    
    // C1_DP_WR_REG
    .c1_dp_wr(c1_dp_wr),           // O [23:0]
    .c1_dp_valid(c1_dp_valid),
    // C1_DP_RD_REG
    .c1_dp_rd(c1_dp_rd),           // I [7:0]
	// --------------------------------------------------------
	// CON 2
    // C2_HMC920_EN_REG
    .c2_hmc920_en(c2_hmc920_en),                    // O
    // C2_SPD_EN_REG
    .c2_spd_en(c2_spd_en),                    // O
    // C2_SIP_EN_REG
    .c2_sip_en(c2_sip_en),                    // O

    // C2_LTC1867_SPI_WR_REG
    .c2_ltc1867_spi_wr        (c2_ltc1867_spi_wr),        // O [15:0]
    .c2_ltc1867_spi_valid    (c2_ltc1867_spi_valid),        // O
    // C2_LTC1867_SPI_RD_REG
    .c2_ltc1867_spi_rd        (c2_ltc1867_spi_rd),        // I [15:0]
    .c2_ltc1867_spi_busy    (~c2_ltc1867_din_ready),    // I

    // C2_SPD_DAC_WR_REG
    .c2_spd_dac_wr            (c2_spd_dac_wr),            // O [15:0]
    .c2_spd_dac_valid        (c2_spd_dac_valid),            // O

    // C2_SIP_DAC_WR_REG
    .c2_sip_dac_wr            (c2_sip_dac_wr),            // O [15:0]
    .c2_sip_dac_valid        (c2_sip_dac_valid),            // O

    // C2_TEC_EN_REG
    .c2_tec_en            (c2_tec_en),            // O
    // C2_TEC_DAC_WR_REG
    .c2_tec_dac_wr        (c2_tec_dac_wr),        // O [15:0]
    .c2_tec_dac_valid    (c2_tec_dac_valid),        // O
    // C2_TEC_DAC_CLR_REG
    .c2_tec_dac_clr        (c2_tec_dac_clr),        // O
    
    // C2_DP_WR_REG
    .c2_dp_wr(c2_dp_wr),           // O [23:0]
    .c2_dp_valid(c2_dp_valid),
    // C2_DP_RD_REG
    .c2_dp_rd(c2_dp_rd),           // I [7:0]
	// --------------------------------------------------------
	// CON 3
    // C3_HMC920_EN_REG
     .c3_hmc920_en(c3_hmc920_en),                    // O
     // C3_SPD_EN_REG
     .c3_spd_en(c3_spd_en),                    // O
     // C3_SIP_EN_REG
     .c3_sip_en(c3_sip_en),                    // O
 
     // C3_LTC1867_SPI_WR_REG
     .c3_ltc1867_spi_wr        (c3_ltc1867_spi_wr),        // O [15:0]
     .c3_ltc1867_spi_valid    (c3_ltc1867_spi_valid),        // O
     // C3_LTC1867_SPI_RD_REG
     .c3_ltc1867_spi_rd        (c3_ltc1867_spi_rd),        // I [15:0]
     .c3_ltc1867_spi_busy    (~c3_ltc1867_din_ready),    // I
 
     // C3_SPD_DAC_WR_REG
     .c3_spd_dac_wr            (c3_spd_dac_wr),            // O [15:0]
     .c3_spd_dac_valid        (c3_spd_dac_valid),            // O
 
     // C3_SIP_DAC_WR_REG
     .c3_sip_dac_wr            (c3_sip_dac_wr),            // O [15:0]
     .c3_sip_dac_valid        (c3_sip_dac_valid),            // O
 
     // C3_TEC_EN_REG
     .c3_tec_en            (c3_tec_en),            // O
     // C3_TEC_DAC_WR_REG
     .c3_tec_dac_wr        (c3_tec_dac_wr),        // O [15:0]
     .c3_tec_dac_valid    (c3_tec_dac_valid),        // O
     // C3_TEC_DAC_CLR_REG
     .c3_tec_dac_clr        (c3_tec_dac_clr),        // O
     
     // C3_DP_WR_REG
     .c3_dp_wr(c3_dp_wr),           // O [23:0]
     .c3_dp_valid(c3_dp_valid),
     // C3_DP_RD_REG
     .c3_dp_rd(c3_dp_rd),           // I [7:0]
	// --------------------------------------------------------
	// CON 4
   // C4_HMC920_EN_REG
     .c4_hmc920_en(c4_hmc920_en),                    // O
     // C4_SPD_EN_REG
     .c4_spd_en(c4_spd_en),                    // O
     // C4_SIP_EN_REG
     .c4_sip_en(c4_sip_en),                    // O
 
     // C4_LTC1867_SPI_WR_REG
     .c4_ltc1867_spi_wr        (c4_ltc1867_spi_wr),        // O [15:0]
     .c4_ltc1867_spi_valid    (c4_ltc1867_spi_valid),        // O
     // C4_LTC1867_SPI_RD_REG
     .c4_ltc1867_spi_rd        (c4_ltc1867_spi_rd),        // I [15:0]
     .c4_ltc1867_spi_busy    (~c4_ltc1867_din_ready),    // I
 
     // C4_SPD_DAC_WR_REG
     .c4_spd_dac_wr            (c4_spd_dac_wr),            // O [15:0]
     .c4_spd_dac_valid        (c4_spd_dac_valid),            // O
 
     // C4_SIP_DAC_WR_REG
     .c4_sip_dac_wr            (c4_sip_dac_wr),            // O [15:0]
     .c4_sip_dac_valid        (c4_sip_dac_valid),            // O
 
     // C4_TEC_EN_REG
     .c4_tec_en            (c4_tec_en),            // O
     // C4_TEC_DAC_WR_REG
     .c4_tec_dac_wr        (c4_tec_dac_wr),        // O [15:0]
     .c4_tec_dac_valid    (c4_tec_dac_valid),        // O
     // C4_TEC_DAC_CLR_REG
     .c4_tec_dac_clr        (c4_tec_dac_clr),        // O
     
     // C4_DP_WR_REG
     .c4_dp_wr(c4_dp_wr),           // O [23:0]
     .c4_dp_valid(c4_dp_valid),
     // C4_DP_RD_REG
     .c4_dp_rd(c4_dp_rd),           // I [7:0] 	
	// --------------------------------------------------------
	// CON 5
	// C5_FPC_EN_REG
    .c5_fpc_en            (c5_fpc_en),            // O

    // C5_FPC_DAC_WR_REG
    .c5_fpc_dac_din            (c5_fpc_dac_din),        // O [31:0]
    .c5_fpc_dac_din_valid    (c5_fpc_dac_din_valid),        // O
    // C5_FPC_DAC_CLR_REG
    .c5_fpc_dac_clr            (c5_fpc_dac_clr),        // O

    // C5_FPC10_DAC_WR_REG
    .c5_fpc10_dac_din        (c5_fpc10_dac_din),        // O [31:0]
    .c5_fpc10_dac_din_valid    (c5_fpc10_dac_din_valid),    // O
    // C5_FPC10_DAC_CLR_REG
    .c5_fpc10_dac_clr        (c5_fpc10_dac_clr),        // O

	// --------------------------------------------------------
	// CON 6	
    // C6_HMC920_EN_REG
    .c6_hmc920_en(c6_hmc920_en),                    // O

    // C6_LTC1867_SPI_WR_REG
    .c6_ltc1867_spi_wr(c6_ltc1867_spi_wr),            // O [15:0]
    .c6_ltc1867_spi_valid(c6_ltc1867_spi_valid),    // O
    // C6_LTC1867_SPI_RD_REG
    .c6_ltc1867_spi_rd(c6_ltc1867_spi_rd),            // I [15:0]
    .c6_ltc1867_spi_busy(~c6_ltc1867_din_ready),    // I

    // C6_AD5761_DAC_WR_REG
    .c6_ad5761_dac_wr(c6_ad5761_dac_wr),            // O [23:0]
    .c6_ad5761_dac_valid(c6_ad5761_dac_valid),        // O

    // C6_AD5761_DAC_RD_REG
    .c6_ad5761_dac_rd(c6_ad5761_dac_rd),            // I [23:0]
 
     // C6_TEC_EN_REG
    .c6_tec_en(c6_tec_en),                    // O
    // C6_TEC_DAC_WR_REG
    .c6_tec_dac_wr(c6_tec_dac_wr),            // O [15:0]
    .c6_tec_dac_valid(c6_tec_dac_valid),    // O
    // C6_TEC_DAC_CLR_REG
    .c6_tec_dac_clr(c6_tec_dac_clr),            // O
    
    // C6_DP_WR_REG
    .c6_dp_wr(c6_dp_wr),           // O [23:0]
    .c6_dp_valid(c6_dp_valid),
    
    // C6_DP_RD_REG
    .c6_dp_rd(c6_dp_rd),           // I [7:0]

	// --------------------------------------------------------
	// CON 7

	// C7_HMC920_EN_REG
    .c7_hmc920_en(c7_hmc920_en),                    // O

    // C7_LTC1867_SPI_WR_REG
    .c7_ltc1867_spi_wr(c7_ltc1867_spi_wr),            // O [15:0]
    .c7_ltc1867_spi_valid(c7_ltc1867_spi_valid),    // O
    // C7_LTC1867_SPI_RD_REG
    .c7_ltc1867_spi_rd(c7_ltc1867_spi_rd),            // I [15:0]
    .c7_ltc1867_spi_busy(~c7_ltc1867_din_ready),    // I

    // C7_AD5761_DAC_WR_REG
    .c7_ad5761_dac_wr(c7_ad5761_dac_wr),            // O [23:0]
    .c7_ad5761_dac_valid(c7_ad5761_dac_valid),        // O

    // C7_AD5761_DAC_RD_REG
    .c7_ad5761_dac_rd(c7_ad5761_dac_rd),            // I [23:0]
    
    // C7_TEC_EN_REG
    .c7_tec_en(c7_tec_en),                    // O
    // C7_TEC_DAC_WR_REG
    .c7_tec_dac_wr(c7_tec_dac_wr),            // O [15:0]
    .c7_tec_dac_valid(c7_tec_dac_valid),    // O
    // C7_TEC_DAC_CLR_REG
    .c7_tec_dac_clr(c7_tec_dac_clr),            // O
    
    // C7_DP_WR_REG
    .c7_dp_wr(c7_dp_wr),           // O [23:0]
    .c7_dp_valid(c7_dp_valid),
    
    // C7_DP_RD_REG
    .c7_dp_rd(c7_dp_rd),           // I [7:0]	
	// --------------------------------------------------------

	// CON 8
    // C8_FPC_EN_REG
    .c8_fpc_en            (c8_fpc_en),            // O

    // C8_FPC_DAC_WR_REG
    .c8_fpc_dac_din            (c8_fpc_dac_din),        // O [31:0]
    .c8_fpc_dac_din_valid    (c8_fpc_dac_din_valid),        // O
    // C8_FPC_DAC_CLR_REG
    .c8_fpc_dac_clr            (c8_fpc_dac_clr),        // O

    // C8_FPC10_DAC_WR_REG
    .c8_fpc10_dac_din        (c8_fpc10_dac_din),        // O [31:0]
    .c8_fpc10_dac_din_valid    (c8_fpc10_dac_din_valid),    // O
    // C8_FPC10_DAC_CLR_REG
    .c8_fpc10_dac_clr        (c8_fpc10_dac_clr)        // O    
	// --------------------------------------------------------
	// CON 9

 	// --------------------------------------------------------
	// CON 10


);


// ---------------------------------------------------------------------------------
// Reg for sig_clk
regs_sig_charlie regs_sig_charlie (
	.clk				(sig_clk),								// I
	.rst				(grst),									// I
	.pcie_clk			(pcie_clk),								// I
	.sig_clk_10M_pulse (sig_clk_10M_pulse),                     // I
    .sig_start_flag     (sig_start_flag),
	.regs_sel			(regs_bsc_sig_sel | regs_ud_sig_sel),		// I
	.regs_adda			(regs_adda),							// I [46:0]
	.regs_adda_valid	(regs_adda_valid),						// I
	.regs_dout			(regs_sig_data),							// O [31:0]
	.regs_dout_valid	(regs_sig_data_valid),					// O
    .fifo_rst(fifo_rst), // O
	// -------------------------------------------------------
	// -- register
	// --------------------------------------------------------
	// CON 1
	// C1_TEST_MODE_REG
    .c1_test_mode    (c1_test_mode),    // O
    // C1_TEST_DATA_REG
    .c1_test_data    (c1_test_data),    // O [31:0]
    // C1_WIDTH_REG
    .c1_width        (c1_width),    // O [5:0]
    .c1_shape_load    (c1_shape_load),    // O
    // C1_OFFSET_QUOT_REG
    .c1_i_offset_quotient        (c1_i_offset_quotient),    // O [6:0]
    // C1_OFFSET_REMD_REG    
    .c1_i_offset_remainder          (c1_i_offset_remainder), // O [2:0]
    // C1_DELAY_TAP_REG
    .c1_delay_tap    (c1_delay_tap),    // O [4:0]
    // C1_DETECT_DELAY_TAP_REG
    .c1_detect_delay_tap(c1_detect_delay_tap), // O [4:0]
	// C1_SPD_CNT1_REG
    .c1_spad_cnt1    (c1_spad_cnt1),        // I [31:0]
	// C1_SPD_CNT2_REG
    .c1_spad_cnt2    (c1_spad_cnt2),        // I [31:0]
	// C1_SPD_CNT3_REG
    .c1_spad_cnt3    (c1_spad_cnt3),        // I [31:0]
    // C1_SPD_CNT4_REG
    .c1_spad_cnt4    (c1_spad_cnt4),        // I [31:0]    
            
	// --------------------------------------------------------
	// CON 2
	// C2_TEST_MODE_REG
    .c2_test_mode    (c2_test_mode),    // O
    // C2_TEST_DATA_REG
    .c2_test_data    (c2_test_data),    // O [31:0]
    // C2_WIDTH_REG
    .c2_width        (c2_width),    // O [5:0]
    .c2_shape_load    (c2_shape_load),    // O
    // C2_OFFSET_QUOT_REG
    .c2_i_offset_quotient        (c2_i_offset_quotient),    // O [6:0]
    // C2_OFFSET_REMD_REG    
    .c2_i_offset_remainder          (c2_i_offset_remainder), // O [2:0]
    // C2_DELAY_TAP_REG
    .c2_delay_tap    (c2_delay_tap),    // O [4:0]
     // C2_DETECT_DELAY_TAP_REG
    .c2_detect_delay_tap(c2_detect_delay_tap), // O [4:0]   
	// C2_SPD_CNT1_REG
    .c2_spad_cnt1    (c2_spad_cnt1),        // I [31:0]
	// C2_SPD_CNT2_REG
    .c2_spad_cnt2    (c2_spad_cnt2),        // I [31:0]
	// C2_SPD_CNT3_REG
    .c2_spad_cnt3    (c2_spad_cnt3),        // I [31:0]
	// C2_SPD_CNT4_REG
    .c2_spad_cnt4    (c2_spad_cnt4),        // I [31:0]            
	// --------------------------------------------------------
	// CON 3
	// C3_TEST_MODE_REG
    .c3_test_mode    (c3_test_mode),    // O
    // C3_TEST_DATA_REG
    .c3_test_data    (c3_test_data),    // O [31:0]
    // C3_WIDTH_REG
    .c3_width        (c3_width),    // O [5:0]
    .c3_shape_load    (c3_shape_load),    // O
    // C3_OFFSET_QUOT_REG
    .c3_i_offset_quotient        (c3_i_offset_quotient),    // O [6:0]
    // C3_OFFSET_REMD_REG    
    .c3_i_offset_remainder          (c3_i_offset_remainder), // O [2:0]
    // C3_DELAY_TAP_REG
    .c3_delay_tap    (c3_delay_tap),    // O [4:0]
    // C3_DETECT_DELAY_TAP_REG
   .c3_detect_delay_tap(c3_detect_delay_tap), // O [4:0]       
	// C3_SPD_CNT1_REG
    .c3_spad_cnt1    (c3_spad_cnt1),        // I [31:0]
    // C3_SPD_CNT2_REG
    .c3_spad_cnt2    (c3_spad_cnt2),        // I [31:0]
    // C3_SPD_CNT3_REG
    .c3_spad_cnt3    (c3_spad_cnt3),        // I [31:0]
    // C3_SPD_CNT4_REG
    .c3_spad_cnt4    (c3_spad_cnt4),        // I [31:0]  
// --------------------------------------------------------
// CON 4
	// C4_TEST_MODE_REG
    .c4_test_mode    (c4_test_mode),    // O
    // C4_TEST_DATA_REG
    .c4_test_data    (c4_test_data),    // O [31:0]
    // C4_WIDTH_REG
    .c4_width        (c4_width),    // O [5:0]
    .c4_shape_load    (c4_shape_load),    // O
    // C4_OFFSET_QUOT_REG
    .c4_i_offset_quotient        (c4_i_offset_quotient),    // O [6:0]
    // C4_OFFSET_REMD_REG    
    .c4_i_offset_remainder          (c4_i_offset_remainder), // O [2:0]
    // C4_DELAY_TAP_REG
    .c4_delay_tap    (c4_delay_tap),    // O [4:0]
    // C4_DETECT_DELAY_TAP_REG
   .c4_detect_delay_tap(c4_detect_delay_tap), // O [4:0]         
	// C4_SPD_CNT1_REG
     .c4_spad_cnt1    (c4_spad_cnt1),        // I [31:0]
     // C4_SPD_CNT2_REG
     .c4_spad_cnt2    (c4_spad_cnt2),        // I [31:0]
     // C4_SPD_CNT3_REG
     .c4_spad_cnt3    (c4_spad_cnt3),        // I [31:0]
     // C4_SPD_CNT4_REG
     .c4_spad_cnt4    (c4_spad_cnt4),        // I [31:0]  
// --------------------------------------------------------
// CON 5
// --------------------------------------------------------
// CON 6
    // C6_TEST_MODE_REG
    .c6_test_mode    (c6_test_mode),    // O
    // C6_TEST_DATA_REG
    .c6_test_data    (c6_test_data),    // O [31:0]

    // C6_WIDTH_REG
    .c6_width        (c6_width),            // O [5:0]
    .c6_add_width     (c6_add_width),
    .c6_shape_load    (c6_shape_load),    // O
    // C6_OFFSET_QUOT_REG
    .c6_i_offset_quotient        (c6_i_offset_quotient),    // O [6:0]
    // C6_OFFSET_REMD_REG    
    .c6_i_offset_remainder          (c6_i_offset_remainder), // O [2:0]
    // C6_DELAY_TAP_REG
    .c6_delay_tap    (c6_delay_tap),    // O [4:0]  
    // C6_ADD_PULSE_DELAY_REG
    .o_a6_delay_cnt  (o_a6_delay_cnt),  // O [14:0]    
// --------------------------------------------------------
// CON 7
    // C7_TEST_MODE_REG
    .c7_test_mode    (c7_test_mode),    // O
    // C7_TEST_DATA_REG
    .c7_test_data    (c7_test_data),    // O [31:0]

    // C7_WIDTH_REG
    .c7_width        (c7_width),            // O [5:0]
    .c7_add_width     (c7_add_width),
    .c7_shape_load    (c7_shape_load),    // O
    // C7_OFFSET_QUOT_REG
    .c7_i_offset_quotient        (c7_i_offset_quotient),    // O [6:0]
    // C6_OFFSET_REMD_REG    
    .c7_i_offset_remainder          (c7_i_offset_remainder), // O [2:0]
    // C7_DELAY_TAP_REG
    .c7_delay_tap    (c7_delay_tap),    // O [4:0]
    // C7_ADD_PULSE_DELAY_REG
    .o_a7_delay_cnt  (o_a7_delay_cnt),  // O [14:0]
// --------------------------------------------------------
// CON 8

    
	// --------------------------------------------------------
	// CON 9

	// --------------------------------------------------------
	// CON 10

	// C_TRIG_TYPE_REG
    .c_trig_type(c_trig_type),       // O [1:0]
	// C_START_REG
	.op_start(op_start),			// O
	// emp_log
	.emp_log(emp_log),				// I [7:0]
	// C_FIFO_STAT_REG
    // C_FIFO_PF_THRES_REG
    // C_FIFO_READ_REG
    .din_fifo(log32),                // I [31:0]
    .div_fifo(log32_valid),          // I
    .dir_fifo(),                    // O
    
    // C_SPD12_CNT_REG  
    .c_spad12_cnt(o_spad12_cnt),// I [31:0]
    // C_SPD13_CNT_REG 
    .c_spad13_cnt(o_spad13_cnt), // I [31:0]    
    // C_SPD24_CNT_REG 
    .c_spad24_cnt(o_spad24_cnt), // I [31:0]   
    // C_SPD34_CNT_REG	
    .c_spad34_cnt(o_spad34_cnt),  // I [31:0]   
    // C_SPD1234_CNT_REG	      
    .c_spad1234_cnt(o_spad1234_cnt) // I [31:0] 

);

// ---------------------------------------------------------------------------------
// Reg for pcie_clk
 wire       o_dma_buf_progbfull;
 wire [13:0]dma_buf_prog_thresh;

regs_bsc_pcie regs_bsc_pcie (
	.pcie_clk			(pcie_clk),					// I
	.sclr				(grst),						// I

	.regs_sel			(regs_bsc_pci_sel),			// I
	.regs_adda			(regs_adda),				// I [46:0]
	.regs_adda_valid	(regs_adda_valid),			// I
	.regs_dout			(regs_bsc_pci_data),		// O [31:0]
	.regs_dout_valid	(regs_bsc_pci_data_valid),	// O
	// -------------------------------------------------------
	// -- register ?���?
	// DMA_DATA_SEL_REG
	.data_sel				(data_sel),				// O [1:0]
	.test_cnt_clr			(test_cnt_clr),			// O
	// DMA_FIFO_EMPTY_REG
	.dma_fifo_empty			(dma_fifo_empty),		// I

	// PCIE_DMA_C2H_0_CTRL_REG
	.c2h_0_rst				(c2h_0_rst),			// O
	// PCIE_DMA_C2H_0_SRCADDR_REG
	.c2h_0_src_addr			(c2h_0_src_addr),		// O [31:0]
	// PCIE_DMA_C2H_0_LEN_REG
	.c2h_0_len				(c2h_0_len),			// O [27:0]
	// PCIE_DMA_C2H_0_STAT_REG
	.c2h_0_sts				(c2h_0_sts),			// I [7:0]

	// PCIE_DMA_C2H_0_BUSY_CNT_REG
	.c2h_0_busy_clk_cnt		(c2h_0_busy_clk_cnt),	// I[31:0]
	// PCIE_DMA_C2H_0_RUN_CNT_REG
	.c2h_0_run_clk_cnt		(c2h_0_run_clk_cnt),	// I [31:0]
	// PCIE_DMA_C2H_0_PACKET_CNT_REG
	.c2h_0_packet_cnt		(c2h_0_packet_cnt),		// I [31:0]
	// PCIE_DMA_C2H_0_DESC_CNT_REG
	.c2h_0_desc_cnt			(c2h_0_desc_cnt),		// I [31:0]

    // DMA_BUFFER_PROG_THRESH
    .o_dma_buf_prog_thresh(dma_buf_prog_thresh),  // O [13:0]
    // DMA_BUFFER_FULL
    .i_dma_buf_progbfull(o_dma_buf_progbfull),  // I



	// PCIE_DMA_H2C_0_STAT_REG
	.h2c_0_sts				(h2c_0_sts)				// I [7:0]
);




dma_for_buffer dma_for_buffer (
    .rst(grst|fifo_rst),		// I // multi-clk reset
    .sig_clk(sig_clk),				// I
    .pcie_clk(pcie_clk),			// I
    // signal interface // sig_clk
    .din(log128),					// I [127:0]
    .din_valid(log128_valid),			// I
    .din_ready(),			// O  not use?
    // PCIE interface //pcie_clk
    .dout(o_dma_buf_data),				// O [127:0]
    .dout_valid(o_dma_buf_valid),		// O
    .dout_keep(o_dma_buf_keep),			// O [15:0]   
    .dout_last(o_dma_buf_last),			// O
    .dout_ready(o_dma_buf_ready),			// I
    .i_dma_buf_prog_thresh(dma_buf_prog_thresh), // I [13:0]
    .prog_bfull(o_dma_buf_progbfull)    //O
);












OBUFDS #(
	.IOSTANDARD("DEFAULT"), // Specify the output I/O standard
	.SLEW("FAST")			// Specify the output slew rate
) OBUFDS_A1 (
	.O(GPIO_1_8_PIN[0]),		// Diff_p output (connect directly to top-level port)
	.OB(GPIO_1_8_PIN[1]),		// Diff_n output (connect directly to top-level port)
	.I(1'b1)				// Buffer input
);

OBUFDS #(
	.IOSTANDARD("DEFAULT"), // Specify the output I/O standard
	.SLEW("FAST")			// Specify the output slew rate
) OBUFDS_A2 (
	.O(GPIO_1_8_PIN[2]),		// Diff_p output (connect directly to top-level port)
	.OB(GPIO_1_8_PIN[3]),		// Diff_n output (connect directly to top-level port)
	.I(1'b1)				// Buffer input
);
OBUFDS #(
	.IOSTANDARD("DEFAULT"), // Specify the output I/O standard
	.SLEW("FAST")			// Specify the output slew rate
) OBUFDS_A3 (
	.O(GPIO_1_8_PIN[4]),		// Diff_p output (connect directly to top-level port)
	.OB(GPIO_1_8_PIN[5]),		// Diff_n output (connect directly to top-level port)
	.I(1'b1)				// Buffer input
);
OBUFDS #(
	.IOSTANDARD("DEFAULT"), // Specify the output I/O standard
	.SLEW("FAST")			// Specify the output slew rate
) OBUFDS_A6 (
	.O(GPIO_1_8_PIN[6]),		// Diff_p output (connect directly to top-level port)
	.OB(GPIO_1_8_PIN[7]),		// Diff_n output (connect directly to top-level port)
	.I(1'b1)				// Buffer input
);
OBUFDS #(
	.IOSTANDARD("DEFAULT"), // Specify the output I/O standard
	.SLEW("FAST")			// Specify the output slew rate
) OBUFDS_A7 (
	.O(GPIO_1_8_PIN[8]),		// Diff_p output (connect directly to top-level port)
	.OB(GPIO_1_8_PIN[9]),		// Diff_n output (connect directly to top-level port)
	.I(1'b1)				// Buffer input
);

 //  (*LOC="IDELAYCTRL_X0Y3"*) // Place the logic in the IDELAYCTRLlocated at the IDELAYCTRL_X0Y3 on the XY IDELAYCTRL grid.
// IDELAY_CTRL for SELECTIO
IDELAYCTRL IDELAYCTRL_SELECTIO_8TO1 (

   .RDY(),  // 1-bit output: Ready output

   .REFCLK(clk_200),    // 1-bit input: Reference clock input

   .RST(grst)       // 1-bit input: Active high reset input

);       
// for removing skew
reg c1_spd_trig_out;
reg c2_spd_trig_out;
reg c3_spd_trig_out;
reg c4_spd_trig_out;
always @(posedge sig_clk) begin
    c1_spd_trig_out     <=c1_spd_trig;
    c2_spd_trig_out     <=c2_spd_trig;
    c3_spd_trig_out     <=c3_spd_trig;
    c4_spd_trig_out     <=c4_spd_trig;
end
assign GPIO_3_3_SMA[4:2] = 3'b0;
//assign GPIO_1_8_SMA[4:1] = 4'b0;

//OBUF   f_clk_10_g33sma0_obuf (.O(GPIO_3_3_SMA[0]), .I(sig_clk_10));
assign GPIO_3_3_SMA[0] = sig_start_flag&sig_clk_10M_pulse; // 3.3V Port 2/4 possible sig_clk_10M_pulse
assign GPIO_3_3_SMA[1] = fifo_rst; // 
// assign GPIO_1_8_SMA[0]    GPIO_1_8_SMA[0] for external ref clk input
assign GPIO_1_8_SMA[0] = c1_spd_trig_out;
assign GPIO_1_8_SMA[1] = c1_spd_trig_valid;//c2_spd_trig_out;
assign GPIO_1_8_SMA[2] = c1_mask_10M;//c3_spd_trig_out;
assign GPIO_1_8_SMA[3] = c4_spd_trig_out;
endmodule
