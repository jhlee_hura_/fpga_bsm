`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/07/15 09:22:14
// Design Name: 
// Module Name: simultaneous_coeff_mea
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module simultaneous_coeff_mea(

    input wire  rst,
    input wire  sig_clk,
    
    input wire  spd_trig_1,
    input wire  spd_trig_2,
    input wire  spd_trig_3,
    input wire  spd_trig_4,
    
    output wire spd_trig12,
    output wire spd_trig13,
    output wire spd_trig24,
    output wire spd_trig34,
    
    output reg [31:0]    spad_cnt12_cnt,
    output reg [31:0]    spad_cnt13_cnt,
    output reg [31:0]    spad_cnt24_cnt,        
    output reg [31:0]    spad_cnt34_cnt, 
    output reg [31:0]    spad_cnt1234_cnt     
    );
    
assign spd_trig12 = spd_trig_1 & spd_trig_2;   
assign spd_trig13 = spd_trig_1 & spd_trig_3;   
assign spd_trig24 = spd_trig_2 & spd_trig_4;  
assign spd_trig34 = spd_trig_3 & spd_trig_4;
wire spd_trig1234 = spd_trig_1 & spd_trig_2 & spd_trig_3 & spd_trig_4; 
// ---------------------------------------------------------------------------------
// SPD Valid Trig Signal Count
// ---------------------------------------------------------------------------------
(* mark_debug = "true" *)reg [31:0] spd_12_cnt_tmp;
(* mark_debug = "true" *)reg [31:0] spd_13_cnt_tmp;
(* mark_debug = "true" *)reg [31:0] spd_24_cnt_tmp;
(* mark_debug = "true" *)reg [31:0] spd_34_cnt_tmp;
reg [31:0] spd_1234_cnt_tmp;
reg [31:0]    clk_cnt;
always @(posedge sig_clk) begin
if (rst) begin
        clk_cnt            <= 'b0;
        spad_cnt12_cnt        <= 'b0;
        spad_cnt13_cnt        <= 'b0;
        spad_cnt24_cnt        <= 'b0;
        spad_cnt34_cnt        <= 'b0;        
        spd_12_cnt_tmp    <= 'b0;
        spd_13_cnt_tmp    <= 'b0;
        spd_24_cnt_tmp    <= 'b0;
        spd_34_cnt_tmp    <= 'b0;                 
    end
    else begin
        clk_cnt          <=    (clk_cnt >= (`SIGCLKPERSEC-1'b1)) ? 'b0 : clk_cnt + 1'b1;
        
        spad_cnt12_cnt        <=    (clk_cnt == 'b0) ? spd_12_cnt_tmp : spad_cnt12_cnt;
        spd_12_cnt_tmp    <=    (clk_cnt == 'b0) ? 'b0 :
                            (spd_trig12 ) ? spd_12_cnt_tmp + 1'b1 : spd_12_cnt_tmp;
                            
        spad_cnt13_cnt        <=    (clk_cnt == 'b0) ? spd_13_cnt_tmp : spad_cnt13_cnt;
        spd_13_cnt_tmp    <=    (clk_cnt == 'b0) ? 'b0 :
                            (spd_trig13 ) ? spd_13_cnt_tmp + 1'b1 : spd_13_cnt_tmp;
                             
        spad_cnt24_cnt        <=    (clk_cnt == 'b0) ? spd_24_cnt_tmp : spad_cnt24_cnt;       
        spd_24_cnt_tmp    <=    (clk_cnt == 'b0) ? 'b0 :
                            (spd_trig24) ? spd_24_cnt_tmp + 1'b1 : spd_24_cnt_tmp;
                               
        spad_cnt34_cnt        <=    (clk_cnt == 'b0) ? spd_34_cnt_tmp : spad_cnt34_cnt;       
        spd_34_cnt_tmp    <=    (clk_cnt == 'b0) ? 'b0 :
                            (spd_trig34) ? spd_34_cnt_tmp + 1'b1 : spd_34_cnt_tmp;   
        spad_cnt1234_cnt     <=    (clk_cnt == 'b0) ? spd_1234_cnt_tmp : spad_cnt1234_cnt;       
                            spd_1234_cnt_tmp    <=    (clk_cnt == 'b0) ? 'b0 :
                          (spd_trig1234) ? spd_1234_cnt_tmp + 1'b1 : spd_1234_cnt_tmp;                                                                                      
    end
end
endmodule
