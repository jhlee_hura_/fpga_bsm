`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: HURA
// Engineer:  LEE Seong Yun (sylee@hura.co.kr)
//
// Create Date: 2019/01/15 21:04:48
// Design Name: register for system clock domain
// Module Name: regs_sys_alice
// Project Name: QKD
// Target Devices:
// Tool Versions: Vivado 2018.1
// Description:
//
// Dependencies:
//
// Revision:
// 		build 1 (2019/01/15)
// Additional Comments:
//
// Copyright 2018, All rights reserved by HURA
//////////////////////////////////////////////////////////////////////////////////
module regs_sys_charlie(
	input 	wire 				clk,		// sys_clk
	input 	wire 				rst,
	input	wire				pcie_clk,

	input	wire  				regs_sel,
	input	wire	[46:0]		regs_adda,
	input	wire  				regs_adda_valid,
	output	wire	[31:0]		regs_dout,
	output	wire				regs_dout_valid,

	// -------------------------------------------------------
	// -- register ?���?
	// BSC_TEMP_REG
	output reg				led_test,
	output reg				led1_on,
	output reg				led2_on,
	output reg				led3_on,

	// LM70_SPI_WR_REG
	output reg				lm70_spi_valid,
	// LM70_SPI_RD_REG
	input  wire		[15:0]	lm70_spi_rd,
	input  wire				lm70_spi_busy,

	// FAN_HIGH_DURA_REG
	output reg		[15:0]	fan_high_dura,
	// FAN_LOW_DURA_REG
	output reg		[15:0]	fan_low_dura,

	// FAN1_HIGH_DURA_REG
	output reg		[15:0]	fan1_high_dura,
	// FAN1_LOW_DURA_REG
	output reg		[15:0]	fan1_low_dura,

	// --------------------------------------------------------
	// CON 1
	// C1_HMC920_EN_REG
    output reg                c1_hmc920_en,

    // C1_SPD_EN_REG
    output reg                c1_spd_en,
    
    // C1_SIP_EN_REG
    output reg                c1_sip_en,

    // C1_LTC1867_SPI_WR_REG
    output reg        [15:0]    c1_ltc1867_spi_wr,
    output reg                c1_ltc1867_spi_valid,
    // C1_LTC1867_SPI_RD_REG
    input wire        [15:0]    c1_ltc1867_spi_rd,
    input wire                c1_ltc1867_spi_busy,

    // C1_SPD_DAC_WR_REG
    output reg        [23:0]    c1_spd_dac_wr,
    output reg                c1_spd_dac_valid,

    // C1_SIP_DAC_WR_REG
    output reg        [23:0]    c1_sip_dac_wr,
    output reg                c1_sip_dac_valid,

    // C1_TEC_EN_REG
    output reg                c1_tec_en,

    // C1_TEC_DAC_WR_REG
    output reg        [15:0]    c1_tec_dac_wr,
    output reg                c1_tec_dac_valid,

    // C1_TEC_DAC_CLR_REG
    output reg                c1_tec_dac_clr,
    
    // C1_DP_WR_REG
    output    reg [23:0]         c1_dp_wr,
    output    reg             c1_dp_valid,
    
    // C1_DP_RD_REG
    input    wire [7:0]        c1_dp_rd,
	// --------------------------------------------------------
	// CON 2
    // C2_HMC920_EN_REG
    output reg                c2_hmc920_en,

    // C2_SPD_EN_REG
    output reg                c2_spd_en,
    
    // C2_SIP_EN_REG
    output reg                c2_sip_en,

    // C2_LTC1867_SPI_WR_REG
    output reg        [15:0]    c2_ltc1867_spi_wr,
    output reg                c2_ltc1867_spi_valid,
    // C2_LTC1867_SPI_RD_REG
    input wire        [15:0]    c2_ltc1867_spi_rd,
    input wire                c2_ltc1867_spi_busy,

    // C2_SPD_DAC_WR_REG
    output reg        [23:0]    c2_spd_dac_wr,
    output reg                c2_spd_dac_valid,

    // C2_SIP_DAC_WR_REG
    output reg        [23:0]    c2_sip_dac_wr,
    output reg                c2_sip_dac_valid,

    // C2_TEC_EN_REG
    output reg                c2_tec_en,

    // C2_TEC_DAC_WR_REG
    output reg        [15:0]    c2_tec_dac_wr,
    output reg                c2_tec_dac_valid,

    // C2_TEC_DAC_CLR_REG
    output reg                c2_tec_dac_clr,
    
    // C2_DP_WR_REG
    output    reg [23:0]         c2_dp_wr,
    output    reg             c2_dp_valid,
    
    // C2_DP_RD_REG
    input    wire [7:0]        c2_dp_rd,
	// --------------------------------------------------------
	// CON 3
	// C3_HMC920_EN_REG
    output reg                c3_hmc920_en,

    // C3_SPD_EN_REG
    output reg                c3_spd_en,
    
    // C3_SIP_EN_REG
    output reg                c3_sip_en,

    // C3_LTC1867_SPI_WR_REG
    output reg        [15:0]  c3_ltc1867_spi_wr,
    output reg                c3_ltc1867_spi_valid,
    // C3_LTC1867_SPI_RD_REG
    input wire        [15:0]  c3_ltc1867_spi_rd,
    input wire                c3_ltc1867_spi_busy,

    // C3_SPD_DAC_WR_REG
    output reg        [23:0]  c3_spd_dac_wr,
    output reg                c3_spd_dac_valid,

    // C3_SIP_DAC_WR_REG
    output reg        [23:0]  c3_sip_dac_wr,
    output reg                c3_sip_dac_valid,

    // C3_TEC_EN_REG
    output reg                c3_tec_en,

    // C3_TEC_DAC_WR_REG
    output reg        [15:0]  c3_tec_dac_wr,
    output reg                c3_tec_dac_valid,

    // C3_TEC_DAC_CLR_REG
    output reg                c3_tec_dac_clr,
    
    // C3_DP_WR_REG
    output    reg [23:0]      c3_dp_wr,
    output    reg             c3_dp_valid,
    
    // C3_DP_RD_REG
    input    wire [7:0]        c3_dp_rd,
// --------------------------------------------------------
// CON 4
	// C4_HMC920_EN_REG
    output reg                c4_hmc920_en,
    
    // C4_SPD_EN_REG
    output reg                c4_spd_en,
    
    // C4_SIP_EN_REG
    output reg                c4_sip_en,
    
    // C4_LTC1867_SPI_WR_REG
    output reg        [15:0]    c4_ltc1867_spi_wr,
    output reg                c4_ltc1867_spi_valid,
    // C4_LTC1867_SPI_RD_REG
    input wire        [15:0]    c4_ltc1867_spi_rd,
    input wire                c4_ltc1867_spi_busy,
    
    // C4_SPD_DAC_WR_REG
    output reg        [23:0]    c4_spd_dac_wr,
    output reg                c4_spd_dac_valid,
    
    // C4_SIP_DAC_WR_REG
    output reg        [23:0]    c4_sip_dac_wr,
    output reg                c4_sip_dac_valid,
    
    // C4_TEC_EN_REG
    output reg                c4_tec_en,
    
    // C4_TEC_DAC_WR_REG
    output reg        [15:0]    c4_tec_dac_wr,
    output reg                c4_tec_dac_valid,
    
    // C4_TEC_DAC_CLR_REG
    output reg                c4_tec_dac_clr,
    
    // C4_DP_WR_REG
    output    reg [23:0]         c4_dp_wr,
    output    reg             c4_dp_valid,
    
    // C4_DP_RD_REG
    input    wire [7:0]        c4_dp_rd,
	// --------------------------------------------------------
	// CON 5
	// C5_FPC_EN_REG
    output reg                c5_fpc_en,
    // C5_FPC_DAC_WR_REG
    output reg        [31:0]    c5_fpc_dac_din,
    output reg                c5_fpc_dac_din_valid,
    // C5_FPC_DAC_CLR_REG
    output reg                c5_fpc_dac_clr,

    // C5_FPC10_DAC_WR_REG
    output reg        [31:0]    c5_fpc10_dac_din,
    output reg                c5_fpc10_dac_din_valid,
    // C5_FPC10_DAC_CLR_REG
    output reg                c5_fpc10_dac_clr,

	// --------------------------------------------------------
	// CON 6
   // C6_HMC920_EN_REG
    output reg                c6_hmc920_en,
 
    // C6_LTC1867_SPI_WR_REG
    output reg        [15:0]    c6_ltc1867_spi_wr,
    output reg                c6_ltc1867_spi_valid,
    // C6_LTC1867_SPI_RD_REG
    input wire        [15:0]    c6_ltc1867_spi_rd,
    input wire                c6_ltc1867_spi_busy,
 
    // C6_AD5761_DAC_WR_REG
    output reg        [23:0]    c6_ad5761_dac_wr,
    output reg                c6_ad5761_dac_valid,
 
    // C6_AD5761_DAC_RD_REG
    input wire        [23:0]    c6_ad5761_dac_rd,
 
    // C6_TEC_EN_REG
    output reg                c6_tec_en,
 
    // C6_TEC_DAC_WR_REG
    output reg        [15:0]    c6_tec_dac_wr,
    output reg                c6_tec_dac_valid,
 
    // C6_TEC_DAC_CLR_REG
    output reg                c6_tec_dac_clr,
    
    // C6_DP_WR_REG
    output    reg [23:0]         c6_dp_wr,
    output    reg             c6_dp_valid,
    
    // C6_DP_RD_REG
    input    wire [7:0]        c6_dp_rd,  	

      
// --------------------------------------------------------
// CON 7
  // C7_HMC920_EN_REG
  output reg                c7_hmc920_en,

  // C7_LTC1867_SPI_WR_REG
  output reg        [15:0]    c7_ltc1867_spi_wr,
  output reg                c7_ltc1867_spi_valid,
  // C7_LTC1867_SPI_RD_REG
  input wire        [15:0]    c7_ltc1867_spi_rd,
  input wire                c7_ltc1867_spi_busy,

  // C7_AD5761_DAC_WR_REG
  output reg        [23:0]    c7_ad5761_dac_wr,
  output reg                c7_ad5761_dac_valid,

  // C7_AD5761_DAC_RD_REG
  input wire        [23:0]    c7_ad5761_dac_rd,

  // C7_TEC_EN_REG
  output reg                c7_tec_en,

  // C7_TEC_DAC_WR_REG
  output reg        [15:0]    c7_tec_dac_wr,
  output reg                c7_tec_dac_valid,

  // C7_TEC_DAC_CLR_REG
  output reg                c7_tec_dac_clr,
  
  // C7_DP_WR_REG
  output    reg [23:0]         c7_dp_wr,
  output    reg             c7_dp_valid,
  
  // C7_DP_RD_REG
  input    wire [7:0]        c7_dp_rd,       
// --------------------------------------------------------
// CON 8
    // C8_FPC_EN_REG
    output reg                c8_fpc_en,
    // C8_FPC_DAC_WR_REG
    output reg        [31:0]    c8_fpc_dac_din,
    output reg                c8_fpc_dac_din_valid,
    // C8_FPC_DAC_CLR_REG
    output reg                c8_fpc_dac_clr,
    
    // C8_FPC10_DAC_WR_REG
    output reg        [31:0]    c8_fpc10_dac_din,
    output reg                c8_fpc10_dac_din_valid,
    // C8_FPC10_DAC_CLR_REG
    output reg                c8_fpc10_dac_clr
// --------------------------------------------------------
// CON 9
        
// --------------------------------------------------------
// CON 10
	
);

wire 	[46:0]	fifo_dout;
wire			regin_empty;
wire			regout_empty;
reg				regout_we;
reg				regout_we1;

wire	[15:0]	addr;
wire			wr_en;
wire	[31:0]	wr_d;
reg 	[31:0]	rd_d;
wire			regin_valid;

// from pcie block
fifo_regin_47x16 regin_fifo (
  .rst(rst),        					// I
  .wr_clk(pcie_clk),  					// I
  .rd_clk(clk),  						// I
  .din(regs_adda),        				// I [46 : 0]
  .wr_en(regs_sel & regs_adda_valid),	// I
  .rd_en(~regin_empty),    				// I
  .dout(fifo_dout),      				// O [46 : 0]
  .full(),      						// O
  .empty(regin_empty),    				// O
  .valid(regin_valid)    				// O
);

// to pcie block	// Standard
fifo_regout_32x16 regout_fifo (
  .rst(rst),        					// I
  .wr_clk(clk),  						// I
  .rd_clk(pcie_clk),  					// I
  .din(rd_d),        					// I [31 : 0]
  .wr_en(regout_we),    				// I
  .rd_en(~regout_empty),    			// I
  .dout(regs_dout),      				// O [31 : 0]
  .full(),      						// O
  .empty(regout_empty),    				// O
  .valid(regs_dout_valid)    			// O
);

always @(posedge clk) begin
	regout_we1 	<= regin_valid & ~fifo_dout[46];
	regout_we 	<= regout_we1;
end

// fifo[46]= 1:wr_en, 0:rd_en
// fifo[45:32] = 14bits addr
// fifo[31:0] = 32bits data
assign wr_en 	= regin_valid & fifo_dout[46];
assign addr 	= {fifo_dout[45:32], 2'b0}; // 14bits to 16bits address
assign wr_d 	= fifo_dout[31:0];

//BSC_VERSION_REG
reg [31:0]	version_date=32'b0;
reg [31:0]	version_dat;
//TEST_PAD_REG
reg [31:0]	pad_reg;

reg clk_sel_rst_r = 1'b0;

always @(posedge clk ) begin
	if (rst)	begin
		rd_d 					<= 32'b0;
		// BSC_VERSION_REG
		version_dat				<= 0;
		// TEST_PAD_REG
		pad_reg					<= 32'b0;

		// BSC_TEMP_REG
		led_test				<= 1'b0;
		led1_on					<= 1'b1;
		led2_on					<= 1'b0;
		led3_on					<= 1'b0;

		// LM70_SPI_WR_REG
		lm70_spi_valid			<= 1'b0;

		// FAN_HIGH_DURA_REG
		fan_high_dura 			<= `FAN_HIGH_DURA_DEF;
		// FAN_LOW_DURA_REG
		fan_low_dura 			<= `FAN_LOW_DURA_DEF;

		// FAN1_HIGH_DURA_REG
		fan1_high_dura 			<= `FAN_HIGH_DURA_DEF;
		// FAN1_LOW_DURA_REG
		fan1_low_dura 			<= `FAN_LOW_DURA_DEF;
		
// CON 1 : 
		// C1_HMC920_EN_REG
        c1_hmc920_en            <= 1'b0;
        
        // C1_SPD_EN_REG
        c1_spd_en                <= 1'b0;
        // C1_SIP_EN_REG
        c1_sip_en                <= 1'b0;
        
        // C1_LTC1867_SPI_WR_REG
        c1_ltc1867_spi_wr        <= 16'b0;
        c1_ltc1867_spi_valid    <= 1'b0;
        
        // C1_SPD_DAC_WR_REG
        c1_spd_dac_wr            <= 23'b0;
        c1_spd_dac_valid        <= 1'b0;
        
        // C1_SIP_DAC_WR_REG
        c1_sip_dac_wr            <= 23'b0;
        c1_sip_dac_valid        <= 1'b0;
        
        // C1_TEC_EN_REG
        c1_tec_en                <= 1'b0;
        // C1_TEC_DAC_WR_REG
        c1_tec_dac_wr            <= 16'b0;
        c1_tec_dac_valid        <= 1'b0;
        // C1_TEC_DAC_CLR_REG
        c1_tec_dac_clr            <= 1'b0;
        
        // C1_DP_WR_REG
        c1_dp_wr        <= 8'b0;
        c1_dp_valid        <= 1'b0;
        
  // CON 2 :       
		// C2_HMC920_EN_REG
          c2_hmc920_en            <= 1'b0;
        
          // C2_SPD_EN_REG
          c2_spd_en                <= 1'b0;
          // C2_SIP_EN_REG
          c2_sip_en                <= 1'b0;
        
          // C2_LTC1867_SPI_WR_REG
          c2_ltc1867_spi_wr        <= 16'b0;
          c2_ltc1867_spi_valid    <= 1'b0;
        
          // C2_SPD_DAC_WR_REG
          c2_spd_dac_wr            <= 23'b0;
          c2_spd_dac_valid        <= 1'b0;
        
          // C2_SIP_DAC_WR_REG
          c2_sip_dac_wr            <= 23'b0;
          c2_sip_dac_valid        <= 1'b0;
        
          // C2_TEC_EN_REG
          c2_tec_en                <= 1'b0;
          // C2_TEC_DAC_WR_REG
          c2_tec_dac_wr            <= 16'b0;
          c2_tec_dac_valid        <= 1'b0;
          // C2_TEC_DAC_CLR_REG
          c2_tec_dac_clr            <= 1'b0;
          
          // C2_DP_WR_REG
          c2_dp_wr        <= 8'b0;
          c2_dp_valid        <= 1'b0;
// CON 3 :        
		// C3_HMC920_EN_REG
        c3_hmc920_en            <= 1'b0;
        
        // C3_SPD_EN_REG
        c3_spd_en                <= 1'b0;
        // C3_SIP_EN_REG
        c3_sip_en                <= 1'b0;
        
        // C3_LTC1867_SPI_WR_REG
        c3_ltc1867_spi_wr        <= 16'b0;
        c3_ltc1867_spi_valid    <= 1'b0;
        
        // C3_SPD_DAC_WR_REG
        c3_spd_dac_wr            <= 23'b0;
        c3_spd_dac_valid        <= 1'b0;
        
        // C3_SIP_DAC_WR_REG
        c3_sip_dac_wr            <= 23'b0;
        c3_sip_dac_valid        <= 1'b0;
        
        // C3_TEC_EN_REG
        c3_tec_en                <= 1'b0;
        // C3_TEC_DAC_WR_REG
        c3_tec_dac_wr            <= 16'b0;
        c3_tec_dac_valid        <= 1'b0;
        // C3_TEC_DAC_CLR_REG
        c3_tec_dac_clr            <= 1'b0;
        
        // C3_DP_WR_REG
        c3_dp_wr        <= 8'b0;
        c3_dp_valid        <= 1'b0;
// CON 4 : 
		// C4_HMC920_EN_REG
        c4_hmc920_en            <= 1'b0;
        
        // C4_SPD_EN_REG
        c4_spd_en                <= 1'b0;
        // C4_SIP_EN_REG
        c4_sip_en                <= 1'b0;
        
        // C4_LTC1867_SPI_WR_REG
        c4_ltc1867_spi_wr        <= 16'b0;
        c4_ltc1867_spi_valid    <= 1'b0;
        
        // C4_SPD_DAC_WR_REG
        c4_spd_dac_wr            <= 23'b0;
        c4_spd_dac_valid        <= 1'b0;
        
        // C4_SIP_DAC_WR_REG
        c4_sip_dac_wr            <= 23'b0;
        c4_sip_dac_valid        <= 1'b0;
        
        // C4_TEC_EN_REG
        c4_tec_en                <= 1'b0;
        // C4_TEC_DAC_WR_REG
        c4_tec_dac_wr            <= 16'b0;
        c4_tec_dac_valid        <= 1'b0;
        // C4_TEC_DAC_CLR_REG
        c4_tec_dac_clr            <= 1'b0;
        
        // C4_DP_WR_REG
        c4_dp_wr        <= 8'b0;
        c4_dp_valid        <= 1'b0;
        
// CON 5 :
		// C5_FPC_EN_REG
        c5_fpc_en                <= 1'b0;
        
        // C5_FPC_DAC_WR_REG
        c5_fpc_dac_din            <= 32'b0;
        c5_fpc_dac_din_valid        <= 1'b0;
        
        // C5_FPC_DAC_CLR_REG
        c5_fpc_dac_clr            <= 1'b0;
        
        // C5_FPC10_DAC_WR_REG
        c5_fpc10_dac_din        <= 32'b0;
        c5_fpc10_dac_din_valid    <= 1'b0;
        
        // C5_FPC10_DAC_CLR_REG
        c5_fpc10_dac_clr        <= 1'b0;

 // CON 6 :             
        // C6_HMC920_EN_REG
      c6_hmc920_en            <= 1'b0;
     
      // C6_LTC1867_SPI_WR_REG
      c6_ltc1867_spi_wr        <= 16'b0;
      c6_ltc1867_spi_valid    <= 1'b0;
     
      // c6_AD5761_DAC_WR_REG
      c6_ad5761_dac_wr        <= 24'b0;
      c6_ad5761_dac_valid        <= 1'b0;
     
      // C6_DP_WR_REG
      c6_dp_wr        <= 8'b0;
      c6_dp_valid        <= 1'b0;    
      
      // C6_TEC_EN_REG
      c6_tec_en                <= 1'b0;
     
      // C6_TEC_DAC_WR_REG
      c6_tec_dac_wr            <= 16'b0;
      c6_tec_dac_valid        <= 1'b0;
     
      // c6_TEC_DAC_CLR_REG
      c6_tec_dac_clr            <= 1'b0;
  
// CON 7 :       
       // C7_HMC920_EN_REG
       c7_hmc920_en            <= 1'b0;
    
       // C7_LTC1867_SPI_WR_REG
       c7_ltc1867_spi_wr        <= 16'b0;
       c7_ltc1867_spi_valid    <= 1'b0;
    
       // c7_AD5761_DAC_WR_REG
       c7_ad5761_dac_wr        <= 24'b0;
       c7_ad5761_dac_valid        <= 1'b0;
    
       // C7_DP_WR_REG
       c7_dp_wr        <= 8'b0;
       c7_dp_valid        <= 1'b0;    
       
       // C7_TEC_EN_REG
       c7_tec_en                <= 1'b0;
    
       // C7_TEC_DAC_WR_REG
       c7_tec_dac_wr            <= 16'b0;
       c7_tec_dac_valid        <= 1'b0;
    
       // c7_TEC_DAC_CLR_REG
       c7_tec_dac_clr            <= 1'b0;
	  
 // CON 8 :
 		// C8_FPC_EN_REG
      c8_fpc_en                <= 1'b0;
      
      // C8_FPC_DAC_WR_REG
      c8_fpc_dac_din            <= 32'b0;
      c8_fpc_dac_din_valid        <= 1'b0;
      
      // C8_FPC_DAC_CLR_REG
      c8_fpc_dac_clr            <= 1'b0;
      
      // C8_FPC10_DAC_WR_REG
      c8_fpc10_dac_din        <= 32'b0;
      c8_fpc10_dac_din_valid    <= 1'b0;
      
      // C8_FPC10_DAC_CLR_REG
      c8_fpc10_dac_clr        <= 1'b0;
     
 
 // CON 10 :             
	end
	else begin
		case (addr)
			`BOARD_MAKER_REG : begin
				rd_d[31:0] <= `BOARD_MAKER;
			end

			`BSC_VERSION_REG : begin
				rd_d[31:0] <= version_date;
				if (wr_en)
					version_dat <= wr_d;
			end

			`BOARD_NAME_REG : begin
				rd_d[31:0] <= `BOARD_NAME;
			end

			`BOARD_NAME1_REG : begin
				rd_d[31:0] <= `BOARD_NAME1;
			end

			`BOARD_NAME2_REG : begin
				rd_d[31:0] <= `BOARD_NAME2;
			end

			`BOARD_NAME3_REG : begin
				rd_d[31:0] <= `BOARD_NAME3;
			end

			`BOARD_NAME4_REG : begin
				rd_d[31:0] <= `BOARD_NAME4;
			end

			`BOARD_NAME5_REG : begin
				rd_d[31:0] <= `BOARD_NAME5;
			end

			`BOARD_NAME6_REG : begin
				rd_d[31:0] <= `BOARD_NAME6;
			end

			`BOARD_NAME7_REG : begin
				rd_d[31:0] <= `BOARD_NAME7;
			end

			`BOARD_NAME8_REG : begin
				rd_d[31:0] <= `BOARD_NAME8;
			end

			`BOARD_NAME9_REG : begin
				rd_d[31:0] <= `BOARD_NAME9;
			end

			`BOARD_NAME10_REG : begin
				rd_d[31:0] <= `BOARD_NAME10;
			end

			`BOARD_NAME11_REG : begin
				rd_d[31:0] <= `BOARD_NAME11;
			end

			`TEST_PAD_REG : begin
				rd_d[31:0] <= pad_reg;
				if (wr_en)
					pad_reg <= ~wr_d;
			end

			`COPYRIGHTER_REG : begin
				rd_d[31:0] <= `COPYRIGHTER;
			end

			`VERSION_REG : begin
				rd_d[31:0] <= `VERSION_DATE_DATA;
			end

			`PRODUCT_REG : begin
				rd_d[31:0] <= `PRODUCT;
			end

			`PRODUCT1_REG : begin
				rd_d[31:0] <= `PRODUCT1;
			end

			`PRODUCT2_REG : begin
				rd_d[31:0] <= `PRODUCT2;
			end

			`PRODUCT3_REG : begin
				rd_d[31:0] <= `PRODUCT3;
			end

			`PRODUCT4_REG : begin
				rd_d[31:0] <= `PRODUCT4;
			end

			`PRODUCT5_REG : begin
				rd_d[31:0] <= `PRODUCT5;
			end

			`PRODUCT6_REG : begin
				rd_d[31:0] <= `PRODUCT6;
			end

			`PRODUCT7_REG : begin
				rd_d[31:0] <= `PRODUCT7;
			end

			`PRODUCT8_REG : begin
				rd_d[31:0] <= `PRODUCT8;
			end

			`PRODUCT9_REG : begin
				rd_d[31:0] <= `PRODUCT9;
			end

			`PRODUCT10_REG : begin
				rd_d[31:0] <= `PRODUCT10;
			end

			`PRODUCT11_REG : begin
				rd_d[31:0] <= `PRODUCT11;
			end

			`BYTESWAP_REG : begin
				rd_d[31:0] <= pad_reg;
				if (wr_en)
					pad_reg <= {wr_d[7:0], wr_d[15:8], wr_d[23:16], wr_d[31:24]};
			end

			`BSC_TEMP_REG	: begin
				rd_d[0] <= led_test;
				rd_d[1] <= led1_on;
				rd_d[2] <= led2_on;
				rd_d[3] <= led3_on;
				rd_d[31:4] <= 28'b0;
				if (wr_en) begin
					led_test <= wr_d[0];
					led1_on <= wr_d[1];
					led2_on <= wr_d[2];
					led3_on <= wr_d[3];
				end
			end

			`LM70_SPI_WR_REG : begin
				if (wr_en) begin
					lm70_spi_valid <= 1'b1;
				end
			end

			`LM70_SPI_RD_REG : begin
				rd_d[15:0] 	<= lm70_spi_rd;
				rd_d[30:16]	<= 'b0;
				rd_d[31]	<= lm70_spi_busy;
			end

			`FAN_HIGH_DURA_REG : begin
				rd_d[15:0]	<= fan_high_dura;
				rd_d[31:16] <= 16'b0;
				if (wr_en) begin
					fan_high_dura	<= wr_d[15:0];
				end
			end

			`FAN_LOW_DURA_REG : begin
				rd_d[15:0]	<= fan_low_dura;
				rd_d[31:16] <= 16'b0;
				if (wr_en) begin
					fan_low_dura	<= wr_d[15:0];
				end
			end

			`FAN1_HIGH_DURA_REG : begin
				rd_d[15:0]	<= fan1_high_dura;
				rd_d[31:16] <= 16'b0;
				if (wr_en) begin
					fan1_high_dura	<= wr_d[15:0];
				end
			end

			`FAN1_LOW_DURA_REG : begin
				rd_d[15:0]	<= fan1_low_dura;
				rd_d[31:16] <= 16'b0;
				if (wr_en) begin
					fan1_low_dura	<= wr_d[15:0];
				end
			end
// CON 1
			`C1_HMC920_EN_REG : begin
				rd_d[0] <= c1_hmc920_en;
				rd_d[31:1] <= 31'b0;
				if (wr_en) begin
					c1_hmc920_en <= wr_d[0];
				end
			end		

			`C1_SPD_EN_REG : begin
				rd_d[0] <= c1_spd_en;
				rd_d[31:1] <= 31'b0;
				if (wr_en) begin
					c1_spd_en <= wr_d[0];
				end
			end

			`C1_SIP_EN_REG : begin
				rd_d[0] <= c1_sip_en;
				rd_d[31:1] <= 31'b0;
				if (wr_en) begin
					c1_sip_en <= wr_d[0];
				end
			end

			`C1_LTC1867_SPI_WR_REG : begin
				rd_d[15:0]	<= c1_ltc1867_spi_wr;
				rd_d[31:16] <= 16'b0;
				if (wr_en) begin
					c1_ltc1867_spi_wr		<= wr_d[15:0];
					c1_ltc1867_spi_valid 	<= 1'b1;
				end
			end

			`C1_LTC1867_SPI_RD_REG : begin
				rd_d[15:0] 	<= c1_ltc1867_spi_rd;
				rd_d[30:16] <= 15'b0;
				rd_d[31] 	<= c1_ltc1867_spi_busy;
			end

			`C1_SPD_DAC_WR_REG : begin
				rd_d[23:0] <= c1_spd_dac_wr;
				rd_d[31:24] <= 8'b0;
				if (wr_en) begin
					c1_spd_dac_wr		<= wr_d[23:0];
					c1_spd_dac_valid 	<= 1'b1;
				end
			end

			`C1_SIP_DAC_WR_REG : begin
				rd_d[23:0] <= c1_sip_dac_wr;
				rd_d[31:24] <= 8'b0;
				if (wr_en) begin
					c1_sip_dac_wr		<= wr_d[23:0];
					c1_sip_dac_valid 	<= 1'b1;
				end
			end

			`C1_TEC_EN_REG : begin
				rd_d[0] <= c1_tec_en;
				rd_d[31:1] <= 31'b0;
				if (wr_en) begin
					c1_tec_en <= wr_d[0];
				end
			end
            `C1_TEC_DAC_WR_REG : begin
                rd_d[15:0] <= c1_tec_dac_wr;
                rd_d[31:16] <= 16'b0;
                if (wr_en) begin
                    c1_tec_dac_wr        <= wr_d[15:0];
                    c1_tec_dac_valid     <= 1'b1;
                end
            end

            `C1_TEC_DAC_CLR_REG : begin
                rd_d[0] <= c1_tec_dac_clr;
                rd_d[31:1] <= 31'b0;
                if (wr_en) begin
                    c1_tec_dac_clr <= wr_d[0];
                end
            end
            
            `C1_DP_WR_REG : begin
                rd_d[23:0] <= c1_dp_wr;
                rd_d[31:24] <= 8'b0;
                if (wr_en) begin
                    c1_dp_wr        <= wr_d[23:0];
                    c1_dp_valid     <= 1'b1;
                end
            end
            `C1_DP_RD_REG : begin
                rd_d[7:0]     <= c1_dp_rd;
                rd_d[31:24]    <= 8'b0;
            end			
// CON 2            
			`C2_HMC920_EN_REG : begin
                rd_d[0] <= c2_hmc920_en;
                rd_d[31:1] <= 31'b0;
                if (wr_en) begin
                    c2_hmc920_en <= wr_d[0];
                end
            end        

            `C2_SPD_EN_REG : begin
                rd_d[0] <= c2_spd_en;
                rd_d[31:1] <= 31'b0;
                if (wr_en) begin
                    c2_spd_en <= wr_d[0];
                end
            end

            `C2_SIP_EN_REG : begin
                rd_d[0] <= c2_sip_en;
                rd_d[31:1] <= 31'b0;
                if (wr_en) begin
                    c2_sip_en <= wr_d[0];
                end
            end

            `C2_LTC1867_SPI_WR_REG : begin
                rd_d[15:0]    <= c2_ltc1867_spi_wr;
                rd_d[31:16] <= 16'b0;
                if (wr_en) begin
                    c2_ltc1867_spi_wr        <= wr_d[15:0];
                    c2_ltc1867_spi_valid     <= 1'b1;
                end
            end

            `C2_LTC1867_SPI_RD_REG : begin
                rd_d[15:0]     <= c2_ltc1867_spi_rd;
                rd_d[30:16] <= 15'b0;
                rd_d[31]     <= c2_ltc1867_spi_busy;
            end

            `C2_SPD_DAC_WR_REG : begin
                rd_d[23:0] <= c2_spd_dac_wr;
                rd_d[31:24] <= 8'b0;
                if (wr_en) begin
                    c2_spd_dac_wr        <= wr_d[23:0];
                    c2_spd_dac_valid     <= 1'b1;
                end
            end

            `C2_SIP_DAC_WR_REG : begin
                rd_d[23:0] <= c2_sip_dac_wr;
                rd_d[31:24] <= 8'b0;
                if (wr_en) begin
                    c2_sip_dac_wr        <= wr_d[23:0];
                    c2_sip_dac_valid     <= 1'b1;
                end
            end

            `C2_TEC_EN_REG : begin
                rd_d[0] <= c2_tec_en;
                rd_d[31:1] <= 31'b0;
                if (wr_en) begin
                    c2_tec_en <= wr_d[0];
                end
            end

            `C2_TEC_DAC_WR_REG : begin
                rd_d[15:0] <= c2_tec_dac_wr;
                rd_d[31:16] <= 16'b0;
                if (wr_en) begin
                    c2_tec_dac_wr        <= wr_d[15:0];
                    c2_tec_dac_valid     <= 1'b1;
                end
            end

            `C2_TEC_DAC_CLR_REG : begin
                rd_d[0] <= c2_tec_dac_clr;
                rd_d[31:1] <= 31'b0;
                if (wr_en) begin
                    c2_tec_dac_clr <= wr_d[0];
                end
            end
            
            `C2_DP_WR_REG : begin
                rd_d[23:0] <= c2_dp_wr;
                rd_d[31:24] <= 8'b0;
                if (wr_en) begin
                    c2_dp_wr        <= wr_d[23:0];
                    c2_dp_valid     <= 1'b1;
                end
            end
            `C2_DP_RD_REG : begin
                rd_d[7:0]     <= c2_dp_rd;
                rd_d[31:24]    <= 8'b0;
            end
// CON 3            
			`C3_HMC920_EN_REG : begin
                rd_d[0] <= c3_hmc920_en;
                rd_d[31:1] <= 31'b0;
                if (wr_en) begin
                    c3_hmc920_en <= wr_d[0];
                end
            end        

            `C3_SPD_EN_REG : begin
                rd_d[0] <= c3_spd_en;
                rd_d[31:1] <= 31'b0;
                if (wr_en) begin
                    c3_spd_en <= wr_d[0];
                end
            end

            `C3_SIP_EN_REG : begin
                rd_d[0] <= c3_sip_en;
                rd_d[31:1] <= 31'b0;
                if (wr_en) begin
                    c3_sip_en <= wr_d[0];
                end
            end

            `C3_LTC1867_SPI_WR_REG : begin
                rd_d[15:0]    <= c3_ltc1867_spi_wr;
                rd_d[31:16] <= 16'b0;
                if (wr_en) begin
                    c3_ltc1867_spi_wr        <= wr_d[15:0];
                    c3_ltc1867_spi_valid     <= 1'b1;
                end
            end

            `C3_LTC1867_SPI_RD_REG : begin
                rd_d[15:0]     <= c3_ltc1867_spi_rd;
                rd_d[30:16] <= 15'b0;
                rd_d[31]     <= c3_ltc1867_spi_busy;
            end

            `C3_SPD_DAC_WR_REG : begin
                rd_d[23:0] <= c3_spd_dac_wr;
                rd_d[31:24] <= 8'b0;
                if (wr_en) begin
                    c3_spd_dac_wr        <= wr_d[23:0];
                    c3_spd_dac_valid     <= 1'b1;
                end
            end

            `C3_SIP_DAC_WR_REG : begin
                rd_d[23:0] <= c3_sip_dac_wr;
                rd_d[31:24] <= 8'b0;
                if (wr_en) begin
                    c3_sip_dac_wr        <= wr_d[23:0];
                    c3_sip_dac_valid     <= 1'b1;
                end
            end

            `C3_TEC_EN_REG : begin
                rd_d[0] <= c3_tec_en;
                rd_d[31:1] <= 31'b0;
                if (wr_en) begin
                    c3_tec_en <= wr_d[0];
                end
            end

            `C3_TEC_DAC_WR_REG : begin
                rd_d[15:0] <= c3_tec_dac_wr;
                rd_d[31:16] <= 16'b0;
                if (wr_en) begin
                    c3_tec_dac_wr        <= wr_d[15:0];
                    c3_tec_dac_valid     <= 1'b1;
                end
            end

            `C3_TEC_DAC_CLR_REG : begin
                rd_d[0] <= c3_tec_dac_clr;
                rd_d[31:1] <= 31'b0;
                if (wr_en) begin
                    c3_tec_dac_clr <= wr_d[0];
                end
            end
            
            `C3_DP_WR_REG : begin
                rd_d[23:0] <= c3_dp_wr;
                rd_d[31:24] <= 8'b0;
                if (wr_en) begin
                    c3_dp_wr        <= wr_d[23:0];
                    c3_dp_valid     <= 1'b1;
                end
            end
            `C3_DP_RD_REG : begin
                rd_d[7:0]     <= c3_dp_rd;
                rd_d[31:24]    <= 8'b0;
            end
//CON 4 :  
            
            `C4_HMC920_EN_REG : begin
                rd_d[0] <= c4_hmc920_en;
                rd_d[31:1] <= 31'b0;
                if (wr_en) begin
                    c4_hmc920_en <= wr_d[0];
                end
            end        
    
            `C4_SPD_EN_REG : begin
                rd_d[0] <= c4_spd_en;
                rd_d[31:1] <= 31'b0;
                if (wr_en) begin
                    c4_spd_en <= wr_d[0];
                end
            end
    
            `C4_SIP_EN_REG : begin
                rd_d[0] <= c4_sip_en;
                rd_d[31:1] <= 31'b0;
                if (wr_en) begin
                    c4_sip_en <= wr_d[0];
                end
            end
    
            `C4_LTC1867_SPI_WR_REG : begin
                rd_d[15:0]    <= c4_ltc1867_spi_wr;
                rd_d[31:16] <= 16'b0;
                if (wr_en) begin
                    c4_ltc1867_spi_wr        <= wr_d[15:0];
                    c4_ltc1867_spi_valid     <= 1'b1;
                end
            end
    
            `C4_LTC1867_SPI_RD_REG : begin
                rd_d[15:0]     <= c4_ltc1867_spi_rd;
                rd_d[30:16] <= 15'b0;
                rd_d[31]     <= c4_ltc1867_spi_busy;
            end
    
            `C4_SPD_DAC_WR_REG : begin
                rd_d[23:0] <= c4_spd_dac_wr;
                rd_d[31:24] <= 8'b0;
                if (wr_en) begin
                    c4_spd_dac_wr        <= wr_d[23:0];
                    c4_spd_dac_valid     <= 1'b1;
                end
            end
    
            `C4_SIP_DAC_WR_REG : begin
                rd_d[23:0] <= c4_sip_dac_wr;
                rd_d[31:24] <= 8'b0;
                if (wr_en) begin
                    c4_sip_dac_wr        <= wr_d[23:0];
                    c4_sip_dac_valid     <= 1'b1;
                end
            end
    
            `C4_TEC_EN_REG : begin
                rd_d[0] <= c4_tec_en;
                rd_d[31:1] <= 31'b0;
                if (wr_en) begin
                    c4_tec_en <= wr_d[0];
                end
            end
    
            `C4_TEC_DAC_WR_REG : begin
                rd_d[15:0] <= c4_tec_dac_wr;
                rd_d[31:16] <= 16'b0;
                if (wr_en) begin
                    c4_tec_dac_wr        <= wr_d[15:0];
                    c4_tec_dac_valid     <= 1'b1;
                end
            end
    
            `C4_TEC_DAC_CLR_REG : begin
                rd_d[0] <= c4_tec_dac_clr;
                rd_d[31:1] <= 31'b0;
                if (wr_en) begin
                    c4_tec_dac_clr <= wr_d[0];
                end
            end
            
            `C4_DP_WR_REG : begin
                rd_d[23:0] <= c4_dp_wr;
                rd_d[31:24] <= 8'b0;
                if (wr_en) begin
                    c4_dp_wr        <= wr_d[23:0];
                    c4_dp_valid     <= 1'b1;
                end
            end
            `C4_DP_RD_REG : begin
                rd_d[7:0]     <= c4_dp_rd;
                rd_d[31:24]    <= 8'b0;
            end            
//CON 5
            
			`C5_FPC_EN_REG : begin
                rd_d[0] <= c5_fpc_en;
                rd_d[31:1] <= 31'b0;
                if (wr_en) begin
                    c5_fpc_en <= wr_d[0];
                end
            end
            
            `C5_FPC_DAC_WR_REG : begin
                rd_d[31:0] <= c5_fpc_dac_din;
                if (wr_en) begin
                    c5_fpc_dac_din        <= wr_d[31:0];
                    c5_fpc_dac_din_valid     <= 1'b1;
                end
            end
            
            `C5_FPC_DAC_CLR_REG : begin
                rd_d[0] <= c5_fpc_dac_clr;
                rd_d[31:1] <= 31'b0;
                if (wr_en) begin
                    c5_fpc_dac_clr <= wr_d[0];
                end
            end
            
            `C5_FPC10_DAC_WR_REG : begin
                rd_d[31:0] <= c5_fpc10_dac_din;
                if (wr_en) begin
                    c5_fpc10_dac_din    <= wr_d[31:0];
                    c5_fpc10_dac_din_valid     <= 1'b1;
                end
            end
            
            `C5_FPC10_DAC_CLR_REG : begin
                rd_d[0] <= c5_fpc10_dac_clr;
                rd_d[31:1] <= 31'b0;
                if (wr_en) begin
                    c5_fpc10_dac_clr <= wr_d[0];
                end
            end
 // CON 6
             `C6_HMC920_EN_REG : begin
                 rd_d[0] <= c6_hmc920_en;
                 rd_d[31:1] <= 31'b0;
                 if (wr_en) begin
                     c6_hmc920_en <= wr_d[0];
                 end
             end
             
             `C6_LTC1867_SPI_WR_REG : begin
                 rd_d[15:0]    <= c6_ltc1867_spi_wr;
                 rd_d[31:16] <= 16'b0;
                 if (wr_en) begin
                     c6_ltc1867_spi_wr        <= wr_d[15:0];
                     c6_ltc1867_spi_valid     <= 1'b1;
                 end
             end
             
             `C6_LTC1867_SPI_RD_REG : begin
                 rd_d[15:0]     <= c6_ltc1867_spi_rd;
                 rd_d[30:16] <= 15'b0;
                 rd_d[31]     <= c6_ltc1867_spi_busy;
             end
             
             `C6_AD5761_DAC_WR_REG : begin
                 rd_d[23:0] <= c6_ad5761_dac_wr;
                 rd_d[31:24] <= 8'b0;
                 if (wr_en) begin
                     c6_ad5761_dac_wr        <= wr_d[23:0];
                     c6_ad5761_dac_valid     <= 1'b1;
                 end
             end
             
             `C6_AD5761_DAC_RD_REG : begin
                 rd_d[23:0]     <= c6_ad5761_dac_rd;
                 rd_d[30:24] <= 8'b0;
             end
             
             `C6_DP_WR_REG : begin
                 rd_d[23:0] <= c6_dp_wr;
                 rd_d[31:24] <= 8'b0;
                 if (wr_en) begin
                     c6_dp_wr        <= wr_d[23:0];
                     c6_dp_valid     <= 1'b1;
                 end
             end
             `C6_DP_RD_REG : begin
                 rd_d[7:0]     <= c6_dp_rd;
                 rd_d[31:24]    <= 8'b0;
             end
             
             `C6_TEC_EN_REG : begin
                 rd_d[0] <= c6_tec_en;
                 rd_d[31:1] <= 31'b0;
                 if (wr_en) begin
                     c6_tec_en <= wr_d[0];
                 end
             end
             
             `C6_TEC_DAC_WR_REG : begin
                 rd_d[15:0] <= c6_tec_dac_wr;
                 rd_d[31:16] <= 16'b0;
                 if (wr_en) begin
                     c6_tec_dac_wr        <= wr_d[15:0];
                     c6_tec_dac_valid     <= 1'b1;
                 end
             end
             
             `C6_TEC_DAC_CLR_REG : begin
                 rd_d[0] <= c6_tec_dac_clr;
                 rd_d[31:1] <= 31'b0;
                 if (wr_en) begin
                     c6_tec_dac_clr <= wr_d[0];
                 end
             end

//CON 7 :      
            `C7_HMC920_EN_REG : begin
                rd_d[0] <= c7_hmc920_en;
                rd_d[31:1] <= 31'b0;
                if (wr_en) begin
                    c7_hmc920_en <= wr_d[0];
                end
            end
            
            `C7_LTC1867_SPI_WR_REG : begin
                rd_d[15:0]    <= c7_ltc1867_spi_wr;
                rd_d[31:16] <= 16'b0;
                if (wr_en) begin
                    c7_ltc1867_spi_wr        <= wr_d[15:0];
                    c7_ltc1867_spi_valid     <= 1'b1;
                end
            end
            
            `C7_LTC1867_SPI_RD_REG : begin
                rd_d[15:0]     <= c7_ltc1867_spi_rd;
                rd_d[30:16] <= 15'b0;
                rd_d[31]     <= c7_ltc1867_spi_busy;
            end
            
            `C7_AD5761_DAC_WR_REG : begin
                rd_d[23:0] <= c7_ad5761_dac_wr;
                rd_d[31:24] <= 8'b0;
                if (wr_en) begin
                    c7_ad5761_dac_wr        <= wr_d[23:0];
                    c7_ad5761_dac_valid     <= 1'b1;
                end
            end
            
            `C7_AD5761_DAC_RD_REG : begin
                rd_d[23:0]     <= c7_ad5761_dac_rd;
                rd_d[30:24] <= 8'b0;
            end
            
            `C7_DP_WR_REG : begin
                rd_d[23:0] <= c7_dp_wr;
                rd_d[31:24] <= 8'b0;
                if (wr_en) begin
                    c7_dp_wr        <= wr_d[23:0];
                    c7_dp_valid     <= 1'b1;
                end
            end
            `C7_DP_RD_REG : begin
                rd_d[7:0]     <= c7_dp_rd;
                rd_d[31:24]    <= 8'b0;
            end
            
            `C7_TEC_EN_REG : begin
                rd_d[0] <= c7_tec_en;
                rd_d[31:1] <= 31'b0;
                if (wr_en) begin
                    c7_tec_en <= wr_d[0];
                end
            end
            
            `C7_TEC_DAC_WR_REG : begin
                rd_d[15:0] <= c7_tec_dac_wr;
                rd_d[31:16] <= 16'b0;
                if (wr_en) begin
                    c7_tec_dac_wr        <= wr_d[15:0];
                    c7_tec_dac_valid     <= 1'b1;
                end
            end
            
            `C7_TEC_DAC_CLR_REG : begin
                rd_d[0] <= c7_tec_dac_clr;
                rd_d[31:1] <= 31'b0;
                if (wr_en) begin
                    c7_tec_dac_clr <= wr_d[0];
                end
            end
// CON8
			`C8_FPC_EN_REG : begin
                 rd_d[0] <= c8_fpc_en;
                 rd_d[31:1] <= 31'b0;
                 if (wr_en) begin
                     c8_fpc_en <= wr_d[0];
                 end
             end
            
             `C8_FPC_DAC_WR_REG : begin
                 rd_d[31:0] <= c8_fpc_dac_din;
                 if (wr_en) begin
                     c8_fpc_dac_din        <= wr_d[31:0];
                     c8_fpc_dac_din_valid     <= 1'b1;
                 end
             end
            
             `C8_FPC_DAC_CLR_REG : begin
                 rd_d[0] <= c8_fpc_dac_clr;
                 rd_d[31:1] <= 31'b0;
                 if (wr_en) begin
                     c8_fpc_dac_clr <= wr_d[0];
                 end
             end
            
             `C8_FPC10_DAC_WR_REG : begin
                 rd_d[31:0] <= c8_fpc10_dac_din;
                 if (wr_en) begin
                     c8_fpc10_dac_din    <= wr_d[31:0];
                     c8_fpc10_dac_din_valid     <= 1'b1;
                 end
             end
            
             `C8_FPC10_DAC_CLR_REG : begin
                 rd_d[0] <= c8_fpc10_dac_clr;
                 rd_d[31:1] <= 31'b0;
                 if (wr_en) begin
                     c8_fpc10_dac_clr <= wr_d[0];
                 end
             end

// CON10			

			default: begin
				rd_d <= 32'b0;
			end
		endcase

		////////////////////////////////////////////////
		// ?��?�� ??�?.
		if (!wr_en) begin
			version_dat	    		<= 32'b0;
//			clk_sel_rst_r		<= 1'b0;
			lm70_spi_valid			<= 1'b0;
			
			c1_ltc1867_spi_valid	<= 1'b0;
            c1_sip_dac_valid        <= 1'b0;
            c1_spd_dac_valid        <= 1'b0;
            c1_tec_dac_valid        <= 1'b0;
            c1_dp_valid             <= 1'b0;
			
			c2_ltc1867_spi_valid	<= 1'b0;
			c2_sip_dac_valid		<= 1'b0;
			c2_spd_dac_valid		<= 1'b0;
			c2_tec_dac_valid		<= 1'b0;
			c2_dp_valid             <= 1'b0;
			
			c3_ltc1867_spi_valid	<= 1'b0;
            c3_sip_dac_valid        <= 1'b0;
            c3_spd_dac_valid        <= 1'b0;
            c3_tec_dac_valid        <= 1'b0;
            c3_dp_valid             <= 1'b0;
			
			c4_ltc1867_spi_valid	<= 1'b0;
            c4_sip_dac_valid        <= 1'b0;
            c4_spd_dac_valid        <= 1'b0;
            c4_tec_dac_valid        <= 1'b0;
            c4_dp_valid             <= 1'b0;
			
			c5_fpc_dac_din_valid	<= 1'b0;
			c5_fpc10_dac_din_valid	<= 1'b0;
			
			c6_ltc1867_spi_valid	<= 1'b0;
            c6_ad5761_dac_valid     <= 1'b0;
            c6_dp_valid             <= 1'b0;       
            c6_tec_dac_valid        <= 1'b0;     
			
			c7_ltc1867_spi_valid	<= 1'b0;
            c7_ad5761_dac_valid     <= 1'b0;
            c7_dp_valid             <= 1'b0;       
            c7_tec_dac_valid        <= 1'b0; 
			

			c8_fpc_dac_din_valid	<= 1'b0;
            c8_fpc10_dac_din_valid    <= 1'b0;      
		end
	end
end

/* pls_expander #(
	.COUNT(`CLKSEL_RST_SYSCLK),
	.POLARITY(1)
) adc_fifo_rst_ext(
	.sin(clk_sel_rst_r),
	.clk(clk),
	.sout(clk_sel_rst)
);
 */

// -------------------------------------------------------------------------
parameter idle_state = 0;
parameter ready_state = 1;
parameter length_message = 2047; // length of rom data

reg state;
reg rom_en;
reg [10:0] addra;
wire [7:0] douta;

always @(posedge clk)
begin
	if (rst) begin
		rom_en	<= 1'b0;
		addra <= 11'h7ff;
		version_date <= 32'hffff_ffff;
		state <= idle_state;
	end
	else
		case (state)
		idle_state : begin
			if (version_dat == 32'h279) begin
				rom_en <= 1'b1;
				addra <= 11'd0;
				version_date <= {24'b0, douta};
				state <= ready_state;
			end
			else begin
				rom_en <= 1'b1;
				addra <= 11'd0;
				version_date <= `BSC_VERSION_DATE_DATA;
				state <= idle_state;
			end

		end
		ready_state : begin
			if (addra > length_message) begin
				rom_en <=1'b0;
				addra <= 11'd0;
				version_date <= {24'b0, douta};
				state <= idle_state;
			end
			else if (regin_valid == 1'b1 && addr == `BSC_VERSION_REG) begin
				rom_en <= 1'b1;
				addra <= addra + 11'b1;
				version_date <= {24'b0, douta};
				state <= ready_state;
			end
			else if (regin_valid == 1'b1 && addr != `BSC_VERSION_REG) begin
				rom_en <=1'b0;
				addra <= 11'd0;
				version_date <= `BSC_VERSION_DATE_DATA;
				state <= idle_state;
			end
			else begin
				rom_en <= 1'b1;
				addra <= addra;
				version_date <= version_date;
				state <= ready_state;
			end

		end
		endcase
end

rom_cr rom_cr(
  .clka(clk), 		// I
  .ena(rom_en), 	// I
  .addra(addra), 	// I [10 : 0]
  .douta(douta) 	// O [7 : 0]
);

endmodule