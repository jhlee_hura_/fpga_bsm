//////////////////////////////////////////////////////////////////////////////////
// Company: HURA
// Engineer: LEE Seong Yun (sylee@hura.co.kr)
//
// Create Date: 2019/01/02 11:05:02
// Design Name:
// Module Name: 
// Project Name: 
// Target Devices: Kintex 7 160T
// Tool Versions: Vivado 2018.1
// Description:
//
// Dependencies:
//
// Revision:
//		build. 6(2019/02/27)
// Additional Comments:
//
// Copyright 2018, All rights reserved by HURA
//////////////////////////////////////////////////////////////////////////////////
// ---------------------------------------------------------------------------------
// ?΄λ¦? λ§λ€κΈ?
// a='QKD Alice Board(HR-QKD-ALICE-01)';
// len_b=ceil(length(a)/4)*4; b=zeros(1,len_b);b(1:length(a))=a;
// c=dec2hex(typecast(uint8(double(b)),'uint32'))
//
// char(typecast(uint32(hex2dec(c)),'uint8'))' % ??Έ?©
// reshape(char(typecast(uint32(hex2dec(c)),'uint8')),4,len_b/4)' % ??Έ?©
// a='QKD ALICE Testbed';
// ---------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------
// Board and FW Constant
`define BSC_VERSION_DATE_DATA			32'h1DA00008	// dec2hex(fix(now)-datenum(2000,01,01))
`define BOARD_MAKER						32'h41525548	// HURA
`define BOARD_NAME						32'h204D5342
`define BOARD_NAME1						32'h6F422043
`define BOARD_NAME2						32'h28647261
`define BOARD_NAME3						32'h422D5248
`define BOARD_NAME4						32'h432D4D53
`define BOARD_NAME5						32'h2931302D
`define BOARD_NAME6						32'h00000000
`define BOARD_NAME7						32'h00000000
`define BOARD_NAME8						32'h00000000
`define BOARD_NAME9						32'h00000000
`define BOARD_NAME10					32'h00000000
`define BOARD_NAME11					32'h00000000

`define VERSION_DATE_DATA				32'h1DA00008	// dec2hex(fix(now)-datenum(2000,01,01))
`define COPYRIGHTER						32'h2052534E	// NSR
`define PRODUCT							32'h204D5342    // BSM C Testbed
`define PRODUCT1						32'h65542043
`define PRODUCT2						32'h65627473
`define PRODUCT3						32'h00000064
`define PRODUCT4						32'h00000000
`define PRODUCT5						32'h00000000
`define PRODUCT6						32'h00000000
`define PRODUCT7						32'h00000000
`define PRODUCT8						32'h00000000
`define PRODUCT9						32'h00000000
`define PRODUCT10						32'h00000000
`define PRODUCT11						32'h00000000
