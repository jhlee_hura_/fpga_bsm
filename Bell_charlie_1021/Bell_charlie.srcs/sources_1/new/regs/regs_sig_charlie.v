`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: HURA
// Engineer:  LEE Seong Yun (sylee@hura.co.kr)
//
// Create Date: 2019/01/15 21:04:48
// Design Name: register for signal clock domain
// Module Name: regs_sig_alice
// Project Name: QKD
// Target Devices: Any Device
// Tool Versions: Vivado 2018.1
// Description:
//
// Dependencies:
//
// Revision:
//		build 1 (2019/01/15)
// Additional Comments:
//
// Copyright 2019, All rights reserved by HURA
//////////////////////////////////////////////////////////////////////////////////
module regs_sig_charlie (
	input	wire				clk,		// sig_clk
	input	wire				rst,		// because of multi-clk
	input	wire				pcie_clk,
	input   wire               sig_clk_10M_pulse,                     // I
	input   wire               sig_start_flag,
	input	wire				regs_sel,
	input	wire	[46:0]		regs_adda,
	input	wire				regs_adda_valid,
	output	wire	[31:0]		regs_dout,
	output	wire				regs_dout_valid,
	output     wire				fifo_rst,                // O
	// -------------------------------------------------------
	// -- register ? λ³?
	// --------------------------------------------------------
	// CON 1 SPAD
	// C1_TEST_MODE_REG
	output reg							c1_test_mode,
	// C1_TEST_DATA_REG
	output reg	[31:0]					c1_test_data,
	// C1_WIDTH_REG
	output reg	[5:0]					c1_width,
	output reg							c1_shape_load,
	// C1_OFFSET_QUOT_REG
	output reg	[6:0]					c1_i_offset_quotient,
	// C1_OFFSET_REMD_REG
    output reg    [2:0]                c1_i_offset_remainder,
    	
	// C1_DELAY_TAP_REG
	output reg	[4:0]					c1_delay_tap,
	
	// C1_DETECT_DELAY_TAP_REG
    output reg  [4:0]                  c1_detect_delay_tap,
    
	// C1_SPD_CNT1_REG
    input wire    [31:0]               c1_spad_cnt1,
    // C1_SPD_CNT2_REG
    input wire    [31:0]               c1_spad_cnt2,
    // C1_SPD_CNT3_REG
    input wire  [31:0]                 c1_spad_cnt3,
     // C1_SPD_CNT4_REG
    input wire  [31:0]                 c1_spad_cnt4,
	// --------------------------------------------------------
	// CON 2 SPAD
	// C2_TEST_MODE_REG
	output reg							c2_test_mode,
	// C2_TEST_DATA_REG
	output reg	[31:0]					c2_test_data,
	// C2_WIDTH_REG
	output reg	[5:0]					c2_width,
	output reg							c2_shape_load,
	// C2_OFFSET_QUOT_REG
    output reg    [6:0]                    c2_i_offset_quotient,
    // C2_OFFSET_REMD_REG
    output reg    [2:0]                c2_i_offset_remainder,
	// C2_DELAY_TAP_REG
	output reg	[4:0]					c2_delay_tap,
	// C2_DETECT_DELAY_TAP_REG
    output reg  [4:0]                  c2_detect_delay_tap,	
	// C2_SPD_CNT1_REG
    input wire    [31:0]               c2_spad_cnt1,
    // C2_SPD_CNT2_REG
    input wire    [31:0]               c2_spad_cnt2,
    // C2_SPD_CNT3_REG
    input wire  [31:0]                 c2_spad_cnt3,
     // C2_SPD_CNT4_REG
    input wire  [31:0]                 c2_spad_cnt4,		
	// --------------------------------------------------------
	// CON 3 LD1550
	// C3_TEST_MODE_REG
	output reg							c3_test_mode,
	// C3_TEST_DATA_REG
	output reg	[31:0]					c3_test_data,
	// C3_WIDTH_REG
	output reg	[5:0]					c3_width,
	output reg							c3_shape_load,
	// C3_OFFSET_QUOT_REG
    output reg    [6:0]                 c3_i_offset_quotient,
    // C3_OFFSET_REMD_REG
    output reg    [2:0]                c3_i_offset_remainder,
	// C3_DELAY_TAP_REG
	output reg	[4:0]					c3_delay_tap,
	// C3_DETECT_DELAY_TAP_REG
    output reg  [4:0]                  c3_detect_delay_tap,    	
	// C3_SPD_CNT1_REG
    input wire    [31:0]               c3_spad_cnt1,
    // C3_SPD_CNT2_REG
    input wire    [31:0]               c3_spad_cnt2,
    // C3_SPD_CNT3_REG
    input wire  [31:0]                 c3_spad_cnt3,
     // C3_SPD_CNT4_REG
    input wire  [31:0]                 c3_spad_cnt4,    
	// --------------------------------------------------------
	// CON 4
 	// C4_TEST_MODE_REG
	output reg							c4_test_mode,
	// C4_TEST_DATA_REG
	output reg	[31:0]					c4_test_data,
	// C4_WIDTH_REG
	output reg	[5:0]					c4_width,
	output reg							c4_shape_load,
	// C4_OFFSET_QUOT_REG
    output reg    [6:0]                 c4_i_offset_quotient,
    // C4_OFFSET_REMD_REG
    output reg    [2:0]                c4_i_offset_remainder,
	// C4_DELAY_TAP_REG
	output reg	[4:0]					c4_delay_tap,
	// C4_DETECT_DELAY_TAP_REG
    output reg  [4:0]                  c4_detect_delay_tap,   	
	// C4_SPD_CNT1_REG
    input wire    [31:0]               c4_spad_cnt1,
    // C4_SPD_CNT2_REG
    input wire    [31:0]               c4_spad_cnt2,
    // C4_SPD_CNT3_REG
    input wire  [31:0]                 c4_spad_cnt3,
     // C4_SPD_CNT4_REG
    input wire  [31:0]                 c4_spad_cnt4,
	// --------------------------------------------------------
	// CON 5
	// --------------------------------------------------------
	// CON 6
	// C6_TEST_MODE_REG
    output reg                            c6_test_mode = 1'b0,
    // C6_TEST_DATA_REG
    output reg    [31:0]                  c6_test_data= 32'hffffffff,
                       
    // C6_WIDTH_REG
    output reg    [5:0]                   c6_width= 6'd1,
    output reg                            c6_shape_load= 1'b0,    
    // C6_ADDWIDTH_REG
    output reg    [7:0]                   c6_add_width= 8'd1,    
    // C6_OFFSET_QUOT_REG
    output reg    [6:0]                 c6_i_offset_quotient,
    // C6_OFFSET_REMD_REG
    output reg    [2:0]                c6_i_offset_remainder,
    // C6_DELAY_TAP_REG
    output reg    [4:0]                   c6_delay_tap= 5'b0,
    // C6_ADD_PULSE_DELAY_REG
    output reg    [14:0]                 o_a6_delay_cnt,           
	// --------------------------------------------------------
	// CON 7
	// C7_TEST_MODE_REG
    output reg                            c7_test_mode = 1'b0,
    // C7_TEST_DATA_REG
    output reg    [31:0]                  c7_test_data= 32'hffffffff,
                          
    // C7_WIDTH_REG
    output reg    [5:0]                    c7_width= 6'd1,
    output reg                            c7_shape_load= 1'b0,    
    // C7_ADDWIDTH_REG
    output reg    [7:0]                    c7_add_width= 8'd1,    
    // C7_OFFSET_QUOT_REG
    output reg    [6:0]                 c7_i_offset_quotient,
    // C7_OFFSET_REMD_REG
    output reg    [2:0]                c7_i_offset_remainder,
    // C7_DELAY_TAP_REG
    output reg    [4:0]                    c7_delay_tap= 5'b0,
    // C7_ADD_PULSE_DELAY_REG
    output reg    [14:0]                 o_a7_delay_cnt,        
	// --------------------------------------------------------
	// CON 8
	


	// --------------------------------------------------------
	// CON 9

	// --------------------------------------------------------
	// CON 10 


	// C_START_REG
	output	reg							op_start,

	// C_FIFO_STAT_REG
	// C_FIFO_PF_THRES_REG
	// C_FIFO_READ_REG
	input	wire	[31:0]				din_fifo,
	input	wire						div_fifo,
	output	wire						dir_fifo,

	// C_TRIG_TYPE_REG
    output reg [1:0]                    c_trig_type,

    // C_SPD12_CNT_REG  
    input wire [31:0] c_spad12_cnt,// I [31:0]
    // C_SPD13_CNT_REG 
    input wire [31:0] c_spad13_cnt, // I [31:0]    
    // C_SPD24_CNT_REG 
    input wire [31:0] c_spad24_cnt, // I [31:0]   
    // C_SPD34_CNT_REG	
    input wire [31:0] c_spad34_cnt,  // I [31:0]    
     // C_SPD1234_CNT_REG	
    input wire [31:0] c_spad1234_cnt,  // I [31:0]    
        
	input	wire	[7:0]				emp_log


	
);

wire	[46:0]	fifo_dout;
wire			regin_empty;
wire			regout_empty;
reg				regout_we;
reg				regout_we1;
reg				regout_we2;

wire	[15:0]	addr;
wire			wr_en;
wire	[31:0]	wr_d;
reg		[31:0]	rd_d;
wire			regin_valid;

// from pcie block
fifo_regin_47x16 regin_fifo (
  .rst(rst),							// I
  .wr_clk(pcie_clk),					// I
  .rd_clk(clk),							// I
  .din(regs_adda),						// I [46 : 0]
  .wr_en(regs_sel & regs_adda_valid),	// I
  .rd_en(~regin_empty),					// I
  .dout(fifo_dout),						// O [46 : 0]
  .full(),								// O
  .empty(regin_empty),					// O
  .valid(regin_valid)					// O
);

// to pcie block
fifo_regout_32x16 regout_fifo (
  .rst(rst),							// I
  .wr_clk(clk),							// I
  .rd_clk(pcie_clk),					// I
  .din(rd_d),				// I [31 : 0]
  .wr_en(regout_we),					// I
  .rd_en(~regout_empty),				// I
  .dout(regs_dout),						// O [31 : 0]
  .full(),								// O
  .empty(regout_empty),					// O
  .valid(regs_dout_valid)				// O
);


(* mark_debug = "true" *)wire reg_re;
(* mark_debug = "true" *)wire fifo_rd_en;
assign	reg_re = regin_valid & (~fifo_dout[46]);
assign	fifo_rd_en = (addr == `C_FIFO_READ_REG) & reg_re;

// fifo latency ?λ¬Έμ 2+1 = 3 clk.
always @(posedge clk) begin
	regout_we1	<= reg_re;
	regout_we2	<= regout_we1;
	regout_we	<= regout_we2;
end

// fifo[46]= 1:wr_en, 0:rd_en
// fifo[45:32] = 14bits addr
// fifo[31:0] = 32bits data
assign wr_en	= regin_valid & fifo_dout[46];
assign addr		= {fifo_dout[45:32], 2'b0};
assign wr_d		= fifo_dout[31:0];

(* mark_debug = "true" *)wire [31:0]			ff_out;
(* mark_debug = "true" *)reg	[15:0]			fifo_pf_thres;
(* mark_debug = "true" *)reg					fifo_en;
(* mark_debug = "true" *)wire				fifo_empty;
(* mark_debug = "true" *)wire				fifo_pf;
(* mark_debug = "true" *)wire				fifo_af;
(* mark_debug = "true" *)wire				fifo_full;
(* mark_debug = "true" *)reg					fifo_rst_r;
(* mark_debug = "true" *)
reg [14:0]   fifo_log_delay_cnt;
always @(posedge clk) begin
	if (rst)	begin
		rd_d					<= 32'b0;

// CON 1
		// C1_TEST_MODE_REG
		c1_test_mode 			<= 1'b0;
		// C1_TEST_DATA_REG
		c1_test_data			<= 32'hffffffff;
		// C1_WIDTH_REG
		c1_width				<= 6'd1;
		c1_shape_load			<= 1'b0;
		// C1_OFFSET_QUOT_REG
		c1_i_offset_quotient	<= 7'b0;
		// C1_OFFSET_REMD_REG
        c1_i_offset_remainder    <= 3'b0;		
		// C1_DELAY_TAP_REG
		c1_delay_tap			<= 5'b0;
		// C1_DETECT_DELAY_TAP_REG
        c1_detect_delay_tap            <= 5'b0;   
// CON 2
		// C2_TEST_MODE_REG
		c2_test_mode 			<= 1'b0;
		// C2_TEST_DATA_REG
		c2_test_data			<= 32'hffffffff;
		// C2_WIDTH_REG
		c2_width				<= 6'd1;
		c2_shape_load			<= 1'b0;
		// C2_OFFSET_QUOT_REG
        c2_i_offset_quotient    <= 7'b0;
        // C2_OFFSET_REMD_REG
        c2_i_offset_remainder    <= 3'b0;
		// C2_DELAY_TAP_REG
		c2_delay_tap			<= 5'b0;
		// C2_DETECT_DELAY_TAP_REG
        c2_detect_delay_tap            <= 5'b0;   
// CON 3
		// C3_TEST_MODE_REG
		c3_test_mode 			<= 1'b0;
		// C3_TEST_DATA_REG
		c3_test_data			<= 32'hffffffff;
		// C3_WIDTH_REG
		c3_width				<= 6'd1;
		c3_shape_load			<= 1'b0;
		// C3_OFFSET_QUOT_REG
        c3_i_offset_quotient    <= 7'b0;
        // C3_OFFSET_REMD_REG
        c3_i_offset_remainder    <= 3'b0;
		// C3_DELAY_TAP_REG
		c3_delay_tap			<= 5'b0;
		// C3_DETECT_DELAY_TAP_REG
        c3_detect_delay_tap            <= 5'b0;   
// CON 4
		// C4_TEST_MODE_REG
		c4_test_mode 			<= 1'b0;
		// C4_TEST_DATA_REG
		c4_test_data			<= 32'hffffffff;
		// C4_WIDTH_REG
		c4_width				<= 6'd1;
		c4_shape_load			<= 1'b0;
		// C4_OFFSET_QUOT_REG
        c4_i_offset_quotient    <= 7'b0;
        // C4_OFFSET_REMD_REG
        c4_i_offset_remainder    <= 3'b0;
		// C4_DELAY_TAP_REG
		c4_delay_tap			<= 5'b0;
		// C4_DETECT_DELAY_TAP_REG
        c4_detect_delay_tap			<= 5'b0;   
// --------------------------------------------------------  
// CON 6
		// C6_TEST_MODE_REG
        c6_test_mode             <= 1'b0;
        // C6_TEST_DATA_REG
        c6_test_data            <= 32'hffffffff;

        // C6_WIDTH_REG
        c6_width                <= 6'd1;
        c6_shape_load            <= 1'b0;
        // C6_ADDWIDTH_REG
        c6_add_width            <= 8'd1;
		// C6_OFFSET_QUOT_REG
        c6_i_offset_quotient    <= 7'b0;
        // C6_OFFSET_REMD_REG
        c6_i_offset_remainder    <= 3'b0;
        // C6_DELAY_TAP_REG
        c6_delay_tap            <= 5'b0;      	
        // C6_ADD_PULSE_DELAY_REG
        o_a6_delay_cnt           <= 15'b0;             
// --------------------------------------------------------        
// CON 7
		// C7_TEST_MODE_REG
        c7_test_mode             <= 1'b0;
        // C7_TEST_DATA_REG
        c7_test_data            <= 32'hffffffff;

        // C7_WIDTH_REG
        c7_width                <= 6'd1;
        c7_shape_load            <= 1'b0;
        // C7_ADDWIDTH_REG
        c7_add_width            <= 8'd1;
		// C7_OFFSET_QUOT_REG
        c7_i_offset_quotient    <= 7'b0;
        // C6_OFFSET_REMD_REG
        c7_i_offset_remainder    <= 3'b0;
        // c7_DELAY_TAP_REG
        c7_delay_tap            <= 5'b0;   
        // C7_ADD_PULSE_DELAY_REG
        o_a7_delay_cnt           <= 15'b0;      		
// CON 8

		
// --------------------------------------------------------
// CON 9
// --------------------------------------------------------
// CON 10
	


		// C_START_REG
		op_start				<= 1'b0;

		// C_FIFO_STAT_REG
		fifo_en					<= 1'b1;
		fifo_rst_r				<= 1'b1;
		// C_FIFO_PF_THRES_REG
		fifo_pf_thres			<= 16'h7fff;
		// C_TRIG_TYPE_REG
        c_trig_type             <= 2'b00;
        // C_FIFO_LOG_DELAY_REG
        fifo_log_delay_cnt      <=15'b0;
	end
	else begin
		case (addr)
			`TMP_SIG_REG : begin
				rd_d[7:0]	<= emp_log;
				rd_d[31:8]	<= 'b0;
			end

			`C1_TEST_MODE_REG : begin
				rd_d[0]	<= c1_test_mode;
				rd_d[31:1]	<= 'b0;
				if (wr_en) begin
					c1_test_mode <= wr_d[0];
					c1_shape_load <= 1'b1;
				end
			end

			`C1_TEST_DATA_REG : begin
				rd_d[31:0]	<= c1_test_data;
				if (wr_en) begin
					c1_test_data <= wr_d[31:0];
				end
			end

			`C1_WIDTH_REG : begin
				rd_d[5:0]	<= c1_width;
				rd_d[31:6]	<= 'b0;
				if (wr_en) begin
					c1_width <= wr_d[5:0];
					c1_shape_load <= 1'b1;
				end
			end

			`C1_OFFSET_QUOT_REG : begin
				rd_d[6:0]	<= c1_i_offset_quotient;
				rd_d[31:7]	<= 'b0;
				if (wr_en) begin
					c1_i_offset_quotient <= wr_d[6:0];
					c1_shape_load <= 1'b1;
				end
			end
			
			`C1_OFFSET_REMD_REG : begin
                rd_d[2:0]    <= c1_i_offset_remainder;
                rd_d[31:3]    <= 'b0;
                if (wr_en) begin
                    c1_i_offset_remainder <= wr_d[2:0];
                    c1_shape_load <= 1'b1;
                end
            end
            
			`C1_DELAY_TAP_REG : begin
				rd_d[4:0]	<= c1_delay_tap;
				rd_d[31:5]	<= 'b0;
				if (wr_en) begin
					c1_delay_tap <= wr_d[4:0];
				end
			end		

			`C1_DETECT_DELAY_TAP_REG : begin
				rd_d[4:0]	<= c1_detect_delay_tap;
				rd_d[31:5]	<= 'b0;
				if (wr_en) begin
					c1_detect_delay_tap <= wr_d[4:0];
				end
			end		
				
			`C1_SPD_CNT1_REG : begin
                rd_d[31:0]    <= c1_spad_cnt1;
            end

            `C1_SPD_CNT2_REG : begin
                rd_d[31:0]    <= c1_spad_cnt2;
            end
            
            `C1_SPD_CNT3_REG : begin
                rd_d[31:0]    <= c1_spad_cnt3;
            end
            `C1_SPD_CNT4_REG : begin
                rd_d[31:0]    <= c1_spad_cnt4;
            end
			
			`C2_TEST_MODE_REG : begin
				rd_d[0]	<= c2_test_mode;
				rd_d[31:1]	<= 'b0;
				if (wr_en) begin
					c2_test_mode <= wr_d[0];
					c2_shape_load <= 1'b1;
				end
			end

			`C2_TEST_DATA_REG : begin
				rd_d[31:0]	<= c2_test_data;
				if (wr_en) begin
					c2_test_data <= wr_d[31:0];
				end
			end

			`C2_WIDTH_REG : begin
				rd_d[5:0]	<= c2_width;
				rd_d[31:6]	<= 'b0;
				if (wr_en) begin
					c2_width <= wr_d[5:0];
					c2_shape_load <= 1'b1;
				end
			end

			`C2_OFFSET_QUOT_REG : begin
				rd_d[6:0]	<= c2_i_offset_quotient;
				rd_d[31:7]	<= 'b0;
				if (wr_en) begin
					c2_i_offset_quotient <= wr_d[6:0];
					c2_shape_load <= 1'b1;
				end
			end
			
			`C2_OFFSET_REMD_REG : begin
                rd_d[2:0]    <= c2_i_offset_remainder;
                rd_d[31:3]    <= 'b0;
                if (wr_en) begin
                    c2_i_offset_remainder <= wr_d[2:0];
                    c2_shape_load <= 1'b1;
                end
            end

			`C2_DELAY_TAP_REG : begin
				rd_d[4:0]	<= c2_delay_tap;
				rd_d[31:5]	<= 'b0;
				if (wr_en) begin
					c2_delay_tap <= wr_d[4:0];
				end
			end
			`C2_DETECT_DELAY_TAP_REG : begin
                rd_d[4:0]    <= c2_detect_delay_tap;
                rd_d[31:5]    <= 'b0;
                if (wr_en) begin
                    c2_detect_delay_tap <= wr_d[4:0];
                end
            end    			
			`C2_SPD_CNT1_REG : begin
                rd_d[31:0]    <= c2_spad_cnt1;
            end

            `C2_SPD_CNT2_REG : begin
                rd_d[31:0]    <= c2_spad_cnt2;
            end
            
            `C2_SPD_CNT3_REG : begin
                rd_d[31:0]    <= c2_spad_cnt3;
            end
            `C2_SPD_CNT4_REG : begin
                rd_d[31:0]    <= c2_spad_cnt4;
            end

			`C3_TEST_MODE_REG : begin
				rd_d[0]	<= c3_test_mode;
				rd_d[31:1]	<= 'b0;
				if (wr_en) begin
					c3_test_mode <= wr_d[0];
					c3_shape_load <= 1'b1;
				end
			end

			`C3_TEST_DATA_REG : begin
				rd_d[31:0]	<= c3_test_data;
				if (wr_en) begin
					c3_test_data <= wr_d[31:0];
				end
			end

			`C3_WIDTH_REG : begin
				rd_d[5:0]	<= c3_width;
				rd_d[31:6]	<= 'b0;
				if (wr_en) begin
					c3_width <= wr_d[5:0];
					c3_shape_load <= 1'b1;
				end
			end

			`C3_OFFSET_QUOT_REG : begin
				rd_d[6:0]	<= c3_i_offset_quotient;
				rd_d[31:7]	<= 'b0;
				if (wr_en) begin
					c3_i_offset_quotient <= wr_d[6:0];
					c3_shape_load <= 1'b1;
				end
			end
			
			`C3_OFFSET_REMD_REG : begin
                rd_d[2:0]    <= c3_i_offset_remainder;
                rd_d[31:3]    <= 'b0;
                if (wr_en) begin
                    c3_i_offset_remainder <= wr_d[2:0];
                    c3_shape_load <= 1'b1;
                end
            end

			`C3_DELAY_TAP_REG : begin
				rd_d[4:0]	<= c3_delay_tap;
				rd_d[31:5]	<= 'b0;
				if (wr_en) begin
					c3_delay_tap <= wr_d[4:0];
				end
			end
			`C3_DETECT_DELAY_TAP_REG : begin
                rd_d[4:0]    <= c3_detect_delay_tap;
                rd_d[31:5]    <= 'b0;
                if (wr_en) begin
                    c3_detect_delay_tap <= wr_d[4:0];
                end
            end  			
			`C3_SPD_CNT1_REG : begin
                rd_d[31:0]    <= c3_spad_cnt1;
            end

            `C3_SPD_CNT2_REG : begin
                rd_d[31:0]    <= c3_spad_cnt2;
            end
            
            `C3_SPD_CNT3_REG : begin
                rd_d[31:0]    <= c3_spad_cnt3;
            end
            `C3_SPD_CNT4_REG : begin
                rd_d[31:0]    <= c3_spad_cnt4;
            end
// CON 4            
			`C4_TEST_MODE_REG : begin
				rd_d[0]	<= c4_test_mode;
				rd_d[31:1]	<= 'b0;
				if (wr_en) begin
					c4_test_mode <= wr_d[0];
					c4_shape_load <= 1'b1;
				end
			end

			`C4_TEST_DATA_REG : begin
				rd_d[31:0]	<= c4_test_data;
				if (wr_en) begin
					c4_test_data <= wr_d[31:0];
				end
			end

			`C4_WIDTH_REG : begin
				rd_d[5:0]	<= c4_width;
				rd_d[31:6]	<= 'b0;
				if (wr_en) begin
					c4_width <= wr_d[5:0];
					c4_shape_load <= 1'b1;
				end
			end

			`C4_OFFSET_QUOT_REG : begin
				rd_d[6:0]	<= c4_i_offset_quotient;
				rd_d[31:7]	<= 'b0;
				if (wr_en) begin
					c4_i_offset_quotient <= wr_d[6:0];
					c4_shape_load <= 1'b1;
				end
			end
			
			`C4_OFFSET_REMD_REG : begin
                rd_d[2:0]    <= c4_i_offset_remainder;
                rd_d[31:3]    <= 'b0;
                if (wr_en) begin
                    c4_i_offset_remainder <= wr_d[2:0];
                    c4_shape_load <= 1'b1;
                end
            end

			`C4_DELAY_TAP_REG : begin
				rd_d[4:0]	<= c4_delay_tap;
				rd_d[31:5]	<= 'b0;
				if (wr_en) begin
					c4_delay_tap <= wr_d[4:0];
				end
			end
			`C4_DETECT_DELAY_TAP_REG : begin
                rd_d[4:0]    <= c4_detect_delay_tap;
                rd_d[31:5]    <= 'b0;
                if (wr_en) begin
                    c4_detect_delay_tap <= wr_d[4:0];
                end
            end  			
			`C4_SPD_CNT1_REG : begin
                rd_d[31:0]    <= c4_spad_cnt1;
            end

            `C4_SPD_CNT2_REG : begin
                rd_d[31:0]    <= c4_spad_cnt2;
            end
            
            `C4_SPD_CNT3_REG : begin
                rd_d[31:0]    <= c4_spad_cnt3;
            end
            `C4_SPD_CNT4_REG : begin
                rd_d[31:0]    <= c4_spad_cnt4;
            end
// CON 6
			`C6_TEST_MODE_REG : begin
                rd_d[0]    <= c6_test_mode;
                rd_d[31:1]    <= 'b0;
                if (wr_en) begin
                    c6_test_mode <= wr_d[0];
                    c6_shape_load <= 1'b1;
                end
            end
            
            `C6_TEST_DATA_REG : begin
                rd_d[31:0]    <= c6_test_data;
                if (wr_en) begin
                    c6_test_data <= wr_d[31:0];
                    c6_shape_load <= 1'b1;                    
                end
            end
            
            `C6_WIDTH_REG : begin
                rd_d[5:0]    <= c6_width;
                rd_d[31:6]    <= 'b0;
                if (wr_en) begin
                    c6_width <= wr_d[5:0];
                    c6_shape_load <= 1'b1;
                end
            end
            
            `C6_ADDWIDTH_REG : begin
                rd_d[7:0]    <= c6_add_width;
                rd_d[31:8]    <= 'b0;
                if (wr_en) begin
                    c6_add_width <= wr_d[7:0];
                    c6_shape_load <= 1'b1;
                end
            end            
            
			`C6_OFFSET_QUOT_REG : begin
                rd_d[6:0]    <= c6_i_offset_quotient;
                rd_d[31:7]    <= 'b0;
                if (wr_en) begin
                    c6_i_offset_quotient <= wr_d[6:0];
                    c6_shape_load <= 1'b1;
                end
            end
            
            `C6_OFFSET_REMD_REG : begin
                rd_d[2:0]    <= c6_i_offset_remainder;
                rd_d[31:3]    <= 'b0;
                if (wr_en) begin
                    c6_i_offset_remainder <= wr_d[2:0];
                    c6_shape_load <= 1'b1;
                end
            end
            
            `C6_DELAY_TAP_REG : begin
                rd_d[4:0]    <= c6_delay_tap;
                rd_d[31:5]    <= 'b0;
                if (wr_en) begin
                    c6_delay_tap <= wr_d[4:0];
                end
            end   
            
			`C6_ADD_PULSE_DELAY_REG : begin
                rd_d[14:0]    <= o_a6_delay_cnt;
                rd_d[31:15]    <= 'b0;
                if (wr_en) begin
                    o_a6_delay_cnt <= wr_d[14:0];
                end
            end  
 // CON 7          
			`C7_TEST_MODE_REG : begin
				rd_d[0]	<= c7_test_mode;
				rd_d[31:1]	<= 'b0;
				if (wr_en) begin
					c7_test_mode <= wr_d[0];
					c7_shape_load <= 1'b1;
				end
			end

			`C7_TEST_DATA_REG : begin
				rd_d[31:0]	<= c7_test_data;
				if (wr_en) begin
					c7_test_data <= wr_d[31:0];
                    c7_shape_load <= 1'b1;					
				end
			end

			`C7_WIDTH_REG : begin
				rd_d[5:0]	<= c7_width;
				rd_d[31:6]	<= 'b0;
				if (wr_en) begin
					c7_width <= wr_d[5:0];
					c7_shape_load <= 1'b1;
				end
			end

			`C7_ADDWIDTH_REG : begin
				rd_d[7:0]	<= c7_add_width;
				rd_d[31:8]	<= 'b0;
				if (wr_en) begin
					c7_add_width <= wr_d[7:0];
					c7_shape_load <= 1'b1;
				end
			end			
			
			`C7_OFFSET_QUOT_REG : begin
                rd_d[6:0]    <= c7_i_offset_quotient;
                rd_d[31:7]    <= 'b0;
                if (wr_en) begin
                    c7_i_offset_quotient <= wr_d[6:0];
                    c7_shape_load <= 1'b1;
                end
            end
            
            `C7_OFFSET_REMD_REG : begin
                rd_d[2:0]    <= c7_i_offset_remainder;
                rd_d[31:3]    <= 'b0;
                if (wr_en) begin
                    c7_i_offset_remainder <= wr_d[2:0];
                    c7_shape_load <= 1'b1;
                end
            end
			
			`C7_DELAY_TAP_REG : begin
                rd_d[4:0]    <= c7_delay_tap;
                rd_d[31:5]    <= 'b0;
                if (wr_en) begin
                    c7_delay_tap <= wr_d[4:0];
                end
            end

			`C7_ADD_PULSE_DELAY_REG : begin
                rd_d[14:0]    <= o_a7_delay_cnt;
                rd_d[31:15]    <= 'b0;
                if (wr_en) begin
                    o_a7_delay_cnt <= wr_d[14:0];
                end
            end            
            
// CON 8          
// CON 9
// CON 10
     
// C_OP		   
			`C_START_REG : begin
				rd_d[0]	<= op_start;
				rd_d[30:1]	<= 30'b0;

				if (wr_en) begin
					op_start <= wr_d[0];
					fifo_en		<=1'b1;
                    fifo_rst_r    <= 1'b1;					
				end
			end

			`C_FIFO_STAT_REG : begin
				rd_d[0]		<= fifo_en;
				rd_d[1]		<= fifo_rst;
				rd_d[7:2]	<= 6'b0;
				rd_d[8]		<= fifo_empty;
				rd_d[10:9]	<= 2'b0;
				rd_d[11]	<= fifo_pf;
				rd_d[12]	<= fifo_af;
				rd_d[13]	<= fifo_full;
				rd_d[31:14] <= 'b0;
				if (wr_en) begin
					fifo_en		<= wr_d[0];
					fifo_rst_r	<= wr_d[1];
				end
			end

			`C_FIFO_PF_THRES_REG : begin
				rd_d[15:0]	<= fifo_pf_thres;
				rd_d[31:16]	<= 16'b0;
				if (wr_en) begin
					fifo_pf_thres <= wr_d[15:0];
				end
			end

			`C_FIFO_READ_REG : begin
				rd_d[31:0] <= ff_out;
			end

			`C_TRIG_TYPE_REG : begin
                rd_d[1:0]    <= c_trig_type;
                rd_d[31:2]    <= 30'b0;
                if (wr_en) begin
                    c_trig_type <= wr_d[1:0];
                end
            end
            
            `C_SPD12_CNT_REG : begin
                rd_d[31:0]    <= c_spad12_cnt;
            end  

            `C_SPD13_CNT_REG : begin
                rd_d[31:0]    <= c_spad13_cnt;
            end  

            `C_SPD24_CNT_REG : begin
                rd_d[31:0]    <= c_spad24_cnt;
            end  
            
            `C_SPD34_CNT_REG : begin
                rd_d[31:0]    <= c_spad34_cnt;
            end           
            `C_SPD1234_CNT_REG : begin
                rd_d[31:0]    <= c_spad1234_cnt;
            end    
            
  			`C_FIFO_LOG_DELAY_REG : begin
                rd_d[14:0]    <= fifo_log_delay_cnt;
                rd_d[31:15]    <= 17'b0;
                if (wr_en) begin
                    fifo_log_delay_cnt <= wr_d[14:0];
                end
            end                                                             
			default : begin
				rd_d <= 32'b0;
			end
		endcase

		////////////////////////////////////////////////
		// ?? ??λ¦?.
		if (!wr_en) begin
			c1_shape_load		<= 1'b0;
			c2_shape_load		<= 1'b0;
			c3_shape_load		<= 1'b0;
			c4_shape_load		<= 1'b0;
			
			c6_shape_load		<= 1'b0;
			c7_shape_load		<= 1'b0;
		

			
			op_start			<= 1'b0;
			fifo_rst_r 			<= 1'b0;

		end


	end
end


wire fifo_rst_r_d;
pulse_delay pulse_delay_fifo_rst (
    .clk(clk), // I
    .sclr(rst), // I
    .i_delay_cnt(fifo_log_delay_cnt), // I [14:0]
    .i_start_pulse(sig_start_flag&sig_clk_10M_pulse), // I
    .i_ref_pulse(sig_clk_10M_pulse), // I
    .o_delayed_pulse(fifo_rst_r_d) // O

);

pls_expander #(
	.COUNT(`C2H_RST_PCIECLK),
	.POLARITY(1)
) c2h_0_rst_ext(
	.sin(fifo_rst_r_d&sig_clk_10M_pulse),
	.clk(clk),
	.sout(fifo_rst)
);

assign				dir_fifo = fifo_af;
fifo_32x64k pcie_fifo (
  .clk(clk),						// I
  .srst(fifo_rst || !fifo_en),			// I
  .din(din_fifo),						// I [31 : 0]
  .wr_en(div_fifo && ~fifo_full),		// I
  .rd_en(fifo_rd_en && !fifo_empty),	// I
  .prog_full_thresh(fifo_pf_thres),		// I [15 : 0]
  .dout(ff_out),						// O [31 : 0]
  .full(fifo_full),						// O
  .almost_full(fifo_af),				// O
  .empty(fifo_empty),					// O
  .valid(),								// O
  .prog_full(fifo_pf)					// O
);	// latency = 2





endmodule