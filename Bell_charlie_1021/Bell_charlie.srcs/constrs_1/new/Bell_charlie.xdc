# Company: HURA
# Engineer: LEE Seong Yun (sylee@hura.co.kr)
#
# Create Date: 2019/01/02 11:40:00
# Design Name:
# Module Name:
# Project Name:
# Target Devices: Kintex7 160T
# Tool Versions: Vivado 2018.1
# Description:
#
# Dependencies:
#
# Revision:
# Revision build 1
# Additional Comments:
#
# -------------------------------------------------------------------------------

set_false_path -through [get_cells -hierarchical *sout*]
set_false_path -to [get_ports F_CLK_3_3]
set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets _18SMA_REF_CLK_IBUF]
#CON1

#set_false_path -through [get_nets -hierarchical *shape_ld*]
set_false_path -through [get_nets -hierarchical *width*]

set_false_path -through [get_nets -hierarchical *add_width*]
set_false_path -through [get_nets -hierarchical *offset*]
set_false_path -through [get_nets -hierarchical *test_data*]
set_false_path -through [get_nets -hierarchical *test_mode*]
set_false_path -through [get_nets -hierarchical *delay_tap_in*]
set_false_path -through [get_nets -hierarchical *delay_tap_out*]
set_false_path -through [get_nets -hierarchical *hmc920_en*]
set_false_path -through [get_nets -hierarchical *tec_en*]
set_false_path -through [get_nets -hierarchical *tec_shdn_b*]
set_false_path -through [get_nets -hierarchical *tec_clr*]

set_false_path -through [get_nets -hierarchical *fpc_en*]
set_false_path -through [get_nets -hierarchical *fpc_dac_clr*]
set_false_path -through [get_nets -hierarchical *fpc10_dac_clr*]
# -------------------------------------------------------------------------------
#CON2


# -------------------------------------------------------------------------------
#CON3

# -------------------------------------------------------------------------------
#CON4


# -------------------------------------------------------------------------------
#CON5



# -------------------------------------------------------------------------------
#CON6


# -------------------------------------------------------------------------------
#CON7

# -------------------------------------------------------------------------------
#CON8


# -------------------------------------------------------------------------------
#CON9


# -------------------------------------------------------------------------------
#CON10


# -------------------------------------------------------------------------------

#Groups a set of IDELAY and IODELAY constraints with an IDELAYCTRL to enable automatic replication and placement of IDELAYCTRL in a design
set_property IODELAY_GROUP IO_DLY1 [get_cells {con1_intf_charlie/trigmask_10M_gen/selectio_6to1_dc/inst/pins[0].odelaye2_bus}]
set_property IODELAY_GROUP IO_DLY1 [get_cells {con2_intf_charlie/trigmask_10M_gen/selectio_6to1_dc/inst/pins[0].odelaye2_bus}]
set_property IODELAY_GROUP IO_DLY1 [get_cells {con3_intf_charlie/trigmask_10M_gen/selectio_6to1_dc/inst/pins[0].odelaye2_bus}]
set_property IODELAY_GROUP IO_DLY1 [get_cells {con4_intf_charlie/trigmask_10M_gen/selectio_6to1_dc/inst/pins[0].odelaye2_bus}]
set_property IODELAY_GROUP IO_DLY1 [get_cells {con6_intf_charlie/laser_trig_gen_1200/selectio_6to1_dc/inst/pins[0].odelaye2_bus}]
set_property IODELAY_GROUP IO_DLY1 [get_cells {con7_intf_charlie/laser_trig_gen_1200/selectio_6to1_dc/inst/pins[0].odelaye2_bus}]
set_property IODELAY_GROUP IO_DLY1 [get_cells {con1_intf_charlie/spad_detect_1/selectio_1to6/inst/pins[0].idelaye2_bus}]
set_property IODELAY_GROUP IO_DLY1 [get_cells {con1_intf_charlie/spad_detect_2/selectio_1to6/inst/pins[0].idelaye2_bus}]
set_property IODELAY_GROUP IO_DLY1 [get_cells {con2_intf_charlie/spad_detect_1/selectio_1to6/inst/pins[0].idelaye2_bus}]
set_property IODELAY_GROUP IO_DLY1 [get_cells {con2_intf_charlie/spad_detect_2/selectio_1to6/inst/pins[0].idelaye2_bus}]
set_property IODELAY_GROUP IO_DLY1 [get_cells {con3_intf_charlie/spad_detect_1/selectio_1to6/inst/pins[0].idelaye2_bus}]
set_property IODELAY_GROUP IO_DLY1 [get_cells {con3_intf_charlie/spad_detect_2/selectio_1to6/inst/pins[0].idelaye2_bus}]
set_property IODELAY_GROUP IO_DLY1 [get_cells {con4_intf_charlie/spad_detect_1/selectio_1to6/inst/pins[0].idelaye2_bus}]
set_property IODELAY_GROUP IO_DLY1 [get_cells {con4_intf_charlie/spad_detect_2/selectio_1to6/inst/pins[0].idelaye2_bus}]

set_property IODELAY_GROUP IO_DLY1 [get_cells IDELAYCTRL_SELECTIO_8TO1]







create_debug_core u_ila_0 ila
set_property ALL_PROBE_SAME_MU true [get_debug_cores u_ila_0]
set_property ALL_PROBE_SAME_MU_CNT 1 [get_debug_cores u_ila_0]
set_property C_ADV_TRIGGER false [get_debug_cores u_ila_0]
set_property C_DATA_DEPTH 2048 [get_debug_cores u_ila_0]
set_property C_EN_STRG_QUAL false [get_debug_cores u_ila_0]
set_property C_INPUT_PIPE_STAGES 0 [get_debug_cores u_ila_0]
set_property C_TRIGIN_EN false [get_debug_cores u_ila_0]
set_property C_TRIGOUT_EN false [get_debug_cores u_ila_0]
set_property port_width 1 [get_debug_ports u_ila_0/clk]
connect_debug_port u_ila_0/clk [get_nets [list clk_sig_mux/inst/clk_out1]]
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe0]
set_property port_width 4 [get_debug_ports u_ila_0/probe0]
connect_debug_port u_ila_0/probe0 [get_nets [list {log_8bit/din_log_4to8[0]} {log_8bit/din_log_4to8[1]} {log_8bit/din_log_4to8[2]} {log_8bit/din_log_4to8[3]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe1]
set_property port_width 8 [get_debug_ports u_ila_0/probe1]
connect_debug_port u_ila_0/probe1 [get_nets [list {log_8bit/do_log_4to8[0]} {log_8bit/do_log_4to8[1]} {log_8bit/do_log_4to8[2]} {log_8bit/do_log_4to8[3]} {log_8bit/do_log_4to8[4]} {log_8bit/do_log_4to8[5]} {log_8bit/do_log_4to8[6]} {log_8bit/do_log_4to8[7]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe2]
set_property port_width 15 [get_debug_ports u_ila_0/probe2]
connect_debug_port u_ila_0/probe2 [get_nets [list {regs_sig_charlie/fifo_log_delay_cnt[0]} {regs_sig_charlie/fifo_log_delay_cnt[1]} {regs_sig_charlie/fifo_log_delay_cnt[2]} {regs_sig_charlie/fifo_log_delay_cnt[3]} {regs_sig_charlie/fifo_log_delay_cnt[4]} {regs_sig_charlie/fifo_log_delay_cnt[5]} {regs_sig_charlie/fifo_log_delay_cnt[6]} {regs_sig_charlie/fifo_log_delay_cnt[7]} {regs_sig_charlie/fifo_log_delay_cnt[8]} {regs_sig_charlie/fifo_log_delay_cnt[9]} {regs_sig_charlie/fifo_log_delay_cnt[10]} {regs_sig_charlie/fifo_log_delay_cnt[11]} {regs_sig_charlie/fifo_log_delay_cnt[12]} {regs_sig_charlie/fifo_log_delay_cnt[13]} {regs_sig_charlie/fifo_log_delay_cnt[14]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe3]
set_property port_width 16 [get_debug_ports u_ila_0/probe3]
connect_debug_port u_ila_0/probe3 [get_nets [list {regs_sig_charlie/fifo_pf_thres[0]} {regs_sig_charlie/fifo_pf_thres[1]} {regs_sig_charlie/fifo_pf_thres[2]} {regs_sig_charlie/fifo_pf_thres[3]} {regs_sig_charlie/fifo_pf_thres[4]} {regs_sig_charlie/fifo_pf_thres[5]} {regs_sig_charlie/fifo_pf_thres[6]} {regs_sig_charlie/fifo_pf_thres[7]} {regs_sig_charlie/fifo_pf_thres[8]} {regs_sig_charlie/fifo_pf_thres[9]} {regs_sig_charlie/fifo_pf_thres[10]} {regs_sig_charlie/fifo_pf_thres[11]} {regs_sig_charlie/fifo_pf_thres[12]} {regs_sig_charlie/fifo_pf_thres[13]} {regs_sig_charlie/fifo_pf_thres[14]} {regs_sig_charlie/fifo_pf_thres[15]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe4]
set_property port_width 32 [get_debug_ports u_ila_0/probe4]
connect_debug_port u_ila_0/probe4 [get_nets [list {regs_sig_charlie/ff_out[0]} {regs_sig_charlie/ff_out[1]} {regs_sig_charlie/ff_out[2]} {regs_sig_charlie/ff_out[3]} {regs_sig_charlie/ff_out[4]} {regs_sig_charlie/ff_out[5]} {regs_sig_charlie/ff_out[6]} {regs_sig_charlie/ff_out[7]} {regs_sig_charlie/ff_out[8]} {regs_sig_charlie/ff_out[9]} {regs_sig_charlie/ff_out[10]} {regs_sig_charlie/ff_out[11]} {regs_sig_charlie/ff_out[12]} {regs_sig_charlie/ff_out[13]} {regs_sig_charlie/ff_out[14]} {regs_sig_charlie/ff_out[15]} {regs_sig_charlie/ff_out[16]} {regs_sig_charlie/ff_out[17]} {regs_sig_charlie/ff_out[18]} {regs_sig_charlie/ff_out[19]} {regs_sig_charlie/ff_out[20]} {regs_sig_charlie/ff_out[21]} {regs_sig_charlie/ff_out[22]} {regs_sig_charlie/ff_out[23]} {regs_sig_charlie/ff_out[24]} {regs_sig_charlie/ff_out[25]} {regs_sig_charlie/ff_out[26]} {regs_sig_charlie/ff_out[27]} {regs_sig_charlie/ff_out[28]} {regs_sig_charlie/ff_out[29]} {regs_sig_charlie/ff_out[30]} {regs_sig_charlie/ff_out[31]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe5]
set_property port_width 32 [get_debug_ports u_ila_0/probe5]
connect_debug_port u_ila_0/probe5 [get_nets [list {simultaneous_coeff_mea/spd_12_cnt_tmp[0]} {simultaneous_coeff_mea/spd_12_cnt_tmp[1]} {simultaneous_coeff_mea/spd_12_cnt_tmp[2]} {simultaneous_coeff_mea/spd_12_cnt_tmp[3]} {simultaneous_coeff_mea/spd_12_cnt_tmp[4]} {simultaneous_coeff_mea/spd_12_cnt_tmp[5]} {simultaneous_coeff_mea/spd_12_cnt_tmp[6]} {simultaneous_coeff_mea/spd_12_cnt_tmp[7]} {simultaneous_coeff_mea/spd_12_cnt_tmp[8]} {simultaneous_coeff_mea/spd_12_cnt_tmp[9]} {simultaneous_coeff_mea/spd_12_cnt_tmp[10]} {simultaneous_coeff_mea/spd_12_cnt_tmp[11]} {simultaneous_coeff_mea/spd_12_cnt_tmp[12]} {simultaneous_coeff_mea/spd_12_cnt_tmp[13]} {simultaneous_coeff_mea/spd_12_cnt_tmp[14]} {simultaneous_coeff_mea/spd_12_cnt_tmp[15]} {simultaneous_coeff_mea/spd_12_cnt_tmp[16]} {simultaneous_coeff_mea/spd_12_cnt_tmp[17]} {simultaneous_coeff_mea/spd_12_cnt_tmp[18]} {simultaneous_coeff_mea/spd_12_cnt_tmp[19]} {simultaneous_coeff_mea/spd_12_cnt_tmp[20]} {simultaneous_coeff_mea/spd_12_cnt_tmp[21]} {simultaneous_coeff_mea/spd_12_cnt_tmp[22]} {simultaneous_coeff_mea/spd_12_cnt_tmp[23]} {simultaneous_coeff_mea/spd_12_cnt_tmp[24]} {simultaneous_coeff_mea/spd_12_cnt_tmp[25]} {simultaneous_coeff_mea/spd_12_cnt_tmp[26]} {simultaneous_coeff_mea/spd_12_cnt_tmp[27]} {simultaneous_coeff_mea/spd_12_cnt_tmp[28]} {simultaneous_coeff_mea/spd_12_cnt_tmp[29]} {simultaneous_coeff_mea/spd_12_cnt_tmp[30]} {simultaneous_coeff_mea/spd_12_cnt_tmp[31]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe6]
set_property port_width 32 [get_debug_ports u_ila_0/probe6]
connect_debug_port u_ila_0/probe6 [get_nets [list {simultaneous_coeff_mea/spd_34_cnt_tmp[0]} {simultaneous_coeff_mea/spd_34_cnt_tmp[1]} {simultaneous_coeff_mea/spd_34_cnt_tmp[2]} {simultaneous_coeff_mea/spd_34_cnt_tmp[3]} {simultaneous_coeff_mea/spd_34_cnt_tmp[4]} {simultaneous_coeff_mea/spd_34_cnt_tmp[5]} {simultaneous_coeff_mea/spd_34_cnt_tmp[6]} {simultaneous_coeff_mea/spd_34_cnt_tmp[7]} {simultaneous_coeff_mea/spd_34_cnt_tmp[8]} {simultaneous_coeff_mea/spd_34_cnt_tmp[9]} {simultaneous_coeff_mea/spd_34_cnt_tmp[10]} {simultaneous_coeff_mea/spd_34_cnt_tmp[11]} {simultaneous_coeff_mea/spd_34_cnt_tmp[12]} {simultaneous_coeff_mea/spd_34_cnt_tmp[13]} {simultaneous_coeff_mea/spd_34_cnt_tmp[14]} {simultaneous_coeff_mea/spd_34_cnt_tmp[15]} {simultaneous_coeff_mea/spd_34_cnt_tmp[16]} {simultaneous_coeff_mea/spd_34_cnt_tmp[17]} {simultaneous_coeff_mea/spd_34_cnt_tmp[18]} {simultaneous_coeff_mea/spd_34_cnt_tmp[19]} {simultaneous_coeff_mea/spd_34_cnt_tmp[20]} {simultaneous_coeff_mea/spd_34_cnt_tmp[21]} {simultaneous_coeff_mea/spd_34_cnt_tmp[22]} {simultaneous_coeff_mea/spd_34_cnt_tmp[23]} {simultaneous_coeff_mea/spd_34_cnt_tmp[24]} {simultaneous_coeff_mea/spd_34_cnt_tmp[25]} {simultaneous_coeff_mea/spd_34_cnt_tmp[26]} {simultaneous_coeff_mea/spd_34_cnt_tmp[27]} {simultaneous_coeff_mea/spd_34_cnt_tmp[28]} {simultaneous_coeff_mea/spd_34_cnt_tmp[29]} {simultaneous_coeff_mea/spd_34_cnt_tmp[30]} {simultaneous_coeff_mea/spd_34_cnt_tmp[31]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe7]
set_property port_width 32 [get_debug_ports u_ila_0/probe7]
connect_debug_port u_ila_0/probe7 [get_nets [list {simultaneous_coeff_mea/spd_13_cnt_tmp[0]} {simultaneous_coeff_mea/spd_13_cnt_tmp[1]} {simultaneous_coeff_mea/spd_13_cnt_tmp[2]} {simultaneous_coeff_mea/spd_13_cnt_tmp[3]} {simultaneous_coeff_mea/spd_13_cnt_tmp[4]} {simultaneous_coeff_mea/spd_13_cnt_tmp[5]} {simultaneous_coeff_mea/spd_13_cnt_tmp[6]} {simultaneous_coeff_mea/spd_13_cnt_tmp[7]} {simultaneous_coeff_mea/spd_13_cnt_tmp[8]} {simultaneous_coeff_mea/spd_13_cnt_tmp[9]} {simultaneous_coeff_mea/spd_13_cnt_tmp[10]} {simultaneous_coeff_mea/spd_13_cnt_tmp[11]} {simultaneous_coeff_mea/spd_13_cnt_tmp[12]} {simultaneous_coeff_mea/spd_13_cnt_tmp[13]} {simultaneous_coeff_mea/spd_13_cnt_tmp[14]} {simultaneous_coeff_mea/spd_13_cnt_tmp[15]} {simultaneous_coeff_mea/spd_13_cnt_tmp[16]} {simultaneous_coeff_mea/spd_13_cnt_tmp[17]} {simultaneous_coeff_mea/spd_13_cnt_tmp[18]} {simultaneous_coeff_mea/spd_13_cnt_tmp[19]} {simultaneous_coeff_mea/spd_13_cnt_tmp[20]} {simultaneous_coeff_mea/spd_13_cnt_tmp[21]} {simultaneous_coeff_mea/spd_13_cnt_tmp[22]} {simultaneous_coeff_mea/spd_13_cnt_tmp[23]} {simultaneous_coeff_mea/spd_13_cnt_tmp[24]} {simultaneous_coeff_mea/spd_13_cnt_tmp[25]} {simultaneous_coeff_mea/spd_13_cnt_tmp[26]} {simultaneous_coeff_mea/spd_13_cnt_tmp[27]} {simultaneous_coeff_mea/spd_13_cnt_tmp[28]} {simultaneous_coeff_mea/spd_13_cnt_tmp[29]} {simultaneous_coeff_mea/spd_13_cnt_tmp[30]} {simultaneous_coeff_mea/spd_13_cnt_tmp[31]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe8]
set_property port_width 32 [get_debug_ports u_ila_0/probe8]
connect_debug_port u_ila_0/probe8 [get_nets [list {simultaneous_coeff_mea/spd_24_cnt_tmp[0]} {simultaneous_coeff_mea/spd_24_cnt_tmp[1]} {simultaneous_coeff_mea/spd_24_cnt_tmp[2]} {simultaneous_coeff_mea/spd_24_cnt_tmp[3]} {simultaneous_coeff_mea/spd_24_cnt_tmp[4]} {simultaneous_coeff_mea/spd_24_cnt_tmp[5]} {simultaneous_coeff_mea/spd_24_cnt_tmp[6]} {simultaneous_coeff_mea/spd_24_cnt_tmp[7]} {simultaneous_coeff_mea/spd_24_cnt_tmp[8]} {simultaneous_coeff_mea/spd_24_cnt_tmp[9]} {simultaneous_coeff_mea/spd_24_cnt_tmp[10]} {simultaneous_coeff_mea/spd_24_cnt_tmp[11]} {simultaneous_coeff_mea/spd_24_cnt_tmp[12]} {simultaneous_coeff_mea/spd_24_cnt_tmp[13]} {simultaneous_coeff_mea/spd_24_cnt_tmp[14]} {simultaneous_coeff_mea/spd_24_cnt_tmp[15]} {simultaneous_coeff_mea/spd_24_cnt_tmp[16]} {simultaneous_coeff_mea/spd_24_cnt_tmp[17]} {simultaneous_coeff_mea/spd_24_cnt_tmp[18]} {simultaneous_coeff_mea/spd_24_cnt_tmp[19]} {simultaneous_coeff_mea/spd_24_cnt_tmp[20]} {simultaneous_coeff_mea/spd_24_cnt_tmp[21]} {simultaneous_coeff_mea/spd_24_cnt_tmp[22]} {simultaneous_coeff_mea/spd_24_cnt_tmp[23]} {simultaneous_coeff_mea/spd_24_cnt_tmp[24]} {simultaneous_coeff_mea/spd_24_cnt_tmp[25]} {simultaneous_coeff_mea/spd_24_cnt_tmp[26]} {simultaneous_coeff_mea/spd_24_cnt_tmp[27]} {simultaneous_coeff_mea/spd_24_cnt_tmp[28]} {simultaneous_coeff_mea/spd_24_cnt_tmp[29]} {simultaneous_coeff_mea/spd_24_cnt_tmp[30]} {simultaneous_coeff_mea/spd_24_cnt_tmp[31]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe9]
set_property port_width 2 [get_debug_ports u_ila_0/probe9]
connect_debug_port u_ila_0/probe9 [get_nets [list {c_trig_type[0]} {c_trig_type[1]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe10]
set_property port_width 32 [get_debug_ports u_ila_0/probe10]
connect_debug_port u_ila_0/probe10 [get_nets [list {con1_intf_charlie/trig1_cnt_tmp[0]} {con1_intf_charlie/trig1_cnt_tmp[1]} {con1_intf_charlie/trig1_cnt_tmp[2]} {con1_intf_charlie/trig1_cnt_tmp[3]} {con1_intf_charlie/trig1_cnt_tmp[4]} {con1_intf_charlie/trig1_cnt_tmp[5]} {con1_intf_charlie/trig1_cnt_tmp[6]} {con1_intf_charlie/trig1_cnt_tmp[7]} {con1_intf_charlie/trig1_cnt_tmp[8]} {con1_intf_charlie/trig1_cnt_tmp[9]} {con1_intf_charlie/trig1_cnt_tmp[10]} {con1_intf_charlie/trig1_cnt_tmp[11]} {con1_intf_charlie/trig1_cnt_tmp[12]} {con1_intf_charlie/trig1_cnt_tmp[13]} {con1_intf_charlie/trig1_cnt_tmp[14]} {con1_intf_charlie/trig1_cnt_tmp[15]} {con1_intf_charlie/trig1_cnt_tmp[16]} {con1_intf_charlie/trig1_cnt_tmp[17]} {con1_intf_charlie/trig1_cnt_tmp[18]} {con1_intf_charlie/trig1_cnt_tmp[19]} {con1_intf_charlie/trig1_cnt_tmp[20]} {con1_intf_charlie/trig1_cnt_tmp[21]} {con1_intf_charlie/trig1_cnt_tmp[22]} {con1_intf_charlie/trig1_cnt_tmp[23]} {con1_intf_charlie/trig1_cnt_tmp[24]} {con1_intf_charlie/trig1_cnt_tmp[25]} {con1_intf_charlie/trig1_cnt_tmp[26]} {con1_intf_charlie/trig1_cnt_tmp[27]} {con1_intf_charlie/trig1_cnt_tmp[28]} {con1_intf_charlie/trig1_cnt_tmp[29]} {con1_intf_charlie/trig1_cnt_tmp[30]} {con1_intf_charlie/trig1_cnt_tmp[31]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe11]
set_property port_width 32 [get_debug_ports u_ila_0/probe11]
connect_debug_port u_ila_0/probe11 [get_nets [list {con1_intf_charlie/trig3_cnt_tmp[0]} {con1_intf_charlie/trig3_cnt_tmp[1]} {con1_intf_charlie/trig3_cnt_tmp[2]} {con1_intf_charlie/trig3_cnt_tmp[3]} {con1_intf_charlie/trig3_cnt_tmp[4]} {con1_intf_charlie/trig3_cnt_tmp[5]} {con1_intf_charlie/trig3_cnt_tmp[6]} {con1_intf_charlie/trig3_cnt_tmp[7]} {con1_intf_charlie/trig3_cnt_tmp[8]} {con1_intf_charlie/trig3_cnt_tmp[9]} {con1_intf_charlie/trig3_cnt_tmp[10]} {con1_intf_charlie/trig3_cnt_tmp[11]} {con1_intf_charlie/trig3_cnt_tmp[12]} {con1_intf_charlie/trig3_cnt_tmp[13]} {con1_intf_charlie/trig3_cnt_tmp[14]} {con1_intf_charlie/trig3_cnt_tmp[15]} {con1_intf_charlie/trig3_cnt_tmp[16]} {con1_intf_charlie/trig3_cnt_tmp[17]} {con1_intf_charlie/trig3_cnt_tmp[18]} {con1_intf_charlie/trig3_cnt_tmp[19]} {con1_intf_charlie/trig3_cnt_tmp[20]} {con1_intf_charlie/trig3_cnt_tmp[21]} {con1_intf_charlie/trig3_cnt_tmp[22]} {con1_intf_charlie/trig3_cnt_tmp[23]} {con1_intf_charlie/trig3_cnt_tmp[24]} {con1_intf_charlie/trig3_cnt_tmp[25]} {con1_intf_charlie/trig3_cnt_tmp[26]} {con1_intf_charlie/trig3_cnt_tmp[27]} {con1_intf_charlie/trig3_cnt_tmp[28]} {con1_intf_charlie/trig3_cnt_tmp[29]} {con1_intf_charlie/trig3_cnt_tmp[30]} {con1_intf_charlie/trig3_cnt_tmp[31]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe12]
set_property port_width 32 [get_debug_ports u_ila_0/probe12]
connect_debug_port u_ila_0/probe12 [get_nets [list {con1_intf_charlie/trig4_cnt_tmp[0]} {con1_intf_charlie/trig4_cnt_tmp[1]} {con1_intf_charlie/trig4_cnt_tmp[2]} {con1_intf_charlie/trig4_cnt_tmp[3]} {con1_intf_charlie/trig4_cnt_tmp[4]} {con1_intf_charlie/trig4_cnt_tmp[5]} {con1_intf_charlie/trig4_cnt_tmp[6]} {con1_intf_charlie/trig4_cnt_tmp[7]} {con1_intf_charlie/trig4_cnt_tmp[8]} {con1_intf_charlie/trig4_cnt_tmp[9]} {con1_intf_charlie/trig4_cnt_tmp[10]} {con1_intf_charlie/trig4_cnt_tmp[11]} {con1_intf_charlie/trig4_cnt_tmp[12]} {con1_intf_charlie/trig4_cnt_tmp[13]} {con1_intf_charlie/trig4_cnt_tmp[14]} {con1_intf_charlie/trig4_cnt_tmp[15]} {con1_intf_charlie/trig4_cnt_tmp[16]} {con1_intf_charlie/trig4_cnt_tmp[17]} {con1_intf_charlie/trig4_cnt_tmp[18]} {con1_intf_charlie/trig4_cnt_tmp[19]} {con1_intf_charlie/trig4_cnt_tmp[20]} {con1_intf_charlie/trig4_cnt_tmp[21]} {con1_intf_charlie/trig4_cnt_tmp[22]} {con1_intf_charlie/trig4_cnt_tmp[23]} {con1_intf_charlie/trig4_cnt_tmp[24]} {con1_intf_charlie/trig4_cnt_tmp[25]} {con1_intf_charlie/trig4_cnt_tmp[26]} {con1_intf_charlie/trig4_cnt_tmp[27]} {con1_intf_charlie/trig4_cnt_tmp[28]} {con1_intf_charlie/trig4_cnt_tmp[29]} {con1_intf_charlie/trig4_cnt_tmp[30]} {con1_intf_charlie/trig4_cnt_tmp[31]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe13]
set_property port_width 32 [get_debug_ports u_ila_0/probe13]
connect_debug_port u_ila_0/probe13 [get_nets [list {con1_intf_charlie/trig2_cnt_tmp[0]} {con1_intf_charlie/trig2_cnt_tmp[1]} {con1_intf_charlie/trig2_cnt_tmp[2]} {con1_intf_charlie/trig2_cnt_tmp[3]} {con1_intf_charlie/trig2_cnt_tmp[4]} {con1_intf_charlie/trig2_cnt_tmp[5]} {con1_intf_charlie/trig2_cnt_tmp[6]} {con1_intf_charlie/trig2_cnt_tmp[7]} {con1_intf_charlie/trig2_cnt_tmp[8]} {con1_intf_charlie/trig2_cnt_tmp[9]} {con1_intf_charlie/trig2_cnt_tmp[10]} {con1_intf_charlie/trig2_cnt_tmp[11]} {con1_intf_charlie/trig2_cnt_tmp[12]} {con1_intf_charlie/trig2_cnt_tmp[13]} {con1_intf_charlie/trig2_cnt_tmp[14]} {con1_intf_charlie/trig2_cnt_tmp[15]} {con1_intf_charlie/trig2_cnt_tmp[16]} {con1_intf_charlie/trig2_cnt_tmp[17]} {con1_intf_charlie/trig2_cnt_tmp[18]} {con1_intf_charlie/trig2_cnt_tmp[19]} {con1_intf_charlie/trig2_cnt_tmp[20]} {con1_intf_charlie/trig2_cnt_tmp[21]} {con1_intf_charlie/trig2_cnt_tmp[22]} {con1_intf_charlie/trig2_cnt_tmp[23]} {con1_intf_charlie/trig2_cnt_tmp[24]} {con1_intf_charlie/trig2_cnt_tmp[25]} {con1_intf_charlie/trig2_cnt_tmp[26]} {con1_intf_charlie/trig2_cnt_tmp[27]} {con1_intf_charlie/trig2_cnt_tmp[28]} {con1_intf_charlie/trig2_cnt_tmp[29]} {con1_intf_charlie/trig2_cnt_tmp[30]} {con1_intf_charlie/trig2_cnt_tmp[31]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe14]
set_property port_width 1 [get_debug_ports u_ila_0/probe14]
connect_debug_port u_ila_0/probe14 [get_nets [list c1_mask_10M]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe15]
set_property port_width 1 [get_debug_ports u_ila_0/probe15]
connect_debug_port u_ila_0/probe15 [get_nets [list c1_spd_trig]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe16]
set_property port_width 1 [get_debug_ports u_ila_0/probe16]
connect_debug_port u_ila_0/probe16 [get_nets [list c1_spd_trig_valid]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe17]
set_property port_width 1 [get_debug_ports u_ila_0/probe17]
connect_debug_port u_ila_0/probe17 [get_nets [list c2_mask_10M]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe18]
set_property port_width 1 [get_debug_ports u_ila_0/probe18]
connect_debug_port u_ila_0/probe18 [get_nets [list c2_spd_trig]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe19]
set_property port_width 1 [get_debug_ports u_ila_0/probe19]
connect_debug_port u_ila_0/probe19 [get_nets [list c2_spd_trig_valid]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe20]
set_property port_width 1 [get_debug_ports u_ila_0/probe20]
connect_debug_port u_ila_0/probe20 [get_nets [list c3_mask_10M]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe21]
set_property port_width 1 [get_debug_ports u_ila_0/probe21]
connect_debug_port u_ila_0/probe21 [get_nets [list c3_spd_trig]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe22]
set_property port_width 1 [get_debug_ports u_ila_0/probe22]
connect_debug_port u_ila_0/probe22 [get_nets [list c3_spd_trig_valid]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe23]
set_property port_width 1 [get_debug_ports u_ila_0/probe23]
connect_debug_port u_ila_0/probe23 [get_nets [list c4_mask_10M]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe24]
set_property port_width 1 [get_debug_ports u_ila_0/probe24]
connect_debug_port u_ila_0/probe24 [get_nets [list c4_spd_trig]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe25]
set_property port_width 1 [get_debug_ports u_ila_0/probe25]
connect_debug_port u_ila_0/probe25 [get_nets [list c4_spd_trig_valid]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe26]
set_property port_width 1 [get_debug_ports u_ila_0/probe26]
connect_debug_port u_ila_0/probe26 [get_nets [list con1_intf_charlie/dff_trig]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe27]
set_property port_width 1 [get_debug_ports u_ila_0/probe27]
connect_debug_port u_ila_0/probe27 [get_nets [list con1_intf_charlie/do_trig1]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe28]
set_property port_width 1 [get_debug_ports u_ila_0/probe28]
connect_debug_port u_ila_0/probe28 [get_nets [list con1_intf_charlie/do_trig2]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe29]
set_property port_width 1 [get_debug_ports u_ila_0/probe29]
connect_debug_port u_ila_0/probe29 [get_nets [list con4_intf_charlie/do_trig2]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe30]
set_property port_width 1 [get_debug_ports u_ila_0/probe30]
connect_debug_port u_ila_0/probe30 [get_nets [list con2_intf_charlie/do_trig2]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe31]
set_property port_width 1 [get_debug_ports u_ila_0/probe31]
connect_debug_port u_ila_0/probe31 [get_nets [list con3_intf_charlie/do_trig2]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe32]
set_property port_width 1 [get_debug_ports u_ila_0/probe32]
connect_debug_port u_ila_0/probe32 [get_nets [list con4_intf_charlie/spad_detect_1/dout_trig]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe33]
set_property port_width 1 [get_debug_ports u_ila_0/probe33]
connect_debug_port u_ila_0/probe33 [get_nets [list con3_intf_charlie/spad_detect_2/dout_trig]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe34]
set_property port_width 1 [get_debug_ports u_ila_0/probe34]
connect_debug_port u_ila_0/probe34 [get_nets [list con2_intf_charlie/spad_detect_2/dout_trig]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe35]
set_property port_width 1 [get_debug_ports u_ila_0/probe35]
connect_debug_port u_ila_0/probe35 [get_nets [list con2_intf_charlie/spad_detect_1/dout_trig]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe36]
set_property port_width 1 [get_debug_ports u_ila_0/probe36]
connect_debug_port u_ila_0/probe36 [get_nets [list con3_intf_charlie/spad_detect_1/dout_trig]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe37]
set_property port_width 1 [get_debug_ports u_ila_0/probe37]
connect_debug_port u_ila_0/probe37 [get_nets [list con1_intf_charlie/spad_detect_2/dout_trig]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe38]
set_property port_width 1 [get_debug_ports u_ila_0/probe38]
connect_debug_port u_ila_0/probe38 [get_nets [list con1_intf_charlie/spad_detect_1/dout_trig]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe39]
set_property port_width 1 [get_debug_ports u_ila_0/probe39]
connect_debug_port u_ila_0/probe39 [get_nets [list con4_intf_charlie/spad_detect_2/dout_trig]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe40]
set_property port_width 1 [get_debug_ports u_ila_0/probe40]
connect_debug_port u_ila_0/probe40 [get_nets [list log_8bit/dov_log0]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe41]
set_property port_width 1 [get_debug_ports u_ila_0/probe41]
connect_debug_port u_ila_0/probe41 [get_nets [list log_8bit/dov_log_4to8]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe42]
set_property port_width 1 [get_debug_ports u_ila_0/probe42]
connect_debug_port u_ila_0/probe42 [get_nets [list regs_sig_charlie/fifo_af]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe43]
set_property port_width 1 [get_debug_ports u_ila_0/probe43]
connect_debug_port u_ila_0/probe43 [get_nets [list regs_sig_charlie/fifo_empty]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe44]
set_property port_width 1 [get_debug_ports u_ila_0/probe44]
connect_debug_port u_ila_0/probe44 [get_nets [list regs_sig_charlie/fifo_en]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe45]
set_property port_width 1 [get_debug_ports u_ila_0/probe45]
connect_debug_port u_ila_0/probe45 [get_nets [list regs_sig_charlie/fifo_full]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe46]
set_property port_width 1 [get_debug_ports u_ila_0/probe46]
connect_debug_port u_ila_0/probe46 [get_nets [list regs_sig_charlie/fifo_pf]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe47]
set_property port_width 1 [get_debug_ports u_ila_0/probe47]
connect_debug_port u_ila_0/probe47 [get_nets [list regs_sig_charlie/fifo_rd_en]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe48]
set_property port_width 1 [get_debug_ports u_ila_0/probe48]
connect_debug_port u_ila_0/probe48 [get_nets [list regs_sig_charlie/fifo_rst_r]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe49]
set_property port_width 1 [get_debug_ports u_ila_0/probe49]
connect_debug_port u_ila_0/probe49 [get_nets [list con1_intf_charlie/neg_trig]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe50]
set_property port_width 1 [get_debug_ports u_ila_0/probe50]
connect_debug_port u_ila_0/probe50 [get_nets [list o_spd_trig12]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe51]
set_property port_width 1 [get_debug_ports u_ila_0/probe51]
connect_debug_port u_ila_0/probe51 [get_nets [list o_spd_trig13]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe52]
set_property port_width 1 [get_debug_ports u_ila_0/probe52]
connect_debug_port u_ila_0/probe52 [get_nets [list o_spd_trig24]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe53]
set_property port_width 1 [get_debug_ports u_ila_0/probe53]
connect_debug_port u_ila_0/probe53 [get_nets [list o_spd_trig34]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe54]
set_property port_width 1 [get_debug_ports u_ila_0/probe54]
connect_debug_port u_ila_0/probe54 [get_nets [list op_start]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe55]
set_property port_width 1 [get_debug_ports u_ila_0/probe55]
connect_debug_port u_ila_0/probe55 [get_nets [list con1_intf_charlie/or_trig]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe56]
set_property port_width 1 [get_debug_ports u_ila_0/probe56]
connect_debug_port u_ila_0/probe56 [get_nets [list regs_sig_charlie/reg_re]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe57]
set_property port_width 1 [get_debug_ports u_ila_0/probe57]
connect_debug_port u_ila_0/probe57 [get_nets [list shape_ld]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe58]
set_property port_width 1 [get_debug_ports u_ila_0/probe58]
connect_debug_port u_ila_0/probe58 [get_nets [list shape_ld_trig]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe59]
set_property port_width 1 [get_debug_ports u_ila_0/probe59]
connect_debug_port u_ila_0/probe59 [get_nets [list shape_load]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe60]
set_property port_width 1 [get_debug_ports u_ila_0/probe60]
connect_debug_port u_ila_0/probe60 [get_nets [list shape_load_trig]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe61]
set_property port_width 1 [get_debug_ports u_ila_0/probe61]
connect_debug_port u_ila_0/probe61 [get_nets [list con3_intf_charlie/sig_10M]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe62]
set_property port_width 1 [get_debug_ports u_ila_0/probe62]
connect_debug_port u_ila_0/probe62 [get_nets [list con4_intf_charlie/sig_10M]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe63]
set_property port_width 1 [get_debug_ports u_ila_0/probe63]
connect_debug_port u_ila_0/probe63 [get_nets [list con1_intf_charlie/sig_10M]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe64]
set_property port_width 1 [get_debug_ports u_ila_0/probe64]
connect_debug_port u_ila_0/probe64 [get_nets [list con2_intf_charlie/sig_10M]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe65]
set_property port_width 1 [get_debug_ports u_ila_0/probe65]
connect_debug_port u_ila_0/probe65 [get_nets [list sig_start_flag]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe66]
set_property port_width 1 [get_debug_ports u_ila_0/probe66]
connect_debug_port u_ila_0/probe66 [get_nets [list sig_start_log]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe67]
set_property port_width 1 [get_debug_ports u_ila_0/probe67]
connect_debug_port u_ila_0/probe67 [get_nets [list con1_intf_charlie/sing_trig]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe68]
set_property port_width 1 [get_debug_ports u_ila_0/probe68]
connect_debug_port u_ila_0/probe68 [get_nets [list con1_intf_charlie/trig1]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe69]
set_property port_width 1 [get_debug_ports u_ila_0/probe69]
connect_debug_port u_ila_0/probe69 [get_nets [list con1_intf_charlie/trig1_r]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe70]
set_property port_width 1 [get_debug_ports u_ila_0/probe70]
connect_debug_port u_ila_0/probe70 [get_nets [list con1_intf_charlie/trig2]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe71]
set_property port_width 1 [get_debug_ports u_ila_0/probe71]
connect_debug_port u_ila_0/probe71 [get_nets [list con1_intf_charlie/trig2_r]]
create_debug_core u_ila_1 ila
set_property ALL_PROBE_SAME_MU true [get_debug_cores u_ila_1]
set_property ALL_PROBE_SAME_MU_CNT 1 [get_debug_cores u_ila_1]
set_property C_ADV_TRIGGER false [get_debug_cores u_ila_1]
set_property C_DATA_DEPTH 2048 [get_debug_cores u_ila_1]
set_property C_EN_STRG_QUAL false [get_debug_cores u_ila_1]
set_property C_INPUT_PIPE_STAGES 0 [get_debug_cores u_ila_1]
set_property C_TRIGIN_EN false [get_debug_cores u_ila_1]
set_property C_TRIGOUT_EN false [get_debug_cores u_ila_1]
set_property port_width 1 [get_debug_ports u_ila_1/clk]
connect_debug_port u_ila_1/clk [get_nets [list xdma_pcie_ep/xdma_0_i/inst/xdma_0_pcie2_to_pcie3_wrapper_i/pcie2_ip_i/inst/inst/gt_top_i/pipe_wrapper_i/pipe_clock_int.pipe_clock_i/INT_USERCLK2_OUT]]
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_1/probe0]
set_property port_width 2 [get_debug_ports u_ila_1/probe0]
connect_debug_port u_ila_1/probe0 [get_nets [list {pcie_dma_data_sel/data_sel[0]} {pcie_dma_data_sel/data_sel[1]}]]
create_debug_port u_ila_1 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_1/probe1]
set_property port_width 128 [get_debug_ports u_ila_1/probe1]
connect_debug_port u_ila_1/probe1 [get_nets [list {pcie_dma_data_sel/din_fifo_tdata[0]} {pcie_dma_data_sel/din_fifo_tdata[1]} {pcie_dma_data_sel/din_fifo_tdata[2]} {pcie_dma_data_sel/din_fifo_tdata[3]} {pcie_dma_data_sel/din_fifo_tdata[4]} {pcie_dma_data_sel/din_fifo_tdata[5]} {pcie_dma_data_sel/din_fifo_tdata[6]} {pcie_dma_data_sel/din_fifo_tdata[7]} {pcie_dma_data_sel/din_fifo_tdata[8]} {pcie_dma_data_sel/din_fifo_tdata[9]} {pcie_dma_data_sel/din_fifo_tdata[10]} {pcie_dma_data_sel/din_fifo_tdata[11]} {pcie_dma_data_sel/din_fifo_tdata[12]} {pcie_dma_data_sel/din_fifo_tdata[13]} {pcie_dma_data_sel/din_fifo_tdata[14]} {pcie_dma_data_sel/din_fifo_tdata[15]} {pcie_dma_data_sel/din_fifo_tdata[16]} {pcie_dma_data_sel/din_fifo_tdata[17]} {pcie_dma_data_sel/din_fifo_tdata[18]} {pcie_dma_data_sel/din_fifo_tdata[19]} {pcie_dma_data_sel/din_fifo_tdata[20]} {pcie_dma_data_sel/din_fifo_tdata[21]} {pcie_dma_data_sel/din_fifo_tdata[22]} {pcie_dma_data_sel/din_fifo_tdata[23]} {pcie_dma_data_sel/din_fifo_tdata[24]} {pcie_dma_data_sel/din_fifo_tdata[25]} {pcie_dma_data_sel/din_fifo_tdata[26]} {pcie_dma_data_sel/din_fifo_tdata[27]} {pcie_dma_data_sel/din_fifo_tdata[28]} {pcie_dma_data_sel/din_fifo_tdata[29]} {pcie_dma_data_sel/din_fifo_tdata[30]} {pcie_dma_data_sel/din_fifo_tdata[31]} {pcie_dma_data_sel/din_fifo_tdata[32]} {pcie_dma_data_sel/din_fifo_tdata[33]} {pcie_dma_data_sel/din_fifo_tdata[34]} {pcie_dma_data_sel/din_fifo_tdata[35]} {pcie_dma_data_sel/din_fifo_tdata[36]} {pcie_dma_data_sel/din_fifo_tdata[37]} {pcie_dma_data_sel/din_fifo_tdata[38]} {pcie_dma_data_sel/din_fifo_tdata[39]} {pcie_dma_data_sel/din_fifo_tdata[40]} {pcie_dma_data_sel/din_fifo_tdata[41]} {pcie_dma_data_sel/din_fifo_tdata[42]} {pcie_dma_data_sel/din_fifo_tdata[43]} {pcie_dma_data_sel/din_fifo_tdata[44]} {pcie_dma_data_sel/din_fifo_tdata[45]} {pcie_dma_data_sel/din_fifo_tdata[46]} {pcie_dma_data_sel/din_fifo_tdata[47]} {pcie_dma_data_sel/din_fifo_tdata[48]} {pcie_dma_data_sel/din_fifo_tdata[49]} {pcie_dma_data_sel/din_fifo_tdata[50]} {pcie_dma_data_sel/din_fifo_tdata[51]} {pcie_dma_data_sel/din_fifo_tdata[52]} {pcie_dma_data_sel/din_fifo_tdata[53]} {pcie_dma_data_sel/din_fifo_tdata[54]} {pcie_dma_data_sel/din_fifo_tdata[55]} {pcie_dma_data_sel/din_fifo_tdata[56]} {pcie_dma_data_sel/din_fifo_tdata[57]} {pcie_dma_data_sel/din_fifo_tdata[58]} {pcie_dma_data_sel/din_fifo_tdata[59]} {pcie_dma_data_sel/din_fifo_tdata[60]} {pcie_dma_data_sel/din_fifo_tdata[61]} {pcie_dma_data_sel/din_fifo_tdata[62]} {pcie_dma_data_sel/din_fifo_tdata[63]} {pcie_dma_data_sel/din_fifo_tdata[64]} {pcie_dma_data_sel/din_fifo_tdata[65]} {pcie_dma_data_sel/din_fifo_tdata[66]} {pcie_dma_data_sel/din_fifo_tdata[67]} {pcie_dma_data_sel/din_fifo_tdata[68]} {pcie_dma_data_sel/din_fifo_tdata[69]} {pcie_dma_data_sel/din_fifo_tdata[70]} {pcie_dma_data_sel/din_fifo_tdata[71]} {pcie_dma_data_sel/din_fifo_tdata[72]} {pcie_dma_data_sel/din_fifo_tdata[73]} {pcie_dma_data_sel/din_fifo_tdata[74]} {pcie_dma_data_sel/din_fifo_tdata[75]} {pcie_dma_data_sel/din_fifo_tdata[76]} {pcie_dma_data_sel/din_fifo_tdata[77]} {pcie_dma_data_sel/din_fifo_tdata[78]} {pcie_dma_data_sel/din_fifo_tdata[79]} {pcie_dma_data_sel/din_fifo_tdata[80]} {pcie_dma_data_sel/din_fifo_tdata[81]} {pcie_dma_data_sel/din_fifo_tdata[82]} {pcie_dma_data_sel/din_fifo_tdata[83]} {pcie_dma_data_sel/din_fifo_tdata[84]} {pcie_dma_data_sel/din_fifo_tdata[85]} {pcie_dma_data_sel/din_fifo_tdata[86]} {pcie_dma_data_sel/din_fifo_tdata[87]} {pcie_dma_data_sel/din_fifo_tdata[88]} {pcie_dma_data_sel/din_fifo_tdata[89]} {pcie_dma_data_sel/din_fifo_tdata[90]} {pcie_dma_data_sel/din_fifo_tdata[91]} {pcie_dma_data_sel/din_fifo_tdata[92]} {pcie_dma_data_sel/din_fifo_tdata[93]} {pcie_dma_data_sel/din_fifo_tdata[94]} {pcie_dma_data_sel/din_fifo_tdata[95]} {pcie_dma_data_sel/din_fifo_tdata[96]} {pcie_dma_data_sel/din_fifo_tdata[97]} {pcie_dma_data_sel/din_fifo_tdata[98]} {pcie_dma_data_sel/din_fifo_tdata[99]} {pcie_dma_data_sel/din_fifo_tdata[100]} {pcie_dma_data_sel/din_fifo_tdata[101]} {pcie_dma_data_sel/din_fifo_tdata[102]} {pcie_dma_data_sel/din_fifo_tdata[103]} {pcie_dma_data_sel/din_fifo_tdata[104]} {pcie_dma_data_sel/din_fifo_tdata[105]} {pcie_dma_data_sel/din_fifo_tdata[106]} {pcie_dma_data_sel/din_fifo_tdata[107]} {pcie_dma_data_sel/din_fifo_tdata[108]} {pcie_dma_data_sel/din_fifo_tdata[109]} {pcie_dma_data_sel/din_fifo_tdata[110]} {pcie_dma_data_sel/din_fifo_tdata[111]} {pcie_dma_data_sel/din_fifo_tdata[112]} {pcie_dma_data_sel/din_fifo_tdata[113]} {pcie_dma_data_sel/din_fifo_tdata[114]} {pcie_dma_data_sel/din_fifo_tdata[115]} {pcie_dma_data_sel/din_fifo_tdata[116]} {pcie_dma_data_sel/din_fifo_tdata[117]} {pcie_dma_data_sel/din_fifo_tdata[118]} {pcie_dma_data_sel/din_fifo_tdata[119]} {pcie_dma_data_sel/din_fifo_tdata[120]} {pcie_dma_data_sel/din_fifo_tdata[121]} {pcie_dma_data_sel/din_fifo_tdata[122]} {pcie_dma_data_sel/din_fifo_tdata[123]} {pcie_dma_data_sel/din_fifo_tdata[124]} {pcie_dma_data_sel/din_fifo_tdata[125]} {pcie_dma_data_sel/din_fifo_tdata[126]} {pcie_dma_data_sel/din_fifo_tdata[127]}]]
create_debug_port u_ila_1 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_1/probe2]
set_property port_width 16 [get_debug_ports u_ila_1/probe2]
connect_debug_port u_ila_1/probe2 [get_nets [list {pcie_dma_data_sel/din_fifo_tkeep[0]} {pcie_dma_data_sel/din_fifo_tkeep[1]} {pcie_dma_data_sel/din_fifo_tkeep[2]} {pcie_dma_data_sel/din_fifo_tkeep[3]} {pcie_dma_data_sel/din_fifo_tkeep[4]} {pcie_dma_data_sel/din_fifo_tkeep[5]} {pcie_dma_data_sel/din_fifo_tkeep[6]} {pcie_dma_data_sel/din_fifo_tkeep[7]} {pcie_dma_data_sel/din_fifo_tkeep[8]} {pcie_dma_data_sel/din_fifo_tkeep[9]} {pcie_dma_data_sel/din_fifo_tkeep[10]} {pcie_dma_data_sel/din_fifo_tkeep[11]} {pcie_dma_data_sel/din_fifo_tkeep[12]} {pcie_dma_data_sel/din_fifo_tkeep[13]} {pcie_dma_data_sel/din_fifo_tkeep[14]} {pcie_dma_data_sel/din_fifo_tkeep[15]}]]
create_debug_port u_ila_1 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_1/probe3]
set_property port_width 16 [get_debug_ports u_ila_1/probe3]
connect_debug_port u_ila_1/probe3 [get_nets [list {pcie_dma_data_sel/to_pcie_tkeep[0]} {pcie_dma_data_sel/to_pcie_tkeep[1]} {pcie_dma_data_sel/to_pcie_tkeep[2]} {pcie_dma_data_sel/to_pcie_tkeep[3]} {pcie_dma_data_sel/to_pcie_tkeep[4]} {pcie_dma_data_sel/to_pcie_tkeep[5]} {pcie_dma_data_sel/to_pcie_tkeep[6]} {pcie_dma_data_sel/to_pcie_tkeep[7]} {pcie_dma_data_sel/to_pcie_tkeep[8]} {pcie_dma_data_sel/to_pcie_tkeep[9]} {pcie_dma_data_sel/to_pcie_tkeep[10]} {pcie_dma_data_sel/to_pcie_tkeep[11]} {pcie_dma_data_sel/to_pcie_tkeep[12]} {pcie_dma_data_sel/to_pcie_tkeep[13]} {pcie_dma_data_sel/to_pcie_tkeep[14]} {pcie_dma_data_sel/to_pcie_tkeep[15]}]]
create_debug_port u_ila_1 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_1/probe4]
set_property port_width 128 [get_debug_ports u_ila_1/probe4]
connect_debug_port u_ila_1/probe4 [get_nets [list {pcie_dma_data_sel/to_pcie_tdata[0]} {pcie_dma_data_sel/to_pcie_tdata[1]} {pcie_dma_data_sel/to_pcie_tdata[2]} {pcie_dma_data_sel/to_pcie_tdata[3]} {pcie_dma_data_sel/to_pcie_tdata[4]} {pcie_dma_data_sel/to_pcie_tdata[5]} {pcie_dma_data_sel/to_pcie_tdata[6]} {pcie_dma_data_sel/to_pcie_tdata[7]} {pcie_dma_data_sel/to_pcie_tdata[8]} {pcie_dma_data_sel/to_pcie_tdata[9]} {pcie_dma_data_sel/to_pcie_tdata[10]} {pcie_dma_data_sel/to_pcie_tdata[11]} {pcie_dma_data_sel/to_pcie_tdata[12]} {pcie_dma_data_sel/to_pcie_tdata[13]} {pcie_dma_data_sel/to_pcie_tdata[14]} {pcie_dma_data_sel/to_pcie_tdata[15]} {pcie_dma_data_sel/to_pcie_tdata[16]} {pcie_dma_data_sel/to_pcie_tdata[17]} {pcie_dma_data_sel/to_pcie_tdata[18]} {pcie_dma_data_sel/to_pcie_tdata[19]} {pcie_dma_data_sel/to_pcie_tdata[20]} {pcie_dma_data_sel/to_pcie_tdata[21]} {pcie_dma_data_sel/to_pcie_tdata[22]} {pcie_dma_data_sel/to_pcie_tdata[23]} {pcie_dma_data_sel/to_pcie_tdata[24]} {pcie_dma_data_sel/to_pcie_tdata[25]} {pcie_dma_data_sel/to_pcie_tdata[26]} {pcie_dma_data_sel/to_pcie_tdata[27]} {pcie_dma_data_sel/to_pcie_tdata[28]} {pcie_dma_data_sel/to_pcie_tdata[29]} {pcie_dma_data_sel/to_pcie_tdata[30]} {pcie_dma_data_sel/to_pcie_tdata[31]} {pcie_dma_data_sel/to_pcie_tdata[32]} {pcie_dma_data_sel/to_pcie_tdata[33]} {pcie_dma_data_sel/to_pcie_tdata[34]} {pcie_dma_data_sel/to_pcie_tdata[35]} {pcie_dma_data_sel/to_pcie_tdata[36]} {pcie_dma_data_sel/to_pcie_tdata[37]} {pcie_dma_data_sel/to_pcie_tdata[38]} {pcie_dma_data_sel/to_pcie_tdata[39]} {pcie_dma_data_sel/to_pcie_tdata[40]} {pcie_dma_data_sel/to_pcie_tdata[41]} {pcie_dma_data_sel/to_pcie_tdata[42]} {pcie_dma_data_sel/to_pcie_tdata[43]} {pcie_dma_data_sel/to_pcie_tdata[44]} {pcie_dma_data_sel/to_pcie_tdata[45]} {pcie_dma_data_sel/to_pcie_tdata[46]} {pcie_dma_data_sel/to_pcie_tdata[47]} {pcie_dma_data_sel/to_pcie_tdata[48]} {pcie_dma_data_sel/to_pcie_tdata[49]} {pcie_dma_data_sel/to_pcie_tdata[50]} {pcie_dma_data_sel/to_pcie_tdata[51]} {pcie_dma_data_sel/to_pcie_tdata[52]} {pcie_dma_data_sel/to_pcie_tdata[53]} {pcie_dma_data_sel/to_pcie_tdata[54]} {pcie_dma_data_sel/to_pcie_tdata[55]} {pcie_dma_data_sel/to_pcie_tdata[56]} {pcie_dma_data_sel/to_pcie_tdata[57]} {pcie_dma_data_sel/to_pcie_tdata[58]} {pcie_dma_data_sel/to_pcie_tdata[59]} {pcie_dma_data_sel/to_pcie_tdata[60]} {pcie_dma_data_sel/to_pcie_tdata[61]} {pcie_dma_data_sel/to_pcie_tdata[62]} {pcie_dma_data_sel/to_pcie_tdata[63]} {pcie_dma_data_sel/to_pcie_tdata[64]} {pcie_dma_data_sel/to_pcie_tdata[65]} {pcie_dma_data_sel/to_pcie_tdata[66]} {pcie_dma_data_sel/to_pcie_tdata[67]} {pcie_dma_data_sel/to_pcie_tdata[68]} {pcie_dma_data_sel/to_pcie_tdata[69]} {pcie_dma_data_sel/to_pcie_tdata[70]} {pcie_dma_data_sel/to_pcie_tdata[71]} {pcie_dma_data_sel/to_pcie_tdata[72]} {pcie_dma_data_sel/to_pcie_tdata[73]} {pcie_dma_data_sel/to_pcie_tdata[74]} {pcie_dma_data_sel/to_pcie_tdata[75]} {pcie_dma_data_sel/to_pcie_tdata[76]} {pcie_dma_data_sel/to_pcie_tdata[77]} {pcie_dma_data_sel/to_pcie_tdata[78]} {pcie_dma_data_sel/to_pcie_tdata[79]} {pcie_dma_data_sel/to_pcie_tdata[80]} {pcie_dma_data_sel/to_pcie_tdata[81]} {pcie_dma_data_sel/to_pcie_tdata[82]} {pcie_dma_data_sel/to_pcie_tdata[83]} {pcie_dma_data_sel/to_pcie_tdata[84]} {pcie_dma_data_sel/to_pcie_tdata[85]} {pcie_dma_data_sel/to_pcie_tdata[86]} {pcie_dma_data_sel/to_pcie_tdata[87]} {pcie_dma_data_sel/to_pcie_tdata[88]} {pcie_dma_data_sel/to_pcie_tdata[89]} {pcie_dma_data_sel/to_pcie_tdata[90]} {pcie_dma_data_sel/to_pcie_tdata[91]} {pcie_dma_data_sel/to_pcie_tdata[92]} {pcie_dma_data_sel/to_pcie_tdata[93]} {pcie_dma_data_sel/to_pcie_tdata[94]} {pcie_dma_data_sel/to_pcie_tdata[95]} {pcie_dma_data_sel/to_pcie_tdata[96]} {pcie_dma_data_sel/to_pcie_tdata[97]} {pcie_dma_data_sel/to_pcie_tdata[98]} {pcie_dma_data_sel/to_pcie_tdata[99]} {pcie_dma_data_sel/to_pcie_tdata[100]} {pcie_dma_data_sel/to_pcie_tdata[101]} {pcie_dma_data_sel/to_pcie_tdata[102]} {pcie_dma_data_sel/to_pcie_tdata[103]} {pcie_dma_data_sel/to_pcie_tdata[104]} {pcie_dma_data_sel/to_pcie_tdata[105]} {pcie_dma_data_sel/to_pcie_tdata[106]} {pcie_dma_data_sel/to_pcie_tdata[107]} {pcie_dma_data_sel/to_pcie_tdata[108]} {pcie_dma_data_sel/to_pcie_tdata[109]} {pcie_dma_data_sel/to_pcie_tdata[110]} {pcie_dma_data_sel/to_pcie_tdata[111]} {pcie_dma_data_sel/to_pcie_tdata[112]} {pcie_dma_data_sel/to_pcie_tdata[113]} {pcie_dma_data_sel/to_pcie_tdata[114]} {pcie_dma_data_sel/to_pcie_tdata[115]} {pcie_dma_data_sel/to_pcie_tdata[116]} {pcie_dma_data_sel/to_pcie_tdata[117]} {pcie_dma_data_sel/to_pcie_tdata[118]} {pcie_dma_data_sel/to_pcie_tdata[119]} {pcie_dma_data_sel/to_pcie_tdata[120]} {pcie_dma_data_sel/to_pcie_tdata[121]} {pcie_dma_data_sel/to_pcie_tdata[122]} {pcie_dma_data_sel/to_pcie_tdata[123]} {pcie_dma_data_sel/to_pcie_tdata[124]} {pcie_dma_data_sel/to_pcie_tdata[125]} {pcie_dma_data_sel/to_pcie_tdata[126]} {pcie_dma_data_sel/to_pcie_tdata[127]}]]
create_debug_port u_ila_1 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_1/probe5]
set_property port_width 1 [get_debug_ports u_ila_1/probe5]
connect_debug_port u_ila_1/probe5 [get_nets [list pcie_dma_data_sel/din_fifo_tlast]]
create_debug_port u_ila_1 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_1/probe6]
set_property port_width 1 [get_debug_ports u_ila_1/probe6]
connect_debug_port u_ila_1/probe6 [get_nets [list pcie_dma_data_sel/din_fifo_tready]]
create_debug_port u_ila_1 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_1/probe7]
set_property port_width 1 [get_debug_ports u_ila_1/probe7]
connect_debug_port u_ila_1/probe7 [get_nets [list pcie_dma_data_sel/din_fifo_tvalid]]
create_debug_port u_ila_1 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_1/probe8]
set_property port_width 1 [get_debug_ports u_ila_1/probe8]
connect_debug_port u_ila_1/probe8 [get_nets [list pcie_dma_data_sel/test_cnt_clr]]
create_debug_port u_ila_1 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_1/probe9]
set_property port_width 1 [get_debug_ports u_ila_1/probe9]
connect_debug_port u_ila_1/probe9 [get_nets [list pcie_dma_data_sel/to_pcie_tlast]]
create_debug_port u_ila_1 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_1/probe10]
set_property port_width 1 [get_debug_ports u_ila_1/probe10]
connect_debug_port u_ila_1/probe10 [get_nets [list pcie_dma_data_sel/to_pcie_tready]]
create_debug_port u_ila_1 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_1/probe11]
set_property port_width 1 [get_debug_ports u_ila_1/probe11]
connect_debug_port u_ila_1/probe11 [get_nets [list pcie_dma_data_sel/to_pcie_tvalid]]
create_debug_core u_ila_2 ila
set_property ALL_PROBE_SAME_MU true [get_debug_cores u_ila_2]
set_property ALL_PROBE_SAME_MU_CNT 1 [get_debug_cores u_ila_2]
set_property C_ADV_TRIGGER false [get_debug_cores u_ila_2]
set_property C_DATA_DEPTH 2048 [get_debug_cores u_ila_2]
set_property C_EN_STRG_QUAL false [get_debug_cores u_ila_2]
set_property C_INPUT_PIPE_STAGES 0 [get_debug_cores u_ila_2]
set_property C_TRIGIN_EN false [get_debug_cores u_ila_2]
set_property C_TRIGOUT_EN false [get_debug_cores u_ila_2]
set_property port_width 1 [get_debug_ports u_ila_2/clk]
connect_debug_port u_ila_2/clk [get_nets [list clk_sig_mux/inst/clk_out2]]
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_2/probe0]
set_property port_width 6 [get_debug_ports u_ila_2/probe0]
connect_debug_port u_ila_2/probe0 [get_nets [list {con4_intf_charlie/spad_detect_1/trig_flag[0]} {con4_intf_charlie/spad_detect_1/trig_flag[1]} {con4_intf_charlie/spad_detect_1/trig_flag[2]} {con4_intf_charlie/spad_detect_1/trig_flag[3]} {con4_intf_charlie/spad_detect_1/trig_flag[4]} {con4_intf_charlie/spad_detect_1/trig_flag[5]}]]
create_debug_port u_ila_2 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_2/probe1]
set_property port_width 6 [get_debug_ports u_ila_2/probe1]
connect_debug_port u_ila_2/probe1 [get_nets [list {con4_intf_charlie/spad_detect_2/trig_flag[0]} {con4_intf_charlie/spad_detect_2/trig_flag[1]} {con4_intf_charlie/spad_detect_2/trig_flag[2]} {con4_intf_charlie/spad_detect_2/trig_flag[3]} {con4_intf_charlie/spad_detect_2/trig_flag[4]} {con4_intf_charlie/spad_detect_2/trig_flag[5]}]]
create_debug_port u_ila_2 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_2/probe2]
set_property port_width 6 [get_debug_ports u_ila_2/probe2]
connect_debug_port u_ila_2/probe2 [get_nets [list {con4_intf_charlie/spad_detect_1/trig_byte[0]} {con4_intf_charlie/spad_detect_1/trig_byte[1]} {con4_intf_charlie/spad_detect_1/trig_byte[2]} {con4_intf_charlie/spad_detect_1/trig_byte[3]} {con4_intf_charlie/spad_detect_1/trig_byte[4]} {con4_intf_charlie/spad_detect_1/trig_byte[5]}]]
create_debug_port u_ila_2 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_2/probe3]
set_property port_width 6 [get_debug_ports u_ila_2/probe3]
connect_debug_port u_ila_2/probe3 [get_nets [list {con4_intf_charlie/spad_detect_2/trig_byte[0]} {con4_intf_charlie/spad_detect_2/trig_byte[1]} {con4_intf_charlie/spad_detect_2/trig_byte[2]} {con4_intf_charlie/spad_detect_2/trig_byte[3]} {con4_intf_charlie/spad_detect_2/trig_byte[4]} {con4_intf_charlie/spad_detect_2/trig_byte[5]}]]
create_debug_port u_ila_2 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_2/probe4]
set_property port_width 6 [get_debug_ports u_ila_2/probe4]
connect_debug_port u_ila_2/probe4 [get_nets [list {con3_intf_charlie/spad_detect_2/trig_byte[0]} {con3_intf_charlie/spad_detect_2/trig_byte[1]} {con3_intf_charlie/spad_detect_2/trig_byte[2]} {con3_intf_charlie/spad_detect_2/trig_byte[3]} {con3_intf_charlie/spad_detect_2/trig_byte[4]} {con3_intf_charlie/spad_detect_2/trig_byte[5]}]]
create_debug_port u_ila_2 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_2/probe5]
set_property port_width 6 [get_debug_ports u_ila_2/probe5]
connect_debug_port u_ila_2/probe5 [get_nets [list {con3_intf_charlie/spad_detect_1/trig_flag[0]} {con3_intf_charlie/spad_detect_1/trig_flag[1]} {con3_intf_charlie/spad_detect_1/trig_flag[2]} {con3_intf_charlie/spad_detect_1/trig_flag[3]} {con3_intf_charlie/spad_detect_1/trig_flag[4]} {con3_intf_charlie/spad_detect_1/trig_flag[5]}]]
create_debug_port u_ila_2 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_2/probe6]
set_property port_width 6 [get_debug_ports u_ila_2/probe6]
connect_debug_port u_ila_2/probe6 [get_nets [list {con3_intf_charlie/spad_detect_2/trig_flag[0]} {con3_intf_charlie/spad_detect_2/trig_flag[1]} {con3_intf_charlie/spad_detect_2/trig_flag[2]} {con3_intf_charlie/spad_detect_2/trig_flag[3]} {con3_intf_charlie/spad_detect_2/trig_flag[4]} {con3_intf_charlie/spad_detect_2/trig_flag[5]}]]
create_debug_port u_ila_2 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_2/probe7]
set_property port_width 6 [get_debug_ports u_ila_2/probe7]
connect_debug_port u_ila_2/probe7 [get_nets [list {con3_intf_charlie/spad_detect_1/trig_byte[0]} {con3_intf_charlie/spad_detect_1/trig_byte[1]} {con3_intf_charlie/spad_detect_1/trig_byte[2]} {con3_intf_charlie/spad_detect_1/trig_byte[3]} {con3_intf_charlie/spad_detect_1/trig_byte[4]} {con3_intf_charlie/spad_detect_1/trig_byte[5]}]]
create_debug_port u_ila_2 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_2/probe8]
set_property port_width 6 [get_debug_ports u_ila_2/probe8]
connect_debug_port u_ila_2/probe8 [get_nets [list {con2_intf_charlie/spad_detect_1/trig_byte[0]} {con2_intf_charlie/spad_detect_1/trig_byte[1]} {con2_intf_charlie/spad_detect_1/trig_byte[2]} {con2_intf_charlie/spad_detect_1/trig_byte[3]} {con2_intf_charlie/spad_detect_1/trig_byte[4]} {con2_intf_charlie/spad_detect_1/trig_byte[5]}]]
create_debug_port u_ila_2 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_2/probe9]
set_property port_width 6 [get_debug_ports u_ila_2/probe9]
connect_debug_port u_ila_2/probe9 [get_nets [list {con2_intf_charlie/spad_detect_2/trig_byte[0]} {con2_intf_charlie/spad_detect_2/trig_byte[1]} {con2_intf_charlie/spad_detect_2/trig_byte[2]} {con2_intf_charlie/spad_detect_2/trig_byte[3]} {con2_intf_charlie/spad_detect_2/trig_byte[4]} {con2_intf_charlie/spad_detect_2/trig_byte[5]}]]
create_debug_port u_ila_2 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_2/probe10]
set_property port_width 6 [get_debug_ports u_ila_2/probe10]
connect_debug_port u_ila_2/probe10 [get_nets [list {con2_intf_charlie/spad_detect_2/trig_flag[0]} {con2_intf_charlie/spad_detect_2/trig_flag[1]} {con2_intf_charlie/spad_detect_2/trig_flag[2]} {con2_intf_charlie/spad_detect_2/trig_flag[3]} {con2_intf_charlie/spad_detect_2/trig_flag[4]} {con2_intf_charlie/spad_detect_2/trig_flag[5]}]]
create_debug_port u_ila_2 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_2/probe11]
set_property port_width 6 [get_debug_ports u_ila_2/probe11]
connect_debug_port u_ila_2/probe11 [get_nets [list {con2_intf_charlie/spad_detect_1/trig_flag[0]} {con2_intf_charlie/spad_detect_1/trig_flag[1]} {con2_intf_charlie/spad_detect_1/trig_flag[2]} {con2_intf_charlie/spad_detect_1/trig_flag[3]} {con2_intf_charlie/spad_detect_1/trig_flag[4]} {con2_intf_charlie/spad_detect_1/trig_flag[5]}]]
create_debug_port u_ila_2 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_2/probe12]
set_property port_width 6 [get_debug_ports u_ila_2/probe12]
connect_debug_port u_ila_2/probe12 [get_nets [list {con1_intf_charlie/spad_detect_2/trig_byte[0]} {con1_intf_charlie/spad_detect_2/trig_byte[1]} {con1_intf_charlie/spad_detect_2/trig_byte[2]} {con1_intf_charlie/spad_detect_2/trig_byte[3]} {con1_intf_charlie/spad_detect_2/trig_byte[4]} {con1_intf_charlie/spad_detect_2/trig_byte[5]}]]
create_debug_port u_ila_2 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_2/probe13]
set_property port_width 6 [get_debug_ports u_ila_2/probe13]
connect_debug_port u_ila_2/probe13 [get_nets [list {con1_intf_charlie/spad_detect_1/trig_byte[0]} {con1_intf_charlie/spad_detect_1/trig_byte[1]} {con1_intf_charlie/spad_detect_1/trig_byte[2]} {con1_intf_charlie/spad_detect_1/trig_byte[3]} {con1_intf_charlie/spad_detect_1/trig_byte[4]} {con1_intf_charlie/spad_detect_1/trig_byte[5]}]]
create_debug_port u_ila_2 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_2/probe14]
set_property port_width 6 [get_debug_ports u_ila_2/probe14]
connect_debug_port u_ila_2/probe14 [get_nets [list {con1_intf_charlie/spad_detect_1/trig_flag[0]} {con1_intf_charlie/spad_detect_1/trig_flag[1]} {con1_intf_charlie/spad_detect_1/trig_flag[2]} {con1_intf_charlie/spad_detect_1/trig_flag[3]} {con1_intf_charlie/spad_detect_1/trig_flag[4]} {con1_intf_charlie/spad_detect_1/trig_flag[5]}]]
create_debug_port u_ila_2 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_2/probe15]
set_property port_width 6 [get_debug_ports u_ila_2/probe15]
connect_debug_port u_ila_2/probe15 [get_nets [list {con1_intf_charlie/spad_detect_2/trig_flag[0]} {con1_intf_charlie/spad_detect_2/trig_flag[1]} {con1_intf_charlie/spad_detect_2/trig_flag[2]} {con1_intf_charlie/spad_detect_2/trig_flag[3]} {con1_intf_charlie/spad_detect_2/trig_flag[4]} {con1_intf_charlie/spad_detect_2/trig_flag[5]}]]
create_debug_port u_ila_2 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_2/probe16]
set_property port_width 1 [get_debug_ports u_ila_2/probe16]
connect_debug_port u_ila_2/probe16 [get_nets [list con1_intf_charlie/spad_detect_1/din_fifo]]
create_debug_port u_ila_2 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_2/probe17]
set_property port_width 1 [get_debug_ports u_ila_2/probe17]
connect_debug_port u_ila_2/probe17 [get_nets [list con3_intf_charlie/spad_detect_2/din_fifo]]
create_debug_port u_ila_2 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_2/probe18]
set_property port_width 1 [get_debug_ports u_ila_2/probe18]
connect_debug_port u_ila_2/probe18 [get_nets [list con4_intf_charlie/spad_detect_2/din_fifo]]
create_debug_port u_ila_2 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_2/probe19]
set_property port_width 1 [get_debug_ports u_ila_2/probe19]
connect_debug_port u_ila_2/probe19 [get_nets [list con3_intf_charlie/spad_detect_1/din_fifo]]
create_debug_port u_ila_2 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_2/probe20]
set_property port_width 1 [get_debug_ports u_ila_2/probe20]
connect_debug_port u_ila_2/probe20 [get_nets [list con2_intf_charlie/spad_detect_2/din_fifo]]
create_debug_port u_ila_2 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_2/probe21]
set_property port_width 1 [get_debug_ports u_ila_2/probe21]
connect_debug_port u_ila_2/probe21 [get_nets [list con2_intf_charlie/spad_detect_1/din_fifo]]
create_debug_port u_ila_2 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_2/probe22]
set_property port_width 1 [get_debug_ports u_ila_2/probe22]
connect_debug_port u_ila_2/probe22 [get_nets [list con4_intf_charlie/spad_detect_1/din_fifo]]
create_debug_port u_ila_2 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_2/probe23]
set_property port_width 1 [get_debug_ports u_ila_2/probe23]
connect_debug_port u_ila_2/probe23 [get_nets [list con1_intf_charlie/spad_detect_2/din_fifo]]
set_property C_CLK_INPUT_FREQ_HZ 300000000 [get_debug_cores dbg_hub]
set_property C_ENABLE_CLK_DIVIDER false [get_debug_cores dbg_hub]
set_property C_USER_SCAN_CHAIN 1 [get_debug_cores dbg_hub]
connect_debug_port dbg_hub/clk [get_nets clk_200]
