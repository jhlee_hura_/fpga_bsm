
# -------------------------------------------------------------------------------
# Company: HURA
# Engineer: LEE Seong Yun (sylee@hura.co.kr)
#
# Create Date: 2019/01/02 11:40:00
# Design Name: QKD
# Module Name: QKD_alice.xdc
# Project Name: QKD (Alice or Bob)
# Target Devices: Kintex7 160T
# Tool Versions: Vivado 2018.1
# Description:
#
# Dependencies:
#
# Revision:
# Revision build 1
# Additional Comments:
#
# -------------------------------------------------------------------------------

set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets con6_intf_alice/PD12_ibuf_s]

set_false_path -through [get_cells -hierarchical *sout*]
set_false_path -to [get_ports F_CLK_3_3]
#CON1


set_false_path -through [get_nets -hierarchical *_i_dac_*]
set_false_path -through [get_nets -hierarchical *width*]
set_false_path -through [get_nets -hierarchical *sig_pola*]
set_false_path -through [get_nets -hierarchical *offset*]
set_false_path -through [get_nets -hierarchical *test_data*]
set_false_path -through [get_nets -hierarchical *test_mode*]
set_false_path -through [get_nets -hierarchical *delay_tap_in*]
set_false_path -through [get_nets -hierarchical *delay_tap_out*]
set_false_path -through [get_nets -hierarchical *hmc920_en*]
set_false_path -through [get_nets -hierarchical *tec_en*]
set_false_path -through [get_nets -hierarchical *tec_shdn_b*]
set_false_path -through [get_nets -hierarchical *tec_clr*]

set_false_path -through [get_nets -hierarchical *pm_pi_en*]
set_false_path -through [get_nets -hierarchical *fpc_en*]
set_false_path -through [get_nets -hierarchical *fpc_dac_clr*]
set_false_path -through [get_nets -hierarchical *fpc10_dac_clr*]
set_false_path -through [get_nets -hierarchical *width*]


# -------------------------------------------------------------------------------
#CON2


# -------------------------------------------------------------------------------
#CON3

# -------------------------------------------------------------------------------
#CON4


# -------------------------------------------------------------------------------
#CON5



# -------------------------------------------------------------------------------
#CON6


# -------------------------------------------------------------------------------
#CON7

# -------------------------------------------------------------------------------
#CON8


# -------------------------------------------------------------------------------
#CON9


# -------------------------------------------------------------------------------
#CON10


# -------------------------------------------------------------------------------

#Groups a set of IDELAY and IODELAY constraints with an IDELAYCTRL to enable automatic replication and placement of IDELAYCTRL in a design

set_property IODELAY_GROUP IO_DLY1 [get_cells {con1_intf_alice/laser_trig_gen_1200/selectio_6to1_dc/inst/pins[0].odelaye2_bus}]
set_property IODELAY_GROUP IO_DLY1 [get_cells {con2_intf_alice/trigmask_10M_gen/selectio_6to1_dc/inst/pins[0].odelaye2_bus}]
set_property IODELAY_GROUP IO_DLY1 [get_cells {con2_intf_alice/spad_detect_1/selectio_1to6/inst/pins[0].idelaye2_bus}]
set_property IODELAY_GROUP IO_DLY1 [get_cells {con2_intf_alice/spad_detect_2/selectio_1to6/inst/pins[0].idelaye2_bus}]

set_property IODELAY_GROUP IO_DLY1 [get_cells {con9_intf_alice/data_serialize/selectio_6to1_dc/inst/pins[0].odelaye2_bus}]
set_property IODELAY_GROUP IO_DLY1 [get_cells {con10_intf_alice/data_serialize/selectio_6to1_dc/inst/pins[0].odelaye2_bus}]


set_property IODELAY_GROUP IO_DLY1 [get_cells IDELAYCTRL_SELECTIO_8TO1]




