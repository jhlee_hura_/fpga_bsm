`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: HURA
// Engineer: LEE Seong Yun (sylee@hura.co.kr)
//
// Create Date: 2019/01/04 11:05:02
// Design Name: QKD Alice
// Module Name: con4_intf_alice
// Project Name: QKD
// Target Devices: Kintex 7 160T
// Tool Versions: Vivado 2018.1
// Description:
//
// Dependencies:
//
// Revision:
// 		build. 1 (2019/01/04)
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////
module con4_intf_alice (

	// to Con 4
    output   wire   CON4B1_p0,
    output   wire   CON4B1_n0,
    input    wire   CON4B1_p1,
    input    wire   CON4B1_n1,
    output   wire   CON4B1_p2,
    output   wire   CON4B1_n2,  
    output   wire   CON4B2_p,
    output   wire   CON4B2_n,     
	inout	 wire  [21:0]	CON4B3,
	
	
	input	wire	sys_clk,
	input	wire	sig_clk,
	input   wire   	clk_200,     
	input	wire	rst,

	// ---- IM data ---
	input	wire 		test_mode,
	input	wire [63:0]	din_test,	// 2bits per data

	input	wire		shape_load,			// set false path
	input	wire [3:0] 	pulse_width,		// set false path
	input	wire [8:0]	pulse_offset,		// set false path

	input	wire [11:0] i_dac_v0,
	input	wire [11:0] i_dac_v1_p,
	input	wire [11:0] i_dac_v1_n,	
	input	wire [11:0] i_dac_v2_p,
	input	wire [11:0] i_dac_v2_n,	
	input	wire [11:0] i_dac_v3_p,
	input	wire [11:0] i_dac_v3_n,
	
	// from random
	input	wire [63:0] din_im,
	input	wire		div_im,
	output	wire		dir_im,

(* mark_debug = "true" *)	output	wire [1:0]	do_im_bit,
	output	wire		dov_im_bit,

	// ---- PM Half DAC (AD5761) ---
	input	wire [23:0]	im_dac_din,
	input	wire		im_dac_din_valid,
	output	wire		im_dac_din_ready
);

OBUFDS #(
	.IOSTANDARD("DEFAULT"), // Specify the output I/O standard
	.SLEW("FAST")			// Specify the output slew rate
) OBUFDS_inst1 (
	.O(CON4B2_p),		// Diff_p output (connect directly to top-level port)
	.OB(CON4B2_n),		// Diff_n output (connect directly to top-level port)
	.I(1'b1)				// Buffer input
);

OBUFDS #(
	.IOSTANDARD("DEFAULT"), // Specify the output I/O standard
	.SLEW("FAST")			// Specify the output slew rate
) OBUFDS_inst2 (
	.O(CON4B1_p2),		// Diff_p output (connect directly to top-level port)
	.OB(CON4B1_n2),		// Diff_n output (connect directly to top-level port)
	.I(1'b0)				// Buffer input
);
OBUFDS #(
	.IOSTANDARD("DEFAULT"), // Specify the output I/O standard
	.SLEW("FAST")			// Specify the output slew rate
) OBUFDS_inst3 (
	.O(CON4B1_p0),		// Diff_p output (connect directly to top-level port)
	.OB(CON4B1_n0),		// Diff_n output (connect directly to top-level port)
	.I(1'b0)				// Buffer input
);
wire [11:0] din_dac;
wire		dac_clk;

im_gen_c im_gen_c (
    .clk			(sig_clk),			// I
	.clk_200        (clk_200),     // I
	.rst			(rst),			// I
	
	.test_mode		(test_mode),	// I
	.din_test		(din_test),		// I [63:0]

	.din			(din_im),			// I [63:0]	
	.din_valid		(div_im),	// I			
	.din_ready		(dir_im),	// O
	
	.do_bit			(do_im_bit),		// O [1:0]
	.dov_bit		(dov_im_bit),		// O
	
	.dout_dac		(din_dac),			// O [11:0]
	.dout_clk		(dac_clk),		// O
	
	.shape_load		(shape_load),	// I
	.pulse_width	(pulse_width),	// I [3:0]
	.pulse_offset	(pulse_offset),	// I [8:0]

	.i_dac_v0				(i_dac_v0),		// I [11:0]
	.i_dac_v1_p				(i_dac_v1_p),		// I [11:0]
	.i_dac_v2_p				(i_dac_v2_p),		// I [11:0]
	.i_dac_v3_p				(i_dac_v3_p),		// I [11:0]
    .i_dac_v1_n             (i_dac_v1_n),        // I [11:0]
    .i_dac_v2_n             (i_dac_v2_n),        // I [11:0]
    .i_dac_v3_n             (i_dac_v3_n)        // I [11:0]
);


// ---------------------------------------------------------------------------------
// Assign CON4B3
// ---------------------------------------------------------------------------------
wire	im_dac_sck;
wire	im_dac_scs;
wire	im_dac_sdo;
con4b3_iobuf_alice con4b3_iobuf_alice (
	.CON(CON4B3),		// IO [21:0]

	// DAC AD9742 for IM
	.dac_in(din_dac),			// I [11:0]
	.dac_clk(dac_clk),			// I

	// DAC AD5761 for IM BIAS
	.im_dac_sck(im_dac_sck),	// I
	.im_dac_scs(im_dac_scs),	// I
	.im_dac_sdo(im_dac_sdo)		// I
);

// ---------------------------------------------------------------------------------
//  IM (AD5761) SPI Port
// ---------------------------------------------------------------------------------
 spi_intf #(
	.SCLK_PERIOD(16),
	.CLK_START_END(8),
	.HIGH_CLK(8),
	.CLK_OFFSET(2),
	.CONT_WIDTH(0),
	.DATA_WIDTH(24),
	.RW_FLAG_BIT(0),
	.SCS_N_MODE(0),
	.SCLK_IDLE_MODE(1)
) spi_intf_ad5761(
	.clk(sys_clk),							// I
	.sclr(rst),						// I

	.sclk		(im_dac_sck),		// O
	.scs_n		(im_dac_scs),		// O
	.sdo		(im_dac_sdo),		// O
	.sdi		(1'b0),				// I
	.iobuf_T	(),						// O //not used for 4line SPI

	.din		(im_dac_din),		// I [WORD_WIDTH-1:0]
	.din_valid	(im_dac_din_valid),	// I
	.din_ready	(im_dac_din_ready),	// O
	.dout		(),					// O [WORD_WIDTH-1:0]
	.dout_valid	()						// O
);


endmodule