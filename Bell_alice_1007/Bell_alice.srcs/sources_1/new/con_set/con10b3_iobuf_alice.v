`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: HURA
// Engineer: LEE Seong Yun (sylee@hura.co.kr)
//
// Create Date: 2019/01/04 11:05:02
// Design Name: QKD Alice
// Module Name: con10b3_iobuf_alice
// Project Name: QKD
// Target Devices: Kintex 7 160T
// Tool Versions: Vivado 2018.1
// Description:
//
// Dependencies:
//
// Revision:
// 		build. 1 (2019/01/04)
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////
module con10b3_iobuf_alice (

	inout 	wire [15:0]	CON,
	
	// DAC AD5761 for PM_HALF
	input wire 			pm_dac_sck,
	input wire 			pm_dac_scs,
	input wire 			pm_dac_sdo,
	output wire 		pm_dac_sdi,
	// Enable
	input wire			hmc920_en,
	input wire			pm_pi_en,
    // [DP] AD5252  for PM_HALF
    input wire            dp_sck,            // I
    output wire           dp_sdi,            // I
    input wire            dp_sdo,            // O
    inout wire            dp_T            // IO
);

// --------------------------------------------------------------------
// PM HALF(AD5761)
// --------------------------------------------------------------------
IOBUF CON_0 (
	.O(),					// Buffer output
	.IO(CON[0]),			// Buffer inout port (connect directly to top-level port)
	.I(pm_dac_scs),			// Buffer input
	.T(1'b0)				// 3-state enable input, high=input, low=output
);

IOBUF CON_1 (
	.O(),					// Buffer output
	.IO(CON[1]),			// Buffer inout port (connect directly to top-level port)
	.I(pm_dac_sck),			// Buffer input
	.T(1'b0)				// 3-state enable input, high=input, low=output
);

IOBUF CON_2 (
	.O(),					// Buffer output
	.IO(CON[2]),			// Buffer inout port (connect directly to top-level port)
	.I(pm_dac_sdo),			// Buffer input
	.T(1'b0)				// 3-state enable input, high=input, low=output
);

IOBUF CON_3 (
	.O(pm_dac_sdi),			// Buffer output
	.IO(CON[3]),			// Buffer inout port (connect directly to top-level port)
	.I(1'bz),				// Buffer input
	.T(1'b1)				// 3-state enable input, high=input, low=output
);

IOBUF CON_4 (
	.O(),				// Buffer output
	.IO(CON[4]),		// Buffer inout port (connect directly to top-level port)
	.I(hmc920_en),		// Buffer input
	.T(1'b0)			// 3-state enable input, high=input, low=output
);

IOBUF CON_5 (
	.O(),				// Buffer output
	.IO(CON[5]),		// Buffer inout port (connect directly to top-level port)
	.I(pm_pi_en),			// Buffer input
	.T(1'b0)			// 3-state enable input, high=input, low=output
);

IOBUF CON_6 (
	.O(dp_sdi),				// Buffer output
	.IO(CON[6]),	// Buffer inout port (connect directly to top-level port)
	.I(dp_sdo),		// Buffer input
	.T(dp_T)			// 3-state enable input, high=input, low=output
);

IOBUF CON_7 (
	.O(),				// Buffer output
	.IO(CON[7]),	// Buffer inout port (connect directly to top-level port)
	.I(dp_sck),		// Buffer input
	.T(1'b0)			// 3-state enable input, high=input, low=output
);

// --------------------------------------------------------------------
IOBUF CON_8 (
	.O(),				// Buffer output
	.IO(CON[8]),	// Buffer inout port (connect directly to top-level port)
	.I(1'b0),		// Buffer input
	.T(1'b0)			// 3-state enable input, high=input, low=output
);

IOBUF CON_9 (
	.O(),				// Buffer output
	.IO(CON[9]),	// Buffer inout port (connect directly to top-level port)
	.I(1'b0),		// Buffer input
	.T(1'b0)			// 3-state enable input, high=input, low=output
);

IOBUF CON_10 (
	.O(),				// Buffer output
	.IO(CON[10]),	// Buffer inout port (connect directly to top-level port)
	.I(1'b0),		// Buffer input
	.T(1'b0)			// 3-state enable input, high=input, low=output
);

IOBUF CON_11 (
	.O(),				// Buffer output
	.IO(CON[11]),	// Buffer inout port (connect directly to top-level port)
	.I(1'b0),		// Buffer input
	.T(1'b0)			// 3-state enable input, high=input, low=output
);

IOBUF CON_12 (
	.O(),				// Buffer output
	.IO(CON[12]),	// Buffer inout port (connect directly to top-level port)
	.I(1'b0),		// Buffer input
	.T(1'b0)			// 3-state enable input, high=input, low=output
);

IOBUF CON_13 (
	.O(),				// Buffer output
	.IO(CON[13]),	// Buffer inout port (connect directly to top-level port)
	.I(1'b0),		// Buffer input
	.T(1'b0)			// 3-state enable input, high=input, low=output
);

IOBUF CON_14 (
	.O(),				// Buffer output
	.IO(CON[14]),	// Buffer inout port (connect directly to top-level port)
	.I(1'b0),		// Buffer input
	.T(1'b0)			// 3-state enable input, high=input, low=output
);

IOBUF CON_15 (
	.O(),				// Buffer output
	.IO(CON[15]),	// Buffer inout port (connect directly to top-level port)
	.I(1'b0),		// Buffer input
	.T(1'b0)			// 3-state enable input, high=input, low=output
);


endmodule