`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: HURA
// Engineer: LEE Seong Yun (sylee@hura.co.kr)
//
// Create Date: 2019/01/04 11:05:02
// Design Name: QKD Alice
// Module Name: con8_intf_alice
// Project Name: QKD
// Target Devices: Kintex 7 160T
// Tool Versions: Vivado 2018.1
// Description:
//
// Dependencies:
//
// Revision:
// 		build. 1 (2019/01/04)
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////
module con8_intf_alice (
	input	wire	sys_clk,
	input	wire	rst,
	
    output  wire   CON8B1_p0,
    output  wire   CON8B1_n0,
    output  wire   CON8B1_p2,
    output  wire   CON8B1_n2,
 	inout	wire [15:0]	CON8B3,    

	// ---- Enable  -----
	input	wire		fpc_en,
	// ---- FPC (AD5044) ---
	input	wire		fpc_dac_clr,
	input	wire [31:0]	fpc_dac_din,
	input	wire		fpc_dac_din_valid,
	output	wire		fpc_dac_din_ready,

	// ---- FPC10 (AD5044) ---
	input	wire		fpc10_dac_clr,
	input	wire [31:0]	fpc10_dac_din,
	input	wire		fpc10_dac_din_valid,
	output	wire		fpc10_dac_din_ready
);
OBUFDS #(
	.IOSTANDARD("DEFAULT"), // Specify the output I/O standard
	.SLEW("FAST")			// Specify the output slew rate
) OBUFDS_inst0 (
	.O(CON8B1_p0),		// Diff_p output (connect directly to top-level port)
	.OB(CON8B1_n0),		// Diff_n output (connect directly to top-level port)
	.I(1'b1)				// Buffer input
);
OBUFDS #(
	.IOSTANDARD("DEFAULT"), // Specify the output I/O standard
	.SLEW("FAST")			// Specify the output slew rate
) OBUFDS_inst1 (
	.O(CON8B1_p2),		// Diff_p output (connect directly to top-level port)
	.OB(CON8B1_n2),		// Diff_n output (connect directly to top-level port)
	.I(1'b1)				// Buffer input
);
// ---------------------------------------------------------------------------------
// Assign CON5B3
// ---------------------------------------------------------------------------------
wire	fpc_dac_sck;
wire	fpc_dac_scs;
wire	fpc_dac_sdo;
wire	fpc10_dac_sck;
wire	fpc10_dac_scs;
wire	fpc10_dac_sdo;

con8b3_iobuf_alice con8b3_iobuf_alice (
	.CON(CON8B3),		// IO [15:0]

	// FPC (AD5044)
	.fpc_dac_sck(fpc_dac_sck),		// I
	.fpc_dac_scs(fpc_dac_scs),		// I
	.fpc_dac_sdo(fpc_dac_sdo),		// I
	// FPC10 (AD5044)
	.fpc10_dac_sck(fpc10_dac_sck),		// I
	.fpc10_dac_scs(fpc10_dac_scs),		// I
	.fpc10_dac_sdo(fpc10_dac_sdo),		// I
	// Enable
	.fpc_en(fpc_en),					// I
	.fpc_dac_clr(fpc_dac_clr),			// I
	.fpc10_dac_clr(fpc10_dac_clr)		// I
);


// ---------------------------------------------------------------------------------
//  FPC AD5044 SPI Port
// ---------------------------------------------------------------------------------
shiftreg_intf #(
	.SCLK_PERIOD(16),
	.CLK_START_END(8),
	.HIGH_CLK(8),
	.CLK_OFFSET(2),
	.DATA_WIDTH(32),
	.SCS_N_MODE(0),
	.SCLK_IDLE_MODE(1)
) fpc_dac(
	.clk	(sys_clk),					// I
	.sclr	(rst),						// I

	.din		(fpc_dac_din),			// I [DATA_WIDTH-1:0]
	.din_valid	(fpc_dac_din_valid),	// I
	.din_ready	(fpc_dac_din_ready),	// O

	.sclk		(fpc_dac_sck),			// O
	.scs_n		(fpc_dac_scs),			// O
	.latch_en	(),						// O
	.sdo		(fpc_dac_sdo)			// O
);

// ---------------------------------------------------------------------------------
//  FPC10 AD5044 SPI Port
// ---------------------------------------------------------------------------------
shiftreg_intf #(
	.SCLK_PERIOD(16),
	.CLK_START_END(8),
	.HIGH_CLK(8),
	.CLK_OFFSET(2),
	.DATA_WIDTH(32),
	.SCS_N_MODE(0),
	.SCLK_IDLE_MODE(1)
) fpc10_dac(
	.clk	(sys_clk),					// I
	.sclr	(rst),						// I

	.din		(fpc10_dac_din),		// I [DATA_WIDTH-1:0]
	.din_valid	(fpc10_dac_din_valid),	// I
	.din_ready	(fpc10_dac_din_ready),	// O

	.sclk		(fpc10_dac_sck),		// O
	.scs_n		(fpc10_dac_scs),		// O
	.latch_en	(),						// O
	.sdo		(fpc10_dac_sdo)			// O
);


endmodule