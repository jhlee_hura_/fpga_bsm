`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: HURA
// Engineer: LEE Seong Yun (sylee@hura.co.kr)
//
// Create Date: 2019/01/04 11:05:02
// Design Name: 
// Module Name:
// Project Name: 
// Target Devices: Kintex 7 160T
// Tool Versions: Vivado 2018.1
// Description:
//
// Dependencies:
//
// Revision:
// 		build. 1 (2019/01/04)
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////
module con9_intf_alice (
	
 
 	output 	 wire      CON9B1_p,
    output   wire      CON9B1_n,    
   
    output    wire        ADC_A_CLK_p,    // ok
    output    wire        ADC_A_CLK_n,     
       
   // output   wire       CON9B2_p,
   // output   wire       CON9B2_n,
    
    
    inout    wire [15:0] CON9B3,   
     
    input    wire    sys_clk,
    input    wire    sig_clk,
    input    wire    clk_200,
    input    wire    clk_600,
    input    wire    rst,      
    // ---- Serdes ---
    input    wire         test_mode,
    input    wire [31:0]  din_test,

    input    wire        shape_load,            // set false path
    input    wire [5:0]  pulse_width,        // set false path
    input   wire [6:0]   i_offset_quotient,        // set false path
    input   wire [2:0]   i_offset_remainder,
    input    wire        sig_pola,
    
    // delay tap 78ps per tap (max 2.418ns)

    input wire [4:0]    delay_tap_in,        // set false path

    // from random
    input     wire [31:0] din_pm,
    input     wire        div_pm,
    output    wire        dir_pm,

    output    wire        do_pm_bit,
    output    wire        dov_pm_bit,

    // ---- Enable  -----
    input    wire        hmc920_en,
    input    wire        pm_pi_en,
    // ---- PM DAC (AD5761) ---
    input    wire [23:0]    pm_dac_din,
    input    wire           pm_dac_din_valid,
    output   wire           pm_dac_din_ready,
    output   wire [23:0]    pm_dac_dout,
    // ---- DP (AD5252) ---
    input    wire [23:0]    dp_din,
    input    wire           dp_din_valid,
    output   wire [7:0]     dp_dout    
 
 
);



// ---------------------------------------------------------------------------------
// CON90B2 Assign
// ---------------------------------------------------------------------------------


/*
OBUFDS #(
    .IOSTANDARD("DEFAULT"), // Specify the output I/O standard
    .SLEW("FAST")            // Specify the output slew rate
) OBUFDS_2 (
    .O(CON9B2_p),        // Diff_p output (connect directly to top-level port)
    .OB(CON9B2_n),        // Diff_n output (connect directly to top-level port)
    .I(1'b1)                // Buffer input
);
*/
OBUFDS #(
    .IOSTANDARD("DEFAULT"), // Specify the output I/O standard
    .SLEW("FAST")            // Specify the output slew rate
) OBUFDS_2 (
    .O(ADC_A_CLK_p),        // Diff_p output (connect directly to top-level port)
    .OB(ADC_A_CLK_n),        // Diff_n output (connect directly to top-level port)
    .I(sig_ind_clk)                // Buffer input
);



data_serialize_c data_serialize (
	.clk			(sig_clk),		// I
	.rst			(rst),			// I
    .clk_200        (clk_200),    // I
    .clk_600        (clk_600),    // I

	.test_mode		(test_mode),	// I
	.din_test		(din_test),		// I [31:0]

	.polarity		(sig_pola),			// I
	.din			(din_pm),		// I [31:0]
	.din_valid		(div_pm),		// I
	.din_ready		(dir_pm),		// O

	.do_bit			(do_pm_bit),	// O
	.dov_bit		(dov_pm_bit),	// O

	.dout_p			(CON9B1_p),		// O
	.dout_n			(CON9B1_n),		// O

	.shape_load		(shape_load),	// I
	.pulse_width	(pulse_width),	// I [5:0]
    .i_offset_quotient    (i_offset_quotient),        // I [6:0]
    .i_offset_remainder   (i_offset_remainder),         // [2:0]


	.delay_tap_in	(delay_tap_in),		// I [4:0]
	.delay_tap_out	()	// O [4:0]

);


// ---------------------------------------------------------------------------------
// Assign CON7B3
// ---------------------------------------------------------------------------------
wire	pm_dac_sck;
wire	pm_dac_scs;
wire	pm_dac_sdo;
wire	pm_dac_sdi;

// [DP] AD5252 
wire	dp_sck;
wire	dp_sdo;
wire	dp_sdi;
wire 	dp_T;
con10b3_iobuf_alice con10b3_iobuf_alice (
	.CON(CON9B3),						// IO [15:0]

	// DAC AD5761 for PM_HALF
	.pm_dac_sck(pm_dac_sck),			// I
	.pm_dac_scs(pm_dac_scs),			// I
	.pm_dac_sdo(pm_dac_sdo),			// I
	.pm_dac_sdi(pm_dac_sdi),			// O
	// Enable
	.hmc920_en(hmc920_en),				// I
	.pm_pi_en(pm_pi_en),						// I
	// [DP] AD5252 
    .dp_sck(dp_sck),            // I
    .dp_sdi(dp_sdi),            // O
    .dp_sdo(dp_sdo),            // I
    .dp_T(dp_T)            // IO    	
);

// ---------------------------------------------------------------------------------
//  PM (AD5761) SPI Port
// ---------------------------------------------------------------------------------
 spi_intf #(
	.SCLK_PERIOD(16),
	.CLK_START_END(8),
	.HIGH_CLK(4),
	.CLK_OFFSET(4),
	.CONT_WIDTH(0),
	.DATA_WIDTH(24),
	.RW_FLAG_BIT(0),
	.SCS_N_MODE(0),
	.SCLK_IDLE_MODE(1)
) spi_intf_ad5761(
	.clk(sys_clk),							// I
	.sclr(rst),						// I

	.sclk		(pm_dac_sck),		// O
	.scs_n		(pm_dac_scs),		// O
	.sdo		(pm_dac_sdo),		// O
	.sdi		(pm_dac_sdi),		// I
	.iobuf_T	(),						// O //not used for 4line SPI

	.din		(pm_dac_din),		// I [WORD_WIDTH-1:0]
	.din_valid	(pm_dac_din_valid),	// I
	.din_ready	(pm_dac_din_ready),	// O
	.dout		(pm_dac_dout),		// O [WORD_WIDTH-1:0]
	.dout_valid	()						// O
);

// ---------------------------------------------------------------------------------
//  Digital Potential Meter(AD5252) I2C Port
// --------------------------------------------------------------------------------


i2c_intf #(
	.SCLK_PERIOD(640),
	.CLK_START_END(320),
	.HIGH_CLK(160),
	.CLK_OFFSET(160),
	.CHIPSEL_WIDTH(8),
	.CONT_WIDTH(8),
	.RCHIPSEL_WIDTH(0),
	.DATA_WIDTH(8),
	.RW_FLAG_BIT(16),
	.DATA_NUM(1)
) i2c_intf_ad5252(
	.clk(sys_clk),							// I
	.sclr(rst),						// I

	.sclk		(dp_sck),		// O
	.sdo		(dp_sdo),		// O
	.sdi		(dp_sdi),		// I
	.iobuf_T	(dp_T),		// O //not used for 4line SPI

	.din		(dp_din),		// I [WORD_WIDTH-1:0]
	.din_valid	(dp_din_valid),	// I
	.dout		(dp_dout),		// O [WORD_WIDTH-1:0]
	.dout_valid	(),		// O
	.s_busy		()
);


endmodule