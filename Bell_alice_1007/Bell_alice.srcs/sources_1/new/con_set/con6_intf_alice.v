`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: HURA
// Engineer: LEE Seong Yun (sylee@hura.co.kr)
//
// Create Date: 2019/01/04 11:05:02
// Design Name: QKD Alice
// Module Name: con6_intf_alice
// Project Name: QKD
// Target Devices: Kintex 7 160T
// Tool Versions: Vivado 2018.1
// Description:
//
// Dependencies:
//
// Revision:
// 		build. 1 (2019/01/04)
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////
module con6_intf_alice (
input	wire	sig_clk,
	input	wire	sys_clk,
	input	wire	rst,
// to Con 6
	input 	wire [1:0]	CON6B1_p,
	input 	wire [1:0]	CON6B1_n,
	output	wire		CON6B2_p,
	output	wire		CON6B2_n,
	inout	wire [15:0]	CON6B3,
	
	// PD12
	output  wire 		clk_optic_locked,		// O	
	output  wire 		clk_optic,				// O
	output reg [31:0] 	do_PD12_cnt,
(* mark_debug = "true" *)	input	wire [3:0]	width_thres,  
(* mark_debug = "true" *)	output	reg			start,
	output	reg	[31:0]	PD_trig_cnt,		
	
	input	wire		pdsync_en,
	
	// ---- DP (AD5252) ---
	input	wire [23:0]	dp_din,
	input	wire		dp_din_valid,
	output	wire [7:0]	dp_dout	
);
// ---------------------------------------------------------------------------------
// Assign CON6B1, CON 
// ---------------------------------------------------------------------------------

wire			PD12_p;
wire	 		PD12_n;
(* mark_debug = "true" *)reg 			PD12_sig;		// O
assign			PD12_p = CON6B1_p[0];
assign          PD12_n = CON6B1_n[0];


// ---------------------------------------------------------------------------------
// Assign CON6B2
// ---------------------------------------------------------------------------------
OBUFDS #(
	.IOSTANDARD("DEFAULT"), // Specify the output I/O standard
	.SLEW("FAST")			// Specify the output slew rate
) OBUFDS_PD (
	.O(CON6B2_p),		// Diff_p output (connect directly to top-level port)
	.OB(CON6B2_n),		// Diff_n output (connect directly to top-level port)
	.I(1'b1)				// Buffer input
);
// ---------------------------------------------------------------------------------
// Assign CON6B3
// ---------------------------------------------------------------------------------
wire    dp_sck;
wire    dp_sdi;
wire    dp_sdo;
wire    dp_T;
con6b3_iobuf_alice con6b3_iobuf_alice (
	.CON(CON6B3),						// IO [21:0]

	.pdsync_en(pdsync_en),
	
	// [DP] AD5252  
	.dp_sck(dp_sck),			// I
	.dp_sdi(dp_sdi),			// O
	.dp_sdo(dp_sdo),			// I
	.dp_T(dp_T)			// IO	
);

// ---------------------------------------------------------------------------------
// PD12
// ---------------------------------------------------------------------------------
wire	PD12_ibuf_s;
IBUFDS i_pd12_ibuf (
    .I (PD12_p),
    .IB (PD12_n),
    .O (PD12_ibuf_s));

wire	input_clk_stopped;
wire	locked;
clk_wiz_optic clk_wiz_optic
(
    // Clock out ports
    .clk_out1(clk_optic),     // output clk_out1
    // Status and control signals
    .reset(rst), // input reset
    .input_clk_stopped(input_clk_stopped), // output input_clk_stopped
    .locked(locked),       // output locked
   // Clock in ports
    .clk_in1(PD12_ibuf_s));    // input clk_in1_n	

assign clk_optic_locked = ~input_clk_stopped & locked;
	
	
//reg		PD12_sig;
(* mark_debug = "true" *)reg		PD12_sig_d;
reg [31:0] clk_cnt;
reg [31:0] PD12_cnt_tmp;
(* mark_debug = "true" *)reg [3:0]	PD12_width;
(* mark_debug = "true" *)reg [31:0] PD_trig_cnt_tmp;

always @(posedge sig_clk) begin
	if (rst) begin
		clk_cnt <= 'b0;
		PD12_sig <= 1'b1;
		PD12_sig_d <= 1'b1;
	end
	else begin
		clk_cnt 		<= 	(clk_cnt >= (`SIGCLKPERSEC-1'b1)) ? 'b0 : clk_cnt + 1'b1;
		PD12_sig 		<= 	PD12_ibuf_s;
		PD12_sig_d 		<= 	PD12_sig;
		do_PD12_cnt		<= 	(clk_cnt == 'b0) ? PD12_cnt_tmp : do_PD12_cnt;
		PD12_cnt_tmp 	<= 	(clk_cnt == 'b0) ? 'b0 :
							(PD12_sig & ~PD12_sig_d) ? PD12_cnt_tmp + 1'b1 : PD12_cnt_tmp;
		PD12_width		<=  (PD12_sig) ? PD12_width + 1'b1 : 'b0;
		start			<=  (~PD12_sig && (PD12_width > width_thres)) ? 1'b1 : 1'b0;
		
		PD_trig_cnt		<= 	(clk_cnt == 'b0) ? PD_trig_cnt_tmp : PD_trig_cnt;		
        PD_trig_cnt_tmp <=     (clk_cnt == 'b0) ? 'b0 :
                            (start) ? PD_trig_cnt_tmp + 1'b1 : PD_trig_cnt_tmp;		
	end
end

// ---------------------------------------------------------------------------------
//  Digital Potential Meter(AD5252) I2C Port
// --------------------------------------------------------------------------------
i2c_intf #(
	.SCLK_PERIOD(640),
	.CLK_START_END(320),
	.HIGH_CLK(160),
	.CLK_OFFSET(160),
	.CHIPSEL_WIDTH(8),
	.CONT_WIDTH(8),
	.RCHIPSEL_WIDTH(0),
	.DATA_WIDTH(8),
	.RW_FLAG_BIT(16),
	.DATA_NUM(1)
) i2c_intf_ad5252(
	.clk(sys_clk),							// I
	.sclr(rst),						// I

	.sclk		(dp_sck),		// O
	.sdo		(dp_sdo),		// O
	.sdi		(dp_sdi),		// I
	.iobuf_T	(dp_T),		// O //not used for 4line SPI

	.din		(dp_din),		// I [WORD_WIDTH-1:0]
	.din_valid	(dp_din_valid),	// I
	.dout		(dp_dout),		// O [WORD_WIDTH-1:0]
	.dout_valid	(),		// O
	.s_busy		()
);

endmodule