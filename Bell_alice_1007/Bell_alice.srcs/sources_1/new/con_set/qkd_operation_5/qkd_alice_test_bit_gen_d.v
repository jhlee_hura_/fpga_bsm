`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: HURA
// Engineer: LEE Seong Yun (sylee@hura.co.kr)
//
// Create Date: 2019/01/04 11:05:02
// Design Name: QKD
// Module Name: qkd_alice_test_bit_gen
// Project Name: QKD
// Target Devices: Kintex 7 160T
// Tool Versions: Vivado 2018.1
// Description:
//
// Dependencies:
//
// Revision:
// 		build. 1 (2019/01/04)
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////
module qkd_alice_test_bit_gen_d (
	input wire 			clk,
	input wire 			sclr,
	input wire			load,
	input wire			rd_en,
	
	input wire [31:0]	_64b1_s0_test_data,
	input wire [31:0]	_64b1_s1_test_data,
	input wire [31:0]	_64b2_b_test_data,
	input wire [31:0]	_64b2_d_test_data,
	input wire [31:0]	_64b3_b_test_data,
	input wire [31:0]	_64b3_d_test_data,
	input wire [31:0]	_32b1_test_data,
	input wire [31:0]	_32b2_test_data,	


	output wire [1:0] 	_64b1_s_bits,
	output wire			_64b2_b_bit,
	output wire			_64b2_d_bit,
	output wire			_64b3_b_bit,
	output wire			_64b3_d_bit,	
	output wire			_32b1_bit,
	output wire			_32b2_bit,	
	
	output reg			dov_bit
);



reg [31:0] _64b1_s0;
reg [31:0] _64b1_s1;
reg [31:0] _64b2_b;
reg [31:0] _64b2_d;
reg [31:0] _64b3_b;
reg [31:0] _64b3_d;
reg [31:0] _32b1;
reg [31:0] _32b2;
assign _64b1_s_bits		= (_64b1_s1[31] && ~_64b1_s0[31]) ? 2'b11 : {_64b1_s1[31],_64b1_s0[31]};
assign _64b2_b_bit		= _64b2_b[31];
assign _64b2_d_bit		= _64b2_d[31];
assign _64b3_b_bit		= _64b3_b[31];
assign _64b3_d_bit		= _64b3_d[31];
assign _32b1_bit		= _32b1[31];
assign _32b2_bit		= _32b2[31];
always @(posedge clk) begin
	if (sclr) begin
		_64b1_s0 <= 'b0;
		_64b1_s1 <= 'b0;
		_64b2_b <= 'b0;
		_64b2_d <= 'b0;
		_64b3_b <= 'b0;
		_64b3_d <= 'b0;
		_32b1	<= 'b0;
		_32b2	<= 'b0;		
		dov_bit <= 1'b0;
	end
	else begin
		if (load) begin
			_64b1_s0 <= _64b1_s0_test_data;
			_64b1_s1 <= _64b1_s1_test_data;
			_64b2_b <= _64b2_b_test_data;
			_64b2_d <= _64b2_d_test_data;
			_64b3_b <= _64b3_b_test_data;
			_64b3_d <= _64b3_d_test_data;
			
			_32b1 <= _32b1_test_data;
			_32b2 <= _32b2_test_data;			
			dov_bit <= 1'b0;
		end
		else if (rd_en) begin
			_64b1_s0 <= {_64b1_s0[30:0],_64b1_s0[31]};
			_64b1_s1 <= {_64b1_s1[30:0],_64b1_s1[31]};
			_64b2_b <= {_64b2_b[30:0],_64b2_b[31]};
			_64b2_d <= {_64b2_d[30:0],_64b2_d[31]};
			_64b3_b <= {_64b3_b[30:0],_64b3_b[31]};
			_64b3_d <= {_64b3_d[30:0],_64b3_d[31]};				
			_32b1 <= {_32b1[30:0],_32b1[31]};
			_32b2 <= {_32b2[30:0],_32b2[31]};	
			
			dov_bit <= 1'b1;
		end
		else begin
			_64b1_s0 <= _64b1_s0;
			_64b1_s1 <= _64b1_s1;
			_64b2_b <= _64b2_b;
			_64b2_d <= _64b2_d;
			_64b3_b <= _64b3_b;
			_64b3_d <= _64b3_d;		
			_32b1 	<= _32b1;
			_32b2 	<= _32b2;
			
			dov_bit <= 1'b0;
		end
	end
end


endmodule