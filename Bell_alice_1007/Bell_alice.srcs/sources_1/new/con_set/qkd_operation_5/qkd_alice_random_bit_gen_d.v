`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: HURA
// Engineer: LEE Seong Yun (sylee@hura.co.kr)
//
// Create Date: 2019/01/04 11:05:02
// Design Name: QKD
// Module Name: qkd_alice_random_bit_gen
// Project Name: QKD
// Target Devices: Kintex 7 160T
// Tool Versions: Vivado 2018.1
// Description:
//
// Dependencies:
//
// Revision:
// 		build. 1 (2019/01/04)
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////
module qkd_alice_random_bit_gen_d (

//------------------------------------COMMON-------------------------------------
    input wire			rd_en,

	output wire [1:0] 	_64b1_s_bits,
	
	output wire			_64b2_b_bit,
	output wire			_64b2_d_bit,

	output wire			_64b3_b_bit,
	output wire			_64b3_d_bit,

	output wire			_32b1_bit,
	output wire			_32b2_bit,
	
	output wire			dov_bit,
//-------------------------------------------------------------------------------


//-------------------------------------LSFR---------------------------------------
	input wire         clk,
	input wire         sclr_00, // except o_op_run_last
	input wire         sclr_01, // include o_op_run_last
	input wire [31:0]  seed,
	input wire         seed_valid,
	input wire [7:0]   thres1,		// lower thres
	input wire [7:0]   thres2,
//-------------------------------------------------------------------------------


//-------------------------------------DRBG--------------------------------------
    input wire          spd_trig,
    input wire          spd_trig_valid,
    
    input wire          rd_en_etp_veri,
    input wire          rd_en_drbg_veri,
    
    output wire [31:0]  etp_valid_cnt,  
    output wire [31:0]  drbg_valid_cnt,
    
    output wire [31:0]  o_etp_veri,
    output wire         full_etp_veri,
    output wire [31:0]  o_drbg_veri,
    output wire         full_drbg_veri,
    
    input wire          drbg_mode,
    
    output wire         ready_b_demux,
    output wire         empty_etp_fifo
//-------------------------------------------------------------------------------
		
);


(* mark_debug = "true" *)wire [63:0] do_lfsr;
wire o_valid_lsfr;

wire o_dwdc_01;
wire o_dwdc_02;
wire o_dwdc_03;
wire o_dwdc_04;
wire o_dwdc_05;
wire o_dwdc_06;
wire o_dwdc_07;
wire o_dwdc_08;
wire o_dwdc_09;
wire o_dwdc_10;

wire o_valid_dwdc_01;

wire [7:0] do_sig_dec;


assign do_sig_dec   = (drbg_mode) ? {o_dwdc_01, o_dwdc_02, o_dwdc_03, o_dwdc_04, o_dwdc_05, o_dwdc_06, o_dwdc_07, o_dwdc_08} :do_lfsr[9:2] ;

assign _64b1_s_bits = (do_sig_dec < thres1) ? 2'b00 : (do_sig_dec < thres2) ? 2'b01 : 2'b11;

assign _64b2_b_bit	= (drbg_mode)? o_dwdc_09 : do_lfsr[50];
assign _64b2_d_bit	= (drbg_mode)? o_dwdc_10 : do_lfsr[0];

assign _64b3_b_bit	= (drbg_mode)? o_dwdc_09 : do_lfsr[49];
assign _64b3_d_bit	= (drbg_mode)? o_dwdc_10 : do_lfsr[1];

assign _32b1_bit	= (drbg_mode)? o_dwdc_09 : do_lfsr[48];
assign _32b2_bit	= (drbg_mode)? o_dwdc_10: do_lfsr[2];

assign dov_bit      = (drbg_mode)? o_valid_dwdc_01 : o_valid_lsfr;


//-------------------------------------LFSR--------------------------------------
lfsr #(
	.NBIT(64)
) lfsr (
	.clk(clk),					// I
	.sclr(sclr_01),				// I
	.en(rd_en),					// I
	.seed({seed,seed}),		    // I [NBIT-1:0] {32'hffffffff,seed}
	.seed_valid(seed_valid),	// I
	.dout(do_lfsr),				// O [NBIT-1:0]
	.dout_valid(o_valid_lsfr)	// O
);
//-------------------------------------------------------------------------------


//-------------------------------------DRBG-------------------------------------- 
    drbg (
        .clk (clk),                           // I
        .sclr (sclr_00 || ~drbg_mode),        // I
        .drbg_mode (drbg_mode),               // I
        
        .spd_trig (spd_trig),                 //I
        .spd_trig_valid (spd_trig_valid),     //I
        
        .empty_etp_fifo (empty_etp_fifo),     // O
        
        .rd_en_etp_veri (rd_en_etp_veri),     // I
        .o_etp_veri (o_etp_veri),             // O [31:0]
        .full_etp_veri (full_etp_veri),       // O
                
        .rd_en_drbg_veri (rd_en_drbg_veri),   // I
        .o_drbg_veri (o_drbg_veri),           // O [31:0]
        .full_drbg_veri (full_drbg_veri),     // O 
        
        .o_etp_gen_valid_cnt (etp_valid_cnt), // O [31:0]
        .oRandomNoValid_cnt (drbg_valid_cnt), // O [31:0]
        
        .ready_b_demux (ready_b_demux),       // O
  
        .rd_en (rd_en),                       // I
        .o_dwdc_01 (o_dwdc_01),               // O
        .o_dwdc_02 (o_dwdc_02),               // O
        .o_dwdc_03 (o_dwdc_03),               // O
        .o_dwdc_04 (o_dwdc_04),               // O
        .o_dwdc_05 (o_dwdc_05),               // O
        .o_dwdc_06 (o_dwdc_06),               // O
        .o_dwdc_07 (o_dwdc_07),               // O
        .o_dwdc_08 (o_dwdc_08),               // O
        .o_dwdc_09 (o_dwdc_09),               // O
        .o_dwdc_10 (o_dwdc_10),               // O
        .o_valid_dwdc_01 (o_valid_dwdc_01),   // O
        .o_valid_dwdc_02 (),                  // O
        .o_valid_dwdc_03 (),                  // O
        .o_valid_dwdc_04 (),                  // O
        .o_valid_dwdc_05 (),                  // O
        .o_valid_dwdc_06 (),                  // O
        .o_valid_dwdc_07 (),                  // O
        .o_valid_dwdc_08 (),                  // O
        .o_valid_dwdc_09 (),                  // O
        .o_valid_dwdc_10 ()                   // O
    );   
//-------------------------------------------------------------------------------

    
endmodule

