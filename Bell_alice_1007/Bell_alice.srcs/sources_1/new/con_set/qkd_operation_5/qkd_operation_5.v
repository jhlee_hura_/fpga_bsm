`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: HURA
// Engineer: LEE Seong Yun (sylee@hura.co.kr)
//
// Create Date: 2019/01/04 11:05:02
// Design Name: QKD Alice
// Module Name: QKD_Alice
// Project Name: QKD
// Target Devices: Kintex 7 160T
// Tool Versions: Vivado 2018.1
// Description:
//
// Dependencies:
//
// Revision:
// 		build. 1 (2019/01/04)
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////
module qkd_operation_5 (
input wire     	   clk, 	// sig_clk,
input wire        sclr,

output reg [63:0]    o_64b1_s,
output reg [63:0]    o_64b2,
output reg [63:0]    o_64b3,    
output reg [31:0]    o_32b1,
output reg [31:0]    o_32b2,
output reg           o_op_run_last,
output reg           o_op_run_start,
output reg           dout_valid,
output wire          busy,
input  wire          i_data_require,
output wire [13:0]   sig_number_128b,
    
    

// A_READY_REG    
input wire              op_ready,
// A_STOP_REG    
input wire              op_stop,

(* mark_debug = "true" *)input wire op_run,
(* mark_debug = "true" *)input wire    [20:0]    sig_number,

// A_SEED_REG
(* mark_debug = "true" *)input wire [31:0]      seed,
(* mark_debug = "true" *)input wire             seed_valid,
 // A_SIG_THRES1_REG     
input wire    [7:0]    thres1,
// A_SIG_THRES2_REG     
input wire    [7:0]    thres2,

//-----------------------------------DRBG----------------------------------------
input wire         spd_trig,
input wire         spd_trig_valid,
// A_DRBG_MODE_REG
input wire         drbg_mode,
// A_ETP_CNT_REG
output wire [31:0] etp_valid_cnt,
// A_DRBG_CNT_REG
output wire [31:0] drbg_valid_cnt,
// A_ETP_DRBG_FIFO_STAT_REG
output wire        ready_b_demux,
output wire        empty_etp_fifo,
output wire        full_etp_veri,
output wire        full_drbg_veri,
// A_ETP_VERI_READ_REG
input wire         rd_en_etp_veri,
output wire [31:0] o_etp_veri,
// A_DRBG_VERI_READ_REG
input wire         rd_en_drbg_veri,
output wire [31:0] o_drbg_veri,            
//-------------------------------------------------------------------------------



// A_SIG_TEST_MODE_REG
input wire           _64b1_test_mode,
// A_SIG0_TEST_DATA_REG    
input wire [31:0]    _64b1_s0_test_data,
// A_SIG1_TEST_DATA_REG    
input wire [31:0]    _64b1_s1_test_data,

// A_DB1_TEST_MODE_REG   
input wire           _64b2_d_test_mode,
input wire           _64b2_b_test_mode,    
 // A_DB1_B_TEST_DATA_REG    
input wire [31:0]    _64b2_b_test_data,
// A_DB1_D_TEST_DATA_REG    
input wire [31:0]    _64b2_d_test_data,

// A_DB2_TEST_MODE_REG   
input wire           _64b3_d_test_mode,
input wire           _64b3_b_test_mode,
// A_DB2_B_TEST_DATA_REG    
input wire [31:0]    _64b3_b_test_data,
// A_DB2_B_TEST_DATA_REG    
input wire [31:0]    _64b3_d_test_data,

// A_PM_TEST_MODE_REG   
input wire           _32b1_test_mode, // 32 bit
// A_PM_TEST_DATA_REG   
input wire [31:0]    _32b1_test_data,

// A_L_TEST_MODE_REG   
input wire           _32b2_test_mode,
// A_L_TEST_DATA_REG   
input wire [31:0]    _32b2_test_data
 
  
);

(* mark_debug = "true" *)reg [1:0]    stat;
localparam WAIT_CNT = 'd5;
localparam IDLE_STAT = 2'b00;
localparam READY_STAT = 2'b01;
localparam RUN_STAT = 2'b10;
localparam WAIT_STAT = 2'b11;
reg         first_flag;
reg [7:0]   wait_cnt;

assign      busy = (stat != IDLE_STAT) | o_op_run_last;

reg            start_flag;
reg            stop_flag;
reg            buffer_full;

(* mark_debug = "true" *)reg  [15:0]    sig_cnt;
(* mark_debug = "true" *)wire [15:0]    sig_number_32b;
assign sig_number_32b = (sig_number[4:0] == 5'b0) ? {1'b0,sig_number[20:5]} : sig_number[20:5] + 1'b1;
assign sig_number_128b = (sig_number[6:0] == 7'b0) ? {1'b0,sig_number[20:7]} : sig_number[20:7] + 1'b1;


wire op_end;

wire           m_word_valid;
wire [63:0]    m_64b1_s;
wire [63:0]    m_64b2;
wire [63:0]    m_64b3;
wire [31:0]    m_32b1;
wire [31:0]    m_32b2;

always @(posedge clk) begin
if (sclr) begin
    
    //OUTPUT
    o_64b1_s    <= 'b0;
    o_64b2        <= 'b0;
    o_64b3        <= 'b0;
    o_32b1        <= 'b0;
    o_32b2        <= 'b0;    
    dout_valid <= 1'b0;
    o_op_run_start    <= 1'b0;
    o_op_run_last    <= 1'b0;
        
    //interior
    buffer_full <= 1'b0;
    sig_cnt  <= 'b0;
    start_flag    <= 1'b0;
    stop_flag    <= 1'b0;
    first_flag    <= 1'b0;
    wait_cnt    <= 8'b0;
    
    stat        <= IDLE_STAT;
    
end
else begin
    case (stat)
        IDLE_STAT : begin
            
            o_64b1_s        <= 64'b0;
            o_64b2        <= 'b0;
            o_64b3        <= 'b0;
            o_32b1        <= 'b0;
            o_32b2        <= 'b0;                    
            dout_valid     <= 1'b0;
            o_op_run_start    <= 1'b0;
            o_op_run_last    <= 1'b0;
                        
            buffer_full    <= 1'b0; // 1!
            sig_cnt        <= 'b0; 
            start_flag    <= 1'b0;
            stop_flag    <= 1'b0;
            first_flag    <= 1'b0;
            wait_cnt    <= 8'b0;
            
            stat        <= (op_ready)    ? READY_STAT : IDLE_STAT;
        end
        
        READY_STAT : begin

            wait_cnt    <= 4'b0;

            stat        <=     (op_run)    ? WAIT_STAT :
                            (op_stop) ? IDLE_STAT : READY_STAT;
        end            
        WAIT_STAT : begin

            wait_cnt    <= (wait_cnt > WAIT_CNT) ? wait_cnt : wait_cnt + 1'b1;
            start_flag    <= 1'b1;
            first_flag    <= 1'b1;
            
            
            if (drbg_mode) begin
            stat        <=     ((wait_cnt > WAIT_CNT) && ~ready_b_demux) ? RUN_STAT : WAIT_STAT;   //            stat        <=     ((wait_cnt > WAIT_CNT) && buffer_full) ? RUN_STAT : WAIT_STAT;
            end 
            else begin
            stat        <=     ((wait_cnt > WAIT_CNT)) ? RUN_STAT : WAIT_STAT;   //            stat        <=     ((wait_cnt > WAIT_CNT) && buffer_full) ? RUN_STAT : WAIT_STAT;
            end
       
        end
        RUN_STAT : begin
            wait_cnt    <= 8'b0;
            stop_flag <= (op_stop) ? 1'b1 : stop_flag;
            if (buffer_full && i_data_require) begin
                o_op_run_start    <= (start_flag) ? 1'b1 : 1'b0;   // 1
                o_op_run_last    <= (op_end || stop_flag || op_stop) ? 1'b1 : 1'b0;   // 0
                start_flag    <= 1'b0;     // 0
                sig_cnt       <= sig_cnt + 1'b1;  // 1 
                first_flag    <= 1'b0;    // 0 
                o_64b1_s      <=  m_64b1_s;  // 0       o_64b1_s      <= (first_flag) ? 64'hffff_ffff_0000_0000 : m_64b1_s;  
                o_64b2        <=  m_64b2; //0       o_64b2        <= (first_flag) ? 64'hffff_ffff_0000_0000 : m_64b2;
                o_64b3        <=  m_64b3;    //0    o_64b3        <= (first_flag) ? 64'hffff_ffff_0000_0000 : m_64b3;               
                o_32b1        <=  m_32b1;            //0      o_32b1        <= (first_flag) ? 32'hffff_0000 : m_32b1; 
                o_32b2        <=  m_32b2;        //0          o_32b2        <= (first_flag) ? 32'hffff_0000 : m_32b2;
                buffer_full <= 1'b0;   //0
                dout_valid    <= 1'b1;   
                stat        <= ( op_end || stop_flag || op_stop) ? IDLE_STAT : RUN_STAT;   //run 
            end
            else begin
                buffer_full <=    (m_word_valid) ? 1'b1 : buffer_full;
                o_op_run_last    <= 1'b0;
                o_op_run_start    <= 1'b0;
                dout_valid    <= 1'b0;
                stat        <= RUN_STAT;
            end

        end
    endcase
end
end

//---------------------------------------------------
// Data generation for indoor
//---------------------------------------------------
wire [63:0]    do_in_64b1_s; // Inner
wire [63:0]    do_in_64b2;
wire [63:0]    do_in_64b3;
wire [31:0]    do_in_32b1;
wire [31:0]    do_in_32b2;
wire           dov_in_word;  
qkd_alice_inner_randm_gen_d qkd_alice_inner_randm_gen_d(
.clk(clk), // I sig_clk,
.sclr_00(sclr), // I
.sclr_01(sclr||o_op_run_last), // I

.buffer_full(buffer_full),     // I
.upper_stat(stat),  // I
// A_READY_REG
.op_ready(op_ready), // I

// A_SEED_REG    
.seed(seed), // I [31:0]
.seed_valid(seed_valid), // I

// A_SIG_THRES1_REG    
.thres1(thres1), // I [7:0]
// A_SIG_THRES2_REG    
.thres2(thres2), // I [7;0]

//-------------------------------------------------------------------------------
    // for DRBG
.spd_trig        (spd_trig),        // I
.spd_trig_valid  (spd_trig_valid),  // I
 
.drbg_mode       (drbg_mode),       // I

.etp_valid_cnt   (etp_valid_cnt),   // O [31:0]
.drbg_valid_cnt  (drbg_valid_cnt),  // O [31:0]

.ready_b_demux   (ready_b_demux),   // O
.empty_etp_fifo  (empty_etp_fifo),  // O
.full_etp_veri   (full_etp_veri),   // O
.full_drbg_veri  (full_drbg_veri),  // O

.rd_en_etp_veri  (rd_en_etp_veri),  // I
.o_etp_veri      (o_etp_veri),      // O [31:0]

.rd_en_drbg_veri (rd_en_drbg_veri), // I  
.o_drbg_veri     (o_drbg_veri),     // O [31:0]
//-------------------------------------------------------------------------------


// ----- OP Test Data    

// A_SIG_TEST_MODE_REG        
._64b1_test_mode(_64b1_test_mode), // I
// A_SIG0_TEST_DATA_REG        
._64b1_s0_test_data(_64b1_s0_test_data), // I [31:0]
// A_SIG1_TEST_DATA_REG    
._64b1_s1_test_data(_64b1_s1_test_data), // I [31:0]

// A_DB1_TEST_MODE_REG   
._64b2_d_test_mode(_64b2_d_test_mode), // I
._64b2_b_test_mode(_64b2_b_test_mode), // I
// A_DB1_B_TEST_DATA_REG   
._64b2_b_test_data(_64b2_b_test_data), // I [31:0]
// A_DB1_D_TEST_DATA_REG    
._64b2_d_test_data(_64b2_d_test_data), // I [31:0]
  
// A_DB2_TEST_MODE_REG   
._64b3_d_test_mode(_64b3_d_test_mode), // I
._64b3_b_test_mode(_64b3_b_test_mode), // I
// A_DB2_B_TEST_DATA_REG   
._64b3_b_test_data(_64b3_b_test_data), // I [31:0]
// A_DB2_D_TEST_DATA_REG    
._64b3_d_test_data(_64b3_d_test_data), // I [31:0]


// A_PM_TEST_MODE_REG   
._32b1_test_mode(_32b1_test_mode), // I
// A_PM_TEST_DATA_REG   
._32b1_test_data(_32b1_test_data), // I [31:0]

// A_L_TEST_MODE_REG   
._32b2_test_mode(_32b2_test_mode), // I
// A_L_TEST_DATA_REG   
._32b2_test_data(_32b2_test_data), // I [31:0]


.word_valid(dov_in_word),    // O

.o_64b1_s(do_in_64b1_s), // O [63:0] IM SIG01
.o_64b2(do_in_64b2), // O [63:0] IM DB1
.o_64b3(do_in_64b3), // O [31:0] IM DB2
.o_32b1(do_in_32b1), // O [31:0]    PM
.o_32b2(do_in_32b2) // O [31:0]    Laser
);

assign    m_64b1_s = do_in_64b1_s;
assign    m_64b2 = do_in_64b2;
assign    m_64b3 = do_in_64b3;
assign    m_32b1 = do_in_32b1;
assign    m_32b2 = do_in_32b2;

assign    m_word_valid = dov_in_word;
assign    op_end = (sig_cnt >= (sig_number_32b-1'b1));
endmodule