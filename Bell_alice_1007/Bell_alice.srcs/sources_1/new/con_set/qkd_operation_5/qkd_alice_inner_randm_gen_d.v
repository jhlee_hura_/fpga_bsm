`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/06/03 11:54:31
// Design Name: 
// Module Name: qkd_alice_inner_randm_gen
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module qkd_alice_inner_randm_gen_d(
	input wire         clk, 	// sig_clk,
	input wire         sclr_00,  // except o_op_run_last
	input wire         sclr_01,
  	
    input wire 			buffer_full,	
	input wire [1:0]   upper_stat,    
	// A_READY_REG
	input wire		    op_ready,
		
	// A_SEED_REG	
	input wire [31:0]  seed,
    input wire         seed_valid,
	

	// A_SIG_THRES1_REG    
    input wire [7:0]   thres1,
	// A_SIG_THRES2_REG    
    input wire [7:0]   thres2,
//-------------------------------------------------------------------------------



//-------------------------------------------------------------------------------
    // for DRBG
    input wire         spd_trig,
    input wire         spd_trig_valid,

    input wire         drbg_mode,
    
    output wire [31:0] etp_valid_cnt,
    output wire [31:0] drbg_valid_cnt,
    
    output wire        empty_etp_fifo,
    output wire        ready_b_demux,
    output wire        full_etp_veri,
    output wire        full_drbg_veri,

    input wire         rd_en_etp_veri,
    output wire [31:0] o_etp_veri,
    
    input wire         rd_en_drbg_veri,
    output wire [31:0] o_drbg_veri,
//-------------------------------------------------------------------------------
    
    
    
    
// ----- OP Test Data	
//-------------------------------------------------------------------------------
	// A_SIG_TEST_MODE_REG    	
	input wire			_64b1_test_mode,
	// A_SIG0_TEST_DATA_REG		
	input wire [31:0]	_64b1_s0_test_data, // sig0
	// A_SIG1_TEST_DATA_REG	
    input wire [31:0]   _64b1_s1_test_data, // sig1
	
 	// A_DB1_TEST_MODE_REG   
    input wire           _64b2_d_test_mode,
    input wire           _64b2_b_test_mode,	
 	// A_DB1_B_TEST_DATA_REG   
    input wire [31:0]    _64b2_b_test_data, // basis
	// A_DB1_D_TEST_DATA_REG    
    input wire [31:0]    _64b2_d_test_data, // data
	
 	// A_DB2_TEST_MODE_REG   
    input wire           _64b3_d_test_mode,
    input wire           _64b3_b_test_mode,	
 	// A_DB2_B_TEST_DATA_REG   
    input wire [31:0]    _64b3_b_test_data,
	// A_DB2_B_TEST_DATA_REG    
    input wire [31:0]    _64b3_d_test_data,
	
 	// A_PM_TEST_MODE_REG   
    input wire           _32b1_test_mode, // 32 bit
 	// A_PM_TEST_DATA_REG   
    input wire [31:0]    _32b1_test_data,
	
 	// A_L_TEST_MODE_REG   
    input wire           _32b2_test_mode,
 	// A_L_TEST_DATA_REG   
    input wire [31:0]    _32b2_test_data,
//-------------------------------------------------------------------------------

    
    
    
//-------------------------------------------------------------------------------
    output reg 			word_valid,    
    output reg [63:0]    o_64b1_s,
    output reg [63:0]    o_64b2, // data_base
    output reg [63:0]    o_64b3, // data_base	
    output reg [31:0]    o_32b1, 
    output reg [31:0]    o_32b2	// laser 	
//-------------------------------------------------------------------------------
);






    reg [63:0]	  r_64b1_s;
    reg [63:0]    r_64b2;
    reg [63:0]    r_64b3;
    reg [31:0]    r_32b1;
    reg [31:0]    r_32b2; // laser	
	
    reg           rd_en;
    wire [1:0]    _64b1_s_bits;
    wire [1:0]    _64b2_bits; // Data & Basis Bit
	wire [1:0]	  _64b3_bits;
	wire		  _32b1_bit;
	wire		  _32b2_bit;

    reg [1:0]	stat;

localparam IDLE_STAT = 2'b00;
localparam RUN_STAT = 2'b10;
localparam OUT_STAT = 2'b11;
    reg [5:0]    k = 'b0;
//	assign o_64b1_s = (word_valid) ? r_64b1_s: 'b0 ;
//	assign o_data_basis = (word_valid) ? r_64b2 : 'b0 ;
//	assign o_basis = (word_valid) ? r_32b1 : 'b0;	
    always @(posedge clk) begin
        if (sclr_01) begin
            k        		<= 6'h0;			
            r_64b1_s    	<= 'b0;
            r_64b2    		<= 'b0;
			r_64b3    		<= 'b0;
            r_32b1    		<= 'b0;		
			r_32b2			<= 'b0;
			
            o_64b1_s    	<= 'b0;
            o_64b2    		<= 'b0;
			o_64b3			<= 'b0;
			o_32b1			<= 'b0;
			o_32b2			<= 'b0;
			stat            <= IDLE_STAT;
            rd_en    		<= 1'b0;
            word_valid    	<= 1'b0;
        end
        else begin

  	    case (stat)
                IDLE_STAT: begin
                    word_valid         <= 1'b0;
                    k                 <= 6'h20;
    
                    r_64b1_s        <= 'b0;
                    r_64b2          <= 'b0;
                    r_64b3          <= 'b0;                
                    r_32b1             <= 'b0; 
                    r_32b2            <= 'b0;
                    stat <= ((word_valid==1'b0)&(buffer_full==0)&(upper_stat==RUN_STAT))? RUN_STAT : IDLE_STAT;
                    rd_en <=((word_valid==1'b0)&(buffer_full==0)&(upper_stat==RUN_STAT))? 1'b1 : 0;
                end
                RUN_STAT : begin
                     k             <= k - 1'b1 ;
                     r_64b1_s    <=(k==0)?r_64b1_s    : {r_64b1_s[61:0], _64b1_s_bits[1:0]};
                     r_64b2      <= (k==0)? r_64b2      :{r_64b2[61:0], _64b2_bits[1:0]};
                     r_64b3      <= (k==0)? r_64b3      :{r_64b3[61:0], _64b3_bits[1:0]};                
                     r_32b1      <= (k==0)? r_32b1      :{r_32b1[30:0], _32b1_bit};
                     r_32b2      <= (k==0)? r_32b2      :{r_32b2[30:0], _32b2_bit}; 
                     stat <= (k==0)? OUT_STAT : RUN_STAT;                
                     rd_en         <=  (k==1)? 1'b0 : rd_en;
                end            
                OUT_STAT : begin
                     word_valid  <= 1'b1;
                     o_64b1_s       <=  r_64b1_s;
                     o_64b2         <=  r_64b2;    
                     o_64b3         <=  r_64b3;    
                     o_32b1         <=  r_32b1;
                     o_32b2         <=  r_32b2;    
                     stat <= IDLE_STAT;
                end
    
            endcase
        end
    end
 /*
 	        else if (k == 'b0) begin
     word_valid    <= (buffer_full) ? 1'b0 : 1'b1;
     k             <= (buffer_full) ? 'b0  : 6'h20;
     rd_en         <= (buffer_full) ? 1'b0 : 1'b1;
     
     o_64b1_s      <=  r_64b1_s;
     o_64b2          <=  r_64b2;    
     o_64b3            <=  r_64b3;    
     o_32b1            <= r_32b1;
     o_32b2                <= r_32b2;    
     
     r_64b1_s        <= (buffer_full) ? r_64b1_s :  {r_64b1_s[61:0], _64b1_s_bits[1:0]};
     r_64b2          <= (buffer_full) ? r_64b2 : {r_64b2[61:0], _64b2_bits[1:0]};
     r_64b3          <= (buffer_full) ? r_64b3 : {r_64b3[61:0], _64b3_bits[1:0]};                
     r_32b1            <= (buffer_full) ? r_32b1 : {r_32b1[30:0], _32b1_bit};   
     r_32b2            <= (buffer_full) ? r_32b2 : {r_32b2[30:0], _32b2_bit};     
 end        
 else begin
     word_valid  <= 1'b0;
     rd_en       <= 1'b1;
     k           <= k - 1'b1 ;
     r_64b1_s    <= {r_64b1_s[61:0], _64b1_s_bits[1:0]};
     r_64b2      <= {r_64b2[61:0], _64b2_bits[1:0]};
     r_64b3      <= {r_64b3[61:0], _64b3_bits[1:0]};                
     r_32b1      <= {r_32b1[30:0], _32b1_bit};
     r_32b2        <= {r_32b2[30:0], _32b2_bit};            
     
     o_64b1_s    <= (buffer_full) ?  'b0  : o_64b1_s;
     o_64b2      <= (buffer_full) ?  'b0  : o_64b2;    
     o_64b3      <= (buffer_full) ?  'b0  : o_64b3;                
     o_32b1        <= (buffer_full) ?    'b0  : o_32b1;    
     o_32b2        <= (buffer_full) ?    'b0  : o_32b2;                    
 end
 */   

// _64b1_s_bits		
    wire [1:0]    t_64b1_s_bits;
    wire [1:0]    r_64b1_s_bits;
	
// _64b2_bits	
    wire         t_64b2_b_bit;	
    wire         t_64b2_d_bit;	
    wire         r_64b2_b_bit;	
    wire         r_64b2_d_bit;
	
// _64b3_bits
    wire         t_64b3_b_bit;	
    wire         t_64b3_d_bit;
    wire         r_64b3_b_bit;	
    wire         r_64b3_d_bit;	
// _32b1_bit, _32b2_bit,  	
    wire         r_32b1_bit;	
    wire         t_32b1_bit;
    wire         r_32b2_bit;	
    wire         t_32b2_bit;
    
    assign _64b1_s_bits = (_64b1_test_mode) ? t_64b1_s_bits : (r_64b1_s_bits);
	assign _64b2_bits[1] =(_64b2_d_test_mode) ? t_64b2_d_bit : r_64b2_d_bit;
	assign _64b2_bits[0] =(_64b2_b_test_mode) ? t_64b2_b_bit : r_64b2_b_bit;
	
	assign _64b3_bits[1] =(_64b3_d_test_mode) ? t_64b3_d_bit : r_64b3_d_bit;
	assign _64b3_bits[0] =(_64b3_b_test_mode) ? t_64b3_b_bit : r_64b3_b_bit;	
//    assign _64b2_bits = (_64b2_test_mode) ? {t_64b2_d_bit,t_64b2_b_bit} : {r_64b2_d_bit,r_64b2_b_bit};
//    assign _64b3_bits = (_64b3_test_mode) ? {t_64b3_d_bit,t_64b3_b_bit} : {r_64b3_d_bit,r_64b3_b_bit};		
	
    assign _32b1_bit  = (_32b1_test_mode) ? {t_32b1_bit} : {r_64b1_s_bits[0]};
    assign _32b2_bit  = (_32b2_test_mode) ? {t_32b2_bit} : {r_64b1_s_bits[1]};
    
    
    
    
// --------------------------	 
// Random bits geneartion   
// --------------------------
    qkd_alice_random_bit_gen_d qkd_alice_random_bit_gen (
        .clk(clk),               // I
        .sclr_00(sclr_00),       // I except o_op_run_last
        .sclr_01(sclr_01),       // I include o_op_run_last
        .seed(seed),             // I [31:0]
        .seed_valid(seed_valid), // I
        .thres1(thres1),         // I [7:0]    // lower thres
        .thres2(thres2),         // I [7:0]
        
        .rd_en(rd_en),                    // I
                
        // DRBG --------------------------------
        .spd_trig        (spd_trig),        // I
        .spd_trig_valid  (spd_trig_valid),  // I
        
        .drbg_mode       (drbg_mode),       // I

        .etp_valid_cnt   (etp_valid_cnt),   // O [31:0]
        .drbg_valid_cnt  (drbg_valid_cnt),  // O [31:0]
        
        .ready_b_demux   (ready_b_demux),   // O
        .empty_etp_fifo  (empty_etp_fifo),  // O
        .full_etp_veri   (full_etp_veri),   // O
        .full_drbg_veri  (full_drbg_veri),  // O

        .rd_en_etp_veri  (rd_en_etp_veri),  // I
        .o_etp_veri      (o_etp_veri),      // O [31:0]
        
        .rd_en_drbg_veri (rd_en_drbg_veri), // I
        .o_drbg_veri     (o_drbg_veri),     // O [31:0]
        // -------------------------------------
    
        ._64b1_s_bits(r_64b1_s_bits),     // O [1:0]
        ._64b2_b_bit(r_64b2_b_bit),       // O
        ._64b2_d_bit(r_64b2_d_bit),       // O
        ._64b3_b_bit(r_64b3_b_bit),       // O
        ._64b3_d_bit(r_64b3_d_bit),       // O		
		._32b1_bit(r_32b1_bit),           // O
		._32b2_bit(r_32b2_bit),		      // O
        .dov_bit()                        // O
    ); // latency = 1
    
    
    
    
// --------------------------	 
// Test bits geneartion   
// --------------------------       
    qkd_alice_test_bit_gen_d qkd_alice_test_bit_gen (
        .clk(clk),                            		// I
        .sclr(sclr_01),                        		// I include o_op_run_last
        .load(op_ready),                        	// I       op_ready
        ._64b1_s0_test_data(_64b1_s0_test_data),    // I [31:0]
        ._64b1_s1_test_data(_64b1_s1_test_data),    // I [31:0]
		._64b2_b_test_data(_64b2_b_test_data),      // I [31:0]
        ._64b2_d_test_data(_64b2_d_test_data),      // I [31:0]
		._64b3_b_test_data(_64b3_b_test_data),      // I [31:0]
        ._64b3_d_test_data(_64b3_d_test_data),      // I [31:0]
		._32b1_test_data(_32b1_test_data),			// I [31:0]
		._32b2_test_data(_32b2_test_data),			// I [31:0]
        .rd_en(rd_en),                        // I
    
        ._64b1_s_bits(t_64b1_s_bits),                // O [1:0]
        ._64b2_b_bit(t_64b2_b_bit),            // O
        ._64b2_d_bit(t_64b2_d_bit),            // O
        ._64b3_b_bit(t_64b3_b_bit),            // O
        ._64b3_d_bit(t_64b3_d_bit),            // O	
		
		._32b1_bit(t_32b1_bit),
		._32b2_bit(t_32b2_bit),
        .dov_bit()                            // O
    ); 
 
    
       
endmodule
