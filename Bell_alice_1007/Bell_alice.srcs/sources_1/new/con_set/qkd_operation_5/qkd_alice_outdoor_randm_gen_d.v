`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/06/03 10:05:05
// Design Name: 
// Module Name: qkd_operation_outdoor
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module qkd_alice_outdoor_randm_gen_d(

	input wire		clk, 	// sig_clk,   
	output wire [31:0]	 sig1,
    output wire [31:0]   sig2,
    output wire [31:0]   data,	
    output wire [31:0]   basis,
	output wire			dout_valid,
 
	input  wire			i_dout_ready,
    input  wire        i_buffer_full,	
    input  wire [1:0]  i_stat,	       	
    // A_OUTDOOR_RNDM_FIFO_REG
    input wire                 fifo_outdr_rndm_en,
    input wire                 fifo_outdr_rndm_rst_r,    
    output  wire                 fifo_outdr_rndm_empty,
    output  wire                fifo_outdr_rndm_pf,
    output  wire                fifo_outdr_rndm_af,
    output  wire                fifo_outdr_rndm_full,
  
    input wire    [12:0]            fifo_outdr_rndm_pf_thres,    
    // A_OUTDOOR_DIN1_REG
    input  wire [31:0]         din_outdr1,
    input  wire               div_outdr1,
    // A_OUTDOOR_DIN2_REG
    input  wire [31:0]         din_outdr2,
    input wire                div_outdr2,
    // A_OUTDOOR_DIN3_REG
    input  wire [31:0]         din_outdr3,
    input  wire                div_outdr3,
    // A_OUTDOOR_DIN4_REG
    input  wire [31:0]         din_outdr4,
    input  wire                div_outdr4,
    // A_OUTDOOR_RNDM_MODE_REG
    input  wire                 outdoor_rndm_mode    
    );
localparam RUN_STAT = 2'b10;
wire        fifo_outdr_rndm_rst;  
wire        rd_en;           
pls_expander #(
    .COUNT(10),
    .POLARITY(1)
) c2h_0_rst_outdr_rndm_ext(
    .sin(fifo_outdr_rndm_rst_r),
    .clk(clk),
    .sout(fifo_outdr_rndm_rst)
); 
assign rd_en= (i_stat == RUN_STAT) && (i_buffer_full) && i_dout_ready;
// Outdoor Random Storage FIFO
fifo_32x8k outdoor_random_sig(
      .clk(clk),                        // I
      .srst(fifo_outdr_rndm_rst || !fifo_outdr_rndm_en),            // I
      .din(din_outdr1),                        // I [31 : 0]
      .wr_en(div_outdr1 && ~fifo_outdr_rndm_full),        // I
      .rd_en(rd_en && !fifo_outdr_rndm_empty),    // I
      .prog_full_thresh(fifo_outdr_rndm_pf_thres),        // I [12 : 0]
      .dout(sig1),                        // O [31 : 0]
      .full(fifo_outdr_rndm_full),                        // O
      .almost_full(fifo_outdr_rndm_af),                // O
      .empty(fifo_outdr_rndm_empty),                    // O
      .valid(dout_valid),                                // O
      .prog_full(fifo_outdr_rndm_pf)                    // O
);

wire                fifo_outdr_rndm_empty2;
wire                fifo_outdr_rndm_pf2;
wire                fifo_outdr_rndm_af2;
wire                fifo_outdr_rndm_full2;
fifo_32x8k outdoor_random_decoy(
      .clk(clk),                        // I
      .srst(fifo_outdr_rndm_rst || !fifo_outdr_rndm_en),            // I
      .din(din_outdr2),                        // I [31 : 0]
      .wr_en(div_outdr2 && ~fifo_outdr_rndm_full2),        // I
      .rd_en(rd_en && !fifo_outdr_rndm_empty2),    // I
      .prog_full_thresh(fifo_outdr_rndm_pf_thres),        // I [12 : 0]
      .dout(sig2),                        // O [31 : 0]
      .full(fifo_outdr_rndm_full2),                        // O
      .almost_full(fifo_outdr_rndm_af2),                // O
      .empty(fifo_outdr_rndm_empty2),                    // O
      .valid(),                                // O
      .prog_full(fifo_outdr_rndm_pf2)                    // O
);

wire                fifo_outdr_rndm_empty3;
wire                fifo_outdr_rndm_pf3;
wire                fifo_outdr_rndm_af3;
wire                fifo_outdr_rndm_full3;
fifo_32x8k outdoor_random_basis(
      .clk(clk),                        // I
      .srst(fifo_outdr_rndm_rst || !fifo_outdr_rndm_en),            // I
      .din(din_outdr3),                        // I [31 : 0]
      .wr_en(div_outdr3 && ~fifo_outdr_rndm_full3),        // I
      .rd_en(rd_en && !fifo_outdr_rndm_empty3),    // I
      .prog_full_thresh(fifo_outdr_rndm_pf_thres),        // I [12 : 0]
      .dout(basis),                        // O [31 : 0]
      .full(fifo_outdr_rndm_full3),                        // O
      .almost_full(fifo_outdr_rndm_af3),                // O
      .empty(fifo_outdr_rndm_empty3),                    // O
      .valid(),                                // O
      .prog_full(fifo_outdr_rndm_pf3)                    // O
);

wire                fifo_outdr_rndm_empty4;
wire                fifo_outdr_rndm_pf4;
wire                fifo_outdr_rndm_af4;
wire                fifo_outdr_rndm_full4;
fifo_32x8k outdoor_random_data(
      .clk(clk),                        // I
      .srst(fifo_outdr_rndm_rst || !fifo_outdr_rndm_en),            // I
      .din(din_outdr4),                        // I [31 : 0]
      .wr_en(div_outdr4 && ~fifo_outdr_rndm_full4),        // I
      .rd_en(rd_en && !fifo_outdr_rndm_empty4),    // I
      .prog_full_thresh(fifo_outdr_rndm_pf_thres),        // I [12 : 0]
      .dout(data),                        // O [31 : 0]
      .full(fifo_outdr_rndm_full4),                        // O
      .almost_full(fifo_outdr_rndm_af4),                // O
      .empty(fifo_outdr_rndm_empty4),                    // O
      .valid(),                                // O
      .prog_full(fifo_outdr_rndm_pf4)                    // O
);
    
    
   
endmodule
