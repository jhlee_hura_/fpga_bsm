`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: HURA
// Engineer: LEE Seong Yun (sylee@hura.co.kr)
//
// Create Date: 2019/01/04 11:05:02
// Design Name: QKD Alice
// Module Name: con4b3_iobuf_alice
// Project Name: QKD
// Target Devices: Kintex 7 160T
// Tool Versions: Vivado 2018.1
// Description:
//
// Dependencies:
//
// Revision:
// 		build. 1 (2019/01/04)
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////
module con5b3_iobuf_alice (
inout 	wire [15:0]	CON,

	input	wire [11:0] dac_in,
	input	wire		dac_clk,

	// DAC AD5761 for IM
	input wire 			im_dac_sck,
	input wire 			im_dac_scs,
	input wire 			im_dac_sdo
);

// --------------------------------------------------------------------
// DAC AD9742
// --------------------------------------------------------------------
IOBUF CON_0 (
	.O(),					// Buffer output
	.IO(CON[0]),			// Buffer inout port (connect directly to top-level port)
	.I(dac_in[0]),			// Buffer input
	.T(1'b0)				// 3-state enable input, high=input, low=output
);

IOBUF CON_1 (
	.O(),					// Buffer output
	.IO(CON[1]),			// Buffer inout port (connect directly to top-level port)
	.I(dac_in[1]),			// Buffer input
	.T(1'b0)				// 3-state enable input, high=input, low=output
);

IOBUF CON_2 (
	.O(),					// Buffer output
	.IO(CON[2]),			// Buffer inout port (connect directly to top-level port)
	.I(dac_in[2]),			// Buffer input
	.T(1'b0)				// 3-state enable input, high=input, low=output
);

IOBUF CON_3 (
	.O(),					// Buffer output
	.IO(CON[3]),			// Buffer inout port (connect directly to top-level port)
	.I(dac_in[3]),			// Buffer input
	.T(1'b0)				// 3-state enable input, high=input, low=output
);

IOBUF CON_4 (
	.O(),				// Buffer output
	.IO(CON[4]),		// Buffer inout port (connect directly to top-level port)
	.I(dac_in[4]),		// Buffer input
	.T(1'b0)			// 3-state enable input, high=input, low=output
);

IOBUF CON_5 (
	.O(),				// Buffer output
	.IO(CON[5]),		// Buffer inout port (connect directly to top-level port)
	.I(dac_in[5]),			// Buffer input
	.T(1'b0)			// 3-state enable input, high=input, low=output
);

IOBUF CON_6 (
	.O(),					// Buffer output
	.IO(CON[6]),			// Buffer inout port (connect directly to top-level port)
	.I(dac_in[6]),			// Buffer input
	.T(1'b0)				// 3-state enable input, high=input, low=output
);

IOBUF CON_7 (
	.O(),					// Buffer output
	.IO(CON[7]),			// Buffer inout port (connect directly to top-level port)
	.I(dac_in[7]),			// Buffer input
	.T(1'b0)				// 3-state enable input, high=input, low=output
);

IOBUF CON_8 (
	.O(),			// Buffer output
	.IO(CON[8]),			// Buffer inout port (connect directly to top-level port)
	.I(dac_in[8]),				// Buffer input
	.T(1'b0)				// 3-state enable input, high=input, low=output
);

IOBUF CON_9 (
	.O(),				// Buffer output
	.IO(CON[9]),		// Buffer inout port (connect directly to top-level port)
	.I(dac_in[9]),		// Buffer input
	.T(1'b0)			// 3-state enable input, high=input, low=output
);

IOBUF CON_10 (
	.O(),				// Buffer output
	.IO(CON[10]),		// Buffer inout port (connect directly to top-level port)
	.I(dac_in[10]),			// Buffer input
	.T(1'b0)			// 3-state enable input, high=input, low=output
);

IOBUF CON_11 (
	.O(),					// Buffer output
	.IO(CON[11]),			// Buffer inout port (connect directly to top-level port)
	.I(dac_in[11]),			// Buffer input
	.T(1'b0)				// 3-state enable input, high=input, low=output
);

IOBUF CON_12 (
	.O(),					// Buffer output
	.IO(CON[12]),			// Buffer inout port (connect directly to top-level port)
	.I(dac_clk),			// Buffer input
	.T(1'b0)				// 3-state enable input, high=input, low=output
);

IOBUF CON_13 (
	.O(),					// Buffer output
	.IO(CON[13]),			// Buffer inout port (connect directly to top-level port)
	.I(im_dac_sck),			// Buffer input
	.T(1'b0)				// 3-state enable input, high=input, low=output
);

IOBUF CON_14 (
	.O(),					// Buffer output
	.IO(CON[14]),			// Buffer inout port (connect directly to top-level port)
	.I(im_dac_sdo),			// Buffer input
	.T(1'b0)				// 3-state enable input, high=input, low=output
);

IOBUF CON_15 (
	.O(),					// Buffer output
	.IO(CON[15]),			// Buffer inout port (connect directly to top-level port)
	.I(im_dac_scs),			// Buffer input
	.T(1'b0)				// 3-state enable input, high=input, low=output
);

endmodule