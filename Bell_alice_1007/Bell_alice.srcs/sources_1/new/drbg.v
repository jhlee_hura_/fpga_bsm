`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: HURA
// Engineer: YUN Hojun (yhj@hura.co.kr)
//
// Create Date: 2021/09/09
// Design Name: DRBG_bit_generation
// Module Name: qkd_alice_drbg_bit_gen
// Project Name:
// Target Devices: xc7k160tffg676-1
// Tool Versions: Vivado 2018.1
// Description:
//
// Dependencies:
//
// Revision: 
// Additional Comments: "Logic will get you from A to B. Imagination will take you everywhere." - Albert Einstein
//
// Copyright 2021, All rights reserved by HURA
//////////////////////////////////////////////////////////////////////////////////


module drbg (

//------------------------------------COMMON-------------------------------------
	input wire         clk,
	input wire         sclr,  // if "sclr_00" is on or "drbg_mode" is off, sclr is on
    input wire         drbg_mode,
//-------------------------------------------------------------------------------


//---------------------------------ENTROPY_GEN-----------------------------------
    input wire         spd_trig,
    input wire         spd_trig_valid,
//-------------------------------------------------------------------------------


//----------------------------------ETP_FIFO-------------------------------------
	output wire        empty_etp_fifo,
//-------------------------------------------------------------------------------


//--------------------------------ETP_VERI_FIFO----------------------------------
    input wire         rd_en_etp_veri,
	output wire        [31:0] o_etp_veri,
	output wire        full_etp_veri,
//-------------------------------------------------------------------------------


//--------------------------------DRBG_VERI_FIFO---------------------------------
	input wire         rd_en_drbg_veri,    
    output wire        [31:0] o_drbg_veri,
    output wire        full_drbg_veri,
//-------------------------------------------------------------------------------


//---------------------ENTROPY_GEN & DRBG output valid count---------------------
    output reg [31:0]  o_etp_gen_valid_cnt,  
    output reg [31:0]  oRandomNoValid_cnt,  
//-------------------------------------------------------------------------------


//--------------------------------DEMUX_AND_FIFO---------------------------------
	output wire        ready_b_demux,
//-------------------------------------------------------------------------------
	

//-----------------------------------DWDC_GROUP----------------------------------
    input wire         rd_en,	
	output wire        o_dwdc_01,
	output wire        o_dwdc_02,
	output wire        o_dwdc_03,
	output wire        o_dwdc_04,
	output wire        o_dwdc_05,
	output wire        o_dwdc_06,
	output wire        o_dwdc_07,
	output wire        o_dwdc_08,
	output wire        o_dwdc_09,
	output wire        o_dwdc_10,
	output wire        o_valid_dwdc_01,
	output wire        o_valid_dwdc_02,
	output wire        o_valid_dwdc_03,
	output wire        o_valid_dwdc_04,
	output wire        o_valid_dwdc_05,
	output wire        o_valid_dwdc_06,
	output wire        o_valid_dwdc_07,
	output wire        o_valid_dwdc_08,
	output wire        o_valid_dwdc_09,
	output wire        o_valid_dwdc_10
//-------------------------------------------------------------------------------

);


wire [255:0] o_etp_gen;
wire o_etp_gen_valid;

wire entropy_req;
wire [255:0] o_etp_fifo;
wire full_etp_fifo;

wire [31:0] o_etp_gen_32;
wire o_etp_gen_32_valid;

wire [255:0] oRandomNo;
wire oRandomNoValid;

wire [31:0] o_drbg_gen_32;
wire o_drbg_gen_32_valid;

wire [255:0] o_fifo_01;
wire [255:0] o_fifo_02;
wire [255:0] o_fifo_03;
wire [255:0] o_fifo_04;
wire [255:0] o_fifo_05;
wire [255:0] o_fifo_06;
wire [255:0] o_fifo_07;
wire [255:0] o_fifo_08;
wire [255:0] o_fifo_09;
wire [255:0] o_fifo_10;
wire full_demux;

wire i_valid_dwdc_01;
wire i_valid_dwdc_02;
wire i_valid_dwdc_03;
wire i_valid_dwdc_04;
wire i_valid_dwdc_05;
wire i_valid_dwdc_06;
wire i_valid_dwdc_07;
wire i_valid_dwdc_08;
wire i_valid_dwdc_09;
wire i_valid_dwdc_10;

wire ready_dwdc_01;
wire ready_dwdc_02;
wire ready_dwdc_03;
wire ready_dwdc_04;
wire ready_dwdc_05;
wire ready_dwdc_06;
wire ready_dwdc_07;
wire ready_dwdc_08;
wire ready_dwdc_09;
wire ready_dwdc_10;


//---------------------ENTROPY_GEN & DRBG output valid count---------------------
reg [31:0] clk_cnt;
reg [31:0] o_etp_gen_valid_cnt_cnt_tmp;
reg [31:0] oRandomNoValid_cnt_tmp;

always @(posedge clk) begin
    if (sclr) begin
        clk_cnt                     <= 'b0;
        o_etp_gen_valid_cnt         <= 'b0;
        o_etp_gen_valid_cnt_cnt_tmp <= 'b0;
        oRandomNoValid_cnt          <= 'b0;
        oRandomNoValid_cnt_tmp      <= 'b0;
    end
    else begin
        clk_cnt                     <= (clk_cnt >= (`SIGCLKPERSEC-1'b1)) ? 'b0 : clk_cnt + 1'b1;
        
        o_etp_gen_valid_cnt         <= (clk_cnt == 'b0) ? o_etp_gen_valid_cnt_cnt_tmp : o_etp_gen_valid_cnt;
        o_etp_gen_valid_cnt_cnt_tmp <= (clk_cnt == 'b0) ? 'b0 :
                                         (o_etp_gen_valid) ? o_etp_gen_valid_cnt_cnt_tmp + 1'b1 : o_etp_gen_valid_cnt_cnt_tmp;
                                               
        oRandomNoValid_cnt          <= (clk_cnt == 'b0) ? oRandomNoValid_cnt_tmp : oRandomNoValid_cnt;
        oRandomNoValid_cnt_tmp      <= (clk_cnt == 'b0) ? 'b0 :
                                        (oRandomNoValid) ? oRandomNoValid_cnt_tmp + 1'b1 : oRandomNoValid_cnt_tmp; 
                                                     
    end
    end
//-------------------------------------------------------------------------------


//---------------------------------ENTROPY_GEN-----------------------------------
entropy_gen entropy_gen (
    .clk                  (clk),            // I   <--  100MHz
    .sclr                 (sclr),           // I
    .drbg_mode            (drbg_mode),      // I
    .spd_trig	          (spd_trig),	    // I
    .spd_trig_valid       (spd_trig_valid), // I
    .o_etp_gen            (o_etp_gen),      // O
    .o_etp_gen_valid      (o_etp_gen_valid) // O
);
//-------------------------------------------------------------------------------


//----------------------------------ETP_FIFO-------------------------------------
fifo_C_D_256x32 etp_fifo (
  .clk      (clk),              // I
  .srst		(sclr),		       	// I
  .din		(o_etp_gen),		// I
  .wr_en	(o_etp_gen_valid),	// I
  .rd_en	(entropy_req),		// I
  .dout		(o_etp_fifo),		// O
  .empty	(empty_etp_fifo),	// O
  .full		(full_etp_fifo)		// O	
);
//-------------------------------------------------------------------------------


//-----------------------------DWDC_FOR_ETP_VERI---------------------------------
datawidth_256to32_downconverter dwdc_for_etp_veri (
	.clk(clk),				         // I
	.sclr(sclr),				     // I 
	.din(o_etp_gen),		         // I [255:0]
	.din_valid(o_etp_gen_valid),	 // I
	.din_ready(),		             // O

	.dout(o_etp_gen_32),			 // O [31:0]
	.dout_valid(o_etp_gen_32_valid), // O
	.dout_ready(1'b1)		         // I
);
//-------------------------------------------------------------------------------


//--------------------------------ETP_VERI_FIFO----------------------------------
fifo_C_D_32x16 etp_veri_fifo (
  .clk      (clk),                // I
  .srst		(sclr),			      // I
  .din		(o_etp_gen_32),		  // I
  .wr_en	(o_etp_gen_32_valid), // I
  .rd_en	(rd_en_etp_veri),	  // I
  .dout		(o_etp_veri),	      // O
  .empty	(),	                  // O
  .full		(full_etp_veri)		  // O	
);
//-------------------------------------------------------------------------------


//----------------------------------DRBG_256N------------------------------------
drbgCore_256N drbgCore_256N (
    .iClk		        (clk),	           // I
    .iReset_n		    (~sclr),	       // I
    .iEnable		    (1'b1),       	   // I
    .iExternEntropyEn	(~empty_etp_fifo), // I
    .iEntropy		    (o_etp_fifo),	   // I
    .inNonce		    (128'h0),	       // I
    .inPerString		(256'h0),	       // I
    .iAdditional		(256'h0),	       // I
    .oEntropyReq		(entropy_req),	   // O
    .oRandomNo          (oRandomNo),       // O
    .oRandomNoValid		(oRandomNoValid)   // O
);
//-------------------------------------------------------------------------------


//------------------------------DWDC_FOR_DRBG_VERI-------------------------------
datawidth_256to32_downconverter dwdc_for_drbg_veri (
	.clk(clk),				          // I
	.sclr(sclr),				      // I 
	.din(oRandomNo),		          // I [255:0]
	.din_valid(oRandomNoValid),	      // I
	.din_ready(),		              // O

	.dout(o_drbg_gen_32),			  // O [31:0]
	.dout_valid(o_drbg_gen_32_valid), // O
	.dout_ready(1'b1)		          // I
);
//-------------------------------------------------------------------------------


//--------------------------------DRBG_VERI_FIFO---------------------------------
fifo_C_B_32x16384 drbg_veri_fifo (
  .clk      (clk),                 // I
  .srst		(sclr),			       // I
  .din		(o_drbg_gen_32),	   // I
  .wr_en	(o_drbg_gen_32_valid), // I
  .rd_en	(rd_en_drbg_veri),	   // I
  .dout		(o_drbg_veri),	       // O
  .empty	(),	                   // O
  .full		(full_drbg_veri)	   // O	
);
//-------------------------------------------------------------------------------


//--------------------------------DEMUX_AND_FIFO---------------------------------
demux_and_fifo demux_and_fifo (
    .clk		       (clk),   	     // I
    .sclr              (sclr),           // I
    .entropy_req       (entropy_req),    // I
    .RandomNo          (oRandomNo),      // I
    .RandomNoValid     (oRandomNoValid), // I
    
    .ready_dwdc_01   (ready_dwdc_01),    // I
    .ready_dwdc_02   (ready_dwdc_02),    // I
    .ready_dwdc_03   (ready_dwdc_03),    // I
    .ready_dwdc_04   (ready_dwdc_04),    // I
    .ready_dwdc_05   (ready_dwdc_05),    // I
    .ready_dwdc_06   (ready_dwdc_06),    // I
    .ready_dwdc_07   (ready_dwdc_07),    // I
    .ready_dwdc_08   (ready_dwdc_08),    // I
    .ready_dwdc_09   (ready_dwdc_09),    // I
    .ready_dwdc_10   (ready_dwdc_10),    // I
    
    .full              (full_demux),     // O    
    .ready_b           (ready_b_demux),  // O
    
    .dout_01           (o_fifo_01),      // O
    .dout_02           (o_fifo_02),      // O
    .dout_03           (o_fifo_03),      // O
    .dout_04           (o_fifo_04),      // O
    .dout_05           (o_fifo_05),      // O
    .dout_06           (o_fifo_06),      // O
    .dout_07           (o_fifo_07),      // O
    .dout_08           (o_fifo_08),      // O
    .dout_09           (o_fifo_09),      // O
    .dout_10           (o_fifo_10),      // O    
    
    .i_valid_dwdc_01  (i_valid_dwdc_01), // O
    .i_valid_dwdc_02  (i_valid_dwdc_02), // O
    .i_valid_dwdc_03  (i_valid_dwdc_03), // O
    .i_valid_dwdc_04  (i_valid_dwdc_04), // O
    .i_valid_dwdc_05  (i_valid_dwdc_05), // O
    .i_valid_dwdc_06  (i_valid_dwdc_06), // O
    .i_valid_dwdc_07  (i_valid_dwdc_07), // O
    .i_valid_dwdc_08  (i_valid_dwdc_08), // O
    .i_valid_dwdc_09  (i_valid_dwdc_09), // O
    .i_valid_dwdc_10  (i_valid_dwdc_10)  // O
);
//-------------------------------------------------------------------------------


//-----------------------------------DWDC_GROUP----------------------------------
dwdc_group dwdc_group (
    .clk              (clk),        // I
    .sclr             (sclr),       // I
    
    .din_01           (o_fifo_01),  // I
    .din_02           (o_fifo_02),  // I
    .din_03           (o_fifo_03),  // I
    .din_04           (o_fifo_04),  // I
    .din_05           (o_fifo_05),  // I
    .din_06           (o_fifo_06),  // I
    .din_07           (o_fifo_07),  // I
    .din_08           (o_fifo_08),  // I
    .din_09           (o_fifo_09),  // I
    .din_10           (o_fifo_10),  // I
    
    .i_valid_dwdc_01  (i_valid_dwdc_01), // I
    .i_valid_dwdc_02  (i_valid_dwdc_02), // I
    .i_valid_dwdc_03  (i_valid_dwdc_03), // I
    .i_valid_dwdc_04  (i_valid_dwdc_04), // I
    .i_valid_dwdc_05  (i_valid_dwdc_05), // I
    .i_valid_dwdc_06  (i_valid_dwdc_06), // I
    .i_valid_dwdc_07  (i_valid_dwdc_07), // I
    .i_valid_dwdc_08  (i_valid_dwdc_08), // I
    .i_valid_dwdc_09  (i_valid_dwdc_09), // I
    .i_valid_dwdc_10  (i_valid_dwdc_10), // I
    
    .dout_ready_01    (~ready_b_demux && rd_en),   // I
    .dout_ready_02    (~ready_b_demux && rd_en),   // I
    .dout_ready_03    (~ready_b_demux && rd_en),   // I
    .dout_ready_04    (~ready_b_demux && rd_en),   // I
    .dout_ready_05    (~ready_b_demux && rd_en),   // I
    .dout_ready_06    (~ready_b_demux && rd_en),   // I
    .dout_ready_07    (~ready_b_demux && rd_en),   // I
    .dout_ready_08    (~ready_b_demux && rd_en),   // I
    .dout_ready_09    (~ready_b_demux && rd_en),   // I
    .dout_ready_10    (~ready_b_demux && rd_en),   // I
    
    .ready_dwdc_01   (ready_dwdc_01),      // O
    .ready_dwdc_02   (ready_dwdc_02),      // O
    .ready_dwdc_03   (ready_dwdc_03),      // O
    .ready_dwdc_04   (ready_dwdc_04),      // O
    .ready_dwdc_05   (ready_dwdc_05),      // O
    .ready_dwdc_06   (ready_dwdc_06),      // O
    .ready_dwdc_07   (ready_dwdc_07),      // O 
    .ready_dwdc_08   (ready_dwdc_08),      // O
    .ready_dwdc_09   (ready_dwdc_09),      // O
    .ready_dwdc_10   (ready_dwdc_10),      // O
  
    .o_dwdc_01        (o_dwdc_01),         // O
    .o_dwdc_02        (o_dwdc_02),         // O
    .o_dwdc_03        (o_dwdc_03),         // O
    .o_dwdc_04        (o_dwdc_04),         // O
    .o_dwdc_05        (o_dwdc_05),         // O
    .o_dwdc_06        (o_dwdc_06),         // O
    .o_dwdc_07        (o_dwdc_07),         // O
    .o_dwdc_08        (o_dwdc_08),         // O
    .o_dwdc_09        (o_dwdc_09),         // O
    .o_dwdc_10        (o_dwdc_10),         // O
    
    .o_valid_dwdc_01   (o_valid_dwdc_01),  // O
    .o_valid_dwdc_02   (o_valid_dwdc_02),  // O
    .o_valid_dwdc_03   (o_valid_dwdc_03),  // O
    .o_valid_dwdc_04   (o_valid_dwdc_04),  // O
    .o_valid_dwdc_05   (o_valid_dwdc_05),  // O
    .o_valid_dwdc_06   (o_valid_dwdc_06),  // O
    .o_valid_dwdc_07   (o_valid_dwdc_07),  // O
    .o_valid_dwdc_08   (o_valid_dwdc_08),  // O
    .o_valid_dwdc_09   (o_valid_dwdc_09),  // O
    .o_valid_dwdc_10   (o_valid_dwdc_10)   // O
);
//-------------------------------------------------------------------------------



endmodule
