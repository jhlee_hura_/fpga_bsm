`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: HURA
// Engineer: LEE Seong Yun (sylee@hura.co.kr)
//
// Create Date: 2020/11/03 11:05:02
// Design Name: BSM Alice/Bob
// Module Name: BSM_Alice/Bob
// Project Name: BSM
// Target Devices: Kintex 7 160T
// Tool Versions: Vivado 2018.1
// Description:
//
// Dependencies:
//
// Revision:
// 		build. 1 (2019/01/04)
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////
module Bell_Alice(

	//------------- PCI Express ---------------
 	input	wire        pcie_ref_clk_p,
	input   wire        pcie_ref_clk_n,
	output  wire [3:0]  pci_exp_txp,
	output  wire [3:0]  pci_exp_txn,
	input   wire [3:0]  pci_exp_rxp,
	input   wire [3:0]  pci_exp_rxn,

 	input	wire        OCXO_p,
    input   wire        OCXO_n,

	// to CON1


	output	wire        CON1B1_p0,
    output  wire        CON1B1_n0,
    input    wire       CON1B1_p1,
    input    wire       CON1B1_n1,
    output   wire       CON1B1_p2,
    output   wire       CON1B1_n2,       
	output	wire		CON1B2_p,
	output	wire		CON1B2_n,
	inout	wire [21:0]	CON1B3,


	// to CON2
	input	 wire    CON2B1_p0,
    input    wire    CON2B1_n0,
    output   wire    CON2B1_p1,
    output   wire    CON2B1_n1,
    input    wire    CON2B1_p2,
    input    wire    CON2B1_n2, 
    output   wire    CON2B2_p,
    output   wire    CON2B2_n,

	inout	wire [21:0]	CON2B3,

	// to CON3
	output	 wire    CON3B1_p0,
    output    wire    CON3B1_n0,
    input   wire    CON3B1_p1,
    input   wire    CON3B1_n1,
    output    wire    CON3B1_p2,
    output    wire    CON3B1_n2, 
    output   wire    CON3B2_p,
    output   wire    CON3B2_n,

	inout	wire [21:0]	CON3B3,

	// to CON4
	output	 wire    CON4B1_p0,
    output    wire    CON4B1_n0,
    input   wire    CON4B1_p1,
    input   wire    CON4B1_n1,
    output    wire    CON4B1_p2,
    output    wire    CON4B1_n2, 
    output   wire    CON4B2_p,
    output   wire    CON4B2_n,

	inout	wire [21:0]	CON4B3,

	// to CON5
	output	wire 	     CON5B1_p,
    output  wire        CON5B1_n,
    output  wire        CON5B2_p,
    output  wire        CON5B2_n,
    inout   wire [15:0] CON5B3,
    
	// to CON6
	input	wire [1:0]	 CON6B1_p,
    input   wire [1:0]  CON6B1_n,
    output  wire        CON6B2_p,
    output  wire        CON6B2_n,
    inout   wire [15:0] CON6B3,
    
	// to CON7
	output	wire		 CON7B1_p,
    output  wire        CON7B1_n,
    output  wire        CON7B2_p,
    output  wire        CON7B2_n,
    inout   wire [15:0] CON7B3,
    
	// to CON8
	output	wire		CON8B1_p0,
    output  wire       CON8B1_n0,
    output    wire     CON8B1_p2,
    output  wire       CON8B1_n2,     
    inout   wire [15:0] CON8B3,
    
	// to CON9
	
	
    output  wire        CON9B1_p,
    output  wire        CON9B1_n,
    output  wire       ADC_A_CLK_p,
    output  wire        ADC_A_CLK_n,     
 //   output  wire        CON9B2_p,
 //   output  wire        CON9B2_n,
    inout   wire [15:0] CON9B3,



	// to CON10
    output  wire        CON10B1_p,
    output  wire        CON10B1_n,
    output  wire        CON10B2_p,
    output  wire        CON10B2_n,
    inout   wire [15:0] CON10B3,

	// Fan control
	output	wire		FAN_CTRL,       // o
	output	wire		FAN_CTRL_MAIN,  // o

	// Temp. Sensor (LM 70)
	output	wire		TMP_MOSI,       // o
	input	wire		TMP_MISO,       // o
	output	wire		TMP_SCS,        // o
	output	wire		TMP_SCK,        // o
                                        // o
	// LED
	output 	wire		LED_FRONT1,		// ok
	output 	wire		LED_FRONT2,		// ok
	output 	wire		LED_FRONT3,		// ok

	// Array LED
	output	wire [7:0]	GPIO_LED,		// ok

	// 10 MHz Clk
	output	wire		F_CLK_3_3,	// alice	// ok
//	input	wire		F_CLK_3_3,	// bob

	output 	wire [7:0]	GPIO_3_3_PIN,		// ok
	output	wire [4:0]	GPIO_3_3_SMA,       // ok
	output 	wire [9:0]	GPIO_1_8_PIN,       // ok
	output  wire       GPIO_1_8_SMA_REF,
	output	wire [3:0]	GPIO_1_8_SMA,       // ok

	output	wire		PD_OSC_EN,					// ok
	input	wire		PD_OSC_p,		// 100MHz	// ok
	input	wire		PD_OSC_n,					// ok

	// to CPLD
	input	wire [7:0]	FGPIO
);

// (* mark_debug = "true" *)
// ---------------------------------------------------------------------------------
// do not assignd pins
assign GPIO_3_3_PIN[7:0] = 8'b0;

//assign GPIO_1_8_PIN[9:0] = 10'b0;


// ---------------------------------------------------------------------------------
// No change Pins
assign PD_OSC_EN = 1'b1;




// ---------------------------------------------------------------------------------
// Clock
// ---------------------------------------------------------------------------------
wire		pcie_clk;
wire		sys_clk;
wire	    pci_sig_clk;

wire		clk_sys_locked;
wire		clk_serdes_locked;

clk_wiz_0 clk_sys (
	.clk_out1(sys_clk),				// O
	.clk_out2(pci_sig_clk),				// O

	.locked(clk_sys_locked),		// O
	.clk_in1(pcie_clk));			// I

 // ---------------------------------------------------------------------------------
// Reset buffer
// ---------------------------------------------------------------------------------
wire     grst;
wire  back_rst_n;
IBUF    hw_rst_n_ibuf (.O(back_rst_n), .I(FGPIO[0]));

pls_expander #(
    .COUNT(10),
    .POLARITY(1)
) grst_exp(
        .sin(~back_rst_n),        // ~(fpga_rst_n & back_rst_n)
        .clk(sys_clk),
        .sout(grst)
);  
// ---------------------------------------------------------------------------------
// Sig clk MUX
// ---------------------------------------------------------------------------------
wire    clk_optic;
wire    clk_200;
wire    clk_600;
wire    sig_clk;
wire    clk_optic_locked;
reg     clk_in_sel = 1'b1;

wire    clk_sel_rst;
wire	clk_sig_locked;

clk_wiz_1 clk_sig (
    .clk_in2(clk_optic),            // I
    .clk_in_sel(clk_in_sel),        // I 1: clk_in1, 0: clk_in2
    .clk_out1(sig_clk),             // O
	.clk_out2(clk_200),				// O
    .clk_out3(clk_600),             // O
    .reset(grst | clk_sel_rst),     // I
    .locked(clk_sig_locked),        // O
    .clk_in1(pci_sig_clk));         // I
    
 reg     clk_in_sel_old;

    always @(posedge sys_clk) begin
        if (grst) begin
            clk_in_sel <= 1'b1;
            clk_in_sel_old <= 1'b1;
        end
        else begin
            clk_in_sel <= (clk_optic_locked) ? 1'b0 : 1'b1;
            clk_in_sel_old <= clk_in_sel;
        end
    end
    
    pls_expander #(
        .COUNT(12),
        .POLARITY(1)
    ) c2h_0_rst_ext(
        .sin(clk_in_sel != clk_in_sel_old),
        .clk(sys_clk),
        .sout(clk_sel_rst)
    );   

    
// --------------------------------------------------------------------------------
// 10M Clock Generation
// ---------------------------------------------------------------------------------
reg [3:0]	sig_clk_cnt = 'b0;
reg 		sig_clk_10 = 1'b0;
reg			sig_clk_10M_pulse = 1'b0;


always @(posedge sig_clk) begin
	sig_clk_cnt <= (sig_clk_cnt == 4'd9) ? 4'b0 : sig_clk_cnt + 1'b1;

	if (sig_clk_cnt == 4'b0) begin
		sig_clk_10M_pulse <= 1'b1;
		sig_clk_10 <= 1'b1;
	end
	else if (sig_clk_cnt == 4'd5) begin
		sig_clk_10M_pulse <= 1'b0;
		sig_clk_10 <= 1'b0;
	end
	else begin
		sig_clk_10 <= sig_clk_10;
		sig_clk_10M_pulse <= 1'b0;
	end
end

// ---------------------------------------------------------------------------------
// sync for differentr clock domain
// ---------------------------------------------------------------------------------
reg			sig_sync = 1'b0;
reg [6:0]	sig_clk_sync_cnt = 'b0;
always @(posedge sig_clk) begin
	sig_clk_sync_cnt <= (sig_clk_sync_cnt == 7'd79) ? 7'b0 : sig_clk_sync_cnt + 1'b1;
	sig_sync <= (sig_clk_sync_cnt == 7'b0) ? 1'b1 : 1'b0;
end

// ---------------------------------------------------------------------------------
// Data generation initialization signal
//  When the pulse width and delay time are changed, the data is created again.
// ---------------------------------------------------------------------------------
reg shape_ld;

wire a1_shape_load;
wire a2_shape_load;
wire a3_shape_load;
wire a4_shape_load;
wire a5_shape_load;
wire a9_shape_load;
wire a10_shape_load;
(* mark_debug = "true" *)wire op_ready;

wire shape_load;
assign shape_load = a2_shape_load | a3_shape_load |a4_shape_load | a5_shape_load | a9_shape_load | a10_shape_load  | op_ready;

reg shape_load_r;
always @(posedge sig_clk) begin
	shape_load_r <=	(shape_load) ? 1'b1 :
					(sig_sync) ? 1'b0 :
					shape_load_r;

	shape_ld <= shape_load_r & sig_sync;
end




reg shape_ld_trig;
wire shape_load_trig;
assign shape_load_trig = a1_shape_load ;

reg shape_load_trig_r;
always @(posedge sig_clk) begin
	shape_load_trig_r <=	(shape_load_trig) ? 1'b1 :
					(sig_sync) ? 1'b0 :
					shape_load_trig_r;

	shape_ld_trig <= shape_load_trig_r & sig_sync;
end



// ---------------------------------------------------------------------------------
// LED Buffers
// ---------------------------------------------------------------------------------
wire [7:0]	array_led;
wire		f_led1;
wire		f_led2;
wire		f_led3;
wire		pcie_link_up;
reg			trig_valid;
//wire		trig_led;

assign GPIO_LED = ~array_led[7:0];

OBUF   front_led_1_obuf (.O(LED_FRONT1), .I(~f_led1));		// FPGA & DDR init & DDR POWER
OBUF   front_led_2_obuf (.O(LED_FRONT2), .I(~f_led2));		// user design part
OBUF   front_led_3_obuf (.O(LED_FRONT3), .I(~f_led3));		// Fault user design part


assign array_led[4:3] = 'b0;
assign array_led[5] = clk_sig_locked;
assign array_led[6] = clk_optic_locked;
assign array_led[7] = 1'b1;

reg [25:0]	clock_count='b0;
always @(posedge sys_clk)
	clock_count <= clock_count + 26'b1;
assign array_led[0]	= clock_count[25];		// 125MHz

reg [25:0]	sig_clk_count='b0;
always @(posedge sig_clk)
	sig_clk_count <=sig_clk_count + 26'b1;
assign array_led[1]	= sig_clk_count[25];

reg [25:0]	clk_200_count='b0;
always @(posedge clk_200)
	clk_200_count <=clk_200_count + 26'b1;
assign array_led[2]	= clk_200_count[25];


wire led_test;
wire led1_on;
wire led2_on;
wire led3_on;
assign f_led1 = led1_on; //(clk_sys_locked && clk_sig_locked);
assign f_led2 = (led_test) ? led2_on : clock_count[25];	//trig_led;
assign f_led3 = (led_test) ? led3_on : pcie_link_up;

// ---------------------------------------------------------------------------------
//	board peripheral (lm 70)
// ---------------------------------------------------------------------------------
wire [15:0] lm70_spi_rd;
wire		lm70_spi_valid;
wire		lm70_din_ready;
wire [15:0]	fan_high_dura;
wire [15:0]	fan_low_dura;
wire [15:0]	fan1_high_dura;
wire [15:0]	fan1_low_dura;

board_perip_intf board_perip_intf(
	.clk			(sys_clk),
	.sclr			(grst),

	// LM70 Temp Sensor
	.lm70_sck		(TMP_SCK),			// O
	.lm70_scs		(TMP_SCS),			// O
	.lm70_sdo		(TMP_MOSI),			// O
	.lm70_sdi		(TMP_MISO),			// I

	.lm70_din_valid	(lm70_spi_valid),	// I
	.lm70_din_ready	(lm70_din_ready),	// O
	.lm70_dout		(lm70_spi_rd),		// O [15:0]

	// FAN MAIN Control
	.fan_main_ctrl		(FAN_CTRL_MAIN),	// O

	.fan_main_high_dura	(fan1_high_dura),	// I [15:0]
	.fan_main_low_dura	(fan1_low_dura),	// I [15:0]

	// FAN Control
	.fan_ctrl		(FAN_CTRL),			// O

	.fan_high_dura	(fan_high_dura),	// I [15:0]
	.fan_low_dura	(fan_low_dura)		// I [15:0]
);

// ---------------------------------------------------------------------------------
// Data generation module for operation mode
// ---------------------------------------------------------------------------------

wire		a2_spd_trig;
wire		a2_spd_trig_valid;

(* mark_debug = "true" *)wire		a6_start_flag;
(* mark_debug = "true" *)wire		op_stop;
(* mark_debug = "true" *)wire		op_busy;
(* mark_debug = "true" *)wire     op_fake_start;
wire     [20:0] op_snapshot;
wire [31:0]	seed;
wire		seed_valid;
wire [7:0]	thres1;
wire [7:0]	thres2;

// DRBG --------------------------------
wire drbg_mode;
wire [31:0] etp_cnt;
wire [31:0] drbg_cnt;

wire empty_drbg_fifo_group;
wire empty_etp_fifo;

wire rd_en_etp_veri;
wire [31:0] o_etp_veri;
wire full_etp_veri;
wire rd_en_drbg_veri;
wire [31:0] o_drbg_veri;
wire full_drbg_veri;
// -------------------------------------

wire		a_64b1_test_mode;
wire [31:0]	a_64b1_s0_test_data;
wire [31:0]	a_64b1_s1_test_data;
wire		a_64b2_d_test_mode;
wire		a_64b2_b_test_mode;
wire [31:0]	a_64b2_b_test_data;
wire [31:0]	a_64b2_d_test_data;
wire		a_64b3_d_test_mode;
wire		a_64b3_b_test_mode;
wire [31:0]	a_64b3_b_test_data;
wire [31:0]	a_64b3_d_test_data;

(* mark_debug = "true" *)wire		a_32b1_test_mode;
(* mark_debug = "true" *)wire [31:0]	a_32b1_test_data;
(* mark_debug = "true" *)wire		a_32b2_test_mode;
(* mark_debug = "true" *)wire [31:0]	a_32b2_test_data;
(* mark_debug = "true" *)wire [63:0]	o_64b1_s;
(* mark_debug = "true" *)wire [63:0] o_64b2;
(* mark_debug = "true" *)wire [63:0] o_64b3;
(* mark_debug = "true" *)wire [31:0] o_32b1;
(* mark_debug = "true" *)wire [31:0] o_32b2;
(* mark_debug = "true" *)wire		sig_valid;
(* mark_debug = "true" *)wire		sig_start;
(* mark_debug = "true" *)wire		sig_last;
(* mark_debug = "true" *)wire		data_require;

(* mark_debug = "true" *)wire		a3_sig_ready;
(* mark_debug = "true" *)wire		a4_sig_ready;
(* mark_debug = "true" *)wire		a5_sig_ready;
wire		a9_sig_ready;
(* mark_debug = "true" *)wire		a10_sig_ready;

wire [13:0]         sig_number_128b;   

qkd_operation_5 qkd_operation_5 (
	.clk			(sig_clk),			// I
    .sclr            (grst),            // I
    
    .o_64b1_s        (o_64b1_s),        // O [63:0] -> IM4
    .o_64b2          (o_64b2),          // O [63:0]   -> IM3
    .o_64b3          (o_64b3),          // O [63:0]   -> IM5
    .o_32b1          (o_32b1),          // O [31:0]  -> PM9
    .o_32b2          (o_32b2),          // O [31:0]   -> PM10
    
    .o_op_run_last   (sig_last),        // O
    .o_op_run_start  (sig_start),       // O
    .dout_valid      (sig_valid),       // O
    .busy            (op_busy),         // O
    .i_data_require  (data_require),    // I
    .sig_number_128b (sig_number_128b), // O  [13:0] 
   
    .op_ready        (op_ready),                    // I
    .op_stop         (op_stop),                     // I
    .op_run          (a6_start_flag|op_fake_start), // I
    .sig_number      (op_snapshot),                 // I [20:0]
    
    //Indoor random_lsfr--------------------    
    .seed            (seed),            // I [31:0]
    .seed_valid      (seed_valid),      // I
    .thres1          (thres1),          // I [7:0]
    .thres2          (thres2),          // I [7:0]
    
    //Indoor random_drbg--------------------
    .spd_trig        (a2_spd_trig),           // I
    .spd_trig_valid  (a2_spd_trig_valid),     // I
     
    .drbg_mode       (drbg_mode),             // I
      
    .etp_valid_cnt   (etp_cnt),               // O [31:0]
    .drbg_valid_cnt  (drbg_cnt),              // O [31:0]
      
    .ready_b_demux   (empty_drbg_fifo_group), // O // if all fifos are not empty? ready_b_demux = 0
    .empty_etp_fifo  (empty_etp_fifo),        // O
    .full_etp_veri   (full_etp_veri),         // O
    .full_drbg_veri  (full_drbg_veri),        // O
             
    .rd_en_etp_veri  (rd_en_etp_veri),        // I
    .o_etp_veri      (o_etp_veri),            // O [31:0]
      
    .rd_en_drbg_veri (rd_en_drbg_veri),       // I
    .o_drbg_veri     (o_drbg_veri),           // O [31:0]
    // -------------------------------------

    //Indoor test -------------------------
    // A_SIG_TEST_MODE_REG
    ._64b1_test_mode(a_64b1_test_mode),
    // A_SIG0_TEST_DATA_REG    
    ._64b1_s0_test_data(a_64b1_s0_test_data), // I [31:0]
    // A_SIG1_TEST_DATA_REG    
    ._64b1_s1_test_data(a_64b1_s1_test_data), // I [31:0]
    
      // A_DB1_TEST_MODE_REG   
    ._64b2_d_test_mode(a_64b2_d_test_mode), // I
    ._64b2_b_test_mode(a_64b2_b_test_mode), // I    
     // A_DB1_B_TEST_DATA_REG    
    ._64b2_b_test_data(a_64b2_b_test_data), // I [31:0]
    //  A_DB1_D_TEST_DATA_REG      
    ._64b2_d_test_data(a_64b2_d_test_data), // I [31:0]

 	// A_DB2_TEST_MODE_REG   
    ._64b3_d_test_mode(a_64b3_d_test_mode), // I
    ._64b3_b_test_mode(a_64b3_b_test_mode), // I   
 	// A_DB2_B_TEST_DATA_REG    
    ._64b3_b_test_data(a_64b3_b_test_data), // I [31:0]
	// A_DATA_TEST_DATA_REG    
    ._64b3_d_test_data(a_64b3_d_test_data), // I [31:0]

 	// A_PM_TEST_MODE_REG   
    ._32b1_test_mode(a_32b1_test_mode), // I
 	// A_PM_TEST_DATA_REG    
    ._32b1_test_data(a_32b1_test_data), // I [31:0]
	
 	// A_L_TEST_MODE_REG   
    ._32b2_test_mode(a_32b2_test_mode), // I
 	// A_L_TEST_DATA_REG    
    ._32b2_test_data(a_32b2_test_data) // I [31:0]    
    
);

(* mark_debug = "true" *)reg sig_start_flag;
always @(posedge sig_clk) begin
	sig_start_flag <= 	(sig_start) ? 1'b1 :
						(sig_clk_10M_pulse) ? 1'b0 :
						sig_start_flag;

end

// ---------------------------------------------------------------------------------
//	Error Check

assign data_require = a9_sig_ready;

reg [5:0] 	ready_err_flag;
wire		ready_err_flag_clear;
wire [4:0]	ready_flag;
wire        ready_sync_err_flag;
assign ready_flag = {a3_sig_ready,a4_sig_ready,a5_sig_ready,a9_sig_ready,a10_sig_ready};
assign ready_sync_err_flag = ((ready_flag == 5'b11111) || (ready_flag == 5'b0)) ? 1'b0 : 1'b1;
always @(posedge sig_clk) begin
	if (grst) begin
		ready_err_flag <= 'b0;
	end
	else begin
		if (ready_err_flag_clear) begin
			ready_err_flag <= 'b0;
		end
		else begin
			ready_err_flag[0] <= (ready_sync_err_flag) ? 1'b1 : ready_err_flag[0];
			ready_err_flag[5:1] <= (ready_sync_err_flag) ? ready_flag : ready_err_flag[5:1];
		end
	end
end

// ---------------------------------------------------------------------------------
//	SPAD Trig signal for log
// ---------------------------------------------------------------------------------
wire [1:0] a_trig_type ;

wire		a2_mask_10M;
(* mark_debug = "true" *)wire [1:0] a3_bit;
(* mark_debug = "true" *)wire       a3_bit_valid;
(* mark_debug = "true" *)wire [1:0] a4_bit;
(* mark_debug = "true" *)wire       a4_bit_valid;
wire [1:0] a5_bit;
wire       a5_bit_valid;

wire       a9_bit;
wire       a9_bit_valid;

wire       a10_bit;
wire       a10_bit_valid;

wire [31:0]log32;
wire       log32_valid;
wire [7:0] emp_log;
(* mark_debug = "true" *)wire [127:0] log128;
(* mark_debug = "true" *)wire       log128_valid;
// Reg for sig_clk
(* mark_debug = "true" *)wire fifo_rst;
// 128b_last signal pci clk
 (* mark_debug = "true" *)reg [20:0] cnt_a3_bit;
 //(* mark_debug = "true" *)wire din_128b_last;
always @(posedge sig_clk) begin
	if (fifo_rst) begin
        cnt_a3_bit <=0;
	end
	else begin
		if (a3_bit_valid) begin
		  cnt_a3_bit <= cnt_a3_bit+1'b1;
		end
		else begin
		  cnt_a3_bit <= cnt_a3_bit;
		end
	end
end		

log_8bit log_8bit (
	.clk		(sig_clk),		// I
	.rst		(grst || op_ready ),			// I a6_start_flag

	.din_log0	(a3_bit[0]),			// I
	.div_log0	(a3_bit_valid),		// I
	.din_log1	(a3_bit[1]),			// I
	.div_log1	(a3_bit_valid),		// I
	.din_log2	(a4_bit[0]),			// I
	.div_log2	(a3_bit_valid),		// I
	.din_log3	(a4_bit[1]),			// I
	.div_log3	(a3_bit_valid),		// I
	
	
/*
	.din_log4	(a5_bit[0]),          // I
	.div_log4	(a3_bit_valid),	// I	// a2_trig_valid : ?κΈ? ???΄μ§? κ°??₯?±. a2_trigκ°? λ©μΆμ§????€.
	.din_log5	(a5_bit[1]),				// I
	.div_log5	(a3_bit_valid),		// I
	.din_log6	(a10_bit),				// I
	.div_log6	(a3_bit_valid),		// I
	.din_log7	(a1_bit),	// I
	.div_log7	(a3_bit_valid),		// I
*/

	.emp_log	(emp_log),			// O [7:0]
	
    .do_log_16to32	 (log32),			// O [31:0]
    .dov_log_16to32  (log32_valid),        // O
    .do_log_64to128  (log128),          // O [127:0]
    .dov_log_64to128 (log128_valid)     // O
);




// START_FLAG is delayed for Trigger laser module
// ---------------------------------------------------------------------------------
wire [9:0] o_a1_delay_cnt;
wire       o_a1_delayed_flag;
pulse_delay pulse_delay_con1_trig (
    .clk(sig_clk), // I
    .sclr(grst), // I
    .i_delay_cnt(o_a1_delay_cnt), // I [9:0]
    .i_start_pulse(sig_start_flag&sig_clk_10M_pulse), // I
    .i_ref_pulse(sig_clk_10M_pulse), // I
    .o_delayed_pulse(o_a1_delayed_flag) // O

);


// ---------------------------------------------------------------------------------
//	Alice Interface
// ---------------------------------------------------------------------------------



wire 			a1_test_mode;
wire [31:0]		a1_test_data;

wire [5:0]		a1_width;
wire [7:0]     a1_add_width;
wire [6:0]		a1_i_offset_quotient;
wire [2:0]		a1_i_offset_remainder;
wire [4:0]		a1_delay_tap;

wire			a1_hmc920_en;
wire [15:0]		a1_ltc1867_spi_wr;
wire			a1_ltc1867_spi_valid;
wire			a1_ltc1867_din_ready;
wire [15:0]		a1_ltc1867_spi_rd;
wire [23:0]		a1_ad5761_dac_wr;
wire			a1_ad5761_dac_valid;
wire [23:0]		a1_ad5761_dac_rd;
wire			a1_tec_en;
wire			a1_tec_dac_clr;
wire [15:0]		a1_tec_dac_wr;
wire			a1_tec_dac_valid;

wire [23:0]     a1_dp_wr;
wire            a1_dp_valid;
wire [7:0]      a1_dp_rd;



// LD SIG
con1_intf_alice con1_intf_alice (

    .sys_clk	(sys_clk),		// I
    .sig_clk	(sig_clk),		// I       
	.clk_200	(clk_200),	// I
	.clk_600	(clk_600),	// I
	.rst(grst),					// I
	
	.CON1B1_p0	(CON1B1_p0),		// O 
    .CON1B1_n0    (CON1B1_n0),        // O 
    .CON1B1_p1  (CON1B1_p1),        // I 
    .CON1B1_n1  (CON1B1_n1),        // I 
    .CON1B1_p2  (CON1B1_p2),        // O 
    .CON1B1_n2  (CON1B1_n2),        // O      
	.CON1B2_p	(CON1B2_p),		// O
	.CON1B2_n	(CON1B2_n),		// O
	.CON1B3		(CON1B3),		// IO [21:0]

	// ---- Serdes ---

        // ---- Serdes ---
    .test_mode         (a1_test_mode),        // I             // sig_clk
    .din_test          (a1_test_data),        // I [31:0]        // sig_clk

    .shape_load        (shape_ld_trig),            // I
    .pulse_width       (a1_width),            // I [5:0]
    .pulse_add_width   (a1_add_width),        // I [7:0]
    .i_offset_quotient (a1_i_offset_quotient),   // I [7:0]    
    .i_offset_remainder(a1_i_offset_remainder), // I [2:0]

    // delay tap 78ps per tap (max 2.418ns)

    .delay_tap_in      (a1_delay_tap),        // I [4:0]

    
    .din_pulse         (sig_clk_10M_pulse),            // I
    .din_pulseadd      (o_a1_delayed_flag & sig_clk_10M_pulse),    // I

    // ---- Enable  -----
    .hmc920_en         (a1_hmc920_en),                // I
        
    // ---- LD_ADC (LTC1867) ---
    .ltc1867_din       (a1_ltc1867_spi_wr),        // I [15:0]
    .ltc1867_din_valid (a1_ltc1867_spi_valid),        // I
    .ltc1867_din_ready (a1_ltc1867_din_ready),        // O
    .ltc1867_dout      (a1_ltc1867_spi_rd),        // O [15:0]
    // ---- TEC DAC (MAX5144) ---
    .tec_shdn_b        (a1_tec_en),                // I
    .tec_clr           (a1_tec_dac_clr),            // I
    .tec_dac_din       (a1_tec_dac_wr),            // I [15:0]
    .tec_dac_din_valid (a1_tec_dac_valid),            // I
    .tec_dac_din_ready (),                            // O
    // ---- LD DAC (AD5761) ---
    .ad5761_dac_din         (a1_ad5761_dac_wr),        // I [23:0]
    .ad5761_dac_din_valid   (a1_ad5761_dac_valid),    // I
    .ad5761_dac_din_ready   (),                        // O
    .ad5761_dac_dout        (a1_ad5761_dac_rd),        // O [23:0]

    // ---- DP (AD5252) ---
    .dp_din(a1_dp_wr),                 // I [23:0]
    .dp_din_valid(a1_dp_valid),        // I
    .dp_dout(a1_dp_rd)                 // O [7:0



);

wire [31:0]    a2_spad_cnt1;
wire [31:0]    a2_spad_cnt2;
wire [31:0]    a2_spad_cnt3;
wire [31:0]    a2_spad_cnt4;

wire 			a2_test_mode;
wire [31:0]		a2_test_data;

wire [5:0]		a2_width;
wire [6:0]		a2_i_offset_quotient;
wire [2:0]		a2_i_offset_remainder;
wire [4:0]		a2_delay_tap;
wire [4:0]     a2_detect_delay_tap;
wire			a2_hmc920_en;
wire [15:0]		a2_ltc1867_spi_wr;
wire			a2_ltc1867_spi_valid;
wire			a2_ltc1867_din_ready;
wire [15:0]		a2_ltc1867_spi_rd;
wire			a2_tec_en;
wire			a2_tec_dac_clr;
wire [15:0]		a2_tec_dac_wr;
wire			a2_tec_dac_valid;
wire 			a2_sip_en;
wire [23:0]		a2_sip_dac_wr;
wire 			a2_sip_dac_valid;
wire 			a2_spd_en;
wire [23:0]		a2_spd_dac_wr;
wire 			a2_spd_dac_valid;
wire [23:0]    a2_dp_wr;
wire           a2_dp_valid;
wire [7:0]     a2_dp_rd;
// CON2 -> reg sig 
wire [31:0]		a2_spad_scnt;
wire [31:0]		a2_spad_dcnt;
wire [31:0]		a2_spad_ncnt;
wire [31:0]		a2_spad_orcnt;
assign a2_spad_scnt = a2_spad_cnt1; 
assign a2_spad_dcnt = a2_spad_cnt2; 
assign a2_spad_ncnt = a2_spad_cnt3; 
assign a2_spad_orcnt = a2_spad_cnt4;
// SPAD
con2_intf_alice con2_intf_alice (

    
    // to Con 2
    .CON2B1_p0    (CON2B1_p0),        // I 
    .CON2B1_n0  (CON2B1_n0),        // I 
    .CON2B1_p1  (CON2B1_p1),        // I 
    .CON2B1_n1  (CON2B1_n1),        // I 
    .CON2B1_p2  (CON2B1_p2),        // I 
    .CON2B1_n2  (CON2B1_n2),        // I  
    .CON2B2_p    (CON2B2_p),        // O
    .CON2B2_n    (CON2B2_n),        // O
    .CON2B3        (CON2B3),        // IO [21:0]
    
	.sys_clk	(sys_clk),		// I
    .sig_clk    (sig_clk),        // I
    .clk_200    (clk_200),    // I
    .clk_600    (clk_600),    // I
    .sig_10M(sig_clk_10M_pulse), // I    
    
    .rst        (grst),            // I    
    // A_TRIG_TYPE_REG
    .trig_type            (a_trig_type),            // I [1:0]
    .spd_trig            (a2_spd_trig),            // O
    .spd_trig_valid        (a2_spd_trig_valid),            // O
    .mask_10M            (a2_mask_10M),    // O
    
    .spad_cnt1    (a2_spad_cnt1),        // O [31:0]
    .spad_cnt2    (a2_spad_cnt2),        // O [31:0]
    .spad_cnt3    (a2_spad_cnt3),        // O [31:0]    
    .spad_cnt4  (a2_spad_cnt4),    // O [31:0]        
    
    // ---- Serdes ---
    .test_mode        (a2_test_mode),        // I             // sig_clk
    .din_test        (a2_test_data),        // I [31:0]        // sig_clk
    .shape_load        (shape_ld),            // I
    .pulse_width    (a2_width),            // I [5:0]
    .i_offset_quotient(a2_i_offset_quotient),   // I [6:0]     
    .i_offset_remainder(a2_i_offset_remainder), // I [2:0]
    
    // delay tap 78ps per tap (max 2.418ns)
    
    .delay_tap_in    (a2_delay_tap),        // I [4:0]
    .detect_delay_tap_in (a2_detect_delay_tap), // I [4:0]
    
    // ---- Enable  -----
    .hmc920_en        (a2_hmc920_en),            // I
    
    // ---- READ TEMP _ADC (LTC1867) ---
    .ltc1867_din        (a2_ltc1867_spi_wr),    // I [15:0]
    .ltc1867_din_valid    (a2_ltc1867_spi_valid),    // I
    .ltc1867_din_ready    (a2_ltc1867_din_ready),    // O
    .ltc1867_dout        (a2_ltc1867_spi_rd),    // O [15:0]
    // ---- TEC DAC (MAX5144) ---
    .tec_shdn_b            (a2_tec_en),                // I
    .tec_clr            (a2_tec_dac_clr),            // I
    .tec_dac_din        (a2_tec_dac_wr),            // I [15:0]
    .tec_dac_din_valid    (a2_tec_dac_valid),        // I
    .tec_dac_din_ready    (),                        // O
    // ---- SIP DAC (AD5060) ---
    .f_sip_disable        (a2_sip_en),            // I
    .sip_dac_din        (a2_sip_dac_wr),        // I [23:0]
    .sip_dac_din_valid    (a2_sip_dac_valid),        // I
    .sip_dac_din_ready(),                        // O
    // ---- SPD DAC (AD5752A) ---
    .spd_en                (a2_spd_en),            // I
    .spd_dac_din        (a2_spd_dac_wr),        // I [23:0]
    .spd_dac_din_valid    (a2_spd_dac_valid),        // I
    .spd_dac_din_ready(),                        // O
    
    // ---- DP (AD5252) ---
    
    .dp_din(a2_dp_wr),                 // I [23:0]
    .dp_din_valid(a2_dp_valid),        // I
    .dp_dout(a2_dp_rd)                 // O [7:0]        
);

wire 			a3_test_mode;
wire [63:0]		a3_test_data;

wire [3:0]		a3_width;
wire [8:0]		a3_offset;
wire [23:0]		a3_im_dac_wr;
wire			a3_im_dac_valid;
wire [11:0]		a3_i_dac_v0;
wire [11:0]		a3_i_dac_v1_p;
wire [11:0]		a3_i_dac_v2_p;
wire [11:0]		a3_i_dac_v3_p;
wire [11:0]		a3_i_dac_v1_n;
wire [11:0]		a3_i_dac_v2_n;
wire [11:0]		a3_i_dac_v3_n;

// IM
con3_intf_alice con3_intf_alice (
	.sys_clk	(sys_clk),		// I
	.sig_clk	(sig_clk),		// I
	.clk_200    (clk_200),       // I	
	.rst		(grst),			// I

	.CON3B1_p0	(CON3B1_p0),		// O
    .CON3B1_n0    (CON3B1_n0),        // O 
    .CON3B1_p1    (CON3B1_p1),        // I
    .CON3B1_n1  (CON3B1_n1),        // I
    .CON3B1_p2    (CON3B1_p2),        // O 
    .CON3B1_n2  (CON3B1_n2),        // O
	.CON3B2_p	(CON3B2_p),		// O
	.CON3B2_n	(CON3B2_n),		// O
	.CON3B3	  (CON3B3),		// IO [21:0]
	//MGT ??΅

	// ---- Serdes ---
	.test_mode		(a3_test_mode),		// I 			// sig_clk
	.din_test		(a3_test_data),		// I [63:0]		// sig_clk

	.shape_load		(shape_ld),		// I
	.pulse_width	(a3_width),			// I [3:0]
	.pulse_offset	(a3_offset),			// I [8:0]

	.i_dac_v0		(a3_i_dac_v0),		// I [11:0]
	.i_dac_v1_p		(a3_i_dac_v1_p),		// I [11:0]
	.i_dac_v2_p		(a3_i_dac_v2_p),		// I [11:0]
	.i_dac_v3_p		(a3_i_dac_v3_p),		// I [11:0]
    .i_dac_v1_n     (a3_i_dac_v1_n),        // I [11:0]
    .i_dac_v2_n     (a3_i_dac_v2_n),        // I [11:0]
    .i_dac_v3_n     (a3_i_dac_v3_n),        // I [11:0]

	// from random
	.din_im			(o_64b2),		// I [63:0]
	.div_im			(sig_valid),        	// I
	.dir_im			(a3_sig_ready),        		// O

	.do_im_bit		(a3_bit),					// O [1:0]
	.dov_im_bit		(a3_bit_valid),       	 		// O

	// ---- IM DAC (AD5761) ---
	.im_dac_din			(a3_im_dac_wr),			// I [23:0]
	.im_dac_din_valid	(a3_im_dac_valid),		// I
	.im_dac_din_ready	()						// O
);


wire 			a4_test_mode;
wire [63:0]		a4_test_data;

wire [3:0]		a4_width;
wire [8:0]		a4_offset;
wire [23:0]		a4_im_dac_wr;
wire			a4_im_dac_valid;
wire [11:0]		a4_i_dac_v0;
wire [11:0]		a4_i_dac_v1_p;
wire [11:0]		a4_i_dac_v2_p;
wire [11:0]		a4_i_dac_v3_p;
wire [11:0]		a4_i_dac_v1_n;
wire [11:0]		a4_i_dac_v2_n;
wire [11:0]		a4_i_dac_v3_n;
// IM 
con4_intf_alice con4_intf_alice (
	.sys_clk	(sys_clk),		// I
	.sig_clk	(sig_clk),		// I
	.clk_200    (clk_200),       // I	
	.rst		(grst),			// I

	// to Con 4
	.CON4B1_p0	(CON4B1_p0),		// O 
    .CON4B1_n0    (CON4B1_n0),        // O 
    .CON4B1_p1    (CON4B1_p1),        // I
    .CON4B1_n1  (CON4B1_n1),        // I
    .CON4B1_p2    (CON4B1_p2),        // O 
    .CON4B1_n2  (CON4B1_n2),        // O
    .CON4B2_p    (CON4B2_p),        // O
    .CON4B2_n    (CON4B2_n),        // O
	.CON4B3		(CON4B3),		// IO [21:0]
	//MGT ??΅

	// ---- Serdes ---
	.test_mode		(a4_test_mode),		// I 			// sig_clk
	.din_test		(a4_test_data),		// I [63:0]		// sig_clk

	.shape_load		(shape_ld),		// I
	.pulse_width	(a4_width),			// I [3:0]
	.pulse_offset	(a4_offset),			// I [8:0]

	.i_dac_v0		(a4_i_dac_v0),		// I [11:0]
	.i_dac_v1_p		(a4_i_dac_v1_p),		// I [11:0]
	.i_dac_v2_p		(a4_i_dac_v2_p),		// I [11:0]
	.i_dac_v3_p		(a4_i_dac_v3_p),		// I [11:0]
    .i_dac_v1_n     (a4_i_dac_v1_n),        // I [11:0]
    .i_dac_v2_n     (a4_i_dac_v2_n),        // I [11:0]
    .i_dac_v3_n     (a4_i_dac_v3_n),        // I [11:0]

	// from random
	.din_im			(o_64b1_s),		// I [63:0]
	.div_im			(sig_valid),        	// I
	.dir_im			(a4_sig_ready),        		// O

	.do_im_bit		(a4_bit),					// O [1:0]
	.dov_im_bit		(a4_bit_valid),       	 		// O

	// ---- IM DAC (AD5761) ---
	.im_dac_din			(a4_im_dac_wr),			// I [23:0]
	.im_dac_din_valid	(a4_im_dac_valid),		// I
	.im_dac_din_ready	()						// O
);

wire 			a5_test_mode;
wire [63:0]		a5_test_data;

wire [3:0]		a5_width;
wire [8:0]		a5_offset;
wire [23:0]		a5_im_dac_wr;
wire			a5_im_dac_valid;
wire [11:0]		a5_i_dac_v0;
wire [11:0]		a5_i_dac_v1_p;
wire [11:0]		a5_i_dac_v2_p;
wire [11:0]		a5_i_dac_v3_p;
wire [11:0]		a5_i_dac_v1_n;
wire [11:0]		a5_i_dac_v2_n;
wire [11:0]		a5_i_dac_v3_n;
// IM
con5_intf_alice con5_intf_alice (
	.sys_clk	(sys_clk),		// I
	.sig_clk	(sig_clk),		// I
	.clk_200    (clk_200),       // I	
	.rst		(grst),			// I

	.CON5B1_p (CON5B1_p),     // O
	.CON5B1_n (CON5B1_n),     // O
	.CON5B2_p (CON5B2_p),     // O
    .CON5B2_n (CON5B2_n),     // O
	.CON5B3	  (CON5B3),		// IO [15:0]
	//MGT ??΅

	// ---- Serdes ---
	.test_mode		(a5_test_mode),		// I 			// sig_clk
	.din_test		(a5_test_data),		// I [63:0]		// sig_clk

	.shape_load		(shape_ld),		// I
	.pulse_width	(a5_width),			// I [3:0]
	.pulse_offset	(a5_offset),			// I [8:0]

	.i_dac_v0		(a5_i_dac_v0),		// I [11:0]
	.i_dac_v1_p		(a5_i_dac_v1_p),		// I [11:0]
	.i_dac_v2_p		(a5_i_dac_v2_p),		// I [11:0]
	.i_dac_v3_p		(a5_i_dac_v3_p),		// I [11:0]
    .i_dac_v1_n     (a5_i_dac_v1_n),        // I [11:0]
    .i_dac_v2_n     (a5_i_dac_v2_n),        // I [11:0]
    .i_dac_v3_n     (a5_i_dac_v3_n),        // I [11:0]

	// from random
	.din_im			(o_64b3),		// I [63:0]
	.div_im			(sig_valid),        	// I
	.dir_im			(a5_sig_ready),        		// O

	.do_im_bit		(a5_bit),					// O [1:0]
	.dov_im_bit		(a5_bit_valid),       	 		// O

	// ---- IM DAC (AD5761) ---
	.im_dac_din			(a5_im_dac_wr),			// I [23:0]
	.im_dac_din_valid	(a5_im_dac_valid),		// I
	.im_dac_din_ready	()						// O
);

wire [31:0]		a6_PD12_cnt;

wire			a6_pdsync_en;
wire [3:0]		a6_start_width_thres;
wire [23:0]     a6_dp_wr;
wire            a6_dp_valid;
wire [7:0]      a6_dp_rd;
wire [31:0]	     a6_PD_trig_cnt;

//
con6_intf_alice con6_intf_alice (
	.sig_clk(sig_clk),			// I
	.sys_clk(sys_clk),
	.rst(grst),					// I

	.CON6B1_p	(CON6B1_p),		// I [1:0]
	.CON6B1_n	(CON6B1_n),		// I [1:0]
	.CON6B2_p	(CON6B2_p),		// O
	.CON6B2_n	(CON6B2_n),		// O
	.CON6B3		(CON6B3),		// IO [15:0]
	
	.clk_optic(clk_optic),				// O
	.clk_optic_locked(clk_optic_locked),		// O	
	// PD
	.do_PD12_cnt(a6_PD12_cnt),		// O [31:0]
	.width_thres(a6_start_width_thres),	// I [3:0]
	.start(a6_start_flag),         // o
    .pdsync_en(a6_pdsync_en),            // I
    .PD_trig_cnt(a6_PD_trig_cnt),		// O [31:0]	
	// ---- DP (AD5252) ---
	.dp_din(a6_dp_wr),                 // I [23:0]
	.dp_din_valid(a6_dp_valid),        // I
	.dp_dout(a6_dp_rd)                 // O [7:0]	
);

wire		a7_fpc_en;
wire		a7_fpc_dac_clr;
wire [31:0]	a7_fpc_dac_din;
wire		a7_fpc_dac_din_valid;
wire		a7_fpc10_dac_clr;
wire [31:0]	a7_fpc10_dac_din;
wire		a7_fpc10_dac_din_valid;
// FPC
con7_intf_alice con7_intf_alice (
	.sys_clk(sys_clk),			// I
	.rst(grst),					// I

	.CON7B1_p	(CON7B1_p),		// O 
	.CON7B1_n	(CON7B1_n),		// O 
	.CON7B2_p	(CON7B2_p),		// O
	.CON7B2_n	(CON7B2_n),		// O
	.CON7B3		(CON7B3),		// IO [15:0]
	// ---- Enable  -----
	.fpc_en					(a7_fpc_en),	// I
	// ---- FPC (AD5044) ---
	.fpc_dac_clr			(a7_fpc_dac_clr),		// I
	.fpc_dac_din			(a7_fpc_dac_din),		// I [31:0]
	.fpc_dac_din_valid		(a7_fpc_dac_din_valid),		// I
	.fpc_dac_din_ready		(),		// O
	// ---- FPC10 (AD5044) ---
	.fpc10_dac_clr			(a7_fpc10_dac_clr),		// I
	.fpc10_dac_din			(a7_fpc10_dac_din),		// I [31:0]
	.fpc10_dac_din_valid	(a7_fpc10_dac_din_valid),	// I
	.fpc10_dac_din_ready	()	// O

);


wire		a8_fpc_en;
wire		a8_fpc_dac_clr;
wire [31:0]	a8_fpc_dac_din;
wire		a8_fpc_dac_din_valid;
wire		a8_fpc10_dac_clr;
wire [31:0]	a8_fpc10_dac_din;
wire		a8_fpc10_dac_din_valid;
// FPC
con8_intf_alice con8_intf_alice (
	.sys_clk(sys_clk),			// I
	.rst(grst),					// I

	// to Con 8
	.CON8B1_p0	(CON8B1_p0),		// O 
    .CON8B1_n0    (CON8B1_n0),        // O 
    .CON8B1_p2    (CON8B1_p2),        // O
    .CON8B1_n2    (CON8B1_n2),        // O    
	.CON8B3		(CON8B3),		// IO [15:0]
	// ---- Enable  -----
	.fpc_en					(a8_fpc_en),	// I
	// ---- FPC (AD5044) ---
	.fpc_dac_clr			(a8_fpc_dac_clr),		// I
	.fpc_dac_din			(a8_fpc_dac_din),		// I [31:0]
	.fpc_dac_din_valid		(a8_fpc_dac_din_valid),		// I
	.fpc_dac_din_ready		(),		// O
	// ---- FPC10 (AD5044) ---
	.fpc10_dac_clr			(a8_fpc10_dac_clr),		// I
	.fpc10_dac_din			(a8_fpc10_dac_din),		// I [31:0]
	.fpc10_dac_din_valid	(a8_fpc10_dac_din_valid),	// I
	.fpc10_dac_din_ready	()	// O

);




wire 			a9_test_mode;
wire [31:0]		a9_test_data;

wire [5:0]		a9_width;
wire [6:0]		a9_i_offset_quotient;
wire [2:0]		a9_i_offset_remainder;
wire [4:0]		a9_delay_tap;
wire			a9_hmc920_en;
wire			a9_pm_pi_en;
wire [23:0]		a9_pm_dac_wr;
wire			a9_pm_dac_valid;
wire [23:0]		a9_pm_dac_rd;
wire [23:0]    a9_dp_wr;
wire           a9_dp_valid;
wire [7:0]     a9_dp_rd;
wire           a9_sig_pola;
// PM 
con9_intf_alice con9_intf_alice (

	.CON9B1_p  (CON9B1_p),		// O 
    .CON9B1_n  (CON9B1_n),        // O 
	
	.ADC_A_CLK_p(ADC_A_CLK_p),		// O
    .ADC_A_CLK_n(ADC_A_CLK_n),        // O    
    //	.CON9B2_p  (CON9B2_p),		// O 
//  .CON9B2_n  (CON9B2_n),        // O 


	.CON9B3	   (CON9B3),		// IO [15:0]
	
	.sys_clk		(sys_clk),		// I
    .sig_clk        (sig_clk),        // I
    .clk_200        (clk_200),    // I
    .clk_600        (clk_600),    // I
    .rst            (grst),            // I
    
	// ---- Serdes ---
	.test_mode		(a9_test_mode),		// I 			// sig_clk
	.din_test		(a9_test_data),		// I [31:0]		// sig_clk

	.shape_load		(shape_ld),
	
		// I
	.pulse_width	(a9_width),			// I [5:0]
    .i_offset_quotient(a9_i_offset_quotient),   // I [6:0]     
    .i_offset_remainder(a9_i_offset_remainder), // I [2:0]
    .sig_pola       (a9_sig_pola),      // I
    
	// delay tap 78ps per tap (max 2.418ns)

	.delay_tap_in	(a9_delay_tap),		// I [4:0]



	// from random
	.din_pm			(o_32b1),				// I [31:0]
	.div_pm			(sig_valid),		// I
	.dir_pm			(a9_sig_ready),	// O

	.do_pm_bit		(a9_bit),					// O
	.dov_pm_bit		(a9_bit_valid),       	 		// O

	// ---- Enable  -----
	.hmc920_en		(a9_hmc920_en),		// I
	.pm_pi_en	    (a9_pm_pi_en),

	// ---- PM Half DAC (AD5761) ---
	.pm_dac_din			(a9_pm_dac_wr),			// I [23:0]
	.pm_dac_din_valid	(a9_pm_dac_valid),		// I
	.pm_dac_din_ready	(),						// O
	.pm_dac_dout		(a9_pm_dac_rd),	        // O [23:0]
	
	// ---- DP (AD5252) ---
    .dp_din(a9_dp_wr),                 // I [23:0]
    .dp_din_valid(a9_dp_valid),        // I
    .dp_dout(a9_dp_rd)                 // O [7:0]    
);



wire 			a10_test_mode;
wire [31:0]		a10_test_data;

wire [5:0]		a10_width;
wire [6:0]		a10_i_offset_quotient;
wire [2:0]		a10_i_offset_remainder;
wire [4:0]		a10_delay_tap;
wire			a10_hmc920_en;
wire			a10_pm_pi_en;
wire [23:0]		a10_pm_dac_wr;
wire			a10_pm_dac_valid;
wire [23:0]		a10_pm_dac_rd;
wire [23:0]    a10_dp_wr;
wire           a10_dp_valid;
wire [7:0]     a10_dp_rd;
wire           a10_sig_pola;
// PM 
con10_intf_alice con10_intf_alice (

	.CON10B1_p	(CON10B1_p),		// O 
    .CON10B1_n  (CON10B1_n),        // O 
	.CON10B2_p	(CON10B2_p),		// O 
    .CON10B2_n  (CON10B2_n),        // O 
	.CON10B3	(CON10B3),		// IO [15:0]
	
	.sys_clk		(sys_clk),		// I
    .sig_clk        (sig_clk),        // I
    .clk_200        (clk_200),    // I
    .clk_600        (clk_600),    // I
    .rst            (grst),            // I
    
	// ---- Serdes ---
	.test_mode		(a10_test_mode),		// I 			// sig_clk
	.din_test		(a10_test_data),		// I [31:0]		// sig_clk

	.shape_load		(shape_ld),	// I
	.pulse_width	(a10_width),			// I [5:0]
    .i_offset_quotient(a10_i_offset_quotient),   // I [6:0]     
    .i_offset_remainder(a10_i_offset_remainder), // I [2:0]
    .sig_pola       (a10_sig_pola),      // I
    
	// delay tap 78ps per tap (max 2.418ns)

	.delay_tap_in	(a10_delay_tap),		// I [4:0]



	// from random
	.din_pm			(o_32b2),				// I [31:0]
	.div_pm			(sig_valid),		// I
	.dir_pm			(a10_sig_ready),	// O

	.do_pm_bit		(a10_bit),					// O
	.dov_pm_bit		(a10_bit_valid),       	 		// O

	// ---- Enable  -----
	.hmc920_en		(a10_hmc920_en),		// I
	.pm_pi_en			(a10_pm_pi_en),

	// ---- PM Half DAC (AD5761) ---
	.pm_dac_din			(a10_pm_dac_wr),			// I [23:0]
	.pm_dac_din_valid	(a10_pm_dac_valid),		// I
	.pm_dac_din_ready	(),						// O
	.pm_dac_dout		(a10_pm_dac_rd),	        // O [23:0]
	
	// ---- DP (AD5252) ---
    .dp_din(a10_dp_wr),                 // I [23:0]
    .dp_din_valid(a10_dp_valid),        // I
    .dp_dout(a10_dp_rd)                 // O [7:0]    
);

// ---------------------------------------------------------------------------------
//Switch for DMA Test
wire			test_cnt_clr;
wire	[1:0]	data_sel;
wire	[127:0]	to_pcie_tdata;
wire	[15:0]	to_pcie_tkeep;
wire			to_pcie_tlast;
wire			to_pcie_tvalid;
wire			to_pcie_tready;



// Module for DMA
wire	[127:0]	o_dma_buf_data;
wire    [15:0]	o_dma_buf_keep;
wire            o_dma_buf_last;
wire            o_dma_buf_valid;
wire            o_dma_buf_ready;



pcie_dma_data_sel #(
	.DATA_WIDTH(128)
) pcie_dma_data_sel (
	.pcie_clk(pcie_clk),			// I
	.sclr(grst|fifo_rst),					// I

	.test_cnt_clr(test_cnt_clr),	// I
	.data_sel(data_sel),			// I [1:0]

    .din_fifo_tdata(o_dma_buf_data),			// I [DATA_WIDTH-1:0]
    .din_fifo_tkeep(o_dma_buf_keep),			// I [KEEP_WIDTH-1:0]
    .din_fifo_tvalid(o_dma_buf_valid),		// I
    .din_fifo_tlast(o_dma_buf_last),			// I
    .din_fifo_tready(o_dma_buf_ready),		// O

	// to PCIE Axi streaming
	.to_pcie_tdata(to_pcie_tdata),			// O [DATA_WIDTH-1:0]
	.to_pcie_tkeep(to_pcie_tkeep),			// O [KEEP_WIDTH-1:0]
	.to_pcie_tlast(to_pcie_tlast),			// O
	.to_pcie_tvalid(to_pcie_tvalid),		// O
	.to_pcie_tready(to_pcie_tready)			// I
);

// ---------------------------------------------------------------------------------
// PCIE BLOCK // all pcie-clk
wire	[46:0]	regs_adda;
wire			regs_adda_valid;

wire			regs_bsc_sys_sel;
wire			regs_bsc_sig_sel;
wire			regs_bsc_pci_sel;
wire			regs_ud_sys_sel;
wire			regs_ud_sig_sel;
//wire			regs_ud_reg3_sel;
//wire			regs_ud_reg4_sel;

wire	[31:0]	regs_sys_data;
wire	[31:0]	regs_sig_data;
//wire	[31:0]	regs_ud_reg3_data;
//wire	[31:0]	regs_ud_reg4_data;
wire	[31:0]	regs_bsc_pci_data;
wire			regs_sys_data_valid;
wire			regs_sig_data_valid;
wire			regs_bsc_pci_data_valid;
//wire			regs_ud_reg3_data_valid;
//wire			regs_ud_reg4_data_valid;

wire	[7:0]	c2h_0_sts;
wire	[7:0]	h2c_0_sts;
wire			c2h_0_rst;			// to use fifo reset
wire			dma_fifo_empty;		// to use fifo reset
wire	[31:0]	c2h_0_src_addr;
wire	[27:0]	c2h_0_len;
wire	[31:0]	c2h_0_busy_clk_cnt;
wire	[31:0]	c2h_0_run_clk_cnt;
wire	[31:0]	c2h_0_packet_cnt;
wire	[31:0]	c2h_0_desc_cnt;
wire regs_data_valid;
assign dma_fifo_empty = 1'b0;
assign regs_data_valid = regs_sig_data_valid | regs_sys_data_valid | regs_bsc_pci_data_valid;// | regs_ud_reg3_data_valid; // | regs_ud_reg4_data_valid;

xdma_pcie_ep xdma_pcie_ep  (
	.pci_exp_txp(pci_exp_txp),						// O [3:0]
	.pci_exp_txn(pci_exp_txn),						// O [3:0]
	.pci_exp_rxp(pci_exp_rxp),						// I [3:0]
	.pci_exp_rxn(pci_exp_rxn),						// I [3:0]

	.sys_clk_p(pcie_ref_clk_p),						// I
	.sys_clk_n(pcie_ref_clk_n),						// I
	.sys_rst_n(~grst),								// I

	// -- to/from REGS
	.regs_adda			(regs_adda),				// O [46:0]
	.regs_adda_valid	(regs_adda_valid),			// O
	.regs_bsc_sys_sel	(regs_bsc_sys_sel),			// O
	.regs_bsc_sig_sel	(regs_bsc_sig_sel),			// O
	.regs_bsc_pci_sel	(regs_bsc_pci_sel),			// O
	.regs_ud_sys_sel	(regs_ud_sys_sel),			// O
	.regs_ud_sig_sel	(regs_ud_sig_sel),			// O
//	.regs_ud_reg3_sel	(regs_ud_reg3_sel),			// O
//	.regs_ud_reg4_sel	(regs_ud_reg4_sel),			// O

	.regs_bsc_sys_data	(regs_sys_data),			// I [31:0]
	.regs_bsc_sig_data	(regs_sig_data),				// I [31:0]
	.regs_bsc_pci_data	(regs_bsc_pci_data),		// I [31:0]
	.regs_ud_sys_data	(regs_sys_data),			// I [31:0]
	.regs_ud_sig_data	(regs_sig_data),				// I [31:0]
//	.regs_ud_reg3_data	(regs_ud_reg3_data),		// I [31:0]
//	.regs_ud_reg4_data	('b0),		// I [31:0]
	.regs_data_valid	(regs_data_valid),			// I

	// DMA Statusregs_sys_data
	.c2h_sts_0			(c2h_0_sts),				// O [7:0]
	.h2c_sts_0			(h2c_0_sts),				// O [7:0]

	.c2h_busy_clk_cnt	(c2h_0_busy_clk_cnt),		// O [31:0]
	.c2h_run_clk_cnt	(c2h_0_run_clk_cnt),		// O [31:0]
	.c2h_packet_cnt		(c2h_0_packet_cnt),			// O [31:0]
	.c2h_desc_cnt		(c2h_0_desc_cnt),			// O [31:0]

	// --  AXI ST interface to user
	// AXI streaming ports
	.s_axis_c2h_tdata_0		(to_pcie_tdata),		// I [127:0]
	.s_axis_c2h_tlast_0		(to_pcie_tlast),		// I
	.s_axis_c2h_tvalid_0	(to_pcie_tvalid),		// I
	.s_axis_c2h_tready_0	(to_pcie_tready),		// O
	.s_axis_c2h_tkeep_0		(to_pcie_tkeep),		// I [15:0]
	.m_axis_h2c_tdata_0		(),						// O [127:0]	//not used
	.m_axis_h2c_tlast_0		(),						// O			//not used
	.m_axis_h2c_tvalid_0	(),						// O			//not used
	.m_axis_h2c_tready_0	(1'b0),					// I			//not used
	.m_axis_h2c_tkeep_0		(),						// O [15:0]		//not used

	// user clk
	.user_clk				(pcie_clk),				// O 125MHz
	.user_resetn			(),						// O
	.user_lnk_up			(pcie_link_up)
);



// ---------------------------------------------------------------------------------
// Reg for sys_clk
regs_sys_alice regs_sys_alice(
	.clk				(sys_clk),								// I
	.rst				(grst),									// I
	.pcie_clk			(pcie_clk),								// I

	.regs_sel			(regs_bsc_sys_sel | regs_ud_sys_sel),		// I
	.regs_adda			(regs_adda),							// I	[46:0]
	.regs_adda_valid	(regs_adda_valid),						// I
	.regs_dout			(regs_sys_data),					// O	[31:0]
	.regs_dout_valid	(regs_sys_data_valid),				// O

	// -------------------------------------------------------
	// -- register ? λ³?
	// BSC_TEMP_REG
	.led_test(led_test),							// O
	.led1_on(led1_on),								// O
	.led2_on(led2_on),								// O
	.led3_on(led3_on),								// O

	// LM70_SPI_WR_REG
	.lm70_spi_valid(lm70_spi_valid),				// O
	// LM70_SPI_RD_REG
	.lm70_spi_rd(lm70_spi_rd),						// I [15:0]
	.lm70_spi_busy(~lm70_din_ready),				// I

	// FAN_HIGH_DURA_REG
	.fan_high_dura(fan_high_dura),					// O [15:0]
	// FAN_LOW_DURA_REG
	.fan_low_dura(fan_low_dura),					// O [15:0]

	// FAN1_HIGH_DURA_REG
	.fan1_high_dura(fan1_high_dura),				// O [15:0]
	// FAN1_LOW_DURA_REG
	.fan1_low_dura(fan1_low_dura),					// O [15:0]

	// --------------------------------------------------------
	// CON 1
	// A1_HMC920_EN_REG
	.a1_hmc920_en(a1_hmc920_en),					// O

	// A1_LTC1867_SPI_WR_REG
	.a1_ltc1867_spi_wr(a1_ltc1867_spi_wr),			// O [15:0]
	.a1_ltc1867_spi_valid(a1_ltc1867_spi_valid),	// O
	// A1_LTC1867_SPI_RD_REG
	.a1_ltc1867_spi_rd(a1_ltc1867_spi_rd),			// I [15:0]
	.a1_ltc1867_spi_busy(~a1_ltc1867_din_ready),	// I

	// A1_LD1550_DAC_WR_REG
	.a1_ad5761_dac_wr(a1_ad5761_dac_wr),			// O [23:0]
	.a1_ad5761_dac_valid(a1_ad5761_dac_valid),		// O

	// A1_LD1550_DAC_RD_REG
	.a1_ad5761_dac_rd(a1_ad5761_dac_rd),			// I [23:0]

	// A1_TEC_EN_REG
	.a1_tec_en(a1_tec_en),					// O
	// A1_TEC_DAC_WR_REG
	.a1_tec_dac_wr(a1_tec_dac_wr),			// O [15:0]
	.a1_tec_dac_valid(a1_tec_dac_valid),	// O
	// A1_TEC_DAC_CLR_REG
	.a1_tec_dac_clr(a1_tec_dac_clr),			// O
	
	// A1_DP_WR_REG
    .a1_dp_wr(a1_dp_wr),           // O [23:0]
    .a1_dp_valid(a1_dp_valid),
    // A1_DP_RD_REG
    .a1_dp_rd(a1_dp_rd),           // I [7:0]
	// --------------------------------------------------------
	// CON 2
	// A2_HMC920_EN_REG
	.a2_hmc920_en(a2_hmc920_en),					// O
	// A2_SPD_EN_REG
	.a2_spd_en(a2_spd_en),					// O
	// A2_SIP_EN_REG
	.a2_sip_en(a2_sip_en),					// O

	// A2_LTC1867_SPI_WR_REG
	.a2_ltc1867_spi_wr		(a2_ltc1867_spi_wr),		// O [15:0]
	.a2_ltc1867_spi_valid	(a2_ltc1867_spi_valid),		// O
	// A1_LTC1867_SPI_RD_REG
	.a2_ltc1867_spi_rd		(a2_ltc1867_spi_rd),		// I [15:0]
	.a2_ltc1867_spi_busy	(~a2_ltc1867_din_ready),	// I

	// A2_SPD_DAC_WR_REG
	.a2_spd_dac_wr			(a2_spd_dac_wr),			// O [15:0]
	.a2_spd_dac_valid		(a2_spd_dac_valid),			// O

	// A2_SIP_DAC_WR_REG
	.a2_sip_dac_wr			(a2_sip_dac_wr),			// O [15:0]
	.a2_sip_dac_valid		(a2_sip_dac_valid),			// O

	// A2_TEC_EN_REG
	.a2_tec_en			(a2_tec_en),			// O
	// A2_TEC_DAC_WR_REG
	.a2_tec_dac_wr		(a2_tec_dac_wr),		// O [15:0]
	.a2_tec_dac_valid	(a2_tec_dac_valid),		// O
	// A2_TEC_DAC_CLR_REG
	.a2_tec_dac_clr		(a2_tec_dac_clr),		// O
	
	// A2_DP_WR_REG
    .a2_dp_wr(a2_dp_wr),           // O [23:0]
    .a2_dp_valid(a2_dp_valid),
    // A2_DP_RD_REG
    .a2_dp_rd(a2_dp_rd),           // I [7:0]
// --------------------------------------------------------
// CON 3
	// A3_IM_DAC_WR_REG
    .a3_im_dac_wr        (a3_im_dac_wr),            // O [23:0]
    .a3_im_dac_valid    (a3_im_dac_valid),            // O    
// --------------------------------------------------------
// CON 4	
	// A4_IM_DAC_WR_REG
    .a4_im_dac_wr        (a4_im_dac_wr),            // O [23:0]
    .a4_im_dac_valid    (a4_im_dac_valid),            // O     
// --------------------------------------------------------
// CON 5
	// A5_IM_DAC_WR_REG
    .a5_im_dac_wr        (a5_im_dac_wr),            // O [23:0]
    .a5_im_dac_valid    (a5_im_dac_valid),            // O    

	// --------------------------------------------------------
	// CON 6
    // A6_PDSYNC_EN_REG
    .a6_pdsync_en(a6_pdsync_en),    // O 
    // A6_DP_WR_REG
    .a6_dp_wr(a6_dp_wr),           // O [23:0]
    .a6_dp_valid(a6_dp_valid),
    // A1_DP_RD_REG
    .a6_dp_rd(a6_dp_rd),           // I [7:0]


	// --------------------------------------------------------
	// CON 7
	// A7_FPC_EN_REG
    .a7_fpc_en            (a7_fpc_en),            // O

    // A7_FPC_DAC_WR_REG
    .a7_fpc_dac_din            (a7_fpc_dac_din),        // O [31:0]
    .a7_fpc_dac_din_valid    (a7_fpc_dac_din_valid),        // O
    // A7_FPC_DAC_CLR_REG
    .a7_fpc_dac_clr            (a7_fpc_dac_clr),        // O

    // A7_FPC10_DAC_WR_REG
    .a7_fpc10_dac_din        (a7_fpc10_dac_din),        // O [31:0]
    .a7_fpc10_dac_din_valid    (a7_fpc10_dac_din_valid),    // O
    // A7_FPC10_DAC_CLR_REG
    .a7_fpc10_dac_clr        (a7_fpc10_dac_clr),        // O 
	
	// --------------------------------------------------------
	// CON 8
	// A8_FPC_EN_REG
    .a8_fpc_en            (a8_fpc_en),            // O

    // A8_FPC_DAC_WR_REG
    .a8_fpc_dac_din            (a8_fpc_dac_din),        // O [31:0]
    .a8_fpc_dac_din_valid    (a8_fpc_dac_din_valid),        // O
    // A8_FPC_DAC_CLR_REG
    .a8_fpc_dac_clr            (a8_fpc_dac_clr),        // O

    // A8_FPC10_DAC_WR_REG
    .a8_fpc10_dac_din        (a8_fpc10_dac_din),        // O [31:0]
    .a8_fpc10_dac_din_valid    (a8_fpc10_dac_din_valid),    // O
    // A8_FPC10_DAC_CLR_REG
    .a8_fpc10_dac_clr        (a8_fpc10_dac_clr),        // O
   
	// --------------------------------------------------------
	// CON 9
	
        // A9_HMC920_EN_REG
        .a9_hmc920_en    (a9_hmc920_en),                // O
        // A9_PMPI_EN_REG
        .a9_pm_pi_en        (a9_pm_pi_en),                    // O
    
        // A9_PM_DAC_WR_REG
        .a9_pm_dac_wr    (a9_pm_dac_wr),                // O [23:0]
        .a9_pm_dac_valid(a9_pm_dac_valid),            // O
    
        // A9_PM_DAC_RD_REG
        .a9_pm_dac_rd    (a9_pm_dac_rd),                // I [23:0]
        
        // A9_DP_WR_REG
        .a9_dp_wr(a9_dp_wr), // O[23:0]
        .a9_dp_valid(a9_dp_valid), // O
        
        // A9_DP_RD_REG
        .a9_dp_rd(a9_dp_rd), // I [7:0]        
	
 	// --------------------------------------------------------
	// CON 10
	
    // A10_HMC920_EN_REG
    .a10_hmc920_en    (a10_hmc920_en),                // O
    // A10_PMPI_EN_REG
    .a10_pm_pi_en        (a10_pm_pi_en),                    // O

    // A10_PM_DAC_WR_REG
    .a10_pm_dac_wr    (a10_pm_dac_wr),                // O [23:0]
    .a10_pm_dac_valid(a10_pm_dac_valid),            // O

    // A10_PM_DAC_RD_REG
    .a10_pm_dac_rd    (a10_pm_dac_rd),                // I [23:0]
    
    // A10_DP_WR_REG
    .a10_dp_wr(a10_dp_wr), // O[23:0]
    .a10_dp_valid(a10_dp_valid), // O
    
    // A10_DP_RD_REG
    .a10_dp_rd(a10_dp_rd) // I [7:0]        
);



// ---------------------------------------------------------------------------------
// Reg for sig_clk
regs_sig_alice regs_sig_alice (
	.clk				(sig_clk),								// I
	.rst				(grst),									// I
	.pcie_clk			(pcie_clk),								// I

	.regs_sel			(regs_bsc_sig_sel | regs_ud_sig_sel),		// I
	.regs_adda			(regs_adda),							// I [46:0]
	.regs_adda_valid	(regs_adda_valid),						// I
	.regs_dout			(regs_sig_data),							// O [31:0]
	.regs_dout_valid	(regs_sig_data_valid),					// O

	// -------------------------------------------------------
	// -- register ? λ³?
	// --------------------------------------------------------
	// CON 1
	// A1_TEST_MODE_REG
	.a1_test_mode	(a1_test_mode),	// O
	// A1_TEST_DATA_REG
	.a1_test_data	(a1_test_data),	// O [31:0]
	// A1_WIDTH_REG
	.a1_width		(a1_width),			// O [5:0]
	.a1_add_width   (a1_add_width),
	.a1_shape_load	(a1_shape_load),	// O
    // A1_OFFSET_QUOT_REG
    .a1_i_offset_quotient        (a1_i_offset_quotient),    // O [6:0]
    // A1_OFFSET_REMD_REG    
    .a1_i_offset_remainder          (a1_i_offset_remainder), // O [2:0]
	// A1_DELAY_TAP_REG
	.a1_delay_tap	(a1_delay_tap),	// O [4:0]
	
    // C6_ADD_PULSE_DELAY_REG
    .o_a1_delay_cnt  (o_a1_delay_cnt),  // O [9:0]  

	// --------------------------------------------------------
	// CON 2
	// A2_TEST_MODE_REG
    .a2_test_mode    (a2_test_mode),    // O
    // A2_TEST_DATA_REG
    .a2_test_data    (a2_test_data),    // O [31:0]
    // A2_WIDTH_REG
    .a2_width        (a2_width),    // O [5:0]
    .a2_shape_load    (a2_shape_load),    // O
    // A2_OFFSET_QUOT_REG
    .a2_i_offset_quotient        (a2_i_offset_quotient),    // O [6:0]
    // A2_OFFSET_REMD_REG    
    .a2_i_offset_remainder          (a2_i_offset_remainder), // O [2:0]
    // A2_DELAY_TAP_REG
    .a2_delay_tap    (a2_delay_tap),    // O [4:0]
    // A2_DETECT_DELAY_TAP_REG
    .a2_detect_delay_tap(a2_detect_delay_tap), // O [4:0]    
    // A2_SPD_CNT1_REG
    .a2_spad_cnt1    (a2_spad_cnt1),        // I [31:0]
    // A2_SPD_CNT2_REG
    .a2_spad_cnt2    (a2_spad_cnt2),        // I [31:0]
    // A2_SPD_CNT3_REG
    .a2_spad_cnt3    (a2_spad_cnt3),        // I [31:0]
    // A2_SPD_CNT4_REG
    .a2_spad_cnt4    (a2_spad_cnt4),        // I [31:0]    

// --------------------------------------------------------
// CON 3
	// A3_TEST_MODE_REG
    .a3_test_mode    (a3_test_mode),        // O
    // A3_TEST_DATA_REG
    .a3_test_l_data    (a3_test_data[31:0]),        // O [31:0]
    // A3_TEST1_DATA_REG
    .a3_test_h_data    (a3_test_data[63:32]),        // O [31:0]
    // A3_WIDTH_REG
    .a3_width        (a3_width),            // O [3:0]
    .a3_shape_load    (a3_shape_load),    // O
    // A3_OFFSET_REG
    .a3_offset        (a3_offset),        // O [8:0]
    // A3_V0_REG
    .a3_i_dac_v0            (a3_i_dac_v0),            // O [11:0]
    // A3_V1_P_REG
    .a3_i_dac_v1_p          (a3_i_dac_v1_p),            // O [11:0]
    // A3_V2_P_REG
    .a3_i_dac_v2_p          (a3_i_dac_v2_p),            // O [11:0]
    // A3_V3_P_REG
    .a3_i_dac_v3_p          (a3_i_dac_v3_p),            // O [11:0]
    // A3_V1_N_REG
    .a3_i_dac_v1_n          (a3_i_dac_v1_n),            // O [11:0]
    // A3_V2_N_REG
    .a3_i_dac_v2_n          (a3_i_dac_v2_n),            // O [11:0]
    // A3_V3_N_REG
    .a3_i_dac_v3_n          (a3_i_dac_v3_n),            // O [11:0]    
// --------------------------------------------------------
// CON 4
	
    	// A4_TEST_MODE_REG
    .a4_test_mode    (a4_test_mode),        // O
    // A4_TEST_DATA_REG
    .a4_test_l_data    (a4_test_data[31:0]),        // O [31:0]
    // A4_TEST1_DATA_REG
    .a4_test_h_data    (a4_test_data[63:32]),        // O [31:0]
    // A4_WIDTH_REG
    .a4_width        (a4_width),            // O [3:0]
    .a4_shape_load    (a4_shape_load),    // O
    // A4_OFFSET_REG
    .a4_offset        (a4_offset),        // O [8:0]
    // A4_V0_REG
    .a4_i_dac_v0            (a4_i_dac_v0),            // O [11:0]
    // A4_V1_P_REG
    .a4_i_dac_v1_p          (a4_i_dac_v1_p),            // O [11:0]
    // A4_V2_P_REG
    .a4_i_dac_v2_p          (a4_i_dac_v2_p),            // O [11:0]
    // A4_V3_P_REG
    .a4_i_dac_v3_p          (a4_i_dac_v3_p),            // O [11:0]
    // A4_V1_N_REG
    .a4_i_dac_v1_n          (a4_i_dac_v1_n),            // O [11:0]
    // A4_V2_N_REG
    .a4_i_dac_v2_n          (a4_i_dac_v2_n),            // O [11:0]
    // A4_V3_N_REG
    .a4_i_dac_v3_n          (a4_i_dac_v3_n),            // O [11:0]  

// --------------------------------------------------------
// CON 5
	// A5_TEST_MODE_REG
    .a5_test_mode    (a5_test_mode),        // O
    // A5_TEST_DATA_REG
    .a5_test_l_data    (a5_test_data[31:0]),        // O [31:0]
    // A5_TEST1_DATA_REG
    .a5_test_h_data    (a5_test_data[63:32]),        // O [31:0]
    // A5_WIDTH_REG
    .a5_width        (a5_width),            // O [3:0]
    .a5_shape_load    (a5_shape_load),    // O
    // A5_OFFSET_REG
    .a5_offset        (a5_offset),        // O [8:0]
    // A5_V0_REG
    .a5_i_dac_v0            (a5_i_dac_v0),            // O [11:0]
    // A5_V1_P_REG
    .a5_i_dac_v1_p          (a5_i_dac_v1_p),            // O [11:0]
    // A5_V2_P_REG
    .a5_i_dac_v2_p          (a5_i_dac_v2_p),            // O [11:0]
    // A5_V3_P_REG
    .a5_i_dac_v3_p          (a5_i_dac_v3_p),            // O [11:0]
    // A5_V1_N_REG
    .a5_i_dac_v1_n          (a5_i_dac_v1_n),            // O [11:0]
    // A5_V2_N_REG
    .a5_i_dac_v2_n          (a5_i_dac_v2_n),            // O [11:0]
    // A5_V3_N_REG
    .a5_i_dac_v3_n          (a5_i_dac_v3_n),            // O [11:0] 
// --------------------------------------------------------
// CON 6

    // A6_PD12_CNT_REG    
    .a6_PD12_cnt    (a6_PD12_cnt),        // I [31:0]    
    //A6_WIDTH_THRES_REG    
    .a6_PD_trig_cnt (a6_PD_trig_cnt),   // I [31:0]   
    // 
   .a6_start_width_thres (a6_start_width_thres),// O [3:0]
// --------------------------------------------------------
// CON 7

// --------------------------------------------------------
// CON 8

	// --------------------------------------------------------
	// CON 9
  
        // A9_TEST_MODE_REG
        .a9_test_mode     (a9_test_mode),        // O
        // A9_TEST_DATA_REG
        .a9_test_data     (a9_test_data),        // O [31:0]
        // A9_WIDTH_REG
        .a9_width         (a9_width),            // O [5:0]
        .a9_shape_load    (a9_shape_load),    // O
        // A9_OFFSET_QUOT_REG
        .a9_i_offset_quotient        (a9_i_offset_quotient),    // O [6:0]
        // A9_OFFSET_REMD_REG    
        .a9_i_offset_remainder       (a9_i_offset_remainder), // O [2:0]
        // A9_DELAY_TAP_REG
        .a9_delay_tap    (a9_delay_tap),        // O [4:0]
        // A9_SIG_POLA
        .a9_sig_pola    (a9_sig_pola),        // O    
    
    
	// --------------------------------------------------------
	// CON 10
	// A10_TEST_MODE_REG
	.a10_test_mode	(a10_test_mode),		// O
	// A10_TEST_DATA_REG
	.a10_test_data	(a10_test_data),		// O [31:0]
	// A10_WIDTH_REG
	.a10_width		(a10_width),			// O [5:0]
	.a10_shape_load	(a10_shape_load),	// O
    // A10_OFFSET_QUOT_REG
    .a10_i_offset_quotient        (a10_i_offset_quotient),    // O [6:0]
    // A10_OFFSET_REMD_REG    
    .a10_i_offset_remainder          (a10_i_offset_remainder), // O [2:0]
	// A10_DELAY_TAP_REG
	.a10_delay_tap	(a10_delay_tap),		// O [4:0]
    // A10_SIG_POLA
    .a10_sig_pola    (a10_sig_pola),        // O    
    
    
     // A_FAKE_START_REG    
     .op_fake_start(op_fake_start), // O

	// A_READY_REG
	.op_ready(op_ready),			// O
	.op_busy(op_busy),				// I
	// A_STOP_REG
	.op_stop(op_stop),				// O
	// A_SNAPSHOT_REG
	.op_snapshot(op_snapshot),		// O [20:0]
	// A_READY_ERR_REG
	.ready_err_flag_clear	(ready_err_flag_clear), // O
	.ready_err_flag			(ready_err_flag),		// I [4:0]
	// A_FIFO_STAT_REG
	.fifo_rst(fifo_rst),    // O
	// A_FIFO_PF_THRES_REG
	// A_FIFO_READ_REG
	.din_fifo(log32),				// I [31:0]
	.div_fifo(log32_valid),			// I
	.dir_fifo(),					// O
	
    // emp_log
    .emp_log(emp_log),                // I [7:0]
    
	// A_SEED_REG
	.seed(seed),				// O [31:0]
	.seed_valid(seed_valid),	// O
	// A_SIG_THRES1_REG
	.sig_thres1(thres1),		// O [7:0]
	// A_SIG_THRES2_REG
	.sig_thres2(thres2),			// O [7:0]

// DRBG----------------------------------------------------
	// A_DRBG_MODE_REG
	.drbg_mode (drbg_mode),                         // O
	// A_ETP_CNT_REG
	.etp_cnt (etp_cnt),                             // I [31:0]
	// A_DRBG_CNT_REG
	.drbg_cnt (drbg_cnt),                           // I [31:0]
	// A_ETP_DRBG_FIFO_STAT_REG
	.empty_etp_fifo (empty_etp_fifo),               // I
	.empty_drbg_fifo_group (empty_drbg_fifo_group), // I
	.full_etp_veri (full_etp_veri),                 // I
	.full_drbg_veri (full_drbg_veri),               // I
	// A_ETP_VERI_READ_REG
	.rd_en_etp_veri (rd_en_etp_veri),               // O
	.o_etp_veri (o_etp_veri),                       // I [31:0]
	// A_DRBG_VERI_READ_REG
	.rd_en_drbg_veri (rd_en_drbg_veri),             // O
	.o_drbg_veri (o_drbg_veri),                     // I [31:0]
// --------------------------------------------------------
	
	// A_SIG_TEST_MODE_REG
	.a_64b1_test_mode(a_64b1_test_mode),		// O

    // A_SIG0_TEST_DATA_REG    
    .a_64b1_s0_test_data(a_64b1_s0_test_data),    // O [31:0]
    // A_SIG1_TEST_DATA_REG    
    .a_64b1_s1_test_data(a_64b1_s1_test_data), // O [31:0]
    
      // A_DB1_D_TEST_MODE_REG 
    .a_64b2_d_test_mode(a_64b2_d_test_mode), // O
      // A_DB1_B_TEST_MODE_REG     
    .a_64b2_b_test_mode(a_64b2_b_test_mode), // O    
     // A_DB1_B_TEST_DATA_REG    
    .a_64b2_b_test_data(a_64b2_b_test_data),// O [31:0]
    //  A_DB1_D_TEST_DATA_REG      
    .a_64b2_d_test_data(a_64b2_d_test_data), // O [31:0]

      // A_DB2_D_TEST_MODE_REG   
    .a_64b3_d_test_mode(a_64b3_d_test_mode), // O
      // A_DB2_B_TEST_MODE_REG     
    .a_64b3_b_test_mode(a_64b3_b_test_mode), // O    
     // A_DB2_B_TEST_DATA_REG    
    .a_64b3_b_test_data(a_64b3_b_test_data),// O [31:0]
    // A_DATA_TEST_DATA_REG    
    .a_64b3_d_test_data(a_64b3_d_test_data), // O [31:0]

     // A_PM_TEST_MODE_REG   
    .a_32b1_test_mode(a_32b1_test_mode), // O
     // A_PM_TEST_DATA_REG    
    .a_32b1_test_data(a_32b1_test_data),// O [31:0]
    
     // A_L_TEST_MODE_REG   
    .a_32b2_test_mode(a_32b2_test_mode), // O
     // A_L_TEST_DATA_REG    
    .a_32b2_test_data(a_32b2_test_data),// O [31:0]    
	// A_TRIG_TYPE_REG
    .a_trig_type(a_trig_type)       // O [1:0]
   
);

// ---------------------------------------------------------------------------------
// Reg for pcie_clk
wire       o_dma_buf_progbfull;
wire [13:0]dma_buf_prog_thresh;

regs_bsc_pcie regs_bsc_pcie (
	.pcie_clk			(pcie_clk),					// I
	.sclr				(grst),						// I

	.regs_sel			(regs_bsc_pci_sel),			// I
	.regs_adda			(regs_adda),				// I [46:0]
	.regs_adda_valid	(regs_adda_valid),			// I
	.regs_dout			(regs_bsc_pci_data),		// O [31:0]
	.regs_dout_valid	(regs_bsc_pci_data_valid),	// O
	// -------------------------------------------------------
	// -- register ? λ³?
	// DMA_DATA_SEL_REG
	.data_sel				(data_sel),				// O [1:0]
	.test_cnt_clr			(test_cnt_clr),			// O
	// DMA_FIFO_EMPTY_REG
	.dma_fifo_empty			(dma_fifo_empty),		// I

	// PCIE_DMA_C2H_0_CTRL_REG
	.c2h_0_rst				(c2h_0_rst),			// O
	// PCIE_DMA_C2H_0_SRCADDR_REG
	.c2h_0_src_addr			(c2h_0_src_addr),		// O [31:0]
	// PCIE_DMA_C2H_0_LEN_REG
	.c2h_0_len				(c2h_0_len),			// O [27:0]
	// PCIE_DMA_C2H_0_STAT_REG
	.c2h_0_sts				(c2h_0_sts),			// I [7:0]

	// PCIE_DMA_C2H_0_BUSY_CNT_REG
	.c2h_0_busy_clk_cnt		(c2h_0_busy_clk_cnt),	// I[31:0]
	// PCIE_DMA_C2H_0_RUN_CNT_REG
	.c2h_0_run_clk_cnt		(c2h_0_run_clk_cnt),	// I [31:0]
	// PCIE_DMA_C2H_0_PACKET_CNT_REG
	.c2h_0_packet_cnt		(c2h_0_packet_cnt),		// I [31:0]
	// PCIE_DMA_C2H_0_DESC_CNT_REG
	.c2h_0_desc_cnt			(c2h_0_desc_cnt),		// I [31:0]
	
    // DMA_BUFFER_PROG_THRESH
    .o_dma_buf_prog_thresh(dma_buf_prog_thresh),  // O [13:0]
    	// DMA_BUFFER_FULL
    .i_dma_buf_progbfull(o_dma_buf_progbfull),  // I

	// PCIE_DMA_H2C_0_STAT_REG
	.h2c_0_sts				(h2c_0_sts)				// I [7:0]
);



dma_for_buffer dma_for_buffer (
    .rst(grst|fifo_rst),		// I // multi-clk reset
    .sig_clk(sig_clk),				// I
    .pcie_clk(pcie_clk),			// I
    // signal interface // sig_clk
    .din(log128),					// I [127:0]
    .din_valid(log128_valid),			// I
    .din_ready(),			// O  not use?
    .sig_number_128b(sig_number_128b),              // I [11:0]
    // PCIE interface //pcie_clk
    .dout(o_dma_buf_data),				// O [127:0]
    .dout_valid(o_dma_buf_valid),		// O
    .dout_keep(o_dma_buf_keep),			// O [15:0]   
    .dout_last(o_dma_buf_last),			// O
    .dout_ready(o_dma_buf_ready),			// I
    .i_dma_buf_prog_thresh(dma_buf_prog_thresh), // I [13:0]
    .prog_bfull(o_dma_buf_progbfull)    //O
);










OBUFDS #(
	.IOSTANDARD("DEFAULT"), // Specify the output I/O standard
	.SLEW("FAST")			// Specify the output slew rate
) OBUFDS_A1 (
	.O(GPIO_1_8_PIN[0]),		// Diff_p output (connect directly to top-level port)
	.OB(GPIO_1_8_PIN[1]),		// Diff_n output (connect directly to top-level port)
	.I(1'b1)				// Buffer input
);

OBUFDS #(
	.IOSTANDARD("DEFAULT"), // Specify the output I/O standard
	.SLEW("FAST")			// Specify the output slew rate
) OBUFDS_A2 (
	.O(GPIO_1_8_PIN[2]),		// Diff_p output (connect directly to top-level port)
	.OB(GPIO_1_8_PIN[3]),		// Diff_n output (connect directly to top-level port)
	.I(1'b1)				// Buffer input
);
OBUFDS #(
	.IOSTANDARD("DEFAULT"), // Specify the output I/O standard
	.SLEW("FAST")			// Specify the output slew rate
) OBUFDS_A3 (
	.O(GPIO_1_8_PIN[4]),		// Diff_p output (connect directly to top-level port)
	.OB(GPIO_1_8_PIN[5]),		// Diff_n output (connect directly to top-level port)
	.I(1'b1)				// Buffer input
);
OBUFDS #(
	.IOSTANDARD("DEFAULT"), // Specify the output I/O standard
	.SLEW("FAST")			// Specify the output slew rate
) OBUFDS_A6 (
	.O(GPIO_1_8_PIN[6]),		// Diff_p output (connect directly to top-level port)
	.OB(GPIO_1_8_PIN[7]),		// Diff_n output (connect directly to top-level port)
	.I(1'b1)				// Buffer input
);
OBUFDS #(
	.IOSTANDARD("DEFAULT"), // Specify the output I/O standard
	.SLEW("FAST")			// Specify the output slew rate
) OBUFDS_A7 (
	.O(GPIO_1_8_PIN[8]),		// Diff_p output (connect directly to top-level port)
	.OB(GPIO_1_8_PIN[9]),		// Diff_n output (connect directly to top-level port)
	.I(1'b1)				// Buffer input
);

 //  (*LOC="IDELAYCTRL_X0Y3"*) // Place the logic in the IDELAYCTRLlocated at the IDELAYCTRL_X0Y3 on the XY IDELAYCTRL grid.
// IDELAY_CTRL for SELECTIO
IDELAYCTRL IDELAYCTRL_SELECTIO_8TO1 (

   .RDY(),  // 1-bit output: Ready output

   .REFCLK(clk_200),    // 1-bit input: Reference clock input

   .RST(grst)       // 1-bit input: Active high reset input

);       
assign GPIO_3_3_SMA[4:2] = 3'b0;
assign GPIO_1_8_SMA[3:2] = 4'b0;


OBUF   f_clk_10_g33sma0_obuf (.O(GPIO_3_3_SMA[0]), .I(sig_clk_10));
assign GPIO_3_3_SMA[1] = sig_clk_10M_pulse;
assign GPIO_1_8_SMA[0] = a6_start_flag;
assign GPIO_1_8_SMA[1] = sig_start;
endmodule
