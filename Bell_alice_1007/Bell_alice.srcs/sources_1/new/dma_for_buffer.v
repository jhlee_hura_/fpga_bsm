`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/06/01 16:08:46
// Design Name: 
// Module Name: dma_for_buffer
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module dma_for_buffer#(
	parameter DIN_WIDTH = 128,
	localparam MASK_WIDTH = DIN_WIDTH / 8		// 1Mask = 8 bits
)(
    input wire rst,
    input wire sig_clk,
    input wire pcie_clk,
    
    input wire [DIN_WIDTH-1:0] din,
    input wire din_valid,

    output wire din_ready,
    input wire   [13:0] sig_number_128b,
    output wire  [DIN_WIDTH-1:0] dout,
    output wire  dout_valid,
  	output wire  [MASK_WIDTH-1:0] dout_keep,    
	output wire  dout_last,    
    input wire   dout_ready,
    input wire   [13:0] i_dma_buf_prog_thresh, // Pcie clk
    output wire  prog_bfull
    );
localparam DIN_PER_MIG = 128 / DIN_WIDTH;	// PCIE ? ?€?΄κ°?? Din κ°??
localparam MSB_DIN_PER_MIG =	(DIN_PER_MIG <= 2) ? 1 :
								(DIN_PER_MIG > 2 && DIN_PER_MIG <= 4) ? 2 :
								(DIN_PER_MIG > 4 && DIN_PER_MIG <= 8) ? 3 :
								(DIN_PER_MIG > 8 && DIN_PER_MIG <= 16) ? 4 :
								(DIN_PER_MIG > 16 && DIN_PER_MIG <= 32) ? 5 :
								(DIN_PER_MIG > 32 && DIN_PER_MIG <= 64) ? 6 : 0;   

// 128b_last signal pci clk
reg [14:0] cnt_128b;
 //(* mark_debug = "true" *)wire din_128b_last;
always @(posedge sig_clk) begin
	if (rst) begin
        cnt_128b <=0;
	end
	else begin
		if (din_valid) begin
		  cnt_128b <= cnt_128b+1'b1;
		end
		else begin
		  cnt_128b <= cnt_128b;
		end
	end
end		
//assign din_128b_last = (cnt_128b==sig_number_128b)? 1'b1 : 0;


//The tkeep bits for transfers for all except the last data transfer of a packet must be all 1s.
//Onthe last transfer of a packet, when tlast is asserted, 
//you can specify a tkeep that is not all 1sto specify a data cycle that is not the full datapath width. 
//The asserted tkeep bits need to bepacked to the lsb, indicating contiguous data.
reg [127:0] fifo_din='b0;
reg [127:0] pre_fifo_din='b0;
reg [15:0]	fifo_mask='b0;
reg [15:0]	pre_fifo_mask='b0;
reg wr_en=1'b0;
reg last_flag;
reg	[MSB_DIN_PER_MIG-1:0]	cnt_block = DIN_PER_MIG-1'b1;

reg [6:0] din_shift_bit='b0;	// size : 512b => 9bit[8:0] 256b => 8bit[7:0]  128b => 7bit[6:0]
reg [3:0] mask_shift_bit='b0;	// size : 64b => 6bit[5:0] 32b => 5bit[4:0] 16b => 4bit[3:0]

always @(posedge sig_clk) begin
	if (rst) begin
		fifo_din		<= 'b0;
		fifo_mask		<= 'b0;
		pre_fifo_din	<= 'b0;
		pre_fifo_mask	<= 'b0;
		wr_en			<= 1'b0;
		cnt_block		<= DIN_PER_MIG-1'b1;
		din_shift_bit	<= 'b0;
		mask_shift_bit	<= 'b0;
		last_flag		<= 'b0;
	end
	else begin
		if (din_valid) begin
			if (cnt_block == 'b0 ) begin //|| din_128b_last
				pre_fifo_din	<= 'b0;			// pre buffer μ΄κΈ°?
				pre_fifo_mask	<= 'b0;			// pre buffer μ΄κΈ°?
				fifo_din		<= pre_fifo_din | (din << din_shift_bit);
				fifo_mask		<= pre_fifo_mask | ({MASK_WIDTH{1'b1}} << mask_shift_bit);
				wr_en			<= 1'b1;		// κ°??κ°? μ°¨κ±°? λ§μ?λ§? ?°?΄?°?΄λ©? FIFO WrEn
				cnt_block		<= DIN_PER_MIG-1'b1;
				din_shift_bit	<= 'b0;			// din shift bit
				mask_shift_bit	<= 'b0;
				last_flag		<= 'b0;
			end
			else begin
				pre_fifo_din	<= pre_fifo_din | (din << din_shift_bit); // din? shift ?΄? OR.
				pre_fifo_mask	<= pre_fifo_mask | ({MASK_WIDTH{1'b1}} << mask_shift_bit);
				fifo_din		<= fifo_din;
				fifo_mask		<= fifo_mask;
				wr_en			<= 1'b0;
				cnt_block		<= cnt_block -1'b1;
				din_shift_bit	<= din_shift_bit + DIN_WIDTH;	// add din shift bit
				mask_shift_bit	<= mask_shift_bit + MASK_WIDTH;
				last_flag		<= 1'b0;
			end
		end
		else begin
			pre_fifo_din	<= pre_fifo_din;
			pre_fifo_mask	<= pre_fifo_mask;
			fifo_din		<= fifo_din;
			fifo_mask		<= fifo_mask;
			wr_en			<= 1'b0;
			cnt_block		<= cnt_block;
			din_shift_bit	<= din_shift_bit;
			mask_shift_bit	<= mask_shift_bit;
			last_flag		<= 1'b0;
		end
	end

end

//----------------------------------------------------- ------------------------
wire full_afifo;
wire empty_afifo;
wire re_afifo;
wire [127:0] do_afifo;
wire dov_afifo;

wire dir_bfifo;
assign din_ready = !full_afifo;
assign re_afifo =   dir_bfifo && ~empty_afifo;
// Clock across FIFO
fifod_128x16 fifod_128x16 (
	.rst(rst),						// I
	.wr_clk(sig_clk),					// I
	.rd_clk(pcie_clk),				// I
	.din(fifo_din),					// I [127:0]
	.wr_en(wr_en),					// I
	.rd_en(re_afifo),				// I
	.dout(do_afifo),				// O [127:0]
	.full(full_afifo),				// O
	.empty(empty_afifo),			// O
	.valid(dov_afifo)				// O
);

wire [16:0] fifo_mask_out;
fifod_17x16 fifod_17x16 (
  .rst(rst),					// I
  .wr_clk(sig_clk),				// I
  .rd_clk(pcie_clk),			// I
  .din({last_flag,fifo_mask}),	// I [16 : 0]   .din({last_flag,fifo_mask}),	// I [16 : 0]
  .wr_en(wr_en),				// I
  .rd_en(re_afifo),				// I
  .dout(fifo_mask_out),			// O [16 : 0]
  .full(),						// O
  .empty(),						// O
  .valid()						// O
);


// 128b_last signal  sig clk
reg [14:0] cnt_128b_pci;
 //(* mark_debug = "true" *)wire din_128b_last_pci;
always @(posedge pcie_clk) begin
	if (rst) begin
        cnt_128b_pci <=0;
	end
	else begin
		if (dov_afifo) begin
		  cnt_128b_pci <= cnt_128b_pci+1'b1;
		end
		else begin
		  cnt_128b_pci  <= cnt_128b_pci;
		end
	end
end		
//assign din_128b_last_pci = (cnt_128b_pci==sig_number_128b)? 1'b1 : 0;

wire empty_bfifo;
wire full_bfifo;
wire re_bfifo;

assign dir_bfifo		= !full_bfifo ;
assign re_bfifo	= dout_ready && ~empty_bfifo;
fifod_128x16_fwft fifod_128x16_fwft (
	.clk(pcie_clk),							// I
	.rst(rst),								// I
	.din(do_afifo),							// I [31:0]
	.wr_en(dov_afifo),						// I
	.rd_en(re_bfifo),					// I
	.dout(dout),							// O [31:0]
	.prog_full_thresh(i_dma_buf_prog_thresh),               // I [13:0]
	.full(full_bfifo),				// O
	.prog_full(prog_bfull),        // O
	.empty(empty_bfifo),			// O
	.valid(dout_valid)						// O

);
// 128b_last signal pci clk
reg [14:0] cnt_128b_todma;
 //(* mark_debug = "true" *)wire din_128b_last;
always @(posedge pcie_clk) begin
	if (rst) begin
        cnt_128b_todma <=0;
	end
	else begin
		if (re_bfifo) begin
		  cnt_128b_todma <= cnt_128b_todma+1'b1;
		end
		else begin
		  cnt_128b_todma <= cnt_128b_todma;
		end
	end
end		

fifod_17x16_fwft fifod_17x16_fwft (
	.clk(pcie_clk),						// I
	.rst(rst),							// I
	.din(fifo_mask_out),				// I [16:0]
	.wr_en(dov_afifo),					// I
	.rd_en(re_bfifo),				// I
	.dout({dout_last,dout_keep}),		// O [16:0]
	.prog_full(),
	.full(),							// O
	.empty(),							// O
	.valid()							// O
);


//----------------------------------------------------- ----------------------------    
endmodule
