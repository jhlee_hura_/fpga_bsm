`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: HURA
// Engineer: YUN Hojun (yhj@hura.co.kr)
//
// Create Date: 2021/09/09
// Design Name: DRBG_bit_generation
// Module Name: entropy_gen
// Project Name:
// Target Devices: xc7k160tffg676-1
// Tool Versions: Vivado 2018.1
// Description:
//
// Dependencies:
//
// Revision: 
// Additional Comments: "Logic will get you from A to B. Imagination will take you everywhere." - Albert Einstein
//
// Copyright 2021, All rights reserved by HURA
//////////////////////////////////////////////////////////////////////////////////



module entropy_gen (
	
	input   wire       clk,   //100MHz
    input   wire       sclr,
	
	input   wire       drbg_mode,
	input 	wire       spd_trig,
	input 	wire       spd_trig_valid,  //10MHz, pulse width:10ns
	
	output reg [255:0] o_etp_gen,
    output reg         o_etp_gen_valid  //10MHz, pulse width:10ns
	
);




//--------------------------Making index-----------------------------------------
//reg clk_100M_cnt = 4'd0;
//reg clk_10M = 1'b1;
reg [1:0]index_cnt = 2'b11;    // from 11 to 00 counter
reg      index = 1'b1;   // 1, 1, 0, 0 cycle index

reg [7:0] bit_cnt = 8'b0;

reg [7:0] o_valid_cnt = 8'b0;

always @(posedge clk) begin
	if (sclr) begin

	   index_cnt <= 2'b11;
	   index <= 1'b1;
	   o_etp_gen <= 8'b0;
       bit_cnt <= 8'b0;
       o_valid_cnt <= 8'b0; 
	end
	
	else begin

          
	   if (spd_trig_valid && drbg_mode) begin
	       index_cnt <= (index_cnt == 2'b00) ? 2'b11 : index_cnt - 2'b1;
	       index <= (index_cnt == 2'b11)? 1'b1 :
                    (index_cnt == 2'b00)? 1'b1 : 1'b0;
            if (spd_trig) begin
                    o_etp_gen <= {o_etp_gen[254:0],index};
                    bit_cnt <= (bit_cnt == 255)? 8'b0 : bit_cnt +1; 
                    o_etp_gen_valid <= (bit_cnt == 255)? 1'b1 : 1'b0;   
                    o_valid_cnt <= (bit_cnt == 255)? o_valid_cnt +1 : o_valid_cnt;  
            end
            else begin
                    o_etp_gen <= o_etp_gen;
                    bit_cnt <= bit_cnt; 
                    o_etp_gen_valid <= 1'b0;
                    o_valid_cnt <= o_valid_cnt; 
            end
       end
       else begin
           index_cnt <= index_cnt;
           index <= index;
           o_etp_gen_valid <= 1'b0;
           o_valid_cnt <= o_valid_cnt; 

       end 
	  
    end  
end  
//-------------------------------------------------------------------------------



endmodule