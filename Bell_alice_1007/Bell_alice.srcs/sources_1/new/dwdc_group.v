`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: HURA
// Engineer: YUN Hojun (yhj@hura.co.kr)
//
// Create Date: 2021/09/09
// Design Name: DRBG_bit_generation
// Module Name: dwdc_group
// Project Name:
// Target Devices: xc7k160tffg676-1
// Tool Versions: Vivado 2018.1
// Description:
//
// Dependencies:
//
// Revision: 
// Additional Comments: "Logic will get you from A to B. Imagination will take you everywhere." - Albert Einstein
//
// Copyright 2021, All rights reserved by HURA
//////////////////////////////////////////////////////////////////////////////////


module dwdc_group(
    input wire clk,
    input wire sclr,
    
    input wire [255:0] din_01,
    input wire [255:0] din_02,
    input wire [255:0] din_03,
    input wire [255:0] din_04,
    input wire [255:0] din_05,
    input wire [255:0] din_06,
    input wire [255:0] din_07,
    input wire [255:0] din_08,
    input wire [255:0] din_09,
    input wire [255:0] din_10,
    
    input wire i_valid_dwdc_01,
    input wire i_valid_dwdc_02,
    input wire i_valid_dwdc_03,
    input wire i_valid_dwdc_04,
    input wire i_valid_dwdc_05,
    input wire i_valid_dwdc_06,
    input wire i_valid_dwdc_07,
    input wire i_valid_dwdc_08,
    input wire i_valid_dwdc_09,
    input wire i_valid_dwdc_10,
    
    input wire dout_ready_01,
    input wire dout_ready_02,
    input wire dout_ready_03,
    input wire dout_ready_04,
    input wire dout_ready_05,
    input wire dout_ready_06,
    input wire dout_ready_07,
    input wire dout_ready_08,
    input wire dout_ready_09,
    input wire dout_ready_10,
    
    output wire ready_dwdc_01,
    output wire ready_dwdc_02,
    output wire ready_dwdc_03,
    output wire ready_dwdc_04,
    output wire ready_dwdc_05,
    output wire ready_dwdc_06,
    output wire ready_dwdc_07,
    output wire ready_dwdc_08,
    output wire ready_dwdc_09,
    output wire ready_dwdc_10,
    
    output wire o_dwdc_01,
    output wire o_dwdc_02,
    output wire o_dwdc_03,
    output wire o_dwdc_04,
    output wire o_dwdc_05,
    output wire o_dwdc_06,
    output wire o_dwdc_07,
    output wire o_dwdc_08,
    output wire o_dwdc_09,
    output wire o_dwdc_10,
    
    output wire o_valid_dwdc_01,
    output wire o_valid_dwdc_02,
    output wire o_valid_dwdc_03,
    output wire o_valid_dwdc_04,
    output wire o_valid_dwdc_05,
    output wire o_valid_dwdc_06,
    output wire o_valid_dwdc_07,
    output wire o_valid_dwdc_08,
    output wire o_valid_dwdc_09,
    output wire o_valid_dwdc_10   
);




//---------------------------data_width_downconvert------------------------------
datawidth_256to1_downconverter datawidth_downconverter_01 (
	.clk(clk),				     // I
	.sclr(sclr),				 // I 
	.din(din_01),		         // I [DIN_WIDTH-1:0]
	.din_valid(i_valid_dwdc_01),	             // I
	.din_ready(ready_dwdc_01),	 // O

	.dout(o_dwdc_01),			 // O [DOUT_WIDTH-1:0]
	.dout_valid(o_valid_dwdc_01),// O
	.dout_ready(dout_ready_01)		     // I
);//-------------------------------------------------------------------------------
datawidth_256to1_downconverter datawidth_downconverter_02 (
	.clk(clk),				// I
	.sclr(sclr),				// I 
	.din(din_02),		// I [DIN_WIDTH-1:0]
	.din_valid(i_valid_dwdc_02),	// I
	.din_ready(ready_dwdc_02),		// O

	.dout(o_dwdc_02),			// O [DOUT_WIDTH-1:0]
	.dout_valid(o_valid_dwdc_02),	// O
	.dout_ready(dout_ready_02)		// I
);
//-------------------------------------------------------------------------------
datawidth_256to1_downconverter datawidth_downconverter_03 (
	.clk(clk),				// I
	.sclr(sclr),				// I 
	.din(din_03),		// I [DIN_WIDTH-1:0]
	.din_valid(i_valid_dwdc_03),	// I
	.din_ready(ready_dwdc_03),		// O

	.dout(o_dwdc_03),			// O [DOUT_WIDTH-1:0]
	.dout_valid(o_valid_dwdc_03),	// O
	.dout_ready(dout_ready_03)		// I
);
//-------------------------------------------------------------------------------
datawidth_256to1_downconverter datawidth_downconverter_04 (
	.clk(clk),				// I
	.sclr(sclr),				// I 
	.din(din_04),		// I [DIN_WIDTH-1:0]
	.din_valid(i_valid_dwdc_04),	// I
	.din_ready(ready_dwdc_04),		// O

	.dout(o_dwdc_04),			// O [DOUT_WIDTH-1:0]
	.dout_valid(o_valid_dwdc_04),	// O
	.dout_ready(dout_ready_04)		// I
);
//-------------------------------------------------------------------------------
datawidth_256to1_downconverter datawidth_downconverter_05 (
	.clk(clk),				// I
	.sclr(sclr),				// I 
	.din(din_05),		// I [DIN_WIDTH-1:0]
	.din_valid(i_valid_dwdc_05),	// I
	.din_ready(ready_dwdc_05),		// O

	.dout(o_dwdc_05),			// O [DOUT_WIDTH-1:0]
	.dout_valid(o_valid_dwdc_05),	// O
	.dout_ready(dout_ready_05)		// I
);
//-------------------------------------------------------------------------------
datawidth_256to1_downconverter datawidth_downconverter_06 (
	.clk(clk),				// I
	.sclr(sclr),				// I 
	.din(din_06),		// I [DIN_WIDTH-1:0]
	.din_valid(i_valid_dwdc_06),	// I
	.din_ready(ready_dwdc_06),		// O

	.dout(o_dwdc_06),			// O [DOUT_WIDTH-1:0]
	.dout_valid(o_valid_dwdc_06),	// O
	.dout_ready(dout_ready_06)		// I
);
//-------------------------------------------------------------------------------
datawidth_256to1_downconverter datawidth_downconverter_07 (
	.clk(clk),				// I
	.sclr(sclr),				// I 
	.din(din_07),		// I [DIN_WIDTH-1:0]
	.din_valid(i_valid_dwdc_07),	// I
	.din_ready(ready_dwdc_07),		// O

	.dout(o_dwdc_07),			// O [DOUT_WIDTH-1:0]
	.dout_valid(o_valid_dwdc_07),	// O
	.dout_ready(dout_ready_07)		// I
);
//-------------------------------------------------------------------------------
datawidth_256to1_downconverter datawidth_downconverter_08 (
	.clk(clk),				// I
	.sclr(sclr),				// I 
	.din(din_08),		// I [DIN_WIDTH-1:0]
	.din_valid(i_valid_dwdc_08),	// I
	.din_ready(ready_dwdc_08),		// O

	.dout(o_dwdc_08),			// O [DOUT_WIDTH-1:0]
	.dout_valid(o_valid_dwdc_08),	// O
	.dout_ready(dout_ready_08)		// I
);
//-------------------------------------------------------------------------------
datawidth_256to1_downconverter datawidth_downconverter_09 (
	.clk(clk),				// I
	.sclr(sclr),				// I 
	.din(din_09),		// I [DIN_WIDTH-1:0]
	.din_valid(i_valid_dwdc_09),	// I
	.din_ready(ready_dwdc_09),		// O

	.dout(o_dwdc_09),			// O [DOUT_WIDTH-1:0]
	.dout_valid(o_valid_dwdc_09),	// O
	.dout_ready(dout_ready_09)		// I
);
//-------------------------------------------------------------------------------
datawidth_256to1_downconverter datawidth_downconverter_10 (
	.clk(clk),				// I
	.sclr(sclr),				// I 
	.din(din_10),		// I [DIN_WIDTH-1:0]
	.din_valid(i_valid_dwdc_10),	// I
	.din_ready(ready_dwdc_10),		// O

	.dout(o_dwdc_10),			// O [DOUT_WIDTH-1:0]
	.dout_valid(o_valid_dwdc_10),	// O
	.dout_ready(dout_ready_10)		// I
);
//-------------------------------------------------------------------------------



endmodule
