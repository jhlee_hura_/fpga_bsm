`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: HURA
// Engineer: YUN Hojun (yhj@hura.co.kr)
//
// Create Date: 2021/09/09
// Design Name: DRBG_bit_generation
// Module Name: datawidth_256to32_downconverter
// Project Name:
// Target Devices: xc7k160tffg676-1
// Tool Versions: Vivado 2018.1
// Description:
//
// Dependencies:
//
// Revision: 
// Additional Comments: "Logic will get you from A to B. Imagination will take you everywhere." - Albert Einstein
//
// Copyright 2021, All rights reserved by HURA
//////////////////////////////////////////////////////////////////////////////////
module datawidth_256to32_downconverter (
	input wire 			clk,
	input wire 			sclr,
	input wire [255:0]	din,
	input wire 			din_valid,
	output wire 		din_ready,

	output reg [31:0]	dout,
	output reg			dout_valid,
	input wire			dout_ready
);





reg [7:0] dout_valid_cnt;
reg [255:0] din_store;
reg empty;
reg [7:0] bit_shift;
reg din_flag;

assign din_ready = empty;    // 앞선 din이 모두 사용되면 din_ready가 1인 된다.



always @(posedge clk) begin
    if (sclr) begin
        din_store <= 1'b0;
        din_flag <= 1'b0;
        dout <= 1'b0;
        dout_valid <= 1'b0;
        dout_valid_cnt <= 1'b0;
        bit_shift <= 1'b0;
        empty <= 1'b1;
    end
    else begin
        if (din_valid && empty) begin
            din_store <= din;
            din_flag <= 1'b1;
            dout <= 1'b0;
            dout_valid <= 1'b0;
            dout_valid_cnt <= 1'b0;
            bit_shift <= 8'd224;
            empty <= 1'b0;
        end 
        else if (dout_ready && din_flag) begin
            dout <= (din_store >> bit_shift);
            dout_valid <= 1'b1;
            dout_valid_cnt <= (dout_valid_cnt == 7)? 1'b0 : dout_valid_cnt +1;
            bit_shift <= (bit_shift == 0)? 8'd224 : bit_shift -32;
            empty <= (bit_shift == 0)? 1'b1 : empty;
            din_flag <= (bit_shift == 0)? 1'b0 : din_flag; 
        end
        else begin
            dout <= dout;
            dout_valid <= 1'b0;
            dout_valid_cnt <= dout_valid_cnt;
            bit_shift <= bit_shift;
            empty <= empty;
            din_flag <= din_flag; 
        end
    end
end



endmodule
