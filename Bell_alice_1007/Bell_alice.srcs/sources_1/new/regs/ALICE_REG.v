//////////////////////////////////////////////////////////////////////////////////
// Company: HURA
// Engineer: LEE Seong Yun (sylee@hura.co.kr)
//
// Create Date: 2019/01/02 11:05:02
// Design Name:
// Module Name: ALICE_REG
// Project Name: QKD
// Target Devices: Kintex 7 160T
// Tool Versions: Vivado 2018.1
// Description:
//
// Dependencies:
//
// Revision:
//		build. 6(2019/02/27)
// Additional Comments:
//
// Copyright 2018, All rights reserved by HURA
//////////////////////////////////////////////////////////////////////////////////
// ---------------------------------------------------------------------------------
// In Octave, to show a Board name, version, copyrighter 
// Example) BOARD_MAKER, BORADNAME, COPYRIGHTER, PRODUCT 
// a='QKD Alice Board(HR-QKD-ALICE-01)';
// len_b=ceil(length(a)/4)*4; b=zeros(1,len_b);b(1:length(a))=a;
// c=dec2hex(typecast(uint8(double(b)),'uint32'))
//

// ---------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------
// Board and FW Constant
`define BSC_VERSION_DATE_DATA			32'h1DA00008	// dec2hex(fix(now)-datenum(2000,01,01))
`define BOARD_MAKER						32'h41525548	// HURA
`define BOARD_NAME						    32'h204D5342    // BSM A/B Board(HR-BSM-A/B-01)
`define BOARD_NAME1						32'h20422F41
`define BOARD_NAME2						32'h72616F42
`define BOARD_NAME3						32'h52482864
`define BOARD_NAME4						32'h4D53422D
`define BOARD_NAME5						32'h422F412D
`define BOARD_NAME6						32'h2931302D
`define BOARD_NAME7						32'h00000000
`define BOARD_NAME8						32'h00000000
`define BOARD_NAME9						32'h00000000
`define BOARD_NAME10					32'h00000000
`define BOARD_NAME11					32'h00000000

`define VERSION_DATE_DATA				32'h1DA00008	// dec2hex(fix(now)-datenum(2000,01,01))
`define COPYRIGHTER						32'h2052534E	// NSR
`define PRODUCT						32'h204D5342       // BSM A/B Testbed
`define PRODUCT1						32'h20422F41
`define PRODUCT2						32'h74736554
`define PRODUCT3						32'h00646562
`define PRODUCT4						32'h00000064
`define PRODUCT5						32'h00000000
`define PRODUCT6						32'h00000000
`define PRODUCT7						32'h00000000
`define PRODUCT8						32'h00000000
`define PRODUCT9						32'h00000000
`define PRODUCT10						32'h00000000
`define PRODUCT11						32'h00000000
