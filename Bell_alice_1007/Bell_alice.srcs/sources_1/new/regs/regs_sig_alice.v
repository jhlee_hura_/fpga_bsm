`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: HURA
// Engineer:  LEE Seong Yun (sylee@hura.co.kr)
//
// Create Date: 2019/01/15 21:04:48
// Design Name: register for signal clock domain
// Module Name: regs_sig_alice
// Project Name: QKD
// Target Devices: Any Device
// Tool Versions: Vivado 2018.1
// Description:
//
// Dependencies:
//
// Revision:
//		build 1 (2019/01/15)
// Additional Comments:
//
// Copyright 2019, All rights reserved by HURA
//////////////////////////////////////////////////////////////////////////////////
module regs_sig_alice (
	input	wire				clk,		// sig_clk
	input	wire				rst,		// because of multi-clk
	input	wire				pcie_clk,

	input	wire				regs_sel,
	input	wire	[46:0]		regs_adda,
	input	wire				regs_adda_valid,
	output	wire	[31:0]		regs_dout,
	output	wire				regs_dout_valid,

	// -------------------------------------------------------
	// -- register ? λ³?
	// --------------------------------------------------------
	// CON 1 LD1530

	// A1_TEST_MODE_REG
    output reg                          a1_test_mode = 1'b0,
    // A1_TEST_DATA_REG
    output reg    [31:0]                a1_test_data= 32'hffffffff,
                       
    // A1_WIDTH_REG
    output reg    [5:0]                 a1_width= 6'd1,
    output reg                          a1_shape_load= 1'b0,    
    // A1ADDWIDTH_REG
    output reg    [7:0]                 a1_add_width= 8'd1,    
    // A1_OFFSET_QUOT_REG
    output reg    [6:0]                 a1_i_offset_quotient,
    // A1_OFFSET_REMD_REG
    output reg    [2:0]                 a1_i_offset_remainder,
    // A1_DELAY_TAP_REG
    output reg    [4:0]                 a1_delay_tap= 5'b0,
    // A1_ADD_PULSE_DELAY_REG
    output reg    [9:0]                 o_a1_delay_cnt,     

	
	
	
	
	// --------------------------------------------------------
	// CON 2 SPAD
    // A2_TEST_MODE_REG
    output reg                            a2_test_mode,
    // A2_TEST_DATA_REG
    output reg    [31:0]                    a2_test_data,
    // A2_WIDTH_REG
    output reg    [5:0]                    a2_width,
    output reg                            a2_shape_load,
	// A2_OFFSET_QUOT_REG
    output reg    [6:0]                a2_i_offset_quotient,
    // A2_OFFSET_REMD_REG
    output reg    [2:0]                a2_i_offset_remainder,
    // A2_DELAY_TAP_REG
    output reg    [4:0]                    a2_delay_tap,
	// A2_DETECT_DELAY_TAP_REG
    output reg  [4:0]                  a2_detect_delay_tap,    
    // A2_SPD_CNT1_REG
    input wire    [31:0]               a2_spad_cnt1,
    // A2_SPD_CNT2_REG
    input wire    [31:0]               a2_spad_cnt2,
    // A2_SPD_CNT3_REG
    input wire  [31:0]                 a2_spad_cnt3,
     // A2_SPD_CNT4_REG
    input wire  [31:0]                 a2_spad_cnt4,    
// --------------------------------------------------------
// CON 3 IM
	// A3_TEST_MODE_REG
    output reg                            a3_test_mode,
    // A3_TEST_L_DATA_REG
    output reg    [31:0]                    a3_test_l_data,
    // A3_TEST_H_DATA_REG
    output reg    [31:0]                    a3_test_h_data,
    // A3_WIDTH_REG
    output reg    [3:0]                    a3_width,
    output reg                            a3_shape_load,
    // A3_OFFSET_REG
    output reg    [8:0]                    a3_offset,

    // A3_V0_REG
    output reg    [11:0]                    a3_i_dac_v0,
    // A3_V1_P_REG
    output reg    [11:0]                    a3_i_dac_v1_p,
    // A3_V2_P_REG
    output reg    [11:0]                    a3_i_dac_v2_p,
    // A3_V3_P_REG
    output reg    [11:0]                    a3_i_dac_v3_p,
    // A3_V1_N_REG
    output reg    [11:0]                    a3_i_dac_v1_n,
    // A3_V2_N_REG
    output reg    [11:0]                    a3_i_dac_v2_n,
    // A3_V3_N_REG
    output reg    [11:0]                    a3_i_dac_v3_n,    
// --------------------------------------------------------
// CON 4
 	// A4_TEST_MODE_REG
    output reg                            a4_test_mode,
    // A4_TEST_L_DATA_REG
    output reg    [31:0]                    a4_test_l_data,
    // A4_TEST_H_DATA_REG
    output reg    [31:0]                    a4_test_h_data,
    // A4_WIDTH_REG
    output reg    [3:0]                    a4_width,
    output reg                            a4_shape_load,
    // A4_OFFSET_REG
    output reg    [8:0]                    a4_offset,

    // A4_V0_REG
    output reg    [11:0]                    a4_i_dac_v0,
    // A4_V1_P_REG
    output reg    [11:0]                    a4_i_dac_v1_p,
    // A4_V2_P_REG
    output reg    [11:0]                    a4_i_dac_v2_p,
    // A4_V3_P_REG
    output reg    [11:0]                    a4_i_dac_v3_p,
    // A4_V1_N_REG
    output reg    [11:0]                    a4_i_dac_v1_n,
    // A4_V2_N_REG
    output reg    [11:0]                    a4_i_dac_v2_n,
    // A4_V3_N_REG
    output reg    [11:0]                    a4_i_dac_v3_n,    

// --------------------------------------------------------
// CON 5 IM
	// A5_TEST_MODE_REG
    output reg                            a5_test_mode,
    // A5_TEST_L_DATA_REG
    output reg    [31:0]                    a5_test_l_data,
    // A5_TEST_H_DATA_REG
    output reg    [31:0]                    a5_test_h_data,
    // A5_WIDTH_REG
    output reg    [3:0]                    a5_width,
    output reg                            a5_shape_load,
    // A5_OFFSET_REG
    output reg    [8:0]                    a5_offset,

    // A5_V0_REG
    output reg    [11:0]                    a5_i_dac_v0,
    // A5_V1_P_REG
    output reg    [11:0]                    a5_i_dac_v1_p,
    // A5_V2_P_REG
    output reg    [11:0]                    a5_i_dac_v2_p,
    // A5_V3_P_REG
    output reg    [11:0]                    a5_i_dac_v3_p,
    // A5_V1_N_REG
    output reg    [11:0]                    a5_i_dac_v1_n,
    // A5_V2_N_REG
    output reg    [11:0]                    a5_i_dac_v2_n,
    // A5_V3_N_REG
    output reg    [11:0]                    a5_i_dac_v3_n,    
    
// --------------------------------------------------------
// CON 6
	 
     // A6_PD12_CNT_REG    
     input wire    [31:0]                    a6_PD12_cnt,
     // A6_PDTRIG_CNT_REG
     input wire [31:0]                  a6_PD_trig_cnt,
	// A6_START_WIDTH_THRES_REG
     output  reg [3:0]                    a6_start_width_thres,
	// --------------------------------------------------------
	// CON 7

	// --------------------------------------------------------
	// CON 8


	// --------------------------------------------------------
	// CON 9
	
	// A9_TEST_MODE_REG
    output reg                         a9_test_mode,
    // A9_TEST_DATA_REG
    output reg    [31:0]               a9_test_data,
    // A9_WIDTH_REG
    output reg    [5:0]                a9_width,
    output reg                         a9_shape_load,
    // A9_OFFSET_QUOT_REG
    output reg    [6:0]                a9_i_offset_quotient,
    // A9_OFFSET_REMD_REG
    output reg    [2:0]                a9_i_offset_remainder,
    // A9_DELAY_TAP_REG
    output reg    [4:0]                a9_delay_tap,
    // A9_SIG_POLA_REG
    output reg                         a9_sig_pola = 1'b1,
	
	
// --------------------------------------------------------
	// CON 10 PM Half
	// A10_TEST_MODE_REG
	output reg							a10_test_mode,
	// A10_TEST_DATA_REG
	output reg	[31:0]					a10_test_data,
	// A10_WIDTH_REG
	output reg	[5:0]					a10_width,
	output reg							a10_shape_load,
	// A10_OFFSET_QUOT_REG
    output reg    [6:0]                a10_i_offset_quotient,
    // A10_OFFSET_REMD_REG
    output reg    [2:0]                a10_i_offset_remainder,
	// A10_DELAY_TAP_REG
	output reg	[4:0]					a10_delay_tap,
	// A10_SIG_POLA_REG
    output reg                          a10_sig_pola = 1'b1,
    // A_FAKE_START_REG
    output reg                          op_fake_start,
	// A_READY_REG
	output	reg							op_ready,
	input	wire						op_busy,
	// A_STOP_REG
	output	reg							op_stop,
	// A_SNAPSHOT_REG
	output	reg	[20:0]					op_snapshot,
	// A_READY_ERR_REG
	output	reg 						ready_err_flag_clear,
	input	wire [5:0]					ready_err_flag,
	// A_FIFO_STAT_REG
	output   wire				        fifo_rst,
	
	// A_FIFO_PF_THRES_REG
	// A_FIFO_READ_REG
	(* mark_debug = "true" *)input	wire	[31:0]				din_fifo,
	(* mark_debug = "true" *)input	wire						div_fifo,
	output	wire						dir_fifo,

	// A_SEED_REG
	output	reg		[31:0]				seed,
	output	reg							seed_valid,
	// A_SIG_THRES1_REG
	output	reg		[7:0]				sig_thres1,
	// A_SIG_THRES2_REG
	output	reg		[7:0]				sig_thres2,
	
	// A_TRIG_TYPE_REG
    output reg [1:0]                    a_trig_type,
	//
	input	wire	[7:0]				emp_log,
	
//-------------------------------------drbg--------------------------------------
    // A_DRBG_MODE_REG
    output reg        drbg_mode,
    // A_ETP_CNT_REG
    input wire [31:0] etp_cnt,
    // A_DRBG_CNT_REG
    input wire [31:0] drbg_cnt,
    // A_ETP_DRBG_FIFO_STAT_REG
    input wire        empty_etp_fifo,
    input wire        empty_drbg_fifo_group,
    input wire        full_etp_veri,
    input wire        full_drbg_veri,
    // A_ETP_VERI_READ_REG
    output wire       rd_en_etp_veri,
    input wire [31:0] o_etp_veri,
    // A_DRBG_VERI_READ_REG
    output wire       rd_en_drbg_veri,
    input wire [31:0] o_drbg_veri,    
    //-------------------------------------------------------------------------------
	
	// A_SIG_TEST_MODE_REG
    output reg           a_64b1_test_mode,
    // A_SIG0_TEST_DATA_REG    
    output reg [31:0]    a_64b1_s0_test_data,
    // A_SIG1_TEST_DATA_REG    
    output reg [31:0]    a_64b1_s1_test_data,
    
     // A_DB1_D_TEST_MODE_REG   
    output reg           a_64b2_d_test_mode,
     // A_DB1_B_TEST_MODE_REG      
    output reg           a_64b2_b_test_mode,    
     // A_DB1_B_TEST_DATA_REG    
    output reg [31:0]    a_64b2_b_test_data,
    // A_DB1_D_TEST_DATA_REG    
    output reg [31:0]    a_64b2_d_test_data,

     // A_DB2_D_TEST_MODE_REG   
    output reg           a_64b3_d_test_mode,
     // A_DB2_B_TEST_MODE_REG       
    output reg           a_64b3_b_test_mode,    
     // A_DB2_B_TEST_DATA_REG    
    output reg [31:0]    a_64b3_b_test_data,
    // A_DB2_B_TEST_DATA_REG    
    output reg [31:0]    a_64b3_d_test_data,
    
     // A_PM1_TEST_MODE_REG   
    output reg           a_32b1_test_mode, // 32 bit
     // A_PM1_TEST_DATA_REG   
    output reg [31:0]    a_32b1_test_data,
    
     // A_PM2_TEST_MODE_REG   
    output reg           a_32b2_test_mode,
     // A_PM2_TEST_DATA_REG   
    output reg [31:0]    a_32b2_test_data


);

wire	[46:0]	fifo_dout;
wire			regin_empty;
wire			regout_empty;
reg				regout_we;
reg				regout_we1;
reg				regout_we2;

wire	[15:0]	addr;
wire			wr_en;
wire	[31:0]	wr_d;
reg		[31:0]	rd_d;
wire			regin_valid;

// from pcie block
fifo_regin_47x16 regin_fifo (
  .rst(rst),							// I
  .wr_clk(pcie_clk),					// I
  .rd_clk(clk),							// I
  .din(regs_adda),						// I [46 : 0]
  .wr_en(regs_sel & regs_adda_valid),	// I
  .rd_en(~regin_empty),					// I
  .dout(fifo_dout),						// O [46 : 0]
  .full(),								// O
  .empty(regin_empty),					// O
  .valid(regin_valid)					// O
);

// to pcie block
fifo_regout_32x16 regout_fifo (
  .rst(rst),							// I
  .wr_clk(clk),							// I
  .rd_clk(pcie_clk),					// I
  .din(rd_d),				// I [31 : 0]
  .wr_en(regout_we),					// I
  .rd_en(~regout_empty),				// I
  .dout(regs_dout),						// O [31 : 0]
  .full(),								// O
  .empty(regout_empty),					// O
  .valid(regs_dout_valid)				// O
);


wire reg_re;
(* mark_debug = "true" *)wire fifo_rd_en;
assign	reg_re = regin_valid & (~fifo_dout[46]);
assign	fifo_rd_en = (addr == `A_FIFO_READ_REG) & reg_re;

assign rd_en_etp_veri = (addr == `A_ETP_VERI_READ_REG) & reg_re;
assign rd_en_drbg_veri = (addr == `A_DRBG_VERI_READ_REG) & reg_re;


// fifo latency ?λ¬Έμ 2+1 = 3 clk.
always @(posedge clk) begin
	regout_we1	<= reg_re;
	regout_we2	<= regout_we1;
	regout_we	<= regout_we2;
end

// fifo[46]= 1:wr_en, 0:rd_en
// fifo[45:32] = 14bits addr
// fifo[31:0] = 32bits data
assign wr_en	= regin_valid & fifo_dout[46];
assign addr		= {fifo_dout[45:32], 2'b0};
assign wr_d		= fifo_dout[31:0];

wire [31:0]		ff_out;
reg	[15:0]		fifo_pf_thres;

(* mark_debug = "true" *)reg				fifo_en;
(* mark_debug = "true" *)wire				fifo_empty;
(* mark_debug = "true" *)wire				fifo_pf;
(* mark_debug = "true" *)wire				fifo_af;
(* mark_debug = "true" *)wire				fifo_full;
(* mark_debug = "true" *)reg				fifo_rst_r;

always @(posedge clk) begin
	if (rst)	begin
		rd_d					<= 32'b0;

		// CON 1
		// A1_TEST_MODE_REG
		a1_test_mode 			<= 1'b0;
		// A1_TEST_DATA_REG
		a1_test_data			<= 32'hffffffff;
		// A1_WIDTH_REG
		a1_width				<= 6'd1;
		a1_shape_load			<= 1'b0;
		//A6_ADDWIDTH_REG
        a1_add_width            <= 8'd1;
		// A1_OFFSET_QUOT_REG
        a1_i_offset_quotient    <= 7'b0;
        // A1_OFFSET_REMD_REG
        a1_i_offset_remainder    <= 3'b0;    
		// A1_DELAY_TAP_REG
		a1_delay_tap			<= 5'b0;
		// C6_ADD_PULSE_DELAY_REG
        o_a1_delay_cnt           <= 10'b0;  

		// CON 2
		// A2_TEST_MODE_REG
        a2_test_mode             <= 1'b0;
        // A2_TEST_DATA_REG
        a2_test_data            <= 32'hffffffff;		
		// A2_WIDTH_REG
		a2_width				<= 6'd1;
		a2_shape_load			<= 1'b0;
		// A2_OFFSET_QUOT_REG
        a2_i_offset_quotient    <= 7'b0;
        // A2_OFFSET_REMD_REG
        a2_i_offset_remainder    <= 3'b0;    
		// A2_DELAY_TAP_REG
		a2_delay_tap			<= 5'b0;
		// A2_DETECT_DELAY_TAP_REG
        a2_detect_delay_tap            <= 5'b0;   
// CON 3
		// A3_TEST_MODE_REG
        a3_test_mode             <= 1'b0;
        // A3_TEST_L_DATA_REG
        a3_test_l_data            <= 32'hffffffff;
        // A3_TEST_H_DATA_REG
        a3_test_h_data            <= 32'hffffffff;
        // A3_WIDTH_REG
        a3_width                <= 4'd1;
        a3_shape_load            <= 1'b0;
        // A3_OFFSET_REG
        a3_offset                <= 9'b0;
        // A3_V0_REG
        a3_i_dac_v0              <= 12'b0;
        // A3_V1_P_REG
        a3_i_dac_v1_p             <= 12'b0;
        // A3_V2_P_REG
        a3_i_dac_v2_p             <= 12'b0;
        // A3_V3_P_REG
        a3_i_dac_v3_p             <= 12'b0;
        // A3_V1_N_REG
        a3_i_dac_v1_n             <= 12'b0;
        // A3_V2_N_REG
        a3_i_dac_v2_n             <= 12'b0;
        // A3_V3_N_REG
        a3_i_dac_v3_n             <= 12'b0;        

// CON 4
        // A4_TEST_MODE_REG
        a4_test_mode             <= 1'b0;
        // A4_TEST_L_DATA_REG
        a4_test_l_data            <= 32'hffffffff;
        // A4_TEST_H_DATA_REG
        a4_test_h_data            <= 32'hffffffff;
        // A4_WIDTH_REG
        a4_width                <= 4'd1;
        a4_shape_load            <= 1'b0;
        // A4_OFFSET_REG
        a4_offset                <= 9'b0;
        // A4_V0_REG
        a4_i_dac_v0              <= 12'b0;
        // A4_V1_P_REG
        a4_i_dac_v1_p             <= 12'b0;
        // A4_V2_P_REG
        a4_i_dac_v2_p             <= 12'b0;
        // A4_V3_P_REG
        a4_i_dac_v3_p             <= 12'b0;
        // A4_V1_N_REG
        a4_i_dac_v1_n             <= 12'b0;
        // A4_V2_N_REG
        a4_i_dac_v2_n             <= 12'b0;
        // A4_V3_N_REG
        a4_i_dac_v3_n             <= 12'b0;  		
          
// CON 5
		// A5_TEST_MODE_REG
        a5_test_mode             <= 1'b0;
        // A5_TEST_L_DATA_REG
        a5_test_l_data            <= 32'hffffffff;
        // A5_TEST_H_DATA_REG
        a5_test_h_data            <= 32'hffffffff;
        // A5_WIDTH_REG
        a5_width                <= 4'd1;
        a5_shape_load            <= 1'b0;
        // A5_OFFSET_REG
        a5_offset                <= 9'b0;
        // A5_V0_REG
        a5_i_dac_v0              <= 12'b0;
        // A5_V1_P_REG
        a5_i_dac_v1_p             <= 12'b0;
        // A5_V2_P_REG
        a5_i_dac_v2_p             <= 12'b0;
        // A5_V3_P_REG
        a5_i_dac_v3_p             <= 12'b0;
        // A5_V1_N_REG
        a5_i_dac_v1_n             <= 12'b0;
        // A5_V2_N_REG
        a5_i_dac_v2_n             <= 12'b0;
        // A5_V3_N_REG
        a5_i_dac_v3_n             <= 12'b0;       
// CON 6
	
 		// B_START_WIDTH_THRES_REG
        a6_start_width_thres    <= 4'd8;       
// CON 7

// CON 8

// --------------------------------------------------------
// CON 9

    	// A9_TEST_MODE_REG
		a9_test_mode 			<= 1'b0;
		// A9_TEST_DATA_REG
		a9_test_data			<= 32'hffffffff;
		// A9_WIDTH_REG
		a9_width				<= 6'd1;
		a9_shape_load			<= 1'b0;
		// A9_OFFSET_QUOT_REG
        a9_i_offset_quotient    <= 7'b0;
        // A10_OFFSET_REMD_REG
        a9_i_offset_remainder    <= 3'b0;   
		// A10_DELAY_TAP_REG
		a9_delay_tap			<= 5'b0;
	    // A10_SIG_POLA_REG
        a9_sig_pola             <= 1'b1;		





// --------------------------------------------------------
// CON 10
		// A10_TEST_MODE_REG
		a10_test_mode 			<= 1'b0;
		// A10_TEST_DATA_REG
		a10_test_data			<= 32'hffffffff;
		// A10_WIDTH_REG
		a10_width				<= 6'd1;
		a10_shape_load			<= 1'b0;
		// A10_OFFSET_QUOT_REG
        a10_i_offset_quotient    <= 7'b0;
        // A10_OFFSET_REMD_REG
        a10_i_offset_remainder    <= 3'b0;   
		// A10_DELAY_TAP_REG
		a10_delay_tap			<= 5'b0;
	    // A10_SIG_POLA_REG
        a10_sig_pola             <= 1'b1;		

       // A_FAKE_START_REG
        op_fake_start           <=1'b0;
		// A_START_REG
		op_ready				<= 1'b0;
		// A_STOP_REG
		op_stop					<= 1'b0;
		// A_SNAPSHOT_REG
		op_snapshot				<= 21'd262144;
		// A_READY_ERR_REG
		ready_err_flag_clear	<= 1'b0;

		// A_FIFO_STAT_REG
		fifo_en					<= 1'b1;
		fifo_rst_r				<= 1'b1;
		// A_FIFO_PF_THRES_REG
		fifo_pf_thres			<= 16'h7fff;

		// A_SEED_REG
		seed					<= 32'b0;
		seed_valid				<= 1'b0;
		// A_SIG_THRES1_REG
		sig_thres1				<= 8'd25;
		// A_SIG_THRES2_REG
		sig_thres2				<= 8'd76;
		
		// A_TRIG_TYPE_REG
        a_trig_type             <= 2'b00;


//-------------------------------------drbg-------------------------------------- 
        // A_DRBG_MODE_REG
        drbg_mode <= 1'b0;
//-------------------------------------------------------------------------------
      
		// A_SIG_TEST_MODE_REG
		a_64b1_test_mode 			<= 1'b0;
		// A_SIG0_TEST_DATA_REG
		a_64b1_s0_test_data			<= 32'hffffffff;
		// A_SIG1_TEST_DATA_REG
		a_64b1_s1_test_data			<= 32'hffffffff;
		
 	   // A_DB1_D_TEST_MODE_REG  
		a_64b2_d_test_mode 			<= 1'b0;
 	   // A_DB1_B_TEST_MODE_REG  		
		a_64b2_b_test_mode 			<= 1'b0;		
	   // A_DB1_B_TEST_DATA_REG    
		a_64b2_b_test_data			<= 32'hffffffff;
	   // A_DB1_D_TEST_DATA_REG
		a_64b2_d_test_data				<= 32'hffffffff;
		
 	   // A_DB2_D_TEST_MODE_REG  
        a_64b3_d_test_mode             <= 1'b0;
 	   // A_DB2_B_TEST_MODE_REG        
        a_64b3_b_test_mode             <= 1'b0;        
       // A_DB2_B_TEST_DATA_REG    
        a_64b3_b_test_data            <= 32'hffffffff;
       // A_DB2_D_TEST_DATA_REG
        a_64b3_d_test_data                <= 32'hffffffff;
        
 	   // A_PM1_TEST_MODE_REG  
        a_32b1_test_mode             <= 1'b0;
       // A_PM1_TEST_DATA_REG    
        a_32b1_test_data            <= 32'hffffffff;
        
 	   // A_PM2_TEST_MODE_REG  
        a_32b2_test_mode             <= 1'b0;
       // A_PM2_TEST_DATA_REG    
        a_32b2_test_data            <= 32'hffffffff;
                        		        
 	     
	end
	else begin
		case (addr)
			`TMP_SIG_REG : begin
				rd_d[7:0]	<= emp_log;
				rd_d[31:8]	<= 'b0;
			end

			`A1_TEST_MODE_REG : begin
				rd_d[0]	<= a1_test_mode;
				rd_d[31:1]	<= 'b0;
				if (wr_en) begin
					a1_test_mode <= wr_d[0];
					a1_shape_load <= 1'b1;
				end
			end
			

			`A1_TEST_DATA_REG : begin
				rd_d[31:0]	<= a1_test_data;
				if (wr_en) begin
					a1_test_data <= wr_d[31:0];
				end
			end

			`A1_WIDTH_REG : begin
				rd_d[5:0]	<= a1_width;
				rd_d[31:6]	<= 'b0;
				if (wr_en) begin
					a1_width <= wr_d[5:0];
					a1_shape_load <= 1'b1;
				end
			end
			
		    `A1_ADDWIDTH_REG : begin
                 rd_d[7:0]    <= a1_add_width;
                 rd_d[31:8]    <= 'b0;
                 if (wr_en) begin
                     a1_add_width <= wr_d[7:0];
                     a1_shape_load <= 1'b1;
                end
            end 

			`A1_OFFSET_QUOT_REG : begin
				rd_d[6:0]	<= a1_i_offset_quotient;
				rd_d[31:7]	<= 'b0;
				if (wr_en) begin
					a1_i_offset_quotient <= wr_d[6:0];
					a1_shape_load <= 1'b1;
				end
			end
			
			`A1_OFFSET_REMD_REG : begin
                rd_d[2:0]    <= a1_i_offset_remainder;
                rd_d[31:3]    <= 'b0;
                if (wr_en) begin
                    a1_i_offset_remainder <= wr_d[2:0];
                    a1_shape_load <= 1'b1;
                end
            end

			`A1_DELAY_TAP_REG : begin
				rd_d[4:0]	<= a1_delay_tap;
				rd_d[31:5]	<= 'b0;
				if (wr_en) begin
					a1_delay_tap <= wr_d[4:0];
				end
			end
			
			
             `A1_ADD_PULSE_DELAY_REG : begin
                 rd_d[9:0]    <= o_a1_delay_cnt;
                 rd_d[31:10]    <= 'b0;
                 if (wr_en) begin
                     o_a1_delay_cnt <= wr_d[9:0];
                 end
             end 
// CON 2			
			`A2_TEST_MODE_REG : begin
                rd_d[0]        <= a2_test_mode;
                rd_d[31:1]    <= 'b0;
                if (wr_en) begin
                    a2_test_mode <= wr_d[0];
                    a2_shape_load <= 1'b1;                
                end
            end

            `A2_TEST_DATA_REG : begin
                rd_d[31:0]    <= a2_test_data;
                if (wr_en) begin
                    a2_test_data <= wr_d[31:0];
                end
            end
			`A2_WIDTH_REG : begin
				rd_d[5:0]	<= a2_width;
				rd_d[31:6]	<= 'b0;
				if (wr_en) begin
					a2_width <= wr_d[5:0];
					a2_shape_load <= 1'b1;
				end
			end

			`A2_OFFSET_QUOT_REG : begin
				rd_d[6:0]	<= a2_i_offset_quotient;
				rd_d[31:7]	<= 'b0;
				if (wr_en) begin
					a2_i_offset_quotient <= wr_d[6:0];
					a2_shape_load <= 1'b1;
				end
			end
			
			`A2_OFFSET_REMD_REG : begin
                rd_d[2:0]    <= a2_i_offset_remainder;
                rd_d[31:3]    <= 'b0;
                if (wr_en) begin
                    a2_i_offset_remainder <= wr_d[2:0];
                    a2_shape_load <= 1'b1;
                end
            end

			`A2_DELAY_TAP_REG : begin
				rd_d[4:0]	<= a2_delay_tap;
				rd_d[31:5]	<= 'b0;
				if (wr_en) begin
					a2_delay_tap <= wr_d[4:0];
				end
			end
			`A2_DETECT_DELAY_TAP_REG : begin
                rd_d[4:0]    <= a2_detect_delay_tap;
                rd_d[31:5]    <= 'b0;
                if (wr_en) begin
                    a2_detect_delay_tap <= wr_d[4:0];
                end
            end        
			`A2_SPD_CNT1_REG : begin
                rd_d[31:0]    <= a2_spad_cnt1;
            end

            `A2_SPD_CNT2_REG : begin
                rd_d[31:0]    <= a2_spad_cnt2;
            end
            
            `A2_SPD_CNT3_REG : begin
                rd_d[31:0]    <= a2_spad_cnt3;
            end
            `A2_SPD_CNT4_REG : begin
                rd_d[31:0]    <= a2_spad_cnt4;
            end

// CON 3
            `A3_TEST_MODE_REG : begin
                rd_d[0]        <= a3_test_mode;
                rd_d[31:1]    <= 'b0;
                if (wr_en) begin
                    a3_test_mode <= wr_d[0];
                    a3_shape_load <= 1'b1;                    
                end
            end
            
            `A3_TEST_L_DATA_REG : begin
                rd_d[31:0]    <= a3_test_l_data;
                if (wr_en) begin
                    a3_test_l_data <= wr_d[31:0];
                end
            end
            
            `A3_TEST_H_DATA_REG : begin
                rd_d[31:0]    <= a3_test_h_data;
                if (wr_en) begin
                    a3_test_h_data <= wr_d[31:0];
                end
            end
            
            `A3_WIDTH_REG : begin
                rd_d[3:0]    <= a3_width;
                rd_d[31:4]    <= 'b0;
                if (wr_en) begin
                    a3_width <= wr_d[3:0];
                    a3_shape_load <= 1'b1;
                end
            end
            
            `A3_OFFSET_REG : begin
                rd_d[8:0]    <= a3_offset;
                rd_d[31:9]    <= 'b0;
                if (wr_en) begin
                    a3_offset <= wr_d[8:0];
                    a3_shape_load <= 1'b1;
                end
            end
            
            `A3_V0_REG : begin
                rd_d[11:0]    <= a3_i_dac_v0;
                rd_d[31:12]    <= 'b0;
                if (wr_en) begin
                    a3_i_dac_v0 <= wr_d[11:0];
                end
            end
            
            `A3_V1_P_REG : begin
                rd_d[11:0]    <= a3_i_dac_v1_p;
                rd_d[31:12]    <= 'b0;
                if (wr_en) begin
                    a3_i_dac_v1_p <= wr_d[11:0];
                end
            end
            
            `A3_V2_P_REG : begin
                rd_d[11:0]    <= a3_i_dac_v2_p;
                rd_d[31:12]    <= 'b0;
                if (wr_en) begin
                    a3_i_dac_v2_p <= wr_d[11:0];
                end
            end
            
            `A3_V3_P_REG : begin
                rd_d[11:0]    <= a3_i_dac_v3_p;
                rd_d[31:12]    <= 'b0;
                if (wr_en) begin
                    a3_i_dac_v3_p <= wr_d[11:0];
                end
            end

            `A3_V1_N_REG : begin
                rd_d[11:0]    <= a3_i_dac_v1_n;
                rd_d[31:12]    <= 'b0;
                if (wr_en) begin
                    a3_i_dac_v1_n <= wr_d[11:0];
                end
            end
            
            `A3_V2_N_REG : begin
                rd_d[11:0]    <= a3_i_dac_v2_n;
                rd_d[31:12]    <= 'b0;
                if (wr_en) begin
                    a3_i_dac_v2_n <= wr_d[11:0];
                end
            end
            
            `A3_V3_N_REG : begin
                rd_d[11:0]    <= a3_i_dac_v3_n;
                rd_d[31:12]    <= 'b0;
                if (wr_en) begin
                    a3_i_dac_v3_n <= wr_d[11:0];
                end
            end

// CON 4            
           `A4_TEST_MODE_REG : begin
                rd_d[0]        <= a4_test_mode;
                rd_d[31:1]    <= 'b0;
                if (wr_en) begin
                    a4_test_mode <= wr_d[0];
                    a4_shape_load <= 1'b1;                    
                end
            end
            
            `A4_TEST_L_DATA_REG : begin
                rd_d[31:0]    <= a4_test_l_data;
                if (wr_en) begin
                    a4_test_l_data <= wr_d[31:0];
                end
            end
            
            `A4_TEST_H_DATA_REG : begin
                rd_d[31:0]    <= a4_test_h_data;
                if (wr_en) begin
                    a4_test_h_data <= wr_d[31:0];
                end
            end
            
            `A4_WIDTH_REG : begin
                rd_d[3:0]    <= a4_width;
                rd_d[31:4]    <= 'b0;
                if (wr_en) begin
                    a4_width <= wr_d[3:0];
                    a4_shape_load <= 1'b1;
                end
            end
            
            `A4_OFFSET_REG : begin
                rd_d[8:0]    <= a4_offset;
                rd_d[31:9]    <= 'b0;
                if (wr_en) begin
                    a4_offset <= wr_d[8:0];
                    a4_shape_load <= 1'b1;
                end
            end
            
            `A4_V0_REG : begin
                rd_d[11:0]    <= a4_i_dac_v0;
                rd_d[31:12]    <= 'b0;
                if (wr_en) begin
                    a4_i_dac_v0 <= wr_d[11:0];
                end
            end
            
            `A4_V1_P_REG : begin
                rd_d[11:0]    <= a4_i_dac_v1_p;
                rd_d[31:12]    <= 'b0;
                if (wr_en) begin
                    a4_i_dac_v1_p <= wr_d[11:0];
                end
            end
            
            `A4_V2_P_REG : begin
                rd_d[11:0]    <= a4_i_dac_v2_p;
                rd_d[31:12]    <= 'b0;
                if (wr_en) begin
                    a4_i_dac_v2_p <= wr_d[11:0];
                end
            end
            
            `A4_V3_P_REG : begin
                rd_d[11:0]    <= a4_i_dac_v3_p;
                rd_d[31:12]    <= 'b0;
                if (wr_en) begin
                    a4_i_dac_v3_p <= wr_d[11:0];
                end
            end

            `A4_V1_N_REG : begin
                rd_d[11:0]    <= a4_i_dac_v1_n;
                rd_d[31:12]    <= 'b0;
                if (wr_en) begin
                    a4_i_dac_v1_n <= wr_d[11:0];
                end
            end
            
            `A4_V2_N_REG : begin
                rd_d[11:0]    <= a4_i_dac_v2_n;
                rd_d[31:12]    <= 'b0;
                if (wr_en) begin
                    a4_i_dac_v2_n <= wr_d[11:0];
                end
            end  
            `A4_V3_N_REG : begin
                rd_d[11:0]    <= a4_i_dac_v3_n;
                rd_d[31:12]    <= 'b0;
                if (wr_en) begin
                    a4_i_dac_v3_n <= wr_d[11:0];
                end
            end              		
// CON 5
            `A5_TEST_MODE_REG : begin
                rd_d[0]        <= a5_test_mode;
                rd_d[31:1]    <= 'b0;
                if (wr_en) begin
                    a5_test_mode <= wr_d[0];
                    a5_shape_load <= 1'b1;                    
                end
            end
            
            `A5_TEST_L_DATA_REG : begin
                rd_d[31:0]    <= a5_test_l_data;
                if (wr_en) begin
                    a5_test_l_data <= wr_d[31:0];
                end
            end
            
            `A5_TEST_H_DATA_REG : begin
                rd_d[31:0]    <= a5_test_h_data;
                if (wr_en) begin
                    a5_test_h_data <= wr_d[31:0];
                end
            end
            
            `A5_WIDTH_REG : begin
                rd_d[3:0]    <= a5_width;
                rd_d[31:4]    <= 'b0;
                if (wr_en) begin
                    a5_width <= wr_d[3:0];
                    a5_shape_load <= 1'b1;
                end
            end
            
            `A5_OFFSET_REG : begin
                rd_d[8:0]    <= a5_offset;
                rd_d[31:9]    <= 'b0;
                if (wr_en) begin
                    a5_offset <= wr_d[8:0];
                    a5_shape_load <= 1'b1;
                end
            end
            
            `A5_V0_REG : begin
                rd_d[11:0]    <= a5_i_dac_v0;
                rd_d[31:12]    <= 'b0;
                if (wr_en) begin
                    a5_i_dac_v0 <= wr_d[11:0];
                end
            end
            
            `A5_V1_P_REG : begin
                rd_d[11:0]    <= a5_i_dac_v1_p;
                rd_d[31:12]    <= 'b0;
                if (wr_en) begin
                    a5_i_dac_v1_p <= wr_d[11:0];
                end
            end
            
            `A5_V2_P_REG : begin
                rd_d[11:0]    <= a5_i_dac_v2_p;
                rd_d[31:12]    <= 'b0;
                if (wr_en) begin
                    a5_i_dac_v2_p <= wr_d[11:0];
                end
            end
            
            `A5_V3_P_REG : begin
                rd_d[11:0]    <= a5_i_dac_v3_p;
                rd_d[31:12]    <= 'b0;
                if (wr_en) begin
                    a5_i_dac_v3_p <= wr_d[11:0];
                end
            end

            `A5_V1_N_REG : begin
                rd_d[11:0]    <= a5_i_dac_v1_n;
                rd_d[31:12]    <= 'b0;
                if (wr_en) begin
                    a5_i_dac_v1_n <= wr_d[11:0];
                end
            end
            
            `A5_V2_N_REG : begin
                rd_d[11:0]    <= a5_i_dac_v2_n;
                rd_d[31:12]    <= 'b0;
                if (wr_en) begin
                    a5_i_dac_v2_n <= wr_d[11:0];
                end
            end
            
            `A5_V3_N_REG : begin
                rd_d[11:0]    <= a5_i_dac_v3_n;
                rd_d[31:12]    <= 'b0;
                if (wr_en) begin
                    a5_i_dac_v3_n <= wr_d[11:0];
                end
            end
// CON 6
		`A6_PD12_CNT_REG : begin
                rd_d[31:0]    <= a6_PD12_cnt;
                end
                
        `A6_PDTRIG_CNT_REG : begin
                rd_d[31:0]    <= a6_PD_trig_cnt;
                end     
        `A6_START_WIDTH_THRES_REG : begin
                rd_d[3:0]    <= a6_start_width_thres;
                rd_d[31:4]    <= 28'b0;
                if (wr_en) begin
                   a6_start_width_thres <= wr_d[3:0];
                end
            end
                      

 // CON 7          
 
// CON 8          

// CON 9
 
 
 
    	`A9_TEST_MODE_REG : begin
             rd_d[0]       <= a9_test_mode;
             rd_d[31:1]    <= 'b0;
             if (wr_en) begin
                 a9_test_mode <= wr_d[0];
                 a9_shape_load <= 1'b1;
             end
         end

         `A9_TEST_DATA_REG : begin
             rd_d[31:0]    <= a9_test_data;
             if (wr_en) begin
                 a9_test_data <= wr_d[31:0];
             end
         end

         `A9_WIDTH_REG : begin
             rd_d[5:0]    <= a9_width;
             rd_d[31:6]    <= 'b0;
             if (wr_en) begin
                 a9_width <= wr_d[5:0];
                 a9_shape_load <= 1'b1;
             end
         end

         `A9_OFFSET_QUOT_REG : begin
             rd_d[6:0]    <= a9_i_offset_quotient;
             rd_d[31:7]    <= 'b0;
             if (wr_en) begin
                 a9_i_offset_quotient <= wr_d[6:0];
                 a9_shape_load <= 1'b1;
             end
         end
         
         `A9_OFFSET_REMD_REG : begin
             rd_d[2:0]    <= a9_i_offset_remainder;
             rd_d[31:3]    <= 'b0;
             if (wr_en) begin
                 a9_i_offset_remainder <= wr_d[2:0];
                 a9_shape_load <= 1'b1;
             end
         end

         `A9_DELAY_TAP_REG : begin
             rd_d[4:0]    <= a9_delay_tap;
             rd_d[31:5]    <= 'b0;
             if (wr_en) begin
                 a9_delay_tap <= wr_d[4:0];
             end
         end
          `A9_SIG_POLA_REG : begin
           rd_d[0]    <= a9_sig_pola;
           rd_d[31:1]    <= 'b0;
           if (wr_en) begin
               a9_sig_pola <= wr_d[0];
               //a9_shape_load <= 1'b1;
            end    
        end  
 
 
// CON 10
			`A10_TEST_MODE_REG : begin
				rd_d[0]		<= a10_test_mode;
				rd_d[31:1]	<= 'b0;
				if (wr_en) begin
					a10_test_mode <= wr_d[0];
					a10_shape_load <= 1'b1;
				end
			end

			`A10_TEST_DATA_REG : begin
				rd_d[31:0]	<= a10_test_data;
				if (wr_en) begin
					a10_test_data <= wr_d[31:0];
				end
			end

			`A10_WIDTH_REG : begin
				rd_d[5:0]	<= a10_width;
				rd_d[31:6]	<= 'b0;
				if (wr_en) begin
					a10_width <= wr_d[5:0];
					a10_shape_load <= 1'b1;
				end
			end

			`A10_OFFSET_QUOT_REG : begin
				rd_d[6:0]	<= a10_i_offset_quotient;
				rd_d[31:7]	<= 'b0;
				if (wr_en) begin
					a10_i_offset_quotient <= wr_d[6:0];
					a10_shape_load <= 1'b1;
				end
			end
			
			`A10_OFFSET_REMD_REG : begin
                rd_d[2:0]    <= a10_i_offset_remainder;
                rd_d[31:3]    <= 'b0;
                if (wr_en) begin
                    a10_i_offset_remainder <= wr_d[2:0];
                    a10_shape_load <= 1'b1;
                end
            end

			`A10_DELAY_TAP_REG : begin
				rd_d[4:0]	<= a10_delay_tap;
				rd_d[31:5]	<= 'b0;
				if (wr_en) begin
					a10_delay_tap <= wr_d[4:0];
				end
			end
			 `A10_SIG_POLA_REG : begin
              rd_d[0]    <= a10_sig_pola;
              rd_d[31:1]    <= 'b0;
              if (wr_en) begin
                  a10_sig_pola <= wr_d[0];
                  //a10_shape_load <= 1'b1;
               end    
           end        
		   
              `A_FAKE_START_REG : begin
              rd_d[0]    <= op_fake_start;
              rd_d[31:1]    <= 31'b0;
              if (wr_en) begin
                  op_fake_start <= wr_d[0];
              end
           end
           
			`A_READY_REG : begin
				rd_d[0]	<= op_ready;
				rd_d[30:1]	<= 30'b0;
				rd_d[31]	<= op_busy;
				if (wr_en) begin
					op_ready <= wr_d[0];
				end
			end

			`A_STOP_REG : begin
				rd_d[0]	<= op_stop;
				rd_d[31:1]	<= 31'b0;
				if (wr_en) begin
					op_stop <= wr_d[0];
				end
			end

			`A_SNAPSHOT_REG : begin
				rd_d[20:0]	<= op_snapshot;
				rd_d[31:21]	<= 13'b0;
				if (wr_en) begin
					op_snapshot <= wr_d[20:0];
				end
			end

			`A_READY_ERR_REG : begin
				rd_d[5:0]	<=	ready_err_flag;
				rd_d[31:6]	<= 'b0;
				if (wr_en) begin
					ready_err_flag_clear <= wr_d[0];
				end
			end

			`A_FIFO_STAT_REG : begin
				rd_d[0]		<= fifo_en;
				rd_d[1]		<= fifo_rst;
				rd_d[7:2]	<= 6'b0;
				rd_d[8]		<= fifo_empty;
				rd_d[10:9]	<= 2'b0;
				rd_d[11]	<= fifo_pf;
				rd_d[12]	<= fifo_af;
				rd_d[13]	<= fifo_full;
				rd_d[31:14] <= 'b0;
				if (wr_en) begin
					fifo_en		<= wr_d[0];
					fifo_rst_r	<= wr_d[1];
				end
			end

			`A_FIFO_PF_THRES_REG : begin
				rd_d[15:0]	<= fifo_pf_thres;
				rd_d[31:16]	<= 16'b0;
				if (wr_en) begin
					fifo_pf_thres <= wr_d[15:0];
				end
			end

			`A_FIFO_READ_REG : begin
				rd_d[31:0] <= ff_out;
			end

			`A_SEED_REG : begin
				rd_d[31:0] <= seed;
				if (wr_en) begin
					seed <= wr_d[31:0];
					seed_valid <= 1'b1;
				end
			end

			`A_SIG_THRES1_REG : begin
				rd_d[7:0] <= sig_thres1;
				rd_d[31:8] <= 24'b0;
				if (wr_en) begin
					sig_thres1 <= wr_d[7:0];
				end
			end

			`A_SIG_THRES2_REG : begin
				rd_d[7:0] <= sig_thres2;
				rd_d[31:8] <= 24'b0;
				if (wr_en) begin
					sig_thres2 <= wr_d[7:0];
				end
			end
			
			`A_TRIG_TYPE_REG : begin
                rd_d[1:0]    <= a_trig_type;
                rd_d[31:2]    <= 30'b0;
                if (wr_en) begin
                    a_trig_type <= wr_d[1:0];
                end
            end
   
//-------------------------------------drbg--------------------------------------
            `A_DRBG_MODE_REG : begin
                rd_d [0]    <= drbg_mode;
                rd_d [31:1] <= 31'b0;
                if (wr_en) begin
                    drbg_mode <= wr_d[0];
                end 
            end
            
            `A_ETP_CNT_REG : begin
                rd_d [31:0] <= etp_cnt;
            end
            
            `A_DRBG_CNT_REG : begin
                rd_d [31:0] <= drbg_cnt;
            end
            
            `A_ETP_DRBG_FIFO_STAT_REG : begin
                rd_d [0]    <= empty_etp_fifo;
                rd_d [1]    <= empty_drbg_fifo_group;
                rd_d [2]    <= full_etp_veri;
                rd_d [3]    <= full_drbg_veri;
                rd_d [31:4] <= 28'b0;
            end
            
            `A_ETP_VERI_READ_REG : begin
                rd_d [31:0] <= o_etp_veri;
            end
            
            `A_DRBG_VERI_READ_REG : begin
                rd_d [31:0] <= o_drbg_veri;
            end
                     
//-------------------------------------------------------------------------------
            
			`A_SIG_TEST_MODE_REG : begin
				rd_d[0]    <= a_64b1_test_mode; 
				rd_d[31:1] <= 31'b0;
				if (wr_en) begin
					a_64b1_test_mode <= wr_d[0];
				end
			end

			`A_SIG0_TEST_DATA_REG : begin
				rd_d[31:0] <= a_64b1_s0_test_data;
				if (wr_en) begin
					a_64b1_s0_test_data <= wr_d[31:0];
				end
			end

			`A_SIG1_TEST_DATA_REG : begin
				rd_d[31:0] <= a_64b1_s1_test_data;
				if (wr_en) begin
					a_64b1_s1_test_data <= wr_d[31:0];
				end
			end
			`A_DB1_D_TEST_MODE_REG : begin			
				rd_d[0] <= a_64b2_d_test_mode;
				rd_d[31:1] <= 30'b0;
				if (wr_en) begin
				
					a_64b2_d_test_mode <= wr_d[0];
				end
			end
			`A_DB1_B_TEST_MODE_REG : begin            
                rd_d[0] <= a_64b2_b_test_mode;
                rd_d[31:1] <= 30'b0;
                if (wr_en) begin
                
                    a_64b2_b_test_mode <= wr_d[0];
                end
            end
			`A_DB1_B_TEST_DATA_REG  : begin
				rd_d[31:0] <= a_64b2_b_test_data;
				if (wr_en) begin
					a_64b2_b_test_data <= wr_d[31:0];
				end
			end


			`A_DB1_D_TEST_DATA_REG   : begin
				rd_d[31:0] <= a_64b2_d_test_data;
				if (wr_en) begin
					a_64b2_d_test_data <= wr_d[31:0];
				end
			end
			
			`A_DB2_D_TEST_MODE_REG : begin			
                rd_d[0] <= a_64b3_d_test_mode;
                rd_d[31:1] <= 30'b0;
                if (wr_en) begin
                
                    a_64b3_d_test_mode <= wr_d[0];
                end
            end
            `A_DB2_B_TEST_MODE_REG : begin            
                rd_d[0] <= a_64b3_b_test_mode;
                rd_d[31:1] <= 30'b0;
                if (wr_en) begin
                
                    a_64b3_b_test_mode <= wr_d[0];
                end
            end
            			
			`A_DB2_B_TEST_DATA_REG  : begin
                rd_d[31:0] <= a_64b3_b_test_data;
                if (wr_en) begin
                    a_64b3_b_test_data <= wr_d[31:0];
                end
            end


            `A_DB2_D_TEST_DATA_REG   : begin
                rd_d[31:0] <= a_64b3_d_test_data;
                if (wr_en) begin
                    a_64b3_d_test_data <= wr_d[31:0];
                end
            end
            
			`A_PM1_TEST_MODE_REG : begin
                rd_d[0] <= a_32b1_test_mode;
                rd_d[31:1] <= 31'b0;
                if (wr_en) begin
                    a_32b1_test_mode <= wr_d[0];
                end
            end
                        
            `A_PM1_TEST_DATA_REG  : begin
                rd_d[31:0] <= a_32b1_test_data;
                if (wr_en) begin
                    a_32b1_test_data <= wr_d[31:0];
                end
            end
                        
			`A_PM2_TEST_MODE_REG : begin
                rd_d[0] <= a_32b2_test_mode;
                rd_d[31:1] <= 31'b0;
                if (wr_en) begin
                    a_32b2_test_mode <= wr_d[0];
                end
            end
                        
            `A_PM2_TEST_DATA_REG  : begin
                rd_d[31:0] <= a_32b2_test_data;
                if (wr_en) begin
                    a_32b2_test_data <= wr_d[31:0];
                end
            end
       
       
			default : begin
				rd_d <= 32'b0;
			end
		endcase

		////////////////////////////////////////////////
		// ?? ??λ¦?.
		if (!wr_en) begin
			a1_shape_load		<= 1'b0;
			a2_shape_load		<= 1'b0;
			a3_shape_load		<= 1'b0;
			a4_shape_load		<= 1'b0;
			a5_shape_load		<= 1'b0;
			a9_shape_load		<= 1'b0;			
			a10_shape_load		<= 1'b0;
			op_fake_start        <= 1'b0;
			op_ready			<= 1'b0;
			op_stop				<= 1'b0;
			ready_err_flag_clear	<= 1'b0;
			fifo_rst_r 			<= 1'b0;
			seed_valid			<= 1'b0;
		
		end


	end
end


assign				dir_fifo = fifo_af;
(* mark_debug = "true" *)wire ff_out_valid;
fifo_32x64k pcie_fifo (
  .clk(clk),						// I
  .srst(fifo_rst || !fifo_en),			// I
  .din(din_fifo),						// I [31 : 0]
  .wr_en(div_fifo && ~fifo_full),		// I
  .rd_en(fifo_rd_en && !fifo_empty),	// I
  .prog_full_thresh(fifo_pf_thres),		// I [15 : 0]
  .dout(ff_out),						// O [31 : 0]
  .full(fifo_full),						// O
  .almost_full(fifo_af),				// O
  .empty(fifo_empty),					// O
  .valid(ff_out_valid),								// O
  .prog_full(fifo_pf)					// O
);	// latency = 2


pls_expander #(
	.COUNT(`C2H_RST_PCIECLK),
	.POLARITY(1)
) c2h_0_rst_ext(
	.sin(fifo_rst_r),
	.clk(clk),
	.sout(fifo_rst)
);


endmodule