`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: HURA
// Engineer: YUN Hojun (yhj@hura.co.kr)
//
// Create Date: 2021/09/09
// Design Name: DRBG_bit_generation
// Module Name: demux_and_fifo
// Project Name:
// Target Devices: xc7k160tffg676-1
// Tool Versions: Vivado 2018.1
// Description:
//
// Dependencies:
//
// Revision: 
// Additional Comments: "Logic will get you from A to B. Imagination will take you everywhere." - Albert Einstein
//
// Copyright 2021, All rights reserved by HURA
//////////////////////////////////////////////////////////////////////////////////

module demux_and_fifo(

    input wire clk,
    input wire sclr,
    input wire entropy_req,
    input wire [255:0] RandomNo,
    input wire RandomNoValid,
  
    input wire ready_dwdc_01,
    input wire ready_dwdc_02,
    input wire ready_dwdc_03,
    input wire ready_dwdc_04,
    input wire ready_dwdc_05,
    input wire ready_dwdc_06,
    input wire ready_dwdc_07,
    input wire ready_dwdc_08,
    input wire ready_dwdc_09,
    input wire ready_dwdc_10,
    
    output wire full,
    output wire ready_b,         //if all fifos are ready? ready_n = 0 

    
    output wire [255:0] dout_01,
    output wire [255:0] dout_02,
    output wire [255:0] dout_03,
    output wire [255:0] dout_04,
    output wire [255:0] dout_05,
    output wire [255:0] dout_06,
    output wire [255:0] dout_07,
    output wire [255:0] dout_08,
    output wire [255:0] dout_09,
    output wire [255:0] dout_10,
    
    output wire i_valid_dwdc_01,
    output wire i_valid_dwdc_02,
    output wire i_valid_dwdc_03,
    output wire i_valid_dwdc_04,
    output wire i_valid_dwdc_05,
    output wire i_valid_dwdc_06,
    output wire i_valid_dwdc_07,
    output wire i_valid_dwdc_08,
    output wire i_valid_dwdc_09,
    output wire i_valid_dwdc_10
        
);

reg [255:0] din_01;
reg [255:0] din_02;
reg [255:0] din_03;
reg [255:0] din_04;
reg [255:0] din_05;
reg [255:0] din_06;
reg [255:0] din_07;
reg [255:0] din_08;
reg [255:0] din_09;
reg [255:0] din_10;

reg wr_en_01;
reg wr_en_02;
reg wr_en_03;
reg wr_en_04;
reg wr_en_05;
reg wr_en_06;
reg wr_en_07;
reg wr_en_08;
reg wr_en_09;
reg wr_en_10;

wire rd_en_01;
wire rd_en_02;
wire rd_en_03;
wire rd_en_04;
wire rd_en_05;
wire rd_en_06;
wire rd_en_07;
wire rd_en_08;
wire rd_en_09;
wire rd_en_10;

wire [255:0] dout_fifo_01;
wire [255:0] dout_fifo_02;
wire [255:0] dout_fifo_03;
wire [255:0] dout_fifo_04;
wire [255:0] dout_fifo_05;
wire [255:0] dout_fifo_06;
wire [255:0] dout_fifo_07;
wire [255:0] dout_fifo_08;
wire [255:0] dout_fifo_09;
wire [255:0] dout_fifo_10;

wire empty_01;
wire empty_02;
wire empty_03;
wire empty_04;
wire empty_05;
wire empty_06;
wire empty_07;
wire empty_08;
wire empty_09;
wire empty_10;

wire full_01;
wire full_02;
wire full_03;
wire full_04;
wire full_05;
wire full_06;
wire full_07;
wire full_08;
wire full_09;
wire full_10;

wire dout_valid_fifo_01;
wire dout_valid_fifo_02;
wire dout_valid_fifo_03;
wire dout_valid_fifo_04;
wire dout_valid_fifo_05;
wire dout_valid_fifo_06;
wire dout_valid_fifo_07;
wire dout_valid_fifo_08;
wire dout_valid_fifo_09;
wire dout_valid_fifo_10;

reg  [3:0] req_cnt;

assign ready_b = (empty_01)||(empty_02)||(empty_03)||(empty_04)||(empty_05)||(empty_06)||(empty_07)||(empty_08)||(empty_09)||(empty_10);  // "ready" is 0 if not all fifos are empty

assign rd_en_01 = (~ready_b)&&(ready_dwdc_01);
assign rd_en_02 = (~ready_b)&&(ready_dwdc_02);
assign rd_en_03 = (~ready_b)&&(ready_dwdc_03);
assign rd_en_04 = (~ready_b)&&(ready_dwdc_04);
assign rd_en_05 = (~ready_b)&&(ready_dwdc_05);
assign rd_en_06 = (~ready_b)&&(ready_dwdc_06);
assign rd_en_07 = (~ready_b)&&(ready_dwdc_07);
assign rd_en_08 = (~ready_b)&&(ready_dwdc_08);
assign rd_en_09 = (~ready_b)&&(ready_dwdc_09);
assign rd_en_10 = (~ready_b)&&(ready_dwdc_10);


//i_valid_dwdc_01 = rd_en_01 ---> i_valid_dwdc_01 = dout_valid_fifo_01
assign i_valid_dwdc_01 = dout_valid_fifo_01;
assign i_valid_dwdc_02 = dout_valid_fifo_02;
assign i_valid_dwdc_03 = dout_valid_fifo_03;
assign i_valid_dwdc_04 = dout_valid_fifo_04;
assign i_valid_dwdc_05 = dout_valid_fifo_05;
assign i_valid_dwdc_06 = dout_valid_fifo_06;
assign i_valid_dwdc_07 = dout_valid_fifo_07;
assign i_valid_dwdc_08 = dout_valid_fifo_08;
assign i_valid_dwdc_09 = dout_valid_fifo_09;
assign i_valid_dwdc_10 = dout_valid_fifo_10;

assign dout_01 = dout_fifo_01;
assign dout_02 = dout_fifo_02;
assign dout_03 = dout_fifo_03;
assign dout_04 = dout_fifo_04;
assign dout_05 = dout_fifo_05;
assign dout_06 = dout_fifo_06;
assign dout_07 = dout_fifo_07;
assign dout_08 = dout_fifo_08;
assign dout_09 = dout_fifo_09;
assign dout_10 = dout_fifo_10;

assign full = (full_01)&&(full_02)&&(full_03)&&(full_04)&&(full_05)&&(full_06)&&(full_07)&&(full_08)&&(full_09)&&(full_10);



always @(posedge clk) begin

    if(sclr) begin
    din_01 <= 1'b0;
    din_02 <= 1'b0;
    din_03 <= 1'b0;
    din_04 <= 1'b0;
    din_05 <= 1'b0;
    din_06 <= 1'b0;
    din_07 <= 1'b0;
    din_08 <= 1'b0;
    din_09 <= 1'b0;
    din_10 <= 1'b0;
    
    wr_en_01 <= 1'b0;
    wr_en_02 <= 1'b0;
    wr_en_03 <= 1'b0;
    wr_en_04 <= 1'b0;
    wr_en_05 <= 1'b0;
    wr_en_06 <= 1'b0;
    wr_en_07 <= 1'b0;
    wr_en_08 <= 1'b0;
    wr_en_09 <= 1'b0;
    wr_en_10 <= 1'b0;
    
    req_cnt <= 1'b0;
    
    end
    else begin
        if (entropy_req) begin
            req_cnt <= (req_cnt == 4'd10)? 1'b1 : req_cnt +1;
        end
        else begin         
            din_01 <= (req_cnt == 4'd1)? RandomNo : 1'b0;
            din_02 <= (req_cnt == 4'd2)? RandomNo : 1'b0;
            din_03 <= (req_cnt == 4'd3)? RandomNo : 1'b0;
            din_04 <= (req_cnt == 4'd4)? RandomNo : 1'b0;
            din_05 <= (req_cnt == 4'd5)? RandomNo : 1'b0;
            din_06 <= (req_cnt == 4'd6)? RandomNo : 1'b0;
            din_07 <= (req_cnt == 4'd7)? RandomNo : 1'b0;
            din_08 <= (req_cnt == 4'd8)? RandomNo : 1'b0;
            din_09 <= (req_cnt == 4'd9)? RandomNo : 1'b0;
            din_10 <= (req_cnt == 4'd10)? RandomNo : 1'b0;
       
            wr_en_01 <= (req_cnt == 4'd1)? RandomNoValid : 1'b0;
            wr_en_02 <= (req_cnt == 4'd2)? RandomNoValid : 1'b0;
            wr_en_03 <= (req_cnt == 4'd3)? RandomNoValid : 1'b0;
            wr_en_04 <= (req_cnt == 4'd4)? RandomNoValid : 1'b0;
            wr_en_05 <= (req_cnt == 4'd5)? RandomNoValid : 1'b0;
            wr_en_06 <= (req_cnt == 4'd6)? RandomNoValid : 1'b0;
            wr_en_07 <= (req_cnt == 4'd7)? RandomNoValid : 1'b0;
            wr_en_08 <= (req_cnt == 4'd8)? RandomNoValid : 1'b0;
            wr_en_09 <= (req_cnt == 4'd9)? RandomNoValid : 1'b0;
            wr_en_10 <= (req_cnt == 4'd10)? RandomNoValid : 1'b0;         
        end      
    end
end                  
    


//--------------------------entropo_FIFO 256bit*32-------------------------------

fifo_C_B_256x2048 fifo_256x2048_01 (
  .clk      (clk),
  .srst		(sclr),		      	// I
  .din		(din_01),		// I
  .wr_en	(wr_en_01),		// I
  .rd_en	(rd_en_01 && ~dout_valid_fifo_01),		// I
  .dout		(dout_fifo_01),	// O
  .valid    (dout_valid_fifo_01),
  .empty	(empty_01),	    // O
  .full		(full_01)		// O	
);



fifo_C_B_256x2048 fifo_256x2048_02 (
  .clk      (clk),
  .srst		(sclr),		      	// I
  .din		(din_02),		// I
  .wr_en	(wr_en_02),		// I
  .rd_en	(rd_en_02 && ~dout_valid_fifo_02),	    // I
  .dout		(dout_fifo_02),	// O
  .valid    (dout_valid_fifo_02),
  .empty	(empty_02),	    // O
  .full		(full_02)		// O	
);


fifo_C_B_256x2048 fifo_256x2048_03 (
  .clk      (clk),
  .srst		(sclr),		      	// I
  .din		(din_03),		// I
  .wr_en	(wr_en_03),		// I
  .rd_en	(rd_en_03 && ~dout_valid_fifo_03),	    // I
  .dout		(dout_fifo_03),	// O
  .valid    (dout_valid_fifo_03),
  .empty	(empty_03),	    // O
  .full		(full_03)		// O	
);


fifo_C_B_256x2048 fifo_256x2048_04 (
  .clk      (clk),
  .srst		(sclr),		      	// I
  .din		(din_04),		// I
  .wr_en	(wr_en_04),		// I
  .rd_en	(rd_en_04 && ~dout_valid_fifo_04),	    // I
  .dout		(dout_fifo_04),	// O
  .valid    (dout_valid_fifo_04),
  .empty	(empty_04),	    // O
  .full		(full_04)		// O	
);


fifo_C_B_256x2048 fifo_256x2048_05 (
  .clk      (clk),
  .srst		(sclr),		      	// I
  .din		(din_05),		// I
  .wr_en	(wr_en_05),		// I
  .rd_en	(rd_en_05 && ~dout_valid_fifo_05),	    // I
  .dout		(dout_fifo_05),	// O
  .valid    (dout_valid_fifo_05),
  .empty	(empty_05),	    // O
  .full		(full_05)		// O	
);


fifo_C_B_256x2048 fifo_256x2048_06 (
  .clk      (clk),
  .srst		(sclr),		      	// I
  .din		(din_06),		// I
  .wr_en	(wr_en_06),		// I
  .rd_en	(rd_en_06 && ~dout_valid_fifo_06),	    // I
  .dout		(dout_fifo_06),	// O
  .valid    (dout_valid_fifo_06),
  .empty	(empty_06),	    // O
  .full		(full_06)		// O	
);


fifo_C_B_256x2048 fifo_256x2048_07 (
  .clk      (clk),
  .srst		(sclr),		      	// I
  .din		(din_07),		// I
  .wr_en	(wr_en_07),		// I
  .rd_en	(rd_en_07 && ~dout_valid_fifo_07),	    // I
  .dout		(dout_fifo_07),	// O
  .valid    (dout_valid_fifo_07),
  .empty	(empty_07),	    // O
  .full		(full_07)		// O	
);


fifo_C_B_256x2048 fifo_256x2048_08 (
  .clk      (clk),
  .srst		(sclr),		      	// I
  .din		(din_08),		// I
  .wr_en	(wr_en_08),		// I
  .rd_en	(rd_en_08 && ~dout_valid_fifo_08),	    // I
  .dout		(dout_fifo_08),	// O
  .valid    (dout_valid_fifo_08),
  .empty	(empty_08),	    // O
  .full		(full_08)		// O	
);


fifo_C_B_256x2048 fifo_256x2048_09 (
  .clk      (clk),
  .srst		(sclr),    			// I
  .din		(din_09),		// I
  .wr_en	(wr_en_09),		// I
  .rd_en	(rd_en_09 && ~dout_valid_fifo_09),	    // I
  .dout		(dout_fifo_09),	// O
  .valid    (dout_valid_fifo_09),
  .empty	(empty_09), 	// O
  .full		(full_09)		// O	
);


fifo_C_B_256x2048 fifo_256x2048_10 (
  .clk      (clk),
  .srst		(sclr),	// I
  .din		(din_10),		// I
  .wr_en	(wr_en_10),		// I
  .rd_en	(rd_en_10 && ~dout_valid_fifo_10),	   	// I
  .dout		(dout_fifo_10),	// O
  .valid    (dout_valid_fifo_10),
  .empty	(empty_10),	    // O
  .full		(full_10)		// O	
);
//-------------------------------------------------------------------------------




endmodule

