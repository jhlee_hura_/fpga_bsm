//======================================================================
// Title: lshDrbgRN_256_N.v
// ---------------------------------------------------------------------
// Description	: Random Numbers Generation of 256 * N bit
// Author		: EJ KIM
// Date			: 6th December, 2018
//======================================================================		
module lshDrbgRN_256_N( //  V: StateValue V, E: Entropy, A: Additional Input
										input wire iClk,
										input wire iReset_n,
										input wire iEnable,
										input	wire [11:0] iRN256_CNT,
										input wire [439:0] iStateV,
										output wire [255:0] oRN256,
										output wire oRN256_Valid,
										output wire oRN256N_Complete
						);
						
parameter S_IDLE				= 4'h0;
parameter S_RN256_GEN		    = 4'h1;
parameter S_RN256_GEN_WAIT		= 4'h2;
parameter S_RN256_GEN_DONE		= 4'h3;
parameter S_RN256_N_DONE	= 4'h4;
////////////////////////////////////////////////////////////////////////////////////////////
//delay  Valid Signal with 1/2 Clk
////////////////////////////////////////////////////////////////////////////////////////////
parameter STATE_DELAY_IDLE       = 0;
parameter STATE_DELAY_1CLK       = 1;
parameter STATE_DELAY_2CLK       = 2;
parameter STATE_DELAY_3CLK       = 3;

reg rTEmpReStateValid;
reg [3:0] rState_enable;
////////////////////////////////////////////////////////////////////////////////////////////

//wire wAlignValid;
//wire 	[1023:0] wTempBlkInput;

wire [31:0] 	wM1_0,  wM1_1,  wM1_2,  wM1_3,  wM1_4,  wM1_5,  wM1_6,  wM1_7,  wM1_8,  wM1_9,  wM1_10, wM1_11, wM1_12, wM1_13, wM1_14, wM1_15;
wire [31:0] 	wM1_16, wM1_17, wM1_18, wM1_19, wM1_20, wM1_21, wM1_22, wM1_23, wM1_24, wM1_25, wM1_26, wM1_27, wM1_28, wM1_29, wM1_30, wM1_31;  

reg  [439:0] 	rTempV;


reg	[11:0] 		rRN256_CNT;
reg	[11:0]		rCnt;

reg				rRN256N_Complete;
reg				rShaEnable;

reg	[3:0] 		rState;



//----------------------------------------------------------------
// Concurrent connectivity for ports etc.
//----------------------------------------------------------------
 // assign wTempBlkInput 		= {rTempV, 1'b1, 583'b0 }; 
//  assign oRN256N_Complete	= rRN256N_Complete;



 assign oRN256N_Complete	= rTEmpReStateValid;

////////////////////////////////////////////////////////////////////////////////////////////
	//1/2 clock 만큼 vaild 신호 밀기 && width = 2clk!!
	////////////////////////////////////////////////////////////////////////////////////////////
      always @(posedge iClk or negedge iReset_n) begin
          if(!iReset_n) begin
              rState_enable <=STATE_DELAY_IDLE;
              rTEmpReStateValid <=1'b0;
          end // if(!iReset_n) begin
          else begin //~if(!iReset_n) begin
             case(rState_enable)
                 STATE_DELAY_IDLE: begin
                     if(rRN256N_Complete) begin
                        rState_enable              <=STATE_DELAY_1CLK;
                        rTEmpReStateValid   <= 1'b1;
                      end // if(digestValid_1) begin
                      else begin // ~ if(iEnable) begin
                        rState_enable              <=STATE_DELAY_IDLE;
                        rTEmpReStateValid   <=1'b0;
                      end // ~ if(digestValid_1) begin
                  end //STATE_DELAY_IDLE: begin
                  STATE_DELAY_1CLK: begin 
                        rState_enable              <=STATE_DELAY_IDLE;
                        rTEmpReStateValid   <=1'b1;
                  end //STATE_DELAY_1CLK: begin 
//                  STATE_DELAY_2CLK: begin 
//                       rState_enable              <=STATE_IDLE;
//                       rTEmpReStateValid   <=1'b1;
//                  end //STATE_DELAY_2CLK: begin 
                 default: begin 
                    rState_enable              <=STATE_DELAY_IDLE;
                    rTEmpReStateValid   <=1'b0;
                 end             
             endcase //  case(rState)
          end // /~/if(!iReset_n) begin
      end //always @(posedge iClk or negedge iReset_n) begin
	////////////////////////////////////////////////////////////////////////////////////////////


// Alignment Test for input[abc]
//  assign wTempBlk1 = {24'h616263, 1'b1, 2023'b0 }; //0X01_000001B8_E[439:0]_1_all's 0 
//  assign wTempBlk2 = {8'h02, 32'h000001B8, inEntropy[439:0], 1'b1, 1567'b0 };
  
always @(posedge iClk or negedge iReset_n) begin
	if(!iReset_n) begin			
		rTempV				<= 440'h0;
		rRN256_CNT			<= 12'h0;
		rRN256N_Complete	<= 1'b0;
		rShaEnable			<= 1'b0;
		rCnt				<= 12'h0;
		rState				<= S_IDLE;
	end //if(!iReset_n) begin
	else begin //~if(!iReset_n) begin
			case(rState) 
				S_IDLE: begin
				 	if(iEnable) begin
						rTempV				<= iStateV;
						rRN256_CNT			<= iRN256_CNT;
						rRN256N_Complete	<= 1'b0;
						rShaEnable			<= 1'b0;
						rCnt					<= 12'h0;
						rState				<= S_RN256_GEN;
					end //	if(iEnable) begin
					else begin // ~if(iEnable) begin
						rTempV				<= 440'h0;
						rRN256_CNT			<= 12'h0;
						rRN256N_Complete	<= 1'b0;
						rShaEnable			<= 1'b0;
						rCnt					<= 12'h0;
						rState				<= S_IDLE;
					end//~if(iEnable) begin
				end //case(rState) begin
				S_RN256_GEN : begin
					rTempV				<= rTempV;
					rRN256_CNT			<= rRN256_CNT;
					rRN256N_Complete	<= 1'b0;
					rShaEnable			<= 1'b1;
					rCnt					<= rCnt + 1'b1;
					rState				<= S_RN256_GEN_WAIT;				
				end //S_RN256_GEN : begin
				S_RN256_GEN_WAIT : begin
					if(oRN256_Valid) begin
						rTempV				<= rTempV;
						rRN256_CNT			<= rRN256_CNT;
						rRN256N_Complete	<= 1'b0;
						rShaEnable			<= 1'b0;
						rCnt					<= rCnt;
						rState				<= S_RN256_GEN_DONE;	
					end //if(oRN256_Valid) begin
					else begin // ~if(oRN256_Valid) begin
						rTempV				<= rTempV;
						rRN256_CNT			<= rRN256_CNT;
						rRN256N_Complete	<= 1'b0;
						rShaEnable			<= 1'b0;
						rCnt					<= rCnt;
						rState				<= S_RN256_GEN_WAIT;	
					end // ~if(oRN256_Valid) begin			
				end //S_RN256_GEN_WAIT : begin
				S_RN256_GEN_DONE : begin
					if(rCnt < rRN256_CNT) begin
						rTempV				<= rTempV + 1'b1;
						rRN256_CNT			<= rRN256_CNT;
						rRN256N_Complete	<= 1'b0;
						rShaEnable			<= 1'b0;
						rCnt					<= rCnt;
						rState				<= S_RN256_GEN;						
					end //if(rCnt == rRN256_CNT) begin
					else begin // ~if(rCnt < rRN256_CNT) begin
						rTempV				<= rTempV;
						rRN256_CNT			<= rRN256_CNT;
						rRN256N_Complete	<= 1'b1;
						rShaEnable			<= 1'b0;
						rCnt					<= rCnt;
						rState				<= S_RN256_N_DONE;
					end //~if(rCnt == rRN256_CNT) begin
				end //S_RN256_GEN_DONE
				S_RN256_N_DONE : begin				
					rTempV				<= rTempV;
					rRN256_CNT			<= rRN256_CNT;
					rRN256N_Complete	<= 1'b0;
					rShaEnable			<= 1'b0;
					rCnt					<= rCnt;
					rState				<= S_IDLE;
				end //S_RN256_N_DONE : begin
				default : begin
					rTempV				<= 440'h0;
					rRN256_CNT			<= 12'h0;
					rRN256N_Complete	<= 1'b0;
					rShaEnable			<= 1'b0;
					rCnt					<= 12'h0;
					rState				<= S_IDLE;
				end // defaultW
			endcase // case(rState) begin		
	end //~if(!iReset_n) begin		
end //always @(posedge iClk or negedge iReset_n) begin


   
//	// Fist LSH input
//	align2048To64s input_1st_alignment ( 		
//								.iClk(iClk),align2048To64s
//								.iReset_n(iReset_n),
//								.iEnable(rShaEnable),
								
//								.iIn2048(wTempBlkInput), 
//								.oOut64_0(wM1_0),   .oOut64_1(wM1_1),   .oOut64_2(wM1_2),   .oOut64_3(wM1_3),   .oOut64_4(wM1_4),   .oOut64_5(wM1_5),   .oOut64_6(wM1_6),   .oOut64_7(wM1_7),
//								.oOut64_8(wM1_8),   .oOut64_9(wM1_9),   .oOut64_10(wM1_10), .oOut64_11(wM1_11), .oOut64_12(wM1_12), .oOut64_13(wM1_13), .oOut64_14(wM1_14), .oOut64_15(wM1_15),
//								.oOut64_16(wM1_16), .oOut64_17(wM1_17), .oOut64_18(wM1_18), .oOut64_19(wM1_19), .oOut64_20(wM1_20), .oOut64_21(wM1_21), .oOut64_22(wM1_22), .oOut64_23(wM1_23),
//								.oOut64_24(wM1_24), .oOut64_25(wM1_25), .oOut64_26(wM1_26), .oOut64_27(wM1_27), .oOut64_28(wM1_28), .oOut64_29(wM1_29), .oOut64_30(wM1_30), .oOut64_31(wM1_31),
//								.oValid(wAlignValid)										
//						);
	
	LSH_SingleBlk sha_1
	(
				.iRset_n(iReset_n),
				.iClk(iClk),
				.iReq(rShaEnable),

//				.iM0(wTempBlkInput[1023:992]),
//				.iM1(wTempBlkInput[991:960]),
//				.iM2(wTempBlkInput[959:928]),
//				.iM3(wTempBlkInput[927:896]),
//				.iM4(wTempBlkInput[895:864]),
//				.iM5(wTempBlkInput[863:832]),
//				.iM6(wTempBlkInput[831:800]),
//				.iM7(wTempBlkInput[799:768]),
//				.iM8(wTempBlkInput[767:736]),
//				.iM9(wTempBlkInput[735:704]),
//				.iM10(wTempBlkInput[703:672]),
//				.iM11(wTempBlkInput[671:640]),
//				.iM12(wTempBlkInput[639:608]),
//				.iM13(wTempBlkInput[607:576]),
//				.iM14(wTempBlkInput[575:544]),
//				.iM15(wTempBlkInput[543:512]),
//				.iM16(wTempBlkInput[511:480]),
//				.iM17(wTempBlkInput[479:448]),
//				.iM18(wTempBlkInput[447:416]),
//				.iM19(wTempBlkInput[415:384]),
//				.iM20(wTempBlkInput[383:352]),
//				.iM21(wTempBlkInput[351:320]),
//				.iM22(wTempBlkInput[319:288]),
//				.iM23(wTempBlkInput[287:256]),
//				.iM24(wTempBlkInput[255:224]),
//				.iM25(wTempBlkInput[223:192]),
//				.iM26(wTempBlkInput[191:160]),
//				.iM27(wTempBlkInput[159:128 ]),
//				.iM28(wTempBlkInput[127:96]),
//				.iM29(wTempBlkInput[95:64]),
//				.iM30(wTempBlkInput[63:32]),
//				.iM31(wTempBlkInput[31:0]),
				
			    .iM31(32'h0),
                .iM30(32'h0),
                .iM29(32'h0),
                .iM28(32'h0),
                .iM27(32'h0),
                .iM26(32'h0),
                .iM25(32'h0),
                .iM24(32'h0),
                .iM23(32'h0),
                .iM22(32'h0),
                .iM21(32'h0),
                .iM20(32'h0),
                .iM19(32'h0),
                .iM18(32'h0),
                .iM17(32'h0),
                .iM16(32'h0),
                .iM15(32'h0),
                .iM14(32'h0),
  
                .iM13({8'h80,             rTempV[7:0],      rTempV[15:8],     rTempV[23:16]} ),    //rTempV[23:16],rTempV[15:8], rTempV[7:0] 0x80
                .iM12({rTempV[31:24],     rTempV[39:32],    rTempV[47:40],    rTempV[55:48]} ),    //rTempV[55:48], rTempV[47:40] rTempV[39:32],rTempV[31:24],
                .iM11({rTempV[63:56],     rTempV[71:64],    rTempV[79:72],    rTempV[87:80]} ),  //,rTempV[87:80], rTempV[79:72] rTempV[71:64],rTempV[63:56],
                .iM10({rTempV[95:88],     rTempV[103:96],   rTempV[111:104],  rTempV[119:112]}),   //rTempV[119:112], rTempV[111:104] rTempV[103:96],rTempV[95:88]
                .iM9({rTempV[127:120],    rTempV[135:128],  rTempV[143:136],  rTempV[151:144]} ),  //rTempV[151:144], rTempV[143:136] rTempV[135:128],rTempV[127:120],
                .iM8({rTempV[159:152],    rTempV[167:160],  rTempV[175:168],  rTempV[183:176]}),   //rTempV[183:176], rTempV[175:168] rTempV[167:160],rTempV[159:152],
                .iM7({rTempV[191:184],    rTempV[199:192],  rTempV[207:200],  rTempV[215:208]}),   //rTempV[215:208], rTempV[207:200] rTempV[199:192],rTempV[191:184],
                
                .iM6({rTempV[223:216],   rTempV[231:224],  rTempV[239:232],  rTempV[247:240]}),   //rTempV[247:240],rTempV[239:232],rTempV[231:224],rTempV[223:216],
                .iM5({rTempV[255:248],   rTempV[263:256],  rTempV[271:264],  rTempV[279:272]}),   //rTempV[279:272], rTempV[271:264], rTempV[263:256],rTempV[255:248],
                .iM4({rTempV[287:280],   rTempV[295:288],  rTempV[303:296],  rTempV[311:304]}),   //rTempV[311:304], rTempV[303:296],  rTempV[295:288],rTempV[287:280],
                .iM3({rTempV[319:312],   rTempV[327:320],  rTempV[335:328],  rTempV[343:336]}),   //rTempV[343:336], rTempV[335:328], rTempV[327:320],rTempV[319:312],
                .iM2({rTempV[351:344],   rTempV[359:352],  rTempV[367:360],  rTempV[375:368]}),   //rTempV[375:368], rTempV[367:360], rTempV[359:352],rTempV[351:344],
                .iM1({rTempV[383:376],   rTempV[391:384],  rTempV[399:392],  rTempV[407:400]}),   //rTempV[407:400], rTempV[399:392], rTempV[391:384],rTempV[383:376],
                .iM0({rTempV[415:408],   rTempV[423:416],  rTempV[431:424],  rTempV[439:432]}),    //rTempV[439:432],rTempV[431:424] rTempV[423:416],rTempV[415:408],
                
				
				
				
				.oHashValue(oRN256),
				.oDone(oRN256_Valid)					
);						
			
endmodule
