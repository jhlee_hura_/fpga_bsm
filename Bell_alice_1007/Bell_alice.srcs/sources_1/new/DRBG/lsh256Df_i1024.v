//======================================================================
// Title: lsh256Df_i1024.v
// ---------------------------------------------------------------------
// Description	: Derivaration Function for lsh_512_256 based drbg
// Author		: EJ KIM
// Date			: 19th April, 2019
//======================================================================		
module lsh256Df_i1024 ( //E: Entropy, N: once, P: Personalizer
										input wire dfClk,
										input wire dfReset_n,
										input wire dfEnable,
										input wire [983:0] inData, 
										output wire [439:0] outSeed,
										output wire seedValid
						);
						
wire [255:0] wDigest_1;
wire [255:0] wDigest_2;
wire digestValid_1;
wire digestValid_2;

//----------------------------------------------------------------
 // Concurrent connectivity for ports etc.
//----------------------------------------------------------------
  //assign wTempBlk1 = {8'h01, 32'h000001B8, inEntropy[255:0], inNonce[127:0], inPerString[255:0], 1'b1, 543'b0 }; //0X01_000001B8_E[439:0]_1_all's 0 
 // assign wTempBlk2 = {8'h02, 32'h000001B8, inEntropy[439:0], 1'b1, 543'b0 };

  assign seedValid = digestValid_1 && digestValid_2;
  assign outSeed = {wDigest_1[255:0], wDigest_2[255:72]}; 
  
  
  
//  wire wAlignValid1;
//  wire wAlignValid2;
  
	LSH_SingleBlk sha_1
	(
			.iRset_n(dfReset_n),
			.iClk(dfClk),
			.iReq(dfEnable),
				
			
			.iM31({inData[7:0],     inData[15:8],    inData[23:16],    inData[31:24]}),// inData[31:24],inData[23:16],inData[15:8], inData[7:0], 
			.iM30({inData[39:32],   inData[47:40],   inData[55:48],    inData[63:56]}),// inData[63:56],inData[55:48],inData[47:40], inData[39:32], 
			.iM29({inData[71:64],   inData[79:72],   inData[87:80],    inData[95:88]}),// inData[95:88],inData[87:80],inData[79:72], inData[71:64], 
			.iM28({inData[103:96],  inData[111:104], inData[119:112],  inData[127:120]}),// inData[127:120],inData[119:112],inData[111:104], inData[103:96], 
			.iM27({inData[135:128], inData[143:136], inData[151:144],  inData[159:152]}),// inData[159:152],inData[151:144],inData[143:136], inData[135:128], 
			.iM26({inData[167:160], inData[175:168], inData[183:176],  inData[191:184]}),// inData[191:184],inData[183:176],inData[175:168], inData[167:160], 
			.iM25({inData[199:192], inData[207:200], inData[215:208],  inData[223:216]}),// inData[223:216],inData[215:208],inData[207:200], inData[199:192], 
			.iM24({inData[231:224], inData[239:232], inData[247:240],  inData[255:248]}),// inData[255:248],inData[247:240],inData[239:232], inData[231:224], 
			.iM23({inData[263:256], inData[271:264], inData[279:272],  inData[287:280]}),// inData[287:280],inData[279:272],inData[271:264], inData[263:256], 
			.iM22({inData[295:288], inData[303:296], inData[311:304],  inData[319:312]}),// inData[319:312],inData[311:304],inData[303:296], inData[295:288], 
			.iM21({inData[327:320], inData[335:328], inData[343:336],  inData[351:344]}), // inData[351:344],inData[343:336],inData[335:328], inData[327:320], 
		
			.iM20({inData[359:352],  inData[367:360],  inData[375:368], inData[383:376]} ),   // inData[383:376],inData[375:368],inData[367:360], inData[359:352], 
            .iM19({inData[391:384],  inData[399:392],  inData[407:400], inData[415:408]} ),  // inData[415:408],inData[407:400],inData[399:392], inData[391:384], 
            .iM18({inData[423:416],  inData[431:424],  inData[439:432], inData[447:440]} ), // inData[447:440],inData[439:432],inData[431:424], inData[423:416], 
            .iM17({inData[455:448],  inData[463:456],  inData[471:464], inData[479:472]}),  // inData[479:472],inData[471:464],inData[463:456], inData[455:448], 
            .iM16({inData[487:480],  inData[495:488],  inData[503:496], inData[511:504]} ), // inData[511:504],inData[503:496],inData[495:488], inData[487:480],    
            .iM15({inData[519:512],  inData[527:520],  inData[535:528], inData[543:536]}),  //inData[543:536],inData[535:528],inData[527:520], inData[519:512],      
            .iM14({inData[551:544],  inData[559:552],  inData[567:560], inData[575:568]}), //inData[575:568],inData[567:560],inData[559:552], inData[551:544],       
			
			.iM13({inData[583:576],  inData[591:584],  inData[599:592], inData[607:600]} ),   //inData[607:600],inData[599:592],inData[591:584], inData[583:576],      
			.iM12({inData[615:608],  inData[623:616],  inData[631:624], inData[639:632]}),        //inData[639:632],inData[ 631:624],inData[623:616], inData[615:608],                
			.iM11({inData[647:640],  inData[655:648],  inData[663:656], inData[671:664]}),       //inData[671:664],inData[663:656],inData[ 655 : 648 ], inData[647:640],       
			.iM10({inData[679:672],  inData[687:680],  inData[695:688], inData[703:696]}),      //inData[703:696],inData[695:688],inData[687: 680], inData[679:672],       
			.iM9({inData[711:704],   inData[719:712],  inData[727:720], inData[735:728]}),       //inData[735:728],inData[727:720],inData[719: 712], inData[711:704],
			.iM8({inData[743:736],   inData[751:744],  inData[759:752], inData[767:760]} ),    //inData[767:760],inData[759:752],inData[751: 744], inData[743:736],   
			.iM7({inData[775:768],   inData[783:776],  inData[791:784], inData[799:792]} ),    //inData[799:792],inData[791:784],inData[783:776], inData[775:768],
			.iM6({inData[807:800],   inData[815:808],  inData[823:816], inData[831:824]}),   //inData[831:824],inData[823:816],inData[815:808], inData[807:800],
			.iM5({inData[839:832],   inData[847:840],  inData[855:848], inData[863:856]}),   //inData[863:856],inData[855:848],inData[847:840], inData[839:832],
			.iM4({inData[871:864],   inData[879:872],  inData[887:880], inData[895:888]}),  //inData[895:888],inData[887: 880],inData[879:872], inData[871:864],
			.iM3({inData[903:896],   inData[911:904],  inData[919:912], inData[927:920]}),   //inData[927:920],inData[919:912],inData[911:904], inData[903:896],
			.iM2({inData[935:928],   inData[943:936],  inData[951:944], inData[959:952]}),   //inData[959:952],inData[951:944],inData[943: 936], inData[935:928],
			
			.iM1({inData[967: 960 ],   inData[975:  968],  inData[983: 976 ],  8'hB8}),                //0xB8, inData[983: 976 ],inData[975:  968],inData[967: 960 ],
			.iM0(32'h01000001), //0x01 00 00 01
   
			.oHashValue(wDigest_1),
			.oDone(digestValid_1)					
);						
	LSH_SingleBlk sha_2
	(
			.iRset_n(dfReset_n),
			.iClk(dfClk),
			.iReq(dfEnable),
			
			.iM31({inData[7:0],     inData[15:8],    inData[23:16],    inData[31:24]}),// inData[31:24],inData[23:16],inData[15:8], inData[7:0], 
            .iM30({inData[39:32],   inData[47:40],   inData[55:48],    inData[63:56]}),// inData[63:56],inData[55:48],inData[47:40], inData[39:32], 
            .iM29({inData[71:64],   inData[79:72],   inData[87:80],    inData[95:88]}),// inData[95:88],inData[87:80],inData[79:72], inData[71:64], 
            .iM28({inData[103:96],  inData[111:104], inData[119:112],  inData[127:120]}),// inData[127:120],inData[119:112],inData[111:104], inData[103:96], 
            .iM27({inData[135:128], inData[143:136], inData[151:144],  inData[159:152]}),// inData[159:152],inData[151:144],inData[143:136], inData[135:128], 
            .iM26({inData[167:160], inData[175:168], inData[183:176],  inData[191:184]}),// inData[191:184],inData[183:176],inData[175:168], inData[167:160], 
            .iM25({inData[199:192], inData[207:200], inData[215:208],  inData[223:216]}),// inData[223:216],inData[215:208],inData[207:200], inData[199:192], 
            .iM24({inData[231:224], inData[239:232], inData[247:240],  inData[255:248]}),// inData[255:248],inData[247:240],inData[239:232], inData[231:224], 
            .iM23({inData[263:256], inData[271:264], inData[279:272],  inData[287:280]}),// inData[287:280],inData[279:272],inData[271:264], inData[263:256], 
            .iM22({inData[295:288], inData[303:296], inData[311:304],  inData[319:312]}),// inData[319:312],inData[311:304],inData[303:296], inData[295:288], 
            .iM21({inData[327:320], inData[335:328], inData[343:336],  inData[351:344]}), // inData[351:344],inData[343:336],inData[335:328], inData[327:320], 
        
            .iM20({inData[359:352],  inData[367:360],  inData[375:368], inData[383:376]} ),   // inData[383:376],inData[375:368],inData[367:360], inData[359:352], 
            .iM19({inData[391:384],  inData[399:392],  inData[407:400], inData[415:408]} ),  // inData[415:408],inData[407:400],inData[399:392], inData[391:384], 
            .iM18({inData[423:416],  inData[431:424],  inData[439:432], inData[447:440]} ), // inData[447:440],inData[439:432],inData[431:424], inData[423:416], 
            .iM17({inData[455:448],  inData[463:456],  inData[471:464], inData[479:472]}),  // inData[479:472],inData[471:464],inData[463:456], inData[455:448], 
            .iM16({inData[487:480],  inData[495:488],  inData[503:496], inData[511:504]} ), // inData[511:504],inData[503:496],inData[495:488], inData[487:480],    
            .iM15({inData[519:512],  inData[527:520],  inData[535:528], inData[543:536]}),  //inData[543:536],inData[535:528],inData[527:520], inData[519:512],      
            .iM14({inData[551:544],  inData[559:552],  inData[567:560], inData[575:568]}), //inData[575:568],inData[567:560],inData[559:552], inData[551:544],       
            
            .iM13({inData[583:576],  inData[591:584],  inData[599:592], inData[607:600]} ),   //inData[607:600],inData[599:592],inData[591:584], inData[583:576],      
            .iM12({inData[615:608],  inData[623:616],  inData[631:624], inData[639:632]}),        //inData[639:632],inData[ 631:624],inData[623:616], inData[615:608],                
            .iM11({inData[647:640],  inData[655:648],  inData[663:656], inData[671:664]}),       //inData[671:664],inData[663:656],inData[ 655 : 648 ], inData[647:640],       
            .iM10({inData[679:672],  inData[687:680],  inData[695:688], inData[703:696]}),      //inData[703:696],inData[695:688],inData[687: 680], inData[679:672],       
            .iM9({inData[711:704],   inData[719:712],  inData[727:720], inData[735:728]}),       //inData[735:728],inData[727:720],inData[719: 712], inData[711:704],
            .iM8({inData[743:736],   inData[751:744],  inData[759:752], inData[767:760]} ),    //inData[767:760],inData[759:752],inData[751: 744], inData[743:736],   
            .iM7({inData[775:768],   inData[783:776],  inData[791:784], inData[799:792]} ),    //inData[799:792],inData[791:784],inData[783:776], inData[775:768],
            .iM6({inData[807:800],   inData[815:808],  inData[823:816], inData[831:824]}),   //inData[831:824],inData[823:816],inData[815:808], inData[807:800],
            .iM5({inData[839:832],   inData[847:840],  inData[855:848], inData[863:856]}),   //inData[863:856],inData[855:848],inData[847:840], inData[839:832],
            .iM4({inData[871:864],   inData[879:872],  inData[887:880], inData[895:888]}),  //inData[895:888],inData[887: 880],inData[879:872], inData[871:864],
            .iM3({inData[903:896],   inData[911:904],  inData[919:912], inData[927:920]}),   //inData[927:920],inData[919:912],inData[911:904], inData[903:896],
            .iM2({inData[935:928],   inData[943:936],  inData[951:944], inData[959:952]}),   //inData[959:952],inData[951:944],inData[943: 936], inData[935:928],
            
            .iM1({inData[967: 960 ],   inData[975:  968],  inData[983: 976 ],  8'hB8}),                //0xB8, inData[983: 976 ],inData[975:  968],inData[967: 960 ],
			.iM0(32'h01000002), //0x02 00 00 01
	
			.oHashValue(wDigest_2),
			.oDone(digestValid_2)	
	);	
endmodule
