//========================================================================================================================================
// Title: drbgCore_256N.v
// ---------------------------------------------------------------------
// Description	: Core Block for Deterministic Random Number Generation 
// Date			: 26th April, 2019 // 512bit 생성인데... 256bit 씩 FIFO에 넣을 수 있도록 설계
//========================================================================================================================================

module drbgCore_256N(
						input 	wire				iClk,
						input 	wire				iReset_n,
						input 	wire 				iEnable,
						input   wire               	iExternEntropyEn,
						input 	wire	[255:0]		iEntropy,
						input   wire    [127:0]    	inNonce,
                        input   wire    [255:0]    	inPerString,
                        input   wire    [255:0]    	iAdditional,
						output  wire               	oEntropyReq, 
						output	wire	[255:0]	    oRandomNo,
						output	wire			    oRandomNoValid

					);
					
	
parameter	CTRL_IDLE					=	0;
parameter	CTRL_INIT_TRIG_0			=	1;
parameter	CTRL_INIT_TRIG				=	2;
parameter	CTRL_INIT					=	3;
parameter	CTRL_RENEW_V_IN_TRIG_0		=	4;
parameter	CTRL_RENEW_V_IN_TRIG		=	5;
parameter	CTRL_RENEW_V_IN			    =	6;
parameter	CTRL_RN1_GEN_TRIG_0			=	7; // including stateV/stateC updates
parameter	CTRL_RN1_GEN_TRIG			=	8; // including stateV/stateC updates
parameter  CTRL_RN1_GEN               	=   9; // including stateV/stateC updates
parameter  CTRL_DECISIN_IN_EXT        	=   10; // including stateV/stateC updates
parameter	CTRL_RENEW_V_EXT_TRIG_0     =   11;
parameter	CTRL_RENEW_V_EXT_TRIG_1	    =   12;
parameter	CTRL_RENEW_V_EXT			=	13;
parameter	CTRL_FIFO_RD_0				=	14;
parameter	CTRL_FIFO_RD_1				=	15;

//parameter	RESEED_CNT_SET		=	48'hFFFFFFFFFFFF; // reseedCnt = 1;
//parameter	RESEED_CNT_SET		=	48'h000000000100; // reseedCnt = 1; 0x100 = 256, 0x80 = 128, 
parameter	RESEED_CNT_SET		=	2; // reseedCnt = 1; 0x100 = 256, 0x80 = 128, 
parameter	RESEED_CNT_INIT  	=	1;


////////////////////////////////////////////////////////////////////////////////////////////
//delay  Valid Signal with 1/2 Clk
////////////////////////////////////////////////////////////////////////////////////////////
parameter STATE_DELAY_IDLE       = 0;
parameter STATE_DELAY_1CLK       = 1;
parameter STATE_DELAY_2CLK       = 2;
parameter STATE_DELAY_3CLK       = 3;

reg rTEmpReStateValid;
reg [3:0] rState_enable;
reg rEnable_randomNoGen;

////////////////////////////////////////////////////////////////////////////////////////////
///// 256bit 씩 FIFO 에 저장하도록 하기 위해 필요한 레지스터

reg [7:0] rTempCntClk;

reg [11:0] rTempRnIternalCnt;

wire [255:0]wTempRN256;

/////////////////////////////////////////////////////////////
reg	rEnable_init;
reg	rEnable_inRenewV;


reg	[439:0]	rStateV;
reg	[439:0]	rStateC;

reg		rCtrlState_RENEW_V_EXT; //FIFO RD enable 신호 ,  0==> INIT 용 FIFO Reading. 1==> REVEW_V_EXT  용 FIFO Reading

wire	[439:0]	wNewStateV_init;

wire	[439:0]	wNewStateC_init;

reg	[5:0]		ctrlState;
reg	[47:0]		rReSeedCnt;
reg 			rTempEntropyReq;

reg [983:0] 	rTempDfInput;
reg [1023:0] 	rTempLshInput;

wire[255:0] 	wDigest;
wire 			wDigestValid;

wire	wInitComplete;
wire	wRandomNoGen_valid;
wire 	wRN_COmplete;

assign oRandomNo = wTempRN256;
assign oRandomNoValid = wRandomNoGen_valid ; // //TTA TEST MODE
assign 	oEntropyReq = rTempEntropyReq;

always @(posedge iClk or negedge iReset_n) begin
			if(!iReset_n) begin
				rStateV				<=	440'h0;
				rStateC				<=	440'h0;
				rTempLshInput 		<= 1024'h0;
				rEnable_init		<= 1'b0;
				rEnable_inRenewV	<= 1'b0;
                rTempDfInput        <= 984'h0;
				rEnable_randomNoGen	<= 1'b0;
				rTempRnIternalCnt  	<= 12'h0;
				rTempCntClk 		<= 8'h0;
				rReSeedCnt			<= 48'h000000000001;
				rTempEntropyReq  	<= 1'b0;		
				rCtrlState_RENEW_V_EXT	<= 1'b0;					
				ctrlState			<=	CTRL_IDLE;
			end // if(!iReset_n) begin
			else begin
				case(ctrlState)
					CTRL_IDLE: begin
					    if(iEnable) begin
							rStateV				<=	440'h0;
							rStateC				<=	440'h0;	      		
                            rTempLshInput 		<= 1024'h0;
							rEnable_init		<= 1'b0;
							rEnable_inRenewV	<= 1'b0;
							rTempDfInput        <= rTempDfInput;
							rEnable_randomNoGen <= 1'b0;
							rTempRnIternalCnt  	<= 12'h0;
							rTempCntClk 		<= 8'h0;
    						rReSeedCnt          <= 48'h000000000001;
							rTempEntropyReq     <= 1'b0;
							rCtrlState_RENEW_V_EXT	<= 1'b0;
						    ctrlState	        <= CTRL_FIFO_RD_0;
						end
						else begin
							rStateV				<=	440'h0;
							rStateC				<=	440'h0;      			
                            rTempLshInput 		<= 	1024'h0;
							rEnable_init		<=	1'b0;
							rEnable_inRenewV	<=	1'b0;
							rTempDfInput        <= 	rTempDfInput;
							rEnable_randomNoGen	<=	1'b0;
							rTempRnIternalCnt  	<= 	12'h0;
							rTempCntClk 		<= 	8'h0;
							rReSeedCnt			<= 	48'h000000000001;	
							rTempEntropyReq     <= 	1'b0;
							rCtrlState_RENEW_V_EXT	<= 1'b0;
							ctrlState	        <= 	CTRL_IDLE;						
						end
					end //CTRL_IDLE: begin
					CTRL_FIFO_RD_0: begin 
						if(iExternEntropyEn) begin
							rEnable_init            <= 1'b0;
							rEnable_inRenewV        <= 1'b0; 
							rTempDfInput          	<= rTempDfInput;
							rEnable_randomNoGen    	<= 1'b0;
							rTempRnIternalCnt  		<= rTempRnIternalCnt;
							rTempCntClk 			<= rTempCntClk;
							rTempLshInput 			<= rTempLshInput;
							rStateV        			<= rStateV;
							rStateC        			<= rStateC;
							rReSeedCnt    			<= rReSeedCnt;
							rTempEntropyReq  		<= 1'b1;
							rCtrlState_RENEW_V_EXT	<= rCtrlState_RENEW_V_EXT;
							ctrlState    			<= CTRL_FIFO_RD_1;
						end  //if(iExternEntropyEn) begin
						else begin 
							rEnable_init            <= 1'b0;
							rEnable_inRenewV        <= 1'b0; 
							rTempDfInput          	<= rTempDfInput;
							rEnable_randomNoGen    	<= 1'b0;
							rTempRnIternalCnt  		<= rTempRnIternalCnt;
							rTempCntClk 			<= rTempCntClk;
							rTempLshInput 			<= rTempLshInput;
							rStateV        			<= rStateV;
							rStateC        			<= rStateC;
							rReSeedCnt    			<= rReSeedCnt;
							rTempEntropyReq  		<= 1'b0;
							rCtrlState_RENEW_V_EXT	<= rCtrlState_RENEW_V_EXT;
							ctrlState    			<= CTRL_FIFO_RD_0;
						end //~if(iExternEntropyEn) begin
                    end //CTRL_FIFO_RD_0: begin  
					CTRL_FIFO_RD_1: begin 
                        rEnable_init            <= 1'b0;
                        rEnable_inRenewV        <= 1'b0; 
                        rTempDfInput          	<= rTempDfInput;
                        rEnable_randomNoGen    	<= 1'b0;
                        rTempRnIternalCnt  		<= rTempRnIternalCnt;
                        rTempCntClk 			<= rTempCntClk;
                        rTempLshInput 			<= rTempLshInput;
                        rStateV        			<= rStateV;
                        rStateC        			<= rStateC;
                        rReSeedCnt    			<= rReSeedCnt;
                        rTempEntropyReq  		<= 1'b0;
						rCtrlState_RENEW_V_EXT	<= rCtrlState_RENEW_V_EXT;
						if(rCtrlState_RENEW_V_EXT)
							ctrlState    			<= CTRL_RENEW_V_EXT_TRIG_0;
						else
							ctrlState    			<= CTRL_INIT_TRIG_0;
                    end //CTRL_FIFO_RD_1: begin  
					CTRL_INIT_TRIG_0: begin 
                        rEnable_init            <= 1'b0;
                        rEnable_inRenewV        <= 1'b0; 
                        rTempDfInput          	<= {iEntropy,inNonce,inPerString, 8'h80, 336'h0};
                        rEnable_randomNoGen    	<= 1'b0;
                        rTempRnIternalCnt  		<= 12'h0;
                        rTempCntClk 			<= 8'h0;
                        rTempLshInput 			<= rTempLshInput;
                        rStateV        			<= rStateV;
                        rStateC        			<= rStateC;
                        rReSeedCnt    			<= rReSeedCnt;
                        rTempEntropyReq  		<= 1'b0;
						rCtrlState_RENEW_V_EXT	<= rCtrlState_RENEW_V_EXT;
                        ctrlState    			<= CTRL_INIT_TRIG;
                    end //CTRL_INIT: begin        
					CTRL_INIT_TRIG: begin //Entropy Update
						rEnable_init			<=	1'b1;
						rEnable_inRenewV		<=	1'b0;
						rTempDfInput            <= rTempDfInput;
						//TTA TEST MODE 
						//rTempDfInput          	<= {256'h7145910782ACCB48308ABB1C0A4107227B9F1AA8F26A6CD53F3C032741913A21,128'hBE1FC13D9266E5280C87112E955995F3, 256'hA1F6BEBDAF3ECD15519841753BF5147DE010E9D693FD4C68EC053ACD6EB1E405, 8'h80, 336'h0};
						rEnable_randomNoGen		<=	1'b0;
						rTempRnIternalCnt  		<= 12'h0;
						rTempCntClk 			<= 8'h0;
						rTempLshInput 			<= rTempLshInput;
						rStateV					<=	rStateV;
						rStateC					<=	rStateC;
						rReSeedCnt				<=	rReSeedCnt;
						rTempEntropyReq  		<=	1'b0;
						rCtrlState_RENEW_V_EXT	<= rCtrlState_RENEW_V_EXT;
						ctrlState				<=	CTRL_INIT;
					end //CTRL_INIT: begin					
					CTRL_INIT: begin
						rEnable_init			<=	1'b0;
						rEnable_inRenewV		<=	1'b0;
						rTempDfInput            <= 	rTempDfInput;
						rTempLshInput 			<= 	rTempLshInput;
						rEnable_randomNoGen		<=	1'b0;
						rTempRnIternalCnt  		<= 	12'h0;
						rTempCntClk 			<= 	8'h0;
						rTempEntropyReq  		<=	1'b0;
						rReSeedCnt				<=	rReSeedCnt;
						rCtrlState_RENEW_V_EXT	<= rCtrlState_RENEW_V_EXT;
						if(wInitComplete) begin
							rStateV				<=	wNewStateV_init;
                            rStateC				<=	wNewStateC_init;
                            ctrlState			<=	CTRL_RENEW_V_IN_TRIG_0;
                        end //	if(rReSeedCnt < RESEED_CNT_SET) begin						
						else begin							
                            rStateV				<=	rStateV;
                            rStateC				<=	rStateC;
                            rReSeedCnt			<=	rReSeedCnt;
                            ctrlState			<=	CTRL_INIT;
						end // if(rReSeedCnt < RESEED_CNT_SET) else begin
					end //CTRL_INIT_TRIG: begin
					CTRL_RENEW_V_IN_TRIG_0: begin
                        rEnable_init            <= 1'b0;
                        rEnable_inRenewV        <= 1'b0;
                        rTempDfInput            <= rTempDfInput;
                        rTempLshInput <= { 8'h02, rStateV[439:0], iAdditional, 8'h80, 312'h0};
                        //TTA TEST MODE 
                        //rTempLshInput 			<= { 8'h02, rStateV[439:0], 256'h6625B06B16AF81E713A03866EC5B7B870CABB597E25A5DC03FFF7C7DFF176951, 8'h80, 312'h0};
                                               
                        rEnable_randomNoGen    	<=  1'b0;
                        rTempRnIternalCnt  		<= 	12'h0;
                        rTempCntClk 			<= 	8'h0;
                        rTempEntropyReq  		<=	1'b0;
                        rStateV        			<=  rStateV;
                        rStateC        			<=  rStateC;
                        rReSeedCnt    			<=  rReSeedCnt;
						rCtrlState_RENEW_V_EXT	<= rCtrlState_RENEW_V_EXT;
                        ctrlState    			<=  CTRL_RENEW_V_IN_TRIG;
                    end //CTRL_RENEW_V_IN_TRIG: begin	
					CTRL_RENEW_V_IN_TRIG: begin
						rEnable_init			<=	1'b0;
						rEnable_inRenewV		<=	1'b1;
						rTempDfInput            <= 	rTempDfInput;
						rTempLshInput 			<= 	rTempLshInput;
						rEnable_randomNoGen		<=	1'b0;
						rTempRnIternalCnt  		<= 	12'h0;
						rTempCntClk 			<= 	8'h0;
						rTempEntropyReq  		<=	1'b0;
						rStateV					<=	rStateV;
						rStateC					<=	rStateC;
						rReSeedCnt				<=	rReSeedCnt;
						rCtrlState_RENEW_V_EXT	<= rCtrlState_RENEW_V_EXT;
						ctrlState				<=	CTRL_RENEW_V_IN;
					end //CTRL_RENEW_V_IN_TRIG: begin
					
					CTRL_RENEW_V_IN: begin
					   rEnable_init				<= 	1'b0;
                       rEnable_inRenewV        	<= 	1'b0;
                      rTempDfInput            	<= 	rTempDfInput;
                      rTempLshInput 			<= 	rTempLshInput;
                       rEnable_randomNoGen    	<= 	1'b0;
                       rTempRnIternalCnt 		<= 	12'h0;
                       rTempCntClk 				<= 	8'h0;
                       rTempEntropyReq  		<=	1'b0;
					   rCtrlState_RENEW_V_EXT	<= rCtrlState_RENEW_V_EXT;
						if(wDigestValid) begin					        
								rStateV		<=	wDigest + rStateV;
								rStateC		<=	rStateC;
								rReSeedCnt	<=	rReSeedCnt;
								ctrlState	<=	CTRL_RN1_GEN_TRIG_0;
						end // if(wDigestValid)
						else begin							
								rStateV		<=	rStateV;
								rStateC		<=	rStateC;
								rReSeedCnt	<=	rReSeedCnt;
								ctrlState	<=	CTRL_RENEW_V_IN;
						end // if(wDigestValid) else begin
					end //CTRL_RENEW_V_IN: begin
					
					CTRL_RN1_GEN_TRIG_0: begin
                        if(iEnable) begin
							rEnable_init            <= 1'b0;
							rEnable_inRenewV        <= 1'b0;
							rTempDfInput            <= rTempDfInput;
							rTempLshInput 			<= {8'h03, rStateV, 8'h80, 568'h0};
							rEnable_randomNoGen    	<= 1'b0;
							rTempRnIternalCnt  		<= 12'h0;
							rTempCntClk 			<= 8'h0;
							rTempEntropyReq  		<= 1'b0;                        
							rStateV        			<= rStateV;
							rStateC        			<= rStateC;
							rReSeedCnt    			<= rReSeedCnt;
							rCtrlState_RENEW_V_EXT	<= rCtrlState_RENEW_V_EXT;
							ctrlState    			<= CTRL_RN1_GEN_TRIG;
						end 
						else begin  //~if(iEnable) begin
							rEnable_init            <= rEnable_init;
							rEnable_inRenewV        <= rEnable_inRenewV;
							rTempDfInput            <= rTempDfInput;
							rTempLshInput 			<= rTempLshInput;
							rEnable_randomNoGen    	<= rEnable_randomNoGen;
							rTempRnIternalCnt  		<= rTempRnIternalCnt;
							rTempCntClk 			<= rTempCntClk;
							rTempEntropyReq  		<= rTempEntropyReq;                        
							rStateV        			<= rStateV;
							rStateC        			<= rStateC;
							rReSeedCnt    			<= rReSeedCnt;
							rCtrlState_RENEW_V_EXT	<= rCtrlState_RENEW_V_EXT;
							ctrlState    			<= CTRL_IDLE;
						end //~if(iEnable) begin
                    end // CTRL_RN_GEN: begin
					CTRL_RN1_GEN_TRIG: begin //CTRL_UPDATE__TRIG
                        rEnable_init            <=  1'b0;
                        rEnable_inRenewV        <=  1'b1;
                        rTempDfInput            <= 	rTempDfInput;
                        rTempLshInput 			<= 	rTempLshInput;
                        rEnable_randomNoGen    	<=  1'b1;
                        rTempCntClk 			<= 	8'h0; 
                        rTempEntropyReq  		<=	1'b0;                        
                        rStateV        			<=  rStateV;
                        rStateC        			<=  rStateC;
                        rReSeedCnt    			<=  rReSeedCnt;
						rCtrlState_RENEW_V_EXT	<= rCtrlState_RENEW_V_EXT;
                        ctrlState    			<=  CTRL_RN1_GEN;
                    end // CTRL_RN_GEN: begin 
                    CTRL_RN1_GEN: begin
                       rEnable_init            	<=  1'b0;
                       rEnable_inRenewV        	<=  1'b0;
                       rEnable_randomNoGen     	<=  1'b0;
                       rTempCntClk  			<= rTempCntClk;   
                       rTempEntropyReq  		<=1'b0;
					   rCtrlState_RENEW_V_EXT	<= rCtrlState_RENEW_V_EXT;
                       if(wRN_COmplete) begin //Update V
                               rStateV       <=    wDigest + rStateC + rStateV+ rReSeedCnt;
                               rStateC       <=    rStateC;
                               rReSeedCnt    <=    rReSeedCnt + 48'h1;
                               rTempDfInput  <= rTempDfInput;
                               rTempLshInput <= rTempLshInput;
                               ctrlState     <= CTRL_DECISIN_IN_EXT ;  
                       end // if(wRN_COmplete) begin
                       else begin                                                 
                               rStateV       	<=  rStateV;
                               rStateC       	<=  rStateC;
                               rReSeedCnt    	<=  rReSeedCnt;
                               rTempDfInput  	<=  rTempDfInput;
                               rTempLshInput 	<=  rTempLshInput;
                               ctrlState    	<=  CTRL_RN1_GEN;
                       end //  if(wRN_COmplete) else begin
                   end // CTRL_RN_GEN: begin    
                  CTRL_DECISIN_IN_EXT: begin //
                       rEnable_init            	<=  1'b0;
                       rEnable_inRenewV        	<=  1'b0;
                       rTempDfInput            	<= 	rTempDfInput;
                       rTempLshInput 			<= 	rTempLshInput;
                       rEnable_randomNoGen    	<=  1'b0;
                       rTempCntClk 				<= 	8'h0; 
                       rTempEntropyReq  		<=	1'b0;                        
                       rStateV        			<=  rStateV;
                       rStateC        			<=  rStateC;
                       
                       if(rReSeedCnt < RESEED_CNT_SET) begin 
						   rCtrlState_RENEW_V_EXT	<= rCtrlState_RENEW_V_EXT;
                           ctrlState    		<=  CTRL_RENEW_V_IN_TRIG_0;
                           rReSeedCnt   		<= 	rReSeedCnt + 1'b1;
                       end //~CTRL_DECISIN_IN_EXT:
                       else begin //
							rCtrlState_RENEW_V_EXT	<= 1'b1;
                            ctrlState    		<=  CTRL_FIFO_RD_0;
                            rReSeedCnt   		<=  rReSeedCnt;
                       end 
                    end // CTRL_DECISIN_IN_EXT: begin    
					CTRL_RENEW_V_EXT_TRIG_0: begin //CTRL_RENEW_V_EXT_TRIG_0
                        rEnable_init            <=    1'b0;
                        rEnable_inRenewV        <=    1'b0;
                         //TTA TEST MODE
                        //z  			<= {8'h01, rStateV,256'h92BAA7658C23A7EE8E80A8EECF3E2B6891A52DFC49686515007AC763F9244C8C,256'hEB57D7B9DE41125F27F686902F4B81F05C1E3A6D34EB1171C69A185C459BD331 , 8'h80, 16'h0};
                        rTempDfInput  			<= {8'h01, rStateV,iEntropy,iAdditional, 8'h80, 16'h0}; 
                        rTempLshInput 			<= rTempLshInput;
                        rEnable_randomNoGen    	<= 1'b0;
                        rTempCntClk  			<= rTempCntClk;
                        rStateV       			<= rStateV;
                        rStateC        			<= rStateC;
                        rReSeedCnt    			<= rReSeedCnt ;
                        rCtrlState_RENEW_V_EXT	<= rCtrlState_RENEW_V_EXT;
                        ctrlState    			<=  CTRL_RENEW_V_EXT_TRIG_1;
                    end // CTRL_RENEW_V_EXT_TRIG_0 
					CTRL_RENEW_V_EXT_TRIG_1: begin
                            rEnable_init		<=	1'b1;
                            rEnable_inRenewV	<=	1'b0;
                            rTempDfInput        <= rTempDfInput;
                            rTempLshInput 		<= rTempLshInput;
                            rEnable_randomNoGen	<=	1'b0;
                            rTempCntClk  		<= rTempCntClk;
                            rTempEntropyReq  	<=	1'b0;
                            rStateV				<=	rStateV;
                            rStateC				<=	rStateC;
                            rReSeedCnt			<=	rReSeedCnt;
							rCtrlState_RENEW_V_EXT	<= rCtrlState_RENEW_V_EXT;
                            ctrlState			<=	CTRL_RENEW_V_EXT;           
					end // CTRL_RENEW_V_EXT_TRIG_1: begin
					CTRL_RENEW_V_EXT: begin
						rEnable_init			<=	1'b0;
						rEnable_inRenewV		<=	1'b0;
						rTempDfInput            <= rTempDfInput;
						rTempLshInput 			<= rTempLshInput;
						rEnable_randomNoGen		<=	1'b0;
						rTempCntClk  			<=  rTempCntClk;
						rTempEntropyReq  		<= 1'b0;
						rCtrlState_RENEW_V_EXT	<= rCtrlState_RENEW_V_EXT;
						if(wInitComplete) begin
								rStateV		<=	wNewStateV_init;
								rStateC		<=	wNewStateC_init;
								rReSeedCnt	<=	RESEED_CNT_INIT;
								ctrlState	<=	CTRL_RN1_GEN_TRIG_0;
						end // if(wInitComplete)
						else begin							
								rStateV		<=	rStateV;
								rStateC		<=	rStateC;
								rReSeedCnt	<=	rReSeedCnt;
								ctrlState	<=	CTRL_RENEW_V_EXT;
						end // if(wInitComplete) else begin
					end // CTRL_RENEW_V_EXT: begin
				    default: begin 
				        rStateV				<=	440'h0;
                        rStateC    			<=  440'h0;            
        
                        rEnable_init        <=  1'b0;
                        rEnable_inRenewV    <=  1'b0;
                        rTempDfInput        <= 	rTempDfInput;
                        rTempLshInput 		<= 	rTempLshInput;
                        rEnable_randomNoGen <=  1'b0;
                        rReSeedCnt          <=  48'h000000000001;
                        rTempEntropyReq  	<=	1'b0;  
						rCtrlState_RENEW_V_EXT	<= rCtrlState_RENEW_V_EXT;						
                        ctrlState           <=  CTRL_IDLE;
				    end //default: begin 
				endcase
			end //// if(!iReset_n) else begin
		end //always @(posedge iClk or negedge iReset_n)
	
lshDrbgInit_iE	initStateVC(
							.iClk(iClk),
							.iReset_n(iReset_n),
							.iEnable(rEnable_init),
							.inData(rTempDfInput),
							.oStateV(wNewStateV_init),
							.oStateC(wNewStateC_init),
							.oInitComplete(wInitComplete)
);
					

LSH_SingleBlk lsh_1Block	(

		.iClk(iClk),
        .iRset_n(iReset_n),
        .iReq(rEnable_inRenewV),
	
     
        .iM31({rTempLshInput[7:0],     rTempLshInput[15:8],     rTempLshInput[23:16],   rTempLshInput[31:24]} ),    //iStateV[31:24],iStateV[23:16],iStateV[15:8],iStateV[7:0],
        .iM30({rTempLshInput[39:32],   rTempLshInput[47:40],    rTempLshInput[55:48],    rTempLshInput[63:56]} ),    //iStateV[63:56],iStateV[55:48], iStateV[47:40], iStateV[39:32],
        .iM29({rTempLshInput[71:64],   rTempLshInput[79:72],    rTempLshInput[87:80],    rTempLshInput[95:88]} ),  //iStateV[95:88],iStateV[87:80], iStateV[79:72], iStateV[71:64],
        .iM28({rTempLshInput[103:96],  rTempLshInput[111:104],  rTempLshInput[119:112],  rTempLshInput[127:120]}),   //iStateV[127:120],iStateV[119:112], iStateV[111:104], iStateV[103:96],
        .iM27({rTempLshInput[135:128], rTempLshInput[143:136],  rTempLshInput[151:144],  rTempLshInput[159:152]} ),  //,iStateV[159:152],iStateV[151:144], iStateV[143:136], iStateV[135:128],
        .iM26({rTempLshInput[167:160],  rTempLshInput[175:168],  rTempLshInput[183:176],  rTempLshInput[191:184]}),   //iStateV[191:184],iStateV[183:176], iStateV[175:168], iStateV[167:160]
        .iM25({rTempLshInput[199:192],  rTempLshInput[207:200],  rTempLshInput[215:208],  rTempLshInput[223:216]}),   //iStateV[223:216],iStateV[215:208], iStateV[207:200], iStateV[199:192],
        
        .iM24({rTempLshInput[231:224],  rTempLshInput[239:232],  rTempLshInput[247:240],  rTempLshInput[255:248]}),   //,iStateV[255:248],iStateV[247:240],iStateV[239:232],iStateV[231:224],
        .iM23({rTempLshInput[263:256],  rTempLshInput[271:264],  rTempLshInput[279:272],  rTempLshInput[287:280]}),   //iStateV[287:280],iStateV[279:272], iStateV[271:264],iStateV[263:256]
        .iM22({rTempLshInput[295:288],  rTempLshInput[303:296],  rTempLshInput[311:304],  rTempLshInput[319:312]}),   //,iStateV[319:312],iStateV[311:304], iStateV[303:296],iStateV[295:288],
        .iM21({rTempLshInput[327:320],  rTempLshInput[335:328],  rTempLshInput[343:336],  rTempLshInput[351:344]}),   //iStateV[351:344],iStateV[343:336], iStateV[335:328],iStateV[327:320]
        .iM20({rTempLshInput[359:352],  rTempLshInput[367:360],  rTempLshInput[375:368],  rTempLshInput[383:376]}),   //iStateV[383:376],iStateV[375:368], iStateV[367:360],iStateV[359:352],
        .iM19({rTempLshInput[391:384],  rTempLshInput[399:392],  rTempLshInput[407:400],  rTempLshInput[415:408]}),   //,iStateV[415:408],iStateV[407:400], iStateV[399:392],iStateV[391:384],
        .iM18({rTempLshInput[423:416],  rTempLshInput[431:424],  rTempLshInput[439:432],   rTempLshInput[447:440]}),                //0x02,iStateV[439:432],iStateV[431:424],iStateV[423:416]
     
        
        .iM17({rTempLshInput[455:448], rTempLshInput[463:456], rTempLshInput[471:464], rTempLshInput[479:472]} ),
        .iM16({rTempLshInput[487:480], rTempLshInput[495:488], rTempLshInput[503:496], rTempLshInput[511:504]} ),
        .iM15({rTempLshInput[519:512], rTempLshInput[527:520], rTempLshInput[535:528], rTempLshInput[543:536]} ),
        .iM14({rTempLshInput[551:544], rTempLshInput[559:552], rTempLshInput[567:560], rTempLshInput[575:568]} ),
        .iM13({rTempLshInput[583:576], rTempLshInput[591:584], rTempLshInput[599:592], rTempLshInput[607:600]} ),
        .iM12({rTempLshInput[615:608], rTempLshInput[623:616], rTempLshInput[631:624], rTempLshInput[639:632]} ),
        .iM11({rTempLshInput[647:640], rTempLshInput[655:648], rTempLshInput[663:656], rTempLshInput[671:664]} ),
        .iM10({rTempLshInput[679:672], rTempLshInput[687:680], rTempLshInput[695:688], rTempLshInput[703:696]} ),
        .iM9({rTempLshInput[711:704], rTempLshInput[719:712], rTempLshInput[727:720], rTempLshInput[735:728]} ),
        .iM8({rTempLshInput[743:736], rTempLshInput[751:744], rTempLshInput[759:752], rTempLshInput[767:760]} ),
        .iM7({rTempLshInput[775:768], rTempLshInput[783:776], rTempLshInput[791:784], rTempLshInput[799:792]} ),
        .iM6({rTempLshInput[807:800], rTempLshInput[815:808], rTempLshInput[823:816], rTempLshInput[831:824]} ),
        .iM5({rTempLshInput[839:832], rTempLshInput[847:840], rTempLshInput[855:848], rTempLshInput[863:856]} ),
        .iM4({rTempLshInput[871:864], rTempLshInput[879:872], rTempLshInput[887:880], rTempLshInput[895:888]} ),
        .iM3({rTempLshInput[903:896], rTempLshInput[911:904], rTempLshInput[919:912], rTempLshInput[927:920]} ),
        .iM2({rTempLshInput[935:928], rTempLshInput[943:936], rTempLshInput[951:944], rTempLshInput[959:952]} ),
        .iM1({rTempLshInput[967:960], rTempLshInput[975:968], rTempLshInput[983:976], rTempLshInput[991:984]} ),
        .iM0({rTempLshInput[999:992], rTempLshInput[1007:1000], rTempLshInput[1015:1008], rTempLshInput[1023:1016]}), 
		
        .oHashValue(wDigest),
        .oDone(wDigestValid)					
);								
																						
lshDrbgRN_256_N genRandomNo( //  V: StateValue V, E: Entropy, A: Additional Input
	.iClk(iClk),
	.iReset_n(iReset_n),
	.iEnable(rEnable_randomNoGen),
	.iStateV(rStateV), 
	.iRN256_CNT(12'h800),  //0x154 = 340
	//.iRN256_CNT(12'h2AA),  //0x400 = 1024, //0x800 = 2048
	//.iRN256_CNT(12'h2), //TTA TEST MODE

	.oRN256(wTempRN256),
	.oRN256_Valid(wRandomNoGen_valid),
	.oRN256N_Complete(wRN_COmplete)
												
);	
                                                      			
endmodule // drbgCore