//======================================================================
// Title: lshDrbgInit_iE.v
// ---------------------------------------------------------------------
// Description	: Intialization Function of StateV && StateC
// Author		: EJ KIM
// Date			: 6th December, 2018
//======================================================================		
module lshDrbgInit_iE(
										input wire iClk,
										input wire iReset_n,
										input wire iEnable,
										input wire [983:0] inData, 
//										input wire [255:0] iEntropy,
//										input wire [127:0] inNonce,
//										input wire [255:0] inPerString,
										output wire [439:0] oStateV,
										output wire [439:0] oStateC,
										output wire oInitComplete
						);
						
parameter       CTRL_IDLE               = 0;
parameter       CTRL_STATE_V_TRIG       = 1;
parameter       CTRL_STATE_V            = 2;
parameter       CTRL_STATE_C_TRIG       = 3;
parameter       CTRL_STATE_C            = 4;
parameter       CTRL_FINISH             = 5;

////////////////////////////////////////////////////////////////////////////////////////////
//delay  Valid Signal with 1/2 Clk
////////////////////////////////////////////////////////////////////////////////////////////
    parameter STATE_DELAY_IDLE       = 0;
    parameter STATE_DELAY_1CLK       = 1;
    parameter STATE_DELAY_2CLK       = 2;
    parameter STATE_DELAY_3CLK       = 3;
    
     reg rTEmpReStateValid;
     reg [3:0] rState_enable;
 ////////////////////////////////////////////////////////////////////////////////////////////

reg [983:0] rTempDf_Input;
reg [439:0] rTempStateV;
reg [439:0] rTempStateC;

reg [3:0]   rState; 
reg rTempDf_En;
reg rTempDf_finish;

wire wTempDf_Complete;
wire [439:0] wTempSeed;

assign oStateV = rTempStateV;
assign oStateC = rTempStateC;

//assign oInitComplete = rTempDf_finish;
assign oInitComplete = rTEmpReStateValid;

////////////////////////////////////////////////////////////////////////////////////////////
//1/2 clock 만큼 vaild 신호 밀기 && width = 2clk!!
////////////////////////////////////////////////////////////////////////////////////////////
always @(posedge iClk or negedge iReset_n) begin
  if(!iReset_n) begin
      rState_enable <=STATE_DELAY_IDLE;
      rTEmpReStateValid <=1'b0;
  end // if(!iReset_n) begin
  else begin //~if(!iReset_n) begin
     case(rState_enable)
         STATE_DELAY_IDLE: begin
             if(rTempDf_finish) begin
                rState_enable              <=STATE_DELAY_1CLK;
                rTEmpReStateValid   <= 1'b1;
              end // if(digestValid_1) begin
              else begin // ~ if(iEnable) begin
                rState_enable              <=STATE_DELAY_IDLE;
                rTEmpReStateValid   <=1'b0;
              end // ~ if(digestValid_1) begin
          end //STATE_DELAY_IDLE: begin
          STATE_DELAY_1CLK: begin 
                rState_enable              <=STATE_DELAY_IDLE;
                rTEmpReStateValid   <=1'b1;
          end //STATE_DELAY_1CLK: begin 
//                  STATE_DELAY_2CLK: begin 
//                       rState_enable              <=STATE_IDLE;
//                       rTEmpReStateValid   <=1'b1;
//                  end //STATE_DELAY_2CLK: begin 
         default: begin 
            rState_enable              <=STATE_DELAY_IDLE;
            rTEmpReStateValid   <=1'b0;
         end             
     endcase //  case(rState)
  end // /~/if(!iReset_n) begin
end //always @(posedge iClk or negedge iReset_n) begin
////////////////////////////////////////////////////////////////////////////////////////////

always @(posedge iClk or negedge iReset_n) begin
			if(!iReset_n) begin
				rTempDf_Input <= 984'h0;
				rTempStateV <= 440'h0;
				rTempStateC <= 440'h0;
				rTempDf_En  <= 1'b0;
				rTempDf_finish <= 1'b0;
				rState      <= CTRL_IDLE;
			end // if(!iReset_n) begin
			else begin //if(!iReset_n) begin
				case(rState)
					CTRL_IDLE: begin
					    if(iEnable) begin
//							rTempDf_Input <= {iEntropy[255:0],inNonce[127:0], inPerString[255:0], 8'h80, 336'h0}  ; //984 =  256+ 128 + 256 + 8+ 336
                            rTempDf_Input <= inData;
                            rTempStateV <= rTempStateV;
                            rTempStateC <= rTempStateC;
                            rTempDf_En  <=rTempDf_En;
                            rTempDf_finish <= 1'b0;
                            rState      <= CTRL_STATE_V_TRIG;
                        end //if(iEnable) begin
                        else begin // ~ if(iEnable) begin
                            rTempDf_Input <= rTempDf_Input;
                            rTempStateV <= rTempStateV;
                            rTempStateC <= rTempStateC;
                            rTempDf_En  <= rTempDf_En;
                            rTempDf_finish <= 1'b0;
                            rState      <= CTRL_IDLE;
                        end // ~if(iEnable) begin
                   end //CTRL_IDLE: begin
                   CTRL_STATE_V_TRIG: begin 
                       rTempDf_Input <= rTempDf_Input;
                       rTempStateV <= rTempStateV;
                       rTempStateC <= rTempStateC;
                       rTempDf_En  <= 1'b1;
                       rTempDf_finish <= 1'b0;
                       rState      <= CTRL_STATE_V;                  
                   end //CTRL_STATE_V_TRIG: begin 
                   CTRL_STATE_V: begin 
                         if(wTempDf_Complete) begin
                               rTempDf_Input <={8'h00, wTempSeed, 8'h80,  528'h0}; //984 = 8 + 440 + 8 +  528
                               rTempStateV <= wTempSeed;
                               rTempStateC <= rTempStateC;
                               rTempDf_En  <= 1'b0;
                               rTempDf_finish <= 1'b0;
                               rState      <= CTRL_STATE_C_TRIG;
                           end //if(iEnable) begin
                           else begin // ~ if(iEnable) begin
                               rTempDf_Input <=rTempDf_Input; //984 =  256+ 128 + 256 + 8+ 336
                               rTempStateV <= rTempStateV;
                               rTempStateC <= rTempStateC;
                               rTempDf_En  <= 1'b0;
                               rTempDf_finish <= 1'b0;
                               rState      <= CTRL_STATE_V;
                           end // ~if(iEnable) begin                      
                   end //CTRL_STATE_V: begin
                   CTRL_STATE_C_TRIG: begin 
                      rTempDf_Input <= rTempDf_Input;
                      rTempStateV <= rTempStateV;
                      rTempStateC <= rTempStateC;
                      rTempDf_En  <= 1'b1;
                      rTempDf_finish <= 1'b0;
                      rState      <= CTRL_STATE_C;  
                      end //CTRL_STATE_V: begin  
                   CTRL_STATE_C: begin 
                       if(wTempDf_Complete) begin
                             rTempDf_Input <= rTempDf_Input;
                             rTempStateV   <= rTempStateV;
                             rTempStateC   <= wTempSeed;
                             rTempDf_En    <= 1'b0;
                             rTempDf_finish <= 1'b0;
                             rState        <= CTRL_FINISH;
                         end //if(iEnable) begin
                         else begin // ~ if(iEnable) begin
                             rTempDf_Input <=rTempDf_Input; //984 =  256+ 128 + 256 + 8+ 336
                             rTempStateV <= rTempStateV;
                             rTempStateC <= rTempStateC;
                             rTempDf_En  <= 1'b0;
                             rTempDf_finish <= 1'b0;
                             rState      <= CTRL_STATE_C;
                         end // ~if(iEnable) begin                              
                  end //CTRL_STATE_C 
                  CTRL_FINISH: begin 
                     rTempDf_Input <= rTempDf_Input;
                     rTempStateV <= rTempStateV;
                     rTempStateC <= rTempStateC;
                     rTempDf_En  <= 1'b0;
                     rTempDf_finish <= 1'b1;
                     rState      <= CTRL_IDLE;                  
                 end //CTRL_FINISH: begin 
                 default: begin 
                      rTempDf_Input <= rTempDf_Input;
                      rTempStateV <= rTempStateV;
                      rTempStateC <= rTempStateC;
                      rTempDf_En  <= rTempDf_En;
                      rTempDf_finish <= rTempDf_finish;
                      rState      <= CTRL_IDLE; 
                 end //default: begin
             endcase //case(ctrlState)
          end //else begin //if(!iReset_n) begin
      end //always @(posedge iClk or negedge iReset_n) begin

//----------------------------------------------------------------
 // Concurrent connectivity for ports etc.
//----------------------------------------------------------------


 lsh256Df_i1024  df1 ( //E: Entropy, N: once, P: Personalizer
								 .dfClk(iClk),
                                 .dfReset_n(iReset_n),
                                 .dfEnable(rTempDf_En),
								 .inData(rTempDf_Input), 
								 .outSeed(wTempSeed),
								 .seedValid(wTempDf_Complete)
						);

endmodule
