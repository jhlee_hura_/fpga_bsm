//==================================================================
// Company       : NSRI
// Engineer      : Kim Eun Ji
// Updated Date  : 2018.12.19
// 
// Design Name   : Messeage Expension Module for Compression Function of
//						 LSH_256(8*w)_256, Word= 32bit, Output = 256bit
// Module Name   : MsgExp.V
// Project Name  : QRNG
//
// Target Devices: Cyclone V SOC (5CSXFC6D6F31C8)
// Tool versions : Quartus Prime 16.1.0 Build 196  10/24/2016
// Revision      : 1.0.0 
//------------------------------------------------------------------
// Notice        : 
//==================================================================

module MsgExp_jth //j th Message expended by M_i, J = [2: 28]
(
//	input		wire iRset_n,
//	input 	wire iClk,
//	input		wire iReq,
	input		wire [31:0] iM_1_0, iM_1_1, iM_1_2, iM_1_3, iM_1_4, iM_1_5, iM_1_6, iM_1_7, iM_1_8, iM_1_9, iM_1_10, iM_1_11, iM_1_12, iM_1_13, iM_1_14, iM_1_15, 
	input		wire [31:0] iM_2_0, iM_2_1, iM_2_2, iM_2_3, iM_2_4, iM_2_5, iM_2_6, iM_2_7, iM_2_8, iM_2_9, iM_2_10, iM_2_11, iM_2_12, iM_2_13, iM_2_14, iM_2_15, 
	

	output 	wire [31:0] oM_0, oM_1, oM_2, oM_3, oM_4, oM_5, oM_6, oM_7, oM_8, oM_9, oM_10, oM_11, oM_12, oM_13, oM_14, oM_15
	//output	wire oDone
);

//========================================================================	
// Parameter Declaration
//========================================================================

//	parameter [7:0] Ns_28	=	8'h1C; //Ns = 28 = 0x1C
	parameter [3:0] COMPLETE_CNT	= 4'h0;
	parameter [1:0] S_IDLE 			= 2'b00;
	parameter [1:0] S_DELAY			= 2'b01;
	parameter [1:0] S_DONE 			= 2'b10;


	
//========================================================================	
// Internal Signals
//========================================================================

	// Register	
	reg	[1:0]		rState;
	reg				rTempDone;
	reg	[3:0]		rCnt;
	
	
//========================================================================
//  Output Assignment
//========================================================================
	
	assign oM_0		= iM_1_0  + iM_2_3;
	assign oM_1		= iM_1_1  + iM_2_2;
	assign oM_2		= iM_1_2  + iM_2_0;
	assign oM_3		= iM_1_3  + iM_2_1;
	assign oM_4		= iM_1_4  + iM_2_7;
	assign oM_5		= iM_1_5  + iM_2_4;
	assign oM_6		= iM_1_6  + iM_2_5;
	assign oM_7		= iM_1_7  + iM_2_6;
	assign oM_8		= iM_1_8  + iM_2_11;
	assign oM_9		= iM_1_9  + iM_2_10;
	assign oM_10	= iM_1_10 + iM_2_8;
	assign oM_11	= iM_1_11 + iM_2_9;
	assign oM_12	= iM_1_12 + iM_2_15;
	assign oM_13	= iM_1_13 + iM_2_12;
	assign oM_14	= iM_1_14 + iM_2_13;
	assign oM_15	= iM_1_15 + iM_2_14;
	
	
//	assign oDone	= rTempDone;
	

////========================================================================	
//// Initial Setting
////========================================================================
//	initial
//	begin
	
//		rTempDone	<= 1'b0;
//		rCnt			<= 4'h0;
//		rState		<= S_IDLE;
		
//	end
////========================================================================	
//// STM
////========================================================================	
//	always @(negedge iRset_n or posedge iClk) begin
//		if(!iRset_n)	begin
//		rTempDone	<= 1'b0;
//		rCnt			<= 4'h0;
//		rState		<= S_IDLE;
		
//		end //if(!iRset_n)	begin
//		else 	begin  //  ~ if (!iRset_n)
//			case (rState)
//				S_IDLE: begin
//					if(iReq) begin			//S_INIT		
//						rTempDone	<= 1'b0;
//						rCnt			<= 4'h0;
//						rState		<= S_DELAY;
//					end //if(iReq) begin
//					else begin 
//						rTempDone	<= 1'b0;
//						rCnt			<= 4'h0;
//						rState		<= S_IDLE;
//					end // ~if(iReq) begin
//				end //S_IDLE: begin

//				S_DELAY: begin
//					if(rCnt == COMPLETE_CNT) begin
//						rTempDone	<= 1'b1;
//						rCnt			<= 4'h0;
//						rState		<= S_DONE;				
//					end //if(rCnt == COMPLETE_CNT) begin
//					else begin
//						rTempDone	<= 1'b0;
//						rCnt			<= rCnt + 1'b1;
//						rState		<= S_DELAY;	
//					end // !if(rCnt == COMPLETE_CNT) begin				
//				end //S_DELAY: begin
//				S_DONE: begin
//					rTempDone	<= 1'b0;
//					rCnt			<= rCnt;
//					rState		<= S_IDLE;
//				end //S_DONE: begin
//				default: begin
//					rTempDone	<= 1'b0;
//					rCnt			<= 4'h0;
//					rState		<= S_IDLE;
//				end //default: begin
//			endcase 
//		end //begin  //  ~ if (!iRset_n)
//	end // always @(negedge iRset_n or posedge iClk)
		
						
endmodule
