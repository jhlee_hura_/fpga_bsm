//==================================================================
// Company       : NSRI
// Engineer      : Kim Eun Ji
// Updated Date  : 2018.12.19
// 
// Design Name   : Update the SCj for Compression Function of
//						 LSH_256(8*w)_256, Word= 32bit, Output = 256bit
// Module Name   : scUpdate.V
// Project Name  : QRNG
//
// Target Devices: Cyclone V SOC (5CSXFC6D6F31C8)
// Tool versions : Quartus Prime 16.1.0 Build 196  10/28/2016
// Revision      : 1.0.0 
//------------------------------------------------------------------
// Notice        : 
//==================================================================

module SCUpdate//j th Message expended by M_i, J = [2: 28]
(
//	input		wire iRset_n,
//	input 	wire iClk,
//	input		wire iReq,
	input		wire [31:0] iSC0,iSC1,iSC2, iSC3, iSC4, iSC5, iSC6, iSC7,
	output	wire [31:0] oSC0,oSC1, oSC2, oSC3, oSC4, oSC5, oSC6, oSC7
//	output	wire oDone
);

//========================================================================	
// Parameter Declaration
//========================================================================

//	parameter [7:0] Ns_28	=	8'h1C; //Ns = 28 = 0x1C
	parameter [3:0] COMPLETE_CNT	= 4'h0;
	parameter [1:0] S_IDLE 			= 2'b00;
	parameter [1:0] S_DELAY			= 2'b01;
	parameter [1:0] S_DONE 			= 2'b10;


	
//========================================================================	
// Internal Signals
//========================================================================

	// Register	
	reg	[1:0]		rState;
	reg				rTempDone;
	reg	[3:0]		rCnt;
	//reg	[63:0] 	rSC0, rSC1, rSC2, rSC3, rSC4, rSC5, rSC6, rSC7;
	
//========================================================================
//  Output Assignment
//========================================================================
	assign oSC0 = iSC0 + { iSC0[23:0], iSC0[31:24]};
	assign oSC1 = iSC1 + { iSC1[23:0], iSC1[31:24]};
	assign oSC2 = iSC2 + { iSC2[23:0], iSC2[31:24]};
	assign oSC3 = iSC3 + { iSC3[23:0], iSC3[31:24]};
	assign oSC4 = iSC4 + { iSC4[23:0], iSC4[31:24]};
	assign oSC5 = iSC5 + { iSC5[23:0], iSC5[31:24]};
	assign oSC6 = iSC6 + { iSC6[23:0], iSC6[31:24]};
	assign oSC7 = iSC7 + { iSC7[23:0], iSC7[31:24]};
	
	
//	assign oDone	= rTempDone;
	

//========================================================================	
// Initial Setting
//========================================================================
//	initial
//	begin
	
//		rTempDone	<= 1'b0;
//		rCnt			<= 4'h0;
//		rSC0			<= 64'h0;
//		rSC1			<= 64'h0;
//		rSC2			<= 64'h0;
//		rSC3			<= 64'h0;
//		rSC4			<= 64'h0;
//		rSC5			<= 64'h0;
//		rSC6			<= 64'h0;
//		rSC7			<= 64'h0;
//		rState		<= S_IDLE;
		
//	end
////========================================================================	
//// STM
////========================================================================	
//	always @(negedge iRset_n or posedge iClk) begin
//		if(!iRset_n)	begin
//		rTempDone	<= 1'b0;
//		rCnt			<= 4'h0;
//		rSC0			<= 64'h0;
//		rSC1			<= 64'h0;
//		rSC2			<= 64'h0;
//		rSC3			<= 64'h0;
//		rSC4			<= 64'h0;
//		rSC5			<= 64'h0;
//		rSC6			<= 64'h0;
//		rSC7			<= 64'h0;
//		rState		<= S_IDLE;
		
//		end //if(!iRset_n)	begin
//		else 	begin  //  ~ if (!iRset_n)
//			case (rState)
//				S_IDLE: begin
//					if(iReq) begin			//S_INIT		
//						rTempDone	<= 1'b0;
//						rCnt			<= 4'h0;
//						rSC0			<= iSC0;
//						rSC1			<= iSC1;
//						rSC2			<= iSC2;
//						rSC3			<= iSC3;
//						rSC4			<= iSC4;
//						rSC5			<= iSC5;
//						rSC6			<= iSC6;
//						rSC7			<= iSC7;
//						rState		<= S_DELAY;
//					end //if(iReq) begin
//					else begin 
//						rTempDone	<= 1'b0;
//						rCnt			<= 4'h0;
//						rSC0			<= rSC0;
//						rSC1			<= rSC1;
//						rSC2			<= rSC2;
//						rSC3			<= rSC3;
//						rSC4			<= rSC4;
//						rSC5			<= rSC5;
//						rSC6			<= rSC6;
//						rSC7			<= rSC7;
//						rState		<= S_IDLE;
//					end // ~if(iReq) begin
//				end //S_IDLE: begin

//				S_DELAY: begin
//					if(rCnt == COMPLETE_CNT) begin
//						rTempDone	<= 1'b1;
//						rCnt			<= 4'h0;
//						rSC0			<= rSC0;
//						rSC1			<= rSC1;
//						rSC2			<= rSC2;
//						rSC3			<= rSC3;
//						rSC4			<= rSC4;
//						rSC5			<= rSC5;
//						rSC6			<= rSC6;
//						rSC7			<= rSC7;
//						rState		<= S_DONE;				
//					end //if(rCnt == COMPLETE_CNT) begin
//					else begin
//						rTempDone	<= 1'b0;
//						rCnt			<= rCnt + 1'b1;
//						rSC0			<= rSC0;
//						rSC1			<= rSC1;
//						rSC2			<= rSC2;
//						rSC3			<= rSC3;
//						rSC4			<= rSC4;
//						rSC5			<= rSC5;
//						rSC6			<= rSC6;
//						rSC7			<= rSC7;
//						rState		<= S_DELAY;	
//					end // !if(rCnt == COMPLETE_CNT) begin				
//				end //S_DELAY: begin
//				S_DONE: begin
//					rTempDone	<= 1'b0;
//					rCnt			<= rCnt;
//					rSC0			<= rSC0;
//					rSC1			<= rSC1;
//					rSC2			<= rSC2;
//					rSC3			<= rSC3;
//					rSC4			<= rSC4;
//					rSC5			<= rSC5;
//					rSC6			<= rSC6;
//					rSC7			<= rSC7;
//					rState		<= S_IDLE;
//				end //S_DONE: begin
//				default: begin
//					rTempDone	<= 1'b0;
//					rCnt			<= 4'h0;
//					rSC0			<= rSC0;
//					rSC1			<= rSC1;
//					rSC2			<= rSC2;
//					rSC3			<= rSC3;
//					rSC4			<= rSC4;
//					rSC5			<= rSC5;
//					rSC6			<= rSC6;
//					rSC7			<= rSC7;
//					rState		<= S_IDLE;
//				end //default: begin
//			endcase 
//		end //begin  //  ~ if (!iRset_n)
//	end // always @(negedge iRset_n or posedge iClk)
		
						
endmodule
