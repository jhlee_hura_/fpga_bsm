//==================================================================
// Company       : NSRI
// Engineer      : Kim Eun Ji
// Updated Date  : 2018.12.19
// 
// Design Name   : Compression Function Module for
//						 LSH_256(8*w)_256, Word= 32bit, Output = 256bit Input Message 
// Module Name   : CFunc.V
// Project Name  : QRNG
//
// Target Devices: Cyclone V SOC (5CSXFC6D6F31C8)
// Tool versions : Quartus Prime 16.1.0 Build 196  10/24/2016
// Revision      : 1.0.0 
//------------------------------------------------------------------
// Notice        :
//==================================================================

module CFunc
(
	input		wire iRset_n,
	input 	wire iClk,
	input		wire iReq,
	input		wire [31:0] iM0, iM1, iM2, iM3, iM4, iM5, iM6, iM7, iM8, iM9, iM10, iM11, iM12, iM13, iM14, iM15,
	input		wire [31:0] iM16, iM17, iM18, iM19, iM20, iM21, iM22, iM23, iM24, iM25, iM26, iM27, iM28, iM29, iM30, iM31,
	
	input		wire [31:0] iCV0, iCV1, iCV2, iCV3, iCV4, iCV5, iCV6, iCV7, iCV8, iCV9, iCV10, iCV11, iCV12, iCV13, iCV14, iCV15,
		
	output	wire [31:0] oCV0, oCV1, oCV2, oCV3, oCV4, oCV5, oCV6, oCV7, oCV8, oCV9, oCV10, oCV11, oCV12, oCV13, oCV14, oCV15,
//	output	wire [3:0] oState,
	output	wire oDone
);

//========================================================================	
// Parameter Declaration
//========================================================================

	parameter [7:0] Ns_26					= 8'h1A; //Ns = 26 = 0x1A
	parameter [3:0] S_IDLE 					= 4'h0;
	parameter [3:0] S_MSG_EXP				= 4'h1; 
	parameter [3:0] S_MSG_EXP_WAIT		= 4'h2;	// if MagExp_done, go toS_MSG_EXP_WAIT2, if Cnt < 28, go S_STEP_OTHERS, else go to S_STEP_DONE 	
	parameter [3:0] S_STEP_0				= 4'h3;	// Start Step FUnction 
	parameter [3:0] S_STEP_0_WAIT			= 4'h4;	// if finished, Cnt++ and go to next MSG_EXP
	parameter [3:0] S_STEP_1				= 4'h5;	// Start Step FUnction 
	parameter [3:0] S_STEP_1_WAIT			= 4'h6;	// if finished, Cnt++ and go to next MSG_EXP
	parameter [3:0] S_STEP_OTHERS			= 4'h7;	// Start Step FUnction
	parameter [3:0] S_STEP_OTHERS_WAIT	= 4'h8;	// Cnt++ before moving to nest MSG_EXP
	parameter [3:0] S_STEP_DONE			= 4'h9;
	parameter [3:0] S_CFUNC_DONE			= 4'hA;

	
//========================================================================	
// Internal Signals
//========================================================================

	
	wire	[31:0] wTempT_0, wTempT_1, wTempT_2, wTempT_3, wTempT_4, wTempT_5, wTempT_6, wTempT_7;
	wire	[31:0] wTempT_8, wTempT_9, wTempT_10, wTempT_11, wTempT_12, wTempT_13, wTempT_14, wTempT_15;
	
	wire	[31:0] wSC0,wSC1, wSC2, wSC3, wSC4, wSC5, wSC6, wSC7; 
	wire	[31:0] wMj_0, wMj_1, wMj_2, wMj_3, wMj_4, wMj_5, wMj_6, wMj_7, wMj_8, wMj_9, wMj_10, wMj_11, wMj_12, wMj_13, wMj_14, wMj_15; 
	
	wire			 wMsgExp_done;
	wire			 wStepFnc_done;
	wire			 wSCUpdate_done;
	wire			 wJ_odd;
	//registers
	
	
	reg	[31:0] rM1_0, rM1_1, rM1_2, rM1_3, rM1_4, rM1_5, rM1_6, rM1_7, rM1_8, rM1_9, rM1_10, rM1_11, rM1_12, rM1_13, rM1_14, rM1_15;
	reg	[31:0] rM2_0, rM2_1, rM2_2, rM2_3, rM2_4, rM2_5, rM2_6, rM2_7, rM2_8, rM2_9, rM2_10, rM2_11, rM2_12, rM2_13, rM2_14, rM2_15;
	
	reg	[31:0] rMj_0, rMj_1, rMj_2, rMj_3, rMj_4, rMj_5, rMj_6, rMj_7, rMj_8, rMj_9, rMj_10, rMj_11, rMj_12, rMj_13, rMj_14, rMj_15; 
	
	reg	[31:0] rSC0,rSC1, rSC2, rSC3, rSC4, rSC5, rSC6, rSC7; 
	reg	[31:0] rTempT_0, rTempT_1, rTempT_2, rTempT_3, rTempT_4, rTempT_5, rTempT_6, rTempT_7;
	reg	[31:0] rTempT_8, rTempT_9, rTempT_10, rTempT_11, rTempT_12, rTempT_13, rTempT_14, rTempT_15;
	
	reg	[3:0]		rState;
	reg				rTempDone;
	reg	[7:0]		rCnt;
//	reg				rMsgExp_req;
	reg				rStepFnc_req;
//	reg				rSCUpdate_req;

reg [7:0] rTempStepFuncCnt;
reg	  rTempDone2;

//========================================================================
//  Output Assignment
//========================================================================
	
	assign oCV0		= wTempT_0 ^ wMj_0;
	assign oCV1		= wTempT_1 ^ wMj_1;
	assign oCV2		= wTempT_2 ^ wMj_2;
	assign oCV3		= wTempT_3 ^ wMj_3;
	assign oCV4		= wTempT_4 ^ wMj_4;
	assign oCV5		= wTempT_5 ^ wMj_5;
	assign oCV6		= wTempT_6 ^ wMj_6;
	assign oCV7		= wTempT_7 ^ wMj_7;
	assign oCV8		= wTempT_8 ^ wMj_8;
	assign oCV9		= wTempT_9 ^ wMj_9;
	assign oCV10	= wTempT_10 ^ wMj_10;
	assign oCV11	= wTempT_11 ^ wMj_11;
	assign oCV12	= wTempT_12 ^ wMj_12;
	assign oCV13	= wTempT_13 ^ wMj_13;
	assign oCV14	= wTempT_14 ^ wMj_14;
	assign oCV15	= wTempT_15 ^ wMj_15;
	
	//assign oDone	= rTempDone;
assign oDone	= rTempDone2 & rStepFnc_req;
//	assign oState	= rState;
	
//========================================================================
//  Inner Assignment
//========================================================================

	assign wJ_odd = rCnt[0];
	

//Clk count

always @(posedge iReq or posedge rStepFnc_req) begin
	if(iReq) begin 
		rTempStepFuncCnt <= 8'h0;
		rTempDone2 <=1'b0;
	end 
	else begin 
		
		if(rTempStepFuncCnt  >= 8'h19) begin
			rTempDone2 <=1'b1;
			rTempStepFuncCnt <= 8'h0;
		end
		else begin
			rTempDone2 <=1'b0;
			rTempStepFuncCnt <= rTempStepFuncCnt + 1'b1;
		end
	end //else

end // always 

//========================================================================
//  External Modules: U1
//========================================================================
		
	MsgExp_jth U1 //j th Message expended by M_i, J = [2: 28]
(
//	.iRset_n(iRset_n),
//	.iClk(~iClk),
//	.iReq(rMsgExp_req),
	.iM_1_0(rM1_0), .iM_1_1(rM1_1), .iM_1_2(rM1_2), .iM_1_3(rM1_3), .iM_1_4(rM1_4), .iM_1_5(rM1_5), .iM_1_6(rM1_6), .iM_1_7(rM1_7), 
	.iM_1_8(rM1_8), .iM_1_9(rM1_9), .iM_1_10(rM1_10), .iM_1_11(rM1_11), .iM_1_12(rM1_12), .iM_1_13(rM1_13), .iM_1_14(rM1_14), .iM_1_15(rM1_15),
	
	.iM_2_0(rM2_0), .iM_2_1(rM2_1), .iM_2_2(rM2_2), .iM_2_3(rM2_3), .iM_2_4(rM2_4), .iM_2_5(rM2_5), .iM_2_6(rM2_6), .iM_2_7(rM2_7), 
	.iM_2_8(rM2_8), .iM_2_9(rM2_9), .iM_2_10(rM2_10), .iM_2_11(rM2_11), .iM_2_12(rM2_12), .iM_2_13(rM2_13), .iM_2_14(rM2_14), .iM_2_15(rM2_15),
	
	
	.oM_0(wMj_0), .oM_1(wMj_1), .oM_2(wMj_2), .oM_3(wMj_3), .oM_4(wMj_4), .oM_5(wMj_5), .oM_6(wMj_6), .oM_7(wMj_7), 
	.oM_8(wMj_8), .oM_9(wMj_9), .oM_10(wMj_10), .oM_11(wMj_11), .oM_12(wMj_12), .oM_13(wMj_13), .oM_14(wMj_14), .oM_15(wMj_15)
//	.oDone(wMsgExp_done)
);

//========================================================================
//  External Modules: U2
//========================================================================
 StepFnc_cv U2
(
	.iRset_n(iRset_n),
	.iClk(iClk),
	.iReq(rStepFnc_req),
	.iM_j_odd(wJ_odd), //if j is odd, iM_j_odd = 1
	.iM0(rMj_0), .iM1(rMj_1), .iM2(rMj_2), .iM3(rMj_3), .iM4(rMj_4), .iM5(rMj_5), .iM6(rMj_6), .iM7(rMj_7), 
	.iM8(rMj_8), .iM9(rMj_9), .iM10(rMj_10), .iM11(rMj_11), .iM12(rMj_12), .iM13(rMj_13), .iM14(rMj_14), .iM15(rMj_15),
	
	.iT0(rTempT_0), .iT1(rTempT_1), .iT2(rTempT_2), .iT3(rTempT_3), .iT4(rTempT_4), .iT5(rTempT_5), .iT6(rTempT_6), .iT7(rTempT_7), 
	.iT8(rTempT_8), .iT9(rTempT_9), .iT10(rTempT_10), .iT11(rTempT_11), .iT12(rTempT_12), .iT13(rTempT_13), .iT14(rTempT_14), .iT15(rTempT_15),
	.iSC0(rSC0), .iSC1(rSC1), .iSC2(rSC2), .iSC3(rSC3), .iSC4(rSC4), .iSC5(rSC5), .iSC6(rSC6), .iSC7(rSC7),	
	
	.oT0(wTempT_0), .oT1(wTempT_1), .oT2(wTempT_2), .oT3(wTempT_3), .oT4(wTempT_4), .oT5(wTempT_5), .oT6(wTempT_6), .oT7(wTempT_7), 
	.oT8(wTempT_8), .oT9(wTempT_9), .oT10(wTempT_10), .oT11(wTempT_11), .oT12(wTempT_12), .oT13(wTempT_13), .oT14(wTempT_14), .oT15(wTempT_15),
	.oDone(wStepFnc_done)
);	

//========================================================================
//  External Modules: U3
//========================================================================
SCUpdate U3
(
//		.iRset_n(iRset_n),
//		.iClk(~iClk),
//		.iReq(rSCUpdate_req),
	
		.iSC0(rSC0), .iSC1(rSC1), .iSC2(rSC2), .iSC3(rSC3), .iSC4(rSC4), .iSC5(rSC5), .iSC6(rSC6), .iSC7(rSC7),
		.oSC0(wSC0), .oSC1(wSC1), .oSC2(wSC2), .oSC3(wSC3), .oSC4(wSC4), .oSC5(wSC5), .oSC6(wSC6), .oSC7(wSC7)
//		.oDone(wSCUpdate_done)
);
//========================================================================	
// Initial Setting
//========================================================================
	initial	begin
		rM1_0 <= 32'h0;
		rM1_1 <= 32'h0; 
		rM1_2 <= 32'h0;  
		rM1_3 <= 32'h0; 
		rM1_4 <= 32'h0; 
		rM1_5 <= 32'h0;
		rM1_6 <= 32'h0; 
		rM1_7 <= 32'h0; 
		rM1_8 <= 32'h0; 
		rM1_9 <= 32'h0; 
		rM1_10 <= 32'h0; 
		rM1_11 <= 32'h0; 
		rM1_12 <= 32'h0; 
		rM1_13 <= 32'h0; 
		rM1_14 <= 32'h0; 
		rM1_15 <= 32'h0;
		
		rM2_0 <= 32'h0; 
		rM2_1 <= 32'h0; 
		rM2_2 <= 32'h0; 
		rM2_3 <= 32'h0; 
		rM2_4 <= 32'h0; 
		rM2_5 <= 32'h0; 
		rM2_6 <= 32'h0; 
		rM2_7 <= 32'h0; 
		rM2_8 <= 32'h0; 
		rM2_9 <= 32'h0; 
		rM2_10 <= 32'h0; 
		rM2_11 <= 32'h0; 
		rM2_12 <= 32'h0; 
		rM2_13 <= 32'h0; 
		rM2_14 <= 32'h0; 
		rM2_15 <= 32'h0;
		
		rSC0	<=	32'h0;
		rSC1	<=	32'h0;
		rSC2	<=	32'h0;
		rSC3	<=	32'h0;
		rSC4	<=	32'h0;
		rSC5	<=	32'h0;
		rSC6	<=	32'h0;
		rSC7	<=	32'h0;
		
		rMj_0 <= 32'h0;
		rMj_1 <= 32'h0;
		rMj_2 <= 32'h0;
		rMj_3 <= 32'h0;
		rMj_4 <= 32'h0;
		rMj_5 <= 32'h0;
		rMj_6 <= 32'h0;
		rMj_7 <= 32'h0;
		rMj_8 <= 32'h0;
		rMj_9 <= 32'h0;
		rMj_10 <= 32'h0;
		rMj_11 <= 32'h0;
		rMj_12 <= 32'h0;
		rMj_13 <= 32'h0;
		rMj_14 <= 32'h0;
		rMj_15 <= 32'h0;
	
		rTempT_0 <= 32'h0; 
		rTempT_1 <= 32'h0; 
		rTempT_2 <= 32'h0; 
		rTempT_3 <= 32'h0; 
		rTempT_4 <= 32'h0; 
		rTempT_5 <= 32'h0; 
		rTempT_6 <= 32'h0; 
		rTempT_7 <= 32'h0;
		rTempT_8 <= 32'h0; 
		rTempT_9 <= 32'h0;
		rTempT_10 <= 32'h0;
		rTempT_11 <= 32'h0;
		rTempT_12 <= 32'h0;
		rTempT_13 <= 32'h0; 
		rTempT_14 <= 32'h0; 
		rTempT_15 <= 32'h0;
	
		rState			<= S_IDLE;
		rTempDone 		<= 1'b0;
		rCnt				<= 8'h00;
	//	rMsgExp_req 	<= 1'b0;
	//	rSCUpdate_req	<= 1'b0;
		rStepFnc_req 	<= 1'b0;
	end //initial
	
	always @(negedge iRset_n or posedge iClk) begin
		if(!iRset_n)	begin
			rM1_0 <= 32'h0;
			rM1_1 <= 32'h0; 
			rM1_2 <= 32'h0;  
			rM1_3 <= 32'h0; 
			rM1_4 <= 32'h0; 
			rM1_5 <= 32'h0;
			rM1_6 <= 32'h0; 
			rM1_7 <= 32'h0; 
			rM1_8 <= 32'h0; 
			rM1_9 <= 32'h0; 
			rM1_10 <= 32'h0; 
			rM1_11 <= 32'h0; 
			rM1_12 <= 32'h0; 
			rM1_13 <= 32'h0; 
			rM1_14 <= 32'h0; 
			rM1_15 <= 32'h0;
			
			rM2_0 <= 32'h0; 
			rM2_1 <= 32'h0; 
			rM2_2 <= 32'h0; 
			rM2_3 <= 32'h0; 
			rM2_4 <= 32'h0; 
			rM2_5 <= 32'h0; 
			rM2_6 <= 32'h0; 
			rM2_7 <= 32'h0; 
			rM2_8 <= 32'h0; 
			rM2_9 <= 32'h0; 
			rM2_10 <= 32'h0; 
			rM2_11 <= 32'h0; 
			rM2_12 <= 32'h0; 
			rM2_13 <= 32'h0; 
			rM2_14 <= 32'h0; 
			rM2_15 <= 32'h0;
			
			rSC0	<=	32'h0;
			rSC1	<=	32'h0;
			rSC2	<=	32'h0;
			rSC3	<=	32'h0;
			rSC4	<=	32'h0;
			rSC5	<=	32'h0;
			rSC6	<=	32'h0;
			rSC7	<=	32'h0;
			
			rMj_0 <= 32'h0;
			rMj_1 <= 32'h0;
			rMj_2 <= 32'h0;
			rMj_3 <= 32'h0;
			rMj_4 <= 32'h0;
			rMj_5 <= 32'h0;
			rMj_6 <= 32'h0;
			rMj_7 <= 32'h0;
			rMj_8 <= 32'h0;
			rMj_9 <= 32'h0;
			rMj_10 <= 32'h0;
			rMj_11 <= 32'h0;
			rMj_12 <= 32'h0;
			rMj_13 <= 32'h0;
			rMj_14 <= 32'h0;
			rMj_15 <= 32'h0;
		
			rTempT_0 <= 32'h0; 
			rTempT_1 <= 32'h0; 
			rTempT_2 <= 32'h0; 
			rTempT_3 <= 32'h0; 
			rTempT_4 <= 32'h0; 
			rTempT_5 <= 32'h0; 
			rTempT_6 <= 32'h0; 
			rTempT_7 <= 32'h0;
			rTempT_8 <= 32'h0; 
			rTempT_9 <= 32'h0;
			rTempT_10 <= 32'h0;
			rTempT_11 <= 32'h0;
			rTempT_12 <= 32'h0;
			rTempT_13 <= 32'h0; 
			rTempT_14 <= 32'h0; 
			rTempT_15 <= 32'h0;
		
			rState			<= S_IDLE;
			rTempDone 		<= 1'b0;
			rCnt				<= 8'h00;
	//		rSCUpdate_req	<= 1'b0;
	//	rMsgExp_req 	<= 1'b0;
			rStepFnc_req 	<= 1'b0;	
		end //if(!iRset_n)	begin
		else 	begin  //  ~ if (!iRset_n)
			case (rState)
				S_IDLE: begin
					if(iReq) begin			//S_INIT		
						rM1_0 <= iM16;
						rM1_1 <= iM17; 
						rM1_2 <= iM18;  
						rM1_3 <= iM19; 
						rM1_4 <= iM20; 
						rM1_5 <= iM21;
						rM1_6 <= iM22; 
						rM1_7 <= iM23; 
						rM1_8 <= iM24; 
						rM1_9 <= iM25; 
						rM1_10 <= iM26; 
						rM1_11 <= iM27; 
						rM1_12 <= iM28; 
						rM1_13 <= iM29; 
						rM1_14 <= iM30; 
						rM1_15 <= iM31;
						
						rM2_0 <= iM0; 
						rM2_1 <= iM1; 
						rM2_2 <= iM2; 
						rM2_3 <= iM3; 
						rM2_4 <= iM4; 
						rM2_5 <= iM5; 
						rM2_6 <= iM6; 
						rM2_7 <= iM7; 
						rM2_8 <= iM8; 
						rM2_9 <= iM9; 
						rM2_10 <= iM10; 
						rM2_11 <= iM11; 
						rM2_12 <= iM12; 
						rM2_13 <= iM13; 
						rM2_14 <= iM14; 
						rM2_15 <= iM15;
						
						rSC0	<=	32'h917caf90;
						rSC1	<=	32'h6c1b10a2;
						rSC2	<=	32'h6f352943;
						rSC3	<=	32'hcf778243;
						rSC4	<=	32'h2ceb7472;
						rSC5	<=	32'h29e96ff2;
						rSC6	<=	32'h8a9ba428;
						rSC7	<=	32'h2eeb2642;
						
						rMj_0 <= iM0;
						rMj_1 <= iM1;
						rMj_2 <= iM2;
						rMj_3 <= iM3;
						rMj_4 <= iM4;
						rMj_5 <= iM5;
						rMj_6 <= iM6;
						rMj_7 <= iM7;
						rMj_8 <= iM8;
						rMj_9 <= iM9;
						rMj_10 <= iM10;
						rMj_11 <= iM11;
						rMj_12 <= iM12;
						rMj_13 <= iM13;
						rMj_14 <= iM14;
						rMj_15 <= iM15;
					
						rTempT_0 <= iCV0; 
                        rTempT_1 <= iCV1; 
                        rTempT_2 <= iCV2; 
                        rTempT_3 <= iCV3; 
                        rTempT_4 <= iCV4; 
                        rTempT_5 <= iCV5; 
                        rTempT_6 <= iCV6; 
                        rTempT_7 <= iCV7;
                        rTempT_8 <= iCV8; 
                        rTempT_9 <= iCV9;
                        rTempT_10 <= iCV10;
                        rTempT_11 <= iCV11;
                        rTempT_12 <= iCV12;
                        rTempT_13 <= iCV13; 
                        rTempT_14 <= iCV14; 
                        rTempT_15 <= iCV15;

                            
						rState			<= S_STEP_0;
						rTempDone 		<= 1'b0;
						rCnt				<= 8'h00;
		//				rSCUpdate_req	<= 1'b0;
		//				rMsgExp_req 	<= 1'b0;
						rStepFnc_req 	<= 1'b0;
					end //if(iReq) begin
					else begin 
						rM1_0 <= 32'h0;
						rM1_1 <= 32'h0; 
						rM1_2 <= 32'h0;  
						rM1_3 <= 32'h0; 
						rM1_4 <= 32'h0; 
						rM1_5 <= 32'h0;
						rM1_6 <= 32'h0; 
						rM1_7 <= 32'h0; 
						rM1_8 <= 32'h0; 
						rM1_9 <= 32'h0; 
						rM1_10 <= 32'h0; 
						rM1_11 <= 32'h0; 
						rM1_12 <= 32'h0; 
						rM1_13 <= 32'h0; 
						rM1_14 <= 32'h0; 
						rM1_15 <= 32'h0;
						
						rM2_0 <= 32'h0; 
						rM2_1 <= 32'h0; 
						rM2_2 <= 32'h0; 
						rM2_3 <= 32'h0; 
						rM2_4 <= 32'h0; 
						rM2_5 <= 32'h0; 
						rM2_6 <= 32'h0; 
						rM2_7 <= 32'h0; 
						rM2_8 <= 32'h0; 
						rM2_9 <= 32'h0; 
						rM2_10 <= 32'h0; 
						rM2_11 <= 32'h0; 
						rM2_12 <= 32'h0; 
						rM2_13 <= 32'h0; 
						rM2_14 <= 32'h0; 
						rM2_15 <= 32'h0;
						
						rSC0	<=	32'h0;
						rSC1	<=	32'h0;
						rSC2	<=	32'h0;
						rSC3	<=	32'h0;
						rSC4	<=	32'h0;
						rSC5	<=	32'h0;
						rSC6	<=	32'h0;
						rSC7	<=	32'h0;
						
						rMj_0 <= 32'h0;
						rMj_1 <= 32'h0;
						rMj_2 <= 32'h0;
						rMj_3 <= 32'h0;
						rMj_4 <= 32'h0;
						rMj_5 <= 32'h0;
						rMj_6 <= 32'h0;
						rMj_7 <= 32'h0;
						rMj_8 <= 32'h0;
						rMj_9 <= 32'h0;
						rMj_10 <= 32'h0;
						rMj_11 <= 32'h0;
						rMj_12 <= 32'h0;
						rMj_13 <= 32'h0;
						rMj_14 <= 32'h0;
						rMj_15 <= 32'h0;
					
						rTempT_0 <= 32'h0; 
						rTempT_1 <= 32'h0; 
						rTempT_2 <= 32'h0; 
						rTempT_3 <= 32'h0; 
						rTempT_4 <= 32'h0; 
						rTempT_5 <= 32'h0; 
						rTempT_6 <= 32'h0; 
						rTempT_7 <= 32'h0;
						rTempT_8 <= 32'h0; 
						rTempT_9 <= 32'h0;
						rTempT_10 <= 32'h0;
						rTempT_11 <= 32'h0;
						rTempT_12 <= 32'h0;
						rTempT_13 <= 32'h0; 
						rTempT_14 <= 32'h0; 
						rTempT_15 <= 32'h0;
					
						rState			<= S_IDLE;
						rTempDone 		<= 1'b0;
						rCnt				<= 8'h00;
		//				rSCUpdate_req	<= 1'b0;
		//				rMsgExp_req 	<= 1'b0;
						rStepFnc_req 	<= 1'b0;	
					end // ~if(iReq) begin
				end //S_IDLE: begin
				S_STEP_0 : begin
					rM1_0 <= rM1_0;
					rM1_1 <= rM1_1; 
					rM1_2 <= rM1_2;  
					rM1_3 <= rM1_3; 
					rM1_4 <= rM1_4; 
					rM1_5 <= rM1_5;
					rM1_6 <= rM1_6; 
					rM1_7 <= rM1_7; 
					rM1_8 <= rM1_8; 
					rM1_9 <= rM1_9; 
					rM1_10 <= rM1_10; 
					rM1_11 <= rM1_11; 
					rM1_12 <= rM1_12; 
					rM1_13 <= rM1_13; 
					rM1_14 <= rM1_14; 
					rM1_15 <= rM1_15;
					
					rM2_0 <= rM2_0; 
					rM2_1 <= rM2_1; 
					rM2_2 <= rM2_2; 
					rM2_3 <= rM2_3; 
					rM2_4 <= rM2_4; 
					rM2_5 <= rM2_5; 
					rM2_6 <= rM2_6; 
					rM2_7 <= rM2_7; 
					rM2_8 <= rM2_8; 
					rM2_9 <= rM2_9; 
					rM2_10 <= rM2_10; 
					rM2_11 <= rM2_11; 
					rM2_12 <= rM2_12; 
					rM2_13 <= rM2_13; 
					rM2_14 <= rM2_14; 
					rM2_15 <= rM2_15;
					
					rSC0	<=	rSC0;
					rSC1	<=	rSC1;
					rSC2	<=	rSC2;
					rSC3	<=	rSC3;
					rSC4	<=	rSC4;
					rSC5	<=	rSC5;
					rSC6	<=	rSC6;
					rSC7	<=	rSC7;
					
					rMj_0 <= rMj_0;
					rMj_1 <= rMj_1;
					rMj_2 <= rMj_2;
					rMj_3 <= rMj_3;
					rMj_4 <= rMj_4;
					rMj_5 <= rMj_5;
					rMj_6 <= rMj_6;
					rMj_7 <= rMj_7;
					rMj_8 <= rMj_8;
					rMj_9 <= rMj_9;
					rMj_10 <= rMj_10;
					rMj_11 <= rMj_11;
					rMj_12 <= rMj_12;
					rMj_13 <= rMj_13;
					rMj_14 <= rMj_14;
					rMj_15 <= rMj_15;
				
					rTempT_0 <= rTempT_0; 
					rTempT_1 <= rTempT_1; 
					rTempT_2 <= rTempT_2; 
					rTempT_3 <= rTempT_3; 
					rTempT_4 <= rTempT_4; 
					rTempT_5 <= rTempT_5; 
					rTempT_6 <= rTempT_6; 
					rTempT_7 <= rTempT_7;
					rTempT_8 <= rTempT_8; 
					rTempT_9 <= rTempT_9;
					rTempT_10 <= rTempT_10;
					rTempT_11 <= rTempT_11;
					rTempT_12 <= rTempT_12;
					rTempT_13 <= rTempT_13; 
					rTempT_14 <= rTempT_14; 
					rTempT_15 <= rTempT_15;
				
					rState			<= S_STEP_0_WAIT;
					rTempDone 		<= 1'b0;
					rCnt				<= 8'h00;
	//				rSCUpdate_req	<= 1'b0;
	//				rMsgExp_req 	<= 1'b0;
					rStepFnc_req 	<= 1'b1;			
				end // S_STEP_0
				S_STEP_0_WAIT : begin
					if(wStepFnc_done == 1'b1) begin
						rM1_0 <= rM1_0;
						rM1_1 <= rM1_1; 
						rM1_2 <= rM1_2;  
						rM1_3 <= rM1_3; 
						rM1_4 <= rM1_4; 
						rM1_5 <= rM1_5;
						rM1_6 <= rM1_6; 
						rM1_7 <= rM1_7; 
						rM1_8 <= rM1_8; 
						rM1_9 <= rM1_9; 
						rM1_10 <= rM1_10; 
						rM1_11 <= rM1_11; 
						rM1_12 <= rM1_12; 
						rM1_13 <= rM1_13; 
						rM1_14 <= rM1_14; 
						rM1_15 <= rM1_15;
						
						rM2_0 <= rM2_0; 
						rM2_1 <= rM2_1; 
						rM2_2 <= rM2_2; 
						rM2_3 <= rM2_3; 
						rM2_4 <= rM2_4; 
						rM2_5 <= rM2_5; 
						rM2_6 <= rM2_6; 
						rM2_7 <= rM2_7; 
						rM2_8 <= rM2_8; 
						rM2_9 <= rM2_9; 
						rM2_10 <= rM2_10; 
						rM2_11 <= rM2_11; 
						rM2_12 <= rM2_12; 
						rM2_13 <= rM2_13; 
						rM2_14 <= rM2_14; 
						rM2_15 <= rM2_15;
						
						rSC0	<=	32'h0e2c4021;
						rSC1	<=	32'h872bb30e;
						rSC2	<=	32'ha45e6cb2;
						rSC3	<=	32'h46f9c612;
						rSC4	<=	32'h185fe69e;
						rSC5	<=	32'h1359621b;
						rSC6	<=	32'h263fccb2;
						rSC7	<=	32'h1a116870;
						
						rMj_0 <= rM1_0;
						rMj_1 <= rM1_1;
						rMj_2 <= rM1_2;
						rMj_3 <= rM1_3;
						rMj_4 <= rM1_4;
						rMj_5 <= rM1_5;
						rMj_6 <= rM1_6;
						rMj_7 <= rM1_7;
						rMj_8 <= rM1_8;
						rMj_9 <= rM1_9;
						rMj_10 <= rM1_10;
						rMj_11 <= rM1_11;
						rMj_12 <= rM1_12;
						rMj_13 <= rM1_13;
						rMj_14 <= rM1_14;
						rMj_15 <= rM1_15;
					
						rTempT_0 <= wTempT_0; 
						rTempT_1 <= wTempT_1; 
						rTempT_2 <= wTempT_2; 
						rTempT_3 <= wTempT_3; 
						rTempT_4 <= wTempT_4; 
						rTempT_5 <= wTempT_5; 
						rTempT_6 <= wTempT_6; 
						rTempT_7 <= wTempT_7;
						rTempT_8 <= wTempT_8; 
						rTempT_9 <= wTempT_9;
						rTempT_10 <= wTempT_10;
						rTempT_11 <= wTempT_11;
						rTempT_12 <= wTempT_12;
						rTempT_13 <= wTempT_13; 
						rTempT_14 <= wTempT_14; 
						rTempT_15 <= wTempT_15;
					
						rState			<= S_STEP_1;
						rTempDone 		<= 1'b0;
						rCnt				<= rCnt + 1'b1;
	//					rSCUpdate_req	<= 1'b0;
	//					rMsgExp_req 	<= 1'b0;
						rStepFnc_req 	<= 1'b0;		
					end//if(wStepFnc_done ==1'b1) begin
					else begin//~if(wStepFnc_done ==1'b1) begin
						rM1_0 <= rM1_0;
						rM1_1 <= rM1_1; 
						rM1_2 <= rM1_2;  
						rM1_3 <= rM1_3; 
						rM1_4 <= rM1_4; 
						rM1_5 <= rM1_5;
						rM1_6 <= rM1_6; 
						rM1_7 <= rM1_7; 
						rM1_8 <= rM1_8; 
						rM1_9 <= rM1_9; 
						rM1_10 <= rM1_10; 
						rM1_11 <= rM1_11; 
						rM1_12 <= rM1_12; 
						rM1_13 <= rM1_13; 
						rM1_14 <= rM1_14; 
						rM1_15 <= rM1_15;
						
						rM2_0 <= rM2_0; 
						rM2_1 <= rM2_1; 
						rM2_2 <= rM2_2; 
						rM2_3 <= rM2_3; 
						rM2_4 <= rM2_4; 
						rM2_5 <= rM2_5; 
						rM2_6 <= rM2_6; 
						rM2_7 <= rM2_7; 
						rM2_8 <= rM2_8; 
						rM2_9 <= rM2_9; 
						rM2_10 <= rM2_10; 
						rM2_11 <= rM2_11; 
						rM2_12 <= rM2_12; 
						rM2_13 <= rM2_13; 
						rM2_14 <= rM2_14; 
						rM2_15 <= rM2_15;
						
						rSC0	<=	rSC0;
						rSC1	<=	rSC1;
						rSC2	<=	rSC2;
						rSC3	<=	rSC3;
						rSC4	<=	rSC4;
						rSC5	<=	rSC5;
						rSC6	<=	rSC6;
						rSC7	<=	rSC7;
						
						rMj_0 <= rMj_0;
						rMj_1 <= rMj_1;
						rMj_2 <= rMj_2;
						rMj_3 <= rMj_3;
						rMj_4 <= rMj_4;
						rMj_5 <= rMj_5;
						rMj_6 <= rMj_6;
						rMj_7 <= rMj_7;
						rMj_8 <= rMj_8;
						rMj_9 <= rMj_9;
						rMj_10 <= rMj_10;
						rMj_11 <= rMj_11;
						rMj_12 <= rMj_12;
						rMj_13 <= rMj_13;
						rMj_14 <= rMj_14;
						rMj_15 <= rMj_15;
					
						rTempT_0 <= rTempT_0; 
						rTempT_1 <= rTempT_1; 
						rTempT_2 <= rTempT_2; 
						rTempT_3 <= rTempT_3; 
						rTempT_4 <= rTempT_4; 
						rTempT_5 <= rTempT_5; 
						rTempT_6 <= rTempT_6; 
						rTempT_7 <= rTempT_7;
						rTempT_8 <= rTempT_8; 
						rTempT_9 <= rTempT_9;
						rTempT_10 <= rTempT_10;
						rTempT_11 <= rTempT_11;
						rTempT_12 <= rTempT_12;
						rTempT_13 <= rTempT_13; 
						rTempT_14 <= rTempT_14; 
						rTempT_15 <= rTempT_15;
					
						rState			<= S_STEP_0_WAIT;
						rTempDone 		<= 1'b0;
						rCnt				<= rCnt;
//						rSCUpdate_req	<= 1'b0;
	//					rMsgExp_req 	<= 1'b0;
						rStepFnc_req 	<= 1'b0;									
					end////~if(wStepFnc_done ==1'b1) begin
				end //S_STEP_0_WAIT : begin
				S_STEP_1 : begin				
					rM1_0 <= rM1_0;
					rM1_1 <= rM1_1; 
					rM1_2 <= rM1_2;  
					rM1_3 <= rM1_3; 
					rM1_4 <= rM1_4; 
					rM1_5 <= rM1_5;
					rM1_6 <= rM1_6; 
					rM1_7 <= rM1_7; 
					rM1_8 <= rM1_8; 
					rM1_9 <= rM1_9; 
					rM1_10 <= rM1_10; 
					rM1_11 <= rM1_11; 
					rM1_12 <= rM1_12; 
					rM1_13 <= rM1_13; 
					rM1_14 <= rM1_14; 
					rM1_15 <= rM1_15;
					
					rM2_0 <= rM2_0; 
					rM2_1 <= rM2_1; 
					rM2_2 <= rM2_2; 
					rM2_3 <= rM2_3; 
					rM2_4 <= rM2_4; 
					rM2_5 <= rM2_5; 
					rM2_6 <= rM2_6; 
					rM2_7 <= rM2_7; 
					rM2_8 <= rM2_8; 
					rM2_9 <= rM2_9; 
					rM2_10 <= rM2_10; 
					rM2_11 <= rM2_11; 
					rM2_12 <= rM2_12; 
					rM2_13 <= rM2_13; 
					rM2_14 <= rM2_14; 
					rM2_15 <= rM2_15;
					
					rSC0	<=	rSC0;
					rSC1	<=	rSC1;
					rSC2	<=	rSC2;
					rSC3	<=	rSC3;
					rSC4	<=	rSC4;
					rSC5	<=	rSC5;
					rSC6	<=	rSC6;
					rSC7	<=	rSC7;
					
					rMj_0 <= rMj_0;
					rMj_1 <= rMj_1;
					rMj_2 <= rMj_2;
					rMj_3 <= rMj_3;
					rMj_4 <= rMj_4;
					rMj_5 <= rMj_5;
					rMj_6 <= rMj_6;
					rMj_7 <= rMj_7;
					rMj_8 <= rMj_8;
					rMj_9 <= rMj_9;
					rMj_10 <= rMj_10;
					rMj_11 <= rMj_11;
					rMj_12 <= rMj_12;
					rMj_13 <= rMj_13;
					rMj_14 <= rMj_14;
					rMj_15 <= rMj_15;
				
					rTempT_0 <= rTempT_0; 
					rTempT_1 <= rTempT_1; 
					rTempT_2 <= rTempT_2; 
					rTempT_3 <= rTempT_3; 
					rTempT_4 <= rTempT_4; 
					rTempT_5 <= rTempT_5; 
					rTempT_6 <= rTempT_6; 
					rTempT_7 <= rTempT_7;
					rTempT_8 <= rTempT_8; 
					rTempT_9 <= rTempT_9;
					rTempT_10 <= rTempT_10;
					rTempT_11 <= rTempT_11;
					rTempT_12 <= rTempT_12;
					rTempT_13 <= rTempT_13; 
					rTempT_14 <= rTempT_14; 
					rTempT_15 <= rTempT_15;
				
					rState			<= S_STEP_1_WAIT;
					rTempDone 		<= 1'b0;
					rCnt				<= rCnt;
		//			rSCUpdate_req	<= 1'b0;
		//			rMsgExp_req 	<= 1'b0;
					rStepFnc_req 	<= 1'b1;			
				end //S_STEP_1 : begin
				S_STEP_1_WAIT : begin
					if(wStepFnc_done == 1'b1) begin
						rM1_0 <= rM1_0;
						rM1_1 <= rM1_1; 
						rM1_2 <= rM1_2;  
						rM1_3 <= rM1_3; 
						rM1_4 <= rM1_4; 
						rM1_5 <= rM1_5;
						rM1_6 <= rM1_6; 
						rM1_7 <= rM1_7; 
						rM1_8 <= rM1_8; 
						rM1_9 <= rM1_9; 
						rM1_10 <= rM1_10; 
						rM1_11 <= rM1_11; 
						rM1_12 <= rM1_12; 
						rM1_13 <= rM1_13; 
						rM1_14 <= rM1_14; 
						rM1_15 <= rM1_15;
						
						rM2_0 <= rM2_0; 
						rM2_1 <= rM2_1; 
						rM2_2 <= rM2_2; 
						rM2_3 <= rM2_3; 
						rM2_4 <= rM2_4; 
						rM2_5 <= rM2_5; 
						rM2_6 <= rM2_6; 
						rM2_7 <= rM2_7; 
						rM2_8 <= rM2_8; 
						rM2_9 <= rM2_9; 
						rM2_10 <= rM2_10; 
						rM2_11 <= rM2_11; 
						rM2_12 <= rM2_12; 
						rM2_13 <= rM2_13; 
						rM2_14 <= rM2_14; 
						rM2_15 <= rM2_15;
						
						rSC0	<=	rSC0;
						rSC1	<=	rSC1;
						rSC2	<=	rSC2;
						rSC3	<=	rSC3;
						rSC4	<=	rSC4;
						rSC5	<=	rSC5;
						rSC6	<=	rSC6;
						rSC7	<=	rSC7;
						
						rMj_0 <= rMj_0;
						rMj_1 <= rMj_1;
						rMj_2 <= rM2_2;
						rMj_3 <= rMj_3;
						rMj_4 <= rMj_4;
						rMj_5 <= rMj_5;
						rMj_6 <= rMj_6;
						rMj_7 <= rMj_7;
						rMj_8 <= rMj_8;
						rMj_9 <= rMj_9;
						rMj_10 <= rMj_10;
						rMj_11 <= rMj_11;
						rMj_12 <= rMj_12;
						rMj_13 <= rMj_13;
						rMj_14 <= rMj_14;
						rMj_15 <= rMj_15;
					
						rTempT_0 <= wTempT_0; 
						rTempT_1 <= wTempT_1; 
						rTempT_2 <= wTempT_2; 
						rTempT_3 <= wTempT_3; 
						rTempT_4 <= wTempT_4; 
						rTempT_5 <= wTempT_5; 
						rTempT_6 <= wTempT_6; 
						rTempT_7 <= wTempT_7;
						rTempT_8 <= wTempT_8; 
						rTempT_9 <= wTempT_9;
						rTempT_10 <= wTempT_10;
						rTempT_11 <= wTempT_11;
						rTempT_12 <= wTempT_12;
						rTempT_13 <= wTempT_13; 
						rTempT_14 <= wTempT_14; 
						rTempT_15 <= wTempT_15;
					
						rState			<= S_MSG_EXP;
						//rState			<= S_MSG_EXP_WAIT;
						rTempDone 		<= 1'b0;
						rCnt				<= rCnt + 1'b1;
	//					rSCUpdate_req	<= 1'b0;
	//					rMsgExp_req 	<= 1'b0;
						rStepFnc_req 	<= 1'b0;				
					end//if(wStepFnc_done ==1'b1) begin
					else begin//~if(wStepFnc_done ==1'b1) begin
						rM1_0 <= rM1_0;
						rM1_1 <= rM1_1; 
						rM1_2 <= rM1_2;  
						rM1_3 <= rM1_3; 
						rM1_4 <= rM1_4; 
						rM1_5 <= rM1_5;
						rM1_6 <= rM1_6; 
						rM1_7 <= rM1_7; 
						rM1_8 <= rM1_8; 
						rM1_9 <= rM1_9; 
						rM1_10 <= rM1_10; 
						rM1_11 <= rM1_11; 
						rM1_12 <= rM1_12; 
						rM1_13 <= rM1_13; 
						rM1_14 <= rM1_14; 
						rM1_15 <= rM1_15;
						
						rM2_0 <= rM2_0; 
						rM2_1 <= rM2_1; 
						rM2_2 <= rM2_2; 
						rM2_3 <= rM2_3; 
						rM2_4 <= rM2_4; 
						rM2_5 <= rM2_5; 
						rM2_6 <= rM2_6; 
						rM2_7 <= rM2_7; 
						rM2_8 <= rM2_8; 
						rM2_9 <= rM2_9; 
						rM2_10 <= rM2_10; 
						rM2_11 <= rM2_11; 
						rM2_12 <= rM2_12; 
						rM2_13 <= rM2_13; 
						rM2_14 <= rM2_14; 
						rM2_15 <= rM2_15;
						
						rSC0	<=	rSC0;
						rSC1	<=	rSC1;
						rSC2	<=	rSC2;
						rSC3	<=	rSC3;
						rSC4	<=	rSC4;
						rSC5	<=	rSC5;
						rSC6	<=	rSC6;
						rSC7	<=	rSC7;
						
						rMj_0 <= rMj_0;
						rMj_1 <= rMj_1;
						rMj_2 <= rMj_2;
						rMj_3 <= rMj_3;
						rMj_4 <= rMj_4;
						rMj_5 <= rMj_5;
						rMj_6 <= rMj_6;
						rMj_7 <= rMj_7;
						rMj_8 <= rMj_8;
						rMj_9 <= rMj_9;
						rMj_10 <= rMj_10;
						rMj_11 <= rMj_11;
						rMj_12 <= rMj_12;
						rMj_13 <= rMj_13;
						rMj_14 <= rMj_14;
						rMj_15 <= rMj_15;
					
						rTempT_0 <= rTempT_0; 
						rTempT_1 <= rTempT_1; 
						rTempT_2 <= rTempT_2; 
						rTempT_3 <= rTempT_3; 
						rTempT_4 <= rTempT_4; 
						rTempT_5 <= rTempT_5; 
						rTempT_6 <= rTempT_6; 
						rTempT_7 <= rTempT_7;
						rTempT_8 <= rTempT_8; 
						rTempT_9 <= rTempT_9;
						rTempT_10 <= rTempT_10;
						rTempT_11 <= rTempT_11;
						rTempT_12 <= rTempT_12;
						rTempT_13 <= rTempT_13; 
						rTempT_14 <= rTempT_14; 
						rTempT_15 <= rTempT_15;
					
						rState			<= S_STEP_1_WAIT;
						rTempDone 		<= 1'b0;
						rCnt				<= rCnt;
			///  		rSCUpdate_req	<= 1'b0;
			//			rMsgExp_req 	<= 1'b0;
						rStepFnc_req 	<= 1'b0;								
					end////~if(wStepFnc_done ==1'b1) begin				
				end //S_STEP_1_WAIT : begin
				S_MSG_EXP : begin
					rM1_0 <= rM1_0;
					rM1_1 <= rM1_1; 
					rM1_2 <= rM1_2;  
					rM1_3 <= rM1_3; 
					rM1_4 <= rM1_4; 
					rM1_5 <= rM1_5;
					rM1_6 <= rM1_6; 
					rM1_7 <= rM1_7; 
					rM1_8 <= rM1_8; 
					rM1_9 <= rM1_9; 
					rM1_10 <= rM1_10; 
					rM1_11 <= rM1_11; 
					rM1_12 <= rM1_12; 
					rM1_13 <= rM1_13; 
					rM1_14 <= rM1_14; 
					rM1_15 <= rM1_15;
					
					rM2_0 <= rM2_0; 
					rM2_1 <= rM2_1; 
					rM2_2 <= rM2_2; 
					rM2_3 <= rM2_3; 
					rM2_4 <= rM2_4; 
					rM2_5 <= rM2_5; 
					rM2_6 <= rM2_6; 
					rM2_7 <= rM2_7; 
					rM2_8 <= rM2_8; 
					rM2_9 <= rM2_9; 
					rM2_10 <= rM2_10; 
					rM2_11 <= rM2_11; 
					rM2_12 <= rM2_12; 
					rM2_13 <= rM2_13; 
					rM2_14 <= rM2_14; 
					rM2_15 <= rM2_15;
										
					rSC0	<=	rSC0;
					rSC1	<=	rSC1;
					rSC2	<=	rSC2;
					rSC3	<=	rSC3;
					rSC4	<=	rSC4;
					rSC5	<=	rSC5;
					rSC6	<=	rSC6;
					rSC7	<=	rSC7;
					
					rMj_0 <= rMj_0;
					rMj_1 <= rMj_1;
					rMj_2 <= rMj_2;
					rMj_3 <= rMj_3;
					rMj_4 <= rMj_4;
					rMj_5 <= rMj_5;
					rMj_6 <= rMj_6;
					rMj_7 <= rMj_7;
					rMj_8 <= rMj_8;
					rMj_9 <= rMj_9;
					rMj_10 <= rMj_10;
					rMj_11 <= rMj_11;
					rMj_12 <= rMj_12;
					rMj_13 <= rMj_13;
					rMj_14 <= rMj_14;
					rMj_15 <= rMj_15;
				
					rTempT_0 <= rTempT_0; 
					rTempT_1 <= rTempT_1; 
					rTempT_2 <= rTempT_2; 
					rTempT_3 <= rTempT_3; 
					rTempT_4 <= rTempT_4; 
					rTempT_5 <= rTempT_5; 
					rTempT_6 <= rTempT_6; 
					rTempT_7 <= rTempT_7;
					rTempT_8 <= rTempT_8; 
					rTempT_9 <= rTempT_9;
					rTempT_10 <= rTempT_10;
					rTempT_11 <= rTempT_11;
					rTempT_12 <= rTempT_12;
					rTempT_13 <= rTempT_13; 
					rTempT_14 <= rTempT_14; 
					rTempT_15 <= rTempT_15;
				
					rState			<= S_MSG_EXP_WAIT;
					rTempDone 		<= 1'b0;
					rCnt				<= rCnt;
	//				rSCUpdate_req	<= 1'b1;
	//				rMsgExp_req 	<= 1'b1;
					rStepFnc_req 	<= 1'b0;					
				end //S_MSG_EXP : begin
				S_MSG_EXP_WAIT : begin
			//		if(wMsgExp_done == 1'b1 && wSCUpdate_done == 1'b1) begin
						if(rCnt < Ns_26) begin						
							rM1_0 <= wMj_0;
							rM1_1 <= wMj_1; 
							rM1_2 <= wMj_2;  
							rM1_3 <= wMj_3; 
							rM1_4 <= wMj_4; 
							rM1_5 <= wMj_5;
							rM1_6 <= wMj_6; 
							rM1_7 <= wMj_7; 
							rM1_8 <= wMj_8; 
							rM1_9 <= wMj_9; 
							rM1_10 <= wMj_10; 
							rM1_11 <= wMj_11; 
							rM1_12 <= wMj_12; 
							rM1_13 <= wMj_13; 
							rM1_14 <= wMj_14; 
							rM1_15 <= wMj_15;
							
							rM2_0 <= rM1_0; 
							rM2_1 <= rM1_1; 
							rM2_2 <= rM1_2; 
							rM2_3 <= rM1_3; 
							rM2_4 <= rM1_4; 
							rM2_5 <= rM1_5; 
							rM2_6 <= rM1_6; 
							rM2_7 <= rM1_7; 
							rM2_8 <= rM1_8; 
							rM2_9 <= rM1_9; 
							rM2_10 <= rM1_10; 
							rM2_11 <= rM1_11; 
							rM2_12 <= rM1_12; 
							rM2_13 <= rM1_13; 
							rM2_14 <= rM1_14; 
							rM2_15 <= rM1_15;
							
							rSC0	<=	wSC0;
							rSC1	<=	wSC1;
							rSC2	<=	wSC2;
							rSC3	<=	wSC3;
							rSC4	<=	wSC4;
							rSC5	<=	wSC5;
							rSC6	<=	wSC6;
							rSC7	<=	wSC7;
							
							rMj_0 <= wMj_0;
							rMj_1 <= wMj_1;
							rMj_2 <= wMj_2;
							rMj_3 <= wMj_3;
							rMj_4 <= wMj_4;
							rMj_5 <= wMj_5;
							rMj_6 <= wMj_6;
							rMj_7 <= wMj_7;
							rMj_8 <= wMj_8;
							rMj_9 <= wMj_9;
							rMj_10 <= wMj_10;
							rMj_11 <= wMj_11;
							rMj_12 <= wMj_12;
							rMj_13 <= wMj_13;
							rMj_14 <= wMj_14;
							rMj_15 <= wMj_15;
						
							rTempT_0 <= rTempT_0; 
							rTempT_1 <= rTempT_1; 
							rTempT_2 <= rTempT_2; 
							rTempT_3 <= rTempT_3; 
							rTempT_4 <= rTempT_4; 
							rTempT_5 <= rTempT_5; 
							rTempT_6 <= rTempT_6; 
							rTempT_7 <= rTempT_7;
							rTempT_8 <= rTempT_8; 
							rTempT_9 <= rTempT_9;
							rTempT_10 <= rTempT_10;
							rTempT_11 <= rTempT_11;
							rTempT_12 <= rTempT_12;
							rTempT_13 <= rTempT_13; 
							rTempT_14 <= rTempT_14; 
							rTempT_15 <= rTempT_15;
						
							rState			<= S_STEP_OTHERS;
							rTempDone 		<= 1'b0;
							rCnt				<= rCnt;
	//						rSCUpdate_req	<= 1'b0;
	//		       			rMsgExp_req 	<= 1'b0;
							rStepFnc_req 	<= 1'b0;					
						end 
						else  begin//~if(rCnt < Ns_26) begin	
							rM1_0 <= wMj_0;
                            rM1_1 <= wMj_1; 
                            rM1_2 <= wMj_2;  
                            rM1_3 <= wMj_3; 
                            rM1_4 <= wMj_4; 
                            rM1_5 <= wMj_5;
                            rM1_6 <= wMj_6; 
                            rM1_7 <= wMj_7; 
                            rM1_8 <= wMj_8; 
                            rM1_9 <= wMj_9; 
                            rM1_10 <= wMj_10; 
                            rM1_11 <= wMj_11; 
                            rM1_12 <= wMj_12; 
                            rM1_13 <= wMj_13; 
                            rM1_14 <= wMj_14; 
                            rM1_15 <= wMj_15;
                            
                            rM2_0 <= rM1_0; 
                            rM2_1 <= rM1_1; 
                            rM2_2 <= rM1_2; 
                            rM2_3 <= rM1_3; 
                            rM2_4 <= rM1_4; 
                            rM2_5 <= rM1_5; 
                            rM2_6 <= rM1_6; 
                            rM2_7 <= rM1_7; 
                            rM2_8 <= rM1_8; 
                            rM2_9 <= rM1_9; 
                            rM2_10 <= rM1_10; 
                            rM2_11 <= rM1_11; 
                            rM2_12 <= rM1_12; 
                            rM2_13 <= rM1_13; 
                            rM2_14 <= rM1_14; 
                            rM2_15 <= rM1_15;
							
							rSC0	<=	rSC0;
							rSC1	<=	rSC1;
							rSC2	<=	rSC2;
							rSC3	<=	rSC3;
							rSC4	<=	rSC4;
							rSC5	<=	rSC5;
							rSC6	<=	rSC6;
							rSC7	<=	rSC7;
			
							
							rMj_0 <= rMj_0;
							rMj_1 <= rMj_1;
							rMj_2 <= rMj_2;
							rMj_3 <= rMj_3;
							rMj_4 <= rMj_4;
							rMj_5 <= rMj_5;
							rMj_6 <= rMj_6;
							rMj_7 <= rMj_7;
							rMj_8 <= rMj_8;
							rMj_9 <= rMj_9;
							rMj_10 <= rMj_10;
							rMj_11 <= rMj_11;
							rMj_12 <= rMj_12;
							rMj_13 <= rMj_13;
							rMj_14 <= rMj_14;
							rMj_15 <= rMj_15;
						
							rTempT_0 <= rTempT_0; 
							rTempT_1 <= rTempT_1; 
							rTempT_2 <= rTempT_2; 
							rTempT_3 <= rTempT_3; 
							rTempT_4 <= rTempT_4; 
							rTempT_5 <= rTempT_5; 
							rTempT_6 <= rTempT_6; 
							rTempT_7 <= rTempT_7;
							rTempT_8 <= rTempT_8; 
							rTempT_9 <= rTempT_9;
							rTempT_10 <= rTempT_10;
							rTempT_11 <= rTempT_11;
							rTempT_12 <= rTempT_12;
							rTempT_13 <= rTempT_13; 
							rTempT_14 <= rTempT_14; 
							rTempT_15 <= rTempT_15;
						  
							
							rState			<= S_CFUNC_DONE;
							rTempDone 		<= 1'b1;
							rCnt				<= rCnt;
		//					rSCUpdate_req	<= 1'b0;
		//					rMsgExp_req 	<= 1'b0;
							rStepFnc_req 	<= 1'b0;
						end // ~if(rCnt < Ns_26) begin								
				end //S_MSG_EXP_WAIT : begin
				S_STEP_OTHERS : begin
					rM1_0 <= rM1_0;
					rM1_1 <= rM1_1; 
					rM1_2 <= rM1_2;  
					rM1_3 <= rM1_3; 
					rM1_4 <= rM1_4; 
					rM1_5 <= rM1_5;
					rM1_6 <= rM1_6; 
					rM1_7 <= rM1_7; 
					rM1_8 <= rM1_8; 
					rM1_9 <= rM1_9; 
					rM1_10 <= rM1_10; 
					rM1_11 <= rM1_11; 
					rM1_12 <= rM1_12; 
					rM1_13 <= rM1_13; 
					rM1_14 <= rM1_14; 
					rM1_15 <= rM1_15;
					
					rM2_0 <= rM2_0; 
					rM2_1 <= rM2_1; 
					rM2_2 <= rM2_2; 
					rM2_3 <= rM2_3; 
					rM2_4 <= rM2_4; 
					rM2_5 <= rM2_5; 
					rM2_6 <= rM2_6; 
					rM2_7 <= rM2_7; 
					rM2_8 <= rM2_8; 
					rM2_9 <= rM2_9; 
					rM2_10 <= rM2_10; 
					rM2_11 <= rM2_11; 
					rM2_12 <= rM2_12; 
					rM2_13 <= rM2_13; 
					rM2_14 <= rM2_14; 
					rM2_15 <= rM2_15;
					
					rSC0	<=	rSC0;
					rSC1	<=	rSC1;
					rSC2	<=	rSC2;
					rSC3	<=	rSC3;
					rSC4	<=	rSC4;
					rSC5	<=	rSC5;
					rSC6	<=	rSC6;
					rSC7	<=	rSC7;
					
					
					rMj_0 <= rMj_0;
					rMj_1 <= rMj_1;
					rMj_2 <= rMj_2;
					rMj_3 <= rMj_3;
					rMj_4 <= rMj_4;
					rMj_5 <= rMj_5;
					rMj_6 <= rMj_6;
					rMj_7 <= rMj_7;
					rMj_8 <= rMj_8;
					rMj_9 <= rMj_9;
					rMj_10 <= rMj_10;
					rMj_11 <= rMj_11;
					rMj_12 <= rMj_12;
					rMj_13 <= rMj_13;
					rMj_14 <= rMj_14;
					rMj_15 <= rMj_15;
				
					rTempT_0 <= rTempT_0; 
					rTempT_1 <= rTempT_1; 
					rTempT_2 <= rTempT_2; 
					rTempT_3 <= rTempT_3; 
					rTempT_4 <= rTempT_4; 
					rTempT_5 <= rTempT_5; 
					rTempT_6 <= rTempT_6; 
					rTempT_7 <= rTempT_7;
					rTempT_8 <= rTempT_8; 
					rTempT_9 <= rTempT_9;
					rTempT_10 <= rTempT_10;
					rTempT_11 <= rTempT_11;
					rTempT_12 <= rTempT_12;
					rTempT_13 <= rTempT_13; 
					rTempT_14 <= rTempT_14; 
					rTempT_15 <= rTempT_15;
				
					rState			<= S_STEP_OTHERS_WAIT;
					rTempDone 		<= 1'b0;
					rCnt				<= rCnt;
		//			rSCUpdate_req	<= 1'b0;
		//			rMsgExp_req 	<= 1'b0;
					rStepFnc_req 	<= 1'b1;			
				end //S_STEP_OTHERS : begin
				S_STEP_OTHERS_WAIT : begin
					if(wStepFnc_done == 1'b1) begin
						rM1_0 <= rM1_0;
						rM1_1 <= rM1_1; 
						rM1_2 <= rM1_2;  
						rM1_3 <= rM1_3; 
						rM1_4 <= rM1_4; 
						rM1_5 <= rM1_5;
						rM1_6 <= rM1_6; 
						rM1_7 <= rM1_7; 
						rM1_8 <= rM1_8; 
						rM1_9 <= rM1_9; 
						rM1_10 <= rM1_10; 
						rM1_11 <= rM1_11; 
						rM1_12 <= rM1_12; 
						rM1_13 <= rM1_13; 
						rM1_14 <= rM1_14; 
						rM1_15 <= rM1_15;
						
						rM2_0 <= rM2_0; 
						rM2_1 <= rM2_1; 
						rM2_2 <= rM2_2; 
						rM2_3 <= rM2_3; 
						rM2_4 <= rM2_4; 
						rM2_5 <= rM2_5; 
						rM2_6 <= rM2_6; 
						rM2_7 <= rM2_7; 
						rM2_8 <= rM2_8; 
						rM2_9 <= rM2_9; 
						rM2_10 <= rM2_10; 
						rM2_11 <= rM2_11; 
						rM2_12 <= rM2_12; 
						rM2_13 <= rM2_13; 
						rM2_14 <= rM2_14; 
						rM2_15 <= rM2_15;
						
						rSC0	<=	rSC0;
						rSC1	<=	rSC1;
						rSC2	<=	rSC2;
						rSC3	<=	rSC3;
						rSC4	<=	rSC4;
						rSC5	<=	rSC5;
						rSC6	<=	rSC6;
						rSC7	<=	rSC7;
					
						
						rMj_0 <= rMj_0;
						rMj_1 <= rMj_1;
						rMj_2 <= rM2_2;
						rMj_3 <= rMj_3;
						rMj_4 <= rMj_4;
						rMj_5 <= rMj_5;
						rMj_6 <= rMj_6;
						rMj_7 <= rMj_7;
						rMj_8 <= rMj_8;
						rMj_9 <= rMj_9;
						rMj_10 <= rMj_10;
						rMj_11 <= rMj_11;
						rMj_12 <= rMj_12;
						rMj_13 <= rMj_13;
						rMj_14 <= rMj_14;
						rMj_15 <= rMj_15;
					
						rTempT_0 <= wTempT_0; 
						rTempT_1 <= wTempT_1; 
						rTempT_2 <= wTempT_2; 
						rTempT_3 <= wTempT_3; 
						rTempT_4 <= wTempT_4; 
						rTempT_5 <= wTempT_5; 
						rTempT_6 <= wTempT_6; 
						rTempT_7 <= wTempT_7;
						rTempT_8 <= wTempT_8; 
						rTempT_9 <= wTempT_9;
						rTempT_10 <= wTempT_10;
						rTempT_11 <= wTempT_11;
						rTempT_12 <= wTempT_12;
						rTempT_13 <= wTempT_13; 
						rTempT_14 <= wTempT_14; 
						rTempT_15 <= wTempT_15;
					
						rState			<= S_MSG_EXP_WAIT;
					//	rState			<= S_MSG_EXP;S_MSG_EXP_WAIT
						rTempDone 		<= 1'b0;
						rCnt				<= rCnt + 1'b1;
		//				rSCUpdate_req	<= 1'b0;
		//				rMsgExp_req 	<= 1'b0;
						rStepFnc_req 	<= 1'b0;				
					end//if(wStepFnc_done ==1'b1) begin
					else begin//~if(wStepFnc_done ==1'b1) begin
						rM1_0 <= rM1_0;
						rM1_1 <= rM1_1; 
						rM1_2 <= rM1_2;  
						rM1_3 <= rM1_3; 
						rM1_4 <= rM1_4; 
						rM1_5 <= rM1_5;
						rM1_6 <= rM1_6; 
						rM1_7 <= rM1_7; 
						rM1_8 <= rM1_8; 
						rM1_9 <= rM1_9; 
						rM1_10 <= rM1_10; 
						rM1_11 <= rM1_11; 
						rM1_12 <= rM1_12; 
						rM1_13 <= rM1_13; 
						rM1_14 <= rM1_14; 
						rM1_15 <= rM1_15;
						
						rM2_0 <= rM2_0; 
						rM2_1 <= rM2_1; 
						rM2_2 <= rM2_2; 
						rM2_3 <= rM2_3; 
						rM2_4 <= rM2_4; 
						rM2_5 <= rM2_5; 
						rM2_6 <= rM2_6; 
						rM2_7 <= rM2_7; 
						rM2_8 <= rM2_8; 
						rM2_9 <= rM2_9; 
						rM2_10 <= rM2_10; 
						rM2_11 <= rM2_11; 
						rM2_12 <= rM2_12; 
						rM2_13 <= rM2_13; 
						rM2_14 <= rM2_14; 
						rM2_15 <= rM2_15;
						
						rSC0	<=	rSC0;
						rSC1	<=	rSC1;
						rSC2	<=	rSC2;
						rSC3	<=	rSC3;
						rSC4	<=	rSC4;
						rSC5	<=	rSC5;
						rSC6	<=	rSC6;
						rSC7	<=	rSC7;
					
						
						rMj_0 <= rMj_0;
						rMj_1 <= rMj_1;
						rMj_2 <= rMj_2;
						rMj_3 <= rMj_3;
						rMj_4 <= rMj_4;
						rMj_5 <= rMj_5;
						rMj_6 <= rMj_6;
						rMj_7 <= rMj_7;
						rMj_8 <= rMj_8;
						rMj_9 <= rMj_9;
						rMj_10 <= rMj_10;
						rMj_11 <= rMj_11;
						rMj_12 <= rMj_12;
						rMj_13 <= rMj_13;
						rMj_14 <= rMj_14;
						rMj_15 <= rMj_15;
					
						rTempT_0 <= rTempT_0; 
						rTempT_1 <= rTempT_1; 
						rTempT_2 <= rTempT_2; 
						rTempT_3 <= rTempT_3; 
						rTempT_4 <= rTempT_4; 
						rTempT_5 <= rTempT_5; 
						rTempT_6 <= rTempT_6; 
						rTempT_7 <= rTempT_7;
						rTempT_8 <= rTempT_8; 
						rTempT_9 <= rTempT_9;
						rTempT_10 <= rTempT_10;
						rTempT_11 <= rTempT_11;
						rTempT_12 <= rTempT_12;
						rTempT_13 <= rTempT_13; 
						rTempT_14 <= rTempT_14; 
						rTempT_15 <= rTempT_15;
					
						rState			<= S_STEP_OTHERS_WAIT;
						rTempDone 		<= 1'b0;
						rCnt				<= rCnt;
		//				rSCUpdate_req	<= 1'b0;
		//				rMsgExp_req 	<= 1'b0;
						rStepFnc_req 	<= 1'b0;								
					end////~if(wStepFnc_done ==1'b1) begin						
				end //S_STEP_OTHERS_WAIT : begin
				S_CFUNC_DONE : begin
					rM1_0 <= rM1_0;
					rM1_1 <= rM1_1; 
					rM1_2 <= rM1_2;  
					rM1_3 <= rM1_3; 
					rM1_4 <= rM1_4; 
					rM1_5 <= rM1_5;
					rM1_6 <= rM1_6; 
					rM1_7 <= rM1_7; 
					rM1_8 <= rM1_8; 
					rM1_9 <= rM1_9; 
					rM1_10 <= rM1_10; 
					rM1_11 <= rM1_11; 
					rM1_12 <= rM1_12; 
					rM1_13 <= rM1_13; 
					rM1_14 <= rM1_14; 
					rM1_15 <= rM1_15;
					
					rM2_0 <= rM2_0; 
					rM2_1 <= rM2_1; 
					rM2_2 <= rM2_2; 
					rM2_3 <= rM2_3; 
					rM2_4 <= rM2_4; 
					rM2_5 <= rM2_5; 
					rM2_6 <= rM2_6; 
					rM2_7 <= rM2_7; 
					rM2_8 <= rM2_8; 
					rM2_9 <= rM2_9; 
					rM2_10 <= rM2_10; 
					rM2_11 <= rM2_11; 
					rM2_12 <= rM2_12; 
					rM2_13 <= rM2_13; 
					rM2_14 <= rM2_14; 
					rM2_15 <= rM2_15;
					
					rSC0	<=	rSC0;
					rSC1	<=	rSC1;
					rSC2	<=	rSC2;
					rSC3	<=	rSC3;
					rSC4	<=	rSC4;
					rSC5	<=	rSC5;
					rSC6	<=	rSC6;
					rSC7	<=	rSC7;
					
					
					rMj_0 <= rMj_0;
					rMj_1 <= rMj_1;
					rMj_2 <= rMj_2;
					rMj_3 <= rMj_3;
					rMj_4 <= rMj_4;
					rMj_5 <= rMj_5;
					rMj_6 <= rMj_6;
					rMj_7 <= rMj_7;
					rMj_8 <= rMj_8;
					rMj_9 <= rMj_9;
					rMj_10 <= rMj_10;
					rMj_11 <= rMj_11;
					rMj_12 <= rMj_12;
					rMj_13 <= rMj_13;
					rMj_14 <= rMj_14;
					rMj_15 <= rMj_15;
				
					rTempT_0 <= rTempT_0; 
					rTempT_1 <= rTempT_1; 
					rTempT_2 <= rTempT_2; 
					rTempT_3 <= rTempT_3; 
					rTempT_4 <= rTempT_4; 
					rTempT_5 <= rTempT_5; 
					rTempT_6 <= rTempT_6; 
					rTempT_7 <= rTempT_7;
					rTempT_8 <= rTempT_8; 
					rTempT_9 <= rTempT_9;
					rTempT_10 <= rTempT_10;
					rTempT_11 <= rTempT_11;
					rTempT_12 <= rTempT_12;
					rTempT_13 <= rTempT_13; 
					rTempT_14 <= rTempT_14; 
					rTempT_15 <= rTempT_15;
				
					rState			<= S_IDLE;
					rTempDone 		<= 1'b0;
					rCnt				<= rCnt;
//					rSCUpdate_req	<= 1'b0;
	//				rMsgExp_req 	<= 1'b0;
					rStepFnc_req 	<= 1'b0;
				end//S_CFUNC_DONE : begin
				default: begin
					rM1_0 <= 32'h0;
					rM1_1 <= 32'h0; 
					rM1_2 <= 32'h0;  
					rM1_3 <= 32'h0; 
					rM1_4 <= 32'h0; 
					rM1_5 <= 32'h0;
					rM1_6 <= 32'h0; 
					rM1_7 <= 32'h0; 
					rM1_8 <= 32'h0; 
					rM1_9 <= 32'h0; 
					rM1_10 <= 32'h0; 
					rM1_11 <= 32'h0; 
					rM1_12 <= 32'h0; 
					rM1_13 <= 32'h0; 
					rM1_14 <= 32'h0; 
					rM1_15 <= 32'h0;
					
					rM2_0 <= 32'h0; 
					rM2_1 <= 32'h0; 
					rM2_2 <= 32'h0; 
					rM2_3 <= 32'h0; 
					rM2_4 <= 32'h0; 
					rM2_5 <= 32'h0; 
					rM2_6 <= 32'h0; 
					rM2_7 <= 32'h0; 
					rM2_8 <= 32'h0; 
					rM2_9 <= 32'h0; 
					rM2_10 <= 32'h0; 
					rM2_11 <= 32'h0; 
					rM2_12 <= 32'h0; 
					rM2_13 <= 32'h0; 
					rM2_14 <= 32'h0; 
					rM2_15 <= 32'h0;
					
					rSC0	<=	32'h0;
					rSC1	<=	32'h0;
					rSC2	<=	32'h0;
					rSC3	<=	32'h0;
					rSC4	<=	32'h0;
					rSC5	<=	32'h0;
					rSC6	<=	32'h0;
					rSC7	<=	32'h0;
					
					rMj_0 <= 32'h0;
					rMj_1 <= 32'h0;
					rMj_2 <= 32'h0;
					rMj_3 <= 32'h0;
					rMj_4 <= 32'h0;
					rMj_5 <= 32'h0;
					rMj_6 <= 32'h0;
					rMj_7 <= 32'h0;
					rMj_8 <= 32'h0;
					rMj_9 <= 32'h0;
					rMj_10 <= 32'h0;
					rMj_11 <= 32'h0;
					rMj_12 <= 32'h0;
					rMj_13 <= 32'h0;
					rMj_14 <= 32'h0;
					rMj_15 <= 32'h0;
				
					rTempT_0 <= 32'h0; 
					rTempT_1 <= 32'h0; 
					rTempT_2 <= 32'h0; 
					rTempT_3 <= 32'h0; 
					rTempT_4 <= 32'h0; 
					rTempT_5 <= 32'h0; 
					rTempT_6 <= 32'h0; 
					rTempT_7 <= 32'h0;
					rTempT_8 <= 32'h0; 
					rTempT_9 <= 32'h0;
					rTempT_10 <= 32'h0;
					rTempT_11 <= 32'h0;
					rTempT_12 <= 32'h0;
					rTempT_13 <= 32'h0; 
					rTempT_14 <= 32'h0; 
					rTempT_15 <= 32'h0;
				
					rState			<= S_IDLE;
					rTempDone 		<= 1'b0;
					rCnt				<= 8'h00;
		//			rSCUpdate_req	<= 1'b0;
		//			rMsgExp_req 	<= 1'b0;
					rStepFnc_req 	<= 1'b0;
				end //default: begin
			endcase 
		end //begin  //  ~ if (!iRset_n)
	end // always @(negedge iRset_n or posedge iClk)
											
endmodule