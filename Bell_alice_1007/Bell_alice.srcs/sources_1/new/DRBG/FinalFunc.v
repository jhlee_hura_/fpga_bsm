//==================================================================
// Company       : NSRI
// Engineer      : Kim Eun Ji
// Updated Date  : 2018.12.02
// 
// Design Name   : Final Function of LSH_512(8*w)_256,
//								 Word= 32bit, Output = 256bit
// Module Name   : FinalFunc.V
// Project Name  : QRNG
//
// Target Devices: Cyclone V SOC (5CSXFC6D6F31C8)
// Tool versions : Quartus Prime 16.1.0 Build 196  10/24/2016
// Revision      : 1.0.0 
//------------------------------------------------------------------
// Notice        :
//==================================================================

module FinalFunc
(
	input		wire iRset_n,
	input 	wire iClk,
	input		wire iReq,
	input		wire [31:0] iCV0, iCV1, iCV2, iCV3, iCV4, iCV5, iCV6, iCV7, iCV8, iCV9, iCV10, iCV11, iCV12, iCV13, iCV14, iCV15,	
	output	wire [255:0] oHashValue,
	output	wire oDone
);

//========================================================================	
// Parameter Declaration
//========================================================================

//	parameter [7:0] Ns_28	=	8'h1C; //Ns = 28 = 0x1C
	parameter [3:0] COMPLETE_CNT   = 4'h2;
	parameter [1:0] S_IDLE         = 2'b00;
	parameter [1:0] S_DELAY        = 2'b01;
	parameter [1:0] S_DONE         = 2'b10;
	
	//delay  Valid Signal with 1/2 Clk
    parameter STATE_DELAY_IDLE       = 0;
    parameter STATE_DELAY_1CLK       = 1;
    parameter STATE_DELAY_2CLK       = 2;
    parameter STATE_DELAY_3CLK       = 3;
    
     reg rTEmpReStateValid;
     reg [3:0] rState_enable;
//========================================================================	
// Internal Signals
//========================================================================

	
	// wire	
	wire	[31:0]	wTempH_0, wTempH_1, wTempH_2, wTempH_3, wTempH_4, wTempH_5, wTempH_6, wTempH_7;
	
	
	wire  [31:0]  wTemp0, wTemp1, wTemp2, wTemp3, wTemp4, wTemp5, wTemp6, wTemp7;
//	wire	[63:0]	wTemp0_0, wTemp0_1, wTemp0_2, wTemp0_3, wTemp0_4, wTemp0_5, wTemp0_6, wTemp0_7;
//	wire	[63:0]	wTemp1_0, wTemp1_1, wTemp1_2, wTemp1_3, wTemp1_4, wTemp1_5, wTemp1_6, wTemp1_7;
//	wire	[63:0]	wTemp2_0, wTemp2_1, wTemp2_2, wTemp2_3, wTemp2_4, wTemp2_5, wTemp2_6, wTemp2_7;
//	wire	[63:0]	wTemp3_0, wTemp3_1, wTemp3_2, wTemp3_3, wTemp3_4, wTemp3_5, wTemp3_6, wTemp3_7;
//	wire	[63:0]	wTemp4_0, wTemp4_1, wTemp4_2, wTemp4_3, wTemp4_4, wTemp4_5, wTemp4_6, wTemp4_7;
//	wire	[63:0]	wTemp5_0, wTemp5_1, wTemp5_2, wTemp5_3, wTemp5_4, wTemp5_5, wTemp5_6, wTemp5_7;
//	wire	[63:0]	wTemp6_0, wTemp6_1, wTemp6_2, wTemp6_3, wTemp6_4, wTemp6_5, wTemp6_6, wTemp6_7;
//	wire	[63:0]	wTemp7_0, wTemp7_1, wTemp7_2, wTemp7_3, wTemp7_4, wTemp7_5, wTemp7_6, wTemp7_7;
	
	reg	[1:0]		rState;
	reg				rTempDone;
	reg	[3:0]		rCnt;
	reg	[31:0]	rTempCV[15:0];
//========================================================================
//  Output Assignment
//========================================================================
	assign wTempH_0 = rTempCV[0] ^ rTempCV[8];
	assign wTempH_1 = rTempCV[1] ^ rTempCV[9];
	assign wTempH_2 = rTempCV[2] ^ rTempCV[10];
	assign wTempH_3 = rTempCV[3] ^ rTempCV[11];
	assign wTempH_4 = rTempCV[4] ^ rTempCV[12];
	assign wTempH_5 = rTempCV[5] ^ rTempCV[13];
	assign wTempH_6 = rTempCV[6] ^ rTempCV[14];
	assign wTempH_7 = rTempCV[7] ^ rTempCV[15];

	assign wTemp0 = {wTempH_0[7:0], wTempH_0[15:8], wTempH_0[23:16], wTempH_0[31:24]};
	assign wTemp1 = {wTempH_1[7:0], wTempH_1[15:8], wTempH_1[23:16], wTempH_1[31:24]};
	assign wTemp2 = {wTempH_2[7:0], wTempH_2[15:8], wTempH_2[23:16], wTempH_2[31:24]};
	assign wTemp3 = {wTempH_3[7:0], wTempH_3[15:8], wTempH_3[23:16], wTempH_3[31:24]};
	assign wTemp4 = {wTempH_4[7:0], wTempH_4[15:8], wTempH_4[23:16], wTempH_4[31:24]};
	assign wTemp5 = {wTempH_5[7:0], wTempH_5[15:8], wTempH_5[23:16], wTempH_5[31:24]};
	assign wTemp6 = {wTempH_6[7:0], wTempH_6[15:8], wTempH_6[23:16], wTempH_6[31:24]};
	assign wTemp7 = {wTempH_7[7:0], wTempH_7[15:8], wTempH_7[23:16], wTempH_7[31:24]};
	
	assign oHashValue = {wTemp0, wTemp1, wTemp2, wTemp3,wTemp4, wTemp5, wTemp6, wTemp7};
	
	
	////////////////////////////////////////////////////////////////////////////////////////////
	//assign oDone	= rTempDone;
	assign oDone	= rTEmpReStateValid;
	////////////////////////////////////////////////////////////////////////////////////////////
	//1/2 clock 만큼 vaild 신호 밀기 && width = 2clk!!
	////////////////////////////////////////////////////////////////////////////////////////////
      always @(negedge iClk or negedge iRset_n) begin
          if(!iRset_n) begin
              rState_enable <=STATE_DELAY_IDLE;
              rTEmpReStateValid <=1'b0;
          end // if(!iReset_n) begin
          else begin //~if(!iReset_n) begin
             case(rState_enable)
                 STATE_DELAY_IDLE: begin
                     if(rTempDone) begin
                        rState_enable              <=STATE_DELAY_1CLK;
                        rTEmpReStateValid   <= 1'b1;
                      end // if(digestValid_1) begin
                      else begin // ~ if(iEnable) begin
                        rState_enable              <=STATE_DELAY_IDLE;
                        rTEmpReStateValid   <=1'b0;
                      end // ~ if(digestValid_1) begin
                  end //STATE_DELAY_IDLE: begin
                  STATE_DELAY_1CLK: begin 
                        rState_enable              <=STATE_DELAY_IDLE;
                        //rTEmpReStateValid   <=1'b1; //2Clk Width
						rTEmpReStateValid   <=1'b0; //1Clk Width
                  end //STATE_DELAY_1CLK: begin 
//                  STATE_DELAY_2CLK: begin 
//                       rState_enable              <=STATE_IDLE;
//                       rTEmpReStateValid   <=1'b1;
//                  end //STATE_DELAY_2CLK: begin 
                 default: begin 
                    rState_enable              <=STATE_DELAY_IDLE;
                    rTEmpReStateValid   <=1'b0;
                 end             
             endcase //  case(rState)
          end // /~/if(!iReset_n) begin
      end //always @(posedge iClk or negedge iReset_n) begin
	////////////////////////////////////////////////////////////////////////////////////////////
	
	
	
//========================================================================	
// Initial Setting
//========================================================================
	initial
	begin
	
		rTempDone	<= 1'b0;
		rCnt			<= 4'h0;
		rState		<= S_IDLE;
		
		rTempCV[0]  <= 32'h0;
		rTempCV[1]  <= 32'h0;
		rTempCV[2]  <= 32'h0;
		rTempCV[3]  <= 32'h0;
		rTempCV[4]  <= 32'h0;
		rTempCV[5]  <= 32'h0;
		rTempCV[6]  <= 32'h0;
		rTempCV[7]  <= 32'h0;
		rTempCV[8]  <= 32'h0;
		rTempCV[9]  <= 32'h0;
		rTempCV[10]  <= 32'h0;
		rTempCV[11]  <= 32'h0;
		rTempCV[12]  <= 32'h0;
		rTempCV[13]  <= 32'h0;
		rTempCV[14]  <= 32'h0;
		rTempCV[15]  <= 32'h0;
		
	end
	
	always @(negedge iRset_n or posedge iClk) begin
		if(!iRset_n)	begin
		rTempDone	<= 1'b0;
		rCnt			<= 4'h0;
		rState		<= S_IDLE;
		
		rTempCV[0]  <= 32'h0;
		rTempCV[1]  <= 32'h0;
		rTempCV[2]  <= 32'h0;
		rTempCV[3]  <= 32'h0;
		rTempCV[4]  <= 32'h0;
		rTempCV[5]  <= 32'h0;
		rTempCV[6]  <= 32'h0;
		rTempCV[7]  <= 32'h0;
		rTempCV[8]  <= 32'h0;
		rTempCV[9]  <= 32'h0;
		rTempCV[10]  <= 32'h0;
		rTempCV[11]  <= 32'h0;
		rTempCV[12]  <= 32'h0;
		rTempCV[13]  <= 32'h0;
		rTempCV[14]  <= 32'h0;
		rTempCV[15]  <= 32'h0;
		
		
		end //if(!iRset_n)	begin
		else 	begin  //  ~ if (!iRset_n)
			case (rState)
				S_IDLE: begin
					if(iReq) begin			//S_INIT		
						rTempDone	<= 1'b0;
						rCnt			<= 4'h0;
						rState		<= S_DELAY;
						rTempCV[0]  <= iCV0;
						rTempCV[1]  <= iCV1;
						rTempCV[2]  <= iCV2;
						rTempCV[3]  <= iCV3;
						rTempCV[4]  <= iCV4;
						rTempCV[5]  <= iCV5;
						rTempCV[6]  <= iCV6;
						rTempCV[7]  <= iCV7;
						rTempCV[8]  <= iCV8;
						rTempCV[9]  <= iCV9;
						rTempCV[10]  <= iCV10;
						rTempCV[11]  <= iCV11;
						rTempCV[12]  <= iCV12;
						rTempCV[13]  <= iCV13;
						rTempCV[14]  <= iCV14;
						rTempCV[15]  <= iCV15;
					end //if(iReq) begin
					else begin 
						rTempDone	<= 1'b0;
						rCnt			<= 4'h0;
						rState		<= S_IDLE;
						
						rTempCV[0]  <= rTempCV[0];
						rTempCV[1]  <= rTempCV[1];
						rTempCV[2]  <= rTempCV[2];
						rTempCV[3]  <= rTempCV[3];
						rTempCV[4]  <= rTempCV[4];
						rTempCV[5]  <= rTempCV[5];
						rTempCV[6]  <= rTempCV[6];
						rTempCV[7]  <= rTempCV[7];
						rTempCV[8]  <= rTempCV[8];
						rTempCV[9]  <= rTempCV[9];
						rTempCV[10]  <= rTempCV[10];
						rTempCV[11]  <= rTempCV[11];
						rTempCV[12]  <= rTempCV[12];
						rTempCV[13]  <= rTempCV[13];
						rTempCV[14]  <= rTempCV[14];
						rTempCV[15]  <= rTempCV[15];
					end // ~if(iReq) begin
				end //S_IDLE: begin

				S_DELAY: begin
					if(rCnt == COMPLETE_CNT) begin
						rTempDone	<= 1'b1;
						rCnt			<= 4'h0;
						rState		<= S_DONE;	
						
						rTempCV[0]  <= rTempCV[0];
						rTempCV[1]  <= rTempCV[1];
						rTempCV[2]  <= rTempCV[2];
						rTempCV[3]  <= rTempCV[3];
						rTempCV[4]  <= rTempCV[4];
						rTempCV[5]  <= rTempCV[5];
						rTempCV[6]  <= rTempCV[6];
						rTempCV[7]  <= rTempCV[7];
						rTempCV[8]  <= rTempCV[8];
						rTempCV[9]  <= rTempCV[9];
						rTempCV[10]  <= rTempCV[10];
						rTempCV[11]  <= rTempCV[11];
						rTempCV[12]  <= rTempCV[12];
						rTempCV[13]  <= rTempCV[13];
						rTempCV[14]  <= rTempCV[14];
						rTempCV[15]  <= rTempCV[15];
					end //if(rCnt == COMPLETE_CNT) begin
					else begin
						rTempDone	<= 1'b0;
						rCnt			<= rCnt + 1'b1;
						rState		<= S_DELAY;	
						
						rTempCV[0]  <= rTempCV[0];
						rTempCV[1]  <= rTempCV[1];
						rTempCV[2]  <= rTempCV[2];
						rTempCV[3]  <= rTempCV[3];
						rTempCV[4]  <= rTempCV[4];
						rTempCV[5]  <= rTempCV[5];
						rTempCV[6]  <= rTempCV[6];
						rTempCV[7]  <= rTempCV[7];
						rTempCV[8]  <= rTempCV[8];
						rTempCV[9]  <= rTempCV[9];
						rTempCV[10]  <= rTempCV[10];
						rTempCV[11]  <= rTempCV[11];
						rTempCV[12]  <= rTempCV[12];
						rTempCV[13]  <= rTempCV[13];
						rTempCV[14]  <= rTempCV[14];
						rTempCV[15]  <= rTempCV[15];
					end // !if(rCnt == COMPLETE_CNT) begin				
				end //S_DELAY: begin
				S_DONE: begin
					rTempDone	<= 1'b0;
					rCnt			<= rCnt;
					rState		<= S_IDLE;
					
					rTempCV[0]  <= rTempCV[0];
					rTempCV[1]  <= rTempCV[1];
					rTempCV[2]  <= rTempCV[2];
					rTempCV[3]  <= rTempCV[3];
					rTempCV[4]  <= rTempCV[4];
					rTempCV[5]  <= rTempCV[5];
					rTempCV[6]  <= rTempCV[6];
					rTempCV[7]  <= rTempCV[7];
					rTempCV[8]  <= rTempCV[8];
					rTempCV[9]  <= rTempCV[9];
					rTempCV[10]  <= rTempCV[10];
					rTempCV[11]  <= rTempCV[11];
					rTempCV[12]  <= rTempCV[12];
					rTempCV[13]  <= rTempCV[13];
					rTempCV[14]  <= rTempCV[14];
					rTempCV[15]  <= rTempCV[15];
				end //S_DONE: begin
				default: begin
					rTempDone	<= 1'b0;
					rCnt			<= 4'h0;
					rState		<= S_IDLE;
					
					rTempCV[0]  <= 32'h0;
					rTempCV[1]  <= 32'h0;
					rTempCV[2]  <= 32'h0;
					rTempCV[3]  <= 32'h0;
					rTempCV[4]  <= 32'h0;
					rTempCV[5]  <= 32'h0;
					rTempCV[6]  <= 32'h0;
					rTempCV[7]  <= 32'h0;
					rTempCV[8]  <= 32'h0;
					rTempCV[9]  <= 32'h0;
					rTempCV[10]  <= 32'h0;
					rTempCV[11]  <= 32'h0;
					rTempCV[12]  <= 32'h0;
					rTempCV[13]  <= 32'h0;
					rTempCV[14]  <= 32'h0;
					rTempCV[15]  <= 32'h0;
				end //default: begin
			endcase 
		end //begin  //  ~ if (!iRset_n)
	end // always @(negedge iRset_n or posedge iClk)
											
endmodule
