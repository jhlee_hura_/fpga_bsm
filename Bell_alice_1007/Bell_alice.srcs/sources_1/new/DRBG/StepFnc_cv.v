//==================================================================
// Company       : NSRI
// Engineer      : Kim Eun Ji
// Updated Date  : 2018.12.19
// 
// Design Name   : Step Function Module for Compression Function of
//						 LSH_256(8*w)_256, Word= 32bit, Output = 256bit
// Module Name   : StepFnc_cv.V
// Project Name  : QRNG
//
// Target Devices: Cyclone V SOC (5CSXFC6D6F31C8)
// Tool versions : Quartus Prime 16.1.0 Build 196  10/24/2016
// Revision      : 1.0.0 
//------------------------------------------------------------------
// Notice        :
//==================================================================

module StepFnc_cv
(
	input		wire iRset_n,
	input 	wire iClk,
	input		wire iReq,
	input		wire iM_j_odd, //if j is odd, iM_j_odd = 1
	input		wire [31:0] iM0, iM1, iM2, iM3, iM4, iM5, iM6, iM7, iM8, iM9, iM10, iM11, iM12, iM13, iM14, iM15,
	
	input		wire [31:0] iT0, iT1, iT2, iT3, iT4, iT5, iT6, iT7, iT8, iT9, iT10, iT11, iT12, iT13, iT14, iT15,
	input		wire [31:0] iSC0, iSC1, iSC2, iSC3, iSC4, iSC5, iSC6, iSC7,	
	
	output	wire [31:0] oT0, oT1, oT2, oT3, oT4, oT5, oT6, oT7, oT8, oT9, oT10, oT11, oT12, oT13, oT14, oT15,
	output	wire oDone
);

//========================================================================	
// Parameter Declaration
//========================================================================

//	parameter [7:0] Ns_28	=	8'h1C; //Ns = 28 = 0x1C
	parameter [7:0] COMPLETE_CNT	= 8'h00;
	parameter [1:0] S_IDLE 			= 2'b00;
	parameter [1:0] S_DELAY			= 2'b01;
	parameter [1:0] S_DONE 			= 2'b10;
	
	
//========================================================================	
// Internal Signals
//========================================================================

	// wire	
	wire	[31:0]	wTemT1_0, wTemT1_1, wTemT1_2, wTemT1_3, wTemT1_4, wTemT1_5, wTemT1_6, wTemT1_7, wTemT1_8, wTemT1_9, wTemT1_10, wTemT1_11, wTemT1_12, wTemT1_13, wTemT1_14, wTemT1_15; 
	wire	[31:0]	wTemT2_0, wTemT2_1, wTemT2_2, wTemT2_3, wTemT2_4, wTemT2_5, wTemT2_6, wTemT2_7, wTemT2_8, wTemT2_9, wTemT2_10, wTemT2_11, wTemT2_12, wTemT2_13, wTemT2_14, wTemT2_15; 
	wire	[31:0]	wTemT3_0, wTemT3_1, wTemT3_2, wTemT3_3, wTemT3_4, wTemT3_5, wTemT3_6, wTemT3_7, wTemT3_8, wTemT3_9, wTemT3_10, wTemT3_11, wTemT3_12, wTemT3_13, wTemT3_14, wTemT3_15;
	wire	[31:0]	wTemT4_0, wTemT4_1, wTemT4_2, wTemT4_3, wTemT4_4, wTemT4_5, wTemT4_6, wTemT4_7, wTemT4_8, wTemT4_9, wTemT4_10, wTemT4_11, wTemT4_12, wTemT4_13, wTemT4_14, wTemT4_15;
	wire	[31:0]	wTemT5_0, wTemT5_1, wTemT5_2, wTemT5_3, wTemT5_4, wTemT5_5, wTemT5_6, wTemT5_7, wTemT5_8, wTemT5_9, wTemT5_10, wTemT5_11, wTemT5_12, wTemT5_13, wTemT5_14, wTemT5_15;
	wire	[31:0]	wTemT6_0, wTemT6_1, wTemT6_2, wTemT6_3, wTemT6_4, wTemT6_5, wTemT6_6, wTemT6_7, wTemT6_8, wTemT6_9, wTemT6_10, wTemT6_11, wTemT6_12, wTemT6_13, wTemT6_14, wTemT6_15;
	wire	[31:0]	wTemT7_0, wTemT7_1, wTemT7_2, wTemT7_3, wTemT7_4, wTemT7_5, wTemT7_6, wTemT7_7, wTemT7_8, wTemT7_9, wTemT7_10, wTemT7_11, wTemT7_12, wTemT7_13, wTemT7_14, wTemT7_15;
	wire	[31:0]	wTemT8_0, wTemT8_1, wTemT8_2, wTemT8_3, wTemT8_4, wTemT8_5, wTemT8_6, wTemT8_7, wTemT8_8, wTemT8_9, wTemT8_10, wTemT8_11, wTemT8_12, wTemT8_13, wTemT8_14, wTemT8_15;
	
	
	reg	[1:0]		rState;
	reg				rTempDone;
	reg	[7:0]		rCnt;

//========================================================================
//  Output Assignment
//========================================================================
	
	assign oT0		= wTemT8_6;
	assign oT1		= wTemT8_4;
	assign oT2		= wTemT8_5;
	assign oT3		= wTemT8_7;
	assign oT4		= wTemT8_12;
	assign oT5		= wTemT8_15;
	assign oT6		= wTemT8_14;
	assign oT7		= wTemT8_13;
	assign oT8		= wTemT8_2;
	assign oT9		= wTemT8_0;
	assign oT10	= wTemT8_1;
	assign oT11	= wTemT8_3;
	assign oT12	= wTemT8_8;
	assign oT13	= wTemT8_11;
	assign oT14	= wTemT8_10;
	assign oT15	= wTemT8_9;
	
	
	assign oDone	= rTempDone;
	
//========================================================================
//  Inner Assignment
//========================================================================
	// MSG ADD
	assign wTemT1_0 = iM0 ^ iT0;
	assign wTemT1_1 = iM1 ^ iT1;
	assign wTemT1_2 = iM2 ^ iT2;
	assign wTemT1_3 = iM3 ^ iT3;
	assign wTemT1_4 = iM4 ^ iT4;
	assign wTemT1_5 = iM5 ^ iT5;
	assign wTemT1_6 = iM6 ^ iT6;
	assign wTemT1_7 = iM7 ^ iT7;
	assign wTemT1_8 = iM8 ^ iT8;
	assign wTemT1_9 = iM9 ^ iT9;
	assign wTemT1_10 = iM10 ^ iT10;
	assign wTemT1_11 = iM11 ^ iT11;
	assign wTemT1_12 = iM12 ^ iT12;
	assign wTemT1_13 = iM13 ^ iT13;
	assign wTemT1_14 = iM14 ^ iT14;
	assign wTemT1_15 = iM15 ^ iT15;
	
	//1st Mixing
	assign wTemT2_0 = wTemT1_0 + wTemT1_8;
	assign wTemT2_1 = wTemT1_1 + wTemT1_9;
	assign wTemT2_2 = wTemT1_2 + wTemT1_10;
	assign wTemT2_3 = wTemT1_3 + wTemT1_11;
	assign wTemT2_4 = wTemT1_4 + wTemT1_12;
	assign wTemT2_5 = wTemT1_5 + wTemT1_13;
	assign wTemT2_6 = wTemT1_6 + wTemT1_14;
	assign wTemT2_7 = wTemT1_7 + wTemT1_15;
	assign wTemT2_8 = wTemT1_8;
	assign wTemT2_9 = wTemT1_9;
	assign wTemT2_10 = wTemT1_10;
	assign wTemT2_11 = wTemT1_11;
	assign wTemT2_12 = wTemT1_12;
	assign wTemT2_13 = wTemT1_13;
	assign wTemT2_14 = wTemT1_14;
	assign wTemT2_15 = wTemT1_15;
	
	//Alpha Shifting
	assign wTemT3_0 = iM_j_odd? {wTemT2_0[26:0], wTemT2_0[31:27]} : {wTemT2_0[2:0], wTemT2_0[31:3]}; //iM_j_odd? wTemT2_0<<5 : wTemT2_0<<29;
	assign wTemT3_1 = iM_j_odd? {wTemT2_1[26:0], wTemT2_1[31:27]} : {wTemT2_1[2:0], wTemT2_1[31:3]};
	assign wTemT3_2 = iM_j_odd? {wTemT2_2[26:0], wTemT2_2[31:27]} : {wTemT2_2[2:0], wTemT2_2[31:3]};
	assign wTemT3_3 = iM_j_odd? {wTemT2_3[26:0], wTemT2_3[31:27]} : {wTemT2_3[2:0], wTemT2_3[31:3]};
	assign wTemT3_4 = iM_j_odd? {wTemT2_4[26:0], wTemT2_4[31:27]} : {wTemT2_4[2:0], wTemT2_4[31:3]};
	assign wTemT3_5 = iM_j_odd? {wTemT2_5[26:0], wTemT2_5[31:27]} : {wTemT2_5[2:0], wTemT2_5[31:3]};
	assign wTemT3_6 = iM_j_odd? {wTemT2_6[26:0], wTemT2_6[31:27]} : {wTemT2_6[2:0], wTemT2_6[31:3]};
	assign wTemT3_7 = iM_j_odd? {wTemT2_7[26:0], wTemT2_7[31:27]} : {wTemT2_7[2:0], wTemT2_7[31:3]};
	assign wTemT3_8 = wTemT2_8;
	assign wTemT3_9 = wTemT2_9;
	assign wTemT3_10 = wTemT2_10;
	assign wTemT3_11 = wTemT2_11;
	assign wTemT3_12 = wTemT2_12;
	assign wTemT3_13 = wTemT2_13;
	assign wTemT3_14 = wTemT2_14;
	assign wTemT3_15 = wTemT2_15;
	
	//Addign SC
	assign wTemT4_0 = wTemT3_0 ^ iSC0 ;
	assign wTemT4_1 = wTemT3_1 ^ iSC1 ;
	assign wTemT4_2 = wTemT3_2 ^ iSC2 ;
	assign wTemT4_3 = wTemT3_3 ^ iSC3 ;
	assign wTemT4_4 = wTemT3_4 ^ iSC4 ;
	assign wTemT4_5 = wTemT3_5 ^ iSC5 ;
	assign wTemT4_6 = wTemT3_6 ^ iSC6 ;
	assign wTemT4_7 = wTemT3_7 ^ iSC7 ;
	assign wTemT4_8 = wTemT3_8 ;
	assign wTemT4_9 = wTemT3_9 ;
	assign wTemT4_10 = wTemT3_10 ;
	assign wTemT4_11 = wTemT3_11 ;
	assign wTemT4_12 = wTemT3_12 ;
	assign wTemT4_13 = wTemT3_13 ;
	assign wTemT4_14 = wTemT3_14 ;
	assign wTemT4_15 = wTemT3_15 ;
	
	//2nd Mixing
	assign wTemT5_0 = wTemT4_0;
	assign wTemT5_1 = wTemT4_1;
	assign wTemT5_2 = wTemT4_2;
	assign wTemT5_3 = wTemT4_3;
	assign wTemT5_4 = wTemT4_4;
	assign wTemT5_5 = wTemT4_5;
	assign wTemT5_6 = wTemT4_6;
	assign wTemT5_7 = wTemT4_7;
	assign wTemT5_8 = wTemT4_8 + wTemT4_0;
	assign wTemT5_9 = wTemT4_9 + wTemT4_1;
	assign wTemT5_10 = wTemT4_10 + wTemT4_2;
	assign wTemT5_11 = wTemT4_11 + wTemT4_3;
	assign wTemT5_12 = wTemT4_12 + wTemT4_4;
	assign wTemT5_13 = wTemT4_13 + wTemT4_5;
	assign wTemT5_14 = wTemT4_14 + wTemT4_6;
	assign wTemT5_15 = wTemT4_15 + wTemT4_7;
	 
	
	//Beta Shifting
	assign wTemT6_0 = wTemT5_0;
	assign wTemT6_1 = wTemT5_1;
	assign wTemT6_2 = wTemT5_2;
	assign wTemT6_3 = wTemT5_3;
	assign wTemT6_4 = wTemT5_4;
	assign wTemT6_5 = wTemT5_5;
	assign wTemT6_6 = wTemT5_6;
	assign wTemT6_7 = wTemT5_7;
//	assign wTemT6_8 = iM_j_odd ?  {wTemT5_8[60:0],  wTemT5_8[63:61]}  : {wTemT5_8[4:0], wTemT5_8[63:5]}; //iM_j_odd ? wTemT5[8]<<3 : wTemT5[8]<<59;
	assign wTemT6_8 = iM_j_odd ?  {wTemT5_8[14:0],  wTemT5_8[31:15]}  : {wTemT5_8[30:0], wTemT5_8[31]}; //iM_j_odd ? wTemT5[8]<<17 : wTemT5[8]<<1;
	assign wTemT6_9 = iM_j_odd ?  {wTemT5_9[14:0],  wTemT5_9[31:15]}  : {wTemT5_9[30:0], wTemT5_9[31]};
	assign wTemT6_10 = iM_j_odd ? {wTemT5_10[14:0], wTemT5_10[31:15]} : {wTemT5_10[30:0], wTemT5_10[31]};
	assign wTemT6_11 = iM_j_odd ? {wTemT5_11[14:0], wTemT5_11[31:15]} : {wTemT5_11[30:0], wTemT5_11[31]};
	assign wTemT6_12 = iM_j_odd ? {wTemT5_12[14:0], wTemT5_12[31:15]} : {wTemT5_12[30:0], wTemT5_12[31]};
	assign wTemT6_13 = iM_j_odd ? {wTemT5_13[14:0], wTemT5_13[31:15]} : {wTemT5_13[30:0], wTemT5_13[31]};
	assign wTemT6_14 = iM_j_odd ? {wTemT5_14[14:0], wTemT5_14[31:15]} : {wTemT5_14[30:0], wTemT5_14[31]};
	assign wTemT6_15 = iM_j_odd ? {wTemT5_15[14:0], wTemT5_15[31:15]} : {wTemT5_15[30:0], wTemT5_15[31]};
	
	//3rd Mixing
	assign wTemT7_0 = wTemT6_0 + wTemT6_8;
	assign wTemT7_1 = wTemT6_1 + wTemT6_9;
	assign wTemT7_2 = wTemT6_2 + wTemT6_10;
	assign wTemT7_3 = wTemT6_3 + wTemT6_11;
	assign wTemT7_4 = wTemT6_4 + wTemT6_12;
	assign wTemT7_5 = wTemT6_5 + wTemT6_13;
	assign wTemT7_6 = wTemT6_6 + wTemT6_14;
	assign wTemT7_7 = wTemT6_7 + wTemT6_15;
	assign wTemT7_8 = wTemT6_8;
	assign wTemT7_9 = wTemT6_9;
	assign wTemT7_10 = wTemT6_10;
	assign wTemT7_11 = wTemT6_11;
	assign wTemT7_12 = wTemT6_12;
	assign wTemT7_13 = wTemT6_13;
	assign wTemT7_14 = wTemT6_14;
	assign wTemT7_15 = wTemT6_15;
	 
	//Gamma Shifting
	assign wTemT8_0 = wTemT7_0;
	assign wTemT8_1 = wTemT7_1;
	assign wTemT8_2 = wTemT7_2;
	assign wTemT8_3 = wTemT7_3;
	assign wTemT8_4 = wTemT7_4;
	assign wTemT8_5 = wTemT7_5;
	assign wTemT8_6 = wTemT7_6;
	assign wTemT8_7 = wTemT7_7;
	assign wTemT8_8 = wTemT7_8; //<<0
	assign wTemT8_9 =  {wTemT7_9[23:0],  wTemT7_9[31:24]}; 	//<<8 
	assign wTemT8_10 = {wTemT7_10[15:0], wTemT7_10[31:16]}; 	//<<16
	assign wTemT8_11 = {wTemT7_11[7:0], wTemT7_11[31:8]};	//<<24
	assign wTemT8_12 = {wTemT7_12[7:0], wTemT7_12[31:8]};	//<<24
	assign wTemT8_13 = {wTemT7_13[15:0], wTemT7_13[31:16]};	//<<16
	assign wTemT8_14 = {wTemT7_14[23:0], wTemT7_14[31:24]};	//<<8
	assign wTemT8_15 = wTemT7_15;	//<<0
//========================================================================	
// Initial Setting
//========================================================================
	initial
	begin
	
		rTempDone	<= 1'b0;
		rCnt			<= 8'h0;
		rState		<= S_IDLE;
		
	end

//========================================================================	
// STM
//========================================================================		
	always @(negedge iRset_n or posedge iClk) begin
		if(!iRset_n)	begin
		rTempDone	<= 1'b0;
		rCnt			<= 8'h0;
		rState		<= S_IDLE;
		
		end //if(!iRset_n)	begin
		else 	begin  //  ~ if (!iRset_n)
			case (rState)
				S_IDLE: begin
					if(iReq) begin			//S_INIT		
						rTempDone	<= 1'b0;
						rCnt			<= 8'h0;
						rState		<= S_DELAY;
					end //if(iReq) begin
					else begin 
						rTempDone	<= 1'b0;
						rCnt			<= 8'h0;
						rState		<= S_IDLE;
					end // ~if(iReq) begin
				end //S_IDLE: begin

				S_DELAY: begin
					if(rCnt == COMPLETE_CNT) begin
						rTempDone	<= 1'b1;
						rCnt			<= 8'h0;
						rState		<= S_DONE;				
					end //if(rCnt == COMPLETE_CNT) begin
					else begin
						rTempDone	<= 1'b0;
						rCnt			<= rCnt + 1'b1;
						rState		<= S_DELAY;	
					end // !if(rCnt == COMPLETE_CNT) begin				
				end //S_DELAY: begin
				S_DONE: begin
					rTempDone	<= 1'b0;
					rCnt			<= rCnt;
					rState		<= S_IDLE;
				end //S_DONE: begin
				default: begin
					rTempDone	<= 1'b0;
					rCnt			<= 8'h0;
					rState		<= S_IDLE;
				end //default: begin
			endcase 
		end //begin  //  ~ if (!iRset_n)
	end // always @(negedge iRset_n or posedge iClk)
											
endmodule
