//==================================================================
// Company       : NSRI
// Engineer      : Kim Eun Ji
// Updated Date  : 2018.12.19
// 
// Design Name   : LSH_256(8*w)_256[Word= 32bit, Output = 256bit], PAD(Message) = 1024bit[CF interation: 1st] 
// Module Name   : CFunc.V
// Project Name  : QRNG
//
// Target Devices: Cyclone V SOC (5CSXFC6D6F31C8)
// Tool versions : Quartus Prime 16.1.0 Build 196  10/24/2016
// Revision      : 1.0.0 
//------------------------------------------------------------------
// Notice        :
//==================================================================

module LSH_SingleBlk
(
	input		wire iRset_n,
	input 	    wire iClk,
	input		wire iReq,
	input		wire [31:0] iM0, iM1, iM2, iM3, iM4, iM5, iM6, iM7, iM8, iM9, iM10, iM11, iM12, iM13, iM14, iM15,
	input		wire [31:0] iM16, iM17, iM18, iM19, iM20, iM21, iM22, iM23, iM24, iM25, iM26, iM27, iM28, iM29, iM30, iM31,
	
	output	wire [255:0] oHashValue,
	output	wire oDone
);

//========================================================================	
// Parameter Declaration
//========================================================================


//========================================================================	
// Internal Signals
//========================================================================

	// wire	
	wire [31:0] wT0, wT1, wT2, wT3, wT4, wT5, wT6, wT7, wT8, wT9, wT10, wT11, wT12, wT13, wT14, wT15;
	wire wCFun_done;

	wire wFinalFunc_done;
	wire [255:0] wHashValue;
	
//========================================================================
//  Output Assignment
//========================================================================
	
	assign oHashValue = wHashValue;
	assign oDone	= wFinalFunc_done;
	
//========================================================================
//  Inner Assignment
//========================================================================


	
//========================================================================
//  External Modules: U1
//========================================================================
	CFunc U1
(	
	.iRset_n(iRset_n),
	.iClk(iClk),
	.iReq(iReq),
	.iM0(iM0), 
	.iM1(iM1), 
	.iM2(iM2), 
	.iM3(iM3), 
	.iM4(iM4), 
	.iM5(iM5), 
	.iM6(iM6), 
	.iM7(iM7), 
	.iM8(iM8), 
	.iM9(iM9), 
	.iM10(iM10), 
	.iM11(iM11), 
	.iM12(iM12), 
	.iM13(iM13), 
	.iM14(iM14), 
	.iM15(iM15), 
	
	.iM16(iM16), 
	.iM17(iM17), 
	.iM18(iM18), 
	.iM19(iM19), 
	.iM20(iM20), 
	.iM21(iM21), 
	.iM22(iM22), 
	.iM23(iM23), 
	.iM24(iM24), 
	.iM25(iM25), 
	.iM26(iM26), 
	.iM27(iM27), 
	.iM28(iM28), 
	.iM29(iM29), 
	.iM30(iM30), 
	.iM31(iM31), 
	
	.iCV0(32'h46a10f1f),
	.iCV1(32'hfddce486),
	.iCV2(32'hb41443a8),
	.iCV3(32'h198e6b9d),
	.iCV4(32'h3304388d),
	.iCV5(32'hb0f5a3c7),
	.iCV6(32'hb36061c4),
	.iCV7(32'h7adbd553),
	.iCV8(32'h105d5378),
	.iCV9(32'h2f74de54),
	.iCV10(32'h5c2f2d95),
	.iCV11(32'hf2553fbe),
	.iCV12(32'h8051357a),
	.iCV13(32'h138668c8),
	.iCV14(32'h47aa4484),
	.iCV15(32'he01afb41),
	
	
	.oCV0(wT0), .oCV1(wT1), .oCV2(wT2), .oCV3(wT3), .oCV4(wT4), .oCV5(wT5), .oCV6(wT6), 
	.oCV7(wT7), .oCV8(wT8), .oCV9(wT9), .oCV10(wT10), .oCV11(wT11), .oCV12(wT12), .oCV13(wT13), .oCV14(wT14), .oCV15(wT15),
//	.oState(wCFuncState),
	.oDone(wCFun_done)
);

//========================================================================
//  External Modules: U2
//========================================================================
	FinalFunc U2
(
	.iRset_n(iRset_n),
	.iClk(iClk),
	.iReq(wCFun_done),
	.iCV0(wT0), .iCV1(wT1), .iCV2(wT2), .iCV3(wT3), .iCV4(wT4), .iCV5(wT5), .iCV6(wT6), .iCV7(wT7), 
	.iCV8(wT8), .iCV9(wT9), .iCV10(wT10), .iCV11(wT11), .iCV12(wT12), .iCV13(wT13), .iCV14(wT14), .iCV15(wT15),	
	.oHashValue(wHashValue),
	.oDone(wFinalFunc_done)
);

endmodule
