`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: HURA
// Engineer: LEE Seong Yun (sylee@hura.co.kr)
//
// Create Date: 2019/01/16 11:05:02
// Design Name: QKD
// Module Name: laser_pm_gen
// Project Name: QDC
// Target Devices: Kintex 7 160T
// Tool Versions: Vivado 2018.1
// Description:
//
// Dependencies:
//
// Revision:
// 		build. 1 (2019/01/16)
// Additional Comments:
//
// Copyright 2018, All rights reserved by HURA
//////////////////////////////////////////////////////////////////////////////////
module laser_trig_gen_1200_b (

    input wire sig_clk,
	input wire clk_200,		// 
	input wire clk_600,		// 
	input wire sclr,
	// delay tap 78ps per tap (max 2.418ns)

	input wire [4:0]		delay_tap_in,		// set false path,  the mount of output fine delay. 
	
	input wire				shape_load, // When the man enther the width, addwidht, offset, shape_load is 1. Atfer 1 clk, the shape_load goes to 0.
	input wire [6:0] 		i_offset_quotient,			// set false path 
	input wire [2:0]		i_offset_remainder,	
	input wire [5:0] 		pulse_width,  // the mount of output pulse width, If width is 2, the pulse width is 2 clk width.
	input wire [7:0] 		pulse_add_width, // when the trig. signal is generated, the mount of added output pulse width
                                             // when the clock and trig signal input together and add_width is 3, the pulse is delayed 3clock more.
	input wire				test_mode, // test mode : 0 => output the pwm(trig. & CLK) data, test mode : 1 => Reapeat & Output the 32bit data
	                                   // if the user pattern is checked in OP, the testmode is 1,
	input wire [31:0]		din_test, // user input data

	input wire				din_pulseadd, // when the 10MHz siganl and trig signal are 1, din_pulseadd is 1.
	input wire				din_pulse,   // when the 10MHz siganl is 1, din_puls is 1.

	output wire 			dout_p,
	output wire				dout_n,
	

	output wire [4:0]		delay_tap_out		// set false path

);
// ------------------------------------------------------------------
	// The part that updates the bit contents of testdata by 1 bit every 100ns
localparam _10M_GEN_CNT = 5'd20;
reg [4:0]		_10M_gen_cnt = 5'd20; // 20 count -> 10MHz at 200MHz 
reg [31:0] 		shift_reg;

reg				test_pulseadd;

always @(posedge clk_200) begin
	if (sclr) begin
		_10M_gen_cnt			<= 5'd20;
		test_pulseadd	<= 1'b0;
		shift_reg		<= 32'b0;
	end
	else begin
		if (shape_load) begin
			_10M_gen_cnt 				<= 'd1;
			test_pulseadd		<= 1'b0;
			shift_reg			<= din_test;
		end
		else if (_10M_gen_cnt == 0) begin
			_10M_gen_cnt			<= _10M_GEN_CNT - 1'b1;
			shift_reg		<= {shift_reg[30:0],shift_reg[31]};
			test_pulseadd	<= shift_reg[31];
		end
		else begin
			_10M_gen_cnt	<= _10M_gen_cnt - 1'b1;
			shift_reg		<= shift_reg;
			test_pulseadd	<= test_pulseadd;
		end
	end
end

// ------------------------------------------------------------------
	// The part reflecting offset and width
localparam READY = 2'b00;
localparam RUN = 2'b01;

reg [119:0] p_wid_strt	= 120'b0;
reg [119:0] p_widadd_strt 	= 120'b1;
reg [2:0] p_offset_low = 3'b0;
reg [6:0] p_offset_high = 3'b0;

always @(posedge clk_200) begin	
	if (sclr) begin
	    p_offset_low	<= 'b0;
		p_offset_high	<= 'b0;
		p_wid_strt		 	<= 120'b0;
		p_widadd_strt		<= 'b1;
	end
	else begin
		if (shape_load) begin
                        p_offset_high    <= i_offset_quotient[6:0];
                        p_offset_low    <= i_offset_remainder[2:0];
                        p_wid_strt <= (pulse_width=='b0) ? 120'b0 : ~(120'hff_ffff_ffff_ffff_ffff_ffff_ffff_ffff << pulse_width);
                        p_widadd_strt <= (pulse_width=='b0) ? 120'b1 : ~(120'hff_ffff_ffff_ffff_ffff_ffff_ffff_ffff << (pulse_width+pulse_add_width));
                    end
            else begin
                p_offset_high    <= p_offset_high;
                p_offset_low    <= p_offset_low;
                p_wid_strt         <= p_wid_strt;
                p_widadd_strt    <= p_widadd_strt;                
            end    
	
	end
end

// ------------------------------------------------------------------
	// The part reflecting offset and width

reg [1:0] state = 2'b00;
reg [4:0] pwm_perid =5'd19;

wire 	pwm_pulse;
wire empty1;
fifod_1x16 fifod_pwm_pulse (
  .rst(sclr),				// I
  .wr_clk(sig_clk),		// I
  .rd_clk(clk_200),				// I
  .din(din_pulse),				// I
  .wr_en(din_pulse),			// I
  .rd_en(~empty1),			// I
  .dout(),			// O
  .full(),					// O
  .empty(empty1),   		// O
  .valid(pwm_pulse)     		// O
);

wire 	pwm_pulseadd;

wire   pulseadd;
assign	pulseadd	= (test_mode) ?   test_pulseadd : din_pulseadd;
wire   pulseadd_d;
wire   pulseadd_d_v;

assign pwm_pulseadd = pulseadd_d & pulseadd_d_v;
wire empty2;
fifod_1x16 fifod_pwm_pulseadd (
  .rst(sclr),				// I
  .wr_clk(sig_clk),		// I
  .rd_clk(clk_200),				// I
  .din(pulseadd),				// I
  .wr_en(din_pulse),			// I
  .rd_en(~empty2),			// I
  .dout(pulseadd_d),			// O
  .full(),					// O
  .empty(empty2),   		// O
  .valid(pulseadd_d_v)     		// O
);
reg [119:0] clk_ser 		= 120'b0;

always @(posedge clk_200) begin	
	if (sclr) begin
		clk_ser <=120'b0;
		pwm_perid =5'd19;
		state<=READY;

	end
	else begin
            if(shape_load) begin
                state<=READY;
                clk_ser <=120'b0;
                pwm_perid =5'd19;
            end
            else begin
                 case(state)
					   READY : begin
						if(pwm_pulse) begin
						 clk_ser <=(pwm_pulseadd)? (p_widadd_strt<<p_offset_low):(p_wid_strt<<p_offset_low) ;
						 state <= RUN;
				
							end                    
						else begin    
							clk_ser <=120'b0;

							end
						end  
						
						RUN : begin
							if(pwm_perid == 1) begin
								clk_ser <= {clk_ser[5:0],clk_ser[119:6]};
								state <= READY;
								pwm_perid <= 19;
							end
							else begin
								clk_ser <= {clk_ser[5:0],clk_ser[119:6]};
								pwm_perid <= pwm_perid-1;
							end
						end    
					endcase
               end
    end
            
end

			
wire [5:0] serdes_in;
wire [5:0] serdes_in_d;
assign serdes_in = clk_ser[5:0];

sr_6x128 sr_6x128 (
  .A(p_offset_high),	// I [6 : 0]
  .D(serdes_in),		// I [5 : 0]
  .CLK(clk_200),		// I
  .Q(serdes_in_d)		// O [5 : 0]
);



wire del_rst;
assign del_rst = delay_tap_out != delay_tap_in;

selectio_6to1_dc selectio_6to1_dc
 (
   .data_out_from_device(serdes_in_d),		// I [5:0]
   .data_out_to_pins_p(dout_p),						// O
   .data_out_to_pins_n(dout_n),						// O
   .out_delay_reset(del_rst),							// I
   .out_delay_data_ce(1'b0),						// I
   .out_delay_data_inc(1'b0),						// I
   .out_delay_tap_in(delay_tap_in),					// I [4:0]
   .out_delay_tap_out(delay_tap_out),				// O [4:0]

//   .delay_locked(delay_locked),						// O
 //  .ref_clock(delay_ref_clk),						// I
   .clk_in(clk_600),								// I
   .clk_div_in(clk_200), 							// I
   .io_reset(sclr)									// I
);

endmodule