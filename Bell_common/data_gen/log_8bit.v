`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: HURA
// Engineer: LEE Seong Yun (sylee@hura.co.kr)
//
// Create Date: 2019/01/04 11:05:02
// Design Name: log_8bit
// Module Name: log_8bit
// Project Name: QKD
// Target Devices: Kintex 7 160T
// Tool Versions: Vivado 2018.1
// Description:
//
// Dependencies:
//
// Revision:
// 		build. 1 (2019/01/04)
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////
module log_8bit (
	input wire 			clk,		// sig_clk
	input wire 			rst,

	input wire			din_log0,
	input wire			div_log0,
	input wire			din_log1,
	input wire			div_log1,
	input wire			din_log2,
	input wire			div_log2,
	input wire			din_log3,
	input wire			div_log3,
//	input wire			din_log4,
//	input wire			div_log4,
//	input wire			din_log5,
//	input wire			div_log5,
//	input wire			din_log6,
//	input wire			div_log6,
//	input wire			din_log7,
//	input wire			div_log7,
//	input wire			log_run,

    output wire	[7:0]	emp_log,

	output wire [31:0]	 do_log_16to32,
	output wire			 dov_log_16to32,
    output wire [127:0] do_log_64to128,
    output wire 		 dov_log_64to128
//	input wire			dor_log

);

//wire	[7:0] 	emp_log;
reg				log_re;
wire			do_log0;
(* mark_debug = "true" *)wire			dov_log0;
wire			do_log1;
wire			dov_log1;
wire			do_log2;
wire			do_log3;
/*
wire			do_log4;
wire			do_log5;
wire			do_log6;
wire			do_log7;
*/


fifod_1x16 log0 (
  .rst		(rst),			// I
  .wr_clk	(clk),			// I
  .rd_clk	(clk),			// I
  .din		(din_log0),		// I
  .wr_en	(div_log0),		// I
  .rd_en	(log_re),		// I
  .dout		(do_log0),		// O
  .full		(),				// O
  .empty	(emp_log[0]),	// O
  .valid	(dov_log0)     	// O
);

fifod_1x16 log1 (
  .rst		(rst),			// I
  .wr_clk	(clk),			// I
  .rd_clk	(clk),			// I
  .din		(din_log1),		// I
  .wr_en	(div_log1),		// I
  .rd_en	(log_re),		// I
  .dout		(do_log1),		// O
  .full		(),				// O
  .empty	(emp_log[1]),		// O
  .valid	(dov_log1)     	// O
);

fifod_1x16 log2 (
  .rst		(rst),			// I
  .wr_clk	(clk),			// I
  .rd_clk	(clk),			// I
  .din		(din_log2),		// I
  .wr_en	(div_log2),		// I
  .rd_en	(log_re),		// I
  .dout		(do_log2),		// O
  .full		(),				// O
  .empty	(emp_log[2]),	// O
  .valid	()				// O
);

fifod_1x16 log3 (
  .rst		(rst),			// I
  .wr_clk	(clk),			// I
  .rd_clk	(clk),			// I
  .din		(din_log3),		// I
  .wr_en	(div_log3),		// I
  .rd_en	(log_re),		// I
  .dout		(do_log3),		// O
  .full		(),				// O
  .empty	(emp_log[3]),	// O
  .valid	()				// O
);

/*
fifod_1x16 log4 (
  .rst		(rst),			// I
  .wr_clk	(clk),			// I
  .rd_clk	(clk),			// I
  .din		(din_log4),		// I
  .wr_en	(div_log4),		// I
  .rd_en	(log_re),		// I
  .dout		(do_log4),		// O
  .full		(),				// O
  .empty	(emp_log[4]),	// O
  .valid	()     			// O
);



fifod_1x16 log5 (
  .rst		(rst),			// I
  .wr_clk	(clk),			// I
  .rd_clk	(clk),			// I
  .din		(din_log5),		// I
  .wr_en	(div_log5),		// I
  .rd_en	(log_re),		// I
  .dout		(do_log5),		// O
  .full		(),				// O
  .empty	(emp_log[5]),	// O
  .valid	()     			// O
);

fifod_1x16 log6 (
  .rst		(rst),			// I
  .wr_clk	(clk),			// I
  .rd_clk	(clk),			// I
  .din		(din_log6),		// I
  .wr_en	(div_log6),		// I
  .rd_en	(log_re),		// I
  .dout		(do_log6),		// O
  .full		(),				// O
  .empty	(emp_log[6]),	// O
  .valid	()				// O
);

fifod_1x16 log7 (
  .rst		(rst),			// I
  .wr_clk	(clk),			// I
  .rd_clk	(clk),			// I
  .din		(din_log7),		// I
  .wr_en	(div_log7),		// I
  .rd_en	(log_re),		// I
  .dout		(do_log7),		// O
  .full		(),				// O
  .empty	(emp_log[7]),	// O
  .valid	()				// O
);


*/


// ---------------------------------------------------------------------------------
always @(posedge clk) begin
	if (emp_log == 8'b0)
		log_re <= 1'b1;
	else begin
		log_re <= 1'b0;
/* 		if (emp_log != 8'hff) begin
			// warning
		end */
	end
end

/* reg last_flag;
reg log_run_d;
reg [1:0] cnt;
always @(posedge clk) begin
	log_run_d <= log_run;
	if (last_flag) begin
		cnt <= cnt + 1'b1;
		if (cnt == 2'b11) last_flag <= 1'b0;
	end
	else begin
		last_flag <= (~log_run && log_run_d) ? 1'b1 : last_flag;
		cnt <= 2'b0;
	end

end */


// ---------------------------------------------------------------------------------
// log

(* mark_debug = "true" *)wire [3:0] din_log_4to8;
(* mark_debug = "true" *)wire [7:0] do_log_4to8;
(* mark_debug = "true" *)wire 		dov_log_4to8;
wire [15:0]	do_log_8to16;
wire 		dov_log_8to16;
wire [63:0] do_log_32to64;
wire 		dov_log_32to64;

assign 		din_log_4to8  = {do_log3,do_log2,do_log1,do_log0};



data_merge2to1 #(.NBITS(4))			// Nbits -> 2Nbits  ND -> 2ND
log_4to8(
    .clk(clk),  						// I
    .sclr(rst),							// I
    .din(din_log_4to8),				// I [NBIT-1:0]
    .din_valid(dov_log0),  		// I		// || last_flag
	.endian(1'b0),						// I // 0 : little endian, 1 : big endian
	.dout(do_log_4to8),   				// O [2*NBIT-1:0] {[NBIT-1:0],[NBIT-1:0]}
	.dout_valid(dov_log_4to8)			// O
);

data_merge2to1 #(.NBITS(8))			// Nbits -> 2Nbits  ND -> 2ND
log_8to16(
    .clk(clk),  						// I
    .sclr(rst),							// I
    .din(do_log_4to8),				// I [NBIT-1:0]
    .din_valid(dov_log_4to8),  		// I		// || last_flag
	.endian(1'b0),						// I // 0 : little endian, 1 : big endian
	.dout(do_log_8to16),   				// O [2*NBIT-1:0] {[NBIT-1:0],[NBIT-1:0]}
	.dout_valid(dov_log_8to16)			// O
);

data_merge2to1 #(.NBITS(16))			// Nbits -> 2Nbits  ND -> 2ND
log_16to32(
    .clk(clk),  						// I
    .sclr(rst),							// I
    .din(do_log_8to16),					// I [NBIT-1:0]
    .din_valid(dov_log_8to16),  		// I
	.endian(1'b0),						// I // 0 : little endian, 1 : big endian
	.dout(do_log_16to32),   					// O [2*NBIT-1:0] {[NBIT-1:0],[NBIT-1:0]}
	.dout_valid(dov_log_16to32)				// O
);

data_merge2to1 #(.NBITS(32))			// Nbits -> 2Nbits  ND -> 2ND
log_32to64(
    .clk(clk),  						// I
    .sclr(rst),							// I
    .din(do_log_16to32),					// I [NBIT-1:0]
    .din_valid(dov_log_16to32),  		// I
	.endian(1'b0),						// I // 0 : little endian, 1 : big endian
	.dout(do_log_32to64),   					// O [2*NBIT-1:0] {[NBIT-1:0],[NBIT-1:0]}
	.dout_valid(dov_log_32to64)				// O
);

data_merge2to1 #(.NBITS(64))			// Nbits -> 2Nbits  ND -> 2ND
log_64to128(
    .clk(clk),  						// I
    .sclr(rst),							// I
    .din(do_log_32to64),					// I [NBIT-1:0]
    .din_valid(dov_log_32to64),  		// I
	.endian(1'b0),						// I // 0 : little endian, 1 : big endian
	.dout(do_log_64to128),   					// O [2*NBIT-1:0] {[NBIT-1:0],[NBIT-1:0]}
	.dout_valid(dov_log_64to128)				// O
);

endmodule