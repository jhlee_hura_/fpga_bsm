`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: HURA
// Engineer: LEE JUN HO 
//
// Create Date: 2019/01/04 11:05:02
// Design Name: QKD
// Module Name: data_serialize
// Project Name: QKD
// Target Devices: Kintex 7 160T
// Tool Versions: Vivado 2018.1
// Description:
//
// Dependencies:
//
// Revision:
// 		build. 1 (2019/01/04)
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////
module data_serialize_c (
	input wire clk,				// 100
	input wire rst,
	input wire clk_200,		// 200
	input wire clk_600,		// 600

	//sig_clk domain
	input wire			test_mode,		// clk domain
	input wire [31:0]	din_test,

	input wire			polarity,		// 0 : normal , 1 : negative
	input wire [31:0]	din,
	input wire			din_valid,
	output wire			din_ready,
	
	// sent data
    output wire			do_bit,
    output wire			dov_bit,

	//to pad
	output wire 		dout_p,
	output wire			dout_n,

	input wire			shape_load,
	// pulse width 0.8ns * 63 κΉμ?. (0.8ns ~ 50.4ns)
	input wire [5:0] 	pulse_width,		// set false path
	// delay serdes sample (min 0.8ns, max 819ns)
	input wire [6:0] 	i_offset_quotient,			// set false path 
	input wire	[2:0]	i_offset_remainder,	
	// delay tap 78ps per tap (max 2.418ns)
	input wire [4:0]	delay_tap_in,		// set false path
	output wire [4:0]	delay_tap_out		// set false path

);




wire [31:0] din_fifo;
wire [31:0] dout_fifo;
wire		wr_en;
wire		full;
wire		empty;
wire		dov_fifo;
wire 		dir_dc;

assign din_ready = ~full; //(test_mode) ? 1'b0 : ~full;
assign din_fifo	= (test_mode) ? din_test : din;
assign wr_en	= (test_mode) ? ~full : (din_valid & din_ready);

// @100MHz for log
data_width_downconvert # (
	.DIN_WIDTH(32),
	.DOUT_WIDTH(1),
	.ENDIAN(1) 		// 0 : little endian , 1: big endian
) data_width_downconvert_log (
	.clk(clk),				// I
	.sclr(rst||shape_load),				// I 
	.din(din_fifo),		// I [DIN_WIDTH-1:0]
	.din_valid(wr_en),	// I
	.din_ready(),		// O

	.dout(do_bit),			// O [DOUT_WIDTH-1:0]
	.dout_valid(dov_bit),	// O
	.dout_ready(1'b1)		// I
);
wire rd_en;
assign rd_en = dir_dc && ~empty&& ~dov_fifo;//
// FIFO for clk domain change and word to byte
fifod_32x16 fifod_32x16 (
  .rst(rst||shape_load ),	// I @LJH     || fifo_rst
  .wr_clk(clk),				// I
  .rd_clk(clk_200),		// I
  .din(din_fifo),			// I [31 : 0]
  .wr_en(wr_en),			// I
  .rd_en(rd_en),	// I
  .dout(dout_fifo),			// O [31 : 0]
  .full(full),				// O
  .empty(empty),			// O
  .valid(dov_fifo)			// O
);


wire 	 	dout_dc;
wire		dov_dc;
reg			dor_dc;
data_width_downconvert # (
	.DIN_WIDTH(32),
	.DOUT_WIDTH(1),
	.ENDIAN(1) 		// 0 : little endian , 1: big endian
) data_width_downconvert (
	.clk(clk_200),		// I
	.sclr(rst|| shape_load),				// I , LJH  
	.din(dout_fifo),		// I [DIN_WIDTH-1:0]
	.din_valid(dov_fifo),	// I
	.din_ready(dir_dc),		// O

	.dout(dout_dc),			// O [DOUT_WIDTH-1:0]
	.dout_valid(dov_dc),	// O
	.dout_ready(dor_dc)		// I
);


reg [2:0] p_offset_low = 'b0;
reg [6:0] p_offset_high = 'b0;
reg [63:0] width_pulse = 64'b0;
reg [3:0] p_width 	= 'b1;
reg [119:0] clk_ser = 120'b1;
reg			pulse_flag;
reg			div_bit;

localparam IDLE_STAT = 2'b00;
localparam LOAD_STAT = 2'b01;
localparam OFFSET_STAT = 2'b10;
reg [1:0] stat;
//Code part to output 1bit every 10MHz.
// 0.8333nsec(1bit)* 120 = 100nsec, we want to adjust the width and delay of the data in 0.833nsec increments
// we need 120 registers.
// To do this, we need 1200 MHz(0.8333nsec). But it is high. we try to use the smallest frequency possible. 
// So we use SelectIO 6to1(parallel -> serial) including DDR(Double Data rate) and 600 MHz 
// By using MMCM, I made 600MHz from 100MHz.
// To use Selectio in 1200MHz, we need to put 6bits for selectio.
// 
//serin_clk(6.4nsec) * 15.625 = 100nsec(10MHz)
// 8bit(6.4nsec)  *15.625 = 125 bits(100nsec), 1bit(0.8nsec)
always @(posedge clk_200) begin
	if (rst) begin
	    p_offset_low	<= 'b0;
		p_offset_high	<= 'b0;
		width_pulse 	<= 'b1;
		clk_ser			<= 'b1;
		dor_dc			<= 1'b0;
		pulse_flag		<= 1'b0;
		stat			<= LOAD_STAT;
	end
	else begin
		div_bit <= dor_dc;
		case(stat)
			IDLE_STAT : begin
				if (shape_load || clk_ser == 'b0 ) begin  // 
					clk_ser 	<= 'b0;
					p_offset_high	<= i_offset_quotient[6:0];
					p_offset_low	<= i_offset_remainder[2:0];
					width_pulse <= (pulse_width=='b0) ? 64'b1 : ~(64'hffff_ffff_ffff_ffff << pulse_width);
					stat		<= LOAD_STAT;
				end
				else begin
					clk_ser 	<= {clk_ser[5:0], clk_ser[119:6]};
					pulse_flag 	<= (clk_ser[5:0] == 6'b0) ? 1'b1 : 1'b0;	// ? ??€ ??
					dor_dc 		<= (clk_ser[5:0] != 6'b0 && pulse_flag) ? 1'b1 : 1'b0;		// ??€κ°? ?λ£λλ©? ? ?°?΄?° λ°κΈ°
					stat		<= IDLE_STAT;
				end
			end
			LOAD_STAT : begin
				pulse_flag	<= 1'b0;
				dor_dc		<= 1'b0;
				clk_ser		<= width_pulse << p_offset_low;
				//clk_ser[p_offset+p_width-1'b1:p_offset] <= {p_width{1'b1}};
				stat	<= IDLE_STAT;
			end
		endcase
	end
end

wire din_ff;
assign din_ff = dout_dc & dov_dc;




wire [5:0] serdes_in;
wire [5:0] serdes_in_d;
assign serdes_in = (polarity) ? ~(clk_ser[113:108] & {6{din_ff}}) : (clk_ser[113:108] & {6{din_ff}});
// clk_ser[113:108] is made to match the timing of dordc of IM module and dordc of PM module. The number 113:108 is the number adjusted through simulation to match the timing.
sr_6x128 sr_6x128 (
  .A(p_offset_high),	// I [6 : 0]
  .D(serdes_in),		// I [7 : 0]
  .CLK(clk_200),		// I
  .Q(serdes_in_d)		// O [7 : 0]
);

wire del_rst;
assign del_rst = delay_tap_out != delay_tap_in;
selectio_6to1_dc selectio_6to1_dc
 (
   .data_out_from_device(serdes_in_d),		// I [7:0]
   .data_out_to_pins_p(dout_p),						// O
   .data_out_to_pins_n(dout_n),						// O
   .out_delay_reset(del_rst),							// I
   .out_delay_data_ce(1'b0),						// I
   .out_delay_data_inc(1'b0),						// I
   .out_delay_tap_in(delay_tap_in),					// I [4:0]
   .out_delay_tap_out(delay_tap_out),				// O [4:0]

   .clk_in(clk_600),								// I
   .clk_div_in(clk_200), 							// I
   .io_reset(rst||shape_load)									// I
);

endmodule
