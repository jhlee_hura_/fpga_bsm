`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: HURA
// Engineer: LEE Seong Yun (sylee@hura.co.kr)
//
// Create Date: 2019/01/04 11:05:02
// Design Name: QKD
// Module Name: spad_detect
// Project Name: QKD
// Target Devices: Kintex 7 160T
// Tool Versions: Vivado 2018.1
// Description:
//
// Dependencies:
//
// Revision:
// 		build. 1 (2019/01/04)
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////
module spad_detect_b (
	input wire sig_clk,				// 100
	input wire rst,
	input wire clk_200,				// 100
	input wire clk_600,		// 700

	//to pad
	input wire 			din_p,
	input wire			din_n,
	
 	input wire [4:0]	delay_tap_in,		// set false path
    output wire [4:0]    delay_tap_out,        // set false path
   
	output 	wire		dout_trig		// clk domain
//	output	wire		dov_trig
);

(* mark_debug = "true" *)wire [5:0] trig_byte;			//LSB first
wire del_rst;
assign del_rst = delay_tap_out != delay_tap_in;
selectio_1to6 selectio_1to6
 (
   .data_in_from_pins_p(din_p),			// I
   .data_in_from_pins_n(din_n),			// I
   .clk_in(clk_600), 				// I
   .clk_div_in(clk_200),                 // I   
   .io_reset(rst), // input io_reset     
   .in_delay_reset(del_rst),// I
   .in_delay_tap_in(delay_tap_in), // I [4:0]
   .in_delay_data_ce(1'b0), // I
   .in_delay_data_inc(1'b0), // I  
   .bitslip(1'b0), 						// I  
   .in_delay_tap_out(delay_tap_out),// O[4:0]   
   .data_in_to_device(trig_byte)		// O [5:0]    
);

wire [5:0] trig_flag;
reg  trig_old;

always @(posedge clk_200) begin
	trig_old <= trig_byte[5];
end

// rising edge detect
assign trig_flag[0] =	(trig_old == 1'b0 && trig_byte[0]);
assign trig_flag[1] =	(trig_byte[1:0] == 2'b10);
assign trig_flag[2] =	(trig_byte[2:1] == 2'b10);
assign trig_flag[3] =	(trig_byte[3:2] == 2'b10);
assign trig_flag[4] =	(trig_byte[4:3] == 2'b10);
assign trig_flag[5] =	(trig_byte[5:4] == 2'b10);


(* mark_debug = "true" *)wire din_fifo;
wire empty;
wire do_trig;
assign din_fifo = (trig_flag != 6'b0);
wire dov_trig;
fifod_1x16 fifod_1x16 (
  .rst(rst),				// I
  .wr_clk(clk_200),		// I
  .rd_clk(sig_clk),				// I
  .din(din_fifo),			// I
  .wr_en(din_fifo),			// I
  .rd_en(~empty),			// I
  .dout(do_trig),			// O
  .full(),					// O
  .empty(empty),   			// O
  .valid(dov_trig)     		// O
);

assign dout_trig = do_trig & dov_trig;

endmodule
