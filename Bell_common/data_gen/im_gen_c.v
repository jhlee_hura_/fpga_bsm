`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: HURA
// Engineer: LEE Seong Yun (sylee@hura.co.kr)
//
// Create Date: 2019/01/04 11:05:02
// Design Name: QKD
// Module Name: im_gen
// Project Name: QKD
// Target Devices: Kintex 7 160T
// Tool Versions: Vivado 2018.1
// Description:
//
// Dependencies:
//
// Revision:
// 		build. 1 (2019/01/04)
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////
module im_gen_c (
	input wire clk,				// 100 MHz
	input wire clk_200,			// 200 MHz
	input wire rst,

	//sig_clk domain
	input wire			test_mode,		// clk domain
	input wire [63:0]	din_test,

	input wire [63:0]	din,
	input wire			din_valid,
	output wire			din_ready,
	// sent data
	output wire	[1:0]	do_bit,
	output wire			dov_bit,

	//to pad
	output wire [11:0]	dout_dac,
	output wire			dout_clk,

	input wire			shape_load,			// set false path
	// pulse width 0.8ns * 15 源뚯?. (0.8ns ~ 12ns)
	input wire [3:0] 	pulse_width,		// set false path
	// delay serdes sample (min 0.8ns, max 209.7144us)
	input wire	[8:0]	pulse_offset,		// set false path // serout_clk ???��?

	input	wire [11:0] i_dac_v0,
	input	wire [11:0] i_dac_v1_p,
	input	wire [11:0] i_dac_v1_n,
	input	wire [11:0] i_dac_v2_p,
	input	wire [11:0] i_dac_v2_n,	
	input	wire [11:0] i_dac_v3_p,
	input	wire [11:0] i_dac_v3_n
);


// ?몢媛?�?? 紐⑤뱶濡? ?�???��
// test_mode : 0 : ??��?뻾紐?���? 1: test_mode
// ?????��??�� 紐⑤�? : 1 word(=32bits)?��? ?�꾩?�� 諛섎?�� ?��?��?��?�???�� 紐⑤�?
// ??��?�? 紐⑤�? : Random Gen?�??�? 32bits data?��? 諛쏆�??�? ?�???��?�???�� 紐⑤�?

wire [63:0] din_fifo;
wire [63:0] dout_fifo;
wire		wr_en;
wire		full;
wire		empty;
wire		dov_fifo;
wire 		dir_dc;

assign din_ready = (test_mode) ? 1'b0 : ~full;
assign din_fifo	= (test_mode) ? din_test : din;
assign wr_en	= (test_mode) ? ~full : (din_valid & din_ready);

// @100MHz for log

data_width_downconvert # (
	.DIN_WIDTH(64),
	.DOUT_WIDTH(2),
	.ENDIAN(1) 		// 0 : little endian , 1: big endian
) data_width_downconvert_log (
	.clk(clk),				// I
	.sclr(rst|| shape_load),				// I 
	.din(din_fifo),		// I [DIN_WIDTH-1:0]
	.din_valid(wr_en),	// I
	.din_ready(),		// O

	.dout(do_bit),			// O [DOUT_WIDTH-1:0]
	.dout_valid(dov_bit),	// O
	.dout_ready(1'b1)		// I
);

// FIFO for clk domain change and word to byte
wire rd_en;
assign rd_en = dir_dc && ~empty&& ~dov_fifo;//
fifod_32x16 fifod_32x16_0 (
  .rst(rst|| shape_load),	// I
  .wr_clk(clk),				// I
  .rd_clk(clk_200),		// I
  .din(din_fifo[31:0]),		// I [31 : 0]
  .wr_en(wr_en),			// I
  .rd_en(rd_en),	// I
  .dout(dout_fifo[31:0]),	// O [31 : 0]
  .full(full),				// O
  .empty(empty),			// O
  .valid(dov_fifo)			// O
);
fifod_32x16 fifod_32x16_1 (
  .rst(rst|| shape_load),	// I
  .wr_clk(clk),				// I
  .rd_clk(clk_200),				// I
  .din(din_fifo[63:32]),	// I [31 : 0]
  .wr_en(wr_en),			// I
  .rd_en(rd_en),	// I
  .dout(dout_fifo[63:32]),	// O [31 : 0]
  .full(),					// O
  .empty(),					// O
  .valid()					// O
);



// @200MHz for signal gen.
wire [1:0] 	dout_dc;
wire		dov_dc;
wire		dor_dc;
data_width_downconvert # (
	.DIN_WIDTH(64),
	.DOUT_WIDTH(2),
	.ENDIAN(1) 		// 0 : little endian , 1: big endian
) data_width_downconvert (
	.clk(clk_200),				// I
	.sclr(rst || shape_load),				// I  ->> Need shape load 400MHz 
	.din(dout_fifo),		// I [DIN_WIDTH-1:0]
	.din_valid(dov_fifo),	// I
	.din_ready(dir_dc),		// O

	.dout(dout_dc),			// O [DOUT_WIDTH-1:0]
	.dout_valid(dov_dc),	// O
	.dout_ready(dor_dc)		// I
);



reg [4:0]	clk_cnt;
reg [4:0]	clk_cnt_delay_cnt;
reg [4:0]  width_cnt;
reg clk200_valid;
wire [2:0] i_sr;
wire [2:0] i_sr_p;
wire [2:0] i_sr_n;
wire [2:0] o_sr;

localparam V0 = 3'b0;
localparam V1_P = 3'b001;
localparam V1_N = 3'b101;
localparam V2_P = 3'b010;
localparam V2_N = 3'b110;
localparam V3_P = 3'b011;
localparam V3_N = 3'b111;
reg [1:0] stat;
localparam IDLE_STAT = 2'b00;
localparam LOAD_STAT = 2'b01;
assign	dor_dc	= (clk_cnt == 'd19)&(stat==LOAD_STAT) ? 1'b1 : 1'b0;
// Clk_delay_cnt is made to match the timing of dordc of IM module and dordc of PM module. The number 2 is the number adjusted through simulation to match the timing.
always @(posedge clk_200) begin
	if (rst) begin
		//width_pulse 	<= 'b1;
		clk_cnt			<= 'd0;
		clk200_valid    <= 1'b0;
		width_cnt      <= 5'b0;
		clk_cnt_delay_cnt <= 5'b0;
		stat			<= IDLE_STAT;
	end
	else begin
        case(stat)
            IDLE_STAT : begin
                    clk_cnt			<= 'd0;
                    clk_cnt_delay_cnt <= 5'b0;
                    clk_cnt_delay_cnt <= (clk_cnt_delay_cnt == 'd2) ? 0 : clk_cnt_delay_cnt+1;
                    stat  <= (clk_cnt_delay_cnt == 'd2) ? LOAD_STAT : IDLE_STAT;  
           
            end        
            LOAD_STAT : begin
                    clk_cnt <= (clk_cnt == 'd19) ? 'd0: clk_cnt + 1'b1;
                    width_cnt <= (dor_dc) ? 0 : width_cnt+1;
                     //with_pulse <=(dor_dc) ? ~(20'h f_ffff << (pulse_width)<<1) : {width_pulse[0], width_pulse[19:1]};
                    clk200_valid <= ((width_cnt < (pulse_width<<1))& i_sr==3'b0 )? 1'b0 : ((width_cnt < (pulse_width<<1)+1) ?  1'b1 : 1'b0);
                    stat			<= (shape_load) ? IDLE_STAT : LOAD_STAT;
            end
        endcase            
	end
end



assign	i_sr_p =  (dout_dc == 2'b00 ) ? V0 :
				  (dout_dc == 2'b11 ) ? V1_P :
				  (dout_dc == 2'b10 ) ? V2_P :
				  (dout_dc == 2'b01 ) ? V3_P : V0;
				  
assign	i_sr_n =  (dout_dc == 2'b00 ) ? V0 :
				  (dout_dc == 2'b11 ) ? V1_N :
				  (dout_dc == 2'b10 ) ? V2_N :
				  (dout_dc == 2'b01 ) ? V3_N : V0;
				  
assign	i_sr = (width_cnt < (pulse_width)) ? i_sr_p&{dov_dc,dov_dc,dov_dc} : (width_cnt < (pulse_width<<1)) ?i_sr_n&{dov_dc,dov_dc,dov_dc} : 3'b0;
//assign	clk200_valid = ((width_cnt < (pulse_width<<1))& i_sr==3'b0 )? 1'b0 : ((width_cnt < (pulse_width<<1)+2) ?  1'b1 : 1'b0);


sr_3x300 sr_3x300 (
  .A(pulse_offset),	// I [1 : 0]
  .D(i_sr),		// I [2 : 0]
  .CLK(clk_200),		// I
  .Q(o_sr)		// O [1 : 0]
);
wire clk200_valid_delay;
sr_1x300 sr_1x300 (
  .A(pulse_offset),	// I [1 : 0]
  .D(clk200_valid),		// I 
  .CLK(clk_200),		// I
  .Q(clk200_valid_delay)		// O 
);

wire [11:0] dac_out;
assign dout_dac = dac_out;
assign dout_clk = clk_200&clk200_valid_delay;

assign	dac_out = (o_sr == 3'b000 ) ? i_dac_v0 :
					(o_sr == 3'b001 ) ? i_dac_v1_p :
					(o_sr == 3'b010 ) ? i_dac_v2_p :
					(o_sr == 3'b011 ) ? i_dac_v3_p : 
					(o_sr == 3'b101 ) ? i_dac_v1_n :
					(o_sr == 3'b110 ) ? i_dac_v2_n :
					(o_sr == 3'b111 ) ? i_dac_v3_n : i_dac_v0;					

endmodule
