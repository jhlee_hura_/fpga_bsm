//////////////////////////////////////////////////////////////////////////////////
// Company: HURA
// Engineer: LEE Seongyun (sylee@hura.co.kr)
//
// Create Date: 2018/07/11 10:12:00
// Design Name: MIN-MAX-AVG
// Module Name:
// Project Name: QDC
// Target Devices: Kintex7 160T
// Tool Versions: Vivado 2018.1
// Description:
//
// Dependencies:
//
// Revision: build 1(2018/07/11)
// Additional Comments:
//
// Copyright 2018 (c) HURA. All rights reserved.
//////////////////////////////////////////////////////////////////////////////////
module pulse_delay  (
	input wire clk,
	input wire sclr,
	input wire [14:0] i_delay_cnt,
	input wire i_start_pulse,
	input wire i_ref_pulse,
	output reg o_delayed_pulse
);

localparam IDLE = 2'b00;
localparam WAIT = 2'b01;
localparam OUT = 2'b10;
reg [1:0] stat=IDLE;
reg [14:0] delay_cnt;

always @(posedge clk) begin
	if (sclr) begin
		o_delayed_pulse	<= 0;
		stat		<= IDLE;
	end
	else begin
        case (stat)
            IDLE : begin
                stat 	<=(i_start_pulse)? WAIT : IDLE;
                delay_cnt 	<=0 ;
                o_delayed_pulse <= 0;	
            end
            WAIT : begin
                delay_cnt <= (i_ref_pulse) ? delay_cnt +1 : delay_cnt;
                stat <= ( delay_cnt == i_delay_cnt) ? OUT : WAIT;
                o_delayed_pulse <= ( delay_cnt == i_delay_cnt)? 1'b1 : 1'b0;
            end
            OUT : begin
                o_delayed_pulse <=(i_ref_pulse) ? 1'b0 :o_delayed_pulse ;
                stat <=(i_ref_pulse) ? IDLE  : OUT ;
            end
         endcase
    end
end


endmodule
