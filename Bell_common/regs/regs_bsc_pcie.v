`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: HURA
// Engineer: LEE Seong Yun (sylee@hura.co.kr)
//
// Create Date: 2018/03/22 21:04:48
// Design Name: Register MAP for XDMA PCIE
// Module Name: regs_bsc_pcie
// Project Name:
// Target Devices: Any Device
// Tool Versions: Vivado 2018.1
// Description:
//
// Dependencies:
//
// Revision:
//		build 4 (2018/07/25)
// Additional Comments:
//
// Copyright 2018, All rights reserved by HURA
//////////////////////////////////////////////////////////////////////////////////
module regs_bsc_pcie(
	input	wire				pcie_clk,
	input	wire				sclr,

	input	wire				regs_sel,
	input	wire	[46:0]		regs_adda,
	input	wire				regs_adda_valid,
	output	wire	[31:0]		regs_dout,
	output	wire				regs_dout_valid,

	// -------------------------------------------------------
	// -- register ?���?
	// DMA_DATA_SEL_REG
	output reg		[1:0]		data_sel,
	output reg					test_cnt_clr,
	// DMA_FIFO_EMPTY_REG
	input wire					dma_fifo_empty,

	// PCIE_DMA_C2H_0_CTRL_REG
	output	wire				c2h_0_rst,
	// PCIE_DMA_C2H_0_SRCADDR_REG
	output	reg		[31:0]		c2h_0_src_addr = `PCIE_DMA_C2H_SRCADDR_DEF,
	// PCIE_DMA_C2H_0_LEN_REG
	output	reg		[27:0]		c2h_0_len = `DMA_LEN_DEF,
	// PCIE_DMA_C2H_0_STAT_REG
	input wire		[7:0]		c2h_0_sts,

	// PCIE_DMA_C2H_0_BUSY_CNT_REG
	input	wire	[31:0]		c2h_0_busy_clk_cnt,
	// PCIE_DMA_C2H_0_RUN_CNT_REG
	input	wire	[31:0]		c2h_0_run_clk_cnt,
	// PCIE_DMA_C2H_0_PACKET_CNT_REG
	input	wire	[31:0]		c2h_0_packet_cnt,
	// PCIE_DMA_C2H_0_DESC_CNT_REG
	input	wire	[31:0]		c2h_0_desc_cnt,

	// PCIE_DMA_H2C_0_STAT_REG
	input wire		[7:0]		h2c_0_sts,
	
	// DMA_BUFFER_PROG_THRESH	
	output reg     [13:0]      o_dma_buf_prog_thresh,
	// DMA_BUFFER_FULL
    input wire i_dma_buf_progbfull

);
// (* mark_debug = "true" *)
reg				regout_we;
reg				regout_we1;

wire	[15:0]	addr;
wire			wr_en;
wire	[31:0]	wr_d;
reg		[31:0]	rd_d;

assign regs_dout		= rd_d;
assign regs_dout_valid	= regout_we;

always @(posedge pcie_clk) begin
	regout_we1	<= ~regs_adda[46] & (regs_sel & regs_adda_valid);
	regout_we	<= regout_we1;
end

// reg_adda[46]= 1:wr_en, 0:rd_en
// reg_adda[45:32] = 14bits addr
// reg_adda[31:0] = 32bits data
assign wr_en	= regs_adda[46] & (regs_sel & regs_adda_valid);
assign addr		= {regs_adda[45:32], 2'b0};	// 14bits to 16bits address
assign wr_d		= regs_adda[31:0];

reg	c2h_0_rst_r;
always @(posedge pcie_clk ) begin
	if (sclr) begin
		rd_d			<= 32'b0;
		// DMA_DATA_SEL_REG
		data_sel		<= 'b0;
		test_cnt_clr	<= 1'b0;
		// PCIE_DMA_C2H_0_CTRL_REG
		c2h_0_rst_r	<= 1'b0;
		// PCIE_DMA_C2H_0_SRCADDR_REG
		c2h_0_src_addr	<= `PCIE_DMA_C2H_SRCADDR_DEF;
		// PCIE_DMA_C2H_0_LEN_REG
		c2h_0_len			<= `DMA_LEN_DEF;
		// DMA_BUFFER_PROG_THRESH
		o_dma_buf_prog_thresh <= 14'd10000;
	end
	else begin
		case (addr)
			`DMA_DATA_SEL_REG : begin
				rd_d[1:0] <= data_sel;
				rd_d[31:2]	<= 30'b0;
				if (wr_en) begin
					data_sel <= wr_d[1:0];
					test_cnt_clr <= wr_d[8];
				end
			end

			`DMA_FIFO_EMPTY_REG : begin
				rd_d[0] <= dma_fifo_empty;
				rd_d[31:1]	<= 31'b0;
			end

			`PCIE_DMA_C2H_0_CTRL_REG : begin
				rd_d[31:0] <= 32'b0;
				if (wr_en) begin
					c2h_0_rst_r <= wr_d[0];
				end
			end

			`PCIE_DMA_C2H_0_SRCADDR_REG : begin
				rd_d[31:0] <= c2h_0_src_addr;
				if (wr_en) begin
					c2h_0_src_addr <= wr_d[31:0];
				end
			end

			`PCIE_DMA_C2H_0_LEN_REG : begin
				rd_d[27:0] <= c2h_0_len;
				rd_d[31:28]	<= 4'b0;
				if (wr_en) begin
					c2h_0_len <= wr_d[27:0];
				end
			end

			`PCIE_DMA_C2H_0_STAT_REG : begin
				rd_d[7:0]	<= c2h_0_sts;
				rd_d[31:8]	<= 24'b0;
			end

			`PCIE_DMA_C2H_0_BUSY_CNT_REG : begin
				rd_d[31:0]	<= c2h_0_busy_clk_cnt;
			end

			`PCIE_DMA_C2H_0_RUN_CNT_REG : begin
				rd_d[31:0]	<= c2h_0_run_clk_cnt;
			end

			`PCIE_DMA_C2H_0_PACKET_CNT_REG : begin
				rd_d[31:0]	<= c2h_0_packet_cnt;
			end

			`PCIE_DMA_C2H_0_DESC_CNT_REG : begin
				rd_d[31:0]	<= c2h_0_desc_cnt;
			end

			`PCIE_DMA_H2C_0_STAT_REG : begin
				rd_d[7:0]	<= h2c_0_sts;
				rd_d[31:8]	<= 24'b0;
			end

            `DMA_BUFFER_PROG_THRESH : begin
                rd_d[13:0]    <= o_dma_buf_prog_thresh;
                rd_d[31:13]    <= 19'b0;
                if (wr_en) begin
                    o_dma_buf_prog_thresh <= wr_d[13:0];
                end
            end    
             			
			`PCIE_DMA_BUFFER_FULL : begin
                rd_d[0]        <= i_dma_buf_progbfull;
                rd_d[1]        <= 0;
                rd_d[7:2]    <= 6'b0;
                rd_d[8]        <= 0;
                rd_d[10:9]    <= 2'b0;
                rd_d[11]    <= 0;
                rd_d[12]    <= 0;
                rd_d[13]    <= 0;
                rd_d[31:14] <= 'b0;

            end
			default: begin
				rd_d <= 32'b0;
			end
		endcase

		////////////////////////////////////////////////
		// ?��?�� ??�?.

		if (!wr_en) begin
			c2h_0_rst_r <= 1'b0;
			test_cnt_clr <= 1'b0;
		end
	end
end

pls_expander #(
	.COUNT(`C2H_RST_PCIECLK),
	.POLARITY(1)
) c2h_0_rst_ext(
	.sin(c2h_0_rst_r),
	.clk(pcie_clk),
	.sout(c2h_0_rst)
);

endmodule