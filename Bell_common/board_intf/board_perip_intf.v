//////////////////////////////////////////////////////////////////////////////////
// Company: HURA (Hub of Radio Technology)
// Engineer: LEE Seong Yun (sylee@hura.co.kr)
//
// Create Date: 2019/01/11 18:39:00
// Design Name: QKD Common
// Module Name: board_perip_intf
// Project Name: QKD
// Target Devices: Kintex7 160T
// Tool Versions: Vivado 2018.1
// Description:
//
// Dependencies:
//
// Revision: build 1(2019/01/11)
// Additional Comments:
//
// Copyright 2019, All rights reserved by HURA
//////////////////////////////////////////////////////////////////////////////////
module board_perip_intf(
	input wire clk,					// 125Mhz
	input wire sclr,

	// LM70 Temp Sensor
	output wire lm70_sck,
	output wire lm70_scs,
	output wire lm70_sdo,
	input  wire lm70_sdi,

	input  wire 		lm70_din_valid,
	output wire 		lm70_din_ready,
	output wire [15:0] 	lm70_dout,

	// MAIN FAN Control
	output wire			fan_main_ctrl,

	input wire	[15:0]	fan_main_high_dura,
	input wire	[15:0]	fan_main_low_dura,

	// FAN Control
	output wire			fan_ctrl,

	input wire	[15:0]	fan_high_dura,
	input wire	[15:0]	fan_low_dura
);



// ---------------------------------------------------------------------------------
//  LM70 Temperature Sensor
// ---------------------------------------------------------------------------------
wire [15:0] lm_tmp;
 spi_intf #(
	.SCLK_PERIOD(20),				// (몇개 clk으로 구성 )2이상
	.CLK_START_END(10),
	.HIGH_CLK(10),
	.CLK_OFFSET(5),  				// HIGH_CLK+CLK_OFFSET (신호에 비해 HIGH가 밀린량)
	.CONT_WIDTH(0), 				// control bit width
	.DATA_WIDTH(16),
	.RW_FLAG_BIT(15),
	.SCS_N_MODE(0),
	.SCLK_IDLE_MODE(0)
)
 spi_intf_lm70(
	.clk(clk),						// I
	.sclr(sclr),					// I

	.sclk(lm70_sck),				// O
	.scs_n(lm70_scs),				// O
	.sdo(lm70_sdo),					// O
	.sdi(lm70_sdi),					// I
	.iobuf_T(),						// O //not used for 4line SPI

	.din(16'h8000),					// I [WORD_WIDTH-1:0]
	.din_valid(lm70_din_valid),		// I
	.din_ready(lm70_din_ready),		// O
	.dout(lm_tmp),					// O [WORD_WIDTH-1:0]
	.dout_valid()					// O
);

assign lm70_dout={lm_tmp[15],lm_tmp[15],lm_tmp[15],lm_tmp[15],lm_tmp[15],lm_tmp[15:5]};



// ---------------------------------------------------------------------------------
//  Fan speed Control (PWM)
// ---------------------------------------------------------------------------------
pwm #(
	.MAX_CLK_CNT_BIT(16)
)
 pwm1(
	.clk(clk),					// I
	.sclr(sclr),				// I

	.high_dura(fan_high_dura),	// I [MAX_CLK_CNT_BIT-1:0]
	.low_dura(fan_low_dura),	// I [MAX_CLK_CNT_BIT-1:0]

	.pwm_out(fan_ctrl)			// O
); //total latency


// ---------------------------------------------------------------------------------
//  Fan speed Control (PWM)
// ---------------------------------------------------------------------------------
pwm #(
	.MAX_CLK_CNT_BIT(16)
)
 pwm2(
	.clk(clk),						// I
	.sclr(sclr),					// I

	.high_dura(fan_main_high_dura),	// I [MAX_CLK_CNT_BIT-1:0]
	.low_dura(fan_main_low_dura),	// I [MAX_CLK_CNT_BIT-1:0]

	.pwm_out(fan_main_ctrl)			// O
); //total latency



endmodule
