`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: HURA
// Engineer: LEE Seong Yun (sylee@hura.co.kr)
//
// Create Date: 2018/03/22 21:04:48
// Design Name: register for MARS X1 Board Support Configuration(BSC)
// Module Name: regs_bsc_sys
// Project Name: HURA MARS X1 EVM
// Target Devices: ku060-ffva1156, MARS X1 board
// Tool Versions: Vivado 2018.1
// Description:
//
// Dependencies:
//
// Revision: build. 1 (2018/08/09)
//
// Additional Comments:
//
// Copyright 2018, All rights reserved by HURA
//////////////////////////////////////////////////////////////////////////////////
module pcie_dma_data_sel #(
	parameter	DATA_WIDTH = 128,
	localparam	KEEP_WIDTH = DATA_WIDTH / 8,	
    localparam	WIDTHPERWORD = DATA_WIDTH / 32
) (
	input wire 	pcie_clk,		// pcie-clk
	input wire	sclr,

 (* mark_debug = "true" *)	input wire			test_cnt_clr,
 (* mark_debug = "true" *)	input wire	[1:0] 	data_sel,

	// from FIFO
 (* mark_debug = "true" *)	input wire	[DATA_WIDTH-1:0]	din_fifo_tdata,
 (* mark_debug = "true" *)	input wire	[KEEP_WIDTH-1:0]	din_fifo_tkeep,
 (* mark_debug = "true" *)	input wire						din_fifo_tvalid,
 (* mark_debug = "true" *)	input wire						din_fifo_tlast,
 (* mark_debug = "true" *)	output wire						din_fifo_tready,
	
	// to PCIE Axi streaming
 (* mark_debug = "true" *)	output wire [DATA_WIDTH-1:0]	to_pcie_tdata,
 (* mark_debug = "true" *)	output wire [KEEP_WIDTH-1:0]	to_pcie_tkeep,
 (* mark_debug = "true" *)	output wire						to_pcie_tlast,
 (* mark_debug = "true" *)	output wire						to_pcie_tvalid,
 (* mark_debug = "true" *)	input wire						to_pcie_tready
);
// (* mark_debug = "true" *)



wire [DATA_WIDTH-1:0] 	tmp_data;

reg [31:0] 	test_count=32'b0;

reg [DATA_WIDTH-1:0] 	test_tdata;
reg [KEEP_WIDTH-1:0]	test_tkeep;
reg						test_tlast;
reg						test_tvalid;

// switch
// MIG FIFO?��?�� FWFT �? 맞춰?�� �? assign?���?
assign to_pcie_tdata	=  (data_sel==`PCIE_DMA_SRC_FIFO) ? din_fifo_tdata 	:  test_tdata;
assign to_pcie_tkeep	=  (data_sel==`PCIE_DMA_SRC_FIFO) ? din_fifo_tkeep 	:  test_tkeep;
assign to_pcie_tlast	=  (data_sel==`PCIE_DMA_SRC_FIFO) ? din_fifo_tlast 	:  test_tlast;
assign to_pcie_tvalid	= (data_sel==`PCIE_DMA_SRC_FIFO) ? din_fifo_tvalid 	:  test_tvalid;
assign din_fifo_tready	= (data_sel==`PCIE_DMA_SRC_FIFO) ? to_pcie_tready 	: 1'b0;



// Test pattern
always @(posedge pcie_clk)	begin
	if (sclr) begin
		test_count 	<= 32'b0;
		test_tdata <= 'b0;
		test_tkeep <= 'b0;
		test_tlast <= 1'b0;
		test_tvalid <= 1'b0;
	end
	else begin
		case (data_sel)
			2'b0 : begin
				if (to_pcie_tready) begin
					test_count		<= (test_cnt_clr) ? 32'b0 : test_count + WIDTHPERWORD;
					test_tdata		<= tmp_data;
					test_tkeep		<= {KEEP_WIDTH{1'b1}};
					test_tlast		<= 1'b0;
					test_tvalid 	<= 1'b1;
				end
			end

			2'b1 : begin
				if (to_pcie_tready) begin
					test_count		<= (test_cnt_clr) ? 32'b0 : test_count + 'b1;
					test_tdata		<= {WIDTHPERWORD{test_count}};
					test_tkeep		<= {KEEP_WIDTH{1'b1}};
					test_tlast		<= 1'b0;
					test_tvalid 	<= 1'b1;
				end
			end
			default : begin
				if (to_pcie_tready) begin
					test_count		<= test_count;
					test_tdata		<= test_tdata;
					test_tkeep		<= test_tkeep;
					test_tlast		<= 1'b0;
					test_tvalid 	<= 1'b0;
				end

			end

		endcase
	end
end









generate
if (WIDTHPERWORD == 1) begin
	assign tmp_data	= test_count;
end
else if (WIDTHPERWORD == 2) begin
	assign tmp_data	= {test_count + 1, test_count};
end
else if (WIDTHPERWORD == 4) begin
	assign tmp_data	= {test_count + 3, test_count + 2, test_count + 1, test_count};
end
else if (WIDTHPERWORD == 8) begin
	assign tmp_data	= { test_count + 7, test_count + 6, test_count + 5, test_count + 4,
						test_count + 3, test_count + 2, test_count + 1, test_count};
end
else if (WIDTHPERWORD == 16) begin
	assign tmp_data	= { test_count + 15, test_count + 14, test_count + 13, test_count + 12,
						test_count + 11, test_count + 10, test_count + 9, test_count + 8,
						test_count + 7, test_count + 6, test_count + 5, test_count + 4,
						test_count + 3, test_count + 2, test_count + 1, test_count};
end
else if (WIDTHPERWORD == 32) begin
	assign tmp_data	= { test_count + 31, test_count + 30, test_count + 29, test_count + 28,
						test_count + 27, test_count + 26, test_count + 25, test_count + 24,
						test_count + 23, test_count + 22, test_count + 21, test_count + 20,
						test_count + 19, test_count + 18, test_count + 17, test_count + 16,
						test_count + 15, test_count + 14, test_count + 13, test_count + 12,
						test_count + 11, test_count + 10, test_count + 9, test_count + 8,
						test_count + 7, test_count + 6, test_count + 5, test_count + 4,
						test_count + 3, test_count + 2, test_count + 1, test_count};
end
else begin
	assign tmp_data	= test_count;
end
endgenerate


endmodule
