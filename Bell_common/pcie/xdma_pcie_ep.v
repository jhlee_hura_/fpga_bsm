`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: HURA
// Engineer: LEE Seong Yun (sylee@hura.co.kr)
//
// Create Date: 2018/04/25 21:04:48
// Design Name: register for MARS X1 Board Support Configuration(BSC)
// Module Name: xdma_pcie_ep
// Project Name: HURA MARS X1F-K7
// Target Devices: Kintex7 410T
// Tool Versions: Vivado 2018.1
// Description:
//
// Dependencies:
//
// Revision:
// Additional Comments:
//
// Copyright 2018, All rights reserved by HURA
//////////////////////////////////////////////////////////////////////////////////
module xdma_pcie_ep (
	output wire	[3:0]		pci_exp_txp,
	output wire	[3:0]		pci_exp_txn,
	input wire	[3:0]		pci_exp_rxp,
	input wire	[3:0]		pci_exp_rxn,

	input wire				sys_clk_p,		// 100MHz
	input wire				sys_clk_n,
	input wire				sys_rst_n,

	// -- to/from REGS
	output	wire	[46:0]	regs_adda,
	output	wire			regs_adda_valid,
	output	wire			regs_bsc_sys_sel,
	output	wire			regs_bsc_sig_sel,
	output	wire			regs_bsc_pci_sel,
	output	wire			regs_ud_sys_sel,
	output	wire			regs_ud_sig_sel,
//	output	wire			regs_ud_reg3_sel,
//	output	wire			regs_ud_reg4_sel,

	input	wire	[31:0]	regs_bsc_sys_data,
	input	wire	[31:0]	regs_bsc_sig_data,
	input	wire	[31:0]	regs_bsc_pci_data,
	input	wire	[31:0]	regs_ud_sys_data,
	input	wire	[31:0]	regs_ud_sig_data,
//	input	wire	[31:0]	regs_ud_reg3_data,
//	input	wire	[31:0]	regs_ud_reg4_data,
	input	wire			regs_data_valid,

	// DMA Status
	output	wire	[7:0]	c2h_sts_0,
	output	wire	[7:0]	h2c_sts_0,

	output reg [31:0] c2h_busy_clk_cnt	='b0,
	output reg [31:0] c2h_run_clk_cnt	='b0,
	output reg [31:0] c2h_packet_cnt	='b0,
	output reg [31:0] c2h_desc_cnt		='b0,

	// --  AXI ST interface to user
	// AXI streaming ports
	input	wire [127:0]	s_axis_c2h_tdata_0,		// I[127:0]
	input	wire			s_axis_c2h_tlast_0,		// I
	input	wire			s_axis_c2h_tvalid_0,	// I
	output	wire			s_axis_c2h_tready_0,	// O
	input	wire [15:0]		s_axis_c2h_tkeep_0,		// I[15:0]
	output	wire [127:0]	m_axis_h2c_tdata_0,		// O[127:0]
	output	wire			m_axis_h2c_tlast_0,		// O
	output	wire			m_axis_h2c_tvalid_0,	// O
	input	wire			m_axis_h2c_tready_0,	// I
	output	wire [15:0]		m_axis_h2c_tkeep_0,		// O [15:0]

	// user clk
	output wire				user_clk,
	output wire				user_resetn,
	output wire				user_lnk_up
 );
// (* mark_debug = "true" *)
//----------------------------------------------------------------------------------------------------------------//
//	  System(SYS) Interface																						  //
//----------------------------------------------------------------------------------------------------------------//
wire		sys_clk;
wire		sys_rst_n_c;
wire [2:0]	msi_vector_width;
wire		msi_enable;
reg			usr_irq_req = 0;
wire		usr_irq_ack;

wire	[31:0]	m_axil_awaddr;
wire			m_axil_awvalid;
wire			m_axil_awready;
wire	[31:0]	m_axil_wdata;
wire	[3:0]	m_axil_wstrb;
wire			m_axil_wvalid;
wire			m_axil_wready;
wire			m_axil_bvalid;
wire	[1:0]	m_axil_bresp;
wire			m_axil_bready;
wire	[31:0]	m_axil_araddr;
wire			m_axil_arvalid;
wire			m_axil_arready;
wire	[31:0]	m_axil_rdata;
wire	[1:0]	m_axil_rresp;
wire			m_axil_rvalid;
wire			m_axil_rready;

// Ref clock buffer
IBUFDS_GTE2 refclk_ibuf (.O(sys_clk), .ODIV2(), .I(sys_clk_p), .CEB(1'b0), .IB(sys_clk_n));

// Core Top Level Wrapper
xdma_0 xdma_0_i (
	//---------------------------------------------------------------------------------------//
	//	PCI Express (pci_exp) Interface														 //
	//---------------------------------------------------------------------------------------//
	.sys_clk		 (sys_clk),				// I
	.sys_rst_n		 (sys_rst_n),			// I
	.user_lnk_up	 (user_lnk_up),			// O
	// Tx
	.pci_exp_txp	 (pci_exp_txp),			// O [3:0]
	.pci_exp_txn	 (pci_exp_txn),			// O [3:0]

	// Rx
	.pci_exp_rxp	 (pci_exp_rxp),			// I [3:0]
	.pci_exp_rxn	 (pci_exp_rxn),			// I [3:0]

	//-- AXI Global
	.axi_aclk		 (user_clk),			// O // 125MHz
	.axi_aresetn	 (user_resetn),			// O

	// IRQ, MSI
	.usr_irq_req	   (usr_irq_req),		// I
	.usr_irq_ack	   (usr_irq_ack),		// O
	.msi_enable		   (msi_enable),		// O
	.msi_vector_width  (msi_vector_width),	// O [2:0]

	// LITE interface
	//-- AXI Master Write Address Channel
	.m_axil_awaddr	  (m_axil_awaddr),		// O [31:0]
	.m_axil_awprot	  (),					// O [2:0]	always 0
	.m_axil_awvalid	  (m_axil_awvalid),		// O
	.m_axil_awready	  (m_axil_awready),		// I
	//-- AXI Master Write Data Channel
	.m_axil_wdata	  (m_axil_wdata),		// O [31:0]
	.m_axil_wstrb	  (m_axil_wstrb),		// O [3:0]
	.m_axil_wvalid	  (m_axil_wvalid),		// O
	.m_axil_wready	  (m_axil_wready),		// I
	//-- AXI Master Write Response Channel
	.m_axil_bvalid	  (m_axil_bvalid),		// I
	.m_axil_bresp	  (m_axil_bresp),		// I [1:0]
	.m_axil_bready	  (m_axil_bready),		// O
	//-- AXI Master Read Address Channel
	.m_axil_araddr	  (m_axil_araddr),		// O [31:0]
	.m_axil_arprot	  (),					// O [2:0]	always 0
	.m_axil_arvalid	  (m_axil_arvalid),		// O
	.m_axil_arready	  (m_axil_arready),		// I
	//-- AXI Master Read Data Channel
	.m_axil_rdata	  (m_axil_rdata),		// I [31:0]
	.m_axil_rresp	  (m_axil_rresp),		// I [1:0]
	.m_axil_rvalid	  (m_axil_rvalid),		// I
	.m_axil_rready	  (m_axil_rready),		// O

	// Config managemnet interface
	.cfg_mgmt_addr	( 19'b0 ),					// I [18:0]
	.cfg_mgmt_write ( 1'b0 ),					// I
	.cfg_mgmt_write_data ( 32'b0 ),				// I [31:0]
	.cfg_mgmt_byte_enable ( 4'b0 ),				// I [3:0]
	.cfg_mgmt_read	( 1'b0 ),					// I
	.cfg_mgmt_read_data (),						// O [31:0]
	.cfg_mgmt_read_write_done (),				// O
	.cfg_mgmt_type1_cfg_reg_access ( 1'b0 ),	// I

	// AXI streaming ports
	.s_axis_c2h_tdata_0	  (s_axis_c2h_tdata_0),		// I [127:0]
	.s_axis_c2h_tlast_0	  (s_axis_c2h_tlast_0),		// I
	.s_axis_c2h_tvalid_0  (s_axis_c2h_tvalid_0),	// I
	.s_axis_c2h_tready_0  (s_axis_c2h_tready_0),	// O
	.s_axis_c2h_tkeep_0	  (s_axis_c2h_tkeep_0),		// I [15:0]
	.m_axis_h2c_tdata_0	  (m_axis_h2c_tdata_0),		// O [127:0]
	.m_axis_h2c_tlast_0	  (m_axis_h2c_tlast_0),		// O
	.m_axis_h2c_tvalid_0  (m_axis_h2c_tvalid_0),	// O
	.m_axis_h2c_tready_0  (m_axis_h2c_tready_0),	// I
	.m_axis_h2c_tkeep_0	  (m_axis_h2c_tkeep_0),		// O [15:0]

	// DMA Status
	.c2h_sts_0				(c2h_sts_0),			// O [7 : 0]
	.h2c_sts_0				(h2c_sts_0)				// O [7 : 0]
);


// ------------------------------------------------------------
// DMA 속도 체크용 counter
// 현재는 c2h의 busy status, control run status를 비교 해보고자 함.
reg c2h_busy_end_flag;
reg c2h_run_end_flag;
wire c2h_busy;
wire c2h_control_run;
wire packet_done;
wire descript_done;
assign c2h_busy = c2h_sts_0[0];
assign c2h_control_run = c2h_sts_0[6];
assign packet_done = c2h_sts_0[4];
assign descript_done = c2h_sts_0[3];


always @(posedge user_clk) begin
	if (c2h_busy) begin
		c2h_busy_clk_cnt <= (c2h_busy_end_flag) ? 'b1 : c2h_busy_clk_cnt + 'b1;	// 새롭게 시작하면 cnt Reset
		c2h_busy_end_flag <= 1'b0;
	end
	else begin
		c2h_busy_clk_cnt <= c2h_busy_clk_cnt;			// busy가 아닐때 지난 값을 유지해 Register로 값을 보냄.
		c2h_busy_end_flag <= 1'b1;
	end

	if (c2h_control_run) begin
		c2h_run_clk_cnt <= (c2h_run_end_flag) ? 'b1 : c2h_run_clk_cnt + 'b1;	// 새롭게 시작하면 cnt Reset
		c2h_run_end_flag <= 1'b0;

		c2h_packet_cnt <=	(c2h_run_end_flag) ? 'b0 :
							(packet_done) ? c2h_packet_cnt + 1'b1 : c2h_packet_cnt;
		c2h_desc_cnt <=		(c2h_run_end_flag) ? 'b0 :
							(descript_done) ? c2h_desc_cnt + 1'b1 : c2h_desc_cnt;
	end
	else begin
		c2h_run_clk_cnt <= c2h_run_clk_cnt;		// busy가 아닐때 지난 값을 유지해 Register로 값을 보냄.
		c2h_packet_cnt <= c2h_packet_cnt;
		c2h_desc_cnt <= c2h_desc_cnt;
		c2h_run_end_flag <= 1'b1;
	end
end


// ------------------------------------------------------------
// interface to axil - regs

axil2regs axil2regs (
	.clk			(user_clk),			// I
	.sclr			(~user_resetn),		// I

	//-- AXI LITE
	//-- AXI Slave interface
	.s_axil_awaddr	(m_axil_awaddr[15:2]),	// I [13:0]
	.s_axil_awvalid	(m_axil_awvalid),		// I
	.s_axil_awready	(m_axil_awready),		// O

	.s_axil_wdata	(m_axil_wdata),			// I [31:0]
	.s_axil_wvalid	(m_axil_wvalid),		// I
	.s_axil_wready	(m_axil_wready),		// O

	.s_axil_bresp	(m_axil_bresp),			// O [1:0]
	.s_axil_bvalid	(m_axil_bvalid),		// O
	.s_axil_bready	(m_axil_bready),		// I

	.s_axil_araddr	(m_axil_araddr[15:2]),	// I [13:0]
	.s_axil_arvalid	(m_axil_arvalid),		// I
	.s_axil_arready	(m_axil_arready),		// O

	.s_axil_rdata	(m_axil_rdata),			// O [31:0]
	.s_axil_rresp	(m_axil_rresp),			// O [1:0]
	.s_axil_rvalid	(m_axil_rvalid),		// O
	.s_axil_rready	(m_axil_rready),		// I

	// -------------------------------------------------------
	.regs_adda			(regs_adda),			// O [46:0]
	.regs_adda_valid	(regs_adda_valid),		// O
	.regs_bsc_sys_sel	(regs_bsc_sys_sel),		// O
	.regs_bsc_sig_sel	(regs_bsc_sig_sel),		// O
	.regs_bsc_pci_sel	(regs_bsc_pci_sel),		// O
	.regs_ud_sys_sel	(regs_ud_sys_sel),		// O
	.regs_ud_sig_sel	(regs_ud_sig_sel),		// O
//	.regs_ud_reg3_sel	(regs_ud_reg3_sel),		// O
//	.regs_ud_reg4_sel	(regs_ud_reg4_sel),		// O

	.regs_bsc_sys_data	(regs_bsc_sys_data),	// I [31:0]
	.regs_bsc_sig_data	(regs_bsc_sig_data),	// I [31:0]
	.regs_bsc_pci_data	(regs_bsc_pci_data),	// I [31:0]
	.regs_ud_sys_data	(regs_ud_sys_data),		// I [31:0]
	.regs_ud_sig_data	(regs_ud_sig_data),		// I [31:0]
//	.regs_ud_reg3_data	(regs_ud_reg3_data),	// I [31:0]
//	.regs_ud_reg4_data	(regs_ud_reg4_data),	// I [31:0]
	.regs_data_valid	(regs_data_valid)		// I
);

endmodule