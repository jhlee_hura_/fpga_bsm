//////////////////////////////////////////////////////////////////////////////////
// Company: HURA
// Engineer: LEE Seong Yun (sylee@hura.co.kr)
//
// Create Date: 2019/01/02 11:05:02
// Design Name:
// Module Name: QKD Common REG
// Project Name: QKD
// Target Devices: Kintex 7 160T
// Tool Versions: Vivado 2018.1
// Description:
//
// Dependencies:
//
// Revision:
//		build. 6(2019/03/05)
// Additional Comments:
//
// Copyright 2018, All rights reserved by HURA
//////////////////////////////////////////////////////////////////////////////////
// --------------------------------------------------------------------------------
// C�? 카피방법
// 1.주소까�? 카피
// replace (ctrl+H)
// 16'h	 -> 0x
// 32'h	 -> 0x
// 'h	 -> 0x
// `define -> #define
// ` -> 
// 'd -> 
// 2'b -> 
// ---------------------------------------------------------------------------------
// ?���? 만들�?
// a='QKD Alice Board(HR-QKD-ALICE-01)';
// len_b=ceil(length(a)/4)*4; b=zeros(1,len_b);b(1:length(a))=a;
// c=dec2hex(typecast(uint8(double(b)),'uint32'))
//
// char(typecast(uint32(hex2dec(c)),'uint8'))' % ?��?��?��
// reshape(char(typecast(uint32(hex2dec(c)),'uint8')),4,len_b/4)' % ?��?��?��
// a='QKD ALICE Testbed';
// ---------------------------------------------------------------------------------

// ----------------------------------------------------------------------
// I/O REGISTER
// ----------------------------------------------------------------------
// address range
// system regs (512 : 8 x 16 x 4)
`define BSC_REG1_ADDR_START			16'h0000
`define	BSC_REG1_ADDR_END			16'h07FF
// mclk regs  regs (320 : 5 x 16 x 4)
`define	BSC_REG2_ADDR_START			16'h0800
`define	BSC_REG2_ADDR_END			16'h0CFF
// pcie domain address (192 : 3 x 16 x 4)
`define	BSC_PCIE_ADDR_START			16'h0D00
`define	BSC_PCIE_ADDR_END			16'h0FFF
// system domain address(7168 : 7 x16 x 16 x 4)
`define UD_REG1_ADDR_START				16'h1000
`define	UD_REG1_ADDR_END			16'h7FFF
// signal domain address(8192 : 8 x 16 x 16 x 4)
`define	UD_REG2_ADDR_START			16'h8000
`define	UD_REG2_ADDR_END			16'hFFFF
/* // message C2H fifo domain address
`define	UD_REG3_ADDR_START				16'h3000
`define	UD_REG3_ADDR_END				16'hFFFF
// message mem domain address
`define	UD_REG4_ADDR_START				16'h4000
`define	UD_REG4_ADDR_END				16'hFFFF */

// ---------------------------- BSC Region ----------------------------
`define BOARD_MAKER_REG				`BSC_REG1_ADDR_START + 16'h0000
`define BSC_VERSION_REG				`BSC_REG1_ADDR_START + 16'h0004
`define BOARD_NAME_REG			    	`BSC_REG1_ADDR_START + 16'h0008
`define BOARD_NAME1_REG				`BSC_REG1_ADDR_START + 16'h000C
`define BOARD_NAME2_REG				`BSC_REG1_ADDR_START + 16'h0010
`define BOARD_NAME3_REG				`BSC_REG1_ADDR_START + 16'h0014
`define BOARD_NAME4_REG				`BSC_REG1_ADDR_START + 16'h0018
`define BOARD_NAME5_REG				`BSC_REG1_ADDR_START + 16'h001C
`define BOARD_NAME6_REG				`BSC_REG1_ADDR_START + 16'h0020
`define BOARD_NAME7_REG				`BSC_REG1_ADDR_START + 16'h0024
`define BOARD_NAME8_REG				`BSC_REG1_ADDR_START + 16'h0028
`define BOARD_NAME9_REG				`BSC_REG1_ADDR_START + 16'h002C
`define BOARD_NAME10_REG		     	`BSC_REG1_ADDR_START + 16'h0030
`define BOARD_NAME11_REG		 	   	`BSC_REG1_ADDR_START + 16'h0034
`define CLK_MUX_REG					`BSC_REG1_ADDR_START + 16'h0038
`define TEST_PAD_REG					`BSC_REG1_ADDR_START + 16'h003C
`define BSC_TEMP_REG					`BSC_REG1_ADDR_START + 16'h0040
`define LED_TEST_REG					`BSC_TEMP_REG

// --------- Project Information Register ---------
`define COPYRIGHTER_REG				`UD_REG1_ADDR_START + 16'h0000
`define VERSION_REG					`UD_REG1_ADDR_START + 16'h0004
`define PRODUCT_REG					`UD_REG1_ADDR_START + 16'h0008
`define PRODUCT1_REG					`UD_REG1_ADDR_START + 16'h000C
`define PRODUCT2_REG					`UD_REG1_ADDR_START + 16'h0010
`define PRODUCT3_REG					`UD_REG1_ADDR_START + 16'h0014
`define PRODUCT4_REG					`UD_REG1_ADDR_START + 16'h0018
`define PRODUCT5_REG					`UD_REG1_ADDR_START + 16'h001C
`define PRODUCT6_REG					`UD_REG1_ADDR_START + 16'h0020
`define PRODUCT7_REG					`UD_REG1_ADDR_START + 16'h0024
`define PRODUCT8_REG					`UD_REG1_ADDR_START + 16'h0028
`define PRODUCT9_REG					`UD_REG1_ADDR_START + 16'h002C
`define PRODUCT10_REG					`UD_REG1_ADDR_START + 16'h0030
`define PRODUCT11_REG					`UD_REG1_ADDR_START + 16'h0034
`define	BYTESWAP_REG				`UD_REG1_ADDR_START + 16'h003C
`define	TMP_SIG_REG					`UD_REG2_ADDR_START + 16'h0040

// --------- Alice Bob Common Register ---------
`define LM70_SPI_WR_REG				`BSC_REG1_ADDR_START  + 16'h0080		// LM70 : Temp. sens.
`define LM70_SPI_RD_REG				`BSC_REG1_ADDR_START  + 16'h0084

`define FAN_HIGH_DURA_REG				`BSC_REG1_ADDR_START  + 16'h0090
`define FAN_LOW_DURA_REG				`BSC_REG1_ADDR_START  + 16'h0094

`define FAN1_HIGH_DURA_REG				`BSC_REG1_ADDR_START  + 16'h0098
`define FAN1_LOW_DURA_REG				`BSC_REG1_ADDR_START  + 16'h009C

//`define ADC_OSC_EN_REG					`BSC_REG1_ADDR_START  + 16'h00A0
`define B_SIG_CLK_SEL_REG			   	`BSC_REG1_ADDR_START  + 16'h00A0


//*// define UD_REG1_ADDR_START				16'h1000
//*// define	UD_REG2_ADDR_START				16'h8000
// A- Alice, B- Bob, C- Charlie
`define C_SYS_BASE_ADD				    `UD_REG1_ADDR_START + 16'h1000
`define A_SYS_BASE_ADD				    `UD_REG1_ADDR_START + 16'h2000
`define B_SYS_BASE_ADD			        `UD_REG1_ADDR_START + 16'h3000

`define C_SIG_BASE_ADD			      	`UD_REG2_ADDR_START + 16'h1000 
`define A_SIG_BASE_ADD			    	`UD_REG2_ADDR_START + 16'h2000 
`define B_SIG_BASE_ADD			        `UD_REG2_ADDR_START + 16'h3000 
// --------------------------------------------------------------------
// ---  Charlie
// --------------------------------------------------------------------

`define C1_SYS_BASE_ADD				`C_SYS_BASE_ADD + 16'h0100
`define C2_SYS_BASE_ADD				`C_SYS_BASE_ADD + 16'h0200
`define C3_SYS_BASE_ADD				`C_SYS_BASE_ADD + 16'h0300
`define C4_SYS_BASE_ADD				`C_SYS_BASE_ADD + 16'h0400
`define C5_SYS_BASE_ADD				`C_SYS_BASE_ADD + 16'h0500
`define C6_SYS_BASE_ADD				`C_SYS_BASE_ADD + 16'h0600
`define C7_SYS_BASE_ADD				`C_SYS_BASE_ADD + 16'h0700
`define C8_SYS_BASE_ADD				`C_SYS_BASE_ADD + 16'h0800
`define C9_SYS_BASE_ADD				`C_SYS_BASE_ADD + 16'h0900
`define C10_SYS_BASE_ADD			    `C_SYS_BASE_ADD + 16'h0A00

`define C1_SIG_BASE_ADD				`C_SIG_BASE_ADD + 16'h0100
`define C2_SIG_BASE_ADD				`C_SIG_BASE_ADD + 16'h0200
`define C3_SIG_BASE_ADD				`C_SIG_BASE_ADD + 16'h0300
`define C4_SIG_BASE_ADD				`C_SIG_BASE_ADD + 16'h0400
`define C5_SIG_BASE_ADD				`C_SIG_BASE_ADD + 16'h0500
`define C6_SIG_BASE_ADD				`C_SIG_BASE_ADD + 16'h0600
`define C7_SIG_BASE_ADD				`C_SIG_BASE_ADD + 16'h0700
`define C8_SIG_BASE_ADD				`C_SIG_BASE_ADD + 16'h0800
`define C9_SIG_BASE_ADD				`C_SIG_BASE_ADD + 16'h0900
`define C10_SIG_BASE_ADD			    `C_SIG_BASE_ADD + 16'h0A00
`define COP_SIG_BASE_ADD			    `C_SIG_BASE_ADD + 16'h0B00

// C_CONNECT 1  ------------------------------------------------- SPAD

//SYS CLK 

`define C1_HMC920_EN_REG				`C1_SYS_BASE_ADD + 16'h0000
`define C1_SPD_EN_REG					`C1_SYS_BASE_ADD + 16'h0004
`define C1_SIP_EN_REG					`C1_SYS_BASE_ADD + 16'h0008

`define C1_LTC1867_SPI_WR_REG			`C1_SYS_BASE_ADD + 16'h000C		// ADC
`define C1_LTC1867_SPI_RD_REG	        `C1_SYS_BASE_ADD + 16'h0010

`define C1_SPD_DAC_WR_REG				`C1_SYS_BASE_ADD + 16'h0014		// DAC
`define C1_SIP_DAC_WR_REG				`C1_SYS_BASE_ADD + 16'h0018		// DAC

`define C1_TEC_EN_REG					`C1_SYS_BASE_ADD + 16'h001C		// PWR DRIVER MAX1968  TEC Control
`define C1_TEC_DAC_WR_REG				`C1_SYS_BASE_ADD + 16'h0020		// DAC MAX5144  
`define C1_TEC_DAC_CLR_REG				`C1_SYS_BASE_ADD + 16'h0024		// DAC MAX5144

`define C1_DP_WR_REG				    `C1_SYS_BASE_ADD + 16'h0028	
`define C1_DP_RD_REG				    `C1_SYS_BASE_ADD + 16'h002C	

 // SIG CLK C1_SIG_BASE_ADD				16'h9100
`define C1_TEST_MODE_REG				`C1_SIG_BASE_ADD + 16'h0000
`define C1_TEST_DATA_REG               `C1_SIG_BASE_ADD + 16'h0004
`define C1_WIDTH_REG					`C1_SIG_BASE_ADD + 16'h0010
`define C1_OFFSET_QUOT_REG				`C1_SIG_BASE_ADD + 16'h0014
`define C1_OFFSET_REMD_REG				`C1_SIG_BASE_ADD + 16'h0018
`define C1_DELAY_TAP_REG				`C1_SIG_BASE_ADD + 16'h001C
`define C1_DETECT_DELAY_TAP_REG		`C1_SIG_BASE_ADD + 16'h0020


`define C1_SPD_CNT1_REG				`C1_SIG_BASE_ADD + 16'h0028
`define C1_SPD_CNT2_REG				`C1_SIG_BASE_ADD + 16'h002C
`define C1_SPD_CNT3_REG				`C1_SIG_BASE_ADD + 16'h0030
`define C1_SPD_CNT4_REG				`C1_SIG_BASE_ADD + 16'h0034

// C_CONNECT 2  ------------------------------------------------- SPAD

//SYS CLK 

`define C2_HMC920_EN_REG				`C2_SYS_BASE_ADD + 16'h0000
`define C2_SPD_EN_REG					`C2_SYS_BASE_ADD + 16'h0004
`define C2_SIP_EN_REG					`C2_SYS_BASE_ADD + 16'h0008

`define C2_LTC1867_SPI_WR_REG			`C2_SYS_BASE_ADD + 16'h000C		// ADC
`define C2_LTC1867_SPI_RD_REG	        `C2_SYS_BASE_ADD + 16'h0010

`define C2_SPD_DAC_WR_REG				`C2_SYS_BASE_ADD + 16'h0014		// DAC
`define C2_SIP_DAC_WR_REG				`C2_SYS_BASE_ADD + 16'h0018		// DAC

`define C2_TEC_EN_REG					`C2_SYS_BASE_ADD + 16'h001C		// PWR DRIVER MAX1968  TEC Control
`define C2_TEC_DAC_WR_REG				`C2_SYS_BASE_ADD + 16'h0020		// DAC MAX5144  
`define C2_TEC_DAC_CLR_REG				`C2_SYS_BASE_ADD + 16'h0024		// DAC MAX5144

`define C2_DP_WR_REG				    `C2_SYS_BASE_ADD + 16'h0028	
`define C2_DP_RD_REG				    `C2_SYS_BASE_ADD + 16'h002C	

 // SIG CLK 
`define C2_TEST_MODE_REG				`C2_SIG_BASE_ADD + 16'h0000
`define C2_TEST_DATA_REG               `C2_SIG_BASE_ADD + 16'h0004
`define C2_WIDTH_REG					`C2_SIG_BASE_ADD + 16'h0010
`define C2_OFFSET_QUOT_REG				`C2_SIG_BASE_ADD + 16'h0014
`define C2_OFFSET_REMD_REG				`C2_SIG_BASE_ADD + 16'h0018
`define C2_DELAY_TAP_REG				`C2_SIG_BASE_ADD + 16'h001C
`define C2_DETECT_DELAY_TAP_REG		`C2_SIG_BASE_ADD + 16'h0020

`define C2_SPD_CNT1_REG				`C2_SIG_BASE_ADD + 16'h0028
`define C2_SPD_CNT2_REG				`C2_SIG_BASE_ADD + 16'h002C
`define C2_SPD_CNT3_REG				`C2_SIG_BASE_ADD + 16'h0030
`define C2_SPD_CNT4_REG				`C2_SIG_BASE_ADD + 16'h0034

// C_CONNECT 3  ------------------------------------------------- SPAD
//SYS CLK 

`define C3_HMC920_EN_REG				`C3_SYS_BASE_ADD + 16'h0000
`define C3_SPD_EN_REG					`C3_SYS_BASE_ADD + 16'h0004
`define C3_SIP_EN_REG					`C3_SYS_BASE_ADD + 16'h0008

`define C3_LTC1867_SPI_WR_REG			`C3_SYS_BASE_ADD + 16'h000C		// ADC
`define C3_LTC1867_SPI_RD_REG	        `C3_SYS_BASE_ADD + 16'h0010

`define C3_SPD_DAC_WR_REG				`C3_SYS_BASE_ADD + 16'h0014		// DAC
`define C3_SIP_DAC_WR_REG				`C3_SYS_BASE_ADD + 16'h0018		// DAC

`define C3_TEC_EN_REG					`C3_SYS_BASE_ADD + 16'h001C		// PWR DRIVER MAX1968  TEC Control
`define C3_TEC_DAC_WR_REG				`C3_SYS_BASE_ADD + 16'h0020		// DAC MAX5144  
`define C3_TEC_DAC_CLR_REG				`C3_SYS_BASE_ADD + 16'h0024		// DAC MAX5144

`define C3_DP_WR_REG				    `C3_SYS_BASE_ADD + 16'h0028	
`define C3_DP_RD_REG				    `C3_SYS_BASE_ADD + 16'h002C	

 // SIG CLK 
`define C3_TEST_MODE_REG				`C3_SIG_BASE_ADD + 16'h0000
`define C3_TEST_DATA_REG               `C3_SIG_BASE_ADD + 16'h0004
`define C3_WIDTH_REG					`C3_SIG_BASE_ADD + 16'h0010
`define C3_OFFSET_QUOT_REG				`C3_SIG_BASE_ADD + 16'h0014
`define C3_OFFSET_REMD_REG				`C3_SIG_BASE_ADD + 16'h0018
`define C3_DELAY_TAP_REG				`C3_SIG_BASE_ADD + 16'h001C
`define C3_DETECT_DELAY_TAP_REG		`C3_SIG_BASE_ADD + 16'h0020

`define C3_SPD_CNT1_REG				`C3_SIG_BASE_ADD + 16'h0028
`define C3_SPD_CNT2_REG				`C3_SIG_BASE_ADD + 16'h002C
`define C3_SPD_CNT3_REG				`C3_SIG_BASE_ADD + 16'h0030
`define C3_SPD_CNT4_REG				`C3_SIG_BASE_ADD + 16'h0034

// C_CONNECT 4  ------------------------------------------------- SPAD
//SYS CLK 

`define C4_HMC920_EN_REG				`C4_SYS_BASE_ADD + 16'h0000
`define C4_SPD_EN_REG					`C4_SYS_BASE_ADD + 16'h0004
`define C4_SIP_EN_REG					`C4_SYS_BASE_ADD + 16'h0008

`define C4_LTC1867_SPI_WR_REG			`C4_SYS_BASE_ADD + 16'h000C		// ADC
`define C4_LTC1867_SPI_RD_REG	        `C4_SYS_BASE_ADD + 16'h0010

`define C4_SPD_DAC_WR_REG				`C4_SYS_BASE_ADD + 16'h0014		// DAC
`define C4_SIP_DAC_WR_REG				`C4_SYS_BASE_ADD + 16'h0018		// DAC

`define C4_TEC_EN_REG					`C4_SYS_BASE_ADD + 16'h001C		// PWR DRIVER MAX1968  TEC Control
`define C4_TEC_DAC_WR_REG				`C4_SYS_BASE_ADD + 16'h0020		// DAC MAX5144  
`define C4_TEC_DAC_CLR_REG				`C4_SYS_BASE_ADD + 16'h0024		// DAC MAX5144

`define C4_DP_WR_REG				    `C4_SYS_BASE_ADD + 16'h0028	
`define C4_DP_RD_REG				    `C4_SYS_BASE_ADD + 16'h002C	

 // SIG CLK 
`define C4_TEST_MODE_REG				`C4_SIG_BASE_ADD + 16'h0000
`define C4_TEST_DATA_REG               `C4_SIG_BASE_ADD + 16'h0004
`define C4_WIDTH_REG					`C4_SIG_BASE_ADD + 16'h0010
`define C4_OFFSET_QUOT_REG				`C4_SIG_BASE_ADD + 16'h0014
`define C4_OFFSET_REMD_REG				`C4_SIG_BASE_ADD + 16'h0018
`define C4_DELAY_TAP_REG				`C4_SIG_BASE_ADD + 16'h001C
`define C4_DETECT_DELAY_TAP_REG		`C4_SIG_BASE_ADD + 16'h0020

`define C4_SPD_CNT1_REG				`C4_SIG_BASE_ADD + 16'h0028
`define C4_SPD_CNT2_REG				`C4_SIG_BASE_ADD + 16'h002C
`define C4_SPD_CNT3_REG				`C4_SIG_BASE_ADD + 16'h0030
`define C4_SPD_CNT4_REG				`C4_SIG_BASE_ADD + 16'h0034

// C_CONNECT 5  ------------------------------------------------- FPC
//SYS CLK 

`define C5_FPC_EN_REG					`C5_SYS_BASE_ADD  + 16'h0010

`define C5_FPC_DAC_WR_REG				`C5_SYS_BASE_ADD  + 16'h0020	// DAC AD5044 :FPC
`define C5_FPC_DAC_CLR_REG				`C5_SYS_BASE_ADD  + 16'h0024	// DAC AD5044 :FPC

`define C5_FPC10_DAC_WR_REG			`C5_SYS_BASE_ADD  + 16'h0030	// DAC AD5044 :FPC
`define C5_FPC10_DAC_CLR_REG			`C5_SYS_BASE_ADD  + 16'h0034	// DAC AD5044 :FPC

// SIG CLK 

// C_CONNECT 6  ------------------------------------------------- LDSYNC
//SYS CLK 


`define C6_HMC920_EN_REG				`C6_SYS_BASE_ADD + 16'h0000

`define C6_LTC1867_SPI_WR_REG			`C6_SYS_BASE_ADD + 16'h0004		// ADC
`define C6_LTC1867_SPI_RD_REG	       	`C6_SYS_BASE_ADD + 16'h0008

`define C6_AD5761_DAC_WR_REG			`C6_SYS_BASE_ADD + 16'h000C		// DAC AD5761  
`define C6_AD5761_DAC_RD_REG			`C6_SYS_BASE_ADD + 16'h0010		// DAC AD5761  

// LD1550 TEC Control
`define C6_TEC_EN_REG				    `C6_SYS_BASE_ADD + 16'h0014		// PWR DRIVER MAX1968 for TEC
`define C6_TEC_DAC_WR_REG			    `C6_SYS_BASE_ADD + 16'h0018		// MAX5144 
`define C6_TEC_DAC_CLR_REG				`C6_SYS_BASE_ADD + 16'h001C		// MAX5144 

`define C6_DP_WR_REG                  	`C6_SYS_BASE_ADD + 16'h0020
`define C6_DP_RD_REG                  	`C6_SYS_BASE_ADD + 16'h0024

// SIG CLK 

`define C6_TEST_MODE_REG				`C6_SIG_BASE_ADD + 16'h0000
`define C6_TEST_DATA_REG				`C6_SIG_BASE_ADD + 16'h0004

`define C6_WIDTH_REG					`C6_SIG_BASE_ADD + 16'h0010
`define C6_ADDWIDTH_REG				`C6_SIG_BASE_ADD + 16'h0014
`define C6_OFFSET_QUOT_REG				`C6_SIG_BASE_ADD + 16'h0018
`define C6_OFFSET_REMD_REG				`C6_SIG_BASE_ADD + 16'h001C
`define C6_DELAY_TAP_REG				`C6_SIG_BASE_ADD + 16'h0020
`define C6_ADD_PULSE_DELAY_REG		    `C6_SIG_BASE_ADD + 16'h0024

// C_CONNECT 7  ------------------------------------------------- LD SYNC
//SYS CLK 


`define C7_HMC920_EN_REG				`C7_SYS_BASE_ADD + 16'h0000

`define C7_LTC1867_SPI_WR_REG			`C7_SYS_BASE_ADD + 16'h0004		// ADC
`define C7_LTC1867_SPI_RD_REG	       	`C7_SYS_BASE_ADD + 16'h0008

`define C7_AD5761_DAC_WR_REG			`C7_SYS_BASE_ADD + 16'h000C		// DAC AD5761  
`define C7_AD5761_DAC_RD_REG			`C7_SYS_BASE_ADD + 16'h0010		// DAC AD5761  

// LD1550 TEC Control
`define C7_TEC_EN_REG				    `C7_SYS_BASE_ADD + 16'h0014		// PWR DRIVER MAX1968 for TEC
`define C7_TEC_DAC_WR_REG			    `C7_SYS_BASE_ADD + 16'h0018		// MAX5144 
`define C7_TEC_DAC_CLR_REG				`C7_SYS_BASE_ADD + 16'h001C		// MAX5144 

`define C7_DP_WR_REG                  	`C7_SYS_BASE_ADD + 16'h0020
`define C7_DP_RD_REG                  	`C7_SYS_BASE_ADD + 16'h0024

// SIG CLK 

`define C7_TEST_MODE_REG				`C7_SIG_BASE_ADD + 16'h0000
`define C7_TEST_DATA_REG				`C7_SIG_BASE_ADD + 16'h0004

`define C7_WIDTH_REG					`C7_SIG_BASE_ADD + 16'h0010
`define C7_ADDWIDTH_REG				`C7_SIG_BASE_ADD + 16'h0014
`define C7_OFFSET_QUOT_REG				`C7_SIG_BASE_ADD + 16'h0018
`define C7_OFFSET_REMD_REG				`C7_SIG_BASE_ADD + 16'h001C
`define C7_DELAY_TAP_REG				`C7_SIG_BASE_ADD + 16'h0020
`define C7_ADD_PULSE_DELAY_REG		    `C7_SIG_BASE_ADD + 16'h0024
// C_CONNECT 8  -------------------------------------------------FPC
//SYS CLK 

`define C8_FPC_EN_REG					`C8_SYS_BASE_ADD  + 16'h0010

`define C8_FPC_DAC_WR_REG				`C8_SYS_BASE_ADD  + 16'h0020	// DAC AD5044 :FPC
`define C8_FPC_DAC_CLR_REG				`C8_SYS_BASE_ADD  + 16'h0024	// DAC AD5044 :FPC

`define C8_FPC10_DAC_WR_REG				`C8_SYS_BASE_ADD  + 16'h0030	// DAC AD5044 :FPC
`define C8_FPC10_DAC_CLR_REG			`C8_SYS_BASE_ADD  + 16'h0034	// DAC AD5044 :FPC

// SIG CLK 

// C_CONNECT 9  ------------------------------------------------- 
//SYS CLK 


// SIG CLK 


// C_CONNECT 10  ------------------------------------------------- 
//SYS CLK 


// SIG CLK 


// C_Operation  ---------------------------------------------------

`define C_FIFO_STAT_REG				`COP_SIG_BASE_ADD + 16'h0000
`define C_FIFO_PF_THRES_REG			`COP_SIG_BASE_ADD + 16'h0004
`define C_FIFO_READ_REG				`COP_SIG_BASE_ADD + 16'h0008

`define C_TRIG_TYPE_REG			    `COP_SIG_BASE_ADD + 16'h000C

`define C_SPD12_CNT_REG			    `COP_SIG_BASE_ADD + 16'h0010
`define C_SPD13_CNT_REG			    `COP_SIG_BASE_ADD + 16'h0014
`define C_SPD24_CNT_REG			    `COP_SIG_BASE_ADD + 16'h0018
`define C_SPD34_CNT_REG			    `COP_SIG_BASE_ADD + 16'h001C
`define C_SPD1234_CNT_REG			    `COP_SIG_BASE_ADD + 16'h0020

`define C_START_REG					`COP_SIG_BASE_ADD + 16'h0030
`define C_FIFO_LOG_DELAY_REG			`COP_SIG_BASE_ADD + 16'h0034


// --------------------------------------------------------------------
// ---  Alice
// --------------------------------------------------------------------

`define A1_SYS_BASE_ADD				`A_SYS_BASE_ADD + 16'h0100
`define A2_SYS_BASE_ADD				`A_SYS_BASE_ADD + 16'h0200
`define A3_SYS_BASE_ADD				`A_SYS_BASE_ADD + 16'h0300
`define A4_SYS_BASE_ADD				`A_SYS_BASE_ADD + 16'h0400
`define A5_SYS_BASE_ADD				`A_SYS_BASE_ADD + 16'h0500
`define A6_SYS_BASE_ADD				`A_SYS_BASE_ADD + 16'h0600
`define A7_SYS_BASE_ADD				`A_SYS_BASE_ADD + 16'h0700
`define A8_SYS_BASE_ADD				`A_SYS_BASE_ADD + 16'h0800
`define A9_SYS_BASE_ADD				`A_SYS_BASE_ADD + 16'h0900
`define A10_SYS_BASE_ADD			    `A_SYS_BASE_ADD + 16'h0A00

`define A1_SIG_BASE_ADD				`A_SIG_BASE_ADD + 16'h0100
`define A2_SIG_BASE_ADD				`A_SIG_BASE_ADD + 16'h0200
`define A3_SIG_BASE_ADD				`A_SIG_BASE_ADD + 16'h0300
`define A4_SIG_BASE_ADD				`A_SIG_BASE_ADD + 16'h0400
`define A5_SIG_BASE_ADD				`A_SIG_BASE_ADD + 16'h0500
`define A6_SIG_BASE_ADD				`A_SIG_BASE_ADD + 16'h0600
`define A7_SIG_BASE_ADD				`A_SIG_BASE_ADD + 16'h0700
`define A8_SIG_BASE_ADD				`A_SIG_BASE_ADD + 16'h0800
`define A9_SIG_BASE_ADD				`A_SIG_BASE_ADD + 16'h0900
`define A10_SIG_BASE_ADD			    `A_SIG_BASE_ADD + 16'h0A00
`define AOP_SIG_BASE_ADD			    `A_SIG_BASE_ADD + 16'h0B00

// A_CONNECT 1  ------------------------------------------------- LD SIG
//SYS CLK 

`define A1_HMC920_EN_REG				`A1_SYS_BASE_ADD + 16'h0000

`define A1_LTC1867_SPI_WR_REG			`A1_SYS_BASE_ADD + 16'h0004		// ADC
`define A1_LTC1867_SPI_RD_REG	        `A1_SYS_BASE_ADD + 16'h0008		// ADC

`define A1_AD5761_DAC_WR_REG			`A1_SYS_BASE_ADD + 16'h000C		// DAC AD5761 
`define A1_AD5761_DAC_RD_REG			`A1_SYS_BASE_ADD + 16'h0010		// DAC AD5761 

`define A1_TEC_EN_REG					`A1_SYS_BASE_ADD + 16'h0014		// PWR DRIVER : MAX1968  TEC Control
`define A1_TEC_DAC_WR_REG				`A1_SYS_BASE_ADD + 16'h0018		// DAC MAX5144 
`define A1_TEC_DAC_CLR_REG				`A1_SYS_BASE_ADD + 16'h001C		// DAC MAX5144 

`define A1_DP_WR_REG				    `A1_SYS_BASE_ADD + 16'h0020	
`define A1_DP_RD_REG				    `A1_SYS_BASE_ADD + 16'h0024	

 // SIG CLK 

`define A1_TEST_MODE_REG				`A1_SIG_BASE_ADD + 16'h0000
`define A1_TEST_DATA_REG				`A1_SIG_BASE_ADD + 16'h0004

`define A1_WIDTH_REG					`A1_SIG_BASE_ADD + 16'h0010
`define A1_ADDWIDTH_REG				`A1_SIG_BASE_ADD + 16'h0014
`define A1_OFFSET_QUOT_REG				`A1_SIG_BASE_ADD + 16'h0018
`define A1_OFFSET_REMD_REG				`A1_SIG_BASE_ADD + 16'h001C
`define A1_DELAY_TAP_REG				`A1_SIG_BASE_ADD + 16'h0020
`define A1_ADD_PULSE_DELAY_REG		    `A1_SIG_BASE_ADD + 16'h0024

// A_CONNECT 2  ------------------------------------------------- SPAD
//SYS CLK 

`define A2_HMC920_EN_REG				`A2_SYS_BASE_ADD + 16'h0000
`define A2_SPD_EN_REG					`A2_SYS_BASE_ADD + 16'h0004
`define A2_SIP_EN_REG					`A2_SYS_BASE_ADD + 16'h0008

`define A2_LTC1867_SPI_WR_REG			`A2_SYS_BASE_ADD + 16'h000C		// ADC
`define A2_LTC1867_SPI_RD_REG	        `A2_SYS_BASE_ADD + 16'h0010

`define A2_SPD_DAC_WR_REG				`A2_SYS_BASE_ADD + 16'h0014		// DAC  
`define A2_SIP_DAC_WR_REG				`A2_SYS_BASE_ADD + 16'h0018		// DAC  

`define A2_TEC_EN_REG					`A2_SYS_BASE_ADD + 16'h001C		// PWR DRIVER MAX1968  TEC Control
`define A2_TEC_DAC_WR_REG				`A2_SYS_BASE_ADD + 16'h0020		// DAC MAX5144
`define A2_TEC_DAC_CLR_REG				`A2_SYS_BASE_ADD + 16'h0024		// DAC MAX5144

`define A2_DP_WR_REG				    `A2_SYS_BASE_ADD + 16'h0028	
`define A2_DP_RD_REG				    `A2_SYS_BASE_ADD + 16'h002C	

 // SIG CLK 
`define A2_TEST_MODE_REG				`A2_SIG_BASE_ADD + 16'h0000
`define A2_TEST_DATA_REG               `A2_SIG_BASE_ADD + 16'h0004
`define A2_DETECT_DELAY_TAP_REG		`A2_SIG_BASE_ADD + 16'h0008
`define A2_WIDTH_REG					`A2_SIG_BASE_ADD + 16'h0010
`define A2_OFFSET_QUOT_REG				`A2_SIG_BASE_ADD + 16'h0014
`define A2_OFFSET_REMD_REG				`A2_SIG_BASE_ADD + 16'h0018
`define A2_DELAY_TAP_REG				`A2_SIG_BASE_ADD + 16'h001C


`define A2_SPD_CNT1_REG				`A2_SIG_BASE_ADD + 16'h0020
`define A2_SPD_CNT2_REG				`A2_SIG_BASE_ADD + 16'h0024
`define A2_SPD_CNT3_REG				`A2_SIG_BASE_ADD + 16'h0028
`define A2_SPD_CNT4_REG				`A2_SIG_BASE_ADD + 16'h002C

// A_CONNECT 3  ------------------------------------------------- IM
//SYS CLK 

`define A3_IM_DAC_WR_REG				`A3_SYS_BASE_ADD + 16'h0030		// DAC AD5761 : IM

// SIG CLK 

`define A3_TEST_MODE_REG				`A3_SIG_BASE_ADD + 16'h0000
`define A3_TEST_L_DATA_REG		 	    `A3_SIG_BASE_ADD + 16'h0004
`define A3_TEST_H_DATA_REG			    `A3_SIG_BASE_ADD + 16'h0008

`define A3_WIDTH_REG					`A3_SIG_BASE_ADD + 16'h0010
`define A3_OFFSET_REG					`A3_SIG_BASE_ADD + 16'h0014

`define A3_V0_REG						`A3_SIG_BASE_ADD + 16'h0020
`define A3_V1_P_REG					`A3_SIG_BASE_ADD + 16'h0024
`define A3_V2_P_REG					`A3_SIG_BASE_ADD + 16'h0028
`define A3_V3_P_REG					`A3_SIG_BASE_ADD + 16'h002C
`define A3_V1_N_REG					`A3_SIG_BASE_ADD + 16'h0030
`define A3_V2_N_REG					`A3_SIG_BASE_ADD + 16'h0034
`define A3_V3_N_REG					`A3_SIG_BASE_ADD + 16'h0038

// A_CONNECT 4  ------------------------------------------------- IM
//SYS CLK 

`define A4_IM_DAC_WR_REG				`A4_SYS_BASE_ADD + 16'h0030		// DAC AD5761 : IM

// SIG CLK 

`define A4_TEST_MODE_REG			`A4_SIG_BASE_ADD + 16'h0000
`define A4_TEST_L_DATA_REG			`A4_SIG_BASE_ADD + 16'h0004
`define A4_TEST_H_DATA_REG			`A4_SIG_BASE_ADD + 16'h0008

`define A4_WIDTH_REG				`A4_SIG_BASE_ADD + 16'h0010
`define A4_OFFSET_REG				`A4_SIG_BASE_ADD + 16'h0014

`define A4_V0_REG					`A4_SIG_BASE_ADD + 16'h0020
`define A4_V1_P_REG					`A4_SIG_BASE_ADD + 16'h0024
`define A4_V2_P_REG					`A4_SIG_BASE_ADD + 16'h0028
`define A4_V3_P_REG					`A4_SIG_BASE_ADD + 16'h002C
`define A4_V1_N_REG					`A4_SIG_BASE_ADD + 16'h0030
`define A4_V2_N_REG					`A4_SIG_BASE_ADD + 16'h0034
`define A4_V3_N_REG					`A4_SIG_BASE_ADD + 16'h0038

// A_CONNECT 5  ------------------------------------------------- IM
//SYS CLK 

`define A5_IM_DAC_WR_REG				`A5_SYS_BASE_ADD + 16'h0030		// DAC AD5761 : IM

// SIG CLK 

`define A5_TEST_MODE_REG			`A5_SIG_BASE_ADD + 16'h0000
`define A5_TEST_L_DATA_REG			`A5_SIG_BASE_ADD + 16'h0004
`define A5_TEST_H_DATA_REG			`A5_SIG_BASE_ADD + 16'h0008

`define A5_WIDTH_REG				`A5_SIG_BASE_ADD + 16'h0010
`define A5_OFFSET_REG				`A5_SIG_BASE_ADD + 16'h0014

`define A5_V0_REG					`A5_SIG_BASE_ADD + 16'h0020
`define A5_V1_P_REG					`A5_SIG_BASE_ADD + 16'h0024
`define A5_V2_P_REG					`A5_SIG_BASE_ADD + 16'h0028
`define A5_V3_P_REG					`A5_SIG_BASE_ADD + 16'h002C
`define A5_V1_N_REG					`A5_SIG_BASE_ADD + 16'h0030
`define A5_V2_N_REG					`A5_SIG_BASE_ADD + 16'h0034
`define A5_V3_N_REG					`A5_SIG_BASE_ADD + 16'h0038

// A_CONNECT 6  ------------------------------------------------- PD SYNC
//SYS CLK 
`define A6_PDSYNC_EN_REG				 `A6_SYS_BASE_ADD + 16'h0004
`define A6_DP_WR_REG			         `A6_SYS_BASE_ADD + 16'h0008		// AD5252 : Digital Potential Meter
`define A6_DP_RD_REG				     `A6_SYS_BASE_ADD + 16'h000C		// AD5252

 // SIG CLK 
`define A6_START_WIDTH_THRES_REG		  `A6_SIG_BASE_ADD + 16'h0014
`define A6_PD12_CNT_REG				  `A6_SIG_BASE_ADD + 16'h0018
`define A6_PDTRIG_CNT_REG                `A6_SIG_BASE_ADD + 16'h001C

// A_CONNECT 7  ------------------------------------------------- FPC
//SYS CLK 

`define A7_FPC_EN_REG					`A7_SYS_BASE_ADD  + 16'h0010

`define A7_FPC_DAC_WR_REG				`A7_SYS_BASE_ADD  + 16'h0020	// DAC AD5044 :FPC
`define A7_FPC_DAC_CLR_REG				`A7_SYS_BASE_ADD  + 16'h0024	// DAC AD5044 :FPC

`define A7_FPC10_DAC_WR_REG				`A7_SYS_BASE_ADD  + 16'h0030	// DAC AD5044 :FPC
`define A7_FPC10_DAC_CLR_REG			`A7_SYS_BASE_ADD  + 16'h0034	// DAC AD5044 :FPC

// SIG CLK 


// A_CONNECT 8  ------------------------------------------------- FPC
//SYS CLK 

`define A8_FPC_EN_REG					`A8_SYS_BASE_ADD  + 16'h0010

`define A8_FPC_DAC_WR_REG				`A8_SYS_BASE_ADD  + 16'h0020	// DAC AD5044 :FPC
`define A8_FPC_DAC_CLR_REG				`A8_SYS_BASE_ADD  + 16'h0024	// DAC AD5044 :FPC

`define A8_FPC10_DAC_WR_REG				`A8_SYS_BASE_ADD  + 16'h0030	// DAC AD5044 :FPC
`define A8_FPC10_DAC_CLR_REG			`A8_SYS_BASE_ADD  + 16'h0034	// DAC AD5044 :FPC

// SIG CLK 


// A_CONNECT 9  ------------------------------------------------- PM
//SYS CLK 

`define A9_HMC920_EN_REG				`A9_SYS_BASE_ADD + 16'h0000
`define A9_PMPI_EN_REG					`A9_SYS_BASE_ADD + 16'h0004

`define A9_DAC_WR_REG					`A9_SYS_BASE_ADD + 16'h0008		// DAC AD5761 : PM
`define A9_DAC_RD_REG					`A9_SYS_BASE_ADD + 16'h000C		// DAC AD5761 : PM

`define A9_DP_WR_REG				    `A9_SYS_BASE_ADD + 16'h0010		// AD5252	
`define A9_DP_RD_REG				    `A9_SYS_BASE_ADD + 16'h0014		// AD5252

// SIG CLK 

`define A9_TEST_MODE_REG				`A9_SIG_BASE_ADD + 16'h0000
`define A9_TEST_DATA_REG				`A9_SIG_BASE_ADD + 16'h0004

`define A9_WIDTH_REG					`A9_SIG_BASE_ADD + 16'h0010
`define A9_OFFSET_QUOT_REG			    `A9_SIG_BASE_ADD + 16'h0014
`define A9_OFFSET_REMD_REG		    	`A9_SIG_BASE_ADD + 16'h0018
`define A9_DELAY_TAP_REG				`A9_SIG_BASE_ADD + 16'h001C
`define A9_SIG_POLA_REG				`A9_SIG_BASE_ADD + 16'h0020




// A_CONNECT 10  ------------------------------------------------- PM PI
//SYS CLK 

`define A10_HMC920_EN_REG				`A10_SYS_BASE_ADD + 16'h0000
`define A10_PMPI_EN_REG			    `A10_SYS_BASE_ADD + 16'h0004

`define A10_DAC_WR_REG					`A10_SYS_BASE_ADD + 16'h0008		// DAC AD5761 : PM
`define A10_DAC_RD_REG					`A10_SYS_BASE_ADD + 16'h000C		// DAC AD5761 : PM

`define A10_DP_WR_REG				    `A10_SYS_BASE_ADD + 16'h0010		// AD5252	
`define A10_DP_RD_REG				    `A10_SYS_BASE_ADD + 16'h0014		// AD5252

// SIG CLK 

`define A10_TEST_MODE_REG				`A10_SIG_BASE_ADD + 16'h0000
`define A10_TEST_DATA_REG				`A10_SIG_BASE_ADD + 16'h0004

`define A10_WIDTH_REG					`A10_SIG_BASE_ADD + 16'h0010
`define A10_OFFSET_QUOT_REG			`A10_SIG_BASE_ADD + 16'h0014
`define A10_OFFSET_REMD_REG			`A10_SIG_BASE_ADD + 16'h0018
`define A10_DELAY_TAP_REG				`A10_SIG_BASE_ADD + 16'h001C
`define A10_SIG_POLA_REG				`A10_SIG_BASE_ADD + 16'h0020

// A_Operation  ---------------------------------------------------

`define A_READY_REG					`AOP_SIG_BASE_ADD + 16'h0000
`define A_STOP_REG						`AOP_SIG_BASE_ADD + 16'h0004
`define A_SNAPSHOT_REG					`AOP_SIG_BASE_ADD + 16'h0008
`define A_READY_ERR_REG				`AOP_SIG_BASE_ADD + 16'h000C
`define A_FIFO_STAT_REG				`AOP_SIG_BASE_ADD + 16'h0010
`define A_FIFO_PF_THRES_REG			`AOP_SIG_BASE_ADD + 16'h0014
`define A_FIFO_READ_REG				`AOP_SIG_BASE_ADD + 16'h0018
`define A_SEED_REG						`AOP_SIG_BASE_ADD + 16'h001C
`define A_SIG_THRES1_REG				`AOP_SIG_BASE_ADD + 16'h0020
`define A_SIG_THRES2_REG				`AOP_SIG_BASE_ADD + 16'h0024
`define A_TRIG_TYPE_REG			    `AOP_SIG_BASE_ADD + 16'h0028
`define A_FAKE_START_REG			    `AOP_SIG_BASE_ADD + 16'h002C                

`define A_SIG_TEST_MODE_REG			`AOP_SIG_BASE_ADD + 16'h0030
`define A_SIG0_TEST_DATA_REG			`AOP_SIG_BASE_ADD + 16'h0034
`define A_SIG1_TEST_DATA_REG			`AOP_SIG_BASE_ADD + 16'h0038
`define A_DB1_D_TEST_MODE_REG			`AOP_SIG_BASE_ADD + 16'h0040
`define A_DB1_B_TEST_MODE_REG			`AOP_SIG_BASE_ADD + 16'h0044
`define A_DB1_B_TEST_DATA_REG			`AOP_SIG_BASE_ADD + 16'h0048
`define A_DB1_D_TEST_DATA_REG			`AOP_SIG_BASE_ADD + 16'h004C
`define A_DB2_D_TEST_MODE_REG			`AOP_SIG_BASE_ADD + 16'h0050
`define A_DB2_B_TEST_MODE_REG			`AOP_SIG_BASE_ADD + 16'h0054
`define A_DB2_B_TEST_DATA_REG			`AOP_SIG_BASE_ADD + 16'h0058
`define A_DB2_D_TEST_DATA_REG			`AOP_SIG_BASE_ADD + 16'h005C
`define A_PM1_TEST_MODE_REG			`AOP_SIG_BASE_ADD + 16'h0060
`define A_PM1_TEST_DATA_REG			`AOP_SIG_BASE_ADD + 16'h0064
`define A_PM2_TEST_MODE_REG			`AOP_SIG_BASE_ADD + 16'h0068
`define A_PM2_TEST_DATA_REG			`AOP_SIG_BASE_ADD + 16'h006C

//-------------------------------------drbg--------------------------------------
`define A_DRBG_MODE_REG                `AOP_SIG_BASE_ADD + 16'h0070
`define A_ETP_CNT_REG                  `AOP_SIG_BASE_ADD + 16'h0074
`define A_DRBG_CNT_REG                 `AOP_SIG_BASE_ADD + 16'h0078
`define A_ETP_DRBG_FIFO_STAT_REG       `AOP_SIG_BASE_ADD + 16'h007C
`define A_ETP_VERI_READ_REG            `AOP_SIG_BASE_ADD + 16'h0080
`define A_DRBG_VERI_READ_REG           `AOP_SIG_BASE_ADD + 16'h0084
//-------------------------------------------------------------------------------
`define A_L_TEST_MODE_REG               0   // do not delete
`define A_L_TEST_DATA_REG               0   // do not delete 
// --------------------------------------------------------------------
// ---  Bob
// --------------------------------------------------------------------

`define B1_SYS_BASE_ADD				`B_SYS_BASE_ADD + 16'h0100
`define B2_SYS_BASE_ADD				`B_SYS_BASE_ADD + 16'h0200
`define B3_SYS_BASE_ADD				`B_SYS_BASE_ADD + 16'h0300
`define B4_SYS_BASE_ADD				`B_SYS_BASE_ADD + 16'h0400
`define B5_SYS_BASE_ADD				`B_SYS_BASE_ADD + 16'h0500
`define B6_SYS_BASE_ADD				`B_SYS_BASE_ADD + 16'h0600
`define B7_SYS_BASE_ADD				`B_SYS_BASE_ADD + 16'h0700
`define B8_SYS_BASE_ADD				`B_SYS_BASE_ADD + 16'h0800
`define B9_SYS_BASE_ADD				`B_SYS_BASE_ADD + 16'h0900
`define B10_SYS_BASE_ADD			`B_SYS_BASE_ADD + 16'h0A00

`define B1_SIG_BASE_ADD				`B_SIG_BASE_ADD + 16'h0100
`define B2_SIG_BASE_ADD				`B_SIG_BASE_ADD + 16'h0200
`define B3_SIG_BASE_ADD				`B_SIG_BASE_ADD + 16'h0300
`define B4_SIG_BASE_ADD				`B_SIG_BASE_ADD + 16'h0400
`define B5_SIG_BASE_ADD				`B_SIG_BASE_ADD + 16'h0500
`define B6_SIG_BASE_ADD				`B_SIG_BASE_ADD + 16'h0600
`define B7_SIG_BASE_ADD				`B_SIG_BASE_ADD + 16'h0700
`define B8_SIG_BASE_ADD				`B_SIG_BASE_ADD + 16'h0800
`define B9_SIG_BASE_ADD				`B_SIG_BASE_ADD + 16'h0900
`define B10_SIG_BASE_ADD			`B_SIG_BASE_ADD + 16'h0A00
`define BOP_SIG_BASE_ADD			`B_SIG_BASE_ADD + 16'h0B00

// B_CONNECT 1  ------------------------------------------------- LD SIG
//SYS CLK 

    //`define B1_HMC920_EN_REG				`B1_SYS_BASE_ADD + 16'h0000

 // SIG CLK 

    //`define B1_TEST_MODE_REG				`B1_SIG_BASE_ADD + 16'h0000


// B_CONNECT 2  ------------------------------------------------- SPAD
//SYS CLK 

    //`define B2_HMC920_EN_REG				`B2_SYS_BASE_ADD + 16'h0000


 // SIG CLK 

    //`define B2_WIDTH_REG					`B2_SIG_BASE_ADD + 16'h0010


// B_CONNECT 3  ------------------------------------------------- IM
//SYS CLK 

    //`define B3_IM_DAC_WR_REG				`B3_SYS_BASE_ADD + 16'h0030		// DAC AD5761 : IM

// SIG CLK 

    //`define B3_TEST_MODE_REG				`B3_SIG_BASE_ADD + 16'h0000


// B_CONNECT 4  ------------------------------------------------- PM PI/2
//SYS CLK 

    //`define B4_PM_H_EN_REG					`B4_SYS_BASE_ADD + 16'h0000


// SIG CLK 

    //`define B4_TEST_MODE_REG				`B4_SIG_BASE_ADD + 16'h0000


// B_CONNECT 5  ------------------------------------------------- IM
//SYS CLK 

    //`define B5_IM_DAC_WR_REG				`B5_SYS_BASE_ADD + 16'h0030		// DAC AD5761 : IM

// SIG CLK 

    //`define B5_TEST_MODE_REG				`B5_SIG_BASE_ADD + 16'h0000


// B_CONNECT 6  ------------------------------------------------- PD SYNC
//SYS CLK 
    //`define B6_PDSYNC_EN_REG				 `B6_SYS_BASE_ADD + 16'h0004


// B_CONNECT 7  ------------------------------------------------- PM PI/2
//SYS CLK 

    //`define B7_PM_H_EN_REG					`B7_SYS_BASE_ADD + 16'h0000


// SIG CLK 

    //`define B7_TEST_MODE_REG				`B7_SIG_BASE_ADD + 16'h0000



// B_CONNECT 8  ------------------------------------------------- PM PI
//SYS CLK 

    //`define B8_HMC920_EN_REG				`B8_SYS_BASE_ADD + 16'h0000


// SIG CLK 

    //`define B8_TEST_MODE_REG				`B8_SIG_BASE_ADD + 16'h0000



// B_CONNECT 9  ------------------------------------------------- PD ADC
//SYS CLK 
    //`define B9_AD9608_CONF_REG				`B9_SYS_BASE_ADD  + 16'h0000		// ADC AD9608


// SIG CLK 
    //`define B9_ADC_NSAMP_REG				`B9_SIG_BASE_ADD + 16'h0000


// B_CONNECT 10  ------------------------------------------------- PM PI
//SYS CLK 

    //`define B10_HMC920_EN_REG				`B10_SYS_BASE_ADD + 16'h0000


// SIG CLK 

    //`define B10_TEST_MODE_REG				`B10_SIG_BASE_ADD + 16'h0000


// B_Operation  ---------------------------------------------------

`define B_START_REG					`BOP_SIG_BASE_ADD + 16'h0000
`define B_STOP_REG						`BOP_SIG_BASE_ADD + 16'h0004
`define B_SNAPSHOT_REG					`BOP_SIG_BASE_ADD + 16'h0008
`define B_READY_ERR_REG				`BOP_SIG_BASE_ADD + 16'h000C
`define B_FIFO_STAT_REG				`BOP_SIG_BASE_ADD + 16'h0020
`define B_FIFO_PF_THRES_REG			`BOP_SIG_BASE_ADD + 16'h0024
`define B_FIFO_READ_REG				`BOP_SIG_BASE_ADD + 16'h0028
`define B_SEED_REG						`BOP_SIG_BASE_ADD + 16'h0030
`define B_SIG_THRES1_REG				`BOP_SIG_BASE_ADD + 16'h0034
`define B_SIG_THRES2_REG				`BOP_SIG_BASE_ADD + 16'h0038

`define B_SIG_TEST_MODE_REG			`BOP_SIG_BASE_ADD + 16'h0040
`define B_SIG0_TEST_DATA_REG			`BOP_SIG_BASE_ADD + 16'h0044
`define B_SIG1_TEST_DATA_REG			`BOP_SIG_BASE_ADD + 16'h0048
`define B_DATB_TEST_MODE_REG			`BOP_SIG_BASE_ADD + 16'h0060
`define B_DATB_TEST_DATA_REG			`BOP_SIG_BASE_ADD + 16'h0064
`define B_PM_TEST_MODE_REG				`BOP_SIG_BASE_ADD + 16'h0070
`define B_PM_TEST_DATA_REG				`BOP_SIG_BASE_ADD + 16'h0074


// ----------------------------------------------------------------------
// PCIE DOMAIN REGISTER (offset 0xD00)
// ----------------------------------------------------------------------
`define PCIE_DMA_BASE_ADD				`BSC_PCIE_ADDR_START

`define DMA_DATA_SEL_REG				`PCIE_DMA_BASE_ADD + 16'h0000
`define DMA_FIFO_EMPTY_REG				`PCIE_DMA_BASE_ADD + 16'h0004

`define PCIE_DMA_C2H_0_CTRL_REG		`PCIE_DMA_BASE_ADD + 16'h0080
`define PCIE_DMA_C2H_0_SRCADDR_REG		`PCIE_DMA_BASE_ADD + 16'h0084
`define PCIE_DMA_C2H_0_LEN_REG			`PCIE_DMA_BASE_ADD + 16'h0088
`define PCIE_DMA_C2H_0_STAT_REG		`PCIE_DMA_BASE_ADD + 16'h008C
`define PCIE_DMA_C2H_0_BUSY_CNT_REG	`PCIE_DMA_BASE_ADD + 16'h0090
`define PCIE_DMA_C2H_0_RUN_CNT_REG		`PCIE_DMA_BASE_ADD + 16'h0094
`define PCIE_DMA_C2H_0_PACKET_CNT_REG	`PCIE_DMA_BASE_ADD + 16'h0098
`define PCIE_DMA_C2H_0_DESC_CNT_REG	`PCIE_DMA_BASE_ADD + 16'h009C

`define PCIE_DMA_H2C_0_STAT_REG		`PCIE_DMA_BASE_ADD + 16'h00AC
`define PCIE_DMA_BUFFER_FULL			`PCIE_DMA_BASE_ADD + 16'h00A0    
`define DMA_BUFFER_PROG_THRESH      	`PCIE_DMA_BASE_ADD + 16'h00A4 

`define PCIE_DMA_C2H_SRCADDR_DEF		0
`define DMA_LEN_DEF					4096


// ---------------------------------------------------------------------------------
// MASK and Bit shift & CONSTANT VALUE
// ---------------------------------------------------------------------------------
// MCLK
`define SIGCLKPERSEC						'd100000000

// CLK define
`define GRST_CYCLE_SYSCLK				10
`define C2H_RST_PCIECLK					10		// 8*10 = 80
`define C2H_RST_MCLK					16		// 5*16 = 80
//`define CLKSEL_RST_SYSCLK				20

/* // CLK Manage (CLK_MUX_REG)
`define CLK_MUX_CLKA					'd0
`define CLK_MUX_CLKB					'd1
`define CLK_MUX_PCIE					'd3 */


// ---------------------------------------------------------------------------------
// DEFAULT VALUE
// ---------------------------------------------------------------------------------
// CLK Manage (CLK_MUX_REG)
`define CLK_MUX_DEF						`CLK_MUX_PCIE

// FAN_HIGH_DURA_REG
`define FAN_HIGH_DURA_DEF				4000		// 5000 ?�� 배분 : duty 20% ?���? High 1000, Low 4000, 40%?���? High 2000, Low 3000
// FAN_LOW_DURA_REG
`define FAN_LOW_DURA_DEF				1000

// LD
`define LD_PERIOD_DEF					20
`define LD_WIDTH_DEF					8
`define LD_ADD_WIDTH_DEF				4
`define LD_OFFSET_DEF					4

// POC
`define ALICE_POC_PERIOD_DEF			20
`define ALICE_POC_WIDTH_DEF				4
`define ALICE_POC_OFFSET_DEF			8

// Trig Mask ?�� 10MHz(100ns)?���?�? ?��쳐서 5 ?��?��?? ?���?
`define ALICE_TM_WIDTH_DEF				3
`define ALICE_TM_OFFSET_DEF				0

// RND
`define MSG_TYPE_DEF					2'b0		// 0 : Random 1: all 0, 2:all 1,  3: Random
`define MSG_SEED_DEF					'hffff0000
`define RND_SEED_DEF					'hffff0001


// DMA DATA Selectiong
`define PCIE_DMA_SRC_FIFO				2'b10

// --------------------------------------------------------------------
// For App programming
// --------------------------------------------------------------------
`define PRODUCT_WORD_MAX_LENGTH			12
`define PCIECLKMHZ						125


