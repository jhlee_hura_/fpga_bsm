`timescale	1ns/100ps
//////////////////////////////////////////////////////////////////////////////////
// Company: HURA
// Engineer: LEE Seong Yun (sylee@hura.co.kr)
//
// Create Date: 2017/02/22 13:11:00
// Design Name: LFSR Random generation (0�� �ȳ���. uniform ����)
// Module Name: LFSR
// Project Name:
// Target Devices: Kintex7 410T
// Tool Versions: Vivado 2015.2
// Description:
//
// Dependencies:
//
// Revision: build 2(2017/02/22) : Table Change Ref Table of LFSR (Roy Ward, Tim Molteno)
// Additional Comments:
//
// Copyright 2018, All rights reserved by HURA
//////////////////////////////////////////////////////////////////////////////////
module lfsr #(
	parameter NBIT = 16
) (
	input wire clk,
	input wire sclr,
	input wire en,
	input wire [NBIT-1:0]	seed,
	input wire				seed_valid,
	output wire [NBIT-1:0]	dout,
	output wire				dout_valid
); // latency = 1

reg [NBIT-1:0]	random;
reg				random_valid;

wire feedback;
generate
	if (NBIT == 3)
		assign feedback = random[2] ^ random[1];
	else if (NBIT == 4)
		assign feedback = random[3] ^ random[2];
	else if (NBIT == 5)
		assign feedback = random[4] ^ random[2];
	else if (NBIT == 6)
		assign feedback = random[5] ^ random[4];
	else if (NBIT == 7)
		assign feedback = random[6] ^ random[5];
	else if (NBIT == 8)
		assign feedback = random[7] ^ random[5] ^ random[4] ^ random[3];
	else if (NBIT == 9)
		assign feedback = random[8] ^ random[4];
	else if (NBIT == 10)
		assign feedback = random[9] ^ random[6];
	else if (NBIT == 11)
		assign feedback = random[10] ^ random[8];
	else if (NBIT == 12)
		assign feedback = random[11] ^ random[10] ^ random[7] ^ random[5];
	else if (NBIT == 13)
		assign feedback = random[12] ^ random[11] ^ random[9] ^ random[8];
	else if (NBIT == 14)
		assign feedback = random[13] ^ random[12] ^ random[10] ^ random[8];
	else if (NBIT == 15)
		assign feedback = random[14] ^ random[13];
	else if (NBIT == 16)
		assign feedback = random[15] ^ random[13] ^ random[12] ^ random[10];
	else if (NBIT == 17)
		assign feedback = random[16] ^ random[13];
	else if (NBIT == 18)
		assign feedback = random[17] ^ random[10];
	else if (NBIT == 19)
		assign feedback = random[18] ^ random[17] ^ random[16] ^ random[13];
	else if (NBIT == 20)
		assign feedback = random[19] ^ random[16];
	else if (NBIT == 21)
		assign feedback = random[20] ^ random[18];
	else if (NBIT == 22)
		assign feedback = random[21] ^ random[20];
	else if (NBIT == 23)
		assign feedback = random[22] ^ random[17];
	else if (NBIT == 24)
		assign feedback = random[23] ^ random[22] ^ random[20] ^ random[19];
	else if (NBIT == 25)
		assign feedback = random[24] ^ random[21];
	else if (NBIT == 26)
		assign feedback = random[25] ^ random[24] ^ random[23] ^ random[19];
	else if (NBIT == 27)
		assign feedback = random[26] ^ random[25] ^ random[24] ^ random[21];
	else if (NBIT == 28)
		assign feedback = random[27] ^ random[24];
	else if (NBIT == 29)
		assign feedback = random[28] ^ random[26];
	else if (NBIT == 30)
		assign feedback = random[29] ^ random[28] ^ random[25] ^ random[23];
	else if (NBIT == 31)
		assign feedback = random[30] ^ random[27];
	else if (NBIT == 32)
		assign feedback = random[31] ^ random[29] ^ random[25] ^ random[24];
	else if (NBIT == 33)
		assign feedback = random[32] ^ random [19];
	else if (NBIT == 34)
		assign feedback = random[33] ^ random[30] ^ random[29] ^ random[25];
	else if (NBIT == 35)
		assign feedback = random[34] ^ random[32];
	else if (NBIT == 36)
		assign feedback = random[35] ^ random[24];
	else if (NBIT == 37)
		assign feedback = random[36] ^ random[35] ^ random[32] ^ random[30];
	else if (NBIT == 38)
		assign feedback = random[37] ^ random[36] ^ random[32] ^ random[31];
	else if (NBIT == 39)
		assign feedback = random[38] ^ random[34];
	else if (NBIT == 40)
		assign feedback = random[39] ^ random[36] ^ random[35] ^ random[34];
	else if (NBIT == 41)
		assign feedback = random[40] ^ random[37];
	else if (NBIT == 42)
		assign feedback = random[41] ^ random[39] ^ random[36] ^ random[34];
	else if (NBIT == 43)
		assign feedback = random[42] ^ random[41] ^ random[37] ^ random[36];
	else if (NBIT == 44)
		assign feedback = random[43] ^ random[41] ^ random[38] ^ random[37];
	else if (NBIT == 45)
		assign feedback = random[44] ^ random[43] ^ random[41] ^ random[40];
	else if (NBIT == 46)
		assign feedback = random[45] ^ random[39] ^ random[38] ^ random[37];
	else if (NBIT == 47)
		assign feedback = random[46] ^ random[41];
	else if (NBIT == 48)
		assign feedback = random[47] ^ random[43] ^ random[40] ^ random[38];
	else if (NBIT == 49)
		assign feedback = random[48] ^ random[39];
	else if (NBIT == 50)
		assign feedback = random[49] ^ random[47] ^ random[46] ^ random[45];
	else if (NBIT == 51)
		assign feedback = random[50] ^ random[49] ^ random[47] ^ random[44];
	else if (NBIT == 52)
		assign feedback = random[51] ^ random[48];
	else if (NBIT == 53)
		assign feedback = random[52] ^ random[51] ^ random[50] ^ random[46];
	else if (NBIT == 54)
		assign feedback = random[53] ^ random[50] ^ random[47] ^ random[45];
	else if (NBIT == 55)
		assign feedback = random[54] ^ random[30];
	else if (NBIT == 56)
		assign feedback = random[55] ^ random[53] ^ random[51] ^ random[48];
	else if (NBIT == 57)
		assign feedback = random[56] ^ random[49];
	else if (NBIT == 58)
		assign feedback = random[57] ^ random[38];
	else if (NBIT == 59)
		assign feedback = random[58] ^ random[56] ^ random[54] ^ random[51];
	else if (NBIT == 60)
		assign feedback = random[59] ^ random[58];
	else if (NBIT == 61)
		assign feedback = random[60] ^ random[59] ^ random[58] ^ random[55];
	else if (NBIT == 62)
		assign feedback = random[61] ^ random[58] ^ random[56] ^ random[55];
	else if (NBIT == 63)
		assign feedback = random[62] ^ random[61];
	else if (NBIT == 64)
		assign feedback = random[63] ^ random[62] ^ random[60] ^ random[59];
	else if (NBIT == 65)
		assign feedback = random[64] ^ random[46];
	else if (NBIT == 66)
		assign feedback = random[65] ^ random[59] ^ random[57] ^ random[56];
	else if (NBIT == 67)
		assign feedback = random[66] ^ random[65] ^ random[64] ^ random[61];
	else if (NBIT == 68)
		assign feedback = random[67] ^ random[58];
	else if (NBIT == 69)
		assign feedback = random[68] ^ random[66] ^ random[63] ^ random[62];
	else if (NBIT == 70)
		assign feedback = random[69] ^ random[68] ^ random[66] ^ random[64];
	else if (NBIT == 71)
		assign feedback = random[70] ^ random[64];
	else if (NBIT == 72)
		assign feedback = random[71] ^ random[68] ^ random[62] ^ random[61];
	else if (NBIT == 73)
		assign feedback = random[72] ^ random[47];	
	else if (NBIT == 74)
		assign feedback = random[73] ^ random[70] ^ random[69] ^ random[66];
	else if (NBIT == 75)
		assign feedback = random[74] ^ random[73] ^ random[71] ^ random[68];
	else if (NBIT == 76)
		assign feedback = random[75] ^ random[73] ^ random[71] ^ random[70];
	else if (NBIT == 77)
		assign feedback = random[76] ^ random[74] ^ random[71] ^ random[70];
	else if (NBIT == 78)
		assign feedback = random[77] ^ random[76] ^ random[75] ^ random[70];
	else if (NBIT == 79)
		assign feedback = random[78] ^ random[69];
	else if (NBIT == 80)
		assign feedback = random[79] ^ random[77] ^ random[75] ^ random[70];
	else if (NBIT == 81)
		assign feedback = random[80] ^ random[76];			
	else
		assign feedback = 1'b0;
endgenerate


always @ (posedge clk) begin
	if (sclr || seed_valid) begin
		random		<= (seed == 'b0) ? -'sd1 : seed; //An LFSR cannot have an all 0 state, thus reset to FF
		random_valid	<= 1'b0;
	end
	else begin
		if (en) begin
			random		<= {random[NBIT-2:0], feedback};
			random_valid	<= 1'b1;
		end
		else begin
			random		<= random;
			random_valid	<= 1'b0;
		end
	end
end

assign dout = random;
assign dout_valid = random_valid;
endmodule
