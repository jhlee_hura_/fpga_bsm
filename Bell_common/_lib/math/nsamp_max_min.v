//////////////////////////////////////////////////////////////////////////////////
// Company: HURA
// Engineer: LEE Seongyun (sylee@hura.co.kr)
//
// Create Date: 2018/07/11 10:12:00
// Design Name: MIN-MAX-AVG
// Module Name:
// Project Name: QDC
// Target Devices: Kintex7 160T
// Tool Versions: Vivado 2018.1
// Description:
//
// Dependencies:
//
// Revision: build 1(2018/07/11)
// Additional Comments:
//
// Copyright 2018 (c) HURA. All rights reserved.
//////////////////////////////////////////////////////////////////////////////////
module nsamp_max_min  #(
	parameter NBIT = 10,
	parameter SIGNED = 0,		// 1: signed 0:unsigned
	parameter LEN_BIT = 32
)(

	input wire clk,
	input wire sclr,

	input wire [NBIT-1:0] 	din,
	input wire 				din_valid,

	input wire [LEN_BIT-1:0]  	nsamp,

	output reg [NBIT-1:0]		dout_max		='b0,
	output reg [LEN_BIT-1:0]	dout_max_idx	='b0,
	output reg [NBIT-1:0]		dout_min		='b0,
	output reg [LEN_BIT-1:0]	dout_min_idx	='b0,
	output reg 					dout_valid		='b0
);


// ------------------------------------------------------------------------------
reg [NBIT-1:0] 			max_value='b0;
wire max;
reg [NBIT-1:0] 			min_value='b0;
wire min;

generate
if (SIGNED) begin
	assign max = 	(din[NBIT-1] != max_value[NBIT-1]) ? max_value[NBIT-1] :
					((din[NBIT-2:0] > max_value[NBIT-2:0])) ? 1'b1 : 1'b0;
	assign min = 	(din[NBIT-1] != min_value[NBIT-1]) ? din[NBIT-1] :
					((din[NBIT-2:0] < min_value[NBIT-2:0])) ? 1'b1 : 1'b0;
end
else begin
	assign max = (din[NBIT-1:0] > max_value[NBIT-1:0]) ? 1'b1 : 1'b0;
	assign min = (din[NBIT-1:0] < min_value[NBIT-1:0]) ? 1'b1 : 1'b0;
end
endgenerate

// ------------------------------------------------------------------------------


localparam first_run_state = 1'b0;
localparam run_state = 1'b1;

reg [LEN_BIT-1:0] 	cnt_sample = 'b0;
reg [LEN_BIT-1:0]	max_idx='b0;
reg [LEN_BIT-1:0]	min_idx='b0;
reg [LEN_BIT-1:0]	nsamp_r;

reg [0:0] state = first_run_state;

always @(posedge clk) begin
	if (sclr) begin
		nsamp_r			<= 'b0;
		state 			<= first_run_state;
	end
	else begin
		if (din_valid) begin
			case (state)
				first_run_state : begin			// 일반 서브블럭의 처음.
					nsamp_r	<= (nsamp<=1) ? 0 : nsamp - 1;
					dout_max 		<= max_value;
					dout_max_idx 	<= max_idx;
					dout_min 		<= min_value;
					dout_min_idx 	<= min_idx;
					dout_valid		<= (nsamp_r=='b0) ? 1'b0 : 1'b1;
					max_value 		<= din;
					max_idx 		<= 'b0;
					min_value 		<= din;
					min_idx 		<= 'b0;
					cnt_sample 		<= 'b1;
					state 			<= run_state;
				end

				run_state : begin		// 일반 서브블럭의 중간.
					dout_valid		<= 1'b0;
					if (max) begin
						max_value 	<= din;
						max_idx 	<= cnt_sample;
					end
					if (min) begin
						min_value 	<= din;
						min_idx 	<= cnt_sample;
					end

					if (cnt_sample >= nsamp_r) begin
						cnt_sample 	<= 'b0;
						state 		<= first_run_state;
					end
					else begin
						cnt_sample 	<= cnt_sample + 1'b1;
						state 		<= run_state;
					end
				end
				default : begin
						cnt_sample 	<= 'b0;
						state 	<= first_run_state;
				end
			endcase
		end
	end
end

/* always @(posedge clk) begin
	if ((state == first_run_state && din_valid)) begin
		dout_max 		<= max_value;
		dout_max_idx 	<= max_idx;
		dout_min 		<= min_value;
		dout_min_idx 	<= min_idx;
		dout_valid		<= 1'b1;
	end
	else begin
		dout_max 		<= dout_max;
		dout_max_idx 	<= dout_max_idx;
		dout_min 		<= dout_min;
		dout_min_idx 	<= dout_min_idx;
		dout_valid		<= 1'b0;
	end
end */


endmodule
