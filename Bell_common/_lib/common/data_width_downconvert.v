`timescale 1ns / 100ps
//////////////////////////////////////////////////////////////////////////////////
// Company: HURA
// Engineer: LEE Seong Yun (sylee@hura.co.kr)
//
// Create Date: 2018/08/12
// Design Name:
// Module Name: data_width_downconvert
// Project Name:
// Target Devices: ku060-ffva1156
// Tool Versions: Vivado 2018.1
// Description:
//
// Dependencies:
//
// Revision build 1(2018/08/12)
// Additional Comments:
//
// Copyright 2018 (c) HURA. All rights reserved.
//////////////////////////////////////////////////////////////////////////////////
module data_width_downconvert # (
	parameter DIN_WIDTH = 64,
	parameter DOUT_WIDTH = 2,
	parameter ENDIAN = 1 // 0 : little endian , 1: big endian
) (
	input wire 					clk,
	input wire 					sclr,
	input wire [DIN_WIDTH-1:0]	din,
	input wire 					din_valid,
	output wire 				din_ready,

	output reg [DOUT_WIDTH-1:0] 	dout,
	output reg						dout_valid,
	input wire						dout_ready
);
// align : lsb align ?��?��?��.

localparam RATIO = DIN_WIDTH/DOUT_WIDTH;  //32
localparam MAX_BIT_SHIFT = (DOUT_WIDTH*RATIO == DIN_WIDTH) ? (DOUT_WIDTH * (RATIO-1)) : (DOUT_WIDTH * RATIO); //62
localparam DIN_SHIFT_INIT = (ENDIAN) ? MAX_BIT_SHIFT : 'b0;  //62
localparam SHIFT_NBITS = 	(DIN_WIDTH <= 2) ? 1 :   //9
							(DIN_WIDTH <= 4) ? 2 :
							(DIN_WIDTH <= 8) ? 3 :
							(DIN_WIDTH <= 16) ? 4 :
							(DIN_WIDTH <= 32) ? 5 :
							(DIN_WIDTH <= 64) ? 6 :
							(DIN_WIDTH <= 128) ? 7 :
							(DIN_WIDTH <= 256) ? 8 :
							(DIN_WIDTH <= 512) ? 9 :
							(DIN_WIDTH <= 1024) ? 10 :
							(DIN_WIDTH <= 2048) ? 11 :
							(DIN_WIDTH <= 4096) ? 12 :
							(DIN_WIDTH <= 8192) ? 13 :
							(DIN_WIDTH <= 16384) ? 14 :
							(DIN_WIDTH <= 32768) ? 15 :
							(DIN_WIDTH <= 65536) ? 16 : 32;



localparam IDLE_STAT = 1'b0;
localparam CONV_STAT = 1'b1;

reg [DIN_WIDTH-1:0] 	din_d='b0;
reg [DIN_WIDTH-1:0] 	din_d1='b0;
reg [0:0]				status = IDLE_STAT;
reg [SHIFT_NBITS-1:0] 	din_shift_bit='b0;

// input data control
reg 		sclr_flag_n;
reg din_full;
assign din_ready = ~din_full;
always @(posedge clk) begin
	if (sclr) begin
		din_d 		<= 'b0;
		din_full 	<= 1'b1;
	end
	else begin
		if (din_valid && din_ready) begin
			din_d <= din;
			din_full <= 1'b1;
		end
		if ((status == IDLE_STAT && din_full && dout_ready) || (din_full && !sclr_flag_n)) begin
			din_full <= 1'b0;
		end
	end
end

// output data control
always @(posedge clk) begin
	if (sclr) begin
		dout <= 'b0;
		dout_valid <= 1'b0;
		din_d1 <= 'b0;
		sclr_flag_n <= 1'b0;
		din_shift_bit <= DIN_SHIFT_INIT;
		status <= IDLE_STAT;
	end
	else begin
		case (status)
			IDLE_STAT : begin
				sclr_flag_n <= 1'b1;
				if (din_full && sclr_flag_n && dout_ready) begin
					din_d1 <= din_d;
					din_shift_bit <= (ENDIAN) ? (DIN_SHIFT_INIT - DOUT_WIDTH) : (DIN_SHIFT_INIT + DOUT_WIDTH);   //60
					dout <= (din_d >> DIN_SHIFT_INIT);
					dout_valid <= 1'b1;
					status <= CONV_STAT;
				end
				else begin
					din_d1 <= din_d1;
					dout_valid <= (dout_ready) ? 1'b0 : dout_valid;
					status <= IDLE_STAT;
				end
			end
			CONV_STAT : begin
				if (dout_ready) begin
					dout <= (din_d1 >> din_shift_bit);
					dout_valid <= 1'b1;
					if (ENDIAN) begin
						din_shift_bit <= (din_shift_bit - DOUT_WIDTH);
						if (din_shift_bit == 'b0)
							status <= IDLE_STAT;
						else
							status <= CONV_STAT;
					end
					else begin
						din_shift_bit <= (din_shift_bit + DOUT_WIDTH);
						if (din_shift_bit >= MAX_BIT_SHIFT)
							status <= IDLE_STAT;
						else
							status <= CONV_STAT;
					end
				end
				else begin
					dout <= dout;
					dout_valid <= dout_valid;
					din_shift_bit <= din_shift_bit;
				end
			end
		endcase
	end
end


endmodule


