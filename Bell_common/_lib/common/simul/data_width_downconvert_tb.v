`timescale 1ns / 100ps
//////////////////////////////////////////////////////////////////////////////////
// Company: HURA
// Engineer: LEE Seong Yun (sylee@hura.co.kr)
//
// Create Date: 2018/08/12
// Design Name:
// Module Name: data_width_downconvert test-bed
// Project Name:
// Target Devices: Kintex7 410T
// Tool Versions: Vivado 2018.1
// Description:
//
// Dependencies:
//
// Revision build 1(2018/08/12)
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////
module data_width_downconvert_tb;

	parameter DIN_WIDTH = 32;
	parameter DOUT_WIDTH = 8;
	parameter ENDIAN = 0; // 0 : little endian , 1: big endian


	reg 					clk;
	reg	 					sclr;
	reg						dma_init='b0;

	reg [DIN_WIDTH-1:0]		din;
	reg 					din_valid;
	wire 					din_ready;

	wire [DOUT_WIDTH-1:0] 	dout;
	wire					dout_valid;
	reg						dout_ready;

	// for testbench
	reg			[31:0]		tb_count;


// ------------------------------------------------------------------
data_width_downconvert # (
	.DIN_WIDTH	(DIN_WIDTH),
	.DOUT_WIDTH	(DOUT_WIDTH),
	.ENDIAN		(ENDIAN) // 0 : little endian , 1: big endian
) data_width_downconvert (
	.clk(clk),					// I
	.sclr(sclr || dma_init),				// I
	.din(din),					// I [DIN_WIDTH-1:0]
	.din_valid(din_valid),		// I
	.din_ready(din_ready),		// O

	.dout(dout),				// O [DOUT_WIDTH-1:0]
	.dout_valid(dout_valid),	// O
	.dout_ready(dout_ready)		// I
);

// ---------------------------------------------------------------------------------
// basic signal generation

	initial begin
		tb_count=0;
		clk = 1;
	end

	always #(5) clk = ~clk;		// 100 MHz

	always #(10) tb_count=tb_count+1;

 	initial begin
		sclr = 1'b1;
		#200;
		sclr = 1'b0;

//		#16000
//		sclr = 1'b1;
//		#200;
//		sclr = 1'b0;
	end

// ---------------------------------------------------------------------------------


reg [DIN_WIDTH-1:0]		din_init_value;
reg				din_valid_flag;
initial
begin
	din_init_value = 'h7060504230201000;
	dout_ready <= 1'b0;
	din_valid_flag <= 1'b0;
	#100
	dout_ready <= #1 1'b1;
	din_valid_flag <= #1 1'b1;
	#100
	dout_ready <= #1 1'b0;
	#20
	dout_ready <= #1 1'b1;
	#20
	dout_ready <= #1 1'b0;
	#40
	dout_ready <= #1 1'b1;
	#20
	dout_ready <= #1 1'b0;
	#10
	dout_ready <= #1 1'b1;
	#10
	dout_ready <= #1 1'b0;
	#10
	dout_ready <= #1 1'b1;
	#10
	dout_ready <= #1 1'b0;
	#10
	dout_ready <= #1 1'b1;	
	#10
	dout_ready <= #1 1'b0;
	#10
	dout_ready <= #1 1'b1;	
	#10
	dout_ready <= #1 1'b0;
	#30
	dout_ready <= #1 1'b1;
	#10
	dout_ready <= #1 1'b0;
	#50
	dout_ready <= #1 1'b1;
		
	#330 
	dma_init <= 1'b1;
	dout_ready <= #1 1'b0;
	#10
	dma_init <= 1'b0;
	#30
	dout_ready <= #1 1'b1;
	#200

	
	
	#2	
	din_valid_flag <= #1 1'b0;
	#200                  
	din_valid_flag <= #1 1'b1;		
	#40                  
	din_valid_flag <= #1 1'b0;	
	#20                  
	din_valid_flag <= #1 1'b1;		
end

// ---------------------------------------------------------------------------------
// Sync signal

always @(posedge clk) begin
	if (sclr) begin
		din <= din_init_value;
		din_valid <= 1'b0;
	end
	else begin
		din <= (din_valid && din_ready) ? din + 'h01010101 : din;
		din_valid <= (din_valid_flag) ? 1'b1 : 1'b0;
	end

end



endmodule