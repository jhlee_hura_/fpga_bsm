`timescale 1ns / 100ps
//////////////////////////////////////////////////////////////////////////////////
// Company: HURA
// Engineer: LEE Seong Yun (sylee@hura.co.kr)
//
// Create Date: 2018/08/12
// Design Name:
// Module Name: delay_reg test-bed
// Project Name:
// Target Devices: Kintex7 410T
// Tool Versions: Vivado 2018.1
// Description:
//
// Dependencies:
//
// Revision build 1(2018/08/12)
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////
module delay_reg_tb;

	parameter DIN_WIDTH = 4;
	parameter DELAY = 13;

	reg 					clk;
	reg	 					sclr;

	reg [DIN_WIDTH-1:0]		din;

	wire [DIN_WIDTH-1:0] 	dout;

	// for testbench
	reg			[31:0]		tb_count;


// ------------------------------------------------------------------
delay_regi # (
	.DIN_WIDTH	(DIN_WIDTH),
	.DELAY	(DELAY)
) delay_regi (
	.clk(clk),					// I
	.sclr(sclr),				// I
	.din(din),					// I [DIN_WIDTH-1:0]
	.dout(dout)				// O [DOUT_WIDTH-1:0]
);

reg [DELAY-1:0] 	dlast_d='b0;		//delay valid data  13clk
wire 		dlast_cmp;
always @(posedge clk)
		dlast_d <= {dlast_d[DELAY-2:0], din[0]} ;

assign dlast_cmp = dlast_d[DELAY-1];

// ---------------------------------------------------------------------------------
// basic signal generation

	initial begin
		tb_count=0;
		clk = 1;
	end

	always #(5) clk = ~clk;		// 100 MHz

	always #(10) tb_count=tb_count+1;

 	initial begin
		sclr = 1'b1;
		#200;
		sclr = 1'b0;

//		#16000
//		sclr = 1'b1;
//		#200;
//		sclr = 1'b0;
	end

// ---------------------------------------------------------------------------------


initial
begin
	din = 'h0;
	#201
	din = 'h1;
	#10
	din = 'h2;
	#10
	din = 'h4;
	#10
	din = 'h3;
	sclr = 1'b1;
	#10
	din = 'h5;
	sclr = 1'b0;
	#10
	din = 'h6;
	#10
	din = 'h7;
	#10
	din = 'h8;
end

// ---------------------------------------------------------------------------------
// Sync signal




endmodule