`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: HURA (Hub of Radio Technology)
// Engineer: LEE Seong Yun (sylee@hura.co.kr)
//
// Create Date: 2015/08/15 16:40:05
// Design Name:
// Module Name: data_merge2to1
// Project Name:
// Target Devices: ku060-ffva1156, MARS X1F board
// Tool Versions: Vivado 2018.1
// Description: N bits 신호를 2*N bits로 변경, din_valid duration 을 2배로
//
// Dependencies:
//
// Revision: build 2(2018/07/16) : port 이름 변경
// Additional Comments:
//
// Copyright 2018 (c) HURA. All rights reserved.
//////////////////////////////////////////////////////////////////////////////////
module data_merge2to1 #(
	parameter NBITS = 32
) (
    input wire clk,
    input wire sclr,
    input wire [NBITS-1:0] din,
    input wire din_valid,
	input wire endian,			// 0 : little endian, 1 : big endian
	output reg [2*NBITS-1:0] dout,   // {[31:0],[31:0]}
	output reg dout_valid
);

reg	count_r;
reg [NBITS-1:0] pre_data;

// getand hold last value of accum. or dout_valid signal
always @(posedge clk) begin
	if (sclr == 1'b1) begin
		dout <= 0;
		pre_data <= 0;
		dout_valid <= 1'b0;
		count_r <= 1'b0;
	end
	else if (din_valid == 1'b1 & count_r == 1'b0) begin
		dout <= dout;
		pre_data <= din;
		dout_valid <= 1'b0;
		count_r <= 1'b1;
	end
	else if (din_valid == 1'b1 & count_r == 1'b1) begin
		if (endian == 1'b1)
			dout <= {pre_data,din};
		else
			dout <= {din,pre_data};
		pre_data <= din;
		dout_valid <= 1'b1;
		count_r <= 1'b0;
	end
	else begin
		dout <= dout;
		pre_data <= pre_data;
		dout_valid <= 1'b0;
		count_r <= count_r;
	end

end

endmodule
