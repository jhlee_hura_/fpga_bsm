`timescale 1ns / 100ps
//////////////////////////////////////////////////////////////////////////////////
// Company: HURA
// Engineer: LEE Seong Yun (sylee@hura.co.kr)
//
// Create Date: 2018/04/25 21:04:48
// Design Name:
// Module Name: adc_ad9608
// Project Name:
// Target Devices:
// Tool Versions: Vivado 2018.1
// Description:
//
// Dependencies:
//
// Revision : build 1(2018/04/25)
// Additional Comments:
//
// Copyright 2018, All rights reserved by HURA
//////////////////////////////////////////////////////////////////////////////////
module adc_ad9608 (
	input wire			adc_clk_in_p,
	input wire			adc_clk_in_n,
	input wire [9:0]	adc_data_in_p,
	input wire [9:0]	adc_data_in_n,
	input wire			adc_or_in_p,
	input wire			adc_or_in_n,

	input wire			adc_ddr_edgesel,

	input wire			sig_clk,			// sig main clk
	output wire			adc_clk,			// adc_clk output
	input wire			adc_dout_en,
	input wire			adc_fifo_rst,
	output wire	[9:0]	adc_data_a,
	output wire	[9:0]	adc_data_b,
	output wire			adc_or_a,
	output wire			adc_or_b
);

wire 	[9:0]	data_a;
wire 	[9:0]	data_b;
wire			or_a;
wire			or_b;

axi_ad9608_if axi_ad9608_if (
    .adc_clk_in_p (adc_clk_in_p),			// I
    .adc_clk_in_n (adc_clk_in_n),			// I
    .adc_data_in_p (adc_data_in_p),			// I [9:0]
    .adc_data_in_n (adc_data_in_n),			// I [9:0]
    .adc_or_in_p (adc_or_in_p),				// I
    .adc_or_in_n (adc_or_in_n),				// I
    .adc_clk (adc_clk),						// O
    .adc_data_a (data_a),					// O [9:0]
    .adc_data_b (data_b),					// O [9:0]
    .adc_or_a (or_a),						// O
    .adc_or_b (or_b),						// O
    .adc_ddr_edgesel (adc_ddr_edgesel)		// I 0=p먼저인지, 1=n먼저인지 등등..
);

	wire [7:0] AaQ0;
	wire [7:0] AaQ1;
	wire [7:0] AaQ2;
	wire [7:0] AbQ0;
	wire [7:0] AbQ1;
	wire [7:0] AbQ2;
	wire [7:0] ORQ;
	reg [9:0]	adc_a_reg;
	reg [9:0]	adc_b_reg;
	reg			or_a_reg;
	reg			or_b_reg;

	assign adc_data_a = adc_a_reg;
	assign adc_data_b = adc_b_reg;
	assign adc_or_a = or_a_reg;
	assign adc_or_b = or_b_reg;

	wire empty;
	IN_FIFO #(
	.ALMOST_EMPTY_VALUE(1),          // Almost empty offset (1-2)
	.ALMOST_FULL_VALUE(1),           // Almost full offset (1-2)
	.ARRAY_MODE("ARRAY_MODE_4_X_4"), // ARRAY_MODE_4_X_8, ARRAY_MODE_4_X_4
	.SYNCHRONOUS_MODE("FALSE")       // Clock synchronous (FALSE)
	)
	IN_FIFO_inst_A (
		// FIFO Status Flags: 1-bit (each) output: Flags and other FIFO status outputs
		.ALMOSTEMPTY(),				// 1-bit output: Almost empty
		.ALMOSTFULL(),				// 1-bit output: Almost full
		.EMPTY(),					// 1-bit output: Empty
		.FULL(),					// 1-bit output: Full
		.Q0(AaQ0),					// 8-bit output: Channel 0
		.Q1(AaQ1),					// 8-bit output: Channel 1
		.Q2(AaQ2),					// 8-bit output: Channel 2
		.Q3(),					// 8-bit output: Channel 3
		.Q4(AbQ0),					// 8-bit output: Channel 4
		.Q5(AbQ1),					// 8-bit output: Channel 5
		.Q6(AbQ2),					// 8-bit output: Channel 6
		.Q7(),					// 8-bit output: Channel 7
		.Q8(ORQ),					// 8-bit output: Channel 8
		.Q9(),						// 8-bit output: Channel 9
		.D0(data_a[3:0]),			// 4-bit input: Channel 0
		.D1(data_a[7:4]),			// 4-bit input: Channel 1
		.D2({2'b0,data_a[9:8]}),			// 4-bit input: Channel 2
		.D3(4'b0),      	// 4-bit input: Channel 3
		.D4(data_b[3:0]),			// 4-bit input: Channel 4
		.D5({4'b0,data_b[7:4]}),	// 8-bit input: Channel 5
		.D6({4'b0,2'b0,data_b[9:8]}),	// 8-bit input: Channel 6
		.D7(4'b0),			// 4-bit input: Channel 7
		.D8({2'b0,or_b,or_a}),		// 4-bit input: Channel 8
		.D9(4'b0),					// 4-bit input: Channel 9
		// FIFO Control Signals: 1-bit (each) input: Clocks, Resets and Enables
		.RDCLK(sig_clk),			// 1-bit input: Read clock
		.RDEN(adc_dout_en),			// 1-bit input: Read enable
		.RESET(adc_fifo_rst),		// 1-bit input: Reset
		.WRCLK(adc_clk),			// 1-bit input: Write clock
		.WREN(~adc_fifo_rst)		// 1-bit input: Write enable
	);

	// Pipelining
	always @(posedge sig_clk) begin
		adc_a_reg <= {AaQ2[1:0],AaQ1[3:0],AaQ0[3:0]};
		adc_b_reg <= {AbQ2[1:0],AbQ1[3:0],AbQ0[3:0]};
		or_a_reg <= ORQ[0];
		or_b_reg <= ORQ[1];
	end



endmodule