`timescale	1ns/100ps
//////////////////////////////////////////////////////////////////////////////////
// Company: HURA (Hub of Radio Technology)
// Engineer: LEE Seong Yun (sylee@hura.co.kr)
// 
// Create Date:	2017/03/09 16:32  
// Design Name:	inter-integrated circuit interface
// Module Name:	i2c_intf
// Project Name: Two Channel Wideband DF System 
// Target Devices: Kintex7 410T
// Tool versions: Vivado 2015.2
// Description: i2c interface base
//				 
// Dependencies: 
//
// Revision: 
// Revision build 1(2017/03/09)
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

module i2c_intf_tb;
	
	reg  					clk;
   	reg  					sclr;

	wire					sclk;
	wire					sdo;
	reg						sdi;
	wire					iobuf_T;		
	
	reg		[WORD_WIDTH-1:0]	din;
	reg							din_valid;
	wire	[DATA_WIDTH-1:0] 	dout;
	wire						dout_valid;
	wire					s_busy;
	
	// for testbench
	reg			[31:0]		tb_count;


// parameter
parameter SCLK_PERIOD 		= 24;	
parameter CLK_START_END 	= 12; 	// SCLK_PERIOD의 Half 근처로 할 것.
parameter SCLK_PERIOD_BIT 	= 5; 	// SCLK_PERIOD의 비트

parameter HIGH_CLK 			= 8;	// LOW=(SCLK_PERIOD-HIGH_CLK)
parameter CLK_OFFSET 		= 8;  	// SCLK_PERIOD-HIGH_CLK 보다 작아야함.(신호에 비해 HIGH가 밀린량)

parameter CHIPSEL_WIDTH 	= 8; 	// chip address width  (0 불가, 8의배수)
parameter CONT_WIDTH 		= 16;	// control(or addr) address width  (0 가능, 8의배수)
parameter RCHIPSEL_WIDTH 	= 8;	// chip address width for read (0 가능, 8의배수)
parameter DATA_WIDTH 		= 8;  	// data bit width //readable data (0 불가, 8의배수)
parameter RW_FLAG_BIT 		= 8; 	// Read/Write Flag bit가  High(Read)이면 Data 출력은 high impedance를 보냄, 전체비트중 위치 
	
parameter DATA_NUM 			= 16; 	// data bit의 반복

//	parameter CYCLE_BREAK = 10;		//Multipl of sck
//	parameter CYCLE_BREAK_BIT = 4;  
localparam	WORD_WIDTH = CHIPSEL_WIDTH + CONT_WIDTH + RCHIPSEL_WIDTH + DATA_WIDTH; // * DATA_NUM;


	
// ---------------------------------------------------------------------------------
// function for test

i2c_intf #(
	.SCLK_PERIOD(SCLK_PERIOD),		// (2이상) //tmp421 400kHz. 240으로 해야 함. 지금은 테스트
	.CLK_START_END(CLK_START_END), 	// (SCLK_PERIOD-1)와 CLK_START_END 보다 큰 값의 비트.
	.SCLK_PERIOD_BIT(SCLK_PERIOD_BIT),
	.HIGH_CLK(HIGH_CLK), 			
	.CLK_OFFSET(CLK_OFFSET), 		// SCLK_PERIOD-HIGH_CLK 보다 작아야함.(신호에 비해 HIGH가 밀린량)
	.CHIPSEL_WIDTH(CHIPSEL_WIDTH), 	// slave address bit width
	.CONT_WIDTH(CONT_WIDTH),		// (0 가능) control
	.RCHIPSEL_WIDTH(RCHIPSEL_WIDTH),	// (0 가능) read slave address bit width 
	.DATA_WIDTH(DATA_WIDTH),		// 	
	.RW_FLAG_BIT(RW_FLAG_BIT),
	.DATA_NUM(DATA_NUM)				// data bit의 반복
)
 i2c_intf1(
	.clk(clk),					// I 
	.sclr(sclr),				// I 

	.sclk(sclk),				// O
	.sdo(sdo),					// O 
	.sdi(sdi),					// I
	.iobuf_T(iobuf_T),			// O

	.din(din),					// I [WORD_WIDTH-1:0]
	.din_valid(din_valid),		// I
	.dout(dout),				// O [WORD_WIDTH-1:0] 	
	.dout_valid(dout_valid),	// O	
	
	.s_busy(s_busy)				// O
); //total latency  


// ---------------------------------------------------------------------------------
// basic signal generation 	
	
	initial begin
		tb_count=0;
		clk = 1;
	end
	
	always #(10) clk = ~clk; // 50 MHz clock #(10) 글자.
	
	always #(20) tb_count=tb_count+1;
	
	initial begin
		sclr = 1'b1;
		#200;
		sclr = 1'b0;
	end
// ---------------------------------------------------------------------------------

// ---------------------------------------------------------------------------------
	initial
	begin
		//din=32'b0;
		din=16'b0;
		din_valid = 1'b0;
		sdi = 1'b0;
#500
	din_valid = 1'b1;
	//din = 32'h98019901;
	din = 16'hE101;
#20
	din_valid = 1'b0;

#18380

   sdi = 1'b1;
   
#480 // 1bit length
  sdi = 1'b0;
#1440
   sdi = 1'b1;
#1440
   sdi = 1'b0; 
  
	end

// ---------------------------------------------------------------------------------
// Sync signal 
/* 	always @(posedge clk) begin
		if (sclr) begin
		end
		else begin
		
		end
	
	end */
	
// ---------------------------------------------------------------------------------	




endmodule


