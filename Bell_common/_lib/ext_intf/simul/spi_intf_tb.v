`timescale	1ns/100ps
//////////////////////////////////////////////////////////////////////////////////
// Company: HURA (Hub of Radio Technology)
// Engineer: LEE Seong Yun (sylee@hura.co.kr)
//
// Create Date: 2016/05/27 13:00:00
// Design Name:
// Module Name: spi_intf
// Project Name:
// Target Devices: Kintex7 410T
// Tool Versions: Vivado 2018.1
// Description:
//
// Dependencies:
//
// Revision: build 1(2016/05/27)
// 			 build 2(2017/03/09) : dout_valid 추가, s_trg -> din_valid 로 변경
//			 build 3(2018/07/18) : s_busy => din_ready 및 극성 반대로 변경. parameter 중 SCLK_PERIOD_BIT 제거
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////

module spi_intf_tb;

	reg  					clk;
   	reg  					sclr;

	wire					sclk;
	wire 					scs_n;
	wire					sdo;
	reg						sdi;
	wire					iobuf_T;

	reg		[WORD_WIDTH-1:0]	din;
	reg							din_valid;
	wire						din_ready;
	wire	[WORD_WIDTH-1:0] 	dout;
	wire						dout_valid;

	wire						sclk_hmc;
	wire 						scs_n_hmc;
	wire						sdo_hmc;
	reg							sdi_hmc;
	wire						iobuf_T_hmc;

	wire						din_ready_hmc;
	wire       [WORD_WIDTH-1:0]	dout_hmc;
	wire						dout_valid_hmc;

	// for testbench
	reg			[31:0]			tb_count;


parameter SCLK_PERIOD = 16;
parameter CLK_START_END = 8; 	//(최대값은 (2^SCLK_PERIOD_BIT)-1 )

parameter HIGH_CLK = 8;			// LOW=(SCLK_PERIOD-HIGH_CLK)
parameter CLK_OFFSET = 2;  		// SCLK_PERIOD-HIGH_CLK 보다 작거나 같아야함.(신호에 비해 HIGH가 밀린량)

parameter CONT_WIDTH = 0; 		// control bit width (0가능)
parameter DATA_WIDTH = 24;  		// data bit width
parameter RW_FLAG_BIT = 0; 		// Read/Write Flag bit가  High(Read)이면 Data 출력은 high impedance를 보냄 //0 이면 Write Only

parameter SCS_N_MODE = 0; 		//0 = active low 1= active high
parameter SCLK_IDLE_MODE = 1;	// 0= high idle 1=low idle

localparam	WORD_WIDTH = CONT_WIDTH + DATA_WIDTH;// * DATA_NUM;


// ---------------------------------------------------------------------------------
// function for test

spi_intf #(
 	.SCLK_PERIOD(SCLK_PERIOD),			// 2이상
	.CLK_START_END(CLK_START_END),
	.HIGH_CLK(HIGH_CLK),
	.CLK_OFFSET(CLK_OFFSET),			// SCLK_PERIOD-HIGH_CLK 보다 작아야함.(신호에 비해 HIGH가 밀린량)
	.CONT_WIDTH(CONT_WIDTH),			// control bit width
	.DATA_WIDTH(DATA_WIDTH),
	.RW_FLAG_BIT(RW_FLAG_BIT),
	.SCS_N_MODE(SCS_N_MODE),
	.SCLK_IDLE_MODE(SCLK_IDLE_MODE)
)
 spi_intf1(
	.clk		(clk),			// I
	.sclr		(sclr),			// I

	.sclk		(sclk),			// O
	.scs_n		(scs_n),		// O
	.sdo		(sdo),			// O
	.sdi		(sdi),			// I
	.iobuf_T	(iobuf_T),		// O

	.din		(din),			// I [WORD_WIDTH-1:0]
	.din_valid	(din_valid),	// I
	.din_ready	(din_ready),	// O
	.dout		(dout),			// O [WORD_WIDTH-1:0]
	.dout_valid	(dout_valid)	// O
); //total latency


spi_intf #(
 	.SCLK_PERIOD(SCLK_PERIOD),			// 2이상
	.CLK_START_END(CLK_START_END),
	.HIGH_CLK(HIGH_CLK),
	.CLK_OFFSET(CLK_OFFSET),			// SCLK_PERIOD-HIGH_CLK 보다 작아야함.(신호에 비해 HIGH가 밀린량)
	.CONT_WIDTH(CONT_WIDTH),			// control bit width
	.DATA_WIDTH(DATA_WIDTH),
	.RW_FLAG_BIT(RW_FLAG_BIT),
	.SCS_N_MODE(1),
    .SCLK_IDLE_MODE(1)
)
 spi_intf2(
	.clk		(clk),				// I
	.sclr		(sclr),				// I

	.sclk		(sclk_hmc),			// O
	.scs_n		(scs_n_hmc),		// O
	.sdo		(sdo_hmc),			// O
	.sdi		(sdi),				// I
	.iobuf_T	(iobuf_T_hmc),		// O

	.din		(din),				// I [WORD_WIDTH-1:0]
	.din_valid	(din_valid),		// I
	.din_ready	(din_ready_hmc),	// O
	.dout		(dout_hmc),			// O [WORD_WIDTH-1:0]
	.dout_valid	(dout_valid_hmc)	// O
); //total latency



// ---------------------------------------------------------------------------------
// basic signal generation

	initial begin
		tb_count=0;
		clk = 1;
	end

	always #(10) clk = ~clk; // 50 MHz clock #(10) 글자.

	always #(20) tb_count=tb_count+1;

	initial begin
		sclr = 1'b1;
		#200;
		sclr = 1'b0;
	end
// ---------------------------------------------------------------------------------

// ---------------------------------------------------------------------------------
initial
begin

	din='b0;
	din_valid = 1'b0;
	sdi=1'b0;
	sdi_hmc=1'b0;

#500
	din_valid = 1'b1;
	din = 24'h8aa1aa;
//	din = 8'h1a;
#20
	din_valid = 1'b0;
# 720
  sdi = 1'b1;
# 480
  sdi = 1'b0;
# 960
  sdi = 1'b1;
# 480
  sdi = 1'b0;

#20
	din_valid = 1'b0;

end


// ---------------------------------------------------------------------------------
// Sync signal
/* 	always @(posedge clk) begin
		if (sclr) begin
		end
		else begin

		end

	end */

// ---------------------------------------------------------------------------------




endmodule


