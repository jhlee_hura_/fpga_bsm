`timescale	1ns/100ps
//////////////////////////////////////////////////////////////////////////////////
// Company: HURA (Hub of Radio Technology)
// Engineer: LEE Seong Yun (sylee@hura.co.kr)
// 
// Create Date: 2018/10/10 13:00:00
// Design Name: 
// Module Name: shiftreg_intf_tb
// Project Name: 
// Target Devices: Kintex7 410T
// Tool Versions: Vivado 2018.1
// Description: 
// 
// Dependencies: 
// 
// Revision: build 1(2018/10/10)
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
module shiftreg_intf_tb;

parameter SCLK_PERIOD = 24;	
parameter CLK_START_END = 4; 	//(최대값은 (2^SCLK_PERIOD_BIT)-1 )

parameter HIGH_CLK = 8;			// LOW=(SCLK_PERIOD-HIGH_CLK)
parameter CLK_OFFSET = 8;  		// SCLK_PERIOD-HIGH_CLK 보다 작거나 같아야함.(신호에 비해 HIGH가 밀린량)

parameter DATA_WIDTH = 16;  		// data bit width
	
parameter SCS_N_MODE = 0; 		//0 = active low 1= active high
parameter SCLK_IDLE_MODE = 1;	// 0= high idle 1=low idle

	
	reg  					clk;
   	reg  					sclr;

	wire					sclk;
	wire 					scs_n;
	wire					latch_en;
	wire					sdo;
	
	reg		[DATA_WIDTH-1:0]	din;
	reg							din_valid;
	wire						din_ready;

	// for testbench
	reg			[31:0]			tb_count;

// ---------------------------------------------------------------------------------
// function for test
	
shiftreg_intf #(
 	.SCLK_PERIOD(SCLK_PERIOD),			// 2이상
	.CLK_START_END(CLK_START_END), 
	.HIGH_CLK(HIGH_CLK),
	.CLK_OFFSET(CLK_OFFSET),			// SCLK_PERIOD-HIGH_CLK 보다 작아야함.(신호에 비해 HIGH가 밀린량)
	.DATA_WIDTH(DATA_WIDTH),
	.SCS_N_MODE(SCS_N_MODE),
	.SCLK_IDLE_MODE(SCLK_IDLE_MODE)
)
 shiftreg_intf(
	.clk		(clk),			// I 
	.sclr		(sclr),			// I 

	.din		(din),			// I [DATA_WIDTH-1:0]
	.din_valid	(din_valid),	// I
	.din_ready	(din_ready),	// O	
	
	.sclk		(sclk),			// O
	.scs_n		(scs_n),		// O
	.latch_en	(latch_en),		// O 
	.sdo		(sdo)			// O
); //total latency  



// ---------------------------------------------------------------------------------
// basic signal generation 	
	
	initial begin
		tb_count=0;
		clk = 1;
	end
	
	always #(10) clk = ~clk; // 50 MHz clock #(10) 글자.
	
	always #(20) tb_count=tb_count+1;
	
	initial begin
		sclr = 1'b1;
		#200;
		sclr = 1'b0;
	end
// ---------------------------------------------------------------------------------

// ---------------------------------------------------------------------------------
reg din_flag;
initial
begin

	din='b0;		
	din_flag = 1'b0;

		
#500
	din_flag = 1'b1;
	din = 16'h551a;
#20
	din_flag = 1'b0;


end

	
// ---------------------------------------------------------------------------------
// Sync signal 
reg tag = 2'b00;

	always @(posedge clk) begin
		if (sclr) begin
			tag <= 2'b00;
			din_valid <= 1'b0;
		end
		else begin
			if (din_flag) begin
				tag = 2'b01;
				din_valid = 1'b1;
			end
			else if (din_ready && tag == 2'b01) begin
				tag = 2'b10;
				din_valid = 1'b1;
			end
			else 
				din_valid = 1'b0;
		
		end
	
	end 
	
// ---------------------------------------------------------------------------------	




endmodule


