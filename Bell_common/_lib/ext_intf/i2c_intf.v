`timescale	1ns/100ps
//////////////////////////////////////////////////////////////////////////////////
// Company: HURA (Hub of Radio Technology)
// Engineer: LEE Seong Yun (sylee@hura.co.kr)
//
// Create Date:	2017/03/09 16:32
// Design Name:	inter-integrated circuit interface
// Module Name:	i2c_intf
// Project Name: Two Channel Wideband DF System
// Target Devices: Kintex7 410T
// Tool versions: Vivado 2018.1
// Description: i2c interface base
//
// Dependencies:
//
// Revision:
// Revision build 1(2017/03/09)
// Additional Comments:
//
// Copyright 2018 (c) HURA. All rights reserved.
//////////////////////////////////////////////////////////////////////////////////

module i2c_intf #(
	parameter SCLK_PERIOD = 8,		// 최대 65536까지
	parameter CLK_START_END = 4, 	// SCLK_PERIOD의 Half 근처로 할 것.

	parameter HIGH_CLK = 2,			// LOW=(SCLK_PERIOD-HIGH_CLK)
	parameter CLK_OFFSET = 2,  		// SCLK_PERIOD-HIGH_CLK 보다 작아야함.(신호에 비해 HIGH가 밀린량)

	parameter CHIPSEL_WIDTH = 8, 	// chip address width  (0 불가, 8의배수)
	parameter CONT_WIDTH = 8,		// control(or addr) address width  (0 가능, 8의배수)
	parameter RCHIPSEL_WIDTH = 8,	// chip address width for read (0 가능, 8의배수)
	parameter DATA_WIDTH = 16,  	// data bit width //readable data (0 불가, 8의배수)
	parameter RW_FLAG_BIT = 16, 	// Read/Write Flag bit가  High(Read)이면 Data 출력은 high impedance를 보냄, 전체비트중 위치

 	parameter DATA_NUM = 1, 		// data bit의 반복

/*	parameter CYCLE_BREAK = 10;		//Multipl of sck
	parameter CYCLE_BREAK_BIT = 4;  */
	localparam	WORD_WIDTH = CHIPSEL_WIDTH + CONT_WIDTH + RCHIPSEL_WIDTH + DATA_WIDTH // * DATA_NUM;
) (
	input wire 	clk,
	input wire 	sclr,

	output reg 	sclk,	//Phy
	output wire sdo,
	input wire 	sdi,
	output wire iobuf_T, // (0 = data write, 1= read(1'bz))

	input wire [WORD_WIDTH-1:0] din,
	input wire 					din_valid,
	output reg [DATA_WIDTH-1:0] dout,
	output reg					dout_valid,

//	input wire s_trg,			// i2c start
	output reg s_busy			// i2c 사용(write and read)
);
// ---------------------------------------------------------------------------------
// Local Paramater
// (SCLK_PERIOD-1)와 CLK_START_END 보다 큰 값의 비트.
localparam SCLK_PERIOD_BIT = 	(SCLK_PERIOD <= 2 && CLK_START_END <= 1) ? 1 :
								(SCLK_PERIOD <= 4 && CLK_START_END <= 3) ? 2 :
								(SCLK_PERIOD <= 8 && CLK_START_END <= 7) ? 3 :
								(SCLK_PERIOD <= 16 && CLK_START_END <= 15) ? 4 :
								(SCLK_PERIOD <= 32 && CLK_START_END <= 31) ? 5 :
								(SCLK_PERIOD <= 64 && CLK_START_END <= 63) ? 6 :
								(SCLK_PERIOD <= 128 && CLK_START_END <= 127) ? 7 :
								(SCLK_PERIOD <= 256 && CLK_START_END <= 255) ? 8 :
								(SCLK_PERIOD <= 512 && CLK_START_END <= 511) ? 9 :
								(SCLK_PERIOD <= 1024 && CLK_START_END <= 1023) ? 10 :
								(SCLK_PERIOD <= 2048 && CLK_START_END <= 2047) ? 11 :
								(SCLK_PERIOD <= 4096 && CLK_START_END <= 4095) ? 12 :
								(SCLK_PERIOD <= 8192 && CLK_START_END <= 8191) ? 13 :
								16;

localparam MAX_OF_WIDTH_BIT = 	(CHIPSEL_WIDTH <= 8 && CONT_WIDTH <= 8 && RCHIPSEL_WIDTH <= 8 && DATA_WIDTH <= 8) ? 3 :
							(CHIPSEL_WIDTH <= 16 && CONT_WIDTH <= 16 && RCHIPSEL_WIDTH <= 16 && DATA_WIDTH <= 16) ? 4 :
							(CHIPSEL_WIDTH <= 32 && CONT_WIDTH <= 32 && RCHIPSEL_WIDTH <= 32 && DATA_WIDTH <= 32) ? 5 :
							(CHIPSEL_WIDTH <= 64 && CONT_WIDTH <= 64 && RCHIPSEL_WIDTH <= 64 && DATA_WIDTH <= 64) ? 6 :
							8;

localparam DATA_NUM_BIT = 	(DATA_NUM <= 2) ? 1 :
							(DATA_NUM <= 4) ? 2 :
							(DATA_NUM <= 8) ? 3 :
							(DATA_NUM <= 16) ? 4 :
							(DATA_NUM <= 32) ? 5 :
							(DATA_NUM <= 64) ? 6 :
							(DATA_NUM <= 128) ? 7 :
							(DATA_NUM <= 256) ? 8 :
							(DATA_NUM <= 512) ? 9 :
							(DATA_NUM <= 1024) ? 10 :
							(DATA_NUM <= 2048) ? 11 :
							(DATA_NUM <= 4096) ? 12 :
							(DATA_NUM <= 8192) ? 13 :
							(DATA_NUM <= 16384) ? 14 :
							(DATA_NUM <= 32768) ? 15 :
							(DATA_NUM <= 65536) ? 16 :
							(DATA_NUM <= 131072) ? 17 :
							(DATA_NUM <= 262144) ? 18 : 0;			// data num의 bit
// ---------------------------------------------------------------------------------
// read/write signal
wire sig_rw;
assign sig_rw = (RW_FLAG_BIT == 0) ? 1'b0 : din[RW_FLAG_BIT];  // low : write, high : read

// ---------------------------------------------------------------------------------
// SCLK Making
// CLK_OFFSET 만큼 LOW, HIGH 갯수만큼 HIGH, 나머지는 LOW
wire [SCLK_PERIOD-1:0] clk_shape;
//CLK_OFFSET == 0 이면 clk_shape 에러이므로 방지 구문
generate
	if (CLK_OFFSET !=0)
		assign clk_shape[SCLK_PERIOD-1:SCLK_PERIOD-CLK_OFFSET] = 0;
endgenerate

assign clk_shape[SCLK_PERIOD-CLK_OFFSET-1:SCLK_PERIOD-CLK_OFFSET-HIGH_CLK] = ~0;

//SCLK_PERIOD == (CLK_OFFSET + HIGH_CLK) 이면 clk_shape 에러이므로 방지 구문
generate
	if (SCLK_PERIOD != (CLK_OFFSET + HIGH_CLK))
		assign clk_shape[SCLK_PERIOD-CLK_OFFSET-HIGH_CLK-1:0] = 0;
endgenerate
// ---------------------------------------------------------------------------------
// Write SPI
localparam state_idle 		= 3'b000;
localparam state_start 		= 3'b001;
localparam state_chipsel 	= 3'b010;
localparam state_cont 		= 3'b011;
localparam state_start_re 	= 3'b100;
localparam state_rchipsel 	= 3'b101;
localparam state_data 		= 3'b110;
localparam state_done 		= 3'b111;

reg [2:0] s_spi;
reg sdo_we;		// write en
reg sdo_r;
reg [3:0] ack_count;
reg sdi_valid;

assign sdo = (sdo_we) ? sdo_r : 1'bz;
assign iobuf_T = ~sdo_we;

reg [SCLK_PERIOD_BIT-1:0] clk_count;
reg [MAX_OF_WIDTH_BIT-1:0] bit_count;
reg [WORD_WIDTH-1:0] shift_reg;
reg [DATA_NUM_BIT-1:0] data_count;

always @(posedge clk) begin
	if (sclr) begin
		sclk 		<= 1'b1;
		sdo_r 		<= 1'b1;
		shift_reg 	<= 'b0;
		s_busy 		<= 1'b0;
		sdo_we 		<= 1'b0;
		sdi_valid 	<= 1'b0;
		clk_count	<= CLK_START_END;	// 처음은 CS 후 CLK 만큼 후에 신호 시작
		bit_count	<= CHIPSEL_WIDTH - 1;		// 먼저 CHIPSEL_WIDTH 먼저
		ack_count 	<= 4'h8;
		data_count <= DATA_NUM - 1;
		s_spi 	<= state_idle;
	end else begin
		case(s_spi)
			state_idle : begin
				if (din_valid) begin		// din_valid가 오면 시작
					sdo_we <= 1'b1;
					s_busy <= 1'b1;
					shift_reg <= din;
					s_spi <= state_start;
				end else begin
					s_busy <= 1'b0;
					sdo_we <= 1'b0;
					shift_reg <= 0;
					s_spi <= state_idle;
				end
				sdo_r <= 1'b1;
				sclk <= 1'b1; //
				sdi_valid <= 1'b0;
				bit_count <= CHIPSEL_WIDTH - 1;		// 먼저 CHIPSEL_WIDTH 먼저
				clk_count <= CLK_START_END;
				data_count <= DATA_NUM - 1;
				ack_count <= 4'h8;
			end
			// ChipSel part, START Condition
			state_start : begin
				if (clk_count > 0) begin	//sclk 구동 (sclk 한클럭 동안 데이터 유지)
					clk_count <= clk_count - 1'b1;	// clk_count 될 동안..
					sclk <= sclk;
					shift_reg <= shift_reg;
					sdo_r <= sdo_r;
					ack_count <= ack_count;
					s_spi <= state_start;
				end else if (sdo_r == 1'b1 ) begin  // SDA Falling, START Condition
					clk_count <= CLK_START_END>>1;	// CLK_START_END의 반
					sclk <= sclk;
					shift_reg <= shift_reg;
					sdo_r <= 1'b0;  				// start
					ack_count <= ack_count;
					s_spi <= state_start;
				end else if (sdo_r == 1'b0 &&  sclk == 1'b1) begin  // SDA Falling, START Condition
					clk_count <= CLK_START_END>>1;	// 나머지 반
					sclk <= 1'b0;
					shift_reg <= shift_reg;
					sdo_r <= 1'b0;  				// start
					ack_count <= ack_count;
					s_spi <= state_start;
				end else begin						// 다음 단계(Chip sel)로
					clk_count <= SCLK_PERIOD - 1;
					sclk <= clk_shape[SCLK_PERIOD - 1];
					shift_reg <= {shift_reg[WORD_WIDTH-2:0],1'b0};
					sdo_r <= shift_reg[WORD_WIDTH-1];
					ack_count <= ack_count - 1'b1;
					s_spi <= state_chipsel;
				end
				sdi_valid <= sdi_valid;
				s_busy <= s_busy;
				sdo_we <= sdo_we;
				bit_count <= bit_count;
				data_count <= data_count;
			end
			// CHIP SEL (Slave Address) 무조건 실행.
			state_chipsel : begin
				if (bit_count > 0 ) begin  	// Chip Sel bit 동안 동작.
					if (clk_count > 0) begin	//sclk 구동 (sclk 한클럭 동안 데이터 유지)
						clk_count <= clk_count - 1'b1;
						bit_count <= bit_count;
						ack_count <= ack_count;
						sclk <= clk_shape[clk_count-1];
						shift_reg <= shift_reg;
						sdo_r <= sdo_r;
						sdo_we <= sdo_we;
						sdi_valid <= sdi_valid;
						s_spi <= state_chipsel;
					end else if (ack_count == 0) begin	// ack 신호 받기
						clk_count <= SCLK_PERIOD - 1;
						bit_count <= bit_count;			// 0으로 유지해서 else가 실행 될수 있게
						ack_count <= 4'h8;				// ack 신호 Reset해서 else가 실행 될수 있게
						sclk <= clk_shape[SCLK_PERIOD - 1];
						shift_reg <= shift_reg;
						sdo_r <= sdo_r;
						sdo_we <= 1'b0;					// ack 신호 받게 1'bz 선택하게
						sdi_valid <= sdi_valid;
						s_spi <= state_chipsel;
					end else begin						// 새 비트
						clk_count <= SCLK_PERIOD - 1;
						bit_count <= bit_count - 1'b1;
						ack_count <= ack_count - 1'b1;
						sclk <= clk_shape[SCLK_PERIOD - 1];
						shift_reg <= {shift_reg[WORD_WIDTH-2:0],1'b0};
						sdo_r <= shift_reg[WORD_WIDTH-1];
						sdo_we <= 1'b1;
						sdi_valid <= sdi_valid;
						s_spi <= state_chipsel;
					end
				end else begin			// 다음 단계로. 마지막 ack..포함.
					if (clk_count > 0) begin			//sclk 구동 (sclk 한클럭 동안 데이터 유지)
						clk_count <= clk_count - 1'b1;
						bit_count <= bit_count;
						ack_count <= ack_count;
						sclk <= clk_shape[clk_count-1];
						shift_reg <= shift_reg;
						sdo_r <= sdo_r;
						sdo_we <= sdo_we;
						sdi_valid <= sdi_valid;
						s_spi <= state_chipsel;
					end else if (ack_count == 0) begin	// ack 신호 받기
						clk_count <= SCLK_PERIOD - 1;
						bit_count <= bit_count;
						ack_count <= 4'h8;
						sclk <= clk_shape[SCLK_PERIOD - 1];
						shift_reg <= shift_reg;
						sdo_r <= sdo_r;
						sdo_we <= 1'b0;					// ack 신호 받게 1'bz 선택하게
						sdi_valid <= sdi_valid;
						s_spi <= state_chipsel;
					end else begin						// 다음단계 진로 결정.
						if (CONT_WIDTH != 0) begin
							clk_count <= SCLK_PERIOD - 1;
							bit_count <= CONT_WIDTH -1;  // CONT 준비
							ack_count <= ack_count - 1'b1;
							sclk <= clk_shape[SCLK_PERIOD - 1];
							shift_reg <= {shift_reg[WORD_WIDTH-2:0],1'b0};
							sdo_r <= shift_reg[WORD_WIDTH-1];
							sdo_we <= 1'b1;
							sdi_valid <= sdi_valid;
							s_spi <= state_cont;
						end else if (RCHIPSEL_WIDTH !=0) begin
							clk_count <= CLK_START_END;
							bit_count <= bit_count;  	// start 신호에서 세팅
							ack_count <= ack_count;		// start 신호에서 세팅
							sclk <= 1'b1;
							shift_reg <= shift_reg;
							sdo_r <= 1'b1;
							sdo_we <= 1'b1;
							sdi_valid <= sdi_valid;
							s_spi <= state_start_re;
						end else begin  								// DATA
							clk_count <= SCLK_PERIOD - 1;
							bit_count <= DATA_WIDTH -1;  // DATA 준비
							ack_count <= ack_count - 1'b1;
							sclk <= clk_shape[SCLK_PERIOD - 1];
							shift_reg <= {shift_reg[WORD_WIDTH-2:0],1'b0};
							sdo_r <=  shift_reg[WORD_WIDTH-1];
							sdo_we <= (sig_rw) ? 1'b0 : 1'b1;
							sdi_valid <= (sig_rw) ? 1'b1 : 1'b0;
							s_spi <= state_data;
						end
					end
				end
				s_busy <=s_busy;
				data_count <= data_count;
			end
			state_cont : begin		// Addr(or Control command) part
				if (bit_count > 0 ) begin  // Control 동안 동작.
					if (clk_count > 0) begin	// 한클럭 정의
						clk_count <= clk_count - 1'b1;
						bit_count <= bit_count;
						ack_count <= ack_count;
						sclk <= clk_shape[clk_count-1];
						shift_reg <= shift_reg;
						sdo_r <= sdo_r;
						sdo_we <= sdo_we;
						sdi_valid <= sdi_valid;
						s_spi <= state_cont;
					end else if (ack_count == 0) begin	// ack 신호 받기
						clk_count <= SCLK_PERIOD - 1;
						bit_count <= bit_count;
						ack_count <= 4'h8;
						sclk <= clk_shape[SCLK_PERIOD - 1];
						shift_reg <= shift_reg;
						sdo_r <= sdo_r;
						sdo_we <= 1'b0;
						sdi_valid <= sdi_valid;
						s_spi <= state_cont;
					end else begin	// 새 비트
						clk_count <= SCLK_PERIOD - 1;
						bit_count <= bit_count - 1'b1;
						ack_count <= ack_count - 1'b1;
						sclk <= clk_shape[SCLK_PERIOD - 1];
						shift_reg <= {shift_reg[WORD_WIDTH-2:0],1'b0};
						sdo_r <= shift_reg[WORD_WIDTH-1];
						sdo_we <= 1'b1;
						sdi_valid <= sdi_valid;
						s_spi <= state_cont;
					end
				end else begin			// 다음 단계로. 마지막 ack..포함.
					if (clk_count > 0) begin
						clk_count <= clk_count - 1'b1;
						bit_count <= bit_count;
						ack_count <= ack_count;
						sclk <= clk_shape[clk_count-1];
						shift_reg <= shift_reg;
						sdo_r <= sdo_r;
						sdo_we <= sdo_we;
						sdi_valid <= sdi_valid;
						s_spi <= state_cont;
					end else if (ack_count == 0) begin	// ack 신호 받기
						clk_count <= SCLK_PERIOD - 1;
						bit_count <= bit_count;
						ack_count <= 4'h8;
						sclk <= clk_shape[SCLK_PERIOD - 1];
						shift_reg <= shift_reg;
						sdo_r <= sdo_r;
						sdo_we <= 1'b0;
						sdi_valid <= sdi_valid;
						s_spi <= state_cont;
					end else begin	// 다음단계 진로 결정.
						if (RCHIPSEL_WIDTH !=0) begin
							clk_count <= CLK_START_END;
							bit_count <= bit_count; //RCHIPSEL_WIDTH -1;  // MCONT 준비
							ack_count <= ack_count;
							sclk <= 1'b1; //clk_shape[SCLK_PERIOD - 1];
							shift_reg <= shift_reg; //{shift_reg[WORD_WIDTH-2:0],1'b0};
							sdo_r <= 1'b1; // shift_reg[WORD_WIDTH-1];
							sdo_we <= 1'b1; //(sig_rw) ? 1'b0 : 1'b1;
							sdi_valid <= sdi_valid;
							s_spi <= state_start_re;
						end else begin
							clk_count <= SCLK_PERIOD - 1;
							bit_count <= DATA_WIDTH -1;  // DATA 준비
							ack_count <= ack_count - 1'b1;
							sclk <= clk_shape[SCLK_PERIOD - 1];
							shift_reg <= {shift_reg[WORD_WIDTH-2:0],1'b0};
							sdo_r <=  shift_reg[WORD_WIDTH-1];
							sdo_we <= (sig_rw) ? 1'b0 : 1'b1;
							sdi_valid <= (sig_rw) ? 1'b1 : 1'b0;
							s_spi <= state_data;
						end
					end
				end
				s_busy <=s_busy;
				data_count <= data_count;
			end

			state_start_re : begin					// ChipSel start part
				if (clk_count > 0) begin
					clk_count <= clk_count - 1'b1;
					bit_count <= bit_count;
					sclk <= sclk;
					shift_reg <= shift_reg;
					sdo_r <= sdo_r;
					sdi_valid <= sdi_valid;
					ack_count <= ack_count;
					sdo_we <= sdo_we;
					s_spi <= state_start_re;
				end
				else if (sdo_r == 1'b1 ) begin
					clk_count <= CLK_START_END>>1;
					sclk <= sclk;
					shift_reg <= shift_reg;
					sdo_r <= 1'b0;  // start
					sdi_valid <= sdi_valid;
					ack_count <= ack_count;
					sdo_we <= sdo_we;
					s_spi <= state_start_re;
				end
				else if (sdo_r == 1'b0 &&  sclk == 1'b1) begin  // SDA Falling, START Condition
					clk_count <= CLK_START_END>>1;	// 나머지 반
					sclk <= 1'b0;
					shift_reg <= shift_reg;
					sdo_r <= 1'b0;  				// start
					ack_count <= ack_count;
					s_spi <= state_start_re;
				end
				else begin
					clk_count <= SCLK_PERIOD - 1;
					bit_count <= RCHIPSEL_WIDTH -1; //DATA_WIDTH - 1;
					sclk <= clk_shape[SCLK_PERIOD - 1];
					shift_reg <= {shift_reg[WORD_WIDTH-2:0],1'b0};
					sdo_r <= shift_reg[WORD_WIDTH-1];
					sdo_we <= 1'b1;
					sdi_valid <= sdi_valid;
					ack_count <= ack_count - 1'b1;
					s_spi <= state_rchipsel;
				end
				s_busy <= s_busy;
				data_count <= data_count;
			end
			state_rchipsel : begin		// Control part
				if (bit_count > 0 ) begin  // Control 동안 동작.
					if (clk_count > 0) begin	// 한클럭 정의
						clk_count <= clk_count - 1'b1;
						bit_count <= bit_count;
						ack_count <= ack_count;
						sclk <= clk_shape[clk_count-1];
						shift_reg <= shift_reg;
						sdo_r <= sdo_r;
						sdo_we <= sdo_we;
						sdi_valid <= sdi_valid;
						s_spi <= state_rchipsel;
					end
					else if (ack_count == 0) begin	// ack 신호 받기
						clk_count <= SCLK_PERIOD - 1;
						bit_count <= bit_count;
						ack_count <= 4'h8;
						sclk <= clk_shape[SCLK_PERIOD - 1];
						shift_reg <= shift_reg;
						sdo_r <= sdo_r;
						sdo_we <= 1'b0;
						sdi_valid <= sdi_valid;
						s_spi <= state_rchipsel;
					end
					else begin	// 새 비트
						clk_count <= SCLK_PERIOD - 1;
						bit_count <= bit_count - 1'b1;
						ack_count <= ack_count - 1'b1;
						sclk <= clk_shape[SCLK_PERIOD - 1];
						shift_reg <= {shift_reg[WORD_WIDTH-2:0],1'b0};
						sdo_r <= shift_reg[WORD_WIDTH-1];
						sdo_we <= 1'b1;
						sdi_valid <= sdi_valid;
						s_spi <= state_rchipsel;
					end
				end
				else begin			// 다음 단계로. 마지막 ack..포함.
					if (clk_count > 0) begin
						clk_count <= clk_count - 1'b1;
						bit_count <= bit_count;
						ack_count <= ack_count;
						sclk <= clk_shape[clk_count-1];
						shift_reg <= shift_reg;
						sdo_r <= sdo_r;
						sdo_we <= sdo_we;
						sdi_valid <= sdi_valid;
						s_spi <= state_rchipsel;
					end
					else if (ack_count == 0) begin	// ack 신호 받기
						clk_count <= SCLK_PERIOD - 1;
						bit_count <= bit_count;
						ack_count <= 4'h8;
						sclk <= clk_shape[SCLK_PERIOD - 1];
						shift_reg <= shift_reg;
						sdo_r <= sdo_r;
						sdo_we <= 1'b0;
						sdi_valid <= sdi_valid;
						s_spi <= state_rchipsel;
					end
					else begin	// 다음단계 진로 결정.
						clk_count <= SCLK_PERIOD - 1;
						bit_count <= DATA_WIDTH -1;  // DATA 준비
						ack_count <= ack_count - 1'b1;
						sclk <= clk_shape[SCLK_PERIOD - 1];
						shift_reg <= {shift_reg[WORD_WIDTH-2:0],1'b0};
						sdo_r <=  shift_reg[WORD_WIDTH-1];
						sdo_we <= (sig_rw) ? 1'b0 : 1'b1;
						sdi_valid <= (sig_rw) ? 1'b1 : 1'b0;
						s_spi <= state_data;
					end
				end
				s_busy <=s_busy;
				data_count <= data_count;
			end

			state_data : begin			// DATA part
				if (bit_count > 0 ) begin
					if (clk_count > 0) begin
						clk_count <= clk_count -1'b1;
						bit_count <= bit_count;
						ack_count <= ack_count;
						data_count <= data_count;
						sclk <= clk_shape[clk_count-1];
						shift_reg <= shift_reg;
						sdo_r <= sdo_r;
						sdo_we <= sdo_we; //(sig_rw) ? 1'b0 : 1'b1;
						sdi_valid <= sdi_valid;
						s_spi <= state_data;
					end
					else if (ack_count == 0) begin	// ack 신호 받기
						clk_count <= SCLK_PERIOD - 1;
						bit_count <= bit_count;
						ack_count <= 4'h8;
						data_count <= data_count;
						sclk <= clk_shape[SCLK_PERIOD - 1];
						shift_reg <= shift_reg;
						sdo_r <= 1'b0; // send ack signal //sdo_r;
						sdo_we <= 1'b1;
						sdi_valid <= sdi_valid;
						s_spi <= state_data;
					end
					else begin
						clk_count <= SCLK_PERIOD - 1;
						bit_count <= bit_count - 1'b1;
						ack_count <= ack_count - 1'b1;
						data_count <= data_count;
						sclk <= clk_shape[SCLK_PERIOD - 1];
						shift_reg <= {shift_reg[WORD_WIDTH-2:0],1'b0};
						sdo_r <= shift_reg[WORD_WIDTH-1];
						sdo_we <= (sig_rw) ? 1'b0 : 1'b1;
						sdi_valid <= sdi_valid;
						s_spi <= state_data;
					end
				end
				else begin  // DATA 마지막 bit
					if (clk_count > 0) begin	//
						clk_count <= clk_count - 1'b1;
						bit_count <= bit_count;
						ack_count <= ack_count;
						data_count <= data_count;
						sclk <= clk_shape[clk_count -1];
						shift_reg <= shift_reg;
						sdo_r <= sdo_r;
						sdo_we <= sdo_we; //(sig_rw) ? 1'b0 : 1'b1;
						sdi_valid <= sdi_valid;
						s_spi <= state_data;
					end
					else if (ack_count == 0) begin	// ack 받기  or nack 보내기
						clk_count <= SCLK_PERIOD - 1;
						bit_count <= bit_count;
						ack_count <= 4'h8;
						data_count <= data_count;
						sclk <= clk_shape[SCLK_PERIOD - 1];
						shift_reg <= shift_reg;
						// sig_rw==0(write) 은 무조건 1'bz, data_count 마지막일때는 nack로 보낸다.
						sdo_r <= (data_count==0) ? 1'b1 : 1'b0; //nack : ack (sdo_we가 1이면 이건 무시)
						sdo_we <= (sig_rw) ? 1'b1 : 1'b0; // Data 부분과 반대.
						sdi_valid <= 1'b0;
						s_spi <= state_data;
					end
					else if (data_count > 0) begin  // DATA NUM 만큼 반복
						clk_count <= SCLK_PERIOD - 1;
						bit_count <= DATA_WIDTH -1;
						ack_count <= ack_count - 1'b1;
						data_count <= data_count - 1'b1;
						sclk <= clk_shape[SCLK_PERIOD - 1];
						shift_reg <= {shift_reg[WORD_WIDTH-2:0],1'b0};
						sdo_r <= shift_reg[WORD_WIDTH-1];
						sdo_we <= (sig_rw) ? 1'b0 : 1'b1;
						sdi_valid <= (sig_rw) ? 1'b1 : 1'b0;;
						s_spi <= state_data;
					end
					else begin
						clk_count <= SCLK_PERIOD - 1;
						bit_count <= 0;
						ack_count <= 4'h8;
						data_count <= data_count;
						sclk <= 1'b0;
						shift_reg <= {shift_reg[WORD_WIDTH-2:0],1'b0};
						sdo_r <= 1'b0;
						sdo_we <= 1'b1;
						sdi_valid <= 1'b0;
						s_spi <= state_done;
					end
				end
				s_busy <=s_busy;

			end
			state_done : begin			// 완료
				if (clk_count > 0) begin
					if (sclk == 1'b1)
						sclk <= sclk;
					else
						sclk <= clk_shape[clk_count -1];
					clk_count <= clk_count - 1'b1;
					sdo_r <= sdo_r;
					s_spi <= state_done;
				end
				else if (sdo_r == 1'b0) begin
					clk_count <= CLK_START_END;
					sdo_r <= 1'b1;
					sclk <= sclk;
					s_spi <= state_done;
				end
				else begin
					clk_count <= clk_count;
					sdo_r <= sdo_r;
					sclk <= sclk;
					s_spi <= state_idle;
				end
				s_busy <=s_busy;
				sdo_we <= 1'b1;
				sdi_valid <= sdi_valid;
				bit_count <= bit_count;
				ack_count <= ack_count;
			end

		endcase
	end
end

// ---------------------------------------------------------------------------------
// Read I2C data
reg d_sclk;
wire data_en;
assign data_en = ~d_sclk & sclk & sdi_valid;  // data 에 clk으로 동기화

always @(posedge clk) begin // BUFG 사용하지 않기
	if (sclr) begin
		d_sclk <= 1'b1;
		dout <= 0;
		dout_valid <= 1'b0;
	end else begin
		if (data_en & ~sdo_we) begin	// sclk의 처음에서 데이터 GET  //ack_count != 4'h8 ->~sdo_we
			dout[0] <= sdi;
			dout[DATA_WIDTH-1:1] <= dout[DATA_WIDTH-2:0];
			d_sclk <= sclk;
			dout_valid <= (bit_count ==  0) ? 1'b1 : 1'b0;
		end	else begin
			dout <= dout;
			d_sclk <= sclk;
			dout_valid <=  1'b0;
		end
	end
end

//assign dout_valid = ((data_en & sdo_we) && bit_count ==  0) ? 1'b1 : 1'b0;

endmodule












