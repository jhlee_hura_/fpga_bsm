`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: HURA
// Engineer: LEE Seong Yun (sylee@hura.co.kr)
//
// Create Date: 2018/08/21 11:05:02
// Design Name: QDC Alice
// Module Name: pwm_bit
// Project Name: QDC
// Target Devices: Kintex 7 160T
// Tool Versions: Vivado 2018.1
// Description:
//
// Dependencies:
//
// Revision:
// 		build. 1 (2018/11/15)
// Additional Comments:
//
// Copyright 2018, All rights reserved by HURA
//////////////////////////////////////////////////////////////////////////////////
module pwm_bit #(
	parameter NBIT = 8,
	parameter PULSE_OFFSET_DEF = 'd4,
	parameter PULSE_WIDTH_DEF = 'd8,
	parameter PULSE_ADD_WIDTH_DEF = 'd4
) (
	input wire clk,
	input wire sclr,

	input wire				shape_load,
	input wire [NBIT-1:0] 	pulse_offset,
	input wire [NBIT-1:0] 	pulse_width,
	input wire [NBIT-1:0] 	pulse_add_width,

	input wire 			din_pulseadd,	// sync
	input wire			din_pulse,		// sync
	input wire			din_valid,		//
	output reg 			din_ready,

	output reg 			dout
);
// pulse shape configuration 은 shape_load 를 valid 신호 임.
// din_pulse 입력시 pulse_width 만큼 펄스 발생 ; 동시에 din_pulseadd 입력시 pulse_width + pulse_add_width 만큼 펄스 발생

reg [NBIT-1:0] p_offset = 'd4;
reg [NBIT-1:0] p_width 	= 'd8;
reg [NBIT-1:0] p_add 	= 'd4;

always @(posedge clk) begin
	if (sclr) begin
	    p_offset		<= PULSE_OFFSET_DEF;
		p_width 		<= PULSE_WIDTH_DEF;
		p_add			<= PULSE_ADD_WIDTH_DEF;
	end
	else begin
		if (shape_load) begin
			p_offset	<= pulse_offset;
			p_width 	<= pulse_width;
			p_add		<= pulse_add_width;
		end
	end
end


reg [NBIT-1:0] offset_cnt = -'sd1;
reg [NBIT-1:0] width_cnt = -'sd1;
reg [NBIT-1:0] pulseadd_cnt = -'sd1;
reg pulse;
reg pulseadd;

localparam READY_STATE	= 2'd0;
localparam OFFSET_STATE	= 2'd1;
localparam PULSE_STATE	= 2'd2;
localparam PULSE_ADD_STATE	= 2'd3;

reg [1:0]	state;

always @(posedge clk) begin
	if (sclr) begin
		offset_cnt		<= PULSE_OFFSET_DEF - 1'b1;
		width_cnt		<= PULSE_WIDTH_DEF - 1'b1;
		pulseadd_cnt	<= PULSE_ADD_WIDTH_DEF - 1'b1;
		pulse			<= 1'b0;
		pulseadd		<= 1'b0;
		dout			<= 1'b0;
		din_ready		<= 1'b0;
		state 			<= READY_STATE;
	end
	else begin
		if (shape_load)
			state <= READY_STATE;
		else
			case (state)
				READY_STATE : begin
					offset_cnt		<= p_offset - 1'b1;
					width_cnt		<= p_width - 1'b1;
					pulseadd_cnt	<= p_add - 1'b1;
					if (din_valid) begin
						dout		<= (p_offset == 'b0) ? din_pulse : 1'b0;
						pulse		<= din_pulse;
						pulseadd 	<= din_pulseadd;
						din_ready	<= 1'b0;
						state 		<= (p_offset == 'b0) ? PULSE_STATE : OFFSET_STATE;
					end
					else begin
						dout		<= 1'b0;
						pulse		<= 1'b0;
						pulseadd 	<= 1'b0;
						din_ready	<= 1'b1;
						state 		<= READY_STATE;
					end

				end
				OFFSET_STATE : begin
					offset_cnt 		<= offset_cnt - 1'b1;
					if (offset_cnt == 'b0) begin
						dout			<= pulse;
						state			<= PULSE_STATE;
					end
					else begin
						dout			<= 1'b0;
						state			<= OFFSET_STATE;
					end
				end
				PULSE_STATE : begin
					width_cnt			<= width_cnt - 1'b1;
					if (width_cnt == 'b0)
						if (pulseadd && p_add !='b0) begin
							dout		<= pulse;
							state		<= PULSE_ADD_STATE;
						end
						else begin
							dout		<= 1'b0;
							din_ready	<= 1'b1;
							state		<= READY_STATE;
						end
					else begin
						dout		<= pulse;
						state		<= PULSE_STATE;
					end
				end
				PULSE_ADD_STATE : begin
					pulseadd_cnt	<= pulseadd_cnt - 1'b1;
					if (pulseadd_cnt == 'b0) begin
						dout		<= 1'b0;
						din_ready	<= 1'b1;
						state		<= READY_STATE;
					end
					else begin
						state		<= PULSE_ADD_STATE;
					end
				end

			endcase
	end
end

endmodule