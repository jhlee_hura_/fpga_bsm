`timescale 1ns / 100ps
//////////////////////////////////////////////////////////////////////////////////
// Company: HURA
// Engineer: LEE Seong Yun (sylee@hura.co.kr)
//
// Create Date: 2017/03/27 09:00:00
// Design Name: PWM (Pulse Width Modulation)
// Module Name: pwm.v
// Project Name: GNSS Monitering System (GNMS)
// Target Devices: Kintex7 410T
// Tool Versions: Vivado 2018.1
// Description:
//
// Dependencies:
//
// Revision : build 1(2017/03/27)
// Additional Comments:
//
// Copyright 2018 (c) HURA. All rights reserved.
//////////////////////////////////////////////////////////////////////////////////
module pwm #(
	parameter MAX_CLK_CNT_BIT = 24
) (
	input wire clk,
	input wire sclr,

	input wire [MAX_CLK_CNT_BIT-1:0] high_dura,		// duration 값을 넣을 것.
	input wire [MAX_CLK_CNT_BIT-1:0] low_dura,		// duartion 값을 넣을 것.

	output reg pwm_out
);

reg [MAX_CLK_CNT_BIT-1:0] cnt='b0;

always @(posedge clk) begin
	if (sclr) begin
		pwm_out = 1'b0;
		cnt ='b0;
	end
	else begin
		cnt	<= cnt + 1'b1;
		if (pwm_out) begin		// high 상태
			if (cnt >= high_dura) begin
				cnt <= 'b1;
				pwm_out <= (low_dura=='b0) ? 1'b1 : 1'b0;
			end
		end
		else begin
			if (cnt >= low_dura) begin
				cnt <= 'b1;
				pwm_out <= (high_dura=='b0) ? 1'b0 : 1'b1;
			end
		end
	end
end

endmodule