`timescale	1ns/100ps
//////////////////////////////////////////////////////////////////////////////////
// Company: HURA (Hub of Radio Technology)
// Engineer: SUK Mi Kyung(mksuk@hura.co.kr)
//
// Create Date: 2018/06/04 13:00:00
// Design Name:
// Module Name: shiftreg_intf
// Project Name:
// Target Devices: Kintex7 410T
// Tool Versions: Vivado 2018.1
// Description:
//
// Dependencies:
//
// Revision: build 1(2018/06/04)
// 			 build 2(2018/07/18) :s_busy => din_ready 및 극성 반대로 변경. parameter 중 SCLK_PERIOD_BIT 제거
// Additional Comments:
//
// Copyright 2018 (c) HURA. All rights reserved.
//////////////////////////////////////////////////////////////////////////////////
module shiftreg_intf # (
	parameter SCLK_PERIOD = 8,
	parameter CLK_START_END = 4, 	//(최대값은 (2^SCLK_PERIOD_BIT)-1 )

	parameter HIGH_CLK = 2,			// LOW=(SCLK_PERIOD-HIGH_CLK)
	parameter CLK_OFFSET = 2,  		// SCLK_PERIOD-HIGH_CLK 보다 작거나 같아야함.(신호에 비해 HIGH가 밀린량)

	parameter DATA_WIDTH = 8,  		// data bit width

	parameter SCS_N_MODE = 0, 		// 0 = active low 1= active high
	parameter SCLK_IDLE_MODE = 0	// 0= high idle 1=low idle
) (
	input wire clk,
	input wire sclr,

	// From/To Register
	input wire [DATA_WIDTH-1:0] din,
	input wire 					din_valid,
	output reg 					din_ready,

	// To pin-out(pad)
	output reg 		sclk,
	output reg 		scs_n,			  //reset
	output reg 		latch_en,	  	  //latch enable
	output wire 	sdo
);
// ---------------------------------------------------------------------------------
// Paramater
// Signal Type configuration
// (SCLK_PERIOD-1)와 CLK_START_END 보다 큰 값의 비트.
localparam SCLK_PERIOD_BIT = 	(SCLK_PERIOD <= 2 && CLK_START_END <= 1) ? 1 :
								(SCLK_PERIOD <= 4 && CLK_START_END <= 3) ? 2 :
								(SCLK_PERIOD <= 8 && CLK_START_END <= 7) ? 3 :
								(SCLK_PERIOD <= 16 && CLK_START_END <= 15) ? 4 :
								(SCLK_PERIOD <= 32 && CLK_START_END <= 31) ? 5 :
								(SCLK_PERIOD <= 64 && CLK_START_END <= 63) ? 6 :
								(SCLK_PERIOD <= 128 && CLK_START_END <= 127) ? 7 :
								(SCLK_PERIOD <= 256 && CLK_START_END <= 255) ? 8 :
								(SCLK_PERIOD <= 512 && CLK_START_END <= 511) ? 9 :
								(SCLK_PERIOD <= 1024 && CLK_START_END <= 1023) ? 10 :
								(SCLK_PERIOD <= 2048 && CLK_START_END <= 2047) ? 11 :
								(SCLK_PERIOD <= 4096 && CLK_START_END <= 4095) ? 12 :
								(SCLK_PERIOD <= 8192 && CLK_START_END <= 8191) ? 13 :
								16;

localparam MAX_OF_WIDTH_BIT = 	(DATA_WIDTH <=2) ? 1 :
                                (DATA_WIDTH <= 4) ? 2 :
                                (DATA_WIDTH <= 8) ? 3 :
                                (DATA_WIDTH <= 16) ? 4 :
                                (DATA_WIDTH <= 32) ? 5 :
                                (DATA_WIDTH <= 64) ? 6 :
                                (DATA_WIDTH <= 128) ? 7 :
								8;

// ---------------------------------------------------------------------------------
// SCLK Making
// CLK_OFFSET 만큼 LOW, HIGH 갯수만큼 HIGH, 나머지는 LOW
wire [SCLK_PERIOD-1:0] clk_shape;
//CLK_OFFSET == 0 이면 clk_shape 에러이므로 방지 구문
generate
	if (CLK_OFFSET !=0)
		assign clk_shape[SCLK_PERIOD-1:SCLK_PERIOD-CLK_OFFSET] = 0;
endgenerate

assign clk_shape[SCLK_PERIOD-CLK_OFFSET-1:SCLK_PERIOD-CLK_OFFSET-HIGH_CLK] = ~0;

//SCLK_PERIOD == (CLK_OFFSET + HIGH_CLK) 이면 clk_shape 에러이므로 방지 구문
generate
	if (SCLK_PERIOD != (CLK_OFFSET + HIGH_CLK))
		assign clk_shape[SCLK_PERIOD-CLK_OFFSET-HIGH_CLK-1:0] = 0;
endgenerate

// ---------------------------------------------------------------------------------
// Write SPI
localparam state_idle = 	3'b000;
localparam state_cs = 		3'b001;
localparam state_data = 	3'b011;
localparam state_end = 		3'b100;
localparam state_done = 	3'b101;

reg [2:0] 	s_spi;
reg 		sdo_r;
assign 		sdo = sdo_r;

reg [SCLK_PERIOD_BIT-1:0]	clk_count;
reg [MAX_OF_WIDTH_BIT-1:0]	bit_count;
reg [DATA_WIDTH-1:0]		shift_reg;

always @(posedge clk) begin
	if (sclr) begin
		scs_n		<= ~SCS_N_MODE;
		sclk		<= ~SCLK_IDLE_MODE;
		sdo_r		<= 1'b0;
		latch_en	<= 1'b0;
		shift_reg	<= 'b0;
		din_ready	<= 1'b1;
		clk_count	<= CLK_START_END;
		bit_count	<= DATA_WIDTH - 1;
		s_spi		<= state_idle;
	end
	else begin
		case(s_spi)
			state_idle : begin
				if (din_valid) begin			// din_valid가 오면 시작
					din_ready	<= 1'b0;
					latch_en	<= 1'b0;
					shift_reg	<= din;
					sdo_r		<= 1'b0;
					s_spi		<= state_cs;
				end
				else begin
					din_ready	<= 1'b1;
					latch_en	<= 1'b0;
					shift_reg	<= 'b0;
					sdo_r		<= 1'b0;
					s_spi		<= state_idle;
				end
				scs_n		<= ~SCS_N_MODE;
				sclk		<= ~SCLK_IDLE_MODE;
				bit_count	<= DATA_WIDTH - 1;
				clk_count	<= CLK_START_END;
			end
			state_cs : begin					// ChipSel part
				if (clk_count > 0) begin
					clk_count	<= clk_count - 1'b1;
					sclk		<= sclk;
					shift_reg	<= shift_reg;
					sdo_r		<= sdo_r;
					s_spi		<= state_cs;
				end
				else begin
					clk_count	<= SCLK_PERIOD - 1;
					bit_count	<= DATA_WIDTH - 1;
					sclk		<= clk_shape[SCLK_PERIOD - 1];
					shift_reg	<= {shift_reg[DATA_WIDTH-2:0],1'b0};
					sdo_r		<= shift_reg[DATA_WIDTH-1];
					s_spi		<= state_data;
				end
				latch_en	<= latch_en;
				scs_n		<= SCS_N_MODE;
				din_ready	<= din_ready;
			end

			state_data : begin				// DATA part
				if (bit_count > 0 ) begin
					if (clk_count > 0) begin
						clk_count	<= clk_count -1'b1;
						bit_count	<= bit_count;
						sclk		<= clk_shape[clk_count-1];
						shift_reg	<= shift_reg;
						sdo_r		<= sdo_r;
						s_spi		<= state_data;
					end
					else begin
						clk_count	<= SCLK_PERIOD - 1;
						bit_count	<= bit_count - 1'b1;
						sclk		<= clk_shape[SCLK_PERIOD - 1];
						shift_reg	<= {shift_reg[DATA_WIDTH-2:0],1'b0};
						sdo_r		<= shift_reg[DATA_WIDTH-1];
						s_spi		<= state_data;
					end
				end
				else begin
					if (clk_count > 'b0) begin
						clk_count	<= clk_count - 1'b1;
						bit_count	<= bit_count;
						sclk		<= clk_shape[clk_count -1];
						shift_reg	<= shift_reg;
						sdo_r		<= sdo_r;
						s_spi		<= state_data;
					end
					else begin
						clk_count	<= CLK_START_END;
						bit_count	<= 'b0;
						sclk		<= 1'b0;
						shift_reg	<= {shift_reg[DATA_WIDTH-2:0],1'b0};
						sdo_r		<= shift_reg[DATA_WIDTH-1];
						s_spi		<= state_end;
					end
				end
				latch_en	<= latch_en;
				scs_n		<= scs_n;
				din_ready	<=din_ready;
			end
			state_end : begin			// 완료
				if (clk_count > 0) begin
					clk_count	<= clk_count - 1'b1;
					scs_n		<= scs_n;
					latch_en	<= latch_en;
					s_spi		<= state_end;
					din_ready	<= din_ready;
					bit_count	<= bit_count;
				end
				else begin
					clk_count	<= CLK_START_END;
					scs_n		<= ~SCS_N_MODE;
					latch_en	<= 1'b1;
					bit_count	<= 1'b0;
					s_spi		<= state_done;
				end
				sclk <= sclk;
				sdo_r <= sdo_r;
				din_ready <=din_ready;
			end
			state_done : begin			// 완료
				if (clk_count > 0) begin
					clk_count	<= clk_count - 1'b1;
					scs_n		<= scs_n;
					bit_count	<= bit_count;
					s_spi		<= state_done;
				end
				else begin
					clk_count	<= CLK_START_END;
					scs_n		<= ~SCS_N_MODE;
					bit_count	<= DATA_WIDTH - 1;
					s_spi		<= state_idle;
				end
				din_ready	<= din_ready;
				sclk		<= sclk;
				sdo_r		<= sdo_r;
				latch_en	<= latch_en;
			end
		endcase
	end
end


endmodule

